package supportlibraries;

import java.util.Hashtable;

/**
 * Class that encapsulates Test Case level Variables
 * @author MFA
 */
public class TestVariables {
//	private String relativePath;
//	private String runConfiguration;	 
//	private boolean stopExecution = false;
	
	private Hashtable<String,Object> ht;
	
	private static final TestVariables TEST_VARIABLES =	new TestVariables();
	
	private TestVariables() {
		// To prevent external instantiation of this class
		ht = new Hashtable<String,Object> ();
	}
	
	/**
	 * Function to return the singleton instance of the {@link TestVariables} object
	 * @return Instance of the {@link TestVariables} object
	 */
	public static TestVariables getInstance() {
		return TEST_VARIABLES;
	}
	
	/**
	 * Function to get a Hashtable<String,Object> value which holds Test Variables of a particular Test Case
	 * @return The Test Variable Hashtable<String,Object> value
	 */
	public Hashtable<String,Object> getTestVariable() {
		return ht;
	}
//	/**
//	 * Function to set a boolean value indicating whether to stop the overall test batch execution
//	 * @param stopExecution Boolean value indicating whether to stop the overall test batch execution
//	 */
//	public void setStopExecution(boolean stopExecution) {
//		this.stopExecution = stopExecution;
//	}
	
	/**
	 * Function to get Variable value from Hash Table
	 * @param variableName String value by which we can get the Variable value of type Object
	 */
	public Object getVariable(String variableName){
		return this.ht.get(variableName);
	}
	
	/**
	 * Function to add/update Variable value from Hash Table
	 * @param variableName String value by which we can add/update the Variable value of type Object
	 * @param variableValue Object value that holds any type of Object (String, Boolean, int, double, etc)
	 */
	public void addVariable(String variableName, Object variableValue){
		this.ht.put(variableName,variableValue);
	}

	
	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
}