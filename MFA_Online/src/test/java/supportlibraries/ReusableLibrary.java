package supportlibraries;

import java.util.Properties;
import com.mfa.framework.CraftDataTable;
import com.mfa.framework.FrameworkParameters;
import com.mfa.framework.Settings;
import com.mfa.framework.selenium.CraftDriver;
import com.mfa.framework.selenium.SeleniumReport;
import com.mfa.framework.selenium.WebDriverUtil;




/**
 * Abstract base class for reusable libraries created by the user
 * @author cognizant
 */
public abstract class ReusableLibrary {
	/**
	 * The {@link CraftDataTable} object (passed from the test script)
	 */
	protected CraftDataTable dataTable;
	/**
	 * The {@link SeleniumReport} object (passed from the test script)
	 */
	protected SeleniumReport report;
	/**
	 * The {@link CraftDriver} object
	 */
	protected CraftDriver driver;

	protected WebDriverUtil driverUtil;
	
	/**
	 * The {@link ScriptHelper} object (required for calling one reusable library from another)
	 */
	protected  ScriptHelper scriptHelper;
	
	/**
	 * The {@link Properties} object with settings loaded from the framework properties file
	 */
	protected Properties properties;
	
	/**
	 * The {@link Properties} object with settings loaded from the framework properties file
	 */
	protected Properties ObjectRepositoryproperties;
	/**
	 * The {@link FrameworkParameters} object
	 */
	protected FrameworkParameters frameworkParameters;
	
	protected TestVariables tv;/////////////////////////////////////////////
	
	//private ReportSettings reportSettings;
	/**
	 * Constructor to initialize the {@link ScriptHelper} object and in turn the objects wrapped by it
	 * @param scriptHelper The {@link ScriptHelper} object
	 */
	public ReusableLibrary(ScriptHelper scriptHelper) {
		this.scriptHelper = scriptHelper;
		this.dataTable = scriptHelper.getDataTable();
		this.report = scriptHelper.getReport();
		this.driver = scriptHelper.getcraftDriver();
		this.driverUtil = scriptHelper.getDriverUtil();
		
		properties = Settings.getInstance();
		//////////////////////////////////////////////////////////////////////////////
		/*
		* Created by Kamatchirajan on Jul 06, 2018
		* ObjectRepositoryproperties ==> created to support Object Repository
		*/
		ObjectRepositoryproperties = Settings.ObjectRepositoryPropertiesInstance();
		frameworkParameters = FrameworkParameters.getInstance();
		tv = TestVariables.getInstance();
	}
	
//////////////////Reusable Methods for Multi Brand Automation ////////////////////////////////
//	/**
//     * Description: Navigate To the given Menu.
//     * @throws Exception
//     * return Boolean - returns True if navigateToMenu is success and False if navigateToMenu is Failed
//     */
//	public Boolean navigateToMenu() throws Exception{    		    				
//		javaScriptClick("AEM.homeAnchor","link - Adobe Experience Manager");
//		javaScriptClick("AEM.compassIcon","Icon Compass");
// 		System.out.println("Clicked on the Link - Adobe Experience Manager");
// 		javaScriptClick("AEM.sites","Link Site"); // Click on the Sites Link
// 		String navigateMenuFlow = properties.getProperty("NavigateMenu");
// 		System.out.println("navigateMenuFlow: " + navigateMenuFlow);
// 		String[] splitMenus = navigateMenuFlow.split("/");
// 		int isplitMenuCount = splitMenus.length;
// 		Boolean bMenuFound = true;
// 		for(int i=0; i<isplitMenuCount; i++){
// 			System.out.println("navigate to Menu: " + splitMenus[i]);
// 			By byLinkMenu = By.xpath("//coral-columnview-item-content[@title='" + splitMenus[i] + "']");
// 			if(!isElementVisible(byLinkMenu,"Link Menu - " + splitMenus[i],10)){ // handle coral Dialog Error
// 				handleCoralDialogError();
// 			}
// 			if(isElementVisible(byLinkMenu,"Link Menu - " + splitMenus[i],10)){
// 				javaScriptClick(byLinkMenu,"Link Menu - " + splitMenus[i]); // Click on the Menu Link 				
// 			}else{
// 				bMenuFound = false;
// 	 			report.updateTestLog("navigateToMenu", "Link Menu - " + splitMenus[i] + " - not Found" , Status.FAIL);
// 	 			//break;
// 				if(splitMenus[i].equalsIgnoreCase("AutomationTesting")){
// 					createAutomationPage();
// 					i--;
// 				}
// 				else{
// 					bMenuFound = false;
// 	 				report.updateTestLog("navigateToMenu", "Link Menu - " + splitMenus[i] + " - not Found" , Status.FAIL);
// 	 				break;
// 				}
// 			}
// 		}
// 		if(bMenuFound){ 			
// 			String navigateMenuFlowCustomized = customizeNavigateMenu(navigateMenuFlow); 			
// 	 		String xpathPageTitles = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']";
// 	 		By byPageTitles = By.xpath(xpathPageTitles);
// 	 		String xpathPageTitles2 = "//coral-columnview-preview[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']";
// 	 		By byPageTitles2 = By.xpath(xpathPageTitles2);
// 	 		if(isElementPresent(byPageTitles,"Navigate Menu - Child Pages") || isElementPresent(byPageTitles2,"Navigate Menu Preview")){
// 	 			System.out.println("navigated to Menu: " + navigateMenuFlow + " - Pass");
// 	 			report.updateTestLog("Navigate to Menu", "Navigated to Menu: " + navigateMenuFlow, Status.PASS);
// 	 			return true;
// 	 		}else{
// 	 			System.out.println("navigated to Menu: " + navigateMenuFlow + " - Fail");
// 	 			report.updateTestLog("navigateMenuFlow", "Navigated to Menu: " + navigateMenuFlow, Status.FAIL);
// 	 			return false;
// 	 		}
// 		}else{
// 			return false;
// 		}
// 	}
//	/**
//     * Description: Navigate To the given Menu.
//     * @throws Exception
//     * return Boolean - returns True if navigateToMenu is success and False if navigateToMenu is Failed
//     */
//	public Boolean navigateToMenu(String navigateMenu) throws Exception{	    				
//		javaScriptClick("AEM.homeAnchor","link - Adobe Experience Manager");
//		javaScriptClick("AEM.compassIcon","Icon Compass");
// 		System.out.println("Clicked on the Link - Adobe Experience Manager");
// 		javaScriptClick("AEM.sites","Link Site"); // Click on the Sites Link
// 		//String navigateMenu = properties.getProperty("NavigateMenu");
// 		System.out.println("navigateMenu: " + navigateMenu);	
// 		String[] splitMenus = navigateMenu.split("/");
// 		int isplitMenuCount = splitMenus.length;
// 		Boolean bMenuFound = true;
// 		String strPageLocation = "";
// 		for(int i=0; i<isplitMenuCount; i++){
// 			if(i >= 1){
// 				if(strPageLocation.equals("")){
// 	 				strPageLocation = splitMenus[i-1];
// 	 			}else{
// 	 				strPageLocation = strPageLocation + "/" + splitMenus[i-1];
// 	 			}
// 			}
// 			
// 			
// 			System.out.println("navigate to Menu: " + splitMenus[i]);
// 			By byLinkMenu = By.xpath("//coral-columnview-item-content[@title='" + splitMenus[i] + "']");
// 			if(!isElementVisible(byLinkMenu,"Link Menu - " + splitMenus[i],10)){ // handle coral Dialog Error
// 				handleCoralDialogError();
// 			}
// 			
// 			if(splitMenus[i].toLowerCase().startsWith("autohome")){
// 				WebElement ePage = isPageMenuPresent(strPageLocation,splitMenus[i]);;
// 	 			if(ePage != null){
// 	 				
// 	 				javaScriptClick(ePage,"Link Menu - " + splitMenus[i]); // Click on the Menu Link 
// 	 			}else{
// 	 				bMenuFound = false;
// 	 	 			report.updateTestLog("navigateToMenu", "Link Menu - " + splitMenus[i] + " - not Found" , Status.FAIL); 	 			
// 	 			}
// 			}else{
// 				if(isElementVisible(byLinkMenu,"Link Menu - " + splitMenus[i],10)){
// 	 				javaScriptClick(byLinkMenu,"Link Menu - " + splitMenus[i]); // Click on the Menu Link 				
// 	 			}else{
// 	 				bMenuFound = false;
// 	 	 			report.updateTestLog("navigateToMenu", "Link Menu - " + splitMenus[i] + " - not Found" , Status.FAIL); 	 			
// 	 			}
// 			}			
// 			
// 		}
// 		if(bMenuFound){ 			
// 			String navigateMenuCustomized = customizeNavigateMenu(navigateMenu);
// 			System.out.println("navigateMenuCustomized: " + navigateMenuCustomized);
// 	 		String xpathPageTitles = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuCustomized +"']";
// 	 		By byPageTitles = By.xpath(xpathPageTitles);
// 	 		String xpathPageTitles2 = "//coral-columnview-preview[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuCustomized +"']";
// 	 		By byPageTitles2 = By.xpath(xpathPageTitles2);
// 	 		System.out.println("With Child Pages - xpathPageTitles: " + xpathPageTitles);
// 	 		System.out.println("Without Any Child Pages - xpathPageTitles2 " + xpathPageTitles);
// 	 		if(isElementPresent(byPageTitles,"Navigate Menu - Child Pages") || isElementPresent(byPageTitles2,"Navigate Menu Preview")){
// 	 			System.out.println("navigated to Menu: " + navigateMenu + " - Pass");
// 	 			report.updateTestLog("Navigate to Menu", "Navigated to Menu: " + navigateMenu, Status.PASS);
// 	 			return true;
// 	 		}else{
// 	 			System.out.println("navigated to Menu: " + navigateMenu + " - Fail");
// 	 			report.updateTestLog("navigateMenu", "Navigated to Menu: " + navigateMenu, Status.FAIL);
// 	 			return false;
// 	 		}
// 		}else{
// 			return false;
// 		}
// 	}
//	
//	public String findNavigateToMenu(String strPageTemplate,String strNavigateToMenu){
//    	if(strNavigateToMenu.isEmpty()){			
//			switch(strPageTemplate.toLowerCase()){
//			case "generic page template":
//				strNavigateToMenu = properties.getProperty("NavigateMenuForGenericPageTemplate");
//				break;
//			case "home page template":
//				strNavigateToMenu = properties.getProperty("NavigateMenuForHomePageTemplate");
//				break;
//			case "full bleed page template":
//				strNavigateToMenu = properties.getProperty("NavigateMenuForFullBleedPageTemplate");
//				break;
//			case "area template":
//				strNavigateToMenu = properties.getProperty("NavigateMenuForAreaTemplate");
//				break;
//			case "self certification page template":
//				break;
//			default:
//				report.updateTestLog("Create Page", "Page Template: " + strPageTemplate + " not Valid", Status.FAIL);
//				frameworkParameters.setStopExecution(true);
//			}
//		}
//		return strNavigateToMenu;
//    }
//	
//	public WebElement isPageMenuPresent(String strPageLocation,String pageTitle) throws Exception{
// 		System.out.println("isPageTitlePresent - PageLocation: " + strPageLocation + " PageTitle: " + pageTitle);
// 		Boolean bFlag = false;
// 		WebElement ePageTitle = null;
//		//System.out.println("CreatedPageTitle: " + pageTitle);
//		//String navigateMenuFlowCustomized = properties.getProperty("NavigateMenuCustomized");
// 		By byCoralWait = By.xpath("//coral-columnview-column-content//coral-wait");
// 		isElementNotVisible(byCoralWait,"byCoralWait");
//		//String navigateMenu = properties.getProperty("NavigateMenu");
//		String navigateMenuFlowCustomized = customizeNavigateMenu(strPageLocation);
//		String xpathPageTitles = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']//coral-columnview-item-content";	
//		System.out.println("xpathPageTitles: " + xpathPageTitles);
//		By byPageTitles = By.xpath(xpathPageTitles);
//		Boolean bElement = isElementPresent(byPageTitles,"Page Titles");
//		if(!bElement){		
//			navigateToMenu(strPageLocation);//Call navigateToMenu() method
//		}else{
//			//highLightElement(driver.findElement(byPageTitles));
//			System.out.println("xpath path identified: ");	
//			List<WebElement> pageTitles = driver.getWebDriver().findElements(byPageTitles);			
//			//String strpageImages = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/multibrand/global-master/en/testing']//coral-columnview-item-thumbnail/img";
//			//String strpageImages = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']//coral-columnview-item-thumbnail/img";
//			//System.out.println("xpath: " + strpageImages);
//			//By bypageImages = By.xpath(strpageImages);
//			//List<WebElement> pageImages = driver.getWebDriver().findElements(bypageImages);	
//			//System.out.println("matching pages collected ");
//			int pageCount = pageTitles.size();
//			System.out.println("Page Count: " + pageCount);			
//			for(int i= 0; i< pageCount; i++){
//				String tmppageTitle = pageTitles.get(i).getAttribute("title");	
//				//javaScriptClick(pageTitles.get(i),tmppageTitle);
//				scrollElementToMiddlePage(pageTitles.get(i),tmppageTitle);
//				if(i >= (pageCount -2)){
//					scrollElementToMiddlePage(pageTitles.get(i),tmppageTitle);
//					javaScriptClick(pageTitles.get(i),tmppageTitle);
//					//highLightElement(pageTitles.get(i));
//					waitFor(1000);
//					if(pageCount > 39){
//						waitFor(2000);
//					}
//					pageTitles = driver.getWebDriver().findElements(byPageTitles);
//					//pageImages = driver.getWebDriver().findElements(bypageImages);
//					int iTmpPageCount = pageTitles.size();
//					if(iTmpPageCount > pageCount){
//						pageCount = iTmpPageCount;
//						System.out.println("Page Count: " + pageCount);
//						//scrollInToElement(pageTitles.get(pageCount - 1),"Last Page Title");
//						//javaScriptClick(pageTitles.get(pageCount - 1),"Last Page Title");
//					}				
//				}
//				//System.out.println(i + " - Page Title name: " + tmppageTitle);
//				if(tmppageTitle.contains("Home") || tmppageTitle.contains("dynamic")){
//					System.out.println("Page name: " + tmppageTitle);
//					System.out.println("");
//				}
//				
//				if(tmppageTitle.equalsIgnoreCase(pageTitle)){
//					//String tmpPageImage = pageImages.get(i).getAttribute("src");
//					//System.out.println("Page Image src: " + tmpPageImage);
//					//pageImages.get(i).click();
//					//scrollToElement(pageTitles.get(i),tmppageTitle);
//					scrollElementToMiddlePage(pageTitles.get(i),tmppageTitle);
//					bFlag = true;
//					ePageTitle = pageTitles.get(i);
//					break;
//				}			
//			}
//		}		
//		if(bFlag){
//			System.out.println(pageTitle + " - Page is Present.");
//		}else{
//			//report.updateTestLog("isPageTitlePresent", pageTitle + " Page not Found." , Status.FAIL);
//			System.out.println(pageTitle + " Page is not selected.");
//		}
//		return ePageTitle;
// 	}
//	
//	
//	/**
//     * Description: Navigate To the given Menu. This method is scripted specific to Edge Browser.
//     * @throws Exception
//     * return Boolean - returns True if navigateToMenu is success and False if navigateToMenu is Failed
//     */
//	public void navigateToMenuInEdge(String navigateMenuFlow) throws Exception{    		    				
//		javaScriptClick("AEM.homeAnchor","link - Adobe Experience Manager");
// 		javaScriptClick("AEM.compassIcon","Icon Compass");
// 		System.out.println("Clicked on the Link - Adobe Experience Manager");
// 		//waitFor(3000);
// 		javaScriptClick("AEM.sites","Link Site"); // Click on the Sites Link
// 		System.out.println("navigateMenuFlow: " + navigateMenuFlow); 	
// 		String[] splitMenus = navigateMenuFlow.split("/");
// 		String strTmpMenu ="";
// 		//waitFor(5000);
// 		Boolean bMenuFound = true;
// 		for(String menu : splitMenus){
// 			System.out.println("navigate to Menu: " + menu);
// 			By byLinkMenu = By.xpath("//coral-columnview-item[contains(@data-foundation-collection-item-id,'/content/" + strTmpMenu + "')]");
// 			if(isElementVisible(byLinkMenu, "Menu")){
// 	 			List<WebElement> eMenus = driver.getWebDriver().findElements(byLinkMenu);
// 	 			int iMenuCount = eMenus.size();
// 	 			System.out.println("iMenuCount: " + iMenuCount);
// 	 			Boolean bFlag = false;
// 	 			for(int i = 0; i< iMenuCount; i++ ){
// 	 				System.out.println("Menus in " + strTmpMenu);
// 	 				//waitAndHighlightElement(eMenus.get(i), "Menu");
// 	 				scrollInToViewElement(eMenus.get(i), "Menu", 100);
// 	 				String strGetMenu = eMenus.get(i).getText().trim();
// 	 				System.out.println(i + " Menus in " + strTmpMenu + " : " + strGetMenu); 				
// 	 				if(menu.equalsIgnoreCase(strGetMenu)){
// 	 					javaScriptClick(eMenus.get(i),menu);
// 	 					bFlag = true;
// 	 					break;
// 	 				}
// 	 			} // Inside For Loop
// 	 			if(bFlag){
// 	 				System.out.println("navigateMenuFlow: " + menu + " Clicked");
// 	 				strTmpMenu = strTmpMenu + menu + "/" ;
// 	 				if(menu.equalsIgnoreCase("AutomationTesting")){
// 	 					// No change
// 	 				}else{
// 	 					strTmpMenu = strTmpMenu.toLowerCase();
// 	 	 				strTmpMenu = strTmpMenu.replace(" ", "-");
// 	 				}			
// 	 				
// 	 			}else{
// 	 				System.out.println("navigateMenuFlow: " + menu + " not Found");
// 	 				report.updateTestLog("navigateToMenu", "Link Menu - " + menu + " - not Found" , Status.FAIL);
// 	 				bMenuFound = false;
// 	 				break;
// 	 			}
// 			}else{
// 				System.out.println("navigateMenuFlow: " + "no Pages in " + strTmpMenu);
//	 				report.updateTestLog("navigateToMenu", "No Pages in " + strTmpMenu , Status.FAIL);
//	 				bMenuFound = false;
//	 				break; 				
// 			}
// 			 			
// 		} // For Loop
// 		if(bMenuFound){
// 			System.out.println("navigated to Menu: " + navigateMenuFlow);
// 			report.updateTestLog("navigateMenuFlow", "Navigated to Menu: " + navigateMenuFlow, Status.PASS);
// 		}else{
// 			report.updateTestLog("navigateMenuFlow", "Navigated to Menu: " + navigateMenuFlow, Status.FAIL);
// 		} 			
// 	}
//
//	/**
//     * Description: Create AutomationPage, if not exists in the path (Multibrand/Global Master/en/)
//     * @throws Exception
//     */
//	public void createAutomationPage() throws Exception {
//		System.out.println("Create Page: AutomationTeating");
//		javaScriptClick("AEM.create","Create Button"); // Click on the Create Button
//		javaScriptClick("AEM.listItemPage","list Item Page"); // Click on the Page Option from the menu    		
//		// Click on Page Template
//		List<WebElement> PageTemplateList= driver.getWebDriver().findElements(getLocator("AEM.PageTemplates"));
//		int iPageTemplateListCount= PageTemplateList.size();
//		boolean bTemplatefound = false;
//		String strPageTemplate = "Generic page template";
//		String strPageTitle = "AutomationTesting";	//Fill Page Details
//		for(int i=0; i< iPageTemplateListCount; i++){
//			String strTemplate = PageTemplateList.get(i).getText();
//			if(strTemplate.equalsIgnoreCase(strPageTemplate)){
//				javaScriptClick(PageTemplateList.get(i),strTemplate); // Click on the Template
//				bTemplatefound = true;
//				break;
//			}    			
//		}
//		if(bTemplatefound){
//			report.updateTestLog("createPage", strPageTemplate + " - Selected from the Template Page" , Status.PASS);
//		}else{
//			report.updateTestLog("createPage", strPageTemplate + " - not present in the Template Page" , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}       		
//		javaScriptClick("AEM.CreatePageNext","Create Page - Next Button"); // Click on the Next Button		
//		setData("AEM.pageName","Page Name",strPageTitle);
//		setData("AEM.title","Title",strPageTitle);
//		setData("AEM.pageTitle","Page Title",strPageTitle);
//		setData("AEM.navTitle","Page Navigation Title",strPageTitle);
//		report.updateTestLog("Create AutomationTesting Page", "Fill Page Template Details ", Status.SCREENSHOT);
//		javaScriptClick("AEM.create","Create Button"); // Click on the Create Button
//		//Handle Dialog box
//		By byDialogContent = getLocator("AEM.dialogContent");
//		if(isElementVisible(byDialogContent, "Dialog Popup")){
//    		waitFor(1000); /// To take screenshot with pop up message.
//    		String dialogMsg = driver.findElement(byDialogContent).getText();
//    		if( dialogMsg.equalsIgnoreCase("Your page has been created.")){
//    			report.updateTestLog("Create AutomationTesting Page", dialogMsg + "Created Page name: " + strPageTitle, Status.PASS);			
//    			//dataTable.putData("MultiBrand_Data", "CreatedPageTitle", strPageTitle);  // Update Created Page Title to Data Sheet
//    		}else{
//    			report.updateTestLog("Create AutomationTesting Page", dialogMsg + "Created Page name: " + strPageTitle, Status.FAIL);
//    		}
//    		javaScriptClick("AEM.dialogDone","Dialog - Done Button"); // Click on the Done Button 
//		}else{
//			report.updateTestLog("createPage", " Create Page - Dialog Pop up not Found" , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}	
//	}
//	/**
//     * Description: Resizes an image by a percentage of original size (proportional).
//     * @param strNavigateMenu - Navigate Menu Path
//     * return String - returns customised Navigate Menu
//     * @throws IOException
//     */
//	public String customizeNavigateMenu(String strNavigateMenu){	
//		//System.out.println("navigateMenuFlow: " + strNavigateMenu);
// 		String[] splitMenus = strNavigateMenu.split("/");
// 		int iMenuCount = splitMenus.length;
// 		String strTmpMenu ="";
// 		for(int i = 0; i< iMenuCount; i++){
// 			String strMenu = splitMenus[i];
//			if(strMenu.equalsIgnoreCase("AutomationTesting") || strMenu.equalsIgnoreCase("Auto_Home") || strMenu.equalsIgnoreCase("AutoHome")){
//				// No change				
//				strMenu = properties.getProperty("AutoHome_CopyName");
//			}else if(strMenu.equalsIgnoreCase("United Kingdom")){
//				strMenu = "uk";
//			}else if(strMenu.equalsIgnoreCase("English")){
//				strMenu = "en";
//			}else if(strMenu.equalsIgnoreCase("Belgium")){
//				strMenu = "be";
//			}else if(strMenu.equalsIgnoreCase("Nederlands")){
//				strMenu = "nl";
//			}else if(strMenu.equalsIgnoreCase("My Account")){
//				strMenu = "my-account";
//			}else{
//				strMenu = strMenu.toLowerCase();
//				strMenu = strMenu.replace(" ", "-");
//			}
//			if(i == (iMenuCount - 1)){
//				strTmpMenu = strTmpMenu + strMenu ;
//			}else{
//				strTmpMenu = strTmpMenu + strMenu + "/" ;
//			}			
// 		}
// 		//System.out.println("customizedNavigateMenu: " + strTmpMenu);
// 		//return strTmpMenu;
// 		String strCustMenu = "";
// 		String strAuthorUrl = properties.getProperty("AEMTouchAuthorUrl");
// 		System.out.println("strAuthorUrl: " + strAuthorUrl);
// 		System.out.println("strTmpMenu: " + strTmpMenu);
// 		Boolean bb = strAuthorUrl.contains("preprod");
// 		System.out.println("strAuthorUrl.contains('preprod'): " + bb);
// 		if(strAuthorUrl.contains("preprod")){
// 			strCustMenu = strTmpMenu;
// 		}
//// 		else{
//// 			strCustMenu = properties.getProperty("NavigateMenuCustomized");// For Prod Testing 		
//// 		}
// 		System.out.println("CustnavigateMenuFlow: " + strCustMenu);
// 		return strCustMenu;
//	}
//	
//	/**
//     * Description: publish the created Page.
//     * @throws Exception
//     */	
//	public void publishPage() throws Exception{
//		// Publish the Page
//		Boolean bPublish = false;
//		isElementVisible("AEM.PageInfo","Page Info Menu");
//		scrollToTop();
//		////////////////waitFor(2000);
//		System.out.println("click on Page Info button");
//		//mouseClickJScript(driver.findElement(getLocator("AEM.PageInfo")),"Page Info Menu");
//		javaScriptClick("AEM.PageInfo","Page Info Menu");
//		By byAlertMsg = By.xpath("//div[contains(@class,'coral-Alert-message')]");
//		////////////waitFor(3000);
//			System.out.println("click on Publish Page option from the Page Info menu");	
//			if(isElementPresent("AEM.PublishPage","Publish Page Option",20)){
//				highLightElement(driver.findElement(By.xpath("//*[@id='pageinfo-data']/button[@title='Publish Page']/span")));
//				//javaScriptClick("AEM.PublishPage","Publish Page Option");
//				mouseClickJScript(driver.findElement(getLocator("AEM.PublishPage")), "Publish Page Option");
//				/////////////////////////waitFor(5000);
//				isElementVisible(byAlertMsg, "Alert Message");
//				mouseClickJScript("AEM.PageInfo","Page Info Menu");
//				if(isElementNotVisible("AEM.PublishPage","Publish Page Option",5)){
//					mouseClickJScript("AEM.PageInfo","Page Info Menu");					
//				}
//				waitFor(5000);								
//				mouseClickJScript(driver.findElement(getLocator("AEM.UnpublishPage")), "Unpublish Page Option");		
//				waitFor(2000);
//				clickElement(driver.findElement(By.xpath("//div[contains(@style,'block')]//button[text()='Confirm']")), "Confirm - Unpublish");
//				isElementVisible(byAlertMsg, "Alert Message");
//				waitFor(5000);
//				javaScriptClick("AEM.PageInfo","Page Info Menu");
//				waitFor(3000);
//				highLightElement(driver.findElement(By.xpath("//*[@id='pageinfo-data']/button[@title='Publish Page']/span")));
//				mouseClickJScript(driver.findElement(getLocator("AEM.PublishPage")), "Publish Page Option");
//				
//				//driver.findElement(getLocator("AEM.PublishPage")).click();								
//				
//				if(isElementPresent(byAlertMsg, "Alert Message")){
//					String strAlertMsg = driver.findElement(byAlertMsg).getText();
//					System.out.println("strAlertMsg:" + strAlertMsg);
//					//report.updateTestLog("publishPage", "Alert Msg:" + strAlertMsg,Status.DONE);
//					if(strAlertMsg.equalsIgnoreCase("The page has been published")){
//						report.updateTestLog("publishPage", strAlertMsg,Status.PASS);
//						bPublish = true;
//					}else{
//						mouseClickJScript(driver.findElement(getLocator("AEM.PageInfo")),"Page Info Menu");
//						if(isElementPresent("AEM.PublishPage","Publish Page Option",20)){
//							javaScriptClick("AEM.PublishPage","Publish Page Option");
//							//By byAlertMsg = By.xpath("//div[contains(@class,'coral-Alert-message')]");
//							if(isElementPresent(byAlertMsg, "Alert Message")){
//								strAlertMsg = driver.findElement(byAlertMsg).getText();
//								System.out.println("strAlertMsg:" + strAlertMsg);
//								if(strAlertMsg.equalsIgnoreCase("The page has been published")){
//									report.updateTestLog("publishPage", strAlertMsg,Status.PASS);
//									bPublish = true;
//								}else{
//									//report.updateTestLog("publishPage", "Page is not published.",Status.FAIL);
//								}
//							}
//						}											
//					}				
//				}else{ // if Alert Message not found
//					By byPublishButton = By.xpath("//coral-button-label[text()='Publish']"); // different publish button
//					By byPublishButton2 = By.xpath("//*[@type='submit'][text()='Publish']"); // different publish button					
//					//By byCancelButton = By.xpath("//a/*[text()='Cancel']");
//					//report.updateTestLog("publishPage", "Page Publish - Screenshot",Status.SCREENSHOT);
//					Boolean bPublishButtonPresent = false;
//					if(isElementPresent(byPublishButton,"byPublishButton")){
//						bPublishButtonPresent = true;
//						System.out.println("Publish Button 1 present:");
//					}else if(isElementPresent(byPublishButton2,"byPublishButton2")){
//						byPublishButton = byPublishButton2;
//						bPublishButtonPresent = true;
//						System.out.println("Publish Button 2 present:");
//					}
//					
//					if(bPublishButtonPresent){
//						By byAllAssets = By.xpath("//*[@id='references']/div[2]/coral-table/div/table/thead/tr/th[1]/coral-th-label/coral-checkbox");
//						clickCheckBox(byAllAssets, "AllAssets Checkbox","Yes");
//						highLightElement(driver.findElement(byPublishButton));
//						report.updateTestLog("publishPage", "Page Publish - Screenshot",Status.SCREENSHOT);
//						javaScriptClick(byPublishButton, "Publish Button");
//						if(isElementPresent(byAlertMsg, "Alert Message")){
//							String strAlertMsg = driver.findElement(byAlertMsg).getText();
//							System.out.println("strAlertMsg:" + strAlertMsg);
//							report.updateTestLog("publishPage", "Alert Msg:" + strAlertMsg,Status.DONE);
//							if(strAlertMsg.equalsIgnoreCase("The page has been published")){
//								report.updateTestLog("publishPage", "Page is published.",Status.PASS);
//								bPublish = true;
//							}else{
//								report.updateTestLog("publishPage", "Page is not published.",Status.FAIL);						
//							}				
//						}
//					}
//				}
//			}else if(isElementPresent("PageInfo_RequestPublication","PageInfo_RequestPublication",10)){
//				report.updateTestLog("publishPage", "Request Publication option is present and so it has to goto approval process.",Status.SCREENSHOT);
//				javaScriptClick("AEM.PageInfo","Page Info");
//				System.out.println("Request Publication option is present and so it has to goto approval process");
//			}else{
//				report.updateTestLog("publishPage", "Publish Page option not present in the Page Info menu",Status.SCREENSHOT);
//				javaScriptClick("AEM.PageInfo","Page Info");
//				System.out.println("publishPageFromAEMsite method called");
//				bPublish = publishPageFromAEMsite();
//			}
//	//	}	////////	
//		if(bPublish){			
//			createPublishUrl();
//		}else{
//			//report.updateTestLog("publishPage", "Page is not published.",Status.FAIL);
//			//dataTable.putData("MultiBrand_Data", "CreatedPageTitle", "Error: Page not published");  // Update Created Page Title to Data Sheet
//			createPublishUrl();
//		}	
//	}
//	/**
//     * Description: publish the Page from AEM site
//     * 
//     * @throws Exception
//     */
// 	public Boolean publishPageFromAEMsite() throws Exception{
//		// change focus to AEM Site
// 		Boolean bPublish = false;
// 		Boolean bPageSelected = false;
//		String pageTitle = dataTable.getData("MultiBrand_Data", "CreatedPageTitle");
//	    String currentTabWinHandle = driver.getWindowHandle();
//	    System.out.println("currentTabWinHandle:" + currentTabWinHandle);
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iWindowcount = availableWindows.size();
//	    System.out.println("iWindowcount:" + iWindowcount);
//	    if(iWindowcount == 1){	    	
//	    	driver.navigate().back();// Click Back and do
//	    	System.out.println("navigate back:");
//	    	report.updateTestLog("publishPageFromAEMsite", "Go Back to Parent window: ", Status.SCREENSHOT);
//	    }else if(iWindowcount == 2){
//	    	String strHandle = availableWindows.get(iWindowcount - 2);
//	    	driver.switchTo().window(strHandle);
//	    	System.out.println("Switch to AEM Site Window: " + strHandle);
//	    	report.updateTestLog("publishPageFromAEMsite", "Switch to old window: " + strHandle, Status.SCREENSHOT);
//	    }else{
//	    	System.out.println("Error: What next");
//	    	//report.updateTestLog("publishPage", "Error: iWindowcount > 2. Do code to handle it",Status.FAIL);
//	    	switchToPage("AEM Sites");
//	    }
//	    /*Boolean bb = isElementVisible("ActionPublish", "Publish Icon",10);
//	    if(!bb){
//	    	bPageSelected = selectCreatedPage(pageTitle);//////////////////////////////////////////////
//	    }*/
//	    String strPageLocation = properties.getProperty("NavigateMenu");
// 		System.out.println("strPageLocation: " + strPageLocation);
//	    bPageSelected = selectCreatedPage(strPageLocation,pageTitle);//////////////////////////////////////////////
//	    if(bPageSelected){
//	    	System.out.println("Page Selected");
//	    	handleCoralDialogError();	////////////////////////////////handleCoralDialogError///////////////////////////////
//		    if(isElementVisible("ActionPublish", "Publish Icon")){
//		    	waitFor(1000);
//		    	report.updateTestLog("publishPageFromAEMsite", "Publish the Page: "+ pageTitle, Status.SCREENSHOT);
//	    		javaScriptClick("ActionPublish", "Publish Icon");
//	    		By byCoralAlert = By.xpath("//coral-alert");
//	    		if(!isElementPresent(byCoralAlert, "Coral Alert",50)){
//	    			By byCoralPrimaryPublish = By.xpath("//coral-panelstack/coral-panel/coral-panel-content/div/button/coral-button-label[text()='Publish]");
//		    		if(isElementNotVisible(byCoralPrimaryPublish,"byCoralPrimaryPublish",10)){
//		    			//html/body/form/div/div/coral-panelstack/coral-panel/coral-panel-content/div/button/coral-button-label
//			    		//coral-panelstack/coral-panel/coral-panel-content/div/button/coral-button-label[text()='Publish]
//		    			javaScriptClick(byCoralPrimaryPublish, "Coral Primary Publish Button");
//		    		}
//	    		}else{
//	    			String strAlertTMsg = driver.findElement(byCoralAlert).getText();
//	    			if(strAlertTMsg.contains("The item has been published")){
//	    				report.updateTestLog("publishPage", "Page is published.",Status.PASS);
//	    				bPublish = true;
//	    			}else{
//	    				report.updateTestLog("publishPage", "Page is not published.",Status.FAIL);
//	    			}
//	    			if(isElementPresent(byCoralAlert, "Coral Alert",50)){
//		    			strAlertTMsg = driver.findElement(byCoralAlert).getText();
//		    			if(strAlertTMsg.contains("The item has been published")){
//		    				report.updateTestLog("publishPage", "Page is published.",Status.PASS);
//		    				bPublish = true;
//		    			}else{
//		    				report.updateTestLog("publishPage", "Page is not published.",Status.FAIL);
//		    			}
//		    		}else{
//		    			report.updateTestLog("publishPage", "Alert Message not Found.",Status.FAIL);
//		    		}
//	    		}	    		
//	    	}else{
//				report.updateTestLog("publishPage", "Publish Icon not Present.",Status.FAIL);
//			}
//		    if(bPublish){
//		    	if(iWindowcount == 1){			    	
//			    	driver.navigate().forward();   // Click Forward and do 		    	
//			    }else if(iWindowcount >= 2){
//			    	driver.switchTo().window(availableWindows.get(iWindowcount - 1));    	
//			    }
//		    }
//	    }else{
//	    	frameworkParameters.setStopExecution(true);
//	    }
//	    return bPublish;
//	}
//	
//	/**
//     * Description: Create the publish Url from the Author page url page and save it in the Test Data file.
//     * @throws Exception
//     */	
//	public void createPublishUrl() {
//		String strPageUrl = driver.getCurrentUrl(); // Get Current Author Url
//		//String strPublishUrl = "http://astrazeneca-digital-preprod.adobecqms.net";
//		//Properties properties = Settings.getInstance();	
//		String strPublishUrlWithAuthCreds = null,strPublishUrl = null;
//		if(strPageUrl.contains("preprod")){
//			 strPublishUrlWithAuthCreds = properties.getProperty("PreProd_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
//			 strPublishUrl = properties.getProperty("PreProd_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file//
//		}
////			 else if(strPageUrl.contains("prod")){
////			 strPublishUrlWithAuthCreds = properties.getProperty("Prod_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
////			 strPublishUrl = properties.getProperty("Prod_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file//
////		}
//		
//		
//		else{
//			report.updateTestLog("createPublishUrl", "Unable to identify PreProd or Prod Environment", Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}	
//		
//		//String strPublishUrlWithAuthCreds = properties.getProperty("PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
//		//String strPublishUrl = properties.getProperty("PublishUrl"); // get AEM App Url from Global Properties file//
//		//String strPublishUrl = dataTable.getData("General_Data", "PublishUrl"); // Get Publish page 
//		String[] tempArray = strPageUrl.split("content", 2);	
//		//int arrSize = tempArray.length;	
//		String temp2 = tempArray[1];
//		System.out.println("temp2: " + temp2);
//		tempArray = temp2.split(".html", 2);
//		temp2 = tempArray[0];
//		strPublishUrlWithAuthCreds = strPublishUrlWithAuthCreds + "/content" + temp2 + ".html";
//		strPublishUrl = strPublishUrl + "/content" + temp2 + ".html";
//		System.out.println("strPublishUrl: " + strPublishUrlWithAuthCreds);
//		dataTable.putData("MultiBrand_Data", "Publish_URL", strPublishUrlWithAuthCreds);  ///////////// Save created Publish url with Basic Authentication details added
//		dataTable.putData("MultiBrand_Data", "Edge_Publish_URL", strPublishUrl);  ///////////// Save created Publish url
//		System.out.println("strPublishUrl updated to MultiBrand_Data sheet.");		
//	}
//	/**
//     * Description: Create the publish Url from the Author page url page and save it in the Test Data file.
//     * @throws Exception
//     */	
//	public String createPublishUrl(String strPageUrl,String strEnvironment){
//		String strPublishUrlWithAuthCreds,strPublishUrl;
//		if(strEnvironment.equalsIgnoreCase("Prod")){
//			 strPublishUrlWithAuthCreds = properties.getProperty("Prod_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
//			 strPublishUrl = properties.getProperty("Prod_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file//
//		}else{
//			 strPublishUrlWithAuthCreds = properties.getProperty("PreProd_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
//			 strPublishUrl = properties.getProperty("PreProd_PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file//
//		}		
//		String[] tempArray = strPageUrl.split("content", 2);	
//		//int arrSize = tempArray.length;	
//		String temp2 = tempArray[1];
//		System.out.println("temp2: " + temp2);
//		tempArray = temp2.split(".html", 2);
//		temp2 = tempArray[0];		
//		//System.out.println("strPublishUrl: " + strPublishUrlWithAuthCreds);
//		String strPublishUrl2 = null;
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		
//		if(strBrowserName.equalsIgnoreCase("Chrome") || strBrowserName.equalsIgnoreCase("Firefox")) {
//			strPublishUrl2 = strPublishUrlWithAuthCreds + "/content" + temp2 + ".html";
//		}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER") || strBrowserName.equalsIgnoreCase("Edge")){
//			strPublishUrl2 = strPublishUrl + "/content" + temp2 + ".html";
//		}else{
//			System.out.println("Browser is unknown: " + strBrowserName);
//			report.updateTestLog("createPublishUrl", "Browser is unknown: " + strBrowserName , Status.FAIL);
//		}
//		System.out.println("strPublishUrl: " + strPublishUrl2);
//		return strPublishUrl2;
//			
//		//dataTable.putData("MultiBrand_Data", "Publish_URL", strPublishUrlWithAuthCreds);  ///////////// Save created Publish url with Basic Authentication details added
//		//dataTable.putData("MultiBrand_Data", "Edge_Publish_URL", strPublishUrl);  ///////////// Save created Publish url
//		//System.out.println("strPublishUrl updated to MultiBrand_Data sheet.");		
//	}
//	/**
//     * Description: Create the publish Url from the Author page url page and save it in the Test Data file.
//     * @throws Exception
//     */	
//	public String createPublishUrl(String strPageUrl) {	
//		String strPublishUrlWithAuthCreds = properties.getProperty("PublishUrlWithAuthCreds"); // get AEM App Url from Global Properties file		
//		String strPublishUrl = properties.getProperty("PublishUrl"); // get AEM App Url from Global Properties file//
//		//String strPublishUrl = dataTable.getData("General_Data", "PublishUrl"); // Get Publish page 
//		String[] tempArray = strPageUrl.split("content", 2);	
//		//int arrSize = tempArray.length;	
//		String temp2 = tempArray[1];
//		System.out.println("temp2: " + temp2);
//		tempArray = temp2.split(".html", 2);
//		temp2 = tempArray[0];		
//		//System.out.println("strPublishUrl: " + strPublishUrlWithAuthCreds);
//		String strPublishUrl2 = null;
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		
//		if(strBrowserName.equalsIgnoreCase("Chrome") || strBrowserName.equalsIgnoreCase("Firefox")) {
//			strPublishUrl2 = strPublishUrlWithAuthCreds + "/content" + temp2 + ".html";
//		}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER") || strBrowserName.equalsIgnoreCase("Edge")){
//			strPublishUrl2 = strPublishUrl + "/content" + temp2 + ".html";
//		}else{
//			System.out.println("Browser is unknown: " + strBrowserName);
//			report.updateTestLog("createPublishUrl", "Browser is unknown: " + strBrowserName , Status.FAIL);
//		}
//		System.out.println("strPublishUrl: " + strPublishUrl2);
//		return strPublishUrl2;
//			
//		//dataTable.putData("MultiBrand_Data", "Publish_URL", strPublishUrlWithAuthCreds);  ///////////// Save created Publish url with Basic Authentication details added
//		//dataTable.putData("MultiBrand_Data", "Edge_Publish_URL", strPublishUrl);  ///////////// Save created Publish url
//		//System.out.println("strPublishUrl updated to MultiBrand_Data sheet.");		
//	}
//	/**
//     * Description: Type a text data on the specified Element (Input Box, Text Area).
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Text data to be typed
//     * @throws Exception
//     */
//	public void setData(String strLocator,String strElementDesc, String strInoutData) throws Exception {
//		By by = getLocator(strLocator);
//		setData(by,strElementDesc, strInoutData);
//	}
//	/**
//     * Description: Type a text data on the specified Element (Input Box, Text Area).
//     * @param By byLocator - Locator of the Element in By Class format
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Text data to be typed
//     * @throws Exception
//     */
//	public void setData(By by, String strElementDesc, String strInoutData) throws Exception {
//		System.out.println("Set data: " + strInoutData + ", to Element: " + strElementDesc);
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		
//		if(isElementPresent(by,strElementDesc)){
//			try {
//				if(strInoutData.isEmpty()){
//					driver.findElement(by).clear();
//				}else{
//					driver.findElement(by).clear();
//					driver.findElement(by).sendKeys(strInoutData);
//				}							
//				report.updateTestLog("Set Data", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc , Status.DONE);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				//e.printStackTrace();
//				report.updateTestLog("Set Data", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc  + " - Failed", Status.FAIL);
//			}			
//		}else{
//			report.updateTestLog("Set Data", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	/**
//     * Description: Type a text data on the specified Element (Input Box, Text Area).
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Text data to be typed
//     * @throws Exception
//     */
//	public void setData(WebElement webElement,String strElementDesc, String strInoutData) throws Exception {
//		System.out.println("Set data: " + strInoutData + ", to Element: " + strElementDesc);
//		if(isElementPresent(webElement,strElementDesc)){
//			try {				
//				if(strInoutData.isEmpty()){
//					webElement.clear();
//				}else{
//					webElement.sendKeys(strInoutData);	
//				}
//				report.updateTestLog("Set Data", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc , Status.DONE);
//			} catch (Exception e) {
//				e.printStackTrace();
//				report.updateTestLog("Set Data", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc  + " - Failed", Status.FAIL);
//			}	}else{
//			report.updateTestLog("Set Data", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	/**
//     * Description: Get data from the specified Element.
//     * @param String strLocator - Locator of the Element in String
//     * @param String strElementDesc -  Description of the Element
//     * @param String strAttribute -  Attribute related to the Element
//     * @param return String - return the data got from the Element
//     * @throws Exception
//     */
//	public String getData(String strLocator, String strElementDesc, String strAttribute) throws Exception {
//		System.out.println("Get Attribute: " + strAttribute + " data, from Element: " + strElementDesc);
//		String strAttributeValue = "Null Value";
//		By by = getLocator(strLocator);
//		if(isElementVisible(by,strElementDesc)){
//			if(strAttribute.equalsIgnoreCase("Text")){
//				strAttributeValue = driver.findElement(by).getText().trim();
//			}else{
//				strAttributeValue = driver.findElement(by).getAttribute(strAttribute).trim();
//			}			
//			//report.updateTestLog("getData", strAttributeValue + ", Attribute: " + strAttribute + " data, from Element: " + strElementDesc , Status.DONE);
//		}else{
//			report.updateTestLog("getData", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//		return strAttributeValue;
//	}
//	/**
//     * Description: Get data from the specified Element.
//     * @param By byLocator - Locator of the Element in By Class format
//     * @param String strElementDesc -  Description of the Element
//     * @param String strAttribute -  Attribute related to the Element
//     * @param return String - return the data got from the Element
//     * @throws Exception
//     */
//	public String getData(By by, String strElementDesc, String strAttribute) throws Exception {
//		//System.out.println("Get Attribute: " + strAttribute + " data, from Element: " + strElementDesc);
//		String strAttributeValue = null;
//		if(isElementPresent(by,strElementDesc)){
//			if(strAttribute.equalsIgnoreCase("Text")){
//				strAttributeValue = driver.findElement(by).getText().trim();
//			}else{
//				strAttributeValue = driver.findElement(by).getAttribute(strAttribute).trim();
//			}			
//			//report.updateTestLog("getData", strAttributeValue + ", Attribute: " + strAttribute + " data, from Element: " + strElementDesc , Status.DONE);
//		}else{
//			report.updateTestLog("getData", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//		return strAttributeValue;
//	}	
//	/////////////////////////////  selectData  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	/**
//     * Description: Select an option present from the drop down list of an Element.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Option to be selected.
//     * @throws Exception
//     */
//	public void selectData(WebElement element,String strElementDesc, String strInoutData) throws Exception {
//		//System.out.println("Select data: " + strInoutData + ", to Element: " + strElementDesc);
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		Boolean bOptionSelected = false;
//		//System.out.println("Browser name: " + strBrowserName);
//		Boolean b = isElementPresent(element,strElementDesc);
//		if (b){
//			try{
//				if(strBrowserName.equalsIgnoreCase("Edge")){
//					element.sendKeys(strInoutData);
//					System.out.println(strInoutData + " selected in " + strBrowserName + " Browser");
//					Select dropdown= new Select(element);					
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select
//						bOptionSelected = true;
//					}
//				}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//					Select dropdown= new Select(element);				
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select						
//						bOptionSelected = true;
//					}else{
//						//WebElement select = driver.findElement(by);
//						Actions action = new Actions((WebDriver) driver.getWebDriver());		 
//						action.moveToElement(element).click(element).build().perform();
//						//System.out.println("inside IE new");
//						By byOptionList = By.xpath("//ul[@aria-hidden='false']/li");
//						if(isElementPresent(byOptionList,"byOptionList")){
//							//waitAndHighlightElement(driver.findElement(byOptionList),strElementDesc);
//							List<WebElement> eOptions = driver.getWebDriver().findElements(byOptionList);
//							int iOptionCount = eOptions.size();
//							for(int i=0;i< iOptionCount; i++){
//								//System.out.println(i + " Option: " + eOptions.get(i).getText());
//								if(eOptions.get(i).getText().equalsIgnoreCase(strInoutData)){
//									//eOptions.get(i).click();
//									waitTillElementExist(eOptions.get(i),"strInoutData");									
//									//javaScriptClick(eOptions.get(i),strInoutData);
//									Actions action2 = new Actions((WebDriver) driver.getWebDriver());		 
//									action2.moveToElement(eOptions.get(i)).click(eOptions.get(i)).build().perform();
//									//waitFor(3000);
//									bOptionSelected = true;
//									break;
//								}
//							}
//						}
//						System.out.println(strInoutData + " selected in " + strBrowserName + " Browser");
//						Select dropdown2= new Select(element);				
//						String strDefaultOption2 = dropdown2.getFirstSelectedOption().getText();
//						if(strDefaultOption2.equalsIgnoreCase(strInoutData)){// No option to select
//							bOptionSelected = true;
//						}
//					}
//				}else{
//					Select dropdown= new Select(element);
//					dropdown.selectByVisibleText(strInoutData);
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select
//						bOptionSelected = true;
//					}
//					//System.out.println( "select Data "+ "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc );		    	
//				    //report.updateTestLog("select Data", "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc , Status.DONE);
//				}
//				if(bOptionSelected){
//					System.out.println( "select Data "+ "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc );		    	
//				    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc , Status.DONE);
//				}else{
//					System.out.println( "select Data "+ "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc );		    	
//				    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc , Status.DONE);
//				}
//			}
//			catch (Exception e) {
//			    System.out.println( "select Data "+ "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc );		    	
//			    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc , Status.FAIL);
//			    //e.getStackTrace();	    	
//			}			
//		}else{
//			report.updateTestLog("Select Data", "Element not Present: " + strElementDesc , Status.FAIL);
//		}				
//	}
//	/**
//     * Description: Select an option present from the drop down list of an Element.
//     * @param By byLocator - Locator of the Element in By Class format
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Option to be selected.
//     * @throws Exception
//     */
//	public void selectData(By by,String strElementDesc, String strInoutData) throws Exception {
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		Boolean bOptionSelected = false;
//		System.out.println("strBrowserName: " + strBrowserName);
//		if (isElementVisible(by,strElementDesc)){
//			try{
//				if(strBrowserName.equalsIgnoreCase("Edge")){
//					driver.findElement(by).sendKeys(strInoutData);
//					System.out.println(strInoutData + " selected in " + strBrowserName + " Browser");
//					Select dropdown= new Select(driver.findElement(by));					
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select
//						bOptionSelected = true;
//					}
//				}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//					Select dropdown= new Select(driver.findElement(by));				
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select						
//						bOptionSelected = true;
//					}else{
//						WebElement select = driver.findElement(by);
//						Actions action = new Actions((WebDriver) driver.getWebDriver());		 
//						action.moveToElement(select).click(select).build().perform();
//						//System.out.println("inside IE new");
//						By byOptionList = By.xpath("//ul[@aria-hidden='false']/li");
//						if(isElementPresent(byOptionList,"byOptionList")){
//							//waitAndHighlightElement(driver.findElement(byOptionList),strElementDesc);
//							List<WebElement> eOptions = driver.getWebDriver().findElements(byOptionList);
//							int iOptionCount = eOptions.size();
//							for(int i=0;i< iOptionCount; i++){
//								//System.out.println(i + " Option: " + eOptions.get(i).getText());
//								if(eOptions.get(i).getText().equalsIgnoreCase(strInoutData)){
//									//eOptions.get(i).click();
//									waitTillElementExist(eOptions.get(i),"strInoutData");									
//									//javaScriptClick(eOptions.get(i),strInoutData);
//									Actions action2 = new Actions((WebDriver) driver.getWebDriver());		 
//									action2.moveToElement(eOptions.get(i)).click(eOptions.get(i)).build().perform();
//									//waitFor(3000);
//									bOptionSelected = true;
//									break;
//								}
//							}
//						}
//						System.out.println(strInoutData + " selected in " + strBrowserName + " Browser");
//						Select dropdown2= new Select(driver.findElement(by));					
//						String strDefaultOption2 = dropdown2.getFirstSelectedOption().getText();
//						if(strDefaultOption2.equalsIgnoreCase(strInoutData)){// No option to select
//							bOptionSelected = true;
//						}
//					}
//				}else{					
//					Select dropdown= new Select(driver.findElement(by));				
//					String strDefaultOption = dropdown.getFirstSelectedOption().getText();
//					if(strDefaultOption.equalsIgnoreCase(strInoutData)){// No option to select						
//						bOptionSelected = true;
//					}else{						
//						List<WebElement> eOptions = dropdown.getOptions();
//						int iOptionCount = dropdown.getOptions().size();
//						for(int i=0;i< iOptionCount; i++){
//							if(eOptions.get(i).getText().equalsIgnoreCase(strInoutData)){
//								dropdown.selectByVisibleText(eOptions.get(i).getText());
//								bOptionSelected = true;
//								break;
//							}
//						}																	
//					}					
//				}
//				if(bOptionSelected){
//					System.out.println( "select Data "+ "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc );		    	
//				    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - selected from the Drop Down List: " + strElementDesc , Status.DONE);
//				}else{
//					System.out.println( "select Data "+ "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc );		    	
//				    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - not present in the Drop Down List: " + strElementDesc , Status.DONE);
//				}
//			}
//			catch (Exception e) {
//			    System.out.println( "select Data "+ "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc );		    	
//			    report.updateTestLog("select Data", "Option: (" + strInoutData + ") - not available in the Drop Down List: " + strElementDesc , Status.FAIL);    	
//			}			
//		}else{
//			report.updateTestLog("Select Data", "Element not Present: " + strElementDesc , Status.FAIL);
//		}				
//	}
//	/**
//     * Description: Select an option present from the drop down list of an Element.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param String strInoutData -  Option to be selected.
//     * @throws Exception
//     */
//	public void selectData(String strLocator,String strElementDesc, String strInoutData) throws Exception {		
//		By by = getLocator(strLocator);
//		selectData(by,strElementDesc, strInoutData);
//	}
//	/**
//     * Description: wait Till Element Exist.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void waitTillElementExist(String strLocator, String strElementDesc) throws Exception{
//		By by = getLocator(strLocator);
//		waitTillElementExist(by,strElementDesc);
//	}
//	/**
//     * Description: wait Till Element Exist.
//     * @param By byLocator - Locator of the Element in By Class format
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void waitTillElementExist(By by, String strElementDesc) throws Exception{
//		//int iWaitSeconds = 20;
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));		
//		boolean bElement = isElementPresent(by,strElementDesc,iWaitSeconds);		
//		if(bElement){
//			System.out.println( "WebElement Found : " + strElementDesc);
//			//report.updateTestLog("waitTillElementExist", "WebElement Found : " + strElementDesc , Status.DONE);	
//		}else{
//			System.out.println( "WebElement not Found : " + strElementDesc);
//			report.updateTestLog("waitTillElementExist", "WebElement not Found : " + strElementDesc , Status.FAIL);
//		}
//	}
//	/**
//     * Description: wait Till Element Exist.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void waitTillElementExist(WebElement element, String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		boolean bElement = isElementPresent(element,strElementDesc,iWaitSeconds);		
//		if(bElement){
//			System.out.println( "WebElement Found : " + strElementDesc);
//			//report.updateTestLog("waitTillElementExist", "WebElement Found : " + strElementDesc , Status.DONE);	
//		}else{
//			System.out.println( "WebElement not Found : " + strElementDesc);
//			report.updateTestLog("waitTillElementExist", "WebElement not Found : " + strElementDesc , Status.FAIL);
//		}		
//	}
//	
//	
//		
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */		
//	public boolean isElementPresent(String strLocator,String strElementDesc) throws Exception{
//		//String strLocator2 = ObjectRepositoryproperties.getProperty(strLocator);
//		//System.out.println("isElementPresent(String,String) called");
//		By by = getLocator(strLocator);
//		return isElementPresent(by,strElementDesc);			
//	}
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementPresent(String strLocator,String strElementDesc, int iWaitSeconds) throws Exception{
//		By by = getLocator(strLocator);
//		return isElementPresent(by,strElementDesc,iWaitSeconds);			
//	}
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementPresent(By by,String strElementDesc) throws Exception{
//		//System.out.println("isElementPresent(By,String) called");
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));		
//		return isElementPresent(by,strElementDesc,iWaitSeconds);		
//	}
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementPresent(By by,String strElementDesc,int iWaitSeconds) throws Exception{
//		Boolean bElement = false;
//		//System.out.println("isElementPresent(By,String,int) called");
//		int iTryCount = 2;
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.presenceOfElementLocated(by));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - waitTillElementExist : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println("NoSuchElementException - WebElement not present : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println("Exception - WebElement not present : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}
//		if(bElement){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementPresent(WebElement element,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		return isElementPresent(element,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: Check whether the given element is present or not in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementPresent(WebElement element,String strElementDesc, int iWaitSeconds) throws Exception{			
//		Boolean bElement = false;
//		int iTryCount = iWaitSeconds;
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=1; i<=iTryCount;i++){
//            try {
//            	if(element.isDisplayed() || element.isEnabled()){
//    				bElement = true;
//    				break;
//    			}else{
//    				waitFor(1000);
//    			}
//                			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - isElementPresent : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println(i + " NoSuchElementException - WebElement not present : " + strElementDesc);
//		    	//i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println(i+ " Exception - WebElement not present : " + strElementDesc);
//		    	//i = iTryCount;
//			}
//		}
//		if(bElement){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}
//	public boolean isElementPresent_Old(WebElement element,String strElementDesc, int iWaitSeconds) throws Exception{			
//		Boolean bElement = false;
//		int iTryCount = 2;
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.visibilityOf(element));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - isElementPresent : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println("NoSuchElementException - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println("Exception - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}
//		if(!bElement){
//			if(element.isDisplayed() || element.isEnabled()){
//				bElement = true;
//			}			
//		}		
//		if(bElement){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is clickable or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementClickable(By by,String strElementDesc) throws Exception{
//		Boolean bElement = false;
//		int iTryCount = 2;
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.presenceOfElementLocated(by));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - waitTillElementExist : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println("NoSuchElementException - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println("Exception - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}		
//		if(bElement){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(String strLocator,String strElementDesc) throws Exception{
//		By byLocator = getLocator(strLocator);
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		return isElementVisible(byLocator,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(String strLocator,String strElementDesc,int iWaitSeconds ) throws Exception{
//		By byLocator = getLocator(strLocator);
//		return isElementVisible(byLocator,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(By by,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		return isElementVisible(by,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(By by,String strElementDesc,int iWaitSeconds) throws Exception{
//		Boolean bElement = false;
//		int iTryCount = 2;
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.visibilityOfElementLocated(by));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e){ 
//              System.out.println( i + " - waitTillElementExist : " + strElementDesc);
//            }
//			catch (NoSuchElementException e){
//		    	System.out.println("NoSuchElementException - WebElement not Visible : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e){
//		    	System.out.println("Exception - WebElement not Visible : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}		
//		if(bElement){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(WebElement element,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		return isElementVisible(element,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if present else returns False
//     * @throws Exception
//     */	
//	public boolean isElementVisible(WebElement element,String strElementDesc,int iWaitSeconds){
//		Boolean bElement = false;
//		int iTryCount = 2;
//		//System.out.println( "Is Element Visible:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.visibilityOf(element));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - waitTillElementExist : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println("NoSuchElementException - WebElement not Visible : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println("Exception - WebElement not Visible : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}		
//		if(bElement){
//			return true;
//		}
//		else{
//			System.out.println( "WebElement not Visible : " + strElementDesc);
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if not visible else returns False
//     * @throws Exception
//     */	
//	public boolean isElementNotVisible(String strLocator,String strElementDesc) throws Exception{	
//		By byLocator = By.xpath(strLocator);
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean b = isElementNotVisible(byLocator,strElementDesc,iWaitSeconds);
//		return b;
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if not visible else returns False
//     * @throws Exception
//     */	
//	public boolean isElementNotVisible(String strLocator,String strElementDesc, int iWaitSeconds) throws Exception{	
//		By byLocator = By.xpath(strLocator);
//		Boolean b = isElementNotVisible(byLocator,strElementDesc,iWaitSeconds);
//		return b;
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if not visible else returns False
//     * @throws Exception
//     */
//	public boolean isElementNotVisible(By byLocator,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean b = isElementNotVisible(byLocator,strElementDesc,iWaitSeconds);
//		return b;
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * return Boolean - returns True, if not visible else returns False
//     * @throws Exception
//     */
//	public boolean isElementNotVisible(By byLocator,String strElementDesc,int iWaitSeconds) throws Exception{			
//		Boolean bElement = false;
//		int iTryCount = 2;
//		//System.out.println( "Is Element Present:  " + strElementDesc);
//		for(int i=0; i<iTryCount;i++){
//            try {
//            	WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitSeconds);
//            	wait.until(ExpectedConditions.invisibilityOfElementLocated(byLocator));
//            	bElement = true;
//                break;			                
//            } catch(StaleElementReferenceException e) { 
//              System.out.println( i + " - isElementPresent : " + strElementDesc);
//            }
//			catch (NoSuchElementException e) {
//		    	System.out.println("NoSuchElementException - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//		    }
//			catch (Exception e) {
//		    	System.out.println("Exception - WebElement not Found : " + strElementDesc);
//		    	i = iTryCount;
//			}
//		}
//		if(bElement){
//			//report.updateTestLog("isElementPresent", "WebElement Present : " + strElementDesc , Status.DONE);
//			return true;
//		}
//		else{
//			return false;
//		}
//	}
//	/**
//     * Description: Check whether the given element is visible or not in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * return Boolean - returns True, if not visible else returns False
//     * @throws Exception
//     */
//	public boolean isElementNotVisible(WebElement element,String strElementDesc){
//		Boolean bNotVisible = false;
//		int iWaitSeconds = 1;
//		int iTryCount = 50;
//		for(int i=1; i<= iTryCount; i++){
//			Boolean b = isElementVisible(element,strElementDesc,iWaitSeconds);
//			if(!b){
//				bNotVisible = true;
//				break;
//			}
//		}
//		return bNotVisible;
//	}
//	/**
//     * Description: Get the locator of the Element from the ObjectMap.properties file.
//     * @param String strLocator - Locator of the Element
//     * return By - returns the locator of the Element in By class reference.
//     * @throws Exception
//     */
//	public By getLocator(String strLocator) throws Exception {
//		// extract the locator type and value from the object
//		Boolean bFound = true;
//		String strExactLocator = null;
//		try {
//			strExactLocator = ObjectRepositoryproperties.getProperty(strLocator);
//		} catch (NullPointerException e) {
//			// TODO Auto-generated catch block
//			//e.printStackTrace();			
//			bFound = false;
//			report.updateTestLog("getLocator",strLocator + " not present in ObjectRepository.",Status.FAIL);
//		}catch (Exception e) {
//			report.updateTestLog("getLocator",e.getMessage(),Status.FAIL);
//		}
//		//System.out.println("strExactLocator: " + strExactLocator);
//
//		if(bFound && strExactLocator.contains(":")){
//			String locatorType = strExactLocator.split(":",2)[0];
//			String locatorValue = strExactLocator.split(":",2)[1];
//			if (locatorType.toLowerCase().equals("xpath"))
//				return By.xpath(locatorValue);
//			else if (locatorType.toLowerCase().equals("id"))
//				return By.id(locatorValue);
//			else if (locatorType.toLowerCase().equals("name"))
//				return By.name(locatorValue);
//			else if ((locatorType.toLowerCase().equals("classname"))
//					|| (locatorType.toLowerCase().equals("class")))
//				return By.className(locatorValue);
//			else if ((locatorType.toLowerCase().equals("tagname"))
//					|| (locatorType.toLowerCase().equals("tag")))
//				return By.className(locatorValue);
//			else if ((locatorType.toLowerCase().equals("linktext"))
//					|| (locatorType.toLowerCase().equals("link")))
//				return By.linkText(locatorValue);
//			else if (locatorType.toLowerCase().equals("partiallinktext"))
//				return By.partialLinkText(locatorValue);
//			else if ((locatorType.toLowerCase().equals("cssselector"))
//					|| (locatorType.toLowerCase().equals("css")))
//				return By.cssSelector(locatorValue);
//			else
//				throw new Exception("Unknown locator type '" + locatorType + "'");
//		}else{
//			System.out.println( strLocator + " is not the Expected Format (locatorType:locatorValue");
//			report.updateTestLog("strLocator: ", strLocator + " is not the Expected Format (locatorType:locatorValue", Status.FAIL);
//			return null;
//		}			
//	}
//	/**
//     * Description: Get the XPath locator of the Element from the ObjectMap.properties file.
//     * @param String strLocator -  Locator of the Element in String.
//     * @return String - returns the XPath locator of the Element in String
//     * @throws Exception
//     */
// 	public String getXpathFromRepository(String strLocator) throws Exception{
// 		String str1 = ObjectRepositoryproperties.getProperty(strLocator);
// 		String strXpath = null;
// 		//System.out.println("str1:" + str1);
//		if(str1.contains("xpath:")){
//			String[] strArray = str1.split(":", 2);
//			strXpath = strArray[1];
//		}else{
//			report.updateTestLog("getLocatorPath", "getLocatorPath: " + strLocator + " - Not a XPATH Locator or Not have splitter - ':'", Status.FAIL);	
//		}
//		return strXpath;
// 	}
//	/**
//     * Description: verify Actual and Expected Data
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strActualData - Actual Data present in the Application page
//     * @param String strExpectedData - Actual Data present in the Application page
//	 * @return 
//     * @throws Exception
//     */
//	public Boolean verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData){
//		//Verify 
//		strActualData = strActualData.trim();
//		strExpectedData = strExpectedData.trim();
//		if(strActualData.equalsIgnoreCase(strExpectedData)){
//			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//					") matches with Expected Value: (" + strExpectedData + ")");
//			report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//					") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);					
//		}else{
//			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//					") not matches with Expected Value: ("+ strExpectedData + ")");
//			report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//					") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//		}		
//	}
//	/**
//     * Description: verify Actual and Expected Data
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strActualData - Actual Data present in the Application page
//     * @param String strExpectedData - Actual Data present in the Application page
//     * @param String strVerifyOption - Verify Options (Exact,Partial)
//     * @throws Exception
//     */
//	public void verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData,String strVerifyOption) throws Exception{
//		//Verify
//		if(strVerifyOption.equalsIgnoreCase("Exact")){
//			if(strActualData.equals(strExpectedData)){
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") matches with Expected Value: (" + strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);					
//			}else{
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") not matches with Expected Value: ("+ strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//			}
//		}else if(strVerifyOption.equalsIgnoreCase("Partial")){
//			if(strActualData.contains(strExpectedData)){
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") matches with Expected Value: (" + strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);					
//			}else{
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") not matches with Expected Value: ("+ strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//			}
//		}else
//			throw new Exception("Unknown Verify Option type '" + strVerifyOption + "'.Verify Option type allowed are 'Exact' or 'Partial'.");
//	}
//	/**
//     * Description: verify Actual and Expected Data
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strActualData - Actual Data present in the Application page
//     * @param String strExpectedData - Actual Data present in the Application page
//     * @param String strVerifyOption - Verify Options (Exact,Partial)
//     * @param String strTakeScreenshotOnPass - TakeScreenshotOnPass  (Yes,No). Yes - Take Screenshot on Pass condition
//	 * @return 
//     * @throws Exception
//     */
//	public void verifyActualExpectedData3(String strVerifyElementDesc, String strActualData, String strExpectedData1, String strExpectedData2,String strVerifyOption,String strTakeScreenshotOnPass) throws Exception{
//		//Verify
//		if(verifyActualExpectedData2(strVerifyElementDesc, strActualData, strExpectedData1,strVerifyOption,strTakeScreenshotOnPass)){
//			if(strTakeScreenshotOnPass.equalsIgnoreCase("Yes")){
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") matches with Expected Value: (" + strExpectedData1 + ")", Status.PASS);
//			}else{
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") matches with Expected Value: (" + strExpectedData1 + ")", Status.DONE);
//			}
//		}else{
//			if(verifyActualExpectedData2(strVerifyElementDesc, strActualData, strExpectedData2,strVerifyOption,strTakeScreenshotOnPass)){
//				if(strTakeScreenshotOnPass.equalsIgnoreCase("Yes")){
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData2 + ")", Status.PASS);
//				}else{
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData2 + ")", Status.DONE);
//				}
//			}
//		}
//
//	}
//	public Boolean verifyActualExpectedData2(String strVerifyElementDesc, String strActualData, String strExpectedData,String strVerifyOption,String strTakeScreenshotOnPass) throws Exception{
//		//Verify
//		
//		Boolean bVerify = false;
//		if(strVerifyOption.equalsIgnoreCase("Exact")){
//			if(strActualData.equals(strExpectedData)){
//				bVerify = true;									
//			}
//		}else if(strVerifyOption.equalsIgnoreCase("Partial")){	
//			if(strExpectedData != null && strActualData.contains(strExpectedData)){
//				bVerify = true;								
//			}
//		}else{
//			System.out.println("Unknown Verify Option type");
//			throw new Exception("Unknown Verify Option type '" + strVerifyOption + "'.Verify Option type allowed are 'Exact' or 'Partial'.");
//			}
//		return bVerify;
//	}
//	public void verifyActualExpectedData(String strVerifyElementDesc, String strActualData, String strExpectedData,String strVerifyOption,String strTakeScreenshotOnPass) throws Exception{
//		//Verify		
//		if(strVerifyOption.equalsIgnoreCase("Exact") || strVerifyOption.equalsIgnoreCase("Equal")
//				|| strVerifyOption.equalsIgnoreCase("Equals")){
//			if(strActualData.equals(strExpectedData)){
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") matches with Expected Value: (" + strExpectedData + ")");
//				if(strTakeScreenshotOnPass.equalsIgnoreCase("Yes")){
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);
//				}else{
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.DONE);
//				}									
//			}else{
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") not matches with Expected Value: ("+ strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//			}
//		}else if(strVerifyOption.equalsIgnoreCase("equalsIgnoreCase")){
//			if(strActualData.equalsIgnoreCase(strExpectedData)){
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") matches with Expected Value: (" + strExpectedData + ")");
//				if(strTakeScreenshotOnPass.equalsIgnoreCase("Yes")){
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);
//				}else{
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.DONE);
//				}									
//			}else{
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") not matches with Expected Value: ("+ strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//			}
//		}else if(strVerifyOption.equalsIgnoreCase("Partial")){
//			if(strExpectedData != null && strActualData.contains(strExpectedData)){
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") matches with Expected Value: (" + strExpectedData + ")");
//				if(strTakeScreenshotOnPass.equalsIgnoreCase("Yes")){
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.PASS);
//				}else{
//					report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//							") matches with Expected Value: (" + strExpectedData + ")", Status.DONE);
//				}									
//			}else{
//				System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + strActualData +
//						") not matches with Expected Value: ("+ strExpectedData + ")");
//				report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + strActualData + 
//						") not matches with Expected Value: (" + strExpectedData + ")", Status.FAIL);			
//			}
//		}else
//			throw new Exception("Unknown Verify Option type '" + strVerifyOption + "'.Verify Option type allowed are 'Exact','Equal',Equals or 'Partial'.");
//	}
//	/**
//     * Description: verify Actual and Expected data of numbers
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String iActualData - Actual Data present in the Application page
//     * @param String iExpectedData - Actual Data present in the Application page
//     * @throws Exception
//     */
//	public void verifyActualExpectedNumber(String strVerifyElementDesc, int iActualData, int iExpectedData){
//		//Verify 
//		if(iActualData == iExpectedData){
//			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + iActualData +
//					") matches with Expected Value: (" + iExpectedData + ")");
//			report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + iActualData + 
//					") matches with Expected Value: (" + iExpectedData + ")", Status.PASS);					
//		}else{
//			System.out.println("Verify Element: " + strVerifyElementDesc + ", Actual Value: (" + iActualData +
//					") not matches with Expected Value: ("+ iExpectedData + ")");
//			report.updateTestLog("Verify Element" , strVerifyElementDesc + ", Actual Value: (" + iActualData + 
//					") not matches with Expected Value: (" + iExpectedData + ")", Status.FAIL);			
//		}		
//	}
//	/**
//     * Description: wait and HighLight the Element present in the Current page.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */	
//	public void waitAndHighLightElement(String strLocator, String strElementDesc) throws Exception{			
//		By by = getLocator(strLocator);
//		waitAndHighLightElement(by,strElementDesc);
//	}
//	/**
//     * Description: wait and HighLight the Element present in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * @throws Exception
//     */	
//	public void waitAndHighLightElement(By by, String strElementDesc,int iWaitSeconds) throws Exception{
//		Boolean bElementPresent = isElementVisible(by,strElementDesc,iWaitSeconds);
//		if(bElementPresent){
//			highLightElement(driver.findElement(by));
//		}					
//	}
//	/**
//     * Description: wait and HighLight the Element present in the Current page.
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */	
//	public void waitAndHighLightElement(By by, String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		waitAndHighLightElement(by,strElementDesc,iWaitSeconds);
//	}
//	/**
//     * Description: wait and HighLight the Element present in the Current page.
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void waitAndHighlightElement(WebElement element, String strElementDesc) throws Exception{
//		Boolean bElementPresent = isElementVisible(element,strElementDesc);
//		if(bElementPresent){
//			highLightElement(element);
//		}else{
//			System.out.println( "WebElement not Present : " + strElementDesc);
//			report.updateTestLog("waitAndHighLightElement", "WebElement not Present : " + strElementDesc , Status.FAIL);	
//		}
//	}
//
//// Element highlight WebElement
//	/**
//     * Description: highLight Element present in the Current page.
//     * @param WebElement element - Element present in the page
//     */
//	public void highLightElement(WebElement element){
//		JavascriptExecutor js=(JavascriptExecutor)(WebDriver) driver.getWebDriver();
//		WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),20);
//		wait.until(ExpectedConditions.visibilityOf(element));
//		for (int i = 0; i <1; i++) {
//	    	js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 6px solid red;");
//	        waitFor(300);
//	        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
//	        waitFor(300);
//	        }
//		try 
//		{
//		Thread.sleep(1000);
//		} 
//		catch (InterruptedException e) {		 
//			System.out.println(e.getMessage());
//		}  
//	}
//	/**
//     * Description: highLight Element present in the Current page.
//     * @param WebElement element - Element present in the page
//	 * @throws Exception 
//     */
//	public void highLightElement(String strLocator, String strElementDesc) throws Exception{
//		if(isElementVisible(strLocator, strElementDesc)){
//			By by = getLocator(strLocator);
//			highLightElement(by, strElementDesc);
//		} 
//	}
//	public void highLightElement(By byLocator, String strElementDesc) throws Exception{
//		if(isElementVisible(byLocator, strElementDesc)){
//			WebElement element = driver.findElement(byLocator);
//			JavascriptExecutor js=(JavascriptExecutor)(WebDriver) driver.getWebDriver();
//			for (int i = 0; i <1; i++) {
//		    	js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 6px solid red;");
//		        waitFor(300);
//		        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
//		        waitFor(1000);
//		        }
//		}
//		try 
//		{
//		Thread.sleep(1000);
//		} 
//		catch (InterruptedException e) {		 
//			System.out.println(e.getMessage());
//		}  
//	}
//	/**
//     * Description: Pause the Test Execution for a specified amount of time
//     * @param long milliSeconds - wait time in millseconds
//     */
//	public void waitFor(long milliSeconds) {
//		try {
//			Thread.sleep(milliSeconds);
//			} 
//		catch (InterruptedException e) 
//		{
//			report.updateTestLog("Wait For", "Wait For "+milliSeconds/1000+" seconds",Status.FAIL);
//			e.printStackTrace();
//		}
//	}
//	/**
//     * Description: verify Tooltip Data for an element present in the AEM Application
//     * 
//     */
//	public void verifyTooltipData() throws Exception{
//		String strEnable_Tooltip = dataTable.getData("MultiBrand_Data", "Enable_Tooltip");
//		String strTooltip_Title = dataTable.getData("MultiBrand_Data", "Tooltip_Title");
//		String strTooltip_Content = dataTable.getData("MultiBrand_Data", "Tooltip_Content");			
//		////////////////////////////////////////////////
//		if(strEnable_Tooltip.equalsIgnoreCase("YES")){  // Tool tip Icon should Present				
//			javaScriptClick("AEM.TitleTooltipIcon","Title - Tooltip Icon");
//			By byActualTooltip_Title = getLocator("AEM.TitleTooltipTitle");
//			waitAndHighLightElement(byActualTooltip_Title,"Title - Tooltip Title");
//			String strActualTooltip_Title = driver.findElement(byActualTooltip_Title).getText();
//			System.out.println( "Actual Tooltip_Title : " + strActualTooltip_Title); 
//			By byActualTooltip_Content = getLocator("AEM.TitleTooltipContent");
//			waitAndHighLightElement(byActualTooltip_Content,"Title - Tooltip Content");
//			String strActualTooltip_Content = driver.findElement(byActualTooltip_Content).getText();
//			System.out.println( "Actual Tooltip_Content : " + strActualTooltip_Content); 
//			//Verify Tooltip Title
//			verifyActualExpectedData("Tooltip Title", strActualTooltip_Title, strTooltip_Title);
//			//Verify Tooltip Content
//			verifyActualExpectedData("Tooltip Content", strActualTooltip_Content, strTooltip_Content);			
//		}else{  // Tool tip Icon should not Present
//			By byTitleTooltipIcon =  getLocator("AEM.TitleTooltipIcon");
//			Boolean b = isElementVisible(byTitleTooltipIcon,"Tool Tip Icon");
//			if(b){
//				System.out.println("Verify Element: " + " Sub Heading - Tooltip Icon is Present" );
//				report.updateTestLog("Verify Element" , "Sub Heading - Tooltip Icon is Present" , Status.FAIL);
//			}else{
//				System.out.println("Verify Element: " + " Sub Heading - Tooltip Icon is not Present" );
//				report.updateTestLog("Verify Element" , "Sub Heading - Tooltip Icon is not Present" , Status.PASS);
//			}
//		}
//	}
//	/**
//     * Description: verify Element Visible or not.
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public Boolean verifyElementVisible(String strLocator,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean b= verifyElementVisible(strLocator,strElementDesc,iWaitSeconds);
//		return b;
//	}
//	public Boolean verifyElementVisible(String strLocator,String strElementDesc, int iWaitSeconds) throws Exception{
//		By by = getLocator(strLocator);
//		Boolean bElement = verifyElementVisible(by,strElementDesc,iWaitSeconds);
//		return bElement;
//	}
//	public Boolean verifyElementVisible(By by,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean bElement = verifyElementVisible(by,strElementDesc,iWaitSeconds);
//		return bElement;
//	}
//	/**
//     * Description: verify Element Visible or not.
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * @throws Exception
//     */
//	public Boolean verifyElementVisible(By by,String strElementDesc, int iWaitSeconds) throws Exception{
//		Boolean bElement = isElementVisible(by,strElementDesc,iWaitSeconds);
//		if (bElement){
//			System.out.println("Element Visible: " + strElementDesc );
//			report.updateTestLog("Verify Element Visible" ,"Element Visible: " + strElementDesc + " as Expected." , Status.PASS);
//			
//		}else{
//			System.out.println("Element not Visible: " + strElementDesc );
//			report.updateTestLog("Verify Element Visible" , "Element Visible: " + strElementDesc , Status.FAIL);
//		}
//		return bElement;
//	}
//	/**
//     * Description: verify Element Present or not.
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @param int iWaitSeconds - wait time in seconds
//     * @throws Exception
//     */
//	public Boolean verifyElementPresent(By by,String strElementDesc, int iWaitSeconds) throws Exception{
//		Boolean bElement = isElementPresent(by,strElementDesc,iWaitSeconds);
//		if (bElement){
//			System.out.println("Element Present: " + strElementDesc );
//			report.updateTestLog("Verify Element Present" ,"Element Present: " + strElementDesc + " as Expected." , Status.PASS);
//			
//		}else{
//			System.out.println("Element not Present: " + strElementDesc );
//			report.updateTestLog("Verify Element Present" , "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//		return bElement;
//	}
//	/**
//     * Description: verify Element Visible or not.
//     * @param String strVerifyElementDesc - Description of the Element to be verified.
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public Boolean verifyElementPresent(String strLocator,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean b= verifyElementPresent(strLocator,strElementDesc,iWaitSeconds);
//		return b;
//	}
//	public Boolean verifyElementPresent(String strLocator,String strElementDesc, int iWaitSeconds) throws Exception{
//		By by = getLocator(strLocator);
//		Boolean bElement = verifyElementPresent(by,strElementDesc,iWaitSeconds);
//		return bElement;
//	}
//	public Boolean verifyElementPresent(By by,String strElementDesc) throws Exception{
//		int iWaitSeconds = Integer.parseInt(properties.getProperty("waitTime"));
//		Boolean bElement = verifyElementPresent(by,strElementDesc,iWaitSeconds);
//		return bElement;
//	}
//
//
//	/**
//	 * Function to handle Authentication in IE Browser and Chrome Browser using AutoIT Tool
//	 * number and column header
//	 * 
//	 * @param NA
//	 * @return NA
//	 */
//	/**
//     * Description: handle Authentication pop present in the Publish page.
//     * 
//     */
//	public void handleAuthentication(String strAuthenticatePopUp){
//		//sendKeysRobot();
//		//Browser b = null;
//		//String strDownloadPath = properties.getProperty("AuthenticatePopUp");
//		if(strAuthenticatePopUp.equalsIgnoreCase("Yes")){
//			String strBrowserName = null;
//			try {
//				//Runtime.getRuntime().exec("C:/Users/kdsf308/Documents/basicAuthChromeMB.exe");
//				SeleniumTestParameters t = driver.getTestParameters();
//				strBrowserName = t.getBrowser().toString();
//				System.out.println("Browser name: " + strBrowserName);
//				if(strBrowserName.equalsIgnoreCase("Chrome")){/////////////////////////////////////
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthChromeMB.exe");
//				}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//					System.out.println("IE Browser - HandleAuth Started");
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthIE2.exe");
//					System.out.println("IE Browser - HandleAuth done");
//				}else if(strBrowserName.equalsIgnoreCase("Edge")){
//					System.out.println("Edge Browser");
//					/*
//					By byInvalid = By.xpath("//id[@id='invalidcert_continue']");
//					if(isElementVisible(byInvalid,"Link - Continue to this webpage (not recommended)")){
//						javaScriptClick(byInvalid,"Link - Continue to this webpage (not recommended)");
//						waitFor(3000);
//					}*/
//					//Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthEdge.exe");
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthIE2.exe");
//				}								
//			} catch (Exception e) {
//				System.out.println("Error in Browser: " + strBrowserName);
//				e.printStackTrace();				
//			}
//		}
//		
//		//waitFor(10000);
//	}
//	
//	public void handleAuthentication(){
//		//sendKeysRobot();
//		//Browser b = null;
//		String strAuthenticatePopUp = properties.getProperty("AuthenticatePopUp");
//		if(strAuthenticatePopUp.equalsIgnoreCase("Yes")){
//			String strBrowserName = null;
//			try {
//				//Runtime.getRuntime().exec("C:/Users/kdsf308/Documents/basicAuthChromeMB.exe");
//				SeleniumTestParameters t = driver.getTestParameters();
//				strBrowserName = t.getBrowser().toString();
//				System.out.println("Browser name: " + strBrowserName);
//				if(strBrowserName.equalsIgnoreCase("Chrome")){/////////////////////////////////////
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthChromeMB.exe");
//				}else if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//					System.out.println("IE Browser - HandleAuth Started");
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthIE2.exe");
//					System.out.println("IE Browser - HandleAuth done");
//				}else if(strBrowserName.equalsIgnoreCase("Edge")){
//					System.out.println("Edge Browser");
//					/*
//					By byInvalid = By.xpath("//id[@id='invalidcert_continue']");
//					if(isElementVisible(byInvalid,"Link - Continue to this webpage (not recommended)")){
//						javaScriptClick(byInvalid,"Link - Continue to this webpage (not recommended)");
//						waitFor(3000);
//					}*/
//					//Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthEdge.exe");
//					Runtime.getRuntime().exec("C:/workspace/Multi_Brand_Automation/AutoIT/basicAuthIE2.exe");
//				}								
//			} catch (Exception e) {
//				System.out.println("Error in Browser: " + strBrowserName);
//				e.printStackTrace();				
//			}
//		}
//		
//		//waitFor(10000);
//	}
//	
//
//	
//	public By selectDragComponentForTemplate(String strTemplate) throws Exception{
//		By byDragComponent = null;
//		switch(strTemplate.toLowerCase()){
//		case "generic page template":
//			byDragComponent = getLocator("Content_DragComponent");
//			break;
//		case "full bleed page template":			
//			//byDragComponent = getLocator("Content_DragComponent");
//			byDragComponent = getLocator("FullBleed_Container_DragComponent");			
//			break;
//		case "exit ramp page template":
//			//Not Required
//			break;
//		case "self certification page template":
//			byDragComponent = getLocator("SelfCertification_DragComponent");
//			break;
//		case "generic snippet page template":
//			byDragComponent = getLocator("Popup_Snippet_DragComponent");
//			break;
//		case "home page template":
//			byDragComponent = getLocator("Content_DragComponent");
//			break;
//		default:
//			byDragComponent = getLocator("Content_DragComponent");
//		}
//		return byDragComponent;
//	}
//	
//	/**
//     * Description: add Component to the AEM Edit Page
//     * @param String strComponentName - Component name to be added
//     * @throws Exception
//     */
//	public Boolean addComponent22222(String strComponentName) throws Exception{
//		//String strLocator = "//div[@id='OverlayWrapper']//div[@title='Drag components here']";			
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strPageTemplate = dataTable.getData("MultiBrand_Data", "Page_Template");
//		//String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		By byDragComponentsHere = null;
//		if(strExecutionMode.equalsIgnoreCase("LOCAL") && strBrowserName.equalsIgnoreCase("EDGE")){
//			if(strPageTemplate.equalsIgnoreCase("Product Page template")){
//				//String strLocator = "//div[@id='OverlayWrapper']//div[@title='Drag components here']";
//				byDragComponentsHere =getLocator("Edge_ProductTemplate_DragComponent");
//			}else{
//				System.out.println("Generic page template");
//				for(int i=4; i<= 9; i++){
//					byDragComponentsHere = By.xpath("//*[@id='OverlayWrapper']/div[" + i + "]/div/div");
//					if(!isElementNotVisible(byDragComponentsHere,"Drag Components Here")){
//						///
//						System.out.println("//*[@id='OverlayWrapper']/div[" + i + "]/div/div");
//						break;
//					}
//				}
//				//byDragComponentsHere = By.xpath("//*[@id='OverlayWrapper']/div[9]/div/div");
//				//byDragComponentsHere =getLocator("Edge_GenericTemplate_DragComponent");
//				//byDragComponentsHere = By.className("cq-Overlay cq-Overlay--component cq-droptarget cq-Overlay--placeholder");
//			}				
//		}else{
//			byDragComponentsHere =getLocator("DragComponent");
//			//byDragComponentsHere = By.xpath("//div[@id='OverlayWrapper']//div[@title='Drag components here']");
//		}
//		
//		//byDragComponentsHere = selectDragComponentForTemplate(strPageTemplate);/////////////////////////////////////////////////////////////////////////////////////////////////////
//		int iIndex = 2;
//		
//		if(isElementPresent(byDragComponentsHere,"Drag Components Here")){
//			List<WebElement> wbs = driver.getWebDriver().findElements(byDragComponentsHere);
//			int elementCount = wbs.size();
//			System.out.println("elementCount: " + elementCount);
//			if(strComponentName.equalsIgnoreCase("Self Certification Component")){				
//				elementCount=elementCount+1;
//			}
//			WebElement eDivItem = wbs.get(elementCount - iIndex); // Default iIndex = 2
//			//highLightElement(eDivItem);
//			Boolean bInsertComponent = insertComponent(eDivItem,"Drag components here", strComponentName);
//			if(bInsertComponent){
//				return true;
//			}else{
//				return false;
//			}
//		}else{
//			report.updateTestLog("addComponent", " Drag Components Here- Element not available in the AEM Edit Page" , Status.FAIL);
//			System.out.println("addComponent: Drag Components Here- Element not available in the AEM Edit Page" );
//			frameworkParameters.setStopExecution(true);
//			return false;
//		}
//	}
//	public Boolean addComponent(String strComponentName) throws Exception{
//		//String strLocator = "//div[@id='OverlayWrapper']//div[@title='Drag components here']";			
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strPageTemplate = dataTable.getData("MultiBrand_Data", "Page_Template");
//		String strTC = t.getCurrentTestcase();
////		if(strPageTemplate.equalsIgnoreCase("full bleed page template")){
////			WebElement eDivItem = driver.findElement(getLocator("Content_DragComponent"));
////			Boolean bInsertComponent = insertComponent(eDivItem,"Drag components here", "Full Bleed Container Component");			
////		}
//		//String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		By byDragComponentsHere = null;
////		if(strExecutionMode.equalsIgnoreCase("LOCAL") && strBrowserName.equalsIgnoreCase("EDGE")){
////			if(strPageTemplate.equalsIgnoreCase("Product Page template")){
////				//String strLocator = "//div[@id='OverlayWrapper']//div[@title='Drag components here']";
////				byDragComponentsHere =getLocator("Edge_ProductTemplate_DragComponent");
////			}else{
////				System.out.println("Generic page template");
////				for(int i=4; i<= 9; i++){
////					byDragComponentsHere = By.xpath("//*[@id='OverlayWrapper']/div[" + i + "]/div/div");
////					if(!isElementNotVisible(byDragComponentsHere,"Drag Components Here")){
////						///
////						System.out.println("//*[@id='OverlayWrapper']/div[" + i + "]/div/div");
////						break;
////					}
////				}
////				//byDragComponentsHere = By.xpath("//*[@id='OverlayWrapper']/div[9]/div/div");
////				//byDragComponentsHere =getLocator("Edge_GenericTemplate_DragComponent");
////				//byDragComponentsHere = By.className("cq-Overlay cq-Overlay--component cq-droptarget cq-Overlay--placeholder");
////			}				
////		}else{
////			byDragComponentsHere =getLocator("DragComponent");
////			//byDragComponentsHere = By.xpath("//div[@id='OverlayWrapper']//div[@title='Drag components here']");
////		}
//		if(strComponentName.equalsIgnoreCase("Self Certification Component")){
//			//strPageTemplate = "Self Certification Page Template";
//			byDragComponentsHere = selectDragComponentForTemplate("Self Certification Page Template");		
//		}else if(strComponentName.equalsIgnoreCase("Modal Window Component")){
//			byDragComponentsHere = getLocator("ModalWindow_DragComponent");				
//		}else if(strComponentName.equalsIgnoreCase("Full Bleed Container Component")){
//			byDragComponentsHere = getLocator("FullBleed_SectionContent_DragComponent");					
//		}else{
//			byDragComponentsHere = selectDragComponentForTemplate(strPageTemplate);
//		}
//		/////////////////////////////////////////////////////////////////////////////////////////////////////	
//		if(byDragComponentsHere != null){
//			if(isElementVisible(byDragComponentsHere,"Drag Components Here")){
//				
////				List<WebElement> wbs = driver.getWebDriver().findElements(byDragComponentsHere);
////				int elementCount = wbs.size();
////				System.out.println("elementCount: " + elementCount);
////				if(strComponentName.equalsIgnoreCase("Self Certification Component")){				
////					elementCount=elementCount+1;
////				}
//				//WebElement eDivItem = wbs.get(elementCount - iIndex); // Default iIndex = 2
//				//highLightElement(eDivItem);
//				WebElement eDivItem = driver.findElement(byDragComponentsHere);
//				Boolean bInsertComponent = insertComponent(eDivItem,"Drag components here", strComponentName);
//				if(bInsertComponent){
//					return true;
//				}else{
//					return false;
//				}
//			}else{
//				report.updateTestLog("addComponent", " Drag Components Here- Element not available in the AEM Edit Page" , Status.FAIL);
//				System.out.println("addComponent: Drag Components Here- Element not available in the AEM Edit Page" );
//				frameworkParameters.setStopExecution(true);
//				return false;
//			}
//		}else{
//			report.updateTestLog("addComponent", " Drag Components Here- Element is null" , Status.FAIL);
//			System.out.println("addComponent: Drag Components Here- Element is null" );
//			frameworkParameters.setStopExecution(true);
//			return false;
//		}			
//	}
//	/**
//     * Description: insert a Component inside an Element specified in the parameter to the AEM Edit Page
//     * @param WebElement e - Element in which specified Component to be inserted.
//     * @param String strElementDesc -  Description of the Element
//     * @param String strComponentName - Component name to be added
//     * @throws Exception
//     */
//	public Boolean insertComponent(WebElement e,String strElementDesc, String strComponentName) throws Exception{
//		isElementVisible(e, strElementDesc);
//		if(isElementVisible(e, strElementDesc)){			
//			//javaScriptClick(e, strElementDesc);			
//			//mouseClickJScript(e, strElementDesc);
//			hoverActionClick(e, strElementDesc);
//			//e.click();////////////////////////////////////////////////////////////////////////////////////////////////////////
//			//waitFor(4000);
//			if(!isElementVisible("AEM.InsertComponent", "Icon Insert(Plus) Component",10)){
//				//mouseClickJScript(e, strElementDesc);
//				hoverActionClick(e, strElementDesc);
//			}
//			if(isElementVisible("AEM.InsertComponent", "Icon Insert(Plus) Component")){
//				javaScriptClick("AEM.InsertComponent", "Icon Insert(Plus) Component");  // Click on the Insert component (+ icon)
//				Boolean bComponentFound = false;
//				/*
//				By byComponent = By.xpath("//div//ul/button[text()='" + strComponentName + "']");
//				if(isElementPresent(byComponent,strComponentName)){
//					scrollToElement(byComponent,strComponentName);
//					javaScriptClick(byComponent,strComponentName);
//					isElementNotPresent("InsertComponentClose","Insert Component Dialog Close");
//					bComponentFound = true;
//				}*/				
//				By bycomponentsList = getLocator("AEM.InsertComponentList");		
//				List<WebElement> componentsList = driver.getWebDriver().findElements(bycomponentsList);
//				int iComponentsListcount = componentsList.size();					
//				for(int i=0;i<iComponentsListcount;i++){
//					//scrollToElement(componentsList.get(i),componentsList.get(i).getText());
//					if(componentsList.get(i).getText().equalsIgnoreCase(strComponentName)){
//						//waitFor(1000); /// To take screenshot with sync
//						//scrollToElement(componentsList.get(i),strComponentName);						
//						scrollElementToMiddlePage(componentsList.get(i),strComponentName);
//						highLightElement(componentsList.get(i));
//						report.updateTestLog("insertComponent", "Select the " + strComponentName + " component from the available list" , Status.SCREENSHOT);	
//						javaScriptClick(componentsList.get(i), strComponentName);						
//						bComponentFound = true;
//						//waitFor(2000);
//						break;
//					}			
//				}
//				if(bComponentFound){
//					//report.updateTestLog("insertComponent", strComponentName + " - Component added to the Page" , Status.PASS);
//					return true;
//				}else{
//					report.updateTestLog("insertComponent", strComponentName + " - Component not available in the Insert Component List" , Status.FAIL);
//					javaScriptClick("InsertComponentClose","Insert Component Dialog Close");
//					frameworkParameters.setStopExecution(true);
//					return false;
//				}
//			}else{
//				report.updateTestLog("insertComponent", strElementDesc + " - Icon Insert(Plus) Element not Present" , Status.FAIL);
//				frameworkParameters.setStopExecution(true);
//				return false;
//			}			
//		}else{
//			report.updateTestLog("insertComponent", strElementDesc + " - Parent Element not Present" , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//			return false;
//		}
//	}
//	
//	/**
//     * Description: This method will open the created page in Edit mode and do page load sync     * 
//     * 
//     * @throws Exception
//     */
//	public void openPageSync(String strPageTemplate) throws Exception{
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		//String strPage_Template = dataTable.getData("MultiBrand_Data", "Page_Template");
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//			openPageSync_IE();
//		}else if(strPageTemplate.equalsIgnoreCase("Popup Snippet Page Template") ||
//				strPageTemplate.equalsIgnoreCase("Generic Snippet Page Template") ||
//				strPageTemplate.equalsIgnoreCase("Self Certification Page Template") ||
//				strPageTemplate.equalsIgnoreCase("Video Resource Snippet Template") ||
//				strPageTemplate.equalsIgnoreCase("Exit Ramp Page Template")){
//			openPageSync_Template(strPageTemplate);
//		}
//		else{
//			openPageSync_Others();
//		}
//	}
//	public void openPageSync() throws Exception{
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strPageTemplate = dataTable.getData("MultiBrand_Data", "Page_Template");
//		String strTC = t.getCurrentTestcase().trim();
//		if(strTC.toLowerCase().contains("selfcertification") || (strTC.toLowerCase().contains("certification") && (strTC.toLowerCase().contains("self")))){
//			strPageTemplate = "Self Certification Page Template";
//		}
//		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//			openPageSync_IE();
//		}else if(strPageTemplate.equalsIgnoreCase("Popup Snippet Page Template") ||
//				strPageTemplate.equalsIgnoreCase("Self Certification Page Template") ||
//				strPageTemplate.equalsIgnoreCase("Video Resource Snippet Template") ||
//				strPageTemplate.equalsIgnoreCase("Exit Ramp Page Template")){
//			openPageSync_Template(strPageTemplate);
//		}
//		else{
//			openPageSync_Others();
//		}
//	}
//	public void openPageSync_OLD() throws Exception{
//		String strPageTitle = null;		
//		Boolean bPageOpened = false;		
//		String strActual_PageTitle = null;
//		strPageTitle = driver.getTitle();
//		String strComponentToAdd = dataTable.getData("MultiBrand_Data", "ComponentToAdd");
//		if(strComponentToAdd.equalsIgnoreCase("Login") || strComponentToAdd.equalsIgnoreCase("User Profile")){
//			strActual_PageTitle = driver.getTitle();
//		}else{
//			strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//		}
//		System.out.println("strPageTitle: " + strPageTitle);	
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();		
//		//String strCreatedPageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	    // change focus to new tab
//	    String oldTabWinHandle = driver.getWindowHandle();
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		System.out.println("Page opened in new Tab");
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iTabcount = availableWindows.size();
//	    System.out.println("Page Count: " + iTabcount);	       
//		System.out.println("Wait for page: " + strActual_PageTitle + " to load");		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//			System.out.println("IN IE Browser");
//			//String newTabWinHandle1 = availableWindows.get(iTabcount -2);
//			String newTabWinHandle2 = availableWindows.get(iTabcount -1);
//			//System.out.println("newTabWinHandle1:" + newTabWinHandle1);
//			//System.out.println("newTabWinHandle2:" + newTabWinHandle2);
//			driver.switchTo().window(newTabWinHandle2);
//			driver.manage().window().maximize();
//			waitFor(2000);
//			String strIEPageTitle = driver.getTitle();
//			System.out.println("strIEPageTitle: " + strIEPageTitle);
//			/*
//			if(!waitForPageTitle(strActual_PageTitle)){
//				driver.navigate().refresh();
//				waitFor(5000);
//				waitForPageTitle(strActual_PageTitle);
//			}
//			*/
//			//report.updateTestLog("PageSync_IE", "Screenshot", Status.SCREENSHOT);
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",50)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//		}else{
//			try {
//				if (iTabcount > 1) {  // Switch to new tab if exist.
//				/////////driver.switchTo().window(availableWindows.get(iTabcount -1));
//				///////String newTabWinHandle = availableWindows.get(iTabcount -1);    
//				////////////////System.out.println("newTabWinHandle:" + newTabWinHandle);
//				
//				////
//				switchToPage(strActual_PageTitle);
//								
//				strPageTitle = driver.getTitle();
//				System.out.println("strPageTitle: " + strPageTitle);
//				}
//			} catch (TimeoutException e){
//				//e.printStackTrace();
//				driver.switchTo().window(availableWindows.get(iTabcount-1));
//			}
//			try {
//				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),75);
//				wait.until(ExpectedConditions.titleIs(strActual_PageTitle));
//			} catch (Exception e) {
//				//e.printStackTrace();
//				System.out.println("strPageTitle: " + strActual_PageTitle + " not loaded properly.");
//			}
//			bPageOpened = isElementVisible("AEM.PageInfo", "Page Info Icon"); //////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(bPageOpened){
//				System.out.println("Page Loaded: " + strActual_PageTitle);
//			}else{
//				System.out.println("Page not Loaded");
//			    driver.switchTo().window(oldTabWinHandle);
//			    waitFor(3000);
//			}
//		}
//		
//		
//		//By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//		By byOverlayWrapper = By.xpath("//div[@title='Drag components here']");
//		int iWaitTime = 100;
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("Edge")){
//			waitFor(5000);
//			By byContainerSearch = getLocator("AEM.inputContainerSearch");
//			if(!isElementVisible(byContainerSearch, "Container Search",20)){
//				waitFor(3000);
//				//By byEdit = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//				if(isElementPresent("GlobalBarEditInPreviewPage","Edit button")){
//					javaScriptClick("GlobalBarEditInPreviewPage","Edit button");
//					//javaScriptClick(byEdit,"Edit button");
//					//*[@id="Content"]/div[1]/nav/div[3]/button[1]
//					System.out.println("Edit Button  Clicked");
//					waitFor(5000);
//					if(!isElementPresent(byOverlayWrapper,"Edit Window",iWaitTime)){ 
//						//javaScriptClick(byEdit,"Edit button");
//						javaScriptClick("GlobalBarEditInPreviewPage","Edit button");
//					}
//				}
//			}			
//		}
//		if(!isElementVisible(byOverlayWrapper,"Edit Window")){
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",30)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//			//By byEdit = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//driver.findElement(byEdit).click();
//		}
//		if(isElementVisible(byOverlayWrapper,"Edit Window",iWaitTime)){ ///////////////////////////////////////////////////////////////////////////////////////////////////////
//			//scrollToTop();
//			scrollInToTopInEditWindow();
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window found." , Status.PASS);
//		}else{
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window not found." , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}
//	}
//	/**
//     * Description: This method will open the created page in Edit mode and do page load sync     * 
//     * 
//     * @throws Exception
//     */
//	public void openPageSync_Others() throws Exception{
//		String strPageTitle = null;		
//		Boolean bPageOpened = false;		
//		String strActual_PageTitle = null;
//		try {
//			strPageTitle = driver.getTitle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			//e1.printStackTrace();
//		}
//		String strComponentToAdd = dataTable.getData("MultiBrand_Data", "ComponentToAdd");
//		String strPage_Template = dataTable.getData("MultiBrand_Data", "Page_Template");
//		if(strComponentToAdd.equalsIgnoreCase("Login") || strComponentToAdd.equalsIgnoreCase("User Profile")){
//			strActual_PageTitle = driver.getTitle();
//		}else if(strPage_Template.equalsIgnoreCase("Popup Snippet Page Template")){
//			strActual_PageTitle = driver.getCurrentUrl();
//		}else{
//			strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");/////////////////////
//		}
//		System.out.println("strPageTitle: " + strPageTitle);
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();		
//		//String strCreatedPageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	    // change focus to new tab
//		try {
//			strPageTitle = driver.getTitle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			//e1.printStackTrace();
//		}
//		System.out.println("strPageTitle: " + strPageTitle);
//	    String oldTabWinHandle = null;
//		try {
//			oldTabWinHandle = driver.getWindowHandle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		System.out.println("Page opened in new Tab");
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iTabcount = availableWindows.size();
//	    System.out.println("Page Count: " + iTabcount);	       
//		System.out.println("Wait for page: " + strActual_PageTitle + " to load");		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("Chrome")){
//			try {
//				if (iTabcount > 1) {  // Switch to new tab if exist.
//					driver.switchTo().window(availableWindows.get(iTabcount -1));
//					String newTabWinHandle = availableWindows.get(iTabcount -1);    
//					System.out.println("newTabWinHandle:" + newTabWinHandle);
//					//switchToPage(strActual_PageTitle);							
//					strPageTitle = driver.getTitle();
//					System.out.println("strPageTitle: " + strPageTitle);
//				}else{
//					driver.switchTo().window(availableWindows.get(iTabcount-1));
//				}
//			} catch (TimeoutException e){
//				//e.printStackTrace();
//				driver.switchTo().window(availableWindows.get(iTabcount-1));
//			}
//			try {
//				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),75);
//				wait.until(ExpectedConditions.titleIs(strActual_PageTitle));
//			} catch (Exception e) {
//				//e.printStackTrace();
//				System.out.println("strPageTitle: " + strActual_PageTitle + " not loaded properly.");
//			}
//			bPageOpened = isElementVisible("AEM.PageInfo", "Page Info Icon"); //////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(bPageOpened){
//				System.out.println("Page Loaded: " + strActual_PageTitle);
//			}else{
//				System.out.println("Page not Loaded");
//			    driver.switchTo().window(oldTabWinHandle);
//			    waitFor(3000);
//			}
//		}else{
//			/////
//		}		
//		//By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//		By byOverlayWrapper = By.xpath("//div[@title='Drag components here']");
//		int iWaitTime = 100;
//		if(!isElementVisible(byOverlayWrapper,"Edit Window")){
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",30)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//			//By byEdit = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//driver.findElement(byEdit).click();
//		}
//		if(isElementVisible(byOverlayWrapper,"Edit Window",iWaitTime)){ ///////////////////////////////////////////////////////////////////////////////////////////////////////
//			//scrollToTop();
//			scrollInToTopInEditWindow();
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window found." , Status.PASS);
//		}else{
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window not found." , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}
//	}
//	public void openPageSync_Others22222222222222222222() throws Exception{
//		String strPageTitle = null;		
//		Boolean bPageOpened = false;		
//		String strActual_PageTitle = null;
//		try {
//			strPageTitle = driver.getTitle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			//e1.printStackTrace();
//		}
//		String strComponentToAdd = dataTable.getData("MultiBrand_Data", "ComponentToAdd");
//		String strPage_Template = dataTable.getData("MultiBrand_Data", "Page_Template");
//		if(strComponentToAdd.equalsIgnoreCase("Login") || strComponentToAdd.equalsIgnoreCase("User Profile")){
//			strActual_PageTitle = driver.getTitle();
//		}else if(strPage_Template.equalsIgnoreCase("Popup Snippet Page Template")){
//			strActual_PageTitle = driver.getCurrentUrl();
//		}else{
//			strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//		}
//		System.out.println("strPageTitle: " + strPageTitle);
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();		
//		//String strCreatedPageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	    // change focus to new tab
//		try {
//			strPageTitle = driver.getTitle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			//e1.printStackTrace();
//		}
//		System.out.println("strPageTitle: " + strPageTitle);
//	    String oldTabWinHandle = null;
//		try {
//			oldTabWinHandle = driver.getWindowHandle();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		System.out.println("Page opened in new Tab");
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iTabcount = availableWindows.size();
//	    System.out.println("Page Count: " + iTabcount);	       
//		System.out.println("Wait for page: " + strActual_PageTitle + " to load");		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("Chrome")){
//			try {
//				if (iTabcount > 1) {  // Switch to new tab if exist.
//					driver.switchTo().window(availableWindows.get(iTabcount -1));
//					String newTabWinHandle = availableWindows.get(iTabcount -1);    
//					System.out.println("newTabWinHandle:" + newTabWinHandle);
//					//switchToPage(strActual_PageTitle);							
//					strPageTitle = driver.getTitle();
//					System.out.println("strPageTitle: " + strPageTitle);
//				}
//			} catch (TimeoutException e){
//				//e.printStackTrace();
//				driver.switchTo().window(availableWindows.get(iTabcount-1));
//			}
//			try {
//				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),75);
//				wait.until(ExpectedConditions.titleIs(strActual_PageTitle));
//			} catch (Exception e) {
//				//e.printStackTrace();
//				System.out.println("strPageTitle: " + strActual_PageTitle + " not loaded properly.");
//			}
//			bPageOpened = isElementVisible("AEM.PageInfo", "Page Info Icon"); //////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(bPageOpened){
//				System.out.println("Page Loaded: " + strActual_PageTitle);
//			}else{
//				System.out.println("Page not Loaded");
//			    driver.switchTo().window(oldTabWinHandle);
//			    waitFor(3000);
//			}
//		}else{
//			/////
//		}		
//		//By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//		By byOverlayWrapper = By.xpath("//div[@title='Drag components here']");
//		int iWaitTime = 100;
//		if(!isElementVisible(byOverlayWrapper,"Edit Window")){
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",30)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//			//By byEdit = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//driver.findElement(byEdit).click();
//		}
//		if(isElementVisible(byOverlayWrapper,"Edit Window",iWaitTime)){ ///////////////////////////////////////////////////////////////////////////////////////////////////////
//			//scrollToTop();
//			scrollInToTopInEditWindow();
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window found." , Status.PASS);
//		}else{
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window not found." , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}
//	}
//	public void openPageSync_Template(String strPage_Template) throws Exception{
//		String strPageTitle = null;		
//		Boolean bPageOpened = false;		
//		String strActual_PageTitle = null;
//		strPageTitle = driver.getTitle();
//		String strComponentToAdd = dataTable.getData("MultiBrand_Data", "ComponentToAdd");
//		//String strPage_Template = dataTable.getData("MultiBrand_Data", "Page_Template");
//		if(strComponentToAdd.equalsIgnoreCase("Login") || strComponentToAdd.equalsIgnoreCase("User Profile")){
//			strActual_PageTitle = driver.getTitle();
//		}else if(strPage_Template.equalsIgnoreCase("Popup Snippet Page Template") ||
//				strPage_Template.equalsIgnoreCase("Generic Snippet Page Template")){
//			strActual_PageTitle = driver.getCurrentUrl();
//		}else{
//			strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//		}
//		System.out.println("strPageTitle: " + strPageTitle);
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();		
//		//String strCreatedPageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	    // change focus to new tab
//	    String oldTabWinHandle = driver.getWindowHandle();
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		System.out.println("Page opened in new Tab");
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iTabcount = availableWindows.size();
//	    System.out.println("Page Count: " + iTabcount);	       
//		System.out.println("Wait for page: " + strActual_PageTitle + " to load");		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("Chrome")){
//			try {
//				if (iTabcount > 1) {  // Switch to new tab if exist.
//					driver.switchTo().window(availableWindows.get(iTabcount -1));
//					String newTabWinHandle = availableWindows.get(iTabcount -1);    
//					System.out.println("newTabWinHandle:" + newTabWinHandle);
//					//switchToPage(strActual_PageTitle);							
//					strPageTitle = driver.getTitle();
//					System.out.println("strPageTitle: " + strPageTitle);
//				}
//			} catch (TimeoutException e){
//				driver.switchTo().window(availableWindows.get(iTabcount-1));
//			}
//			bPageOpened = isElementVisible("AEM.PageInfo", "Page Info Icon"); //////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(bPageOpened){
//				System.out.println("Page Loaded: " + strActual_PageTitle);
//			}else{
//				System.out.println("Page not Loaded");
//			    driver.switchTo().window(oldTabWinHandle);
//			    waitFor(3000);
//			}
//		}else{
//			/////
//		}		
//		//By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//		By byOverlayWrapper = By.xpath("//div[@title='Drag components here']");
//		int iWaitTime = 100;
//		Boolean bEditFound = true;
//		if(!isElementVisible(byOverlayWrapper,"Edit Window")){
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			
//			try {
//				if(isElementPresent(byEdit,"Edit button",30)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}else{
//				bEditFound = false;
//			}
//			} catch (Exception e) {
//				bEditFound = false;
//				report.updateTestLog("openPageSync_Template", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//		}
//		if(bEditFound){ ///////////////////////////////////////////////////////////////////////////////////////////////////////
//			scrollInToTopInEditWindow();
//			report.updateTestLog("openPageSync_Template", strActual_PageTitle + " Page - Edit Window found." , Status.PASS);
//		}else{
//			report.updateTestLog("openPageSync_Template", strActual_PageTitle + " Page - Edit Window not found." , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}
//	}
//	
//	/**
//     * Description: This method will open the created page in Edit mode and do page load sync     * 
//     * 
//     * @throws Exception
//     */
//	public void openPageSync_IE() throws Exception{
//		String strPageTitle = null;		
//		Boolean bPageOpened = false;		
//		String strActual_PageTitle = null;
//		strPageTitle = driver.getTitle();
//		String strComponentToAdd = dataTable.getData("MultiBrand_Data", "ComponentToAdd");
//		if(strComponentToAdd.equalsIgnoreCase("Login") || strComponentToAdd.equalsIgnoreCase("User Profile")){
//			strActual_PageTitle = driver.getTitle();
//		}else{
//			strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//		}
//		System.out.println("openPageSync_IE: strPageTitle: " + strPageTitle);	
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();		
//		//String strCreatedPageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	    // change focus to new tab
//	    String oldTabWinHandle = driver.getWindowHandle();
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		System.out.println("Page opened in new Tab");
//	    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//	    int iTabcount = availableWindows.size();
//	    System.out.println("Page Count: " + iTabcount);	       
//		System.out.println("Wait for page: " + strActual_PageTitle + " to load");		
//		if(strExecutionMode.equalsIgnoreCase("Local") && strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//			System.out.println("IN IE Browser");
//			//String newTabWinHandle1 = availableWindows.get(iTabcount -2);
//			String newTabWinHandle2 = availableWindows.get(iTabcount -1);
//			//System.out.println("newTabWinHandle1:" + newTabWinHandle1);
//			//System.out.println("newTabWinHandle2:" + newTabWinHandle2);
//			driver.switchTo().window(newTabWinHandle2);
//			driver.manage().window().maximize();
//			waitFor(2000);
//			String strIEPageTitle = driver.getTitle();
//			System.out.println("strIEPageTitle: " + strIEPageTitle);
//			/*
//			if(!waitForPageTitle(strActual_PageTitle)){
//				driver.navigate().refresh();
//				waitFor(5000);
//				waitForPageTitle(strActual_PageTitle);
//			}
//			*/
//			//report.updateTestLog("PageSync_IE", "Screenshot", Status.SCREENSHOT);
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",50)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//		}		
//		//By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//		By byOverlayWrapper = By.xpath("//div[@title='Drag components here']");
//		int iWaitTime = 100;
//		if(!isElementVisible(byOverlayWrapper,"Edit Window - Drag components")){
//			By byEdit = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']");			
//			By byEdit2 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[@data-layer='Edit']/span");
//			By byEdit3 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]/span");
//			By byEdit4 = By.xpath("//*[@id='Content']/div[1]/nav/div[3]/button[1]");
//			//By byEdit5 = By.xpath("//div[@class='editor-GlobalBar-rightContainer']/button[text()='Edit']");
//			try {
//				if(isElementPresent(byEdit,"Edit button",30)){
//					//driver.findElement(byEdit).click();
//					mouseClickJScript(byEdit,"Edit button");
//				}else if(isElementPresent(byEdit2,"Edit button",10)){
//					System.out.println("Edit button present ");
//					mouseClickJScript(byEdit2,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit3,"Edit button",10)){
//					mouseClickJScript(byEdit3,"Edit button");
//				}else if(isElementPresent(byEdit4,"Edit button",10)){
//					mouseClickJScript(byEdit4,"Edit button");
//			}
//			} catch (Exception e) {
//				report.updateTestLog("openPageSync", "IE Browser openPageSync - Edit Button not Found", Status.FAIL);
//			}
//		}
//		System.out.println("IE Browser: Verify Element Visible: Edit Window - Drag components");
//		if(isElementVisible(byOverlayWrapper,"Edit Window - Drag components",iWaitTime)){ ///////////////////////////////////////////////////////////////////////////////////////////////////////
//			//scrollToTop();
//			scrollInToTopInEditWindow();
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window found." , Status.PASS);
//		}else{
//			report.updateTestLog("openPage", strActual_PageTitle + " Page - Edit Window not found." , Status.FAIL);
//			frameworkParameters.setStopExecution(true);
//		}
//	}
//	
//	
////////////////////Reusable Methods for Multi Brand Automation ////////////////////////////////
//	/**
//     * Description: Click an Element present in the application page using JavascriptExecutor class
//     * @param String strLocator - Locator of the Element
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void javaScriptClick(String strLocator, String strElementDesc) throws Exception {
// 		By by = getLocator(strLocator);
// 		javaScriptClick(by, strElementDesc);
// 	}
//	/**
//     * Description: Click an Element present in the application page using JavascriptExecutor class
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void javaScriptClick(By by, String strElementDesc) throws Exception {
//		if (isElementPresent(by,strElementDesc)){
//			WebElement element = driver.findElement(by);
//			try {
//				((JavascriptExecutor) driver.getWebDriver()).executeScript("arguments[0].click();", element);
//				System.out.println("javaScriptClick : Element Clicked: " + strElementDesc);
//				report.updateTestLog("javaScriptClick", "Element Clicked: " + strElementDesc , Status.DONE);
//			} catch (Exception e) {
//				report.updateTestLog("javaScriptClick", "Element Click: " + strElementDesc + " - Failed." + e.getMessage(), Status.FAIL);
//			}
//		}else{
//			report.updateTestLog("javaScriptClick: ", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	/**
//     * Description: Click an Element present in the application page using JavascriptExecutor class
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void javaScriptClick(WebElement element, String strElementDesc) throws Exception {
//		Boolean b = isElementPresent(element,strElementDesc);
//		if (b){
//			//highLightElement(element);
//			try {
//				((JavascriptExecutor) driver.getWebDriver()).executeScript("arguments[0].click();", element);
//				System.out.println("Element Clicked: " + strElementDesc);
//				report.updateTestLog("javaScriptClick", "Element Clicked: " + strElementDesc , Status.DONE);
//			} catch (Exception e) {
//				report.updateTestLog("javaScriptClick", "Element Click: " + strElementDesc + " - Failed." + e.getMessage(), Status.FAIL);
//			}
//		}else{
//			report.updateTestLog("javaScriptClick", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	
//	/**
//     * Description: scroll In To View Element present in the application page using JavascriptExecutor class
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @param int iScrollPixel - Pixel count
//     * @throws Exception
//     */
// 	public void scrollInToViewElement(WebElement webElement,String strElementDesc, int iScrollPixel) throws Exception {
// 		if(isElementPresent(webElement,strElementDesc)){ 		
// 	 		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();// Create instance of Javascript executor
// 	 		SeleniumTestParameters t = driver.getTestParameters();
// 			String strBrowserName = t.getBrowser().toString();
// 			String strExecutionMode = t.getExecutionMode().toString();
// 			if(strExecutionMode.equalsIgnoreCase("LOCAL")  && strBrowserName.equalsIgnoreCase("EDGE")){
// 				je.executeScript("arguments[0].scrollIntoView(true);",webElement);
// 			}else{
// 				je.executeScript("window.scrollTo(" + webElement.getLocation().x + "," + (webElement.getLocation().y - iScrollPixel) + ");");
// 			} 	 		
// 	 		//System.out.println("Scroll in to view Element:" + strElementDesc);
// 		}else{
//			report.updateTestLog("scrollInToViewElement", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
// 		 		
// 	}
//	/**
//     * Description: scroll In To View Element present in the application page using JavascriptExecutor class
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @param int iScrollPixel - Pixel count
//     * @throws Exception
//     */
// 	public void scrollInToViewElement(By by,String strElementDesc,int iScrollPixel) throws Exception {
// 		if(isElementPresent(by,strElementDesc)){
// 			WebElement webElement = driver.findElement(by); 		
// 	 		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver(); // Create instance of Javascript executor
// 	 		SeleniumTestParameters t = driver.getTestParameters();
// 			String strBrowserName = t.getBrowser().toString();
// 			String strExecutionMode = t.getExecutionMode().toString();
// 			if(strExecutionMode.equalsIgnoreCase("LOCAL")  && strBrowserName.equalsIgnoreCase("EDGE")){
// 				je.executeScript("arguments[0].scrollIntoView(true);",webElement);
// 			}else{
// 				je.executeScript("window.scrollTo(" + webElement.getLocation().x + "," + (webElement.getLocation().y - iScrollPixel) + ");");
// 			} 	 		
// 	 		//System.out.println("Scroll in to view Element:" + strElementDesc);
// 		}else{
//			report.updateTestLog("scrollInToViewElement", "Element not Present: " + strElementDesc , Status.FAIL);
//		} 				
// 	}
//	/**
//     * Description: scroll In To View Element present in the application page using JavascriptExecutor class
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void scrollInToViewElement(String strLocator, String strElementDesc) throws Exception{			
//		By by = getLocator(strLocator);
//		scrollInToViewElement(by,strElementDesc, 100); 					
// 	}
//	/**
//     * Description: scroll In To Element present in the application page using JavascriptExecutor class
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void scrollToElement(By by,String strElementDesc) throws Exception{
// 		
// 		scrollInToViewElement(by,strElementDesc,100);
// 		/*
// 		if(isElementPresent(by,strElementDesc)){
// 			 WebElement webElement = driver.findElement(by);
// 		 	   JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver(); // Create instance of Javascript executor 	   
// 		 	  je.executeScript("arguments[0].scrollIntoView(true);",webElement);
// 		 	//System.out.println("Scroll in to view Element:" + strElementDesc);
// 		}else{
// 			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc , Status.FAIL);
// 		} 	*/   
// 	 }
//	/**
//     * Description: scroll In To Element present in the application page using JavascriptExecutor class
//	 * @param  
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void scrollToElement(WebElement webElement,String strElementDesc) throws Exception{
// 		
// 		scrollInToViewElement(webElement,strElementDesc,100);
// 		/*
// 		if(isElementPresent(webElement,strElementDesc)){
// 		 	   JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver(); // Create instance of Javascript executor 	   
// 		 	  je.executeScript("arguments[0].scrollIntoView(true);",webElement);
// 		 	//System.out.println("Scroll in to view Element:" + strElementDesc);
// 		}else{
// 			report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc , Status.FAIL);
// 		} 	   */
// 	 }
//	/**
//     * Description: scroll the window to the bottom of the page.
//     * 
//     * @throws Exception
//     */ 	
//	 public void scrollToBottom() {		
//		System.out.println("Scroll to the Bottom of the page");	
//		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//		Long iScrollHeight = (long) 1.00;
//		Long iScrollHeight_Old = (long) 0.00;
//		while(iScrollHeight > iScrollHeight_Old){
//			((JavascriptExecutor) driver.getWebDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight);");//scroll To Bottom of the Page
//			waitFor(2000);
//			iScrollHeight_Old = iScrollHeight;
//			iScrollHeight = (Long) je.executeScript("return document.body.scrollHeight;");
//			//report.updateTestLog("testPage",  "Scroll to bottom", Status.SCREENSHOT);
//		}
//    }
//	 
//	 public void scrollToMiddlePage(){		
//			System.out.println("Scroll to the Bottom of the page");	
//			JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//			Double iScrollHeight = (Double) 1.00;
//			Double iScrollHeight_Old = (Double) 0.00;
//			while(iScrollHeight > iScrollHeight_Old){
//				((JavascriptExecutor) driver.getWebDriver()).executeScript("window.scrollTo(0, document.body.scrollHeight%2);");//scroll To Bottom of the Page
//				waitFor(2000);
//				iScrollHeight_Old = iScrollHeight;
//				iScrollHeight = (Double) je.executeScript("return document.body.scrollHeight/2;");
//				//report.updateTestLog("testPage",  "Scroll to bottom", Status.SCREENSHOT);
//			}
//	    }
//	 
//
//		public void scrollElementToMiddlePage(By by,String strElementDesc) throws Exception{	
//			//scrollInToViewElement(by,strElementDesc,100);		
//			if(isElementPresent(by,strElementDesc)){
//				 WebElement webElement = driver.findElement(by);
//				 String strJS = "arguments[0].scrollIntoView(true); var viewportH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); " + 
//						 "window.scrollBy(0, (arguments[0].getBoundingClientRect().height-viewportH)/2);";
//			 	 JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver(); // Create instance of Javascript executor 	   
//			 	 je.executeScript(strJS,webElement);
//			 	//System.out.println("Scroll in to view Element:" + strElementDesc);
//			}else{
//				report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc , Status.FAIL);
//			} 	
//		 }
//		
//		public void scrollElementToMiddlePage(WebElement webElement,String strElementDesc) throws Exception{	
//			//scrollInToViewElement(by,strElementDesc,100);		
//			if(isElementPresent(webElement,strElementDesc)){
//				 //WebElement webElement = driver.findElement(by);
//				 String strJS = "arguments[0].scrollIntoView(true); var viewportH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0); " + 
//						 "window.scrollBy(0, (arguments[0].getBoundingClientRect().height-viewportH)/2);";
//			 	 JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver(); // Create instance of Javascript executor 	   
//			 	 je.executeScript(strJS,webElement);
//			 	//System.out.println("Scroll in to view Element:" + strElementDesc);
//			}else{
//				report.updateTestLog("ScrollToElement", "Element not Present: " + strElementDesc , Status.FAIL);
//			} 	
//		 }
//	 
//	 
//		/**
//	     * Description: scroll the window to the Top of the page.
//	     * 
//	     * @throws Exception
//	     */ 
//	 public void scrollToTop() {
//		//System.out.println("Scroll to Top Window");
//		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//		//((JavascriptExecutor) driver.getWebDriver()).executeScript("window.scrollBy(0, -2500)");
//		//je.executeScript("window.scrollTo(" + webElement.getLocation().x + "," + (webElement.getLocation().y - 100) + ");");
//		je.executeScript("window.scrollTo(0,0);");
//		System.out.println("Scroll in to Top of the page");
//    } 	
//	
//	/**
//     * Description: verify whether a Page is opened in New Window or Not
//     * @param WebElement e - Element in which specified Component to be inserted.
//     * @param String strElementDesc -  Description of the Element
//     * @param String strNewWindowOption - strNewWindowOption (yes, no).
//     * @throws Exception
//     */
// 	public void verifyPageOpenedInNewWindow(WebElement e, String strElementDesc, String strNewWindowOption) throws Exception{
// 		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		String strParentPageTitle = driver.getTitle();
//		String strPageUrl = driver.getCurrentUrl();
//		System.out.println("strParentPageTitle: " + strParentPageTitle);	    
//	    String oldTabWinHandle = driver.getWindowHandle();// change focus to new tab
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//	    ArrayList<String> availableWindowsBefore = new ArrayList<String>(driver.getWindowHandles());
//	    int iWindowcount = availableWindowsBefore.size();
//	    System.out.println("Page Count Before: " + iWindowcount);
//	    //highLightElement(e);
//	    String strNavigateLink = e.getAttribute("href");
//	    report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Navigate to Url:" + strNavigateLink, Status.DONE);
//	    highLightElement(e);
//	    javaScriptClick(e,strElementDesc);  ///  Click on the List Item
//	    waitFor(3000);
//	    //////////////// Special case to handle Exit Ramp Popups
//	    
//	    if(strElementDesc.contains("SPC") || strElementDesc.contains("VerifyExitRamp")){
//	    	By byExitRamp_Continue = By.xpath("//div[@class='modal-dialog']//div[@class='modal-footer']/a/span[text()='Continue']");
//	    	if(isElementPresent(byExitRamp_Continue,"VerifyExitRamp_Continue")){
//				javaScriptClick(byExitRamp_Continue,"VerifyExitRamp_Continue");
//			}
//	    }
//	    /////
//	    waitFor(3000);
//	    ArrayList<String> availableWindowsAfter = new ArrayList<String>(driver.getWindowHandles());
//	    int iWindowcountAfter = availableWindowsAfter.size();
//	    System.out.println("Page Count After: " + iWindowcountAfter);
//	    
//	    if(strNewWindowOption.equalsIgnoreCase("YES")){
//	    	for(int i= 1; i<=5; i++){
//	    		availableWindowsAfter = new ArrayList<String>(driver.getWindowHandles());
//	    		iWindowcountAfter = availableWindowsAfter.size();
//	    		if (iWindowcount < iWindowcountAfter) {
//	    			break;
//	    		}else{
//	    			waitFor(3000);
//	    		}
//	    	}
//	    }
//	    
//	    
//	    report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - New Window Option:" + strNewWindowOption, Status.DONE);
//	    try {
//    		if(strNewWindowOption.equalsIgnoreCase("YES")){  			
//				if (iWindowcount < iWindowcountAfter) {  // Check Page Count opened in New Window
//					if(strExecutionMode.equalsIgnoreCase("LOCAL") && strBrowserName.equalsIgnoreCase("Chrome")){				
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1); 
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						//driver.close();
//						switchToPage(strParentPageTitle);
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("IOS")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						waitFor(5000);
//						System.out.println("Page opened in new Tab");
//						strPageUrl = driver.getCurrentUrl();
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened. Url is: " + strPageUrl, Status.SCREENSHOT);
//						switchToPage(strParentPageTitle);
//						waitFor(3000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);							
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("ANDROID")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						waitFor(5000);
//						System.out.println("Page opened in new Tab");
//						strPageUrl = driver.getCurrentUrl();
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened. Url is: " + strPageUrl, Status.SCREENSHOT);
//						switchToPage(strParentPageTitle);
//						waitFor(3000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);					
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("WINDOWS") &&
//							strBrowserName.equalsIgnoreCase("FIREFOX")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1); 
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						System.out.println(" Switch to Parent Window: " + strParentPageTitle);
//						waitFor(3000);
//						switchToPage(strParentPageTitle);
//						System.out.println(" Switched to Parent Window: " + strParentPageTitle);
//					}else if(strExecutionMode.equalsIgnoreCase("LOCAL") && (strBrowserName.equalsIgnoreCase("Edge") ||
//							strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER"))){					
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1);
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						//driver.close();
//						switchToPage(strParentPageTitle);
//					}				
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else if(iWindowcount == iWindowcountAfter) {
//			    	System.out.println("Page opened in same Window");
//				    strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog("verifyPageOpenedInNewWindow", "List Item Type: " + strElementDesc + " - Page opened in same Window which is not expected. URL is: " + strPageUrl, Status.FAIL);
//					driver.navigate().back();
//					report.updateTestLog("verifyPageOpenedInNewWindow", strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else{
//					/// Not Possible (iWindowcount > iWindowcountAfter)
//				} //End of iWindowcount compare with iWindowcountAfter
//			}else{ ///////////////////////////////////////////////////////// NewWindowOption is NO ////////////////////////////////////////////////////////////////////
//	 			report.updateTestLog("verifyPageOpenedInNewWindow",   "Verify Page opened in same Window OR NOT", Status.DONE);
//			    if (iWindowcount == iWindowcountAfter){  // Check Page opened in Same Window
//			    	System.out.println("Page opened in same Window");
//				    strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in same Window as expected. URL is: " + strPageUrl, Status.PASS);
//					
//					if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//						System.out.println("strBrowserName: " + strBrowserName);
//						JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//						je.executeScript("window.history.back();");
//						waitFor(5000);
//						report.updateTestLog("verifyPageOpenedInNewWindow", " - Firefox goback", Status.SCREENSHOT);
//					}else{
//						driver.navigate().back();
//						waitFor(5000);
//					}					
//					
//					System.out.println("Switch Back to Parent window" );
//		            String strPageTitle = driver.getTitle();
//		            String strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	                if(strPageTitle.equalsIgnoreCase(strActual_PageTitle)){
//	                	/////////////
//	                }else{
//	                	if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//							System.out.println("strBrowserName: " + strBrowserName);
//							JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//							je.executeScript("window.history.back();");
//							waitFor(5000);
//							//report.updateTestLog("verifyPageOpenedInNewWindow", " - Firefox goback", Status.SCREENSHOT);
//						}else{
//							driver.navigate().back();
//						}
//	                	try {
//	        				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),100);
//	        				wait.until(ExpectedConditions.titleIs(strActual_PageTitle));				
//	        			} catch (Exception ee) {
//	        				System.out.println("strPageTitle: " + strActual_PageTitle + " not loaded properly.");
//	        			}
//	                }
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else if (iWindowcount < iWindowcountAfter){
//					driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//					strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in new Window which is not expected. Url is: " + strPageUrl, Status.FAIL);	
//					driver.close();
//					System.out.println("Switch to Parent Window: " + strParentPageTitle);
//					switchToPage(strParentPageTitle);
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else{
//					/// Not Possible (iWindowcount > iWindowcountAfter)
//				} //End of iWindowcount compare with iWindowcountAfter	    
//		 	}
//								
//		} catch (TimeoutException ex) {
//			ex.printStackTrace();
//			switchToPage(strParentPageTitle);
//		} 		
// 	} //Method End
// 	public void verifyPageOpenedInNewWindow222(WebElement e, String strElementDesc, String strNewWindowOption) throws Exception{
// 		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		String strParentPageTitle = driver.getTitle();
//		String strPageUrl = driver.getCurrentUrl();
//		System.out.println("strParentPageTitle: " + strParentPageTitle);	    
//	    String oldTabWinHandle = driver.getWindowHandle();// change focus to new tab
//	    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//	    ArrayList<String> availableWindowsBefore = new ArrayList<String>(driver.getWindowHandles());
//	    int iWindowcount = availableWindowsBefore.size();
//	    System.out.println("Page Count Before: " + iWindowcount);
//	    //highLightElement(e);
//	    String strNavigateLink = e.getAttribute("href");
//	    report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Navigate to Url:" + strNavigateLink, Status.DONE);
//	    javaScriptClick(e,strElementDesc);  ///  Click on the List Item
//	    waitFor(3000);
//	    ArrayList<String> availableWindowsAfter = new ArrayList<String>(driver.getWindowHandles());
//	    int iWindowcountAfter = availableWindowsAfter.size();
//	    System.out.println("Page Count After: " + iWindowcountAfter);
//	    report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - New Window Option:" + strNewWindowOption, Status.DONE);
//	    try {
//    		if(strNewWindowOption.equalsIgnoreCase("YES")){   			
//				if (iWindowcount < iWindowcountAfter) {  // Check Page Count opened in New Window
//					if(strExecutionMode.equalsIgnoreCase("LOCAL") && strBrowserName.equalsIgnoreCase("Chrome")){				
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1); 
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						//driver.close();
//						switchToPage(strParentPageTitle);
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("IOS")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						waitFor(5000);
//						System.out.println("Page opened in new Tab");
//						strPageUrl = driver.getCurrentUrl();
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened. Url is: " + strPageUrl, Status.SCREENSHOT);
//						switchToPage(strParentPageTitle);
//						waitFor(3000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);							
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("ANDROID")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						waitFor(5000);
//						System.out.println("Page opened in new Tab");
//						strPageUrl = driver.getCurrentUrl();
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened. Url is: " + strPageUrl, Status.SCREENSHOT);
//						switchToPage(strParentPageTitle);
//						waitFor(3000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);					
//					}else if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("WINDOWS") &&
//							strBrowserName.equalsIgnoreCase("FIREFOX")){
//						System.out.println("MobileExecutionPlatform: " + strMobileExecutionPlatform);
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1); 
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						System.out.println(" Switch to Parent Window: " + strParentPageTitle);
//						waitFor(3000);
//						switchToPage(strParentPageTitle);
//						System.out.println(" Switched to Parent Window: " + strParentPageTitle);
//					}else if(strExecutionMode.equalsIgnoreCase("LOCAL") && (strBrowserName.equalsIgnoreCase("Edge") ||
//							strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER"))){					
//						driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//						System.out.println("Page opened in new Tab");
//						String newTabWinHandle = availableWindowsAfter.get(iWindowcountAfter - 1);
//						System.out.println("newTabWinHandle:" + newTabWinHandle);
//						String strPageTitle = driver.getTitle();
//						System.out.println("strPageTitle: " + strPageTitle);
//						waitFor(2000);
//						report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in New Window. Page Title is: " + strPageTitle, Status.PASS);
//						//driver.close();
//						switchToPage(strParentPageTitle);
//					}				
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else if(iWindowcount == iWindowcountAfter) {
//			    	System.out.println("Page opened in same Window");
//				    strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog("verifyPageOpenedInNewWindow", "List Item Type: " + strElementDesc + " - Page opened in same Window which is not expected. URL is: " + strPageUrl, Status.FAIL);
//					driver.navigate().back();
//					report.updateTestLog("verifyPageOpenedInNewWindow", strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else{
//					/// Not Possible (iWindowcount > iWindowcountAfter)
//				} //End of iWindowcount compare with iWindowcountAfter
//			}else{ ///////////////////////////////////////////////////////// NewWindowOption is NO
//	 			report.updateTestLog("verifyPageOpenedInNewWindow",   "Verify Page opened in same Window OR NOT", Status.DONE);
//			    if (iWindowcount == iWindowcountAfter){  // Check Page opened in Same Window
//			    	System.out.println("Page opened in same Window");
//				    strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in same Window as expected. URL is: " + strPageUrl, Status.PASS);
//					
//					if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//						System.out.println("strBrowserName: " + strBrowserName);
//						JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//						je.executeScript("window.history.back();");
//						waitFor(5000);
//						report.updateTestLog("verifyPageOpenedInNewWindow", " - Firefox goback", Status.SCREENSHOT);
//					}else{
//						driver.navigate().back();
//					}					
//					
//					System.out.println("Switch Back to Parent window" );
//		            String strPageTitle = driver.getTitle();
//		            String strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//	                if(strPageTitle.equalsIgnoreCase(strActual_PageTitle)){
//	                	/////////////
//	                }else{
//	                	if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//							System.out.println("strBrowserName: " + strBrowserName);
//							JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//							je.executeScript("window.history.back();");
//							waitFor(5000);
//							//report.updateTestLog("verifyPageOpenedInNewWindow", " - Firefox goback", Status.SCREENSHOT);
//						}else{
//							driver.navigate().back();
//						}
//	                	try {
//	        				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),100);
//	        				wait.until(ExpectedConditions.titleIs(strActual_PageTitle));	
//	        			} catch (Exception ee) {
//	        				System.out.println("strPageTitle: " + strActual_PageTitle + " not loaded properly.");
//	        			}
//	                }
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else if (iWindowcount < iWindowcountAfter){
//					driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//					strPageUrl = driver.getCurrentUrl();
//					System.out.println(strElementDesc + " - Page opened in new Window which is not expected. Url is: " + strPageUrl);
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Page opened in new Window which is not expected. Url is: " + strPageUrl, Status.FAIL);	
//					//driver.close();
//					System.out.println("Switch to Parent Window: " + strParentPageTitle);
//					switchToPage(strParentPageTitle);
//					System.out.println("Switched to Parent Window: ");
//					report.updateTestLog("verifyPageOpenedInNewWindow",  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else{
//					/// Not Possible (iWindowcount > iWindowcountAfter)
//				} //End of iWindowcount compare with iWindowcountAfter	    
//		 	}
//								
//		} catch (TimeoutException ex) {
//			ex.printStackTrace();
//			switchToPage(strParentPageTitle);
//		} 		
// 	} //Method End
//
//	/**
//     * Description: add Tool Tip to the inserted component present in the Current page.
//     * @param String strComponent -  Component in which Toop tip to be added.
//     * @param String strEnable_Tooltip -   Enable Tool tip (true, false)
//     * @param String strTooltip_Title -  Tool tip Title
//     * @param String strTooltip_Content -  Tool tip Content Text
//     * @throws Exception
//     */
// 	public void addToolTip(String strComponent, String strEnable_Tooltip, String strTooltip_Title, String strTooltip_Content) throws Exception{
// 		System.out.println(strComponent + " - Add Tool Tip Details");
//		if(strEnable_Tooltip.equalsIgnoreCase("Yes")){  //  Enable Tooltip Option		
//			By byEnableTooltip = getLocator("AEM.cbEnableTooltip");
//			By byTooltip_Title = getLocator("AEM.inputToolTipTitle");
//			By byTooltip_Content = getLocator("AEM.taToolTipContent");			
//			javaScriptClick(byEnableTooltip,strComponent + " - Enable Tool Tip Checkbox");
//			setData(byTooltip_Title, strComponent + " - Tooltip Title", strTooltip_Title);
//			setData(byTooltip_Content, strComponent + " - Tooltip Content", strTooltip_Content);		
//		}else{
//			report.updateTestLog("addToolTip", "Enable Tool Tip Checkbox is not Checked", Status.DONE);
//		}
// 	}
//	/**
//     * Description: verify Tool Tip details added to the component present in the Current page.
//     * @param WebElement webElement - Element in which Component is present
//     * @param String strComponentToVerify -  Component in which Tool tip details to be verifed.
//     * @param String strEnable_Tooltip -   Enable Tool tip (true, false)
//     * @param String strTooltip_Title -  Tool tip Title
//     * @param String strTooltip_Content -  Tool tip Content Text
//     * @throws Exception
//     */
// 	public void verifyAddedComponentToolTip(WebElement webElement,String strComponentToVerify, String strEnable_Tooltip, String strTooltip_Title, String strTooltip_Content) throws Exception{
// 		System.out.println("Verify Tool Tip Details");
// 		By byToolTipIcon = null;
//		By byTooltip_Title = null;
//		By byTooltip_Content = null;
//		if(strEnable_Tooltip.equalsIgnoreCase("YES")){  //  Title - Tooltip Icon should Present			
//			switch (strComponentToVerify){
//			case "Title":
//				byToolTipIcon = getLocator("Title_TooltipIcon");
//				byTooltip_Title = getLocator("Title_TooltipTitle");
//				byTooltip_Content = getLocator("Title_TooltipContent");
//				break;
//			case "Sub Heading":
//				byToolTipIcon = getLocator("SubHeading_TooltipIcon");
//				byTooltip_Title = getLocator("SubHeading_TooltipTitle");
//				byTooltip_Content = getLocator("SubHeading_TooltipContent");
//				break;
//			case "Section Heading":
//				byToolTipIcon = getLocator("SectionHeading_TooltipIcon");
//				byTooltip_Title = getLocator("SectionHeading_TooltipTitle");
//				byTooltip_Content = getLocator("SectionHeading_TooltipContent");
//				break;
//			default:
//				report.updateTestLog("verifyAddedComponentToolTip", "Component To Verify: " + strComponentToVerify + " is NOT Valid", Status.FAIL);	
//			}
//			WebElement eToolTipIcon = null;
//			if(isElementVisible(byToolTipIcon,strComponentToVerify + " ToolTip Icon")){
//				eToolTipIcon =  webElement.findElement(byToolTipIcon);
//				//highLightElement(eToolTipIcon);
//			}
//			if(isElementVisible(eToolTipIcon, strComponentToVerify + " - Tool Tip Icon")){
//				javaScriptClick(eToolTipIcon,"Title - Tooltip Icon");
//				waitTillElementExist(byTooltip_Title,strComponentToVerify + " - Tooltip Title");
//				scrollInToViewElement(webElement.findElement(byTooltip_Title),"Tool Tip Title", 100);
//				String strActualTooltip_Title = webElement.findElement(byTooltip_Title).getText();
//				System.out.println( "Actual Tooltip_Title : " + strActualTooltip_Title); 
//				waitTillElementExist(byTooltip_Content,strComponentToVerify + " - Tooltip Content");
//				String strActualTooltip_Content = webElement.findElement(byTooltip_Content).getText();
//				System.out.println( "Actual Tooltip_Content : " + strActualTooltip_Content); 
//				//Verify Tooltip Title
//				//scrollInToViewElement(webElement.findElement(byTooltip_Title),"Tool Tip Title");
//				verifyActualExpectedData("Tooltip Title", strActualTooltip_Title, strTooltip_Title); // Verify Tool Tip Title
//				//Verify Tooltip Content
//				verifyActualExpectedData("Tooltip Content", strActualTooltip_Content, strTooltip_Content); // Verify Tool Tip Content
//				if(isElementVisible(webElement.findElement(byTooltip_Content), strComponentToVerify + " - Tool Tip Icon")){
//					javaScriptClick(eToolTipIcon,"Title - Tooltip Icon"); // Close Tool Tip
//				}
//				isElementNotVisible(byTooltip_Content, strComponentToVerify + " - Tool Tip Icon");
//			}else{
//				//System.out.println("Verify Element: " + strComponentToVerify + " - Tooltip Icon is NOT Present" );
//				report.updateTestLog("Verify Tool Tip" , strComponentToVerify + " - Tooltip Icon is NOT Present" , Status.FAIL);
//			}		
//		}else{  //  Tool tip Icon should not Present
//			Boolean b = isElementNotVisible(byToolTipIcon,strComponentToVerify + " - Tool Tip Icon");
//			if(b){
//				System.out.println("Verify Tool Tip " + strComponentToVerify + " - Tooltip Icon is Present" );
//				report.updateTestLog("Verify Tool Tip" , strComponentToVerify + " - Tooltip Icon is Present" , Status.FAIL);
//			}else{
//				System.out.println("Verify  Tool Tip " + strComponentToVerify + "  - Tooltip Icon is not Present" );
//				report.updateTestLog("Verify  Tool Tip " , strComponentToVerify + " - Tooltip Icon is not Present" , Status.PASS);
//			}
//		}
// 	}
//	/**
//     * Description: verify Tool Tip details added to the component present in the Current page.
//     * @param String strComponentToVerify -  Component in which Tool tip details to be verifed.
//     * @param String strEnable_Tooltip -   Enable Tool tip (true, false)
//     * @param String strTooltip_Title -  Tool tip Title
//     * @param String strTooltip_Content -  Tool tip Content Text
//     * @throws Exception
//     */
// 	public void verifyComponentToolTip(String strComponentToVerify, String strEnable_Tooltip, String strTooltip_Title, String strTooltip_Content) throws Exception{
// 		System.out.println("Verify Tool Tip Details");
// 		By byToolTipIcon = null;
//		By byTooltip_Title = null;
//		By byTooltip_Content = null;
//		if(strEnable_Tooltip.equalsIgnoreCase("YES")){  //  Title - Tool tip Icon should Present			
//			switch (strComponentToVerify){
//			case "Title":
//				byToolTipIcon = getLocator("Title_TooltipIcon");
//				byTooltip_Title = getLocator("Title_TooltipTitle");
//				byTooltip_Content = getLocator("Title_TooltipContent");
//				break;
//			case "Sub Heading":
//				byToolTipIcon = getLocator("SubHeading_TooltipIcon");
//				byTooltip_Title = getLocator("SubHeading_TooltipTitle");
//				byTooltip_Content = getLocator("SubHeading_TooltipContent");
//				break;
//			case "Section Heading":
//				byToolTipIcon = getLocator("SectionHeading_TooltipIcon");
//				byTooltip_Title = getLocator("SectionHeading_TooltipTitle");
//				byTooltip_Content = getLocator("SectionHeading_TooltipContent");
//				break;
//			default:
//				report.updateTestLog("verifyAddedComponentToolTip", "Component To Verify: " + strComponentToVerify + " is NOT Valid", Status.FAIL);	
//			}
//			Boolean bToolTipIcon = isElementVisible(byToolTipIcon, strComponentToVerify + " - Tool Tip Icon");
//			System.out.println(strComponentToVerify + " - Tool Tip Icon Present: " + bToolTipIcon);
//			if(bToolTipIcon){
//				javaScriptClick(byToolTipIcon, strComponentToVerify + " - Tool Tip Icon");
//				waitTillElementExist(byTooltip_Title,strComponentToVerify + " - Tooltip Title");
//				//scrollInToViewElement(driver.findElement(byTooltip_Title),"Tool Tip Title");
//				String strActualTooltip_Title = driver.findElement(byTooltip_Title).getText();
//
//				waitTillElementExist(byTooltip_Content,strComponentToVerify + " - Tooltip Content");
//				String strActualTooltip_Content = driver.findElement(byTooltip_Content).getText();
//			
//				////////////scrollInToViewElement(driver.findElement(byTooltip_Title),"Tool Tip Title");
//				verifyActualExpectedData("Tooltip Title", strActualTooltip_Title, strTooltip_Title); // Verify Tool Tip Title
//				//Verify Tooltip Content
//				verifyActualExpectedData("Tooltip Content", strActualTooltip_Content, strTooltip_Content); // Verify Tool Tip Content
//				javaScriptClick(byToolTipIcon, strComponentToVerify + " - Tool Tip Icon"); // Close Tool Tip
//			}else{
//				report.updateTestLog("Verify Element" , strComponentToVerify + " - Tooltip Icon is NOT Present" , Status.FAIL);
//			}
//		}else{  //  Title - Tooltip Icon should not Present
//			Boolean bToolTipIcon = isElementVisible(byToolTipIcon, strComponentToVerify + " - Tool Tip Icon");
//			if(bToolTipIcon){
//				report.updateTestLog("Verify Element" , strComponentToVerify + " - Tooltip Icon is Present" , Status.FAIL);
//			}else{
//				report.updateTestLog("Verify Element" , strComponentToVerify + " - Tooltip Icon is not Present" , Status.PASS);
//			}
//		}
// 	}
//	/**
//     * Description: Select the Created Page present in the AEM Site.
//     * @param String strPageTitle -  created Page Title
//     * @return Boolean - returns true, if page is selected else returns false.
//     * @throws Exception
//     */
// 	public Boolean selectCreatedPage(String strPageLocation,String strPageTitle) throws Exception{
// 		Boolean bFlag = false;
//		System.out.println("selectCreatedPage: " + strPageTitle);
//		WebElement e = isPageTitlePresent(strPageLocation,strPageTitle); ///////// call isPageTitlePresent method
//		if(e!= null){
//			// Select the page
//			javaScriptClick(e,strPageTitle + " - Thumbnail Image");
//			bFlag = true;
//			//System.out.println("bPageSelected:" + bPageSelected);
//			System.out.println("Page Selected");
//		}
//		if(bFlag){
//			//report.updateTestLog("openPage", strPageTitle + " Page is selected." , Status.PASS);
//			System.out.println(strPageTitle + " Page is selected.");
//		}else{
//			report.updateTestLog("selectCreatedPage", strPageTitle + " Page not Found." , Status.FAIL);
//			System.out.println(strPageTitle + " Page is not selected.");
//		}
//		return bFlag;
// 	}
//	/**
//     * Description: Check whether Created Page is present in the AEM Site.
//     * @param String strPageTitle -  created Page Title
//     * @return Boolean - returns true, if page is present else returns false.
//     * @throws Exception
//     */
// 	public WebElement isPageTitlePresent(String strPageLocation,String pageTitle) throws Exception{
// 		System.out.println("isPageTitlePresent - PageLocation: " + strPageLocation + " PageTitle: " + pageTitle);
// 		Boolean bFlag = false;
// 		WebElement ePageTitle = null;
//		//System.out.println("CreatedPageTitle: " + pageTitle);
//		//String navigateMenuFlowCustomized = properties.getProperty("NavigateMenuCustomized");
// 		By byCoralWait = By.xpath("//coral-columnview-column-content//coral-wait");
// 		isElementNotVisible(byCoralWait,"byCoralWait");
//		//String navigateMenu = properties.getProperty("NavigateMenu");
//		String navigateMenuFlowCustomized = customizeNavigateMenu(strPageLocation);
//		String xpathPageTitles = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']//coral-columnview-item-content";	
//		System.out.println("xpathPageTitles: " + xpathPageTitles);
//		By byPageTitles = By.xpath(xpathPageTitles);
//		Boolean bElement = isElementPresent(byPageTitles,"Page Titles");
//		if(!bElement){		
//			navigateToMenu(strPageLocation);//Call navigateToMenu() method
//		}else{
//			//highLightElement(driver.findElement(byPageTitles));
//			System.out.println("xpath path identified: ");	
//			List<WebElement> pageTitles = driver.getWebDriver().findElements(byPageTitles);			
//			//String strpageImages = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/multibrand/global-master/en/testing']//coral-columnview-item-thumbnail/img";
//			String strpageImages = "//coral-columnview-column[@data-foundation-layout-columnview-columnid='/content/" + navigateMenuFlowCustomized +"']//coral-columnview-item-thumbnail/img";
//			System.out.println("xpath: " + strpageImages);
//			By bypageImages = By.xpath(strpageImages);
//			List<WebElement> pageImages = driver.getWebDriver().findElements(bypageImages);	
//			//System.out.println("matching pages collected ");
//			int pageCount = pageTitles.size();
//			System.out.println("Page Count: " + pageCount);			
//			for(int i= 0; i< pageCount; i++){
//				String tmppageTitle = pageTitles.get(i).getAttribute("title");				
//				//scrollToElement(pageTitles.get(i),tmppageTitle);
//				if(i >= (pageCount -2)){
//					//scrollToElement(pageTitles.get(i),tmppageTitle);
//					scrollElementToMiddlePage(pageTitles.get(i),tmppageTitle);
//					javaScriptClick(pageTitles.get(i),tmppageTitle);
//					//highLightElement(pageTitles.get(i));
//					waitFor(1000);
//					if(pageCount > 39){
//						waitFor(2000);
//					}
//					pageTitles = driver.getWebDriver().findElements(byPageTitles);
//					pageImages = driver.getWebDriver().findElements(bypageImages);
//					int iTmpPageCount = pageTitles.size();
//					if(iTmpPageCount > pageCount){
//						pageCount = iTmpPageCount;
//						System.out.println("Page Count: " + pageCount);
//						//scrollInToElement(pageTitles.get(pageCount - 1),"Last Page Title");
//						//javaScriptClick(pageTitles.get(pageCount - 1),"Last Page Title");
//					}				
//				}
//				//System.out.println(i + " - Page Title name: " + tmppageTitle);
//				if(tmppageTitle.equalsIgnoreCase(pageTitle)){
//					//String tmpPageImage = pageImages.get(i).getAttribute("src");
//					//System.out.println("Page Image src: " + tmpPageImage);
//					//pageImages.get(i).click();
//					//scrollToElement(pageTitles.get(i),tmppageTitle);
//					scrollElementToMiddlePage(pageTitles.get(i),tmppageTitle);
//					bFlag = true;
//					ePageTitle = pageImages.get(i);
//				}			
//			}
//		}		
//		if(bFlag){
//			System.out.println(pageTitle + " - Page is Present.");
//		}else{
//			//report.updateTestLog("isPageTitlePresent", pageTitle + " Page not Found." , Status.FAIL);
//			System.out.println(pageTitle + " Page is not selected.");
//		}
//		return ePageTitle;
// 	}	
//	/**
//     * Description: insert a Char to each character in the input String  and returns the updated data
//     * @param String strInput -  Input text in which charValue will be added
//     * @return String - the updated data
//     */
// 	public String insertACharToEachCharInString(String strInput, String charValue){
//		String strTmp = "";
//		int iStrInputCount = strInput.length();
//		for(int i=0; i< iStrInputCount ; i++){			
//			String strsubString = strInput.substring(i, i+1);
//			strTmp = strTmp + strsubString + "-";
//		}
//		return strTmp;		
//	}
//	/**
//     * Description: switch to specified page present in the current session
//     * @param String strPageRef -  Page title
//     * @return Boolean - returns true, if switched to the specified page else returns false.
//     */
// 	public Boolean switchToPage(String strPageRef) throws Exception{
//		// change focus to AEM Site
//		//String strPageTitle = dataTable.getData("MultiBrand_Data", "CreatedPageTitle");
//		Boolean bAEMSite = false;
//		System.out.println("switchToPage: " + strPageRef);
//		String strTmpPageTitle1 = driver.getTitle();
//	    if(strTmpPageTitle1.equalsIgnoreCase(strPageRef)){
//	    	bAEMSite = true;
//	    }else{
//	    	String currentTabWinHandle = driver.getWindowHandle();
//		    System.out.println("currentTabWinHandle:" + currentTabWinHandle);
//		    ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
//		    int iWindowcount = availableWindows.size();
//		    System.out.println("iWindowcount:" + iWindowcount);
//		    if(iWindowcount == 1){
//		    	// Click Back and do
//		    	driver.navigate().back();
//		    	System.out.println("navigate back:");
//		    	String strTmpPageTitle = driver.getTitle();
//		    	System.out.println("Switch to Page: " + strTmpPageTitle);
//		    	if(strTmpPageTitle.equalsIgnoreCase(strPageRef)){
//		    		bAEMSite = true;
//		    	}
//		    	report.updateTestLog("deleteAPage", "Go Back to Parent window: ", Status.SCREENSHOT);
//		    }else{
//		    	for(int i=0; i< iWindowcount; i++){
//		    		String strHandle = availableWindows.get(i);
//			    	driver.switchTo().window(strHandle);
//			    	String strTmpPageTitle = driver.getTitle();
//			    	System.out.println(i + " Page: " + strTmpPageTitle);
//			    	if(strTmpPageTitle.equalsIgnoreCase(strPageRef)){	    		
//			    		bAEMSite = true;
//			    		break;
//			    	}
//			    } //for Loop		    	
//		    	if(bAEMSite){
//		    		System.out.println("Switched to Page: " + strPageRef);
//		    		report.updateTestLog("switchToPage", "Switched to Page: " + strPageRef,Status.DONE);
//		    	}else{
//		    		report.updateTestLog("switchToPage", "AEM Site Page not Found",Status.SCREENSHOT);
//		    	}
//		    }		    
//		}
//	    return bAEMSite;
//	    } //End of switchToPage method 	
//	/**
//     * Description: Scroll in to top of the page in AEM Edit page
//     * 
//     */
// 	public void scrollInToTopInEditWindow() throws Exception{
// 		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//		if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strMobileExecutionPlatform.equalsIgnoreCase("WINDOWS") &&
//				strBrowserName.equalsIgnoreCase("FIREFOX")){
//			By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//			scrollInToViewElement(byOverlayWrapper,"Edit Window", 100);
//			System.out.println("scroll To Top Page: ");
//		}else{
//			By byOverlayWrapper = By.xpath("//div[@id='OverlayWrapper']");
//			scrollInToViewElement(byOverlayWrapper,"Edit Window", 100);
//			System.out.println("scroll To Top Page in Edt Window");
//		}
// 	}
//	/**
//     * Description: handle Coral Dialog Error pop up comes in any of the AEM Pages.
//     * 
//     */
//	public void handleCoralDialogError(){
//		//System.out.println("handleCoralDialogError method called.");	
//		//////////////////  commenting this codes since Coral Dialog Error is not comming in recent days
//		try {
//			if(isElementVisible("CoralDialogHeader","Coral Dialog Header",3)){					
//				highLightElement(driver.findElement(getLocator("CoralDialogHeader")));
//				By byCoralDialogHeader = getLocator("CoralDialogHeader");
//				report.updateTestLog("handleCoralDialogError", "Coral Dialog Error ",Status.FAIL);
//				//highLightElement(driver.findElement(byCoralDialogHeader));
//				String strHeaderText = getData(byCoralDialogHeader, "Coral Dialog Header", "Text");
//				if(strHeaderText.equalsIgnoreCase("Error")){
//					String strCoralDialogContent = "";
//					By byCoralDialogContent = getLocator("CoralDialogContent");
//					//highLightElement(driver.findElement(byCoralDialogContent));
//					//String strCoralDialogContent = getData(byCoralDialogContent, "CoralDialogContent", "Text");
//					//System.out.println("strCoralDialogContent: " + strCoralDialogContent);
//					report.updateTestLog("handleCoralDialogError", "Coral Dialog Error Content: " + strCoralDialogContent,Status.FAIL);
//					javaScriptClick("CoralDialogClose","Coral Dialog Close");
//				}
//			}else{
//				//report.updateTestLog("handleCoralDialogError", "Coral Dialog Not Present",Status.SCREENSHOT);
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			report.updateTestLog("handleCoralDialogError", e.getMessage(), Status.FAIL);
//		}
//	}
//	/**
//     * Description: scroll to Section Container in AEM Edit page
//     * 
//     */
//	public void scrollToSectionContainer() throws Exception{
//		SeleniumTestParameters t = driver.getTestParameters();
//		String strBrowserName = t.getBrowser().toString();
//		String strExecutionMode = t.getExecutionMode().toString();
//		String strPageTemplate = dataTable.getData("MultiBrand_Data", "Page_Template");
//		if(strExecutionMode.equalsIgnoreCase("LOCAL") && strBrowserName.equalsIgnoreCase("EDGE")){
//			if(strPageTemplate.equalsIgnoreCase("Product Page template")){
//				By bySectionContainer = getLocator("Edge_Edit_SectionContainer");
//				scrollToElement(bySectionContainer,"Section Container - DragComponent");
//			}else{
//				By bySectionContainer = getLocator("Edge_Edit_SectionContainer");
//				scrollToElement(bySectionContainer,"Section Container - DragComponent");
//			}
//		}else{
//			By bySectionContainer = getLocator("Edit_SectionContainer");
//			scrollToElement(bySectionContainer,"Section Container - DragComponent");
//		}
//	}
//	/**
//     * Description: mouse Hover to Element present in the application page using JavascriptExecutor class
//     * @param By byLocator - Locator of the Element in By Class reference
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
// 	public void mouseHoverJScript(String strLocator, String strElementDesc) throws Exception {
//		By byElement = getLocator(strLocator);
//		if (isElementVisible(byElement,strElementDesc)) {
//			WebElement e = driver.findElement(byElement);
//			mouseHoverJScript(e,strElementDesc);
//		}
//	}
//	/**
//     * Description: mouse Hover to Element present in the application page using JavascriptExecutor class
//     * @param WebElement element - Element present in the page
//     * @param String strElementDesc -  Description of the Element
//     * @throws Exception
//     */
//	public void mouseHoverJScript(WebElement HoverElement, String strElementDesc) {
//		try {
//			if (isElementVisible(HoverElement,strElementDesc)) {
//				JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//				String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
//				je.executeScript(mouseOverScript,HoverElement);
//			} else {
//				System.out.println("Element was not visible to hover " + "\n");
//			}
//		} catch (StaleElementReferenceException e) {
//			System.out.println("Element with " + HoverElement
//					+ "is not attached to the page document"
//					+ e.getStackTrace());
//		} catch (NoSuchElementException e) {
//			System.out.println("Element " + HoverElement + " was not found in DOM"
//					+ e.getStackTrace());
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println("Error occurred while hovering"
//					+ e.getStackTrace());
//		}
//	}
// 	
//	public static void setAttribute(WebElement element, String attributeName, String value){
//		  WrapsDriver wrappedElement = (WrapsDriver) element;
//		  JavascriptExecutor driver = 
//		  (JavascriptExecutor) wrappedElement.getWrappedDriver();
//		  driver.executeScript("arguments[0].setAttribute(arguments[1],arguments[2])", 
//		  element, attributeName, value);
//		}
//	
//	public void robotSetData(By by,String strElementDesc,String strInoutData) throws Exception {
//		report.updateTestLog("Set Data", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc , Status.DONE);
//		if(isElementVisible(by,strElementDesc)){
//			WebElement eElement = driver.findElement(by);
//			//highLightElement(eElement);				
//			javaScriptClick(by, strElementDesc);
//			waitFor(2000);
//			writeData(strInoutData);
//			report.updateTestLog("robotSetData", "Write data screenshot", Status.SCREENSHOT);
//			/*
//			StringSelection stringSelection = new StringSelection(strData);
//			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
//			clpbrd.setContents(stringSelection, null);
//			//eElement.sendKeys(Keys.CONTROL + "v");
//			Actions builder = new Actions(driver.getWebDriver());
//            builder.keyDown(eElement, Keys.CONTROL).perform();
//            builder.sendKeys(eElement, "v").perform();
//            builder.keyUp(eElement, Keys.CONTROL).perform();
//			
//			//setData(byParagraphText, "Paragrah Text Component",strParagraphText);	
//			//new Actions(driver.getWebDriver()).sendKeys(strParagraphText).perform();				
//			//WebElement eParagraphText = driver.findElement(byParagraphText);
//			//setAttribute(eParagraphText,"value",strParagraphText);
//			 * */		
//		}
//	}
//	
//	public synchronized void writeData(String strInoutData){
//		try {
//			StringSelection stringSelection = new StringSelection(strInoutData);
//			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
//			clpbrd.setContents(stringSelection, null);
//			Robot robot = new Robot();
//			robot.keyPress(KeyEvent.VK_CONTROL);
//			robot.keyPress(KeyEvent.VK_V);
//			robot.keyRelease(KeyEvent.VK_CONTROL);
//			robot.keyRelease(KeyEvent.VK_V);			
//		} catch (AWTException e) {
//			System.out.println("writeData " + strInoutData + " - data not entereted.");
//			report.updateTestLog("writeData", strInoutData + " - data not entereted." , Status.FAIL);
//			//e.printStackTrace();
//		}catch (Exception e) {
//			System.out.println("writeData Error:" + e.getMessage());
//			report.updateTestLog("writeData", strInoutData + " Error:" + e.getMessage() , Status.FAIL);
//			//e.printStackTrace();
//		}
//		/*
//		
//		String strComponentToVerify = dataTable.getData("MultiBrand_Data", "Component");
//		if(strComponentToVerify.equalsIgnoreCase("Carousel")){
//			//String myString = "This text will be copied into clipboard when running this code!";
//			try {
//				StringSelection stringSelection = new StringSelection(strData);
//				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
//				clpbrd.setContents(stringSelection, null);
//				Robot robot = new Robot();
//				robot.keyPress(KeyEvent.VK_CONTROL);
//				robot.keyPress(KeyEvent.VK_V);
//				robot.keyRelease(KeyEvent.VK_CONTROL);
//				robot.keyRelease(KeyEvent.VK_V);
//			} catch (AWTException e) {
//				System.out.println("writeData " + strData + " - data not entereted.");
//				report.updateTestLog("writeData", strData + " - data not entereted." , Status.FAIL);
//				//e.printStackTrace();
//			}catch (Exception e) {
//				System.out.println("writeData Error:" + e.getMessage());
//				report.updateTestLog("writeData", strData + " Error:" + e.getMessage() , Status.FAIL);
//				//e.printStackTrace();
//			}
//		}else{
//			Robot robot;
//			try {
//				 robot = new Robot();
//				 for (int i = 0; i < strData.length(); i++) {
//			        char c = strData.charAt(i);	        
//			       System.out.print( c );
//			        if (Character.isLetter(c)) {
//			        	if (Character.isUpperCase(c)) {
//				            robot.keyRelease(KeyEvent.VK_SHIFT);
//				        }
//			            //System.out.println(c + " - is an alphabet");
//				        robot.keyPress(c);
//				        robot.keyRelease(c); //Character.toUpperCase(c)
//				        
//				        if (Character.isUpperCase(c)) {
//				            robot.keyRelease(KeyEvent.VK_SHIFT);
//				        }
//				        //System.out.println(c + " - char entered");
//			        }else{
//			        	robot.keyPress(c);
//				        robot.keyRelease(c);
//				        //System.out.println(c + " - entered");
//			        }
//			       
//				  }
//				}catch (AWTException e) {
//					System.out.println("writeData " + strData + " - data not entereted.");
//					report.updateTestLog("writeData", strData + " - data not entereted." , Status.FAIL);
//					//e.printStackTrace();
//				}catch (Exception e) {
//					System.out.println("writeData Error:" + e.getMessage());
//					report.updateTestLog("writeData", strData + " Error:" + e.getMessage() , Status.FAIL);
//					//e.printStackTrace();
//				}
//				System.out.println();
//		}
//		
//		*/
//	}
//	public void hoverActionClick(String strLocator, String strElementDesc ) throws Exception {
//		By by = getLocator(strLocator);
//		hoverActionClick(by, strElementDesc);
//	}
//	public void hoverActionClick(By by, String strElementDesc ) throws Exception {
//		if (isElementPresent(by,strElementDesc)){
//			WebElement elementToClick = driver.findElement(by);
//			//highLightElement(element);
//			Actions action = new Actions((WebDriver) driver.getWebDriver());		 
//			action.moveToElement(elementToClick).click(elementToClick).build().perform();
//			System.out.println("Element Clicked: " + strElementDesc);
//			report.updateTestLog("hoverActionClick", "Element Clicked: " + strElementDesc , Status.DONE);
//		}else{
//			report.updateTestLog("hoverActionClick", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	public void hoverActionClick(WebElement elementToClick, String strElementDesc) throws Exception {		
//		if (isElementPresent(elementToClick,strElementDesc)){
//			//highLightElement(element);
//			Actions action = new Actions((WebDriver) driver.getWebDriver());		 
//			action.moveToElement(elementToClick).click(elementToClick).build().perform();
//			System.out.println("Element Clicked: " + strElementDesc);
//			report.updateTestLog("hoverActionClick", "Element Clicked: " + strElementDesc , Status.DONE);
//		}else{
//			report.updateTestLog("hoverActionClick", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	
//	public void hoverActionMoveToElement(By by, String strElementDesc ) throws Exception {
//		if (isElementPresent(by,strElementDesc)){
//			WebElement elementToClick = driver.findElement(by);
//			//highLightElement(element);
//			Actions action = new Actions((WebDriver) driver.getWebDriver());		 
//			action.moveToElement(elementToClick).build().perform();
//			//System.out.println("Element Clicked: " + strElementDesc);
//			report.updateTestLog("hoverActionMoveToElement", "Moved to Element: " + strElementDesc , Status.DONE);
//		}else{
//			report.updateTestLog("hoverActionMoveToElement", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//	}
//	
//	
//	public void actionMoveCursor(WebElement elementToMove, String strElementDesc) throws Exception{
//		if (isElementPresent(elementToMove,strElementDesc)){
//			Point p = elementToMove.getLocation();
//			int x = p.getX();
//			int y = p.getY();
//			Dimension d = elementToMove.getSize();
//			int h = d.getHeight();//To get the Height of the Element	
//			int w = d.getWidth();//To get the Width of the Element		
//			//System.out.println(" (X,Y): " + (x + (w/2)) +  " , " + (y+(h/2)));
//			
//			//System.out.println("Height: " + h + "Width: " + w);
//			Actions actionBuilder=new Actions( driver.getWebDriver());
//			Action drawOnCanvas=actionBuilder			               
//			               .moveToElement(elementToMove,-h, w)			               
//			               .moveByOffset(200, -20)
//			               .moveByOffset(-300,-20)
//			               .moveByOffset(-44,140)       	   
//			               .build();
//			drawOnCanvas.perform();	
//		}else{
//			report.updateTestLog("actionMoveCursor", "Element not Present: " + strElementDesc , Status.FAIL);
//		}
//		
//	}
//	
//	
/////////////////////////////////////////////   clickElement    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///**
//* Description: Click an Element present in the application page.
//* @param String strLocator - Locator of the Element
//* @param String strElementDesc -  Description of the Element
//* @throws Exception
//*/
//public void clickElement(String strLocator, String strElementDesc) throws Exception {		
//By by = getLocator(strLocator);
//clickElement(by, strElementDesc);
//}
///**
//* Description: Click an Element present in the application page.
//* @param By byLocator - Locator of the Element in By Class format
//* @param String strElementDesc -  Description of the Element
//* @throws Exception
//*/
//public void clickElement(By byLocator, String strElementDesc) throws Exception {
//	Boolean b = isElementVisible(byLocator,strElementDesc);
//	if (b){		
//		driver.findElement(byLocator).click();
//		System.out.println("Element Clicked: " + strElementDesc);
//		report.updateTestLog("clickElement", "Element Clicked: " + strElementDesc , Status.DONE);
//	}else{
//		report.updateTestLog("clickElement", "Element not Present: " + strElementDesc , Status.FAIL);
//	}
//}
///**
//* Description: Click an Element present in the application page.
//* @param WebElement element - Element present in the page
//* @param String strElementDesc -  Description of the Element
//* @throws Exception
//*/
//public void clickElement(WebElement element, String strElementDesc) throws Exception {	
//	Boolean b = isElementVisible(element,strElementDesc);
//	if (b){
//		element.click();
//		System.out.println("Element Clicked: " + strElementDesc);
//		report.updateTestLog("clickElement", "Element Clicked: " + strElementDesc , Status.DONE);
//	}else{
//		report.updateTestLog("clickElement", "Element not Present: " + strElementDesc , Status.FAIL);
//	}
//}
//public void mouseClickJScript(String strLocator, String strElementDesc) throws Exception {
//	By by = getLocator(strLocator);
//	mouseClickJScript(by,strElementDesc);
//}
//public void mouseClickJScript(By by, String strElementDesc) throws Exception {
//		if (isElementPresent(by,strElementDesc)) {
//			WebElement HoverElement = driver.findElement(by);
//			mouseClickJScript(HoverElement, strElementDesc);
//		}else{
//			report.updateTestLog("mouseClickJScript", "Element not Found: " + strElementDesc  , Status.FAIL);
//		}
//}
//public void mouseClickJScript(WebElement HoverElement, String strElementDesc) {
//	try {
//		if (isElementVisible(HoverElement,strElementDesc)) {
//			JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//			// Mouse move to Element
//			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
//			je.executeScript(mouseOverScript,HoverElement);
//			// Mouse click on the Element
//			mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('click',true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onclick');}";
//			je.executeScript(mouseOverScript,HoverElement);
//			System.out.println("mouseClickJScript: Element Clicked: " + strElementDesc);
//			report.updateTestLog("mouseClickJScript", "Element Clicked: " + strElementDesc , Status.DONE);
//		} else {
//			System.out.println("Element was not visible to MouseHover " + "\n");
//			report.updateTestLog("mouseClickJScript", "Element was not visible to MouseHover: " + strElementDesc , Status.FAIL);
//		}
//	} catch (StaleElementReferenceException e) {
//		System.out.println("Element with " + HoverElement
//				+ "is not attached to the page document"
//				+ e.getStackTrace());
//	} catch (NoSuchElementException e) {
//		System.out.println("Element " + HoverElement + " was not found in DOM"
//				+ e.getStackTrace());
//	} catch (Exception e) {
//		e.printStackTrace();
//		System.out.println("Error occurred while hovering"
//				+ e.getStackTrace());
//	}
//}
//
//
//public void javaScriptSetData(By by, String strElementDesc, String strInoutData,String strAtributeType) throws Exception {
//	Boolean b = isElementPresent(by,strElementDesc);
//	if (b){
//		WebElement element = driver.findElement(by);
//		try {
//			JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//			//((JavascriptExecutor) driver.getWebDriver()).executeScript("arguments[0].value='Kirtesh';", element);
//			////((JavascriptExecutor) driver.getWebDriver()).executeScript("arguments[0].innerHTML='Kirtesh';", element);
//			//je.executeScript("arguments[0].innerHTML='" + strValue + "';", element);
//			//je.executeScript("arguments[1].value = arguments[0]; ", strValue, element);
//			if(strAtributeType.equalsIgnoreCase("value")){
//				je.executeScript("arguments[1].value = arguments[0]; ", strInoutData, element);
//			}else if(strAtributeType.equalsIgnoreCase("aria-valuetext")){
//				je.executeScript("arguments[1].aria-valuetext = arguments[0]; ", strInoutData, element);
//			}else{
//				je.executeScript("arguments[1].innerHTML = arguments[0]; ", strInoutData, element);
//			}
//			System.out.println("Element Clicked: " + strElementDesc);
//			report.updateTestLog("javaScriptSetData", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc , Status.DONE);
//		} catch (Exception e) {
//			report.updateTestLog("javaScriptSetData", "Set data: (" + strInoutData + ") to Input Box Element: " + strElementDesc  + " - Failed." + e.getMessage(), Status.FAIL);
//		}
//	}else{
//		report.updateTestLog("javaScriptSetData", "Element not Found: " + strElementDesc  , Status.FAIL);
//	}
//}
//
//public Boolean waitForPageTitle(String strPageTitle) throws Exception{
//	Boolean bPageTitleFound = false;
//	try {
//		WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),100);
//		wait.until(ExpectedConditions.titleIs(strPageTitle));			
//	} catch (Exception e) {
//		System.out.println("strPageTitle: " + strPageTitle + " not loaded properly.");
//	}
//	By byPageTitle = By.xpath("//nav/div[@class='editor-GlobalBar-pageTitle']");
//	//wait.until(ExpectedConditions.textToBePresentInElementValue(byPageTitle, strActual_PageTitle));
//	int iCount = 3;
//	for(int j=0;j< iCount; j++){
//		if(isElementPresent(byPageTitle, "Page Title: " + strPageTitle)){
//			String strPageTitle2 = driver.findElement(byPageTitle).getText().trim();
//			if(strPageTitle2.equalsIgnoreCase(strPageTitle)){
//				j = iCount;
//				System.out.println("strPageTitle: " + strPageTitle);
//				bPageTitleFound = true;
//			}else{
//				waitFor(1000);
//			}
//		}else{
//			switchToPage(strPageTitle);
//		}
//	}
//	return bPageTitleFound;
//}
//
//public String javaScriptGetData(WebElement element, String strElementDesc) throws Exception{
//	String strActualData = "NotFound";
//	if(isElementPresent(element, strElementDesc)){
//		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//		strActualData = (String) je.executeScript("return arguments[0].innerHTML;",element);
//		//System.out.println("byCarousel_ButtonLabel: Found");
//	}
//	System.out.println("strActualData: " + strActualData);
//	return strActualData;
//}
//
//public void executeJavaScript(WebElement element, String strJavaScript) throws Exception{
//	String strActualData = "NotFound";
//	if(isElementPresent(element, "executeJavaScript - Element")){
//		JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//		je.executeScript(strJavaScript,element);
//		System.out.println("executeJavaScript done");
//	}
//	System.out.println("executeJavaScript executed");
//}
//
//public void openPage(String strPageLocation,String strPageName) throws Exception{
//	System.out.println("openPage - PageLocation:" + strPageLocation + " PageName: " +  strPageName);
//		navigateToMenu(strPageLocation);
//		Boolean bPageSelected = selectCreatedPage(strPageLocation,strPageName);   		
//	if(bPageSelected){		
//		javaScriptClick("ActionEdit", "Action Edit");// Click on the Edit coral-actionbar-item Button
//		openPageSync(); 			
//	}
//	else{
//		report.updateTestLog("openPage", strPageName + " Page is not opened for Edit." , Status.FAIL);
//		frameworkParameters.setStopExecution(true);
//	}    		
//}
//
///**
// * Customise the page url and save it to the "strColumnName" column in "strDataSheet" Sheet
// * @param strDataSheet - Sheet name present in the test Data Xl
// * @param strColumnName - Column name present in the "strDataSheet" Sheet
// */
//public void putUrlPath(String strDataSheet, String strColumnName){
//		String strPageUrl = driver.getCurrentUrl(); // Get Current Author Url
//		String[] tempArray = strPageUrl.split("editor.html", 2);
//		//int arrSize = tempArray.length;	
//		String temp2 = tempArray[1];
//		System.out.println("temp2: " + temp2);
//		tempArray = temp2.split(".html", 2);
//		temp2 = tempArray[0];
//		System.out.println(strColumnName + " - Value: " + temp2);
//		dataTable.putData(strDataSheet, strColumnName, temp2);
//}
//
//public void openIncognitoTab2() throws Exception{
//	Robot robot;
//	try {
//		robot = new Robot();
//		robot.keyPress(KeyEvent.VK_CONTROL);
//		robot.keyPress(KeyEvent.VK_SHIFT);
//		robot.keyPress(KeyEvent.VK_N);
//		robot.keyRelease(KeyEvent.VK_CONTROL);
//		robot.keyRelease(KeyEvent.VK_SHIFT);
//		robot.keyRelease(KeyEvent.VK_N);
//	} catch (AWTException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}	  
//	
//	//String strKey_Chord = Keys.chord(Keys.CONTROL, Keys.SHIFT, "N");
//	//driver.findElement(By.cssSelector("body")).sendKeys(strKey_Chord);
//	
////	String strKeys = Keys.chord(Keys.CONTROL, Keys.SHIFT, "n");
////	
////	//Keys.CONTROL
////	//Keys.SHIFT
////	Actions builder = new Actions(driver.getWebDriver());
////	WebElement eBody = driver.findElement(By.cssSelector("body"));
////	//Action mouseOverHome = builder.sendKeys(eBody, strKeys).build();
////	
////	builder.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("n").build().perform();
//	
//	//mouseOverHome.perform();
//
//	
//	//driver.findElement(By.xpath("//body")).sendKeys(strKeys);
//	
//	
//	
//  //report.updateTestLog("openIncognitoTab", "chrome in incognito mode",Status.PASS);
// ArrayList<String> strWindowHandles = new ArrayList<String> (driver.getWindowHandles());
// int iCount = strWindowHandles.size();
// System.out.println("strWindowHandles.size: " + iCount);
// String strwindowHandle = strWindowHandles.get(iCount-1);
//driver.switchTo().window(strwindowHandle);
//waitFor(3000);
//report.updateTestLog("openIncognitoTab", "In incognito mode",Status.PASS);
//}
//
//public void openIncognitoTab() throws Exception{
//	Robot robot;
//	try {
//		robot = new Robot();
//		robot.keyPress(KeyEvent.VK_CONTROL);
//		robot.keyPress(KeyEvent.VK_SHIFT);
//		robot.keyPress(KeyEvent.VK_N);
//		robot.keyRelease(KeyEvent.VK_CONTROL);
//		robot.keyRelease(KeyEvent.VK_SHIFT);
//		robot.keyRelease(KeyEvent.VK_N);
//	} catch (AWTException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//	
////	String selectAll = Keys.chord(Keys.ALT, Keys.SHIFT,"z");
////	driver.findElement(By.tagName("html")).sendKeys(selectAll);
//	
////	String newPrivateTab = Keys.chord(Keys.CONTROL , Keys.SHIFT , "N");
////	driver.findElement(By.cssSelector("body")).sendKeys(newPrivateTab);
//	
//	waitFor(3000);
//  //report.updateTestLog("openIncognitoTab", "chrome in incognito mode",Status.PASS);
// ArrayList<String> strWindowHandles = new ArrayList<String> (driver.getWindowHandles());
// int iCount = strWindowHandles.size();
// System.out.println("strWindowHandles.size: " + iCount);
// String strwindowHandle = strWindowHandles.get(iCount-1);
//driver.switchTo().window(strwindowHandle);
//waitFor(3000);
//report.updateTestLog("openIncognitoTab", "In incognito mode",Status.PASS);
//}
//	
///**
// * Description: Click CheckboxElement present in the application page using JavascriptExecutor class
// * @param By byLocator - Locator of the Element in By Class reference
// * @param String strElementDesc -  Description of the Element
// * @param String strCheckFlag -  Enable or Disable Check box Element
// * @throws Exception
// */
//	public void clickCheckBox(By by, String strElementDesc,String strCheckFlag) throws Exception {
//		if(isElementVisible(by,strElementDesc)){
//			String strV1 = driver.findElement(by).getAttribute("checked");
//			System.out.println("CheckBox - " + strElementDesc + " : " + strV1);
//			//if(isElementNotVisible(getLocator("Home_SelfCertification_EnableChecked"),"Home_SelfCertification_EnableChecked")){
//			//	mouseClickJScript("Home_SelfCertification_Enable","Home_SelfCertification_Enable"); // click Home_SelfCertification_Enable
//			//}
//			if(strCheckFlag.equalsIgnoreCase("Yes")){
//				javaScriptClick(by,"CheckBox: " + strElementDesc);
//			}else{
//				report.updateTestLog("clickCheckBox", "CheckBox: " + strElementDesc + " - Checkbox is not Checked", Status.DONE);
//			}
//		}else{
//			report.updateTestLog("clickCheckBox", "CheckBox: " + strElementDesc + " - Checkbox is not Visible", Status.DONE);
//		}
//		
//	}
//	/**
//	 * Description: Click CheckboxElement present in the application page using JavascriptExecutor class
//	 * @param By byLocator - Locator of the Element in By Class reference
//	 * @param String strElementDesc -  Description of the Element
//	 * @param String strCheckFlag -  Enable or Disable Check box Element
//	 * @throws Exception
//	 */
//		public void clickCheckBox(String strLocator, String strElementDesc,String strCheckFlag) throws Exception {
//			By by = getLocator(strLocator);
//			clickCheckBox(by,strElementDesc,strCheckFlag);
//		}
//		
//		public void dragAndDrop(WebElement sourceElement, WebElement destinationElement) {
//			try {
//				System.out.println("dragAndDrop 1");
//				if (sourceElement.isDisplayed() && destinationElement.isDisplayed()) {
//					Actions action = new Actions(driver.getWebDriver());
//					System.out.println("dragAndDrop 2");
//					action.dragAndDrop(sourceElement, destinationElement).build().perform();
//					System.out.println("dragAndDrop 3");
//				} else {
//					System.out.println("Element was not displayed to drag");
//				}
//			} catch (StaleElementReferenceException e) {
//				System.out.println("Element with " + sourceElement + "or" + destinationElement + "is not attached to the page document "
//						+ e.getStackTrace());
//			} catch (NoSuchElementException e) {
//				System.out.println("Element " + sourceElement + "or" + destinationElement + " was not found in DOM "+ e.getStackTrace());
//			} catch (Exception e) {
//				System.out.println("Error occurred while performing drag and drop operation "+ e.getStackTrace());
//			}
//			System.out.println("dragAndDrop4");
//		}
//		
//		public Boolean waitUntilAttrubuteContains(By byLocator, String strElementDesc, String strAttribute,String strExpectedValue) throws Exception{
//			Boolean bWait = false;
//			//if(isElementVisible(byLocator, strElementDesc)){
//			if(isElementPresent(byLocator, strElementDesc)){
//				try {
//					WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),20);
//					wait.until(ExpectedConditions.attributeContains(byLocator, strAttribute, strExpectedValue));
//					bWait = true;
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					//e.printStackTrace();
//				}
//			}
//			return bWait;			
//		}
//		public Boolean waitUntilAttrubuteContains(By byLocator, String strElementDesc, String strAttribute,String strExpectedValue, int iWaitTime) throws Exception{
//			Boolean bWait = false;
//			//if(isElementVisible(byLocator, strElementDesc)){
//			if(isElementPresent(byLocator, strElementDesc)){
//				try {
//					WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),iWaitTime);
//					wait.until(ExpectedConditions.attributeContains(byLocator, strAttribute, strExpectedValue));
//					bWait = true;
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					//e.printStackTrace();
//				}
//			}
//			return bWait;			
//		}
//		
//		public void verifyPageElement(String strLocator,String strElementDesc,String strAttribure,String strExpectedValue,String strVerifyType,String strScreenShotReq) throws Exception{
//			System.out.println("verifyElementVisible: " + strElementDesc);
//			By byLocator = getLocator(strLocator);
//			verifyPageElement(byLocator,strElementDesc,strAttribure,strExpectedValue,strVerifyType,strScreenShotReq);			
//		}//End of verifyPageElement()
//		public void verifyPageElement(By byLocator,String strElementDesc,String strAttribure,String strExpectedValue,String strVerifyType,String strScreenShotReq) throws Exception{
//			System.out.println("verifyElementVisible: " + strElementDesc);
//			if(driverUtil.objectExists(byLocator)){
//			//if(isElementVisible(byLocator,strElementDesc)){
//				String strActualValue = getData(byLocator,strElementDesc,strAttribure);
//				System.out.println(strElementDesc +" ActualValue: " + strActualValue);
//				verifyActualExpectedData(strElementDesc, strActualValue, strExpectedValue,strVerifyType,strScreenShotReq);
//			}else{
//				report.updateTestLog("verifyPageElement","Element not Visible: " + strElementDesc, Status.FAIL);
//			}
//		}//End of verifyPageElement()
//		
////		public void updateProperty(String strKey, String strValue) throws Exception {
////			PropertiesConfiguration config = new PropertiesConfiguration();			
////			Reader reader = new FileReader("Global Settings.properties");
////			config.read(reader);
////			config.setProperty(strKey, strValue);		
////			Writer writer = new FileWriter("Global Settings.properties");
////			config.write(writer);			 
////			System.out.println("Global Settings.properties Successfully Updated..");
////		}//End of updateProperty()
//		
//		public void setTakeScreenshot(Boolean bsetTakeScreenshot){
//			SeleniumTestParameters t = driver.getTestParameters();
//			String strBrowserName = t.getBrowser().toString();
//			if(strBrowserName.equalsIgnoreCase("INTERNET_EXPLORER")){
//				reportSettings.setTakeScreenshotFailedStep(bsetTakeScreenshot);
//				reportSettings.setTakeScreenshotPassedStep(bsetTakeScreenshot);
//			}
//		}
//		
//		public void handleSelfCertificationPopup() throws Exception{
//			//if(driverUtil.objectExists(getLocator("SelfCertificationDialog"))){
//			if(isElementVisible("SelfCertificationDialog","SelfCertificationDialog",10)){
//				report.updateTestLog("SelfCertificationDialog",  "SelfCertification Dialog", Status.SCREENSHOT);
//				SeleniumTestParameters t = driver.getTestParameters();
//				String strTestCase = t.getCurrentTestcase();
//				//if(strTestCase.contains("Header & Footer") || strTestCase.contains("getDataFromPage")){ // test Cookies content for Header & Footer Test cases only.
//				if(strTestCase.contains("Header & Footer1234567") || strTestCase.contains("getDataFromPage1234567")){
//					String strDataSheet = "HeaderFooter";
//					String strupdateMissMatchToXL = dataTable.getData("VerifyPage", "updateMissMatchToXL");
//					String strSelfCert_Heading = getData("SelfCertification_Heading","SelfCertification_Heading","Text");
//			 		String strSelfCert_P = getData("SelfCertification_P","SelfCertification_P","Text");
//			 		String strSelfCert_PLinkTxt = getData("SelfCertification_PLink","SelfCertification_PLink","Text");
//			 		String strSelfCert_PLinkUrl = getData("SelfCertification_PLink","SelfCertification_PLink","href");		 		
//			 		String strSelfCert_NoLinkTxt = getData("SelfCertification_NoLink","SelfCertification_NoLink","text");
//			 		String strSelfCert_NoLinkUrl = getData("SelfCertification_NoLink","SelfCertification_NoLink","href");
//			 		String strSubmitButtonTxt = getData("SelfCertification_SubmitButton","SelfCertification SubmitButton","text");
//			 		strSelfCert_PLinkUrl = customizeUrl(strSelfCert_PLinkUrl);		 		
//			 		strSelfCert_NoLinkUrl = customizeUrl(strSelfCert_NoLinkUrl);
//					if(strupdateMissMatchToXL.equalsIgnoreCase("Yes")){//get
//						dataTable.putData(strDataSheet, "SelfCert_Heading", strSelfCert_Heading);
//						dataTable.putData(strDataSheet, "SelfCert_P", strSelfCert_P);
//						dataTable.putData(strDataSheet, "SelfCert_PLink", strSelfCert_PLinkTxt + "::" + strSelfCert_PLinkUrl);					
//						if(isElementVisible("SelfCertification_FormGroup","SelfCertification_FormGroup")){
//				 			String strSelfCert_FormLabel = getData("SelfCertification_FormLabel","SelfCertification_FormLabel","Text");
//					 		String strSelfCert_FormInputPlaceHolder = getData("SelfCertification_Input","SelfCertification_Input","placeholder");
//					 		dataTable.putData(strDataSheet, "SelfCert_FormLabel", strSelfCert_FormLabel);
//					 		dataTable.putData(strDataSheet, "SelfCert_FormInputPlaceHolder", strSelfCert_FormInputPlaceHolder);
//				 		}
//						dataTable.putData(strDataSheet, "SelfCert_NoLink", strSelfCert_NoLinkTxt + "::" + strSelfCert_NoLinkUrl);
//						dataTable.putData(strDataSheet, "SelfCert_SubmitButtonTxt", strSubmitButtonTxt);
//					}else{//verify
//						String strXLSelfCert_Heading = dataTable.getData(strDataSheet, "SelfCert_Heading");
//				 		String strXLSelfCert_P = dataTable.getData(strDataSheet, "SelfCert_P");
//				 		String strXLSelfCert_PLink = dataTable.getData(strDataSheet, "SelfCert_PLink");			 				 		
//				 		String strXLSelfCert_NoLink = dataTable.getData(strDataSheet, "SelfCert_NoLink");			 		
//				 		String strXLSubmitButtonTxt = dataTable.getData(strDataSheet, "SelfCert_SubmitButtonTxt");
//				 		String strSelfCert_FormLabel = dataTable.getData(strDataSheet, "SelfCert_FormLabel");
//				 		String strSelfCert_FormInputPlaceHolder = dataTable.getData(strDataSheet, "SelfCert_FormInputPlaceHolder");
//				 		
//			 			verifyActualExpectedData("SelfCert_Heading",strSelfCert_Heading,strXLSelfCert_Heading,"Exact","No");	 			
//			 			verifyActualExpectedData("SelfCert_P",strSelfCert_P,strXLSelfCert_P,"Exact","No");
//			 			//String[] arystrSelfCert_NoLink = strXLSelfCert_PLink.split("::");
//			 			String[] arystrSelfCert_NoLink = stringToArray(strXLSelfCert_PLink,"::");
//			 			verifyActualExpectedData("SelfCert_PLinkTxt",strSelfCert_PLinkTxt,arystrSelfCert_NoLink[0],"Exact","No");
//			 			verifyActualExpectedData("SelfCert_PLinkUrl",strSelfCert_PLinkUrl,arystrSelfCert_NoLink[1],"Partial","No");
//			 			if(isElementVisible("SelfCertification_FormGroup","SelfCertification_FormGroup")){
//				 			String strXLSelfCert_FormLabel = getData("SelfCertification_FormLabel","SelfCertification_FormLabel","Text");
//					 		String strXLSelfCert_FormInputPlaceHolder = getData("SelfCertification_Input","SelfCertification_Input","placeholder");
//					 		verifyActualExpectedData("SelfCert_FormLabel",strSelfCert_FormLabel,strXLSelfCert_FormLabel,"Exact","No");
//					 		verifyActualExpectedData("SelfCert_FormInputPlaceHolder",strSelfCert_FormInputPlaceHolder,strXLSelfCert_FormInputPlaceHolder,"Exact","No");
//				 		}
//			 			//String[] arySelfCert_NoLink = strXLSelfCert_NoLink.split("::");
//			 			String[] arySelfCert_NoLink = stringToArray(strXLSelfCert_NoLink,"::");
//			 			verifyActualExpectedData("SelfCert_NoLinkTxt",strSelfCert_NoLinkTxt,arySelfCert_NoLink[0],"Exact","No");
//			 			verifyActualExpectedData("SelfCert_NoLinkUrl",strSelfCert_NoLinkUrl,arySelfCert_NoLink[1],"Partial","No");
//			 			verifyActualExpectedData("SelfCert_NoLinkTxt",strSubmitButtonTxt,strXLSubmitButtonTxt,"Exact","No");
//					}		 		
//				}//HeaderFooter
//				if(isElementVisible("SelfCertification_Input","SelfCertification_Input")){
//					String strUrl = driver.getCurrentUrl();
//					if(strUrl.contains("/content/multibrand/be/")){
//						String strBECode = properties.getProperty("Belgium_Code");
//						setData("SelfCertification_Input","SelfCertification_Input",strBECode);
//					}
//				}
//				String strSubmitButtonTxt = getData("SelfCertification_SubmitButton","SelfCertification SubmitButton","text");
//				javaScriptClick("SelfCertification_SubmitButton",strSubmitButtonTxt);
//				isElementNotVisible("SelfCertification_SubmitButton","SelfCertification Submit Button",50);		
//			}
//		}
//		
//		public String[] stringToArray(String strLinkWithTxt,String strDelimiter){
//	 		String[] aryLink = {strLinkWithTxt, "No Data"};
//	 		if(strDelimiter.equals("::")){ 			
//	 			if(strLinkWithTxt.contains(strDelimiter)){
//	 				aryLink = strLinkWithTxt.split(strDelimiter);
//	 			}
//	 		} 		
//			return aryLink;
//	 	}//stringToArray
//		
//		public String customizeUrl(String strUrl){
//	 		if(strUrl != null && strUrl.contains("digital-astrazeneca.com:4503")){			
//				strUrl = strUrl.substring(strUrl.indexOf("digital-astrazeneca.com:4503"));
//			}
//	 		if(strUrl != null && strUrl.contains("/content")){			
//				strUrl = strUrl.substring(strUrl.indexOf("/content/"));
//			}
//	 		
//			return strUrl;
//	 	}//customizeUrl
//		
//		public void verifyLink(By by, String strElementDesc, String strLinkUrl, String strPageOpenedInNew) throws Exception{
//	 		String strCurrentMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
//	 		if(isElementVisible(by,strElementDesc)){
//	 			WebElement e = driver.findElement(by);
//	 			verifyLink(e, strElementDesc, strLinkUrl, strPageOpenedInNew);	 			
//			}else{
//				report.updateTestLog(strCurrentMethodName,strElementDesc + " not Present", Status.FAIL);
//			}
//	 	}
//		
//		public void verifyLink(WebElement e, String strElementDesc, String strLinkUrl, String strPageOpenedInNew) throws Exception{
//	 		String strCurrentMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
//	 		if(isElementVisible(e,strElementDesc)){
//	 			highLightElement(e);
//	 			String strActualValue = e.getAttribute("href");
//	 			//verifyPageOpenedInNewWindow2(e,strElementDesc,strLinkUrl,strPageOpenedInNew);
//	 			
//	 			if(!strLinkUrl.isEmpty() && strActualValue.contains(strLinkUrl)){
//	 				System.out.println(strElementDesc + ": " + strLinkUrl);
//	 				verifyActualExpectedData(strElementDesc, strActualValue, strLinkUrl,"Partial","Yes");
//	 				//verifyPageElement(strLocator,strElementDesc,"href",strLinkUrl,"Partial","Yes");
//	 				//WebElement e = driver.findElement(getLocator(strLocator));			                 
//	 	            verifyPageOpenedInNewWindow2(e,strElementDesc,strLinkUrl,strPageOpenedInNew);
//	 	            //scrollToElement(e,strElementDesc);
//	 			}else{
//	 				report.updateTestLog(strCurrentMethodName,"Expected Link Url File Url:(" + strLinkUrl + ") is null or it is not matching with Actual value: ( "+ strActualValue + ")", Status.FAIL);
//	 			}
//	 			
//			}else{
//				report.updateTestLog(strCurrentMethodName,strElementDesc + " not Present", Status.FAIL);
//			}
//	 	}
//		/**
//	     * Description: verify whether a Page is opened in New Window or Not
//	     * @param WebElement e - Element in which specified Component to be inserted.
//	     * @param String strElementDesc -  Description of the Element
//	     * @param String strNewWindowOption - strNewWindowOption (yes, no).
//	     * @throws Exception
//	     */
//		public void verifyPageOpenedInNewWindow2(WebElement e, String strElementDesc,String strLinkUrl,String strNewWindowOption) throws Exception{
//			String strCurrentMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
//			SeleniumTestParameters t = driver.getTestParameters();
//			String strBrowserName = t.getBrowser().toString();
//			String strExecutionMode = t.getExecutionMode().toString();
//			String strMobileExecutionPlatform = t.getMobileExecutionPlatform().toString();
//			String strParentPageTitle = driver.getTitle();
//			String strParentPageUrl = driver.getCurrentUrl();
//			String strPageUrl = driver.getCurrentUrl();
//			System.out.println("strParentPageTitle: " + strParentPageTitle);    
//		    String oldTabWinHandle = driver.getWindowHandle();// change focus to new tab
//		    System.out.println("oldTabWinHandle:" + oldTabWinHandle);
//		    ArrayList<String> availableWindowsBefore = new ArrayList<String>(driver.getWindowHandles());
//		    int iWindowcount = availableWindowsBefore.size();
//		    System.out.println("Page Count Before: " + iWindowcount);
//		    //highLightElement(e);
//		    //String strNavigateLink = e.getAttribute("href");	   
//		    //report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Navigate to Url:" + strNavigateLink, Status.DONE);	    
//		    
//		    javaScriptClick(e,strElementDesc);  ///  Click on the Link  ////////////////////////
//		    //////////////// Special case to handle Exit Ramp Popups
//		    handleExitRamp();	    
//		    waitFor(3000);
//		    ArrayList<String> availableWindowsAfter = new ArrayList<String>(driver.getWindowHandles());
//		    int iWindowcountAfter = availableWindowsAfter.size();
//		    System.out.println("Page Count After: " + iWindowcountAfter);
//		    //report.updateTestLog(strCurrentMethodName,  strElementDesc + " - New Window Option:" + strNewWindowOption, Status.DONE);
//		    try {
//	    		///////////////////////////////////////////////////////// NewWindowOption is NO
//	 			//report.updateTestLog(strCurrentMethodName,   "Verify Page opened in same Window OR NOT", Status.DONE);
//			    if (iWindowcount == iWindowcountAfter){  // Check Page opened in Same Window
//			    	System.out.println("Page opened in same Window");
//				    strPageUrl = driver.getCurrentUrl();			    
//				    //handleExitRamp();
//				    handleSelfCertificationPopup();
//					report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Page opened in same Window as expected. URL is: " + strPageUrl, Status.DONE);
//					String strCurrenrt = strPageUrl;
//					//String strLinkUrl2 = strLinkUrl.replaceAll("https://", "http://");
//					String strLinkUrl2 = strLinkUrl.replaceFirst("https://", "");
//					strLinkUrl2 = strLinkUrl2.replaceFirst("http://", "");
//					strLinkUrl2 = strLinkUrl2.replaceFirst("www", "");
//					boolean bVerify = verifyActualExpectedData2(strElementDesc, strPageUrl,strLinkUrl2, "Partial","No");				
//					if(bVerify){
//				    	report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Navigate to Url:" + strLinkUrl, Status.PASS);
//				    }else{
//				    	report.updateTestLog(strCurrentMethodName , strElementDesc + ", Actual PageUrl: (" + strPageUrl + 
//								") not matches with Expected PageUrl: (" + strLinkUrl + ")", Status.FAIL);
//				    }////////////////////
//					/*
//					if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//						System.out.println("strBrowserName: " + strBrowserName);
//						JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//						je.executeScript("window.history.back();");
//						waitFor(5000);
//						report.updateTestLog(strCurrentMethodName, " - Firefox goback", Status.SCREENSHOT);
//					}else{
//						String strPageTitle = driver.getTitle();
//			            System.out.println("Current Page Title: " + strPageTitle );
//			            System.out.println("driver.navigate().back();" );
//						driver.navigate().back();
//					}				*/
//					System.out.println("Switch Back to Parent window" );
//		            String strPageTitle = driver.getTitle();
//		            String strCurrentPageUrl = driver.getCurrentUrl();
//		            System.out.println("Current Page Title: " + strPageTitle );
//		            ///////////////////////////////////////String strActual_PageTitle = dataTable.getData("MultiBrand_Data", "Page_PageTitle");
//		            
//	                if(strCurrentPageUrl.equalsIgnoreCase(strParentPageUrl)){
//	                	///////////// No Swich
//	                }else{
//	                	if(strExecutionMode.equalsIgnoreCase("BROWSER_STACK") && strBrowserName.equalsIgnoreCase("FIREFOX")){
//							System.out.println("strBrowserName: " + strBrowserName);
//							JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//							je.executeScript("window.history.back();");
//							waitFor(5000);
//							//report.updateTestLog("verifyPageOpenedInNewWindow", " - Firefox goback", Status.SCREENSHOT);
//						}else{
//							System.out.println("strBrowserName: " + strBrowserName);
//							System.out.println("Click Back button");
//							//driver.navigate().back();
//							JavascriptExecutor je = (JavascriptExecutor) driver.getWebDriver();
//							je.executeScript("window.history.back();");
//							waitFor(5000);
//						}
//	                	try {
//	        				WebDriverWait wait = new WebDriverWait((WebDriver) driver.getWebDriver(),10);
//	        				wait.until(ExpectedConditions.titleIs(strParentPageTitle));		
//	        			} catch (Exception ee) {
//	        				System.out.println("strPageTitle: " + strParentPageTitle + " not loaded properly.");
//	        			}
//	                }
//					report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else if (iWindowcount < iWindowcountAfter){
//					driver.switchTo().window(availableWindowsAfter.get(iWindowcountAfter - 1));
//					strPageUrl = driver.getCurrentUrl();
//					report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Page opened in new Window. Url is: " + strPageUrl, Status.PASS);				
//					String strLinkUrl2 = strLinkUrl.replaceFirst("https://", "");
//					strLinkUrl2 = strLinkUrl2.replaceFirst("http://", "");
//					strLinkUrl2 = strLinkUrl2.replaceFirst("www", "");
//					//report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Page opened in same Window as expected. URL is: " + strPageUrl, Status.PASS);
//					if(verifyActualExpectedData2(strElementDesc, strPageUrl,strLinkUrl2, "Partial","No")){
//				    	report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Navigate to Url:" + strPageUrl, Status.DONE);
//				    }else{
//				    	report.updateTestLog(strCurrentMethodName , strElementDesc + ", Actual PageUrl: (" + strPageUrl + 
//								") not matches with Expected PageUrl: (" + strLinkUrl + ")", Status.FAIL);
//				    }////////////////////
//					driver.close();////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//					driver.switchTo().window(oldTabWinHandle);
//					System.out.println("Switch to Parent Window: " + strParentPageTitle);
//					switchToPage(strParentPageTitle);
//					report.updateTestLog(strCurrentMethodName,  strElementDesc + " - Switch Back to Parent Window", Status.DONE);
//				}else{
//					/// Not Possible (iWindowcount > iWindowcountAfter)
//				} //End of iWindowcount compare with iWindowcountAfter	    
//			 	
//									
//			} catch (TimeoutException ex) {
//				//ex.printStackTrace();
//				switchToPage(strParentPageTitle);
//			} 		
//	 	} //Method End
//		public void handleExitRamp() throws Exception{
//			if(isElementVisible("VerifyExitRamp_Continue","VerifyExitRamp_Continue",5)){
//				javaScriptClick("VerifyExitRamp_Continue","VerifyExitRamp_Continue");
//				isElementNotVisible("VerifyExitRamp_Continue","VerifyExitRamp_Continue");
//			}
//		}
//		
//		
//		public void switchToPreviewMode(String strOption){
//			if(strOption.equalsIgnoreCase("Preview")){
//				try {
//					driver.switchTo().defaultContent();				
//					driver.switchTo().frame("ContentFrame");
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					report.updateTestLog("switchToPreviewMode","Switch to Preview Mode Failed", Status.FAIL);
//				}
//				System.out.println("Switch Back to Preview - Content Frame" );												
//			}
//		}//switchToPreviewMode
//		
//		public void doubleClickByJSE(By by, String strElementDesc) throws Exception {
//			Boolean bElementPresent = isElementPresent(by,strElementDesc);
//			if(bElementPresent){
//				JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
//				js.executeScript("arguments[0].doubleClick();", driver.findElement(by));
//				report.updateTestLog("Verify the Element <b>"+strElementDesc+"</b> is clicked", "The Element <b>"+strElementDesc+"</b> is clicked", Status.DONE);
//				System.out.println("The Element: "+ strElementDesc + " is double clicked");
//			}else{
//				report.updateTestLog("Verify the Element <b>"+strElementDesc+"</b> is clicked", "The Element <b>"+strElementDesc+"</b> is not present", Status.FAIL);
//			}
//		}
//		
//		
//		 public void doubleClick(By by,String strElementDesc) throws Exception{			 
//			 Boolean bElementVisible = isElementVisible(by,strElementDesc);
//				if(bElementVisible){
//					Actions action = new Actions(driver.getWebDriver());
//					 //Find the targeted element
//					 WebElement ele = driver.findElement(by);
//					                //Here I used JavascriptExecutor interface to scroll down to the targeted element
//					// ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", ele);
//					                //used doubleClick(element) method to do double click action
//					 action.doubleClick(ele).build().perform();
//				}
//			 
//			                
//			 }//doubleClick()
//
//		 /**
//		  * Converts a hex string to a color. If it can't be converted null is returned.
//		  * @param hex (i.e. #CCCCCCFF or CCCCCC)
//		  * @return Color
//		  */
//		 public String hexToRGBColor(String hex){
//			 Color color = null;
//		     hex = hex.replace("#", "");
//		     switch (hex.length()) {
//		         case 6:
//		        	 color = new Color(
//		             Integer.valueOf(hex.substring(0, 2), 16),
//		             Integer.valueOf(hex.substring(2, 4), 16),
//		             Integer.valueOf(hex.substring(4, 6), 16));
//		        	 break;
//		         case 8:
//		        	 color = new Color(
//		             Integer.valueOf(hex.substring(0, 2), 16),
//		             Integer.valueOf(hex.substring(2, 4), 16),
//		             Integer.valueOf(hex.substring(4, 6), 16),
//		             Integer.valueOf(hex.substring(6, 8), 16));
//		        	 break;
//		        default:
//		        	report.updateTestLog("hexToRGBColor", "Error in Hex Color code",Status.FAIL);
//					
//		     }
//		     //return null;
//		     StringBuffer sb = new StringBuffer();
//		     if(color != null){		    	 
//			     sb.append("rgb(");
//			     sb.append(color.getRed());
//			     sb.append(", ");
//			     sb.append(color.getGreen());
//			     sb.append(", ");
//			     sb.append(color.getBlue());
//			     sb.append(")");
//		     }
//		     
//		     return sb.toString();
//		 }//hexToRGBColor()
		

}//End Class