package supportlibraries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.objCallsForMedical;
import ObjectRepository.objHeaderPage;
import ObjectRepository.objVeevaAdvanceCoachingReport;

public class VeevaFunctions extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	public VeevaFunctions(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
	Date dNow = new Date();
	public static String date;
	public static String datecalls;

	public void clickVeevaTab2(String strTabName) throws Exception
	{
		Boolean tab = false;
		cf.clickLink(objVeevaAdvanceCoachingReport.plussymbol, "Plus Symbol");
		if (cf.isElementVisible(By.xpath("//table[@class='detailList tabs']//a[.='" + strTabName + "']"), strTabName))
		{
			cf.clickLink(By.xpath("//table[@class='detailList tabs']//a[.='" + strTabName + "']"), " " + strTabName + " Link");
			tab = true;
		}
		if (tab == true)
			report.updateTestLog("Verify able to click on " + strTabName + "", "able to click on Tab Link: " + strTabName + "", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to click on " + strTabName + "", "Unable to click on Tab Link: " + strTabName + "", Status.FAIL);
		}
	}

	public void selectaccount() throws Exception
	{
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		String accounttype = dataTable.getData("Login", "AccountType");
		String account = dataTable.getData("Login", "AccountName");
		if (account.contains(";"))
		{
			String[] acc = account.split(";");
			account = acc[0];
			System.out.println("Spilt using (;) Acocunt Name: " + account);
		}
		if (account.contains(","))
		{
			String[] account1 = account.split(",");
			account = account1[1] + " " + account1[0];
			System.out.println("Spilt using (,) Acocunt Name: " + account);
		}

//		else if (locale.equals("RU"))
//		{
//			if (account.contains(" "))
//			{
//				if (accounttype.equals("Hospital"))
//					account = account;
//				else
//				{
//					String account1[] = account.split(" ");
//					account = account1[1] + " " + account1[0];
//				}
//
//			}
//			else
//				account = account;
//
//		}
		else if (locale.equals("JP"))
		{
			account = account;
		}
		try
		{
			searchAccountForRecordCall(locale, profile, accounttype, account);
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void searchAccountForRecordCall(String locale, String profile, String accounttype, String account) throws Exception
	{
		if (cf.isElementVisible(objHeaderPage.textSearch, "Search Box"))
		{
			if (cf.isElementPresent(By.id("sen")))
			{
				cf.selectData(By.id("sen"), "Search Module Dropdown", "Accounts");
				cf.setData(objHeaderPage.textSearch, "Search Box", account);
				if(cf.isElementPresent(By.xpath("//strong[contains(text(),'"+account+"')]")))
					cf.clickElement(By.xpath("//strong[contains(text(),'"+account+"')]"), "Search Account Suggestions");
				else
				cf.clickButton(objHeaderPage.btnGo, "Search GO");
			}
			else
			{
				cf.clickButton(By.xpath("//a[.='Advanced Search...']"), "Advanced Search");
				wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".searchBoxInput>input[value='Search']")));
				cf.setData(By.cssSelector(".searchBoxInput>input"), "Search in Advanced Search", account);
				cf.clickButton(By.xpath("//label[.='Accounts']/preceding-sibling::input[@type='checkbox']"), "Choose Account");
				cf.clickButton(By.cssSelector(".searchBoxInput>input[value='Search']"), "Search Button");
			}

			if (driver.findElements(By.cssSelector(".bPageTitle h1")).size() > 0)
			{
				if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("search results"))
				{
					if (cf.isElementVisible(By.xpath("//strong[contains(text(),'There are no matching')]"), "There are no matching - Accounts"))
					{
						for (int i = 1; i <= 2; i++)
						{
							account = dataTable.getData("Login", "AccountName");
							if (account.contains(";"))
							{
								String[] acc = account.split(";");
								account = acc[i];
								System.out.println("Spilt using (;) Acocunt Name: " + account);
							}
							if (account.contains(","))
							{
								String[] account1 = account.split(",");
								account = account1[1] + " " + account1[0];
								System.out.println("Spilt using (,) Acocunt Name: " + account);
							}
							else if (locale.equals("RU") && profile.equals("Commercial"))
							{
								String account1[] = account.split(" ");
								account = account1[1] + " " + account1[0];
							}
							else if (locale.equals("JP"))
							{
								account = account.substring(2, 5) + account.substring(0, 2);
							}
							cf.setData(By.xpath("(//input[@class='searchTextBox'])[2]"), "Search Box", account);
							cf.clickElement(By.xpath("//input[@value='Search']"), "Search Button");
							if (cf.isElementPresent(By.xpath("//strong[contains(text(),'There are no matching')]")) != true)
							{
								break;
							}
						}
					}
					else if (driver.findElement(By.xpath("//span[@class='searchFirstCell']")).getText().trim().toLowerCase().contains("accounts"))
					{
						if (accounttype.contains("Professional"))
						{
							cf.clickElement(By.xpath("((//tr[contains(@class,'dataRow')]//th/following-sibling::td[text()='Professional'])/preceding-sibling::th/a)[1]"), "Account Type - Professional");

						}
						if (accounttype.contains("Hospital"))
						{
							String recordType = driver.findElement(By.xpath("//tr[contains(@class,'dataRow')]//th/following-sibling::td")).getText().trim();

							if (recordType.equals("Hospital"))
							{
								cf.clickElement(By.xpath("((//tr[contains(@class,'dataRow')]//th/following-sibling::td[text()='Hospital'])/preceding-sibling::th/a)[1]"), "Account Type - Hospital");
							}

							if (recordType.equals("Organization"))
							{
								cf.clickElement(By.xpath("((//tr[contains(@class,'dataRow')]//th/following-sibling::td[text()='Organization'])/preceding-sibling::th/a)[1]"), "Account Type - Organization");
							}

						}

					}
				}
			}
		}
		else
		{
			report.updateTestLog("Verify able to see search Box for entering the account", "Unable to see search Box for entering the account", Status.FAIL);
		}
	}

	public Boolean clickVeevaTab(String strTabName) throws Exception
	{
		strTabName = strTabName.trim();
		Boolean bTabFound = false;
		By byVeevaTabs = By.xpath("//div[@id='tabContainer']//nav/ul/li");
		if (cf.isElementVisible(byVeevaTabs, "Veeva Tabs"))
		{
			int iTabsCount = driver.findElements(byVeevaTabs).size();
			for (int iTab = 1; iTab <= iTabsCount; iTab++)
			{
				By byTabName = By.xpath("//div[@id='tabContainer']//nav/ul/li" + "[" + iTab + "]/a");
				String strTabNameTmp = cf.getData(byTabName, "Table Name" + iTab, "Text");
				if (strTabNameTmp.equalsIgnoreCase(strTabName))
				{
					cf.clickLink(byTabName, "Tab Name: " + strTabNameTmp);
					cf.waitForSeconds(15);
					bTabFound = true;
					break;
				}
			} // for loop
		} // Veeva Tabs visible
		if (!bTabFound)
		{
			By byMoreTabs = By.xpath("//div[@id='tabContainer']//nav/ul/li[@id='MoreTabs_Tab']");
			cf.clickByJSE(byMoreTabs, "More Tabs");
			By byMoreTabsList = By.xpath("//ul[@id='MoreTabs_List']/li");
			int iMoreTabsCount = driver.findElements(byMoreTabsList).size();
			for (int iTab = 1; iTab <= iMoreTabsCount; iTab++)
			{
				By byTabName = By.xpath("//ul[@id='MoreTabs_List']/li" + "[" + iTab + "]/a");
				String strTabNameTmp = cf.getData(byTabName, "Table Name" + iTab, "Text");
				if (strTabNameTmp.equalsIgnoreCase(strTabName))
				{
					cf.clickLink(byTabName, "Tab Name: " + strTabNameTmp);
					cf.waitForSeconds(15);
					bTabFound = true;
					break;
				}
			} // for loop
		} // bTabFound
		if (!bTabFound)
		{
			By byAllTabs = By.xpath("//div[@id='tabContainer']/nav/ul/li[@id='AllTab_Tab']/a");
			cf.clickLink(byAllTabs, "AllTabs");
			By byAllTabsList = By.xpath("//table[@class='detailList tabs']/tbody//a[contains(@class,'listRelatedObject')]");
			if (cf.verifyElementVisible(byAllTabsList, "All Tabs List"))
			{
				List<WebElement> eAllTabsList = driver.findElements(byAllTabsList);
				int iAllTabsListCount = eAllTabsList.size();
				for (int i = 0; i < iAllTabsListCount; i++)
				{
					String strTabNameTmp = cf.getData(eAllTabsList.get(i), i + "AllTabsList", "text");
					if (strTabNameTmp.equalsIgnoreCase(strTabName))
					{
						cf.clickElement(eAllTabsList.get(i), "Tab Name: " + strTabNameTmp);
						cf.waitForSeconds(15);
						bTabFound = true;
						break;
					}
				} // for loop
			}
			if (!bTabFound)
			{
				// report.updateTestLog("clickVeevaTab", "Veeva Tab: " +
				// strTabName + " not found.", Status.FAIL);
				throw new FrameworkException("Veeva Tab: " + strTabName + " not found.");
			}
		} // bTabFound
		return bTabFound;
	}// clickVeevaTab()

	public void selectterittory(String strPath) throws Exception
	{
		cf.clickLink(objVeevaAdvanceCoachingReport.selectterritories, "Select territorries");
		String[] aryPath = strPath.split("/");
		int iPath_Count = aryPath.length;
		String strXPath = "//div[@class='popupContainer']/div/ul/li";
		for (int i = 0; i < iPath_Count; i++)
		{
			String strTmp = aryPath[i];
			if (i > 0)
			{
				strXPath = strXPath + "/ul/li";
			}
			By byTeritory = By.xpath(strXPath);
			if (cf.isElementVisible(byTeritory, "byTeritory"))
			{
				int iChild_Count = driver.findElements(byTeritory).size(); // Child
																			// Elements
																			// count
				for (int j = 1; j <= iChild_Count; j++)
				{
					By byTeritory_Name = By.xpath(strXPath + "[" + j + "]/label");
					String strTeritory_Name = cf.getData(byTeritory_Name, j + "byTeritory_Name", "text");
					if ((strTmp.trim()).equals(strTeritory_Name.trim()))
					{
						if (i == iPath_Count - 1)
						{
							By byTeritoryClick = By.xpath(strXPath + "[" + j + "]//input[@type='checkbox']");
							cf.clickLink(byTeritoryClick, "byTeritoryClick: " + strTeritory_Name);// click
																									// the
																									// check
																									// box
							report.updateTestLog("Verify territory is checked", "Terittory is selected", Status.PASS);
							break;
						}
						else
						{// expand it
							By byTeritory1 = By.xpath(strXPath + "[" + j + "]");
							String strClass = cf.getData(byTeritory1, "Root", "class");
							By byTeritoryExpand = By.xpath(strXPath + "[" + j + "]/div");
							if (strClass.equals("closed expandable lastExpandable") || strClass.equals("closed expandable"))
							{
								cf.clickLink(byTeritoryExpand, "Teritory Expand:" + strTeritory_Name);
								strXPath = strXPath + "[" + j + "]";
								break;
							}
							else
							{
								// Already expanded
							}
						}
					}
				} // for j loop
			}
			else
			{// Teritory Visible
				report.updateTestLog("Verify able to see the territory", "No child elements inisde Teritory:" + strTmp, Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
	}

	public String dateformat()
	{
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");
		if (profile.equals("Commercial"))
		{
			if (locale.equals("RU"))
			{
				date = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
			}
			else if (locale.equals("EU1") || locale.equals("BR") || locale.equals("CA"))
			{
				date = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
			}
			else if (locale.equals("IC"))
			{
				date = new SimpleDateFormat("MM/dd/YYYY").format(dNow).toString();
			}
			else if (locale.equals("IE") || locale.equals("EU2"))
			{
				date = new SimpleDateFormat("d/M/YYYY").format(dNow).toString();
			}
			else if (locale.equals("ANZ"))
			{
				date = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
			}
			else if (locale.equals("CN"))
			{
				date = new SimpleDateFormat("YYYY/M/d").format(dNow).toString();
			}
			else if (locale.equals("JP"))
			{
				date = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
			}
			else if (locale.equals("US"))
			{
				date = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
			}
		}
		else if (profile.equals("Medical"))
		{
			if (locale.equals("IE") || (locale.equals("JP")))
			{
				date = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
			}
			if (locale.equals("EU2") || locale.equals("RU"))
			{
				date = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
			}
			else if (locale.equals("EU1") || locale.equals("BR") || locale.equals("CA"))
			{
				date = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
			}
			else if (locale.equals("IC"))
			{
				date = new SimpleDateFormat("MM/dd/YYYY").format(dNow).toString();
			}
			else if (locale.equals("ANZ"))
			{
				date = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
			}
			else if (locale.equals("CN"))
			{
				date = new SimpleDateFormat("YYYY/M/d").format(dNow).toString();
			}
		}
		else if (profile.equals("DXL"))
		{
			if (locale.equals("EU1"))
			{
				date = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
			}
		}
		return date;
	}

	public Date formatAndGetDate(String strDate, String strLocale, String strProfile) throws ParseException
	{
		Date parseDate;
		switch (strLocale.trim().toUpperCase())
		{
			case "IE" :
				if (strProfile.trim().equalsIgnoreCase("Commercial"))
					parseDate = new SimpleDateFormat("d/M/yyyy").parse(strDate);// new
				// SimpleDateFormat("d/M/YYYY").format(dNow).toString();
				else
					parseDate = new SimpleDateFormat("yyyy/MM/dd").parse(strDate);
				break;
			case "JP" :
				if (strProfile.trim().equalsIgnoreCase("Commercial"))
					parseDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
				else
					parseDate = new SimpleDateFormat("yyyy/MM/dd").parse(strDate);
				break;
			case "EU2" :
			case "RU" :
				parseDate = new SimpleDateFormat("dd.MM.yyyy").parse(strDate);
				break;
			case "EU1" :
				if (strProfile.equals("DxL"))
					parseDate = new SimpleDateFormat("dd.MM.yyyy").parse(strDate);
				else
					parseDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
				break;
			case "BR" :
			case "CA" :
				parseDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
				break;
			case "ANZ" :
				parseDate = new SimpleDateFormat("d/MM/yyyy").parse(strDate);
				break;
			case "CN" :
				parseDate = new SimpleDateFormat("yyyy/M/d").parse(strDate);
				break;
			default :
				parseDate = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
		}
		return parseDate;
		/*
		 * / Date fgt=parseDate.parse(strDate); String
		 * strFormat=parseDate.format(fgt); return parseDate.parse(strFormat);
		 * //
		 */
	}

	public String addMinsToDateru(String startTime, int mins) throws ParseException
	{
		SimpleDateFormat sdf2 = new SimpleDateFormat("H:mm");
		Date date1 = new SimpleDateFormat("H:mm").parse(startTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.MINUTE, mins); // Number of Days to add
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public String timeconvertToRailwayTimeru(String strTime) throws Exception
	{
		//SimpleDateFormat displayFormat = new SimpleDateFormat("H:mm");
		//SimpleDateFormat parseFormat = new SimpleDateFormat("h:mm a");
		
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
		Date date = parseFormat.parse(strTime);
		// System.out.println(parseFormat.format(date) + " = " +
		// displayFormat.format(date));
		return displayFormat.format(date);
	}

	/**
	 * Validate date and time format and date is required block or not.
	 * 
	 * @throws Exception
	 */
	/**
	 * Validate date and time format and date is required block or not.
	 * 
	 * @throws Exception
	 */
	public String dateValidationCalls() throws Exception
	{
		Date dNow = new Date();
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.daterequired, "Date and Time Required"))
			report.updateTestLog("Verify Datetime is required block or not", "Able to see Datetime as required block", Status.PASS);
		else
			report.updateTestLog("Verify Datetime is required block or not", "Unable to see Datetime as required block", Status.FAIL);
		String datetimecalls = formatDateTimeMarketWiseCalls(dNow, locale, profile, "date");
		System.out.println("Current date: " + datetimecalls);
		String appdateandtime = driver.findElement(objCallsForMedical.labelDateTimeFormat).getText().trim().split(" ")[0];
		System.out.println("Time displayed in rec call: " + appdateandtime);
		if (appdateandtime.equals(datetimecalls))
		{
			report.updateTestLog("Verify able to see proper date and time", "Able to see proper date and time", Status.PASS);
		}
		else
		{
			report.updateTestLog("Verify able to see proper date and time", "Unable to see proper date and time", Status.FAIL);
		}
		return datetimecalls;
	}

	/**
	 * This method will return the date string based on market and profile based
	 * 
	 * @param dNow
	 * @param locale
	 * @param profile
	 * @param strDateOrTime
	 * @return
	 */
	public String formatDateTimeMarketWise(Date dNow, String locale, String profile, String strDateOrTime)
	{
		String datecalls = "", timecalls = "";
		SimpleDateFormat time;
		switch (locale.trim().toUpperCase())
		{
			case "IC" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "IW" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;

			case "IE" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("d/M/YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "JP" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();// 2018/12/28
				else// medical
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "EU2" :
				// case "RU":
				if (profile.trim().equalsIgnoreCase("DxL"))
					datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				else if (profile.trim().equalsIgnoreCase("Commercial"))
				{
					datecalls = new SimpleDateFormat("dd/M/YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				else if (profile.trim().equalsIgnoreCase("Medical"))
				{
					// Changed by Jk on Jul 24, 2019
					datecalls = new SimpleDateFormat("dd/M/YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "RU" :
				if (profile.trim().equalsIgnoreCase("Medical"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				else
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "EU1" :
				if (profile.trim().equals("DxL"))
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "BR" :
			case "CA" :
				datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "ANZ" :
				datecalls = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "CN" :
				datecalls = new SimpleDateFormat("YYYY/M/dd").format(dNow).toString();
				time = new SimpleDateFormat("ah:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "US" :
				datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			default :
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
		}
		if (strDateOrTime.trim().equalsIgnoreCase("date"))
			return datecalls;
		else if (strDateOrTime.trim().equalsIgnoreCase("time"))
			return timecalls;
		else
			return datecalls + " " + timecalls;
	}

	public String formatDateTimeMarketWiseCalls(Date dNow, String locale, String profile, String strDateOrTime)
	{
		String datecalls = "", timecalls = "";
		SimpleDateFormat time;
		switch (locale.trim().toUpperCase())
		{
			case "IC" :
				if (profile.trim().equalsIgnoreCase("Commercial") || profile.trim().equalsIgnoreCase("Medical") )
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "IW" :
				if (profile.trim().equalsIgnoreCase("Commercial") || profile.trim().equalsIgnoreCase("Medical"))
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;

			case "IE" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("d/M/YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "JP" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();// 2018/12/28
				else// medical
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "EU2" :
				// case "RU":
				if (profile.trim().equalsIgnoreCase("DxL"))
					datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				else if (profile.trim().equalsIgnoreCase("Commercial"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				else if (profile.trim().equalsIgnoreCase("Medical"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "RU" :
				if (profile.trim().equalsIgnoreCase("Medical"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				else
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "EU1" :
				if (profile.trim().equals("DxL"))
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				if (profile.trim().equals("Commercial"))
					datecalls = new SimpleDateFormat("dd/MM/YYYY HH:mm").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				System.out.println(datecalls);
				break;
			case "BR" :
			case "CA" :
				datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "ANZ" :
				datecalls = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "CN" :
				datecalls = new SimpleDateFormat("YYYY/M/dd").format(dNow).toString();
				time = new SimpleDateFormat("ah:mm");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			case "US" :
				datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				System.out.println(datecalls + "  " + timecalls);
				break;
			default :
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
		}
		if (strDateOrTime.trim().equalsIgnoreCase("date"))
			return datecalls;
		else if (strDateOrTime.trim().equalsIgnoreCase("time"))
			return timecalls;
		else
			return datecalls + " " + timecalls;
	}

	public String gmtconversions(Date date, String timeZone)
	{
		SimpleDateFormat dateformatvariable = null;
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");
		// null check
		if (date == null)
			return null;
		// create SimpleDateFormat object with input format
		dateformatvariable = dateformatvariableCallStd(locale, profile);
		// default system timezone if passed null or empty
		if (timeZone == null || "".equalsIgnoreCase(timeZone.trim()))
		{
			timeZone = Calendar.getInstance().getTimeZone().getID();
		}
		// set timezone to SimpleDateFormat
		dateformatvariable.setTimeZone(TimeZone.getTimeZone(timeZone));
		return dateformatvariable.format(date);
	}

	public SimpleDateFormat dateformatvariableCallStd(String locale, String profile)
	{
		SimpleDateFormat dateformatvariable = null;
		switch (locale.trim().toUpperCase())
		{
			case "IE" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					dateformatvariable = new SimpleDateFormat("d/M/yyyy h:mm a");
				else
					dateformatvariable = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				break;
			case "JP" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					dateformatvariable = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				else
					dateformatvariable = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				break;
			case "EU2" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					dateformatvariable = new SimpleDateFormat("dd.MM.yyyy HH:mm");
				else
					dateformatvariable = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
				break;
			case "RU" :
				dateformatvariable = new SimpleDateFormat("dd.MM.yyyy H:mm");
				break;
			case "EU1" :
			case "BR" :
				dateformatvariable = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				break;
			case "CA" :
				dateformatvariable = new SimpleDateFormat("dd/MM/yyyy h:mm a");
				break;
			case "ANZ" :
				dateformatvariable = new SimpleDateFormat("d/MM/yyyy HH:mm");
				break;
			case "CN" :
				dateformatvariable = new SimpleDateFormat("yyyy/M/d ah:mm");
				break;
			case "IC" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					dateformatvariable = new SimpleDateFormat("dd.MM.yyyy HH:mm");
				else
					dateformatvariable = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				break;
			case "IW" :
				dateformatvariable = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				break;
			default :
				break;
		}
		return dateformatvariable;
	}

	public String formatDateTimeMarketWiseACR(Date dNow, String locale, String profile, String strDateOrTime)
	{
		String datecalls = "", timecalls = "";
		SimpleDateFormat time;
		switch (locale.trim().toUpperCase())
		{
			case "IC" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				break;
			case "IW" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd-MM-YYYY").format(dNow).toString();
				break;

			case "IE" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("dd/M/YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				break;
			case "JP" :
				if (profile.trim().equalsIgnoreCase("Commercial"))
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();// 2018/12/28
				else// medical
					datecalls = new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "EU2" :
				// case "RU":
				if (profile.trim().equalsIgnoreCase("DxL"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}		
				else if (profile.trim().equalsIgnoreCase("Commercial"))
				{
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				else if (profile.trim().equalsIgnoreCase("Medical"))
				{
					// Changed by Jk on Jul 24, 2019
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					time = new SimpleDateFormat("HH:mm");
					timecalls = time.format(dNow).toString();
				}
				break;
			case "RU" :
				// Changed by JK on Jun 24, 2019
				datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "EU1" :
				if (profile.trim().equals("DxL"))
					datecalls = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
				else
					datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				break;
			case "BR" :
			case "CA" :
				datecalls = new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				break;
			case "ANZ" :
				datecalls = new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "CN" :
				datecalls = new SimpleDateFormat("YYYY/M/d").format(dNow).toString();
				time = new SimpleDateFormat("ah:mm");
				timecalls = time.format(dNow).toString();
				break;
			case "US" :
				datecalls = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
				time = new SimpleDateFormat("h:mm a");
				timecalls = time.format(dNow).toString();
				break;
			default :
				time = new SimpleDateFormat("HH:mm");
				timecalls = time.format(dNow).toString();
		}
		if (strDateOrTime.trim().equalsIgnoreCase("date"))
			return datecalls;
		else if (strDateOrTime.trim().equalsIgnoreCase("time"))
			return timecalls;
		else
			return datecalls + " " + timecalls;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public Boolean verifyVeevaPageOpened(String strpageDescription, String strPageType) throws Exception
	{
		strPageType = strPageType.trim();
		strpageDescription = strpageDescription.trim();
		String strAppPageDescription = "", strAppPageType = "";
		Boolean bExpectedVeevaPageOpened = false;
		if (strpageDescription == null || strpageDescription.equals(""))
		{
			strpageDescription = "";
		}
		else
		{
			cf.isElementVisible(cf.getSFElementXPath("pageDescription", "pageDescription"), "page Description");
			strAppPageDescription = cf.getSFData("pageDescription", "pageDescription", "text");
		}
		if (strPageType == null || strPageType.equals(""))
		{
			strPageType = "";
		}
		else
		{
			cf.isElementVisible(cf.getSFElementXPath("PageType", "PageType"), "Page Type");
			strAppPageType = cf.getSFData("PageType", "PageType", "text");
		}
		if (strAppPageDescription.equalsIgnoreCase(strpageDescription) && strAppPageType.equalsIgnoreCase(strPageType))
		{
			report.updateTestLog("Verify " + strpageDescription + " Page Opened", strpageDescription + " Page is opened", Status.PASS);
			bExpectedVeevaPageOpened = true;
		}
		else
		{
			report.updateTestLog("Verify " + strpageDescription + " Page Opened", strpageDescription + " Page is not opened", Status.FAIL);
			// frameworkParameters.setStopExecution(true);
		}
		return bExpectedVeevaPageOpened;
	}// verifyVeevaPageOpened()

	public Boolean verifyVeevaPageOpened(String strpageDescription, String strPageType, String strPageTitle) throws Exception
	{
		strPageType = strPageType.trim();
		strpageDescription = strpageDescription.trim();
		String strAppPageDescription = "", strAppPageType = "", strAppPageTitle = "Empty";
		Boolean bExpectedVeevaPageOpened = false;
		if (strPageTitle == null || strPageTitle.equals(""))
		{
			strPageTitle = "";
		}
		else
		{
			By byPageTitle = cf.getSFElementXPath("pagetitle", "pagetitle");
			cf.isElementVisible(byPageTitle, "Pag Title");
			strAppPageTitle = cf.getData(byPageTitle, "Pag Title", "title");
		}
		if (strpageDescription == null || strpageDescription.equals(""))
		{
			strpageDescription = "";
		}
		else
		{
			cf.isElementVisible(cf.getSFElementXPath("pageDescription", "pageDescription"), "page Description");
			strAppPageDescription = cf.getSFData("pageDescription", "pageDescription", "text");
		}
		if (strPageType == null || strPageType.equals(""))
		{
			strPageType = "";
		}
		else
		{
			cf.isElementVisible(cf.getSFElementXPath("PageType", "PageType"), "Page Type");
			strAppPageType = cf.getSFData("PageType", "PageType", "text");
		}
		if (strAppPageDescription.equalsIgnoreCase(strpageDescription) && strAppPageType.equalsIgnoreCase(strPageType))
		{
			report.updateTestLog("Verify " + strpageDescription + " Page Opened", strpageDescription + " Page is opened", Status.PASS);
			bExpectedVeevaPageOpened = true;
		}
		else if (strAppPageTitle.equalsIgnoreCase(strPageTitle))
		{
			report.updateTestLog("Verify " + strpageDescription + " Page Opened", "Page Title: " + strPageTitle + ", " + strpageDescription + " Page is opened", Status.PASS);
			bExpectedVeevaPageOpened = true;
		}
		else
		{
			report.updateTestLog("Verify " + strpageDescription + " Page Opened", strpageDescription + " Page is not opened", Status.FAIL);
			// frameworkParameters.setStopExecution(true);
		}
		return bExpectedVeevaPageOpened;
	}// verifyVeevaPageOpened()

	/**
	 * Method to click lookup icon nearby text box and select the first or
	 * random search result inside lookup popup
	 * 
	 * @param byLookUpIcon
	 * @param strElementDesc
	 * @return
	 * @throws Exception
	 */

	public String selectRandomLookupData(By byLookUpIcon, String strElementDesc) throws Exception
	{
		// Assumption is data present in Col 1 in search results
		int iRowCount = 0;
		Boolean bResultsFound = false;
		String strRowColData = null;
		String strResultTable_Rows = "//div[@class='pbBody lookup-results']//table/tbody/tr";
		By byLookUp_Input = By.xpath("//div[@class='modal-content']//div[@class='lookup']//input[@name='query']");
		By byLookUp_Close = By.xpath("//div[@class='modal-content']//a[@class='dialogClose']");
		if (cf.isElementVisible(byLookUpIcon, strElementDesc))
		{
			cf.clickButton(byLookUpIcon, strElementDesc);
			By byResultTable_Rows = By.xpath(strResultTable_Rows);
			if (cf.isElementVisible(byLookUp_Input, "LookUp dialog Window"))
			{
				cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows");
				if (cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows"))
				{
					iRowCount = driver.findElements(byResultTable_Rows).size();
					if (iRowCount > 1)
					{
						bResultsFound = true;
					}
					else
					{
						report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
						cf.clickButton(byLookUp_Close, "Dialog - Close");
					}
				}
				else
				{
					report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
					cf.clickButton(byLookUp_Close, "Dialog - Close");
				} // byResultTable_Rows
			} // LookUp dialog Window present
		} // byLookUpIcon
		if (bResultsFound)
		{
			int iRow = cf.getRandomNumber(2, iRowCount);
			By byRowColData = By.xpath(strResultTable_Rows + "[" + iRow + "]/*[1]/a");
			strRowColData = cf.getData(byRowColData, strElementDesc, "Text");
			cf.highLightElement(byRowColData, strElementDesc + ": " + strRowColData);
			cf.actionClick(byRowColData, strElementDesc + ": " + strRowColData);
			System.out.println(strElementDesc + ": " + strRowColData + " found ");
			return strRowColData;
		} // bResultsFound
		cf.clickButton(byLookUp_Close, "Dialog - Close");
		return null;
	}// End of Method

	public String getLookupData(By byLookUpIcon, String strElementDesc, String strSearchData) throws Exception
	{
		// Assumption is data present in Col 1 in search results
		int iRowCount = 0;
		Boolean bResultsFound = false;
		String strRowColData = null;
		String strResultTable_Rows = "//div[@class='pbBody lookup-results']//table/tbody/tr";
		By byLookUp_Input = By.xpath("//div[@class='modal-content']//div[@class='lookup']//input[@name='query']");
		By byLookUp_Close = By.xpath("//div[@class='modal-content']//a[@class='dialogClose']");
		if (cf.isElementVisible(byLookUpIcon, strElementDesc))
		{
			cf.clickButton(byLookUpIcon, strElementDesc);
			// waitForSeconds(3);
			By byResultTable_Rows = By.xpath(strResultTable_Rows);
			if (cf.isElementVisible(byLookUp_Input, "LookUp dialog Window"))
			{
				// setData(byLookUp_Input, "LookUp Input",strSearchData);
				// clickButton(byLookUp_Go,"LookUp_Go");
				// (3);
				cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows");
				if (cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows"))
				{
					iRowCount = driver.findElements(byResultTable_Rows).size();
					if (iRowCount > 1)
					{
						bResultsFound = true;
					}
					else
					{
						report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
						cf.clickButton(byLookUp_Close, "Dialog - Close");
					}
				}
				else
				{
					report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
					cf.clickButton(byLookUp_Close, "Dialog - Close");
				} // byResultTable_Rows
			} // LookUp dialog Window present
		} // byLookUpIcon
		if (bResultsFound)
		{
			Boolean bSearchDataFound = false;
			for (int i = 2; i <= iRowCount; i++)
			{
				By byRowColData = By.xpath(strResultTable_Rows + "[" + i + "]/*[1]/a");
				strRowColData = cf.getData(byRowColData, strElementDesc, "Text");
				if (strRowColData.equalsIgnoreCase(strSearchData))
				{
					bSearchDataFound = true;
					// highLightElement(byRowColData,strElementDesc + ": " +
					// strRowColData);
					cf.actionClick(byRowColData, strElementDesc + ": " + strRowColData);
					System.out.println(strElementDesc + ": " + strRowColData + " found ");
				}
			} // for loop
			if (!bSearchDataFound)
			{
				report.updateTestLog("Lookup Search", strSearchData + " not found in Lookup Search Table", Status.FAIL);
				cf.clickButton(byLookUp_Close, "Dialog - Close");
			}
		} // bResultsFound
		return strRowColData;
	}// End of Method

	public String getLookupDataWithSearch(By byLookUpIcon, String strElementDesc, String strSearchData) throws Exception
	{
		// Assumption is data present in Col 1 in search results
		int iRowCount = 0;
		Boolean bResultsFound = false;
		String strRowColData = null;
		String strResultTable_Rows = "//div[@class='pbBody lookup-results']//table/tbody/tr";
		By byLookUp_Close = By.xpath("//div[@class='modal-content']//a[@class='dialogClose']");
		if (cf.isElementVisible(byLookUpIcon, strElementDesc))
		{
			cf.clickButton(byLookUpIcon, strElementDesc);
			// waitForSeconds(3);
			By byLookup_Spinner = By.xpath("//div[@class='modal-content']//div[@class='spinner']/parent::span[@class='ng-scope ng-hide']");
			cf.isElementPresent(byLookup_Spinner, "byLookup_Spinner");
			By byLookUp_Input = By.xpath("//div[@class='modal-content']//div[@class='lookup']//input[@name='query']");
			By byLookUp_Go = By.xpath("//div[@class='modal-content']//div[@class='lookup']//input[@name='go'][@type='submit']");
			By byResultTable_Rows = By.xpath(strResultTable_Rows);
			if (cf.isElementVisible(byLookUp_Input, "LookUp dialog Window"))
			{
				cf.setData(byLookUp_Input, "LookUp Input", strSearchData);
				cf.clickButton(byLookUp_Go, "LookUp_Go");
				cf.isElementPresent(byLookup_Spinner, "byLookup_Spinner");
				// (3);
				cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows");
				if (cf.isElementVisible(byResultTable_Rows, "byResultTable_Rows"))
				{
					iRowCount = driver.findElements(byResultTable_Rows).size();
					if (iRowCount > 1)
					{
						bResultsFound = true;
					}
					else
					{
						report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
						cf.clickButton(byLookUp_Close, "Dialog - Close");
					}
				}
				else
				{
					report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.FAIL);
					cf.clickButton(byLookUp_Close, "Dialog - Close");
				} // byResultTable_Rows
			} // LookUp dialog Window present
		} // byLookUpIcon
		if (bResultsFound)
		{
			Boolean bSearchDataFound = false;
			for (int i = 2; i <= iRowCount; i++)
			{
				By byRowColData = By.xpath(strResultTable_Rows + "[" + i + "]/*[1]/a");
				strRowColData = cf.getData(byRowColData, strElementDesc, "Text");
				if (strRowColData.equalsIgnoreCase(strSearchData))
				{
					bSearchDataFound = true;
					// highLightElement(byRowColData,strElementDesc + ": " +
					// strRowColData);
					cf.actionClick(byRowColData, strElementDesc + ": " + strRowColData);
					System.out.println(strElementDesc + ": " + strRowColData + " found ");
				}
			} // for loop
			if (!bSearchDataFound)
			{
				report.updateTestLog("Lookup Search", strSearchData + " not found in Lookup Search Table", Status.FAIL);
				cf.clickButton(byLookUp_Close, "Dialog - Close");
			}
		} // bResultsFound
		return strRowColData;
	}// End of Method

	public String searchAndGo(String strSearchScope, String strSearchText) throws Exception
	{
		/**
		 * String strSearchType = dataTable.getData("Search", "Search Type");
		 * String strSearchText = dataTable.getData("Search", "Search Text");
		 */
		String strSearchData = null;
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 20);
		By bySideBarLink = By.xpath("//a[@id='handlebarContainer']");
		if (!cf.isElementVisible(objHeaderPage.textSearch, "Search Text", 10))
		{
			cf.clickLink(bySideBarLink, "Open Side Bar");
		}
		if (cf.isElementVisible(objHeaderPage.textSearch, "Search Text"))
		{
			if (cf.isElementVisible(objHeaderPage.selectSearch, "Search Scope", 2))
			{
				cf.selectData(objHeaderPage.selectSearch, "Search Scope", strSearchScope);
			}
			cf.setData(objHeaderPage.textSearch, "Search Text", strSearchText);
			cf.clickButton(objHeaderPage.btnGo, "Go button in Search");
			strSearchData = strSearchText;
		}
		else if (cf.isElementVisible(By.xpath("//a[.='Advanced Search...']"), "Advanced Search"))
		{// Advanced Search
			cf.clickButton(By.xpath("//a[.='Advanced Search...']"), "Advanced Search");
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".searchBoxInput>input[value='Search']")));
			if (cf.isElementVisible(By.cssSelector(".searchBoxInput>input"), "Search in Advanced Search"))
			{
				cf.setData(By.cssSelector(".searchBoxInput>input"), "Search in Advanced Search", strSearchText);
				// Clear all Filters
				By byDeSelectAll = By.xpath("//table[@class='entitiesSelect']//a[text()='Deselect All']");
				cf.clickLink(byDeSelectAll, "Deselect All");
				By bySearchScopeCheckBox = By.xpath("//table[@class='entitiesSelect']//label[text()='" + strSearchScope + "']/preceding-sibling::input[@type='checkbox']");
				cf.clickByJSE(bySearchScopeCheckBox, strSearchScope);
				cf.clickButton(By.cssSelector(".searchBoxInput>input[value='Search']"), "Search Button");
				strSearchData = cf.verifyGetClickSearchDataFromTable("Accounts", "Name", strSearchText, "", "", "Click", "Search");
			}
			else
			{
				report.updateTestLog("Search and Go", "Search in Advanced Search Page not Found", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Search and Go", "Search Option not Found", Status.FAIL);
		}
		return strSearchData;
	}

	public void verifySFPageElement(String strElementDesc, String strElementType, String strAttribure, String strExpectedValue, String strVerifyType, Boolean bScreenShotReq) throws Exception
	{
		System.out.println("verifyElementVisible: " + strElementDesc);
		By byLocator = cf.getSFElementXPath(strElementDesc, strElementType);
		cf.verifyPageElement(byLocator, strElementDesc, strAttribure, strExpectedValue, strVerifyType, bScreenShotReq);
	}

	public void verifySFPageElement(String strElementTable, String strElementDesc, String strElementType, String strAttribure, String strExpectedValue, String strVerifyType, Boolean bScreenShotReq) throws Exception
	{
		System.out.println("verifyElementVisible: " + strElementDesc);
		By byLocator = cf.getSFElementXPath(strElementTable, strElementDesc, strElementType);
		cf.verifyPageElement(byLocator, strElementDesc, strAttribure, strExpectedValue, strVerifyType, bScreenShotReq);
	}

	public String verifyCheckBoxDataFromTableOneColumn(String strTableName, String strColName, String strColData, String strClickGetSearch, String strTableType) throws Exception
	{
		String strRowColData = null, strReturnData = null;
		int iRowIndex = -1, iRowNoDataToRetrieve = 0, iRowNoDataToRetrieveIndex = 0;
		if (strColData.startsWith("##"))
		{
			strColData = strColData.replace("##", "");
			try
			{
				iRowNoDataToRetrieve = Integer.parseInt(strColData);
			}
			catch (Exception e)
			{
				// e.printStackTrace();
			}
		}
		String strTableXPath = null;
		switch (strTableType.toLowerCase())
		{
			case "linklet" :
				strTableXPath = "//td[@class='pbTitle']//*[text()='" + strTableName + "']//ancestor::table/parent::div/following-sibling::div/table";
				break;
			case "list" :
				strTableXPath = "//*[@class='bPageTitle']//*[text()='" + strTableName + "']/following::div[@class='pbBody']/table";
				break;
			case "search" :
				strTableXPath = "//td[@class='pbTitle']//*[contains(text(),'" + strTableName + "')]//ancestor::table/parent::div/following-sibling::div/table";
				break;
			case "calendar" :
				strTableXPath = "//div[@id='calendar']//table[@id='vodDayViewTbl']";
				break;
			default :
				report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " is not valid. Valid Table types are linkLet, list, search or calendar." + "</b>", Status.FAIL);
		}
		// if(strTableType.equalsIgnoreCase("LinkLet")){
		// strLinkLetTableXPath = "//td[@class='pbTitle']//*[text()='" +
		// strTableName +
		// "']//ancestor::table/parent::div/following-sibling::div/table";
		// }else if(strTableType.equalsIgnoreCase("List")){
		// strLinkLetTableXPath = "//*[@class='bPageTitle']//*[text()='" +
		// strTableName + "']/following::div[@class='pbBody']/table";
		// }
		// else if(strTableType.equalsIgnoreCase("Search")){
		// strLinkLetTableXPath = "//td[@class='pbTitle']//*[contains(text(),'"
		// + strTableName +
		// "')]//ancestor::table/parent::div/following-sibling::div/table";
		// }
		By byLinkLetTable = By.xpath(strTableXPath);
		Boolean bTableFound = false;
		if (cf.isElementVisible(byLinkLetTable, "LinkLetTable - " + strTableName))
		{
			bTableFound = true;
		}
		else
		{// bTableFound
			report.updateTestLog(strClickGetSearch + " data in " + strTableName, "<b>" + strTableType + " Table: " + strTableName + " not found in the page." + "</b>", Status.FAIL);
		}
		int iColNo = -1;
		By byHeaderTableCols = By.xpath(strTableXPath + "/tbody/tr[@class='headerRow']/th");// get
																							// column
																							// names
																							// from
																							// Header
																							// Table
		if (bTableFound)
		{
			//////////// Show more ///////////////////////////////////
			if (strTableType.equalsIgnoreCase("LinkLet"))
			{
				cf.expandLinkLetTable(strTableName);
			}
			iColNo = cf.getColNumFromTable(byHeaderTableCols, strTableName, strColName);
			if (iColNo != -1)
			{
				By byLinkLetTableRows = By.xpath(strTableXPath + "/tbody/tr");
				Boolean bRowCol_Found = false;
				int iLinkLetTableRowsCount = cf.getRowcountFromTable(byLinkLetTableRows, "LinkLetTableRows") + 1; /// since
																													/// header
																													/// row
																													/// also
																													/// included
																													/// in
																													/// the
																													/// table
				for (int iRow = 2; iRow <= iLinkLetTableRowsCount; iRow++)
				{ // data starts from 2nd row
					By byRowColData = By.xpath(strTableXPath + "/tbody/tr[" + iRow + "]/*[" + iColNo + "]");
					strRowColData = cf.getData(byRowColData, iRow + strColName, "Text");
					// highLightElement(byRowColData, iRow + strColName);
					if (strColData == null || strColData.isEmpty() || strColData.equalsIgnoreCase(""))
					{
						// return the first matching data
						report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + "<b> is found in the Table: " + strTableName + "</b>", Status.PASS);
						strReturnData = strRowColData;
						bRowCol_Found = true;
						iRowIndex = iRow;
						/// break;
					}
					else
					{// strColName is not empty
						++iRowNoDataToRetrieveIndex;
						if (iRowNoDataToRetrieve >= 1 && iRowNoDataToRetrieve == iRowNoDataToRetrieveIndex)
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " in the Table: " + strTableName + "</b>", Status.PASS);
							strReturnData = strRowColData;
							bRowCol_Found = true;
							iRowIndex = iRowNoDataToRetrieveIndex + 1; // Since
																		// 1st
																		// row
																		// is
																		// Header
																		// row
																		// break;
						}
						else if (strRowColData.equalsIgnoreCase(strColData))
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] found in the Row#: " + iRowIndex + " <b> is found in the Table: " + strTableName + "</b>", Status.PASS);
							bRowCol_Found = true;
						}
					} //// strColName is not empty
					if (bRowCol_Found)
					{
						if (strClickGetSearch.equalsIgnoreCase("Click"))
						{
							By byRowColDataLink = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[" + iColNo + "]//a");
							if (cf.isElementPresent(byRowColDataLink))
							{
								cf.clickByJSE(byRowColDataLink, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strRowColData);
							}
							else
							{
								cf.clickByJSE(byRowColData, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]: " + strRowColData);
							}
						}
						else if (strClickGetSearch.equalsIgnoreCase("Checkbox") || strClickGetSearch.equalsIgnoreCase("Select"))
						{
							By byRowColDataCheckbox = By.xpath(strTableXPath + "/tbody/tr[" + iRowIndex + "]/*[1]//input[@type='checkbox']");
							if (cf.isElementPresent(byRowColDataCheckbox))
							{
								cf.clickByJSE(byRowColDataCheckbox, strTableName + " - " + strColName + " - RowColData[" + iRowIndex + ":" + iColNo + "]:" + strRowColData);
							}
							else
							{
								report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] - checkbox not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
						break;
					} // bRowCol_Found
				} // for loop
				if (!strClickGetSearch.equalsIgnoreCase("Get"))
				{
					if (iRowIndex > 1)
					{// data starts from 2nd row
						if (strReturnData == null && !strClickGetSearch.equalsIgnoreCase("Search"))
						{// if search or Get just return null or data only
							if (!bRowCol_Found)
							{
								report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>[" + strColName + ": " + strColData + "] not found in the Table: " + strTableName + "</b>", Status.FAIL);
							}
						}
					}
					else
					{
						if (iLinkLetTableRowsCount == 0)
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b> No data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
						else
						{
							report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>" + strColName + ":" + strColData + " data not found in the Table: " + strTableName + "</b>", Status.FAIL);
						}
					} // (iRowIndex > 0)
				} // if(!strClickGetSearch.equalsIgnoreCase("Get")){
			}
			else
			{
				report.updateTestLog(strClickGetSearch + " data from " + strTableName, "<b>Column Name:" + strColName + " not found in the Table: " + strTableName + "</b>", Status.FAIL);
			} // (iColNo != -1)
		} // bTableFound
		return strReturnData;
	}//

	public String searchInLookupWindow(By by, String strElementDesc, String strSearchData) throws Exception
	{
		String strSearchInLookup = searchInLookupWindow(by, strElementDesc, strSearchData, "");
		return strSearchInLookup;
	}// searchInLookup() method

	public String searchInLookupWindow(By by, String strElementDesc, String strSearchData, String strRole) throws Exception
	{
		// Assumption is data present in Col 1 in search results
		Boolean bSearchFound = false;
		Boolean bLookUpSearchFound = false;
		String strCurrentPageTitle = cf.getSFPageTitle();
		Boolean bswitchToPage = false;
		String strRowCol1Data = null;
		if (cf.isElementPresent(by, strElementDesc))
		{
			cf.waitForPageToOpenAfterLinkClick(by, strElementDesc);
			cf.waitForSeconds(3);
			// Boolean bLookUpSearchFound = cf.switchToFrame("searchFrame");
			// //Switch to LookupSearch Frame
			driver.switchTo().defaultContent();
			// driver.findElement(by)
			By frame_LookupSearch2 = By.xpath("//frameset/frame[@id='searchFrame']");
			bLookUpSearchFound = cf.switchToFrame(frame_LookupSearch2, "frame_LookupSearch");
		}
		else
		{// isElementPresent
			report.updateTestLog("searchInLookup", strElementDesc + " not found ", Status.FAIL);
		}
		if (bLookUpSearchFound)
		{
			By lookUpInput = By.xpath("//form//div[@class='lookup']//input[@id='lksrch'][@type='text']");
			cf.isElementVisible(lookUpInput, "lookUpInput");
			cf.setData(lookUpInput, "lookUpInput", strSearchData);
			By lookUpSearchGo = By.xpath("//form//div[@class='lookup']//div/input[@name='go'][@type='submit']");
			cf.clickLink(lookUpSearchGo, "lookUpSearchGo");//
			cf.waitForSeconds(3);
			driver.switchTo().defaultContent();
			// Boolean bLookUpResultsFound = cf.switchToFrame("resultsFrame");
			By frame_LookupResults2 = By.xpath("//frameset/frame[@id='resultsFrame']");
			Boolean bLookUpResultsFound = cf.switchToFrame(frame_LookupResults2, "frame_LookupResults");
			if (bLookUpResultsFound)
			{
				report.updateTestLog("LookupResults", "frame_LookupResults", Status.SCREENSHOT);
				String byRowsXPath = "//div[@class='pbBody']//table//tr";
				By byRows = By.xpath(byRowsXPath);
				if (cf.isElementVisible(byRows, "Search Results"))
				{
					int iRowCount = driver.findElements(byRows).size();
					String strNoRowsMsg = cf.getData(By.xpath("//div[@class='pbBody']//table//tr[2]/*[1]"), "Lookup Results", "class");
					if (iRowCount > 1 && !strNoRowsMsg.equalsIgnoreCase("noRows"))
					{
						if (strSearchData.isEmpty())
						{
							int iRowIndex = cf.getRandomNumber(2, iRowCount);
							By byRowCol1Data = By.xpath(byRowsXPath + "[" + iRowIndex + "]/*[1]/a");
							strRowCol1Data = cf.getData(byRowCol1Data, strElementDesc, "Text");
							cf.highLightElement(byRowCol1Data, strElementDesc + ": " + strRowCol1Data);
							cf.actionClick(byRowCol1Data, strElementDesc + ": " + strRowCol1Data);
							bSearchFound = true;
						}
						else
						{
							for (int i = 2; i <= iRowCount; i++)
							{
								String strAppRole = "";
								if (strRole.equalsIgnoreCase("") || strRole.isEmpty() || strRole == null)
								{
									strRole = "";
								}
								else
								{
									// waitForSeconds(3);
									By byRowCol2Data = By.xpath(byRowsXPath + "[" + i + "]/*[2]");// get
																									// Role
									strAppRole = cf.getData(byRowCol2Data, strElementDesc + "- Role", "Text");
								}
								if (strAppRole.equalsIgnoreCase(strRole))
								{
									By byRowCol1Data = By.xpath(byRowsXPath + "[" + i + "]/*[1]/a");
									strRowCol1Data = cf.getData(byRowCol1Data, strElementDesc, "Text");
									if (strRowCol1Data.equalsIgnoreCase(strSearchData))
									{
										cf.highLightElement(byRowCol1Data, strElementDesc + ": " + strRowCol1Data);
										cf.actionClick(byRowCol1Data, strElementDesc + ": " + strRowCol1Data);
										System.out.println(strElementDesc + ": " + strRowCol1Data + " found ");
										bSearchFound = true;
										break;
									}
								}
							} // for loop
						} // strSearchData Empty
					}
					else
					{
						report.updateTestLog("searchInLookup", strElementDesc + ": No Records found ", Status.FAIL);
						driver.close();// Close Lookup window
					}
				} // isElementVisible
				if (!bSearchFound)
				{
					report.updateTestLog("searchInLookup", strElementDesc + ": " + strSearchData + " found ", Status.FAIL);
					driver.close();// Close Lookup window
				}
			} // bLookUpResultsFound
			bswitchToPage = cf.switchToPage(strCurrentPageTitle);
		} // bLookUpSearchFound
		if (bswitchToPage)
		{
			return strRowCol1Data;
		}
		else
		{
			return null;
		}
	}// searchInLookup() method

	public String getLookupDataInWindow(By byLookUpIcon, String strElementDesc, String strSearchData) throws Exception
	{
		String strRowColData;
		String parentWindow = driver.getWindowHandle();
		if (cf.isElementVisible(byLookUpIcon, strElementDesc))
		{
			// cf.clickButton(byLookUpIcon, strElementDesc);
			// cf.waitForSeconds(6);
			// if(driver.getWindowHandles().size()>1){
			try
			{
				if (cf.waitForPageToOpenAfterLinkClick(byLookUpIcon, strElementDesc))// switchToPage("Search
																							// ~
																						// Salesforce
																						// -
																						// Unlimited
																						// Edition")){
					if (!strSearchData.isEmpty())
					{
						driver.switchTo().frame("searchFrame");
						By byLookUp_Input = By.cssSelector(".lookup input[type='text']");
						By byLookUp_Go = By.cssSelector(".lookup input.btn");
						if (cf.isElementVisible(byLookUp_Input, "LookUp dialog Window"))
						{
							cf.setData(byLookUp_Input, "LookUp Input", strSearchData);
							cf.clickButton(byLookUp_Go, "LookUp_Go");
						} // LookUp dialog Window present
					}
				driver.switchTo().parentFrame();
				List<WebElement> listResultRows;
				driver.switchTo().frame("resultsFrame");
				listResultRows = driver.findElements(By.cssSelector("div.lookup tr.dataRow th a"));
				if (listResultRows.size() == 0)
					listResultRows = driver.findElements(By.cssSelector("div.lookupSearch tr.dataRow th a"));
				if (listResultRows.size() > 0)
				{
					if (!strSearchData.trim().equalsIgnoreCase("") || !strSearchData.isEmpty())
					{
						for (int i = 0; i < listResultRows.size(); i++)
						{
							strRowColData = listResultRows.get(i).getText();
							if (strRowColData.equalsIgnoreCase(strSearchData))
							{
								cf.clickElement(listResultRows.get(i), strSearchData);
								cf.waitForSeconds(2);
								driver.switchTo().window(parentWindow);
								return strRowColData;
							}
						}
					}
					else
					{
						int iRow = cf.getRandomNumber(0, listResultRows.size() - 1);
						strRowColData = listResultRows.get(iRow).getText();
						cf.clickElement(listResultRows.get(iRow), strSearchData);
						cf.waitForSeconds(2);
						driver.switchTo().window(parentWindow);
						return strRowColData;
					}
				}
				else
				{
					report.updateTestLog("Lookup Search", "No Results found in Lookup Search Table", Status.SCREENSHOT);
					// cf.switchToParentFrame();
					driver.close();
					driver.switchTo().window(parentWindow);
					return null;
				}
			}
			catch (Exception e)
			{
				report.updateTestLog("Lookup Search", "Switching to parent window. Error occurred : " + e.getMessage(), Status.FAIL);
				Set<String> newHandles = driver.getWindowHandles();
				for (String handle : newHandles)
					if (handle.trim().equalsIgnoreCase(parentWindow.trim()))
					{
						driver.switchTo().window(parentWindow);
						break;
					}
			}
		}
		return null;
	}

	public String addDaysToDate(Date dtDate, int iDays) throws ParseException
	{
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/YYYY");
		// Date date1=new SimpleDateFormat("MM/dd/yyyy").parse(strStartDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtDate);
		cal.add(Calendar.DAY_OF_MONTH, iDays); // Number of Days to add
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public String addDaysToDate(String strStartDate, int iDays) throws ParseException
	{
		SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/YYYY");
		Date date1 = new SimpleDateFormat("MM/dd/yyyy").parse(strStartDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.DAY_OF_MONTH, iDays); // Number of Days to add
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public String addMinsToDate12(String startTime, int mins) throws ParseException
	{
		//SimpleDateFormat sdf2 = new SimpleDateFormat("h:mm a");
		//Date date1 = new SimpleDateFormat("h:mm a").parse(startTime);
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm a");
		Date date1 = new SimpleDateFormat("hh:mm a").parse(startTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.MINUTE, mins); // Number of Days to add
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public String addMinsToDate(String startTime, int mins) throws ParseException
	{
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
		Date date1 = new SimpleDateFormat("HH:mm").parse(startTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.MINUTE, mins); // Number of Days to add
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public Date addMonthToDate(Date date1, int iMonths) throws ParseException
	{
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		// SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/YYYY");
		// Date date1=new SimpleDateFormat("MM/dd/yyyy").parse(strStartDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.MONTH, iMonths); // Number of Months to add
		return cal.getTime();
	}

	public String getDayValueFromDate(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// System.out.println("Current date : " + (now.get(Calendar.MONTH) + 1)
		// + "-"
		// + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
		String[] strDays = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"};
		// Day_OF_WEEK starts from 1 while array index starts from 0
		String strDay = strDays[cal.get(Calendar.DAY_OF_WEEK) - 1];
		System.out.println("Current day is : " + strDay);
		return strDay;
	}

	public String timeconvertToRailwayTime(String strTime) throws Exception
	{
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
		//SimpleDateFormat parseFormat = new SimpleDateFormat("h:mm a");
		Date date = parseFormat.parse(strTime);
		// System.out.println(parseFormat.format(date) + " = " +
		// displayFormat.format(date));
		return displayFormat.format(date);
	}

	public String getRandomNumber(int charLength)
	{
		String strNum = String.valueOf(charLength < 1 ? 0 : new Random().nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1) + (int) Math.pow(10, charLength - 1));
		return strNum;
	}

	public void swithLightingToClassic() throws Exception
	{
		String currentUrl = driver.getCurrentUrl();

		if (currentUrl.contains("lightning"))
		{
			cf.waitForSeconds(10);
			if (cf.isElementPresent(By.xpath("//div[@class='tooltipTrigger tooltip-trigger uiTooltip']/span[@class='photoContainer forceSocialPhoto']")))
			{
				cf.clickElement(By.xpath("//div[@class='tooltipTrigger tooltip-trigger uiTooltip']/span[@class='photoContainer forceSocialPhoto']"), "Profile");

				report.updateTestLog("Verify Profile is Clicked", "Profile is Clicked", Status.PASS);

				cf.waitForSeconds(10);
				if (cf.isElementPresent(By.xpath("//a[text()='Switch to Salesforce Classic']")))
				{
					cf.clickElement(By.xpath("//a[text()='Switch to Salesforce Classic']"), "Switch to Salesforce Classic");
					report.updateTestLog("Verify Switch to Salesforce Classic is Switched", "Switch to Salesforce Classic is Switched", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify Switch to Salesforce Classic is Switched", "Switch to Salesforce Classic is NOT Switched", Status.FAIL);
				}
			}

			else
			{
				report.updateTestLog("Verify Profile is Clicked", "Profile is NOT Clicked", Status.FAIL);
			}

		}

	}

	public int getRandomNumber(int min, int max)
	{
		return (int) Math.floor(Math.random() * (max - min + 1)) + min;
	}
}