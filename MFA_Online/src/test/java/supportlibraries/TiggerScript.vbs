
'On Error Resume Next

Dim e, objExcel, objSheet, i, j, introwcount
Dim wb
Dim sheet
Dim RunFinished
Dim strUsername
Dim strPassword
Dim qcDomain
Dim qcProject
Dim tdc, qcServer, testSetFactory, testSetTreeManager, testSetFolder, theTestSet, testSetsList, testSetPath, testSetName
Dim execStatus
Dim passcount, failcount, notcompletedcount, noruncount, testcasecount, resultpath, abortedcount
Dim TestCase, TestStatus, ExecutionDate, ExecutionTime, duration, sDuration, strExcelPath, ExecutionPlatform
Dim emailid, row
Dim starttime, endtime, totaltime, elapsedtime, executionend, executionstart
Dim filename, testsetAttachments
Dim execustatus
Dim sDetailedtable
Dim sTimeStart, sTimeEnd
Dim Mainpath
Dim ResultFolderPath
Dim RemainingPath
Dim LastFolder

ExcelSheetOpen

Public Function ExcelSheetOpen()

passcount = 0
failcount = 0
notcompletedcount = 0
noruncount = 0
testcasecount = 0
abortedcount = 0 ''''''''''''''''''''''''''''''''''''''''''''''


Set fs = CreateObject("Scripting.FileSystemObject")
'Mainpath = "C:\digital\digital-testing\Multi_Brand_Automation\Results\Generic"
'Mainpath = "C:\workspace\MB_Automation\Results\Generic"
Mainpath = WScript.Arguments.Item(0)

Set MainFolder = fs.GetFolder(Mainpath)
For Each fldr In MainFolder.SubFolders
    ''As per comment
    If fldr.DateLastModified > LastDate Or IsEmpty(LastDate) Then
        LastFolder = fldr.Name
        LastDate = fldr.DateLastModified
                
    End If
Next

                ExecutionStartTime = Split(LastDate, " ", -1, 1)
                ExecutionStartTimeformail = ExecutionStartTime(1)
'WScript.Echo ExecutionStartTimeformail
ResultFolderPath = Mainpath & "\" & LastFolder ''''''''''''''''''''''''''''''''''''
RemainingPath = "\Excel Results\Summary.xls"
Exactpath = ResultFolderPath & RemainingPath
'WScript.Echo Exactpath

                Set e = CreateObject("EXCEL.APPLICATION")
                e.Workbooks.Open (Exactpath)
                Set wb = e.ActiveWorkbook
                Set sheet = wb.Sheets("Result_Summary")
introwcount = sheet.UsedRange.Rows.Count
'WScript.Echo introwcount
Call initializetable

sTime = "_" & Month(Date) & "_" & Day(Date) & "_" & Year(Date) & "_" & Hour(Now()) & "_" & Minute(Now()) & "_" & Second(Now())
'stime = "_" & Day(Date) & "_"& Month(Date) & "_" & Year(Date) & "_" & Hour(Now()) & "_" & Minute(Now())
'WScript.Echo stime
For i = 2 To introwcount - 2
                
'WScript.Echo testSetName

                                'If Sheet.Cells(i,1).Value = "" Then
                                                'startExecution = False
                                'Else
                                                testcasecount = introwcount - 3

                                                testDomain = sheet.Cells(i, 1).Value

                                                TestCase = sheet.Cells(i, 2).Value

                                                testProject = sheet.Cells(i, 2).Value

                                                TestStatus = sheet.Cells(i, 7).Value

                                                ExecutionDate = Date
'if introwcount<> introwcount-2 Then
                                                testSetName = sheet.Cells(i, 1).Value
'End if
                                                starttime = ExecutionStartTimeformail

                                                'WScript.Echo starttime

                                                endtime = Hour(Now()) & ":" & Minute(Now()) & ":" & Second(Now())

                                                'WScript.Echo endtime

                                                ExecutionTime = sheet.Cells(i, 6).Value
                                
                                                ExecutionElapsedTime = DateDiff("h", endtime, starttime) & " " & "Hrs"
                                               ExecutionPlatform = sheet.Cells(i, 5).Value
'WScript.Echo ExecutionPlatform

                                                'WScript.Echo ExecutionElapsedTime

                                                testHostName = sheet.Cells(i, 5).Value

                                                Totalduration = sheet.Cells(introwcount - 1, 1).Value
'WScript.Echo Totalduration
                               durationsplitup = Split(Totalduration, ":", -1, 1)
                                                
                                                duration = durationsplitup(1)
'WScript.Echo duration
Call writetotable(TestCase, TestStatus, ExecutionDate, ExecutionTime, ExecutionPlatform)
 Next

Call sendemail
Call ExcelSheetClose

End Function

Public Function ExcelSheetClose()
                Set sheet = Nothing
                wb.Close
                Set wb = Nothing
                e.Quit
                Set e = Nothing
End Function



Function initializefile()
row = 1
sTime = "_" & Month(Date) & "_" & Day(Date) & "_" & Year(Date) & "_" & Hour(Now()) & "_" & Minute(Now()) & "_" & Second(Now())
'filename = "Test Run Result" & stime & ".xls"
filename = "Summary.html"
'resultpath = "C:\digital\digital-testing\Multi_Brand_Automation\Results\Generic\Run_20-Apr-2017_01-40-29_PM\HTML Results\"
'resultpath = "C:\workspace\MB_Automation\Results\Generic\Run_20-Apr-2017_01-40-29_PM\HTML Results\"


'strExcelPath = resultpath & filename

Set objExcel = CreateObject("Excel.Application")

If (Err.Number <> 0) Then
   Wscript.Echo "Excel application not found."
   Wscript.Quit
End If

' Create a new workbook.
objExcel.Workbooks.Add
' Bind to worksheet.
Set objSheet = objExcel.ActiveWorkbook.Worksheets(1)
objSheet.Name = "Test Run Result"
                
Call writeheader

End Function

Function writeheader()
objSheet.Cells(row, 1).Font.Size = "12"
objSheet.Cells(row, 1).Font.Bold = True
objSheet.Cells(row, 1).Interior.ColorIndex = 20
objSheet.Cells(row, 1).ColumnWidth = 20
objSheet.Cells(row, 1).Value = "Test Case Name"
objSheet.Cells(row, 2).Font.Size = "12"
objSheet.Cells(row, 2).Font.Bold = True
objSheet.Cells(row, 2).Interior.ColorIndex = 20
objSheet.Cells(row, 2).ColumnWidth = 20
objSheet.Cells(row, 2).Value = "Test Execution Status"
objSheet.Cells(row, 3).Font.Size = "12"
objSheet.Cells(row, 3).Font.Bold = True
objSheet.Cells(row, 3).Interior.ColorIndex = 20
objSheet.Cells(row, 3).ColumnWidth = 20
objSheet.Cells(row, 3).Value = "Execution Date"
objSheet.Cells(row, 4).Font.Size = "12"
objSheet.Cells(row, 4).Font.Bold = True
objSheet.Cells(row, 4).Interior.ColorIndex = 20
objSheet.Cells(row, 4).ColumnWidth = 20
objSheet.Cells(row, 4).Value = "Execution Time"
objSheet.Cells(row, 5).Font.Size = "12"
objSheet.Cells(row, 5).Font.Bold = True
objSheet.Cells(row, 5).Interior.ColorIndex = 20
objSheet.Cells(row, 5).ColumnWidth = 20
objSheet.Cells(row, 5).Value = "ExecutionPlatform"

row = row + 1

End Function


' Function closefile()
' ' Save the spreadsheet and close the workbook.
' ' Specify Excel7 File Format.
' objExcel.ActiveWorkbook.SaveAs strExcelPath
' objExcel.ActiveWorkbook.Close

' ' Quit Excel.
' objExcel.Application.Quit

' End Function


Function format(sNum)
	If sNum < 10 Then
		format = 0 & sNum
	Else
		format = sNum

	End If
End Function

Public Function ConvertTime(sTime)

h = Int(sTime / 3600)
m1 = sTime - (h * 3600)
If h < 10 Then
                h = 0 & h
End If

m = Int(m1 / 60)
s = Int(m1 - (m * 60))

If m < 10 Then
                m = 0 & m
End If

If s < 10 Then
                s = 0 & s
End If

duration = h & ":" & m & ":" & s

End Function

Public Function ConvertElapsedTime(sTime)

h = Int(sTime / 3600)
m1 = sTime - (h * 3600)
m = Int(m1 / 60)
s = Int(m1 - (m * 60))
duration = h & "hr " & m & "min " & s & "sec"

End Function


Function sendemail()

Set fs = CreateObject("Scripting.FileSystemObject")
'Mainpath = "C:\digital\digital-testing\Multi_Brand_Automation\Results\Generic"
'Mainpath = "C:\workspace\MB_Automation\Results\Generic"
Mainpath = Wscript.Arguments(0)

Set MainFolder = fs.GetFolder(Mainpath)
For Each fldr In MainFolder.SubFolders
    ''As per comment
    If fldr.DateLastModified > LastDate Or IsEmpty(LastDate) Then
        LastFolder = fldr.Name
        LastDate = fldr.DateLastModified
    End If
Next

ResultFolderPath = Mainpath & "\" & LastFolder '''''''''''''''''''''''''''''''''''''''''''''''''
RemainingPath = "\HTML Results\Summary.html"
Exactpath = ResultFolderPath & RemainingPath
'WScript.Echo Exactpath

cHour = format(Hour(Now()))
cMinute = format(Minute(Now()))
cSecond = format(Second(Now()))

aHour = cHour + 1

sTimeStart = cHour & ":" & cMinute & ":" & cSecond
sTimeEnd = aHour & ":" & cMinute & ":" & cSecond

Set objOL = CreateObject("Outlook.Application")
Set objMail = objOL.CreateItem(0)
'emailid = "sundaresan.subramanian@astrazeneca.com;Sankara.RajaKumar@astrazeneca.com;ravikumar.thangaraj@astrazeneca.com"
emailid = Trim(Wscript.Arguments(1))

filename = "Summary.html"
'resultpath = "C:\digital\digital-testing\Multi_Brand_Automation\Results\Generic\Run_20-Apr-2017_01-40-29_PM\HTML Results\"
'resultpath = "C:\workspace\MB_Automation\Results\Generic\Run_20-Apr-2017_01-40-29_PM\HTML Results\"
'strExcelPath = resultpath & filename

'With objMail
'.Subject = "VEEVA Automation Test Execution Summary"
'.To = emailid
'.Attachments.Add Exactpath
'''
'.HTMLBody = "<html><p>Hi All,<br>Please find the VEEVA Automation Execution Report below.<br><br> Note: This is an Auto generated Report Mail, "
'If there are some failed test cases, then it will be analysed by automation team and Defects will be raised accordingly</p></html><html><font face = Calibri>" & "<table border=1 cellspacing = 0 cellpadding=5><tr><td>Executed by </td>" & "<td>" & "VEEVA Automation Team" & "</td></tr>" &"<tr><td>Executed on </td>" & "<td>" & Date() & "</td></tr>" & "<td>Execution Start Time </td>" & "<td>" & starttime & "</td></tr>"& "<td>Execution End Time </td>" & "<td>" & endtime & "</td></tr>" & "<td>Execution Elapsed Time </td>" & "<td>" &duration& "</td></tr>" &  "<td>Total Number of Test cases </td>" & "<td>" & testcasecount & "</td></tr>" & "<td>Passed </td>" & "<td><font color = green>" & passcount & "</font></td></tr>" & "<td>Failed</td>" & "<td><font color = red>" & failcount & "</font></td></tr>" & "<td>Not Completed </td>" & "<td>" & notcompletedcount & "</td></tr>" & "<t
'd>No Run </td>" & "<td>" & noruncount & "</td></tr></table>" & "<br>" & sDetailedtable & "</html><html><p>Thanks & Regards,<br>Automation Team</p></html>"
'.Send

objMail.Subject = "VEEVA Automation Test Execution Summary"
objMail.To = emailid
objMail.Attachments.Add Exactpath

strMailBody = "<html><p>Hi All,<br>Please find the VEEVA Automation Execution Report below.<br><br> Note: This is an Auto generated Report Mail, " & _
"If there are some failed test cases, then it will be analysed by automation team and Defects will be raised accordingly</p></html><html><font face = Calibri>" & _
"<table border=1 cellspacing = 0 cellpadding=5>" & _
"<tr><th>Executed by </th>" & "<td>" & "VEEVA Automation Team" & "</td></tr>" & _
"<tr><th>Executed on </th>" & "<td>" & Date & "</td></tr>" & _
"<tr><th>Execution Start Time </th>" & "<td>" & starttime & "</td></tr>" & _
"<tr><th>Execution End Time </th>" & "<td>" & endtime & "</td></tr>" & _
"<tr><th>Execution Elapsed Time </th>" & "<td>" & duration & "</td></tr>" & _
"<tr><th>Total Number of Test cases </th>" & "<td>" & testcasecount & "</td></tr>" & _
"<tr><th>Passed </th>" & "<td><font color = green>" & passcount & "</font></td></tr>" & _
"<tr><th>Failed</th>" & "<td><font color = red>" & failcount & "</font></td></tr>" & _
"<tr><th>Aborted </th>" & "<td>" & abortedcount & "</td></tr>" & _
"<tr><th>Not Completed </td>" & "<td>" & notcompletedcount & "</td></tr>" & _
"<tr><th>No Run </th>" & "<td>" & noruncount & "</td></tr>" & _
"</table>" & "<br>" & _
sDetailedtable & "</html><html><p>Thanks & Regards,<br>Automation Team</p></html>"

objMail.HTMLBody = strMailBody
objMail.Send

Set iMsg = Nothing
Set iConf = Nothing

End Function

Function initializetable()

sDetailedtable = "<table border = 1 cellspacing = 0 cellpadding = 5><b><tr><td> Test Set Name</td><td>Test Case Name</td><td>Test Execution Status</td><td>Execution Date</td><td>Execution Time</td><td>Execution Platform</td></b></tr>"

End Function

Function writetotable(TestCase, TestStatus, ExecutionDate, ExecutionTime, ExecutionPlatform)

sDetailedtable = sDetailedtable & "<tr><td>" & testSetName & "</td><td>" & TestCase & "</td>"


If (TestStatus = "Passed") Then
                passcount = passcount + 1
                sDetailedtable = sDetailedtable & "<td bgcolor = green>" & TestStatus & "</td>"
End If


If (TestStatus = "Failed") Then
                failcount = failcount + 1
                sDetailedtable = sDetailedtable & "<td bgcolor = red>" & TestStatus & "</td>"
End If


If (TestStatus = "Not Completed") Then
                notcompletedcount = notcompletedcount + 1
                sDetailedtable = sDetailedtable & "<td bgcolor = gray>" & TestStatus & "</td>"
End If


If (TestStatus = "No Run") Then
                noruncount = noruncount + 1
                sDetailedtable = sDetailedtable & "<td>" & TestStatus & "</td>"
End If

If (TestStatus = "Aborted") Then
                abortedcount = abortedcount + 1
                sDetailedtable = sDetailedtable & "<td bgcolor = gray>" & TestStatus & "</td>"
End If

sDetailedtable = sDetailedtable & "<td>" & ExecutionDate & "</td><td>" & ExecutionTime & "</td><td>" & ExecutionPlatform & "</td></tr>"

'testcasecount = testcasecount + 1

End Function

Function closetable()
                sDetailedtable = sDetailedtable & "</table>"
End Function

Function writetofile(TestCase, TestStatus, ExecutionDate, ExecutionTime, duration)
objSheet.Cells(row, 1).Interior.ColorIndex = 2
objSheet.Cells(row, 1).Value = TestCase

If (TestStatus = "Passed") Then
passcount = passcount + 1
objSheet.Cells(row, 2).Interior.ColorIndex = 50
End If

If (TestStatus = "Failed") Then
failcount = failcount + 1
objSheet.Cells(row, 2).Interior.ColorIndex = 3
End If

If (TestStatus = "Not Completed") Then
notcompletedcount = notcompletedcount + 1
End If

objSheet.Cells(row, 2).Value = TestStatus

objSheet.Cells(row, 3).Interior.ColorIndex = 2
objSheet.Cells(row, 3).Value = ExecutionDate

objSheet.Cells(row, 4).Interior.ColorIndex = 2
objSheet.Cells(row, 4).Value = ExecutionTime

objSheet.Cells(row, 5).Interior.ColorIndex = 2
objSheet.Cells(row, 5).Value = duration
row = row + 1
End Function

