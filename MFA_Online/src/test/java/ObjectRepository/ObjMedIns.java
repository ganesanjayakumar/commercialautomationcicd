
package ObjectRepository;

import org.openqa.selenium.By;




public class ObjMedIns {	
	
	public static By loginFormVeeva = By.xpath("//div[@id='theloginform']/form");
	public static By loginUserName = By.id("username");
	public static By loginPassword = By.id("password");	
	public static By loginSubmit = By.id("Login");
	public static By loginError = By.xpath("//div[@id='theloginform']//div[@id='error']");
	public static By passwordResetCancel = By.id("cancel-button");
	public static By menu = By.id("tabBar");
	public static By accountstab=By.xpath("//a[.='My Accounts']");
	public static By viewdropdown=By.id("vwid");
	public static By sframe=By.id("itarget");
	public static By moreactionsButton=By.name("moreActionsButton");
	public static By productSelection=By.id("filterTableBody");
	public static By mailFrame=By.id("vod_iframe");
	public static By addDocuments=By.xpath("//button[.='Add Documents']");
	public static By addSelectedDoc=By.xpath("//button[@class='addSelectedDocs']");
    public static By sendButton=By.className("sendButton");
    public static By manageEmailAddress=By.className("emailSelect");
    public static By manageSecondaryEmail=By.xpath("//td[.='Secondary Email']//following-sibling::td/input");
    public static By manageEmailSave=By.id("email_mgmt_save");
    public static By addTemplatesButton=By.id("addTemplatesButton");
    public static By changeOptPreferences=By.xpath("//a[.='Change Opt Preferences']");
    public static By changeOptContinue=By.xpath("//input[@name='continueButton' and @title='Continue']");
    public static By paperconsent=By.xpath("//div[@class='requiredInput']//input");
    public static By papercosentsave=By.name("Save");
    public static By papercosentfinish=By.name("Save");
    public static By  successNote=By.id("successNote");
    public static By close=By.id("cancelButton");
    public static By previewButton=By.xpath("//button[.='Preview']");
    public static By deletedoclink =By.xpath("//td[@class='removeDoc']//img");
    //Logout
    public static By logOutUser=By.id("userNavLabel");
    public static By logOutLink=By.xpath("//div[@id='userNav-menuItems']//a[.='Logout']");
    
    //Call Standardization 
    public static By searchPanel = By.className("standardSearchElementBody");
    public static By searchAll = By.xpath("//select[@id='sen']");
    public static By searchBox = By.xpath("//input[@id='sbstr' and @class='searchTextBox']");
    public static By go = By.xpath("//*[@value=' Go! ']");
    public static By accountDetailsPage = By.id("bodyCell");
    public static By goSelect = By.className("autocompleteMatch");
    
    
    public static By samplePromo_Heading =By.xpath("//h3[text()='Samples And Promotional Items']");

    
    //Medical Insight
    
    public static By newKeyMedicalInsight_button = By.xpath("//td[@id='topButtonRow']/input[@value='New Key Medical Insight']");  
    public static By newKeyMedicalInsight_button_new = By.xpath("//td[@id='topButtonRow']/input[@value='New Dx/Medical Insights']"); 
    public static By therapauticAreaDrpdwn = By.id("MA_Therapeutic_Area_AZ__c");
    public static By disnind_drpdwn = By.id("MA_Disease_Indication_AZ__c");
    public static By insightTopic_drpDwn = By.id("MA_Scientific_Insight_Topic_AZ__c");
    public static By medInsightSummary_text = By.id("Summary_vod__c");
    public static By medInsightDesc_text = By.xpath("//div[@class='rich-text-area ng-scope']/div[2]");
    /*Core 29 - As part of this release this field is removed*/
   // public static By insightCategory_Drpdwn = By.id("MA_Insight_Category_AZ__c");
   // public static By insightSubCategory_Drpdwn = By.id("MA_Insight_Sub_Category_AZ__c");
    public static By venue_DrpDwn = By.id("MA_Venue_AZ__c");
    public static By medicalEvent_Txt = By.id("Medical_Event_vod__c");
    public static By medicalEvent_search = By.xpath("//span[@class='lookupInput']/a[@title='Medical Event Lookup']");
    public static By medicalEvent_searchPage = By.xpath("//div[@class='modal-header ng-scope']//div[text()='Medical Event']");
    public static By attest_DrpDwn=By.id("MA_Insight_Patient_Safety_Attestation_AZ__c"); 
    public static By account = By.id("Account_vod__c");
    public static By dateofinsight = By.xpath("//span[@class='ng-scope dateInput ng-isolate-scope ng-valid ng-valid-required']/input");
    public static By save_Btn = By.xpath("//*[@id='bottomButtonRow']//span/input[@name='Save']");
    public static By save_Status = By.xpath("//label[text()='Status']");
    public static By saved_StatusValue = By.name("Status_vod__c");
    public static By med_aff = By.xpath("//label[text()='Medical Affairs']");
    public static By country_code = By.xpath("//label[text()='Country Code']");
    public static By submit_Btn = By.xpath("//div[@class='pbBottomButtons']/table/tbody/tr/td[2]/span[3]/input");
    public static By submit_Status = By.xpath("//label[text()='Status']");
    public static By submit_Account = By.xpath("//label[text()='Account']");
    public static By submit_AccountLink = By.name("Account_vod__c");
    public static By medicalInsightID = By.xpath("//h2[@class='pageDescription ng-binding']");
    public static By medIns_relatedListLink = By.xpath("//span[@class='listTitle'][text()='Medical Insights']");
    public static By medIns_relatedList = By.xpath("//span[@class='listTitle' and text()='Medical/Dx Insights']");
    public static By goToLink = By.xpath("//h3[contains(text(),'Medical Insights')]/ancestor::div[1]/following-sibling::div//a[contains(text(),'Go to')]");
    public static By save_TherapyArea = By.name("MA_Therapeutic_Area_AZ__c");
    public static By save_DisInd = By.name("MA_Disease_Indication_AZ__c");
    public static By save_Topic = By.name("MA_Scientific_Insight_Topic_AZ__c");
    public static By save_Summary = By.name("Summary_vod__c");
    public static By save_Desc = By.name("Description_vod__c");
    /*Core 29 - As part of this release this field is removed*/
   // public static By submit_Cat = By.name("MA_Insight_Category_AZ__c");  
   // public static By submit_SubCat = By.name("MA_Insight_Sub_Category_AZ__c");
    public static By submit_Venue = By.name("MA_Venue_AZ__c");
    public static By submit_attest = By.name("MA_Insight_Patient_Safety_Attestation_AZ__c");
    
    
    
    
    
    

}