package ObjectRepository;

import org.openqa.selenium.By;

public class objGlobalAccountSearchPage {

	public static By editName=By.xpath("//td[contains(text(),'Name')]/following::input[@type='text']");//td[contains(text(),'Name')]/following-sibling::input[type='text']");
	public static By radNameExactMatch=By.xpath("//td[contains(text(),'Name')]/following::input[@value='Exact Match']");//td[contains(text(),'Name')]/following-sibling::input[value='Exact Match']");
	public static By radNameContains=By.xpath("//td[contains(text(),'Name')]/following::input[@value='Contains']");

	public static By editFirstName=By.xpath("//td[contains(text(),'First Name')]/following::input[@type='text']");
	public static By radFirstNameExactMatch=By.xpath("//td[contains(text(),'First Name')]/following::input[@value='Exact Match']");
	public static By radFirstNameContains=By.xpath("//td[contains(text(),'First Name')]/following::input[@value='Contains']");

	public static By editLastName=By.xpath("//td[contains(text(),'Last Name')]/following::input[@type='text']");
	public static By radLastNameExactMatch=By.xpath("//td[contains(text(),'Last Name')]/following::input[@value='Exact Match']");
	public static By radLastNameContains=By.xpath("//td[contains(text(),'Last Name')]/following::input[@value='Contains']");

	public static By editSpecialities=By.xpath("//td[contains(text(),'Specialties')]/following::input[@type='text']");
	public static By radSpecialitiesStartsWith=By.xpath("//td[contains(text(),'Specialties')]/following::input[@value='Exact Match']");
	public static By radSpecialitiesContains=By.xpath("//td[contains(text(),'Specialties')]/following::input[@value='Contains']");
	public static By selectPrimarySpecialty=By.xpath("//td[contains(text(),'Primary Specialty')]/following::select");
	
	public static By lnkSearch=By.cssSelector("td[id*='bottom']>input[value='Search']");
	
	public static By tblSearchResults=By.id("restable");
	public static By chkFirstSearchResult=By.cssSelector("#restable input[type='checkbox']");
	public static By btnAddToTerritory=By.cssSelector("input[value='Add To Territory']");
	
	public static By lblAlerts=By.cssSelector("ul[role='alert']");
	public static By lblPageErrorAlert=By.cssSelector("#theErrorPage:theError");
	
	public static By lstTerritory=By.cssSelector("select#terrId");
	public static By lnkAccounts=By.cssSelector("td.dataCell>a");
	public static By lstViewSelection=By.id("vwid");
			
}
