package ObjectRepository;

import org.openqa.selenium.By;

public class objHCMSearchAndAdd {

	public static By selectState=By.xpath("//label[contains(text(),'State')]/following-sibling::select");//td[contains(text(),'Name')]/following-sibling::input[type='text']");
	public static String strState="//label[contains(text(),'State')]/following-sibling::select";//td[contains(text(),'Name')]/following-sibling::input[type='text']");
	public static By txtExternalID=By.xpath("//label[contains(text(),'External ID')]/following-sibling::input[@type='text']");
	public static By txtFirstName=By.xpath("//label[contains(text(),'First Name')]/following-sibling::input[@type='text']");
	public static By txtLastName=By.xpath("//label[contains(text(),'Last Name')]/following-sibling::input[@type='text']");
	public static String xpathExternalID="//label[contains(text(),'External ID')]/following-sibling::input[@type='text']";
	public static String xpathFirstName="//label[contains(text(),'First Name')]/following-sibling::input[@type='text']";
	public static String xpathLastName="//label[contains(text(),'Last Name')]/following-sibling::input[@type='text']";
	public static String strHCPTable="//td[contains(@id,'tabHCP')]";
	public static String strHCATable="//td[contains(@id,'tabHCA')]";
	
			
}
