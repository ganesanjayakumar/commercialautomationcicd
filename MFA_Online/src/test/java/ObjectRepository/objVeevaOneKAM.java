package ObjectRepository;

import java.util.Date;

import org.openqa.selenium.By;

public class objVeevaOneKAM {

	public static By accountPlanPageTitle=By.xpath("//h1[normalize-space(text())='Account Plans']");
	public static By accountPlanEditPageTitle=By.xpath("//h1[normalize-space(text())='Account Plan Edit']");
	
	
	
	/**************************Global variables for One KAM************************************************/
	public String strAccountName="";
	public String strAccountPlanName="";
	public String strProductName="";
	public String strDecisionMakingUnitID="";
	public String strAccountPlanTeamMembers="";
	public int intSequenceNumber=0;
	public String strTacticName ="";
	public String strLocale="", strProfile="";
	
	public String strBackgroundInformation="";
	public String strSWOTStrength="";
	public String strSWOTWeakness="";
	public String strSWOTOpportunity="";
	public String strSWOTThreats="";
	public String strGoalsCriticalSuccesFactors="";
	public String strGoal="";
	public String strConfidenceStatus="";
	public String strConfidenceLevel="";
	public String strConfidenceReason="";
	public Date dateConfidenceLevelChangeDate;
	public String strCondifenceStatusIndicator="";
	
}

