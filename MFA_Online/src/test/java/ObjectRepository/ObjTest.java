
package ObjectRepository;

import org.openqa.selenium.By;

public class ObjTest {	
    
	public static By home =By.xpath("//*[@id='phHeaderLogoImage']");
	public static By cancelPasswordChange = By.xpath("//*[@id='cancel-button']");
	public static By homeButton = By.xpath("//*[@id='home_Tab']");
	
	public static By frame_ContactList = By.xpath("//iframe[contains(@src,'/003?fcf=')]");
	public static By frame_NewContactUpdate = By.xpath("//iframe[contains(@src,'/003/e?')]");
	
	public static String strXPath = "//input[@id='test']";
	
}
