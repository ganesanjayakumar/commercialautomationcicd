package ObjectRepository;

import org.openqa.selenium.By;

public class objMyAccounts {

	public static By sframe=By.id("vod_iframe");
	public static By selectView=By.id("vwid");
	public static By aAccountLink=By.cssSelector("#vodResultSet td>a[target='_top']");
	public static By btnSurveyTarget=By.cssSelector("input[value='New Survey Target']");
	
	
	////////Created By Kamatchirajan
	////////////////////////////////////////////iFrames
	public static By frame_ITarget = By.xpath("//iframe[@id='itarget']");
	public static By frame_RelationshipMap = By.xpath("//iframe[@title='SphereOfInfluence']");
	public static By frame_AccountTerritoryInfo = By.xpath("//iframe[@title='Account Territory Info']");
	public static By frame_Hieratchy = By.xpath("//iframe[@id='vod_iframe']");
	public static By frame_VOD = By.xpath("//iframe[@id='vod_iframe']");
	
	public static By login_UserNavLabel = By.xpath("//div[@class='navLinks']//div[@id='userNav']");
	
	public static By accountDetails_TopName = By.xpath("//div[@class='bPageTitle']//h2[@class='topName' or @class='pageDescription']");
	public static By userTerritory_Page_Heading = By.xpath("//div[@class='accountBlock']//div[@class='pbHeader']//h2[@class='mainTitle']");
	public static String userTerritory_TableRowsXPath = "//div[@class='accountBlock']//div[@class='pbBody']//table/tbody/tr";
	
	public static By relationship_Container = By.xpath("//div[@id='container']");
	public static By newRelationship = By.xpath("//div[@id='container']//a[@title='New Relationship' and 1]");
	public static By relationshipCircle = By.xpath("//div[@id='container']//circle");
	
	public static By myPreferredAddress = By.xpath("//td[@class='labelCol']/label[text()='My Preferred Address']/following::td[1]");
	public static By table_Addresses = By.xpath("//div[@class='bRelatedList']//td[@class='pbTitle']//h3[text()='Addresses']");
	
	
	//view Hierarchy
	public static By byHierarchy_Table = By.xpath("//table[@id='vodhierarchytable']");
	public static By byHierarchy_Table_HCP_Self = By.xpath("//table[@id='vodhierarchytable']//tr[@id='vodself']//a");
	public static By byHierarchy_Table_HCP_PrimaryParent = By.xpath("//table[@id='vodhierarchytable']//td[@class='vodacct vodprimary vodacct-Green']/a");	
	public static By byHierarchy_Table_HCA_Self = By.xpath("//table[@id='vodhierarchytable']//tr[@id='vodself']//td[@class='vodself vodacct vodacct-Green']//a");
//	public static By byHierarchy_Table_HCA_Child = By.xpath("//table[@id='vodhierarchytable']//tr//td[@class='vodacct vodacct-Clear']//a");
	public static By byHierarchy_Table_HCA_Child = By.xpath("//table[@id='vodhierarchytable']//tr//td[starts-with(@class,'vodacct vodacct')]//a");	
	public static By byHierarchy_Table_Connect = By.xpath("//table[@id='vodhierarchytable']//tr[@class='vodconnect']");
	public static By byHierarchy_BackToAccount = By.xpath("//div[@id='bPageTitle']//div[@id='ptBreadcrumb']/span/a");
	
	//View Overview
	public static By overView_AccountLink = By.xpath("//div[@class='bPageTitle']//a[@id='ptBreadcrumb']");
	
	public static By timeLine_Calls = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Calls']/preceding::img[1]");
	public static By timeLine_Events = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Events']/preceding::img[1]");	
	public static By timeLine_MedicalInquiries = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Medical Inquiries']/preceding::img[1]");
	public static By timeLine_Meetings = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Meetings']/preceding::img[1]");
	public static By timeLine_MultichannelActivities = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Multichannel Activities']/preceding::img[1]");
	public static By timeLine_SentEmail = By.xpath("//div[@class='timelineLegend']//span[@class='labelText ng-binding'][text()='Sent Email']/preceding::img[1]");
	
	public static By filterByOwnerCheckbox = By.xpath("//input[@id='filterByOwnerCheckbox'][@type='checkbox']");
	
	
}
