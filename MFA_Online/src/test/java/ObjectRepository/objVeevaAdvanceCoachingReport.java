package ObjectRepository;

import org.openqa.selenium.By;

public class objVeevaAdvanceCoachingReport
{
	public static By goButton = By.xpath("//label[contains(text(),'View')]//following::input[@name='go']");
	public static By plussymbol = By.xpath("//img[@class='allTabsArrow']");
	public static By surveysLink = By.xpath("//table[@class='detailList tabs']//a[.='Surveys']");
	public static By advanceCoachingLink = By.xpath("//table[@class='detailList tabs']//a[.='Advanced Coaching Reports']");
	public static By viewdropdownsurvey = By.xpath("//select[@id='fcf']");
	public static By newButton = By.xpath("//input[@name='new']");
	public static By recordType = By.xpath("//select[@id='p3']");
	public static By continueReport = By.xpath("//td[@id='bottomButtonRow']//input[@name='save']");
	public static By surveyname = By.xpath("//input[@id='reqi-value:string:Survey_vod__c:Name']");
	public static By mansaveReport = By.xpath("//div[@id='pbBottomButtons']//input[@name='Save']");
	public static By selectterritories = By.xpath("//input[@title='Select Territories']");
	public static By opsNewQuestion = By.id("Q_New_Question_Btn");
	public static By opssurveyQuestionarea = By.xpath("//h3[.='Question Text']/parent::div/following-sibling::div//textarea");
	// Changed by Murali on July 23
	// public static By opsanswerchoice1=By.xpath("//div[@class='qlistitem'][1]//div[1]/div[1]/input");
	// public static By opsanswerchoice2=By.xpath("//div[@class='qlistitem'][2]//div[1]/div[1]/input");
	public static By opsanswerchoice1 = By.xpath("//div[@class='qlistitem'][1]//div[1]/div[1]/textarea");
	public static By opsanswerchoice2 = By.xpath("//div[@class='qlistitem'][2]//div[1]/div[1]/textarea");
	public static By opsweight1 = By.xpath("//div[@class='qlistitem'][1]//div[1]/div[2]/input");
	public static By opsweight2 = By.xpath("//div[@class='qlistitem'][2]//div[1]/div[2]/input");
	public static By opsquestionsave = By.id("Q_EditSave");
	public static By opsquestionbankbutton = By.id("Q_Question_Bank_Btn");
	public static By opscalcsharing = By.id("CalculateSharing");
	public static By opspublish = By.id("Publish");
	public static By manNewcoachingReport = By.xpath("//input[@title='New Coaching Report']");
	public static By mannewcoachingReportButtoninNewcoachingRepPage = By.xpath("//input[@title='New Coaching Report']");
	public static By surveydetailstartdate = By.xpath("//*[@id='Start_Date_vod__c']/input");
	public static By surveydetailenddate = By.xpath("//*[@id='End_Date_vod__c']/input");
	public static By surveydetaildurationvisit = By.id("Duration_of_Field_Visit_AZ__c");// By.xpath("//select[@id='reqi-value:picklist:Survey_Target_vod__c:Duration_of_Field_Visit_AZ__c']");
	// added by JK on Jul 23, 2019
	public static By surveydetailReviewdatedev = By.xpath("//*[@id='Review_Date_vod__c']/input");
	public static By surveydetailReviewdate = By.xpath("//span[@id='Review_Date_vod__c']//a");
	public static By surveydetailsurveyName = By.xpath("//veev-edit-field/div/span/input[@id='Name']");
	public static By surveydetailmanager = By.name("Coach_vod__c");// By.xpath("//input[@id='reqi-value:reference:Survey_Target_vod__c:Coach_vod__c']");
	public static By surveysendtoEmployee = By.xpath("//div[contains(@class,'pbHeader')]//input[@name='Send_To_Employee_vod']");
	public static By surveysave = By.xpath("//*[@id='bottomButtonRow']/span[4]/input");
	public static By employeesurveytargetacknowledge = By.xpath("//div[contains(@class,'pbHeader')]//input[@name='Complete_vod']");
	public static By recall = By.xpath("//div[contains(@class,'pbHeader')]//input[@name='Recall_vod']");
	public static By managerclone = By.xpath("//div[contains(@class,'pbHeader')]//input[@name='Clone_vod']");
	public static By mansurveyclonestatus = By.name("Report_Status_vod__c");
	public static By lookupemployeego = By.name("go");
	public static By cnmedicalaffairs = By.xpath("//select[@id='i-value:picklist:Survey_vod__c:MA_Medical_Affairs_AZ__c']");
	public static By engagementType = By.xpath("//select[@id='MA_Engagement_Type_AZ__c']");
}
