package ObjectRepository;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

public class ObjectMap {

	static Properties prop;
	
	public static void main(String[] args){
		//System.out.println(System.getProperty("user.dir")+"\\src\\test\\java");
	}
	static
	{
	//This method will be called when an object is created and object repository will be instantiated
			prop = new Properties();

			try {
				FileInputStream file = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\objectmap.properties");
				prop.load(file);
				file.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	public ObjectMap() {
	//This method will be called when an object is created and object repository will be instantiated
		prop = new Properties();

		try {
			FileInputStream file = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\objectmap.properties");
			prop.load(file);
			file.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	public static String getProperyData(String strElement) throws Exception {

		// retrieve the specified Property value from Global Property
		String strData = null;
		try {
			strData = prop.getProperty(strElement);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return strData;
	}

	public static By getLocator(String strElement) throws Exception {

		// retrieve the specified object from the object list
		String locator = ObjectMap.getProperyData(strElement);
		//locator = prop.getProperty(strElement);
		// extract the locator type and value from the object
		String locatorType = locator.split(":")[0];
		String locatorValue = locator.split(":")[1];

		if (locatorType.toLowerCase().equals("xpath"))
			return By.id(locatorValue);
		else if(locatorType.toLowerCase().equals("id"))
			return By.xpath(locatorValue);
		else if (locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if ((locatorType.toLowerCase().equals("classname"))
				|| (locatorType.toLowerCase().equals("class")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("tagname"))
				|| (locatorType.toLowerCase().equals("tag")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("linktext"))
				|| (locatorType.toLowerCase().equals("link")))
			return By.linkText(locatorValue);
		else if (locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if ((locatorType.toLowerCase().equals("cssselector"))
				|| (locatorType.toLowerCase().equals("css")))
			return By.cssSelector(locatorValue);
		
		else
			throw new Exception("Unknown locator type '" + locatorType + "'");
	}
}

