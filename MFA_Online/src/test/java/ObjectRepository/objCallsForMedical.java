package ObjectRepository;

import org.openqa.selenium.By;

public class objCallsForMedical {

	public static By sframe=By.id("itarget");
	public static By selectView=By.id("vwid");
	public static By aAccountLink=By.cssSelector("#vodResultSet td>a[target='_top']");
	public static By btnSurveyTarget=By.cssSelector("input[value='New Survey Target']");
	public static By btnRecordACall=By.cssSelector("input[value='Record a Call']");
	public static By newcallReportPage=By.xpath("//h2[.='New MSL Interaction']");
	
	public static By listMandatoryFields=By.cssSelector("[ng-required=required][required=required]");
	public static By accountrequired=By.xpath("//label[.='Account']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By selectAddress=By.cssSelector("div[name='Address_vod__c']>select");		////label[.='Address']/parent::div/following-sibling::div//select
	public static By selectAffiliation=By.cssSelector("div[name='zvod_Business_Account_vod__c']>select");
	public static By selectInitiatedBy=By.xpath("//label[.='Initiated by']/parent::div/following-sibling::div//select");
	public static By selectEngagementType=By.xpath("//label[.='Engagement Type']/parent::div/following-sibling::div//select");
	public static By selectChannel=By.cssSelector("select[name='MA_Channel_AZ__c']");////label[.='Interaction Channel']/parent::div/following-sibling::div//select
	public static By selectSafetyAttestation=By.id("MA_Patient_Safety_Attestation_AZ__c");
	public static By selectAdditionalAZAttendee=By.id("MA_Additional_MA_Attendee_AZ_US__c");
	public static By textDate=By.cssSelector("input[ng-model='dateModel.modifiedDate']");
	public static By textTimeHours=By.cssSelector(".time-section.hours input");
	public static By textTimeMinutes=By.cssSelector(".time-section.minutes input");
	public static By labelDateTimeFormat=By.cssSelector("#Call_Datetime_vod__c a[ng-click*=updatePicker]");
	public static By selectDuration=By.cssSelector("#MA_Duration_AZ__c");
	public static By setDuration=By.xpath("//label[.='Duration(mins)']/parent::div/following-sibling::div//input");
    public static By affiliationrequired=By.xpath("//label[.='Affiliation']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
    public static By affiliationdropdown=By.xpath("//div[@name='zvod_Business_Account_vod__c']/select");
    public static By selectCallForm=By.xpath("//label[.='Call Form']/parent::div/following-sibling::div//select");
    
	public static By linkMedicalEventLookup=By.cssSelector("a[title='Medical Event Lookup']");
	public static By linkMeetingEventLookup=By.cssSelector(".lookupInput a[title*='Meeting']");
	
	public static By linkMedicalLookup=By.xpath("//input[@name='Medical_Event_vod__c']/following-sibling::a");
	public static By textRecordTypeID=By.cssSelector("#RecordTypeId");

	public static By btnSearch=By.cssSelector("div[name='zvod_Attendees_vod__c'] input[value='Search']");
	public static By btnSave=By.cssSelector("#bottomButtonRow input[name='Save']");
	public static By btnSaveAndNew=By.cssSelector("#bottomButtonRow input[name='SaveAndNew']");
	public static By btnSubmit=By.cssSelector("#bottomButtonRow input[name='Submit']");
	public static By btnMoreActions=By.cssSelector(".pbBottomButtons input[name='MoreActions']");//#bottomButtonRow input[name='MoreActions']");
	public static By lnkMedicalInquiryInCalls=By.xpath("//div[@class='popover bottom display_block']//a[.='Medical/Dx Inquiry']");
	public static By btnEditInSavedPage=By.cssSelector(".pbButtonb input[name='Edit']");
	
	public static By labelsDiscussion=By.xpath("//h3[contains(text(),'Medical Discussions')]/following::label[@class='ng-binding']");

	//label[normalize-space(text())='On-going Clinical Study']/following-sibling::input[@value='Add Discussion']
	
	//************* Added the below objects as per latest changes in Medical discussions********************************************************************//
	public static By tablesOfDiscussions=By.xpath("//label[contains(text(),'Clinical Study')]/parent::td/parent::tr/parent::tbody/parent::table");//("//label[.='  Clinical Study ']/parent::td/parent::tr/parent::tbody/parent::table");
	public static By btnClinicalDiscussionCopy=By.xpath("//label[contains(text(),'Clinical Study')]/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='addDiscussion(discussion)']");
	public static By btnClinicalDiscussionDel=By.xpath("//label[contains(text(),'Clinical Study')]/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='removeDiscussion(discussion)']");
	public static By btnClinicalDiscussionAdd=By.xpath("//label[contains(text(),'Clinical Study')]/following-sibling::input[@value='Add Discussion']");

	public static By btnMedicalInsightCopy=By.xpath("//label[normalize-space(text())='New Medical Insight']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='addDiscussion(discussion)']");
	public static By btnMedicalInsightDel=By.xpath("//label[normalize-space(text())='New Medical Insight']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='removeDiscussion(discussion)']");
	public static By btnMedicalInsightAdd=By.xpath("//label[normalize-space(text())='New Medical Insight']/following-sibling::input[@value='Add Discussion']");
	//**************End of new changes**************************************************************************************************************************//
	public static By btnCollaborationCopy=By.xpath("//label[.='  Collaboration ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='addDiscussion(discussion)']");
	public static By btnCollaborationDel=By.xpath("//label[.='  Collaboration ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='removeDiscussion(discussion)']");
	public static By btnCollaborationAdd=By.xpath("//label[.='  Collaboration ']/following-sibling::input[@value='Add Discussion']");

	public static By btnDiseaseAwarenessCopy=By.xpath("//label[.='  Disease Awareness ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='addDiscussion(discussion)']");
	public static By btnDiseaseAwarenessDel=By.xpath("//label[.='  Disease Awareness ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='removeDiscussion(discussion)']");
	public static By btnDiseaseAwarenessAdd=By.xpath("//label[.='  Disease Awareness ']/following-sibling::input[@value='Add Discussion']");

	public static By btnProductValueCopy=By.xpath("//label[.='  Product Value ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='addDiscussion(discussion)']");//label[.='  Clinical Study ']//following::a[.='Copy']");////a[.='Copy'][@ng-click='addDiscussion(discussion)']");
	public static By btnProductValueDel=By.xpath("//label[.='  Product Value ']/parent::td/parent::tr/parent::tbody/parent::table//a[@ng-click='removeDiscussion(discussion)']");////label[.='  Clinical Study ']//following::a[.='Del']");////a[.='Del'][@ng-click='removeDiscussion(discussion)']");
	public static By btnProductValueAdd=By.xpath("//label[.='  Product Value ']/following-sibling::input[@value='Add Discussion']");

	public static By copyButtons=By.xpath("//a[.='Copy'][@ng-click='addDiscussion(discussion)']");
	public static By delButtons=By.xpath("//a[.='Del'][@ng-click='removeDiscussion(discussion)']");
	
	public static By scheduleCallButton=By.xpath("//input[@name='scheduleCallButton']");
	public static By scheduleCall=By.xpath("//button[@id='schedCallsbutton']");
	public static By week = By.xpath("//a[text()='Week']");
	public static By callInteractionHeader = By.xpath("//h1[@class='pageType ng-binding']");
	/** objects for clinical study **/
	public static By selectDiscussionIntent=By.xpath("//label[.='Discussion Intent']/parent::div/following-sibling::div//select");
	public static By selectTherapeuticArea=By.xpath("//label[.='Therapeutic Area']/parent::div/following-sibling::div//select");
	public static By selectDiseaseIndication=By.xpath("//label[.='Disease and Indication']/parent::div/following-sibling::div//select");
	public static By searchClinicalStudy=By.xpath("//label[.='Clinical Study']/parent::div/following-sibling::div//a");
	public static By searchProduct=By.xpath("//label[.='Product']/parent::div/following-sibling::div//a");
	public static By searchText=By.cssSelector("#Product_vod__c_searchText");
	public static By btnGo=By.cssSelector("[name='lookup_form'] > input[name='go']");
	public static By linkInsideSearch=By.cssSelector(".pbBody.lookup-results .dataCell > a");
	public static By selectClinicSupport=By.xpath("//label[.='Clinical Study Support Activity']/parent::div/following-sibling::div//select");
	
	/** objects for collaboration **/
	public static By selectCollaboration=By.xpath("//label[.='Collaboration']/parent::div/following-sibling::div//select");
	/** objects for Disease awareness **/
	public static By selectDiseaseAwareness=By.xpath("//label[.='Disease Awareness']/parent::div/following-sibling::div//select");
	/** objects for Product value**/
	public static By selectProductValue=By.xpath("//label[.='Product Value']/parent::div/following-sibling::div//select");
	public static By selectProductSubValue=By.xpath("//label[.='Product Sub-Value']/parent::div/following-sibling::div//select");
	
	
	public static By attendesSearch=By.xpath("//div[@name='zvod_Attendees_vod__c']//input[@type='button'][@value='Search']");
    public static By attendeesearchinput=By.id("searchInput");
    public static By attendeesearchgo=By.xpath("//input[@value='Go!']");
    public static By addattendee=By.xpath("//input[@id='searchInput']/parent::td/parent::tr/following-sibling::tr//input[@title='Add']");
    public static By attendeecheckbox=By.xpath("//input[@id='searchInput']/parent::td/parent::tr//following-sibling::tr//table//tr[1]//td[1]/input"); 
    
    public static By linkAccountNameInDetailsPage=By.cssSelector("div[name=Account_vod__c] > a");
	public static By mySettings=By.xpath("//a[.='My Settings']");
	public static By personalsettings=By.xpath("//span[.='Personal']");
	public static By advanceduserdetails=By.id("AdvancedUserDetails_font");
	public static By preferredchannel=By.xpath("//td[.='Preferred Channel']/following-sibling::td");
	
	public static By errorPatientSafety=By.xpath("(//div[text()='Patient Safety Attestation field is required when submitting the call'])[2]");
	public static By errorVenue=By.xpath("//label[.='Venue']/parent::div/following-sibling::div//div[@class='requiredInput ng-scope']//div[@ng-show='hasError()']");
	public static By errorInitiatedBy=By.xpath("//label[.='Initiated by']/parent::div/following-sibling::div//div[@class='requiredInput ng-scope']//div[@ng-show='hasError()']");
	public static By errorInteractionChannel=By.xpath("//label[.='Interaction Channel']/parent::div/following-sibling::div//div[@class='requiredInput ng-scope']//div[@ng-show='hasError()']");
	public static By emailLookup=By.xpath("//a[@title='Email Medical Insight to Lookup']");
	public static By go=By.xpath("//input[text()='Go!']");
	
	public static By errorMessage=By.cssSelector(".pbError");
	
	public static By selectDeliveryMethod=By.xpath("//label[normalize-space(text())='Delivery Method']/parent::td/following-sibling::td//select");//By.cssSelector("span > [id*='Delivery_Method_vod']");
	public static By checksendToNewEmailMedicalInquiry=By.id("shipToNewEmail");//By.cssSelector("span > [id*='Delivery_Method_vod']");
	public static By textNewEmailMedicalInquiry=By.cssSelector("[id*='email:Medical_Inquiry_vod__c'][type='text']");//By.cssSelector("span > [id*='Delivery_Method_vod']");
	public static By selectEmailDeliveryInMedicalInquiry=By.cssSelector("#EMailSelect");//By.cssSelector("span > [id*='Delivery_Method_vod']");
	public static By searchProductName=By.cssSelector("td[id*='Product Name'] span>a[onclick*='Medical_Inquiry_vod']");
	public static By linkSelectProductName=By.cssSelector("tr.dataRow>th>a");
	public static By btnGoInMedicalInquiry=By.cssSelector("input#searchButton");
	public static By linkSearchResultsInMedicalInquiry=By.cssSelector("a[onclick*='selectAndClose']");
	public static By textareaInquiry=By.cssSelector("textarea[id*='Inquiry_Text__c']");
	public static By btnSaveInMedicalInquiry=By.cssSelector("input#pbButtonTableColSaveBottom");
	public static By btnSubmitInMedicalInquiry=By.cssSelector("input#pbButtonTableColSubmitBottom");
	public static By btnCancelMedicalInquiry=By.cssSelector("input#pbButtonTableColCancelBottom");
	public static By selectAddressInMedicalInquiry=By.cssSelector("#AddrSelect");
	
	public static By mslInteractionHeader=By.cssSelector("h2.pageDescription");
	public static By statusLabel=By.cssSelector("span[name='Status_vod__c']");
	public static String strAccountNameForCalls="";
	public static String strCallID="";
	
	
	public static By selectMedicalInquiryType=By.cssSelector(".requiredInput > select#p3");
	public static By btnContinueMedicalInquiry=By.cssSelector("td#bottomButtonRow input[value='Continue']");
	public static By addDiscussionMedicalInsight = By.xpath("//label[text()='  New Medical Insight ']/following-sibling::input[@value='Add Discussion']");
	public static By therapeuticArea=By.id("MA_Therapeutic_Area_AZ__c");
	public static By scientificInsightTopic=By.id("MA_Scientific_Insight_Topic_AZ__c");
	public static By medicalInsightDescription=By.id("MA_Medical_Insight_Description_AZ__c");
	public static By medicalInsightSummary=By.id("MA_Medical_Insight_Summary_AZ__c");
	
	
	public static By addDiscussion = By.xpath("//label[text()='  On-going Clinical Study ']/following-sibling::input[@value='Add Discussion']");
	public static By clinicalStudyErrorMsg = By.xpath("//div[@class='veeva-pl-field ng-scope']/div[text()='Please select a clinical study']");;
	
	 public static By recordType=By.id("RecordTypeId");
	
	/*/
	public static By recordCallButton=By.xpath("//td[@id='topButtonRow']//input[@name='record_a_call']");
	public static By accountrequired=By.xpath("//label[.='Account']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By addressrequired=By.xpath("//label[.='Address']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
    public static By addressdropdown=By.xpath("//div[@name='Address_vod__c']/select");
    public static By daterequired=By.xpath("//label[.='Datetime']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
    public static By dateandtime=By.xpath("//span[@name='Call_Datetime_vod__c Call_Datetime_vod__c']//a");
    public static By recordType=By.id("RecordTypeId");
//*/


}

