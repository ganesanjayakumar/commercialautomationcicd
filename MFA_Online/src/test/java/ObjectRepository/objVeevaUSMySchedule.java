
package ObjectRepository;

import org.openqa.selenium.By;


public class objVeevaUSMySchedule {	
	
	public static By subject = By.cssSelector("input#evt5");
	public static By subjectPopUpButton = By.cssSelector("a[href*=combobox]");
	public static By newCalendarEntry=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[2]/a");
	public static By subjectType=By.cssSelector("ul > li > a");
	public static By callList=By.cssSelector("span.fc-event-title-name");
	public static By eventStartDate=By.cssSelector("input#StartDateTime");
	public static By eventEndDate=By.cssSelector("#EndDateTime");
	public static By eventStartTime=By.cssSelector("#StartDateTime + .timeInput > input");
	public static By eventEndTime=By.cssSelector("#EndDateTime + .timeInput > input");
	
	//My Schedule
    public static By scheduletab=By.id("myScheduleNavigation");
    public static By dayView=By.xpath("//a[.='Day']");
    public static By weekView=By.xpath("//a[.='Week']");
    public static By monthView=By.xpath("//a[.='Month']");
    public static By cycleView=By.xpath("//a[.='Cycle']");
//    public static By workweek=By.xpath("//span[.='Show work week']/preceding-sibling::input");
    public static By workweek=By.xpath("//div[@id='myScheduleNavigation']//input[@value='work']");
  
    
    public static By fullweek=By.xpath("//span[.='Show full week']/preceding-sibling::input");
    public static By newwCall=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[1]/a");
    public static By searchacccall=By.id("accSearch");
    public static By gosearchcall=By.xpath("//input[@class='multiSave multiSaveOn btn']");
    public static By addCall=By.xpath("//input[@class='multiSave multiSaveAdd btn']");
    public static By cancelcall=By.xpath("//input[@class='multiSave multiSaveAdd btn']//following-sibling::input[@name='Cancel']");
    public static By submitcall=By.xpath("//td[@id='topButtonRow']//input[@name='Submit']");
    public static By callreportpage=By.xpath("//h2[.='Call Report']");
    public static By jpCalldetail=By.xpath("//h2[.='AZ Call Detail PRC']");
    public static By mslPage=By.xpath("//h2[.='MSL Interaction']");
    public static By custAct=By.xpath("//h2[.='Customer Activity']");
    public static By mslInteraction=By.xpath("//select[@id='MA_Channel_AZ__c']");
    public static By mslPatientSafety=By.xpath("//select[@id='MA_Patient_Safety_Attestation_AZ__c']");
    public static By schedularSection=By.xpath("//div[@id='scheduler']");
    public static By schedularaccounts=By.xpath("//div[@id='schedFiltersDiv']//div[@class='filterSelectContainer']//select");
    public static By accountselectionschedular=By.xpath("//td[.='Accounts:']/parent::tr//select");
    
    
    //My ScheduleTOT
    
    public static By reasonLookUp=By.xpath("//span[@class='lookupInput']/input");
    public static By reasonRequiredInputsales=By.xpath("//span[@class='lookupInput']/parent::div/div[@class='requiredBlock']");
    public static By daterequired=By.xpath("//span[@class='dateInput dateOnlyInput']/parent::div/div[@class='requiredBlock']");//added
    public static By lookupReason=By.xpath("//label[.='Search']//following-sibling::input[@id='lksrch']");
    public static By lookupReasonGo=By.name("go");
    public static  By defaultstatus=By.xpath("//td[.='Status']/parent::tr//td[2]//select");
    public static By dateselectionTOT=By.xpath("//span[@class='dateInput dateOnlyInput']/input");
    public static By selectTime=By.xpath("//label[.='Time']/parent::td//following-sibling::td//select");
    public static By hrsoff=By.xpath("//label[.='Hours off']/parent::td//following-sibling::td//select");
    public static By strttime=By.xpath("//label[.='Start Time']/parent::td//following-sibling::td//select");
    public static By saveTOT=By.xpath("//td[@id='topButtonRow']//input[@name='save']");
    public static By deleteTOT=By.xpath("//td[@id='topButtonRow']//input[@name='del']");
    public static By newtot=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[.='New Time Off Territory']/a");
	public static By reasonRequiredmsl=By.xpath("//label[contains(text(),'Reason')]/parent::td/parent::tr//select");
   //Mailinator
    
    public static By inputEmail=By.id("inboxfield");
    public static By searchmail=By.xpath("//span[@class='input-group-btn']/button");
    
    //Gmail
    public static String mailaddress="veeva.automation91@mailinator.com";
    public static String mailpassword="Veeva@123";
    public static String mailaddressBCC="veeva.automationBCC@mailinator.com";   
    public static By bcc= By.xpath("//table[@class='headerTable']/tbody//td//input");
    		//By.id("bccInput-a1n0Q0000002FMgQAM");
  //*[@id="bccInput-a1n0Q0000000f20QAA"]
  //table[@class='headerTable']//td[.='Bcc']/following-sibling::td//input
 
    public static By usernamegmail=By.name("identifier");
    public static By gmailPasswordNextBtn=By.xpath("//span[.='Next']"); 
    public static By gmailPassword=By.xpath("//input[@name='password']");
	
    
    
    




}
	