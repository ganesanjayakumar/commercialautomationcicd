package ObjectRepository;

import org.openqa.selenium.By;

public class ObjVeevaSurveys {
	public static By channelclm = By.xpath("//select[@id='reqi-value:string:Survey_vod__c:Channels_vod__c_unselected']//option[.='CLM']");
	public static By channelcrm = By.xpath("//select[@id='reqi-value:string:Survey_vod__c:Channels_vod__c_unselected']//option[.='CRM']");
	public static By assignmentType=By.xpath("//select[@id='reqi-value:picklist:Survey_vod__c:Assignment_Type_vod__c']");
	public static By allowuserschoosetargets=By.xpath("//input[@id='i-value:boolean:Survey_vod__c:Open_vod__c']");
	public static By medicalaffairs=By.xpath("//select[@id='reqi-value:picklist:Survey_vod__c:MA_Medical_Affairs_AZ__c']");
	public static By countrycode=By.xpath("//input[@id='reqi-value:string:Survey_vod__c:Country_Code_AZ__c']");
	public static By editsegment=By.id("S_Edit_Segment_Btn");
	public static By addsegment=By.id("S_Add_Segment_Btn");
	public static By segmentno=By.xpath("//table[@id='S_segmentsTable']//td[2]//input[contains(@class,'stextinput displaysError')]");
	public static By minscore=By.xpath("//table[@id='S_segmentsTable']//td[3]//input[contains(@class,'stextinput displaysError')]");
	public static By maxscore=By.xpath("//table[@id='S_segmentsTable']//td[4]//input[contains(@class,'stextinput displaysError')]");
	public static By savesegment=By.id("S_Save_Segment_Btn");
	public static By newsurveytarget=By.id("T_NewSurveyTarget");
	public static By staticsegmentno=By.xpath("//table[@id='S_segmentsTable']//td[2]//span");
	public static By staticminscore=By.xpath("//table[@id='S_segmentsTable']//td[3]//span");
	public static By staticmaxscore=By.xpath("//table[@id='S_segmentsTable']//td[4]//span");
	public static By searchaccount=By.xpath("//div[@id='T_addSurveyTargetDialog']//span[.='Search']/parent::span/input");
	public static By addselectedaccount=By.id("T_addSelected");
	public static By closesurveytarget=By.xpath("//span[.='Add Survey Target']/parent::div//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']");
	public static By surveytargetsdropdown=By.id("fcf");
	public static By submitanssurveytarget=By.xpath("//input[@title='Submit']");
	public static By radioSurveyOption=By.cssSelector("input[name='surveyRadio']");
	public static By frameAddSurveyTarget=By.id("vod_iframe");
	public static By txtSurveyNames=By.cssSelector("#surveyTable td>span");
	public static By btnAddSurveyTarget=By.id("AddSurveyTarget");
	public static By selectSearch=By.id("sen");
	public static By textSearch=By.cssSelector(".standardSearchElementBody > input.searchTextBox");
	public static By btnGo=By.cssSelector("input[name='search'][title='Go!']");
	
	
	
	
	
}
