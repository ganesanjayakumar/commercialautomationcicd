package ObjectRepository;

import org.openqa.selenium.By;

public class ObjVeevaCallsStandardization_Commercial
{
	public static By mySettings = By.xpath("//a[.='My Settings']");
	public static By personalsettings = By.xpath("//span[.='Personal']");
	public static By advanceduserdetails = By.id("AdvancedUserDetails_font");
	public static By preferredchannel = By.xpath("//td[.='Preferred Channel']/following-sibling::td");
	public static By recordCallButton = By.xpath("//td[@id='topButtonRow']//input[@name='record_a_call']");
	public static By newcallReportPage = By.xpath("//h2[.='New Call Report']");
	public static By pharmacypage = By.xpath("//h2[.='New Pharmacy Call']");
	public static By backoffsamplespage = By.xpath("//h2[.='New Back Office Samples']");
	public static By accountrequired = By.xpath("//label[.='Account']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By addressrequired = By.xpath("//label[.='Address']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By addressdropdown = By.xpath("//div[@name='Address_vod__c']/select");
	public static By affiliationrequired = By.xpath("//label[.='Affiliation']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By affiliationdropdown = By.xpath("//div[@name='zvod_Business_Account_vod__c']/select");
	public static By solicitedrequired = By.xpath("//label[.='Solicited']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By soliciteddropdown = By.xpath("//select[@id='Solicited_Call_AZ_EU_c__c']");
	public static By daterequired = By.xpath("//label[.='Datetime']/parent::div/following-sibling::div//div[@class='requiredBlock ng-scope']");
	public static By dateandtime = By.xpath("//span[@name='Call_Datetime_vod__c Call_Datetime_vod__c']//a");
	public static By recordType = By.id("RecordTypeId");
	public static By attendesSearch = By.xpath("//div[@name='zvod_Attendees_vod__c']//input[@type='button'][@value='Search']");
	public static By attendeesearchinput = By.id("searchInput");
	public static By attendeesearchgo = By.xpath("//input[@value='Go!']");
	public static By addattendee = By.xpath("//input[@id='searchInput']/parent::td/parent::tr/following-sibling::tr//input[@title='Add']");
	public static By attendeecheckbox = By.xpath("//input[@id='searchInput']/parent::td/parent::tr//following-sibling::tr//table//tr[1]//td[1]/input");
	public static By deliverychannel = By.id("Delivery_Channel_AZ__c");
	public static By savechanges = By.xpath("//td[@id='topButtonRow']//input[@value='Save']");
	public static By defaultsavedeliverychannel = By.xpath("//span[@name='Delivery_Channel_AZ__c']");
	public static By editrecord = By.xpath("//div[@class='pbHeader ng-scope']//input[@name='Edit']");
	public static By submitrecord = By.xpath("//td[@id='topButtonRow']//input[@value='Submit']");
	public static By sampleSendCard = By.id("Sample_Send_Card_vod__c");
	public static By deliveryinstructions = By.id("Delivery_Instructions_AZ__c");
	public static By quantity = By.xpath("//input[@ng-model='row.data.Quantity_vod__c']");
	public static By shipaddress = By.xpath("//select[@ng-model='page.ctrl.call.data.Ship_To_Address_vod__c']");
	public static By searchProduct = By.xpath("//label[.='Product']/parent::div/following-sibling::div//a");
	public static By btnGo = By.cssSelector("[name='lookup_form'] > input[name='go']");
	public static By linkInsideSearch = By.xpath("//div[contains(@class,'pbBody lookup-results')]/table/tbody//tr[2]//td[1]/a");
	public static By sellingpage = By.xpath("//h2[.='New Selling']");
	public static By nonsellingpage = By.xpath("//h2[.='New Non-Selling']");
	public static By azsubtype = By.cssSelector("[id*='Call_Type']");
	public static By iovprovided = By.id("Promo_Items_Provided_AZ_US__c");
	public static By statesamplingverification = By.id("HCP_TDDD_Validation_AZ_US__c");

}
