
package ObjectRepository;

import org.openqa.selenium.By;


public class ObjVeevaEmail {	
	
	public static By loginFormVeeva = By.xpath("//div[@id='theloginform']/form");
	public static By loginUserName = By.id("username");
	public static By loginPassword = By.id("password");	
	public static By loginSubmit = By.id("Login");
	public static By loginError = By.xpath("//div[@id='theloginform']//div[@id='error']");
	public static By userMarket = By.xpath("//div[@id='AppBodyHeader']//div[@class='msgContent']/span/span[2]");
	public static By passwordResetCancel = By.id("cancel-button");
	public static By menu = By.id("tabBar");
	public static By accountstab=By.xpath("//a[.='My Accounts']");
	public static By viewdropdown=By.id("vwid");
	public static By sframe=By.xpath("//iframe[@id='vod_iframe']");
	public static By moreactionsButton=By.name("moreActionsButton");
	public static By productSelection=By.id("filterTableBody");
	public static By mailFrame=By.xpath("//iframe[@id='vod_iframe']");
	public static By addDocuments=By.xpath("//button[.='Add Documents']");
	public static By addSelectedDoc=By.xpath("//button[@class='addSelectedDocs']");
    public static By sendButton=By.className("sendButton");
    public static By manageEmailAddress=By.className("emailSelect");
    public static By manageSecondaryEmail=By.xpath("//td[.='Secondary Email']//following-sibling::td/input");
    public static By manageEmailSave=By.id("email_mgmt_save");
    public static By addTemplatesButton=By.id("addTemplatesButton");
    public static By changeOptPreferences=By.xpath("//a[.='Change Opt Preferences']");
    public static By changeOptContinue=By.xpath("//input[@name='continueButton' and @title='Continue']");
    public static By paperconsent=By.xpath("//div/div[@class='requiredInput']//input[@name='ConsentID']");
    public static By papercosentsave=By.name("Save");
    public static By papercosentfinish=By.name("Save");
    public static By  successNote=By.id("successNote");
    public static By close=By.id("cancelButton");
    public static By previewButton=By.xpath("//button[.='Preview']");
    public static By optInConfirmMsg = By.className("recipientErrorMsg");
    
    //Logout
    public static By logOutUser=By.id("userNavLabel");
    public static By logOutLink=By.xpath("//div[@id='userNav-menuItems']//a[.='Logout']");
    
    
    //My Schedule
    public static By scheduletab=By.id("myScheduleNavigation");
    public static By dayView=By.xpath("//a[.='Day']");
    public static By weekView=By.xpath("//a[.='Week']");
    public static By monthView=By.xpath("//a[.='Month']");
    public static By monthView1=By.xpath("//span[@class='month']");
    public static By cycleView=By.xpath("//a[.='Cycle']");
//    public static By workweek=By.xpath("//span[.='Show work week']/preceding-sibling::input");
    public static By workweek=By.xpath("//div[@id='myScheduleNavigation']//input[@value='work']");
  
    
    public static By fullweek=By.xpath("//span[.='Show full week']/preceding-sibling::input");
    public static By newwCall=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[1]/a");
    public static By newwCalender=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[2]/a");
    public static By searchacccall=By.id("accSearch");
    public static By gosearchcall=By.xpath("//input[@class='multiSave multiSaveOn btn']");
    public static By addCall=By.xpath("//input[@class='multiSave multiSaveAdd btn']");
    public static By cancelcall=By.xpath("//input[@class='multiSave multiSaveAdd btn']//following-sibling::input[@name='Cancel']");
    public static By submitcall=By.xpath("//td[@id='topButtonRow']//input[@name='Submit']");
    public static By callreportpage=By.xpath("//h2[.='Call Report']");
    public static By jpCalldetail=By.xpath("//h2[.='AZ Call Detail PRC']");
    public static By mslPage=By.xpath("//h2[.='MSL Interaction']");
    public static By custAct=By.xpath("//h2[.='Customer Activity']");
    public static By dxlcall=By.xpath("//h2[.='Diagnostic Interaction']");
    public static By mainter=By.xpath("//h2[.='MA Interaction']");
    public static By cancel=By.xpath("(//input[@type='button' and @name='Cancel'])[1]");
    public static By selAct=By.xpath("//h2[.='Selling']");
    public static By mslInteraction=By.xpath("//select[@id='MA_Channel_AZ__c']");
    public static By mslPatientSafety=By.xpath("//select[@id='MA_Patient_Safety_Attestation_AZ__c']");
    public static By schedularSection=By.xpath("//div[@id='scheduler']");
    public static By schedularaccounts=By.xpath("//div[@id='schedFiltersDiv']//div[@class='filterSelectContainer']//select");
    public static By accountselectionschedular=By.xpath("//td[.='Accounts:']/parent::tr//select");
    
    
    //My ScheduleTOT
    
    public static By reasonLookUp=By.xpath("//span[@class='lookupInput']/input");
    public static By reasonRequiredInputsales=By.xpath("//span[@class='lookupInput']/parent::div/div[@class='requiredBlock']");
    public static By daterequired=By.xpath("//span[@class='dateInput dateOnlyInput']/parent::div/div[@class='requiredBlock']");//added
    public static By lookupReason=By.xpath("//label[.='Search']//following-sibling::input[@id='lksrch']");
    public static By lookupReasonGo=By.name("go");
    public static  By defaultstatus=By.xpath("//td[.='Status']/parent::tr//td[2]//select");
    public static By dateselectionTOT=By.xpath("//span[@class='dateInput dateOnlyInput']/input");
    public static By selectTime=By.xpath("//label[text()='Time']/parent::td//following-sibling::td//select");
    public static By hrsoff=By.xpath("//label[.='Hours off']/parent::td//following-sibling::td//select");
    public static By strttime=By.xpath("//label[.='Start Time']/parent::td//following-sibling::td//select");
    public static By saveTOT=By.xpath("//td[@id='topButtonRow']//input[@name='save']");
    public static By deleteTOT=By.xpath("//td[@id='topButtonRow']//input[@name='del']");
    public static By newtot=By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[.='New Time Off Territory']/a");
	public static By reasonRequiredmsl=By.xpath("//label[contains(text(),'Reason')]/parent::td/parent::tr//select");
   //Mailinator
    
    public static By inputEmail=By.id("inboxfield");
    public static By searchmail=By.xpath("//span[@class='input-group-btn']/button");
    
    //Gmail
    public static String mailaddress="veeva.automation91@mailinator.com";
    public static String mailpassword="Veeva@123";
    public static String mailaddressBCC="veeva.automationBCC@mailinator.com";   
    public static By bcc= By.xpath("//table[@class='headerTable']/tbody//td//input");
    		//By.id("bccInput-a1n0Q0000002FMgQAM");
  //*[@id="bccInput-a1n0Q0000000f20QAA"]
  //table[@class='headerTable']//td[.='Bcc']/following-sibling::td//input
 
    public static By usernamegmail=By.name("identifier");
    public static By gmailPasswordNextBtn=By.xpath("//span[.='Next']"); 
    public static By gmailPassword=By.xpath("//input[@name='password']");
	
    
    
    




}
	