package businesscomponents;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objGlobalAccountSearchPage;
import ObjectRepository.objHeaderPage;
import ObjectRepository.objMyAccounts;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_CallsStandardization extends VeevaFunctions
{
	public Veeva_CallsStandardization(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	MyAccountsPage ma = new MyAccountsPage(scriptHelper);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
	VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	MyAccountsPage ap = new MyAccountsPage(scriptHelper);
	/**
	 * Default Delivery channel of user will be stored in variable from My
	 * Settings Page and navigate to My Accounts Page.
	 * 
	 * @throws Exception
	 */
	public String datecalls;
	public String datetimecalls;

	public void searchAndNavigateToAccount() throws Exception
	{
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		/*	if (strProfile.trim().equalsIgnoreCase("Commercial"))
				if (!strLocale.trim().equalsIgnoreCase("JP") && !strLocale.trim().equalsIgnoreCase("CN") && !strLocale.trim().equalsIgnoreCase("US"))
				{
					getdefaultDeliveryChannel();
					ap.navigateToMyAccounts();
				}*/
		selectaccount();
	}

	/**
	 * Default Delivery channel of user will be stored in variable from My
	 * Settings Page
	 * 
	 * @throws Exception
	 */
	public void getdefaultDeliveryChannel() throws Exception
	{
		try
		{
			String defaultchannel;
			String datasheet = "Calls_Standardization";
			cf.clickLink(ObjVeevaEmail.logOutUser, "UserName");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.mySettings, "My Settings");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.personalsettings, "Personal Settings");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.advanceduserdetails, "Advanced User Details");
			cf.scrollToElement(ObjVeevaCallsStandardization_Commercial.preferredchannel, "Preferred Channel");
			defaultchannel = driver.findElement(ObjVeevaCallsStandardization_Commercial.preferredchannel).getText();
			dataTable.putData(datasheet, "DefaultChannel", defaultchannel);
			report.updateTestLog("Verify able to see the preferred Channel", "Able to see preferred channel", Status.SCREENSHOT);
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void clickrecordcall_Commercial() throws Exception
	{

		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.recordCallButton));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call"))
			{
				report.updateTestLog("Verify able to see Record Call Button", "Able to see Record Call Button", Status.PASS);
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call");

			}
			else
			{
				report.updateTestLog("Verify able to see Record Call Button", "Unable to see Record Call Button", Status.FAIL);

			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate account name block is required and account name is present or
	 * not.
	 * 
	 * @throws Exception
	 */
	public void accountblock() throws Exception
	{
		try
		{
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.accountrequired, "Account Required"))
			{
				report.updateTestLog("Verify account is required block or not", "Able to see account as requred block", Status.PASS);
				String accounttext = driver.findElement(By.xpath("//label[.='Account']/parent::div/following-sibling::div//a")).getText().trim();
				if (!(accounttext == null))
				{
					report.updateTestLog("Verify account name is present", "Able to see account name", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify account name is present", "Able to see account name", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify account is required block or not", "Unable to see account as required block", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate whether address field is required block and address value is
	 * present. If address value is None select any other address option from
	 * dropdown.
	 * 
	 * @throws Exception
	 */
	public void addressBlock() throws Exception
	{
		try
		{
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if (cf.isElementVisible(By.xpath("//label[.='Address']/parent::div/following-sibling::div//div[@name='Address_vod__c']"), "Address Required"))
			{
				report.updateTestLog("Verify address is required block or not", "Able to see address as requred block", Status.PASS);
				Select se = new Select(driver.findElement(ObjVeevaCallsStandardization_Commercial.addressdropdown));
				String addressoption = se.getFirstSelectedOption().getText().trim();
				int optionscount = se.getOptions().size();
				if (!(addressoption.equals("--None--")))
				{
					report.updateTestLog("Verify address value is selected as option", "Able to see address value as selected as option", Status.PASS);
				}
				else if (optionscount > 1)
				{
					String option2 = driver.findElement(By.xpath("//div[@name='Address_vod__c']/select/option[2]")).getText().trim();
					cf.selectData(ObjVeevaCallsStandardization_Commercial.addressdropdown, "Address Dropdown", option2);
					report.updateTestLog("Verify address is selected", "Able to select address value", Status.PASS);
					// frameworkparameters.setStopExecution(true);
				}
				else
				{
					report.updateTestLog("Verify address is available", "Address value is not available", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify address is required block or not", "Unable to see address as required block", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate affiliation field is required field and and address value is
	 * present. If affiliation value is None select any other address option
	 * from dropdown
	 * 
	 * @throws Exception
	 */
	public void affiliationblock() throws Exception
	{
		// try {
		// if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.affiliationrequired,"Affiliation
		// Required")){
		// report.updateTestLog("Verify Affiliation is required block or not",
		// "Able to see Affiliation as requred block", Status.PASS);
		//// Select se=new
		// Select(driver.findElement(ObjVeevaCallsStandardization_Commercial.affiliationdropdown));
		//// String
		// affiliationoption=se.getFirstSelectedOption().getText().trim();
		//// int optionscount=se.getOptions().size();
		////
		//// if(!(affiliationoption.equals("--None--"))){
		//// report.updateTestLog("Verify Affiliation address value is selected
		// as option", "Able to see Affiliation address value as selected as
		// option", Status.PASS);
		//// }
		//// else if(optionscount>1){
		//// String
		// option2=driver.findElement(By.xpath("//div[@name='zvod_Business_Account_vod__c']/select/option[2]")).getText().trim();
		//// cf.selectData(ObjVeevaCallsStandardization_Commercial.affiliationdropdown,
		// "Affiliation Dropdown", option2);
		//// report.updateTestLog("Verify Affiliation is selected", "Able to
		// select Affiliation value", Status.PASS);
		//// //frameworkparameters.setStopExecution(true);
		//// }
		//// else{
		//// report.updateTestLog("Verify Affiliation value is available",
		// "Affiliation value is not available", Status.FAIL);
		//// //frameworkparameters.setStopExecution(true);
		//// }
		//
		// }
		// else{
		// report.updateTestLog("Verify Affiliation is required block or not",
		// "Unable to see Affiliation as required block", Status.FAIL);
		// //frameworkparameters.setStopExecution(true);
		// }
		// } catch (Exception e) {
		// throw new Exception(e.getMessage());
		// }
	}

	/**
	 * Validate solicitedBlock field is required field and and address value is
	 * present. If solicited value is None select any other address option from
	 * dropdown
	 * 
	 * @throws Exception
	 */
	public void solicitedBlock() throws Exception
	{
		try
		{
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.solicitedrequired, "Solicited Required"))
			{
				report.updateTestLog("Verify Affiliation is required block or not", "Able to see Solicited as requred block", Status.PASS);
				Select se = new Select(driver.findElement(ObjVeevaCallsStandardization_Commercial.soliciteddropdown));
				String affiliationoption = se.getFirstSelectedOption().getText().trim();
				int optionscount = se.getOptions().size();
				if (!(affiliationoption.equals("--None--")))
				{
					report.updateTestLog("Verify Solicited value is selected as option", "Able to see Solicited value as selected as option", Status.PASS);
				}
				else if (optionscount > 1)
				{
					String option2 = driver.findElement(By.xpath("//select[@id='Solicited_Call_AZ_EU_c__c']//option[2]")).getText().trim();
					cf.selectData(ObjVeevaCallsStandardization_Commercial.soliciteddropdown, "Solicited Dropdown", option2);
					report.updateTestLog("Verify Solicited is selected", "Able to select Solicited value", Status.PASS);
					// frameworkparameters.setStopExecution(true);
				}
				else
				{
					report.updateTestLog("Verify Solicited value is available", "Solicited value is not available", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify Solicited is required block or not", "Unable to see Solicited as required block", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate date and time format and date is required block or not.
	 * 
	 * @throws Exception
	 */
	// */
	public void daterequiredblock() throws Exception
	{
		if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.daterequired, "Date and Time Required"))
			report.updateTestLog("Verify Datetime is required block or not", "Able to see Datetime as required block", Status.PASS);
		else
		{
			report.updateTestLog("Verify Datetime is required block or not", "Unable to see Datetime as required block", Status.FAIL);
		}
	}

	public String dateValidationBlock()
	{
		String datetime = driver.findElement(By.xpath("//span[@name='Call_Datetime_vod__c Call_Datetime_vod__c']/span/a")).getText();
		dataTable.putData("Login", "DateTime", datetime);
		return datetime;
	}
	// */
	/**
	 * DATE format
	 * 
	 * @return
	 */

	/*
	 * / public String dateformatcalls_Commercial(){ String
	 * strDataSheet="Login"; String locale=dataTable.getData(strDataSheet,
	 * "Locale"); String profile=dataTable.getData(strDataSheet, "Profile");
	 * 
	 * if(profile.equals("Commercial")) {
	 * if(locale.equals("EU2")||locale.equals("RU")){ datecalls=new
	 * SimpleDateFormat("dd.MM.YYYY").format(dNow).toString(); } else
	 * if(locale.equals("EU1")||locale.equals("BR")||locale.equals("CA")){
	 * datecalls=new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString(); }
	 * else if(locale.equals("JP")){ datecalls=new
	 * SimpleDateFormat("YYYY/MM/dd").format(dNow).toString(); } else
	 * if(locale.equals("IC")){ datecalls=new
	 * SimpleDateFormat("MM/dd/YYYY").format(dNow).toString(); } else
	 * if(locale.equals("IE")){ datecalls=new
	 * SimpleDateFormat("d/M/YYYY").format(dNow).toString();
	 * 
	 * } else if(locale.equals("ANZ")){ datecalls=new
	 * SimpleDateFormat("d/MM/YYYY").format(dNow).toString(); }
	 * 
	 * else if(locale.equals("CN")){ datecalls=new
	 * SimpleDateFormat("YYYY/M/d").format(dNow).toString(); }
	 * 
	 * } return datecalls;
	 * 
	 * } //
	 */
	/**
	 * Selecting Report Type.
	 * 
	 * @throws Exception
	 */
	public void reportSelection() throws Exception
	{
		String strDataSheet = "Login";
		String recordType = dataTable.getData(strDataSheet, "Record Type");
		String strDataSheet1 = "Login";
		String locale = dataTable.getData(strDataSheet1, "Locale");
		cf.waitForSeconds(20);

		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		try
		{
			if (!recordType.equals("Pharmacy"))
			{
				if (locale.equals("RU") && recordType.equals("Call Report"))
				{
					WebElement sRecord = driver.findElement(By.xpath("//span[@name='RecordTypeId' and text()='Call Report']"));
					String sRec = sRecord.getText().toString().trim();
					if (sRec.equalsIgnoreCase(recordType))
					{
						report.updateTestLog("Verify able to see the static Record call type", "Able to see the static Record call type", Status.PASS);
					}
				}
				else
				{
					cf.selectData(ObjVeevaCallsStandardization_Commercial.recordType, "Record Type", recordType);
				}
				cf.waitForSeconds(6);
			}
			if ((locale.equals("ANZ") && recordType.equals("Call Report")) || recordType.equals("Non-Selling") || recordType.equals("Selling"))
			{
				cf.selectARandomOptionFromComboBox(ObjVeevaCallsStandardization_Commercial.azsubtype, "Select Random Value.");
			}
			if (locale.equals("ANZ") && recordType.equals("Back Office Samples"))
			{
				cf.setData(By.id("ANZ_Order_Number_AZ__c"), "Order Number", "12345");
			}
			if ((locale.equals("BR") || locale.equals("RU")) && recordType.equals("Call Report (Biz Opt)"))
			{
				cf.clickButton(By.xpath("//a[@title='Call Sub-Type Lookup']//img"), "Search Call Sub Type");
				cf.waitForSeconds(15);
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.btnGo, "Go");
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.linkInsideSearch, "Search result");
			}
			if (recordType.equals("Pharmacy"))
			{
				String type = driver.findElement(By.xpath("//span[@name='RecordTypeId']")).getText();
				if (type.equals("Pharmacy Call"))
				{
					report.updateTestLog("Verify able to see the Pharmacy Call as static", "Able to see the Pharmacy Call as static", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify able to see the Pharmacy Call as static", "Unable to see the Pharmacy Call as static", Status.PASS);
				}
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Adding Attendees to the Call.
	 * 
	 * @throws Exception
	 */
	public void addattendes() throws Exception
	{
		String strDataSheet = "Calls_Standardization";
		String attendee;
		attendee = dataTable.getData(strDataSheet, "Attendee");
		addattendes(attendee);
	}

	/**
	 * Adding Attendees to the Call based on the Market wise and alignments of
	 * other Markets.
	 * 
	 * @throws Exception
	 */
	public void addattendes(String attendee) throws Exception
	{
		String strDataSheet1 = "Login";
		String locale = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		try
		{
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendesSearch, "Search");
			cf.setData(ObjVeevaCallsStandardization_Commercial.attendeesearchinput, "Attendee Name", attendee);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendeesearchgo, "Go");
			cf.waitForSeconds(10);
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.attendeecheckbox, "Select Attendee");
			report.updateTestLog("Verify able to see Attendee is selected", "Able to see attendee is selected", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.addattendee, "Add Attendee");
			cf.waitForSeconds(20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
			if (!locale.equals("RU") && !locale.equals("JP"))
			{
				cf.waitForSeconds(15);
				if (cf.isElementVisible(By.xpath("//b[.='" + attendee + "']"), "Selected Attendee"))
				{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}
				else if (locale.equals("ANZ") && profile.equals("Medical"))
				{
					String[] att = attendee.split(" ");
					String x = att[1].concat(", ");
					String attName = x.concat(att[0]);
					if (cf.isElementVisible(By.xpath("//b[.='" + attName + "']"), "Selected Attendee"))
					{
						report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
					}
				}
				else
				{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see  selected attendee after adding", Status.FAIL);
					// frameworkParameters.setStopExecution(true);
				}
			}
			else
			{
				if (attendee.contains(" "))
					attendee = attendee.split(" ")[1];
				if (cf.isElementVisible(By.xpath("//b[contains(text(),'" + attendee + "')]"), "Selected Attendee"))
				{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					cf.waitForSeconds(10);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}
				else
				{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see  selected attendee after adding", Status.FAIL);
					// frameworkParameters.setStopExecution(true);
				}
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validating Default Delivery Channel after saved Record Call Page.
	 * 
	 * @throws Exception
	 */
	public void validatedefaultchannel() throws Exception
	{
		try
		{
			String strdatasheet = "Calls_Standardization";
			String recordtype = dataTable.getData("Login", "Record Type");
			String defaultvalidatechannel;
			if (recordtype.equals("Veeva Remote Detailing"))
			{
				defaultvalidatechannel = "Remote Detailing";
			}
			else
			{
				defaultvalidatechannel = dataTable.getData(strdatasheet, "DefaultChannel");
			}
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.editrecord, "Edit");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.deliverychannel));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel"))
			{
				cf.selectData(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel", "F2F");
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.savechanges, "Save");
				cf.waitForSeconds(10);
				wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.defaultsavedeliverychannel));
				if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.defaultsavedeliverychannel, "Delivery Channel Saved"))
				{
					String defaultdeliverchannel = driver.findElement(ObjVeevaCallsStandardization_Commercial.defaultsavedeliverychannel).getText().trim();
					if (defaultdeliverchannel.equals(defaultvalidatechannel))
						report.updateTestLog("Verify able to see Default Delivery Channel Correctly", "Able to see Default Delivery Channel Correctly", Status.PASS);
					else
					{
						report.updateTestLog("Verify able to see Default Delivery Channel Correctly", "Unable to see Default Delivery Channel Correctly", Status.FAIL);
					}
				}
				else
				{
					report.updateTestLog("Verify able to see  saved Default Delivery Channel", "Unable to see  saved Default Delivery Channel", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Dlivery channel Dropdown", "Unable to see Delivery Channel Dropdown", Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void validatedefaultchannelVRD() throws Exception
	{
		try
		{
			String strdatasheet = "Calls_Standardization";
			String recordtype = dataTable.getData("Login", "Record Type");
			String defaultvalidatechannel;
			if (recordtype.equals("Veeva Remote Detailing"))
			{
				defaultvalidatechannel = "Remote Detailing";
			}
			else
			{
				defaultvalidatechannel = dataTable.getData(strdatasheet, "DefaultChannel");
			}
			//cf.clickButton(ObjVeevaCallsStandardization_Commercial.editrecord, "Edit");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.deliverychannel));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel"))
			{
				cf.selectData(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel", defaultvalidatechannel);
				//cf.clickButton(ObjVeevaCallsStandardization_Commercial.savechanges, "Save");
				cf.waitForSeconds(10);
				//wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.defaultsavedeliverychannel));

				String defaultdeliverchannel = driver.findElement(By.xpath("//select[@name='Delivery_Channel_AZ__c']/option[2]")).getText().trim();
				if (defaultdeliverchannel.equals(defaultvalidatechannel))
					report.updateTestLog("Verify able to see Default Delivery Channel Correctly", "Able to see Default Delivery Channel Correctly", Status.PASS);
				else
				{
					report.updateTestLog("Verify able to see Default Delivery Channel Correctly", "Unable to see Default Delivery Channel Correctly", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Dlivery channel Dropdown", "Unable to see Delivery Channel Dropdown", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Change Delivery Channel and Validate in Saved Record Call Page.
	 * 
	 * @throws Exception
	 */
	public void editRecordchangedelivery() throws Exception
	{
		String strdatasheet = "Login";
		String recordtype = dataTable.getData(strdatasheet, "Record Type");
		String validatechannel;
		try
		{
			if (!recordtype.equals("Veeva Remote Detailing"))
			{
				cf.selectData(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel", "Email");
				validatechannel = "Email";
				cf.waitForSeconds(10);
			}
			else
			{
				cf.selectData(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel", "Remote Detailing");
				validatechannel = "Remote Detailing";
			}
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.soliciteddropdown, "Solicited Block"))
			{
				cf.selectData(ObjVeevaCallsStandardization_Commercial.soliciteddropdown, "Solicited value", "Yes");
			}
			/*
			 * if(cf.isElementVisible(By.
			 * xpath("//a[@title='Call Sub-Type Lookup']"), "Call sub type")){
			 * cf.clickButton(By.xpath("//a[@title='Call Sub-Type Lookup']//img"
			 * ), "Search Call Sub Type"); cf.waitForSeconds(5);
			 * cf.clickButton(ObjVeevaCallsStandardization_Commercial.btnGo,
			 * "Go"); cf.clickButton(ObjVeevaCallsStandardization_Commercial.
			 * linkInsideSearch, "Search result"); }
			 */
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.savechanges, "Save");
			// driver.findElement(ObjVeevaCallsStandardization_Commercial.savechanges).click();
			cf.waitForSeconds(20);
			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bPageTitle']//a[.='My
			// Schedule']")));
			if (cf.isElementVisible(By.xpath("//div[@class='bPageTitle']//a[.='My Schedule']"), "Saved Page"))
			{
				String changedeliverchannel = driver.findElement(ObjVeevaCallsStandardization_Commercial.defaultsavedeliverychannel).getText().trim();
				if (changedeliverchannel.equals(validatechannel))
					report.updateTestLog("Verify able to see Changed Delivery Channel Correctly", "Able to see Changed Delivery Channel Correctly", Status.PASS);
				else
				{
					report.updateTestLog("Verify able to see Changed Delivery Channel Correctly", "Unable to see Changed Delivery Channel Correctly", Status.FAIL);
					// frameworkParameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Edit Button", "Unable to see Edit Button After Saving", Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Click Edit Button and Submit the Call without adding any product and
	 * validate the error
	 * 
	 * @throws Exception
	 */
	public void submitproducterror() throws Exception
	{
		try
		{
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.editrecord, "Edit");
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.submitrecord));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.submitrecord, "Submit"))
			{
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.submitrecord, "Submit");
				cf.waitForSeconds(10);
				// Added by Murali on 31/03/2019
				cf.scrollToBottom();
				if (cf.isElementVisible(By.xpath("//div/a/b[@class='ng-binding']"), "Detailed product link"))
				{
					cf.clickLink(By.xpath("//div/a/b[@class='ng-binding']"), "Detailed product link");
					cf.waitForSeconds(10);
				}
				/*
				 * String
				 * producterror="A detailed product must be selected in order to submit the call."
				 * ; String appproducterror=driver.findElement(By.
				 * xpath("//div[@class='pbBody']//div[@class='error-span ng-binding ng-scope']"
				 * )).getText().trim();
				 * if(appproducterror.equals(producterror)){ report.
				 * updateTestLog("Verify able to see the error Message when product is not added"
				 * , "Able to see the error Message when product is not added",
				 * Status.PASS); } else{ report.
				 * updateTestLog("Verify able to see the error Message when product is not added"
				 * ,
				 * "Unable to see the error Message when product is not added",
				 * Status.FAIL); // frameworkParameters.setStopExe cution(true);
				 * } } else{
				 * report.updateTestLog("Verify able to see the Submit Button",
				 * "Unable to see the submit Button", Status.FAIL); //
				 * frameworkParameters.setStopExecution(true);
				 */ }
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Submit the Call without adding any attendee and validate the error
	 * 
	 * @throws Exception
	 */
	public void submitattendeeerror() throws Exception
	{
		try
		{
			String appproducterror;
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.submitrecord));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.submitrecord, "Submit"))
			{
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.submitrecord, "Submit");
				String producterror = "Call Attendee is a required field";
				appproducterror = driver.findElement(By.xpath("//div[@class='pbBody']//div[@class='error-span ng-scope']/div")).getText().trim();
				if (appproducterror.equals(producterror))
				{
					report.updateTestLog("Verify able to see the error Message when attendee is not added", "Able to see the error Message when attendee is not added", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify able to see the error Message when attendee is not added", "Unable to see the error Message when attendee is not added", Status.FAIL);
					// frameworkParameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see the Submit Button", "Unable to see the submit Button", Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void addproducts() throws Exception
	{
		String datasheet = "Login";
		String locale = dataTable.getData(datasheet, "Locale");
		try
		{
			int size = driver.findElements(By.xpath("//table[@id='vod_detailing']//td[2]")).size();
			cf.scrollElementToMiddlePage(By.xpath("//table[@id='vod_detailing']//td[2]"), "Product");
			if (size > 0)
			{
				report.updateTestLog("Verify able to see products", "Able to see products in Detail Reporting Module", Status.PASS);
				try
				{
					WebElement ele;
					if (locale.equals("IC") && locale.equals("IE"))
						ele = driver.findElement(By.xpath("//table[@id='vod_detailing']/tbody[2]/tr[1]/td[2]/input"));
					else
						ele = driver.findElement(By.xpath("//table[@id='vod_detailing']/tbody[1]/tr[1]/td[2]/input"));
					if (!ele.isSelected())
					{
						if (locale.equals("IC") && locale.equals("IE"))
							cf.clickLink(By.xpath("//table[@id='vod_detailing']/tbody[2]/tr[1]/td[2]/input"), "Product");
						else
							cf.clickLink(By.xpath("//table[@id='vod_detailing']/tbody[1]/tr[1]/td[2]/input"), "Product");

						report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
					}
				}
				catch (Exception e)
				{
					if (!(locale.equals("RU") || locale.equals("EU1")))
					{
						WebElement ele;

						if (locale.equals("IC") && locale.equals("IE"))
							ele = driver.findElement(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"));
						else
							ele = driver.findElement(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"));
						if (!ele.isSelected())
						{
							if (locale.equals("IC") && locale.equals("IE"))
								cf.clickLink(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"), "Product");
							else
								cf.clickLink(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"), "Product");

							report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
						}
					}
					else
					{
						WebElement ele = driver.findElement(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"));
						if (!ele.isSelected())
						{
							cf.clickLink(By.xpath("(//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input)[1]"), "Product");
							report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
						}
					}
				}
			}
			else
			{
				report.updateTestLog("Verify able to see products", "Not able to see products in Detail Reporting Module", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Adding Samples of Products to the call.
	 * 
	 * @throws Exception
	 */
	public void addSamples() throws Exception
	{
		String datasheet = "Login";
		String locale = dataTable.getData(datasheet, "Locale");
		String shipaddress = null;
		try
		{
			if (locale.equals("IW"))
			{
				cf.setData(By.xpath("//input[@name='Sample_Card_vod__c']"), "Sample Card", "1");
			}
			else if (locale.equals("IE"))
			{
				cf.setData(ObjVeevaCallsStandardization_Commercial.sampleSendCard, "Sample Send Card", "1");
			}
			else if (!(locale.equals("ANZ") || locale.equals("RU") || locale.equals("US") || locale.equals("IC")))
			{
				cf.setData(ObjVeevaCallsStandardization_Commercial.sampleSendCard, "Sample Send Card", "1");
			}
			// RU , ANZ have only these fields in the samples section
			if (!locale.equals("US"))
				cf.setData(ObjVeevaCallsStandardization_Commercial.deliveryinstructions, "Delivery Instructions", "Delivery Instructions");
			if (!(locale.equals("ANZ") || locale.equals("RU") || locale.equals("US")))
			{

				if (locale.equals("IC") || locale.equals("IW"))
				{
					cf.clickElement(By.xpath("//table[@name='call2_sample_vod__c']//table//tbody[1]//tr[1]/td/div/img"), "Sample - Arrow");
					cf.clickLink(By.xpath("//table[@name='call2_sample_vod__c']//table//tbody[1]//tr[2]/td[2]/input"), "Sample Check Box");
				}
				else
				{
					cf.clickLink(By.xpath("//h3[.='BRC']/preceding-sibling::img"), "BRC");
					cf.clickLink(By.xpath("//table[@name='call2_sample_vod__c']//table//tbody[1]//tr[2]/td[2]/input"), "Sample Check Box");
					shipaddress = driver.findElement(By.xpath("//select[@ng-model='page.ctrl.call.data.Ship_To_Address_vod__c']/option[2]")).getText();
					cf.selectData(ObjVeevaCallsStandardization_Commercial.shipaddress, "Ship Address", shipaddress);
				}
			}
			if (!(locale.equals("ANZ") || locale.equals("RU")))
			{
				if (locale.equals("IE"))
				{
					cf.setData(By.xpath("//input[@ng-model='row.data.Quantity_vod__c']"), "Quantity", "1");
				}

				else if (locale.equals("IW"))
				{
					cf.setData(By.xpath("//input[@ng-model='row.data.Quantity_vod__c']"), "Quantity", "1");

					String lots = driver.findElement(By.xpath("//select[@ng-model='row.data.Lot_vod__c']/option[2]")).getText().trim();
					cf.selectData(By.xpath("//select[@ng-model='row.data.Lot_vod__c']"), "Quantity", lots);
				}
				else if (locale.equals("IC"))
				{
					String quantity = driver.findElement(By.xpath("//select[@ng-model='row.data.Quantity_vod__c']/option[2]")).getText().trim();
					cf.selectData(By.xpath("//select[@ng-model='row.data.Quantity_vod__c']"), "Quantity", quantity);

					String lots = driver.findElement(By.xpath("//select[@ng-model='row.data.Lot_vod__c']/option[2]")).getText().trim();
					cf.selectData(By.xpath("//select[@ng-model='row.data.Lot_vod__c']"), "Quantity", lots);
				}
				else
				{
					String quantity = driver.findElement(By.xpath("//select[@ng-model='row.data.Quantity_vod__c']/option[2]")).getText();
					cf.selectData(By.xpath("//select[@ng-model='row.data.Quantity_vod__c']"), "Quantity", quantity);
				}
			}
			report.updateTestLog("Verify able to see Sample Details", "Able to see Sample Details", Status.SCREENSHOT);
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Check Survey Target Section is present or not.
	 * 
	 * @throws Exception
	 */
	public void checksurveytargetsection() throws Exception
	{
		try
		{
			if (cf.isElementVisible(By.xpath("//h3[contains(text(),'Survey Target')]"), "Survey Target Section"))
				report.updateTestLog("Verify able to see Survey Target Section", "Able to see Survey Target Section", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to see Survey Target Section", "Unable to see Survey Target Section", Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Edit Record by clicking edit button in Saved Record Call Page.
	 * 
	 * @throws Exception
	 */
	public void editrecord() throws Exception
	{
		try
		{
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.editrecord));
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.editrecord, "Edit");
			cf.waitForSeconds(10);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Save the Record by clicking save button.
	 * 
	 * @throws Exception
	 */
	public void saverecord() throws Exception
	{
		try
		{
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.savechanges, "Save");
			cf.waitForSeconds(15);
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Adding attendee for HCA account for Call Report Record Type.
	 * 
	 * @throws Exception
	 */
	public void clickaccountHCA() throws Exception
	{
		String datasheet = "Calls_Standardization";
		String attendee = dataTable.getData(datasheet, "Attendee");
		clickaccountHCA(attendee);
	}

	public void clickaccountHCA(String strAttendee) throws Exception
	{
		try
		{
			cf.clickLink(By.xpath("//b[.='" + strAttendee + "']"), "Account");
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Submit the Call and store the Call Report Id in Login Sheet.
	 */
	public void submitrecordcall()
	{
		
		String callReportId = submitrecordcallInsideCallReports();
		dataTable.putData("Login", "CallReportId", callReportId);
	}

	/**
	 * Submit the record a call and validate whether navigating to proper page
	 * after submitting the call.
	 */
	public String submitrecordcallInsideCallReports()
	{
		String callReportId = "";
		try
		{
			cf.waitForSeconds(5);
			if (cf.isElementVisible(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit"))
			{
				cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
				alertvalidate();

				if (cf.isElementClickable(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit"))
				{
					cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
					alertvalidate();
				}

				if (cf.isElementClickable(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit"))
				{
					cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
					alertvalidate();
				}

				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='bPageTitle']//a[.='My Schedule']")));
				if (cf.isElementVisible(By.xpath("//div[@class='bPageTitle']//a[.='My Schedule']"), "Call Report Page"))
				{
					callReportId = driver.findElement(By.xpath("//h2[@class='pageDescription ng-binding']")).getText();
					System.out.println(callReportId);
					report.updateTestLog("Verify able to see Call Report Page after Sumbitting call", "Able to see Call Report Page as Call is Recorded Sucessfully", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify able to see Call Report Page after Sumbitting call", "Unable to see Call Report Page as Call is not Recorded Sucessfully", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Submit Button", "Unable to see Submit Button", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return callReportId;
	}

	/**
	 * Validate alert is present or not if yes accept the alert else skip
	 */
	public void alertvalidate()
	{
		try
		{
			driver.switchTo().alert().accept();
			System.out.println("Alert is Present");
		}
		catch (Exception e)
		{
			System.out.println("No Alert is Present");
		}
	}

	/**
	 * Validating the Recorded Call ID with proper date in Related List of
	 * Account.
	 * 
	 * @throws Exception
	 */
	public void callReportValidation() throws Exception
	{
		
		
		String CallReportidapp;
		int rows;
		Boolean callid = null;
		String datesent = "";
		String strLocale = dataTable.getData("Login", "Locale");
		String callReportId = "";
		callReportId = dataTable.getData("Login", "CallReportId");
		String dateTime = dataTable.getData("Login", "DateTime");
		try
		{
			if (strLocale.trim().equalsIgnoreCase("CN"))
				cf.clickSFLinkLet("Calls (Account)");
			else
				cf.clickSFLinkLet("Calls");
			if (cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Calls')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List"))
			{
				WebElement element1 = driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Calls')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
				cf.clickElement(element1, "Go to list");
				cf.waitForSeconds(6);
			}
			String callreportstatus, callReportColumn = "";
			if (cf.isElementVisible(By.xpath("//h3[.='Calls']"), null) || cf.isElementVisible(By.xpath("//h3[.='Calls (Account)']"), null))
			{
				if (strLocale.trim().equalsIgnoreCase("CN"))
				{
					callReportColumn = "Calls (Account)";
					rows = cf.getRowCountFromLinkLetTable("Calls (Account)");
				}
				else
				{
					callReportColumn = "Calls";
					rows = cf.getRowCountFromLinkLetTable("Calls");
				}
				for (int j = 2; j <= rows + 1; j++)
				{
					CallReportidapp = cf.getDataFromLinkLetTable(callReportColumn, "Call Name", j);
					System.out.println("Call Name: " + CallReportidapp);
					callreportstatus = cf.getDataFromLinkLetTable(callReportColumn, "Status", j);
					System.out.println("Status: " + callreportstatus);
					if (strLocale.trim().equalsIgnoreCase("CN"))
						datesent = cf.getDataFromLinkLetTable("Calls", "Date", j);
					else
						datesent = cf.getDataFromLinkLetTable("Calls", "Datetime", j);
					System.out.println("Date Time as: " + datesent);
					if ((dateTime.trim()).equalsIgnoreCase(datesent.trim()))
					{
						if (callreportstatus.equalsIgnoreCase("Submitted"))
						{
							callid = true;
							break;
						}
					}
				}
			}
			else
			{
				if (strLocale.trim().equalsIgnoreCase("CN"))
					rows = cf.rowscountexpanded("Calls");
				else
					rows = cf.rowscountexpanded("Calls");
				for (int j = 2; j <= rows + 1; j++)
				{
					CallReportidapp = cf.getDataFromLinkLetTableexpanded("Calls", "Call Name", j);
					System.out.println("Call Name: " + CallReportidapp);
					callreportstatus = cf.getDataFromLinkLetTableexpanded("Calls", "Status", j);
					System.out.println("Status: " + callreportstatus);
					if (strLocale.trim().equalsIgnoreCase("CN"))
						datesent = cf.getDataFromLinkLetTableexpanded("Calls", "Date", j);
					else
						datesent = cf.getDataFromLinkLetTableexpanded("Calls", "Datetime", j);
					System.out.println("Date Time as: " + datesent);
					if ((dateTime.trim()).equalsIgnoreCase(datesent.trim()))
					{
						if (callreportstatus.equalsIgnoreCase("Submitted"))
						{
							callid = true;
							break;
						}
					}
				}
			}
			if (callid == true)
				report.updateTestLog("Verify able to see CallReport ID " + callReportId + " of todays date with Status as Submitted", "Able to see CallReport ID " + callReportId + " of todays date with Status as Submitted", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to see CallReport ID  " + callReportId + "of todays date with Status as Submitted", "Unable to see CallReport ID " + callReportId + " of todays date with Status as Submitted", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		
	}

	public void reportValidation() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");

		if (strLocale.trim().equalsIgnoreCase("CN"))
			cf.clickSFLinkLet("Calls (Account)");
		else
			cf.clickSFLinkLet("Calls");
		if (cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Calls')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List"))
		{
			WebElement element1 = driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Calls')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Go to list");
			cf.waitForSeconds(6);
		}

		cf.clickElement(By.xpath("(//tr[contains(@class,'dataRow')]/th/a)[1]"), "First Call");

		cf.implicitWait(10, TimeUnit.SECONDS);
		cf.waitForSeconds(5);

		if (cf.isElementVisible(By.xpath("//img[@title='Veeva Remote Detailing']"), "Veeva Remote Detailing"))
		{
			cf.clickElement(By.xpath("//input[@value='Start']"), "Start");
		}

		cf.waitForSeconds(5);
		String currenturl = null;
		String parentwindow = driver.getWindowHandle();
		Set<String> childwindows = driver.getWindowHandles();
		for (String window : childwindows)
		{
			if (!(parentwindow.equals(window)))
			{
				driver.switchTo().window(window);
				currenturl = driver.getCurrentUrl();
				System.out.println(currenturl);
				System.out.println("Window switched successfully");
				cf.waitForSeconds(5);
			}
		}

		//driver.get("https://engage.veeva.com/windowshost/VeevaCRMEngage_Launcher.exe");
		cf.waitForSeconds(5);
		driver.get("chrome://downloads/");

		Screen s = new Screen();
		Pattern p1 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\launcher.PNG");
		System.out.println(p1);
		Screen s1 = new Screen();
		Pattern p2 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\run.PNG");

		cf.waitForSeconds(3);

		s.click(p1);

		cf.waitForSeconds(3);

		s1.click(p2);

		cf.waitForSeconds(30);
		
		/*driver.get(currenturl);

		cf.waitForSeconds(5);
		driver.get("chrome://downloads/");
		
		
		cf.waitForSeconds(3);


		s.click(p1);

		cf.waitForSeconds(3);

		s1.click(p2);


		cf.waitForSeconds(30);*/
		
		
	}

	public void canadaAccounts() throws Exception
	{
		try
		{
			cf.clickButton(By.xpath("//a[@title='Call Sub-Type Lookup']//img"), "Search Call Sub Type");
			cf.waitForSeconds(5);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.btnGo, "Go");
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.linkInsideSearch, "Search result");
		}
		catch (Exception e)
		{
		}
	}

	public void scheduleRemoteMeeting() throws Exception
	{
		try
		{
			cf.scrollToTop();
			
			cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
			alertvalidate();
				
			cf.waitForSeconds(3);
			
			String errormessage = driver.findElement(By.xpath("//div[@class='pbBody']//div[@class='error-span ng-scope']/div")).getText();
			System.out.println(errormessage);
			
			if(errormessage.contains("Remote meeting can not be submitted without scheduling and starting the meeting"))
				report.updateTestLog("Verify able to see error message " + errormessage+"", "Able to see error message " + errormessage + "", Status.PASS);
			else
				report.updateTestLog("Verify able to see error message  " + errormessage + "", "Unable to see error message " + errormessage + "", Status.FAIL);
			
			if (cf.isElementVisible(By.xpath("//input[@value='Schedule']"), "Schedule"))
			{
				cf.clickElement(By.xpath("//input[@value='Schedule']"), "Schedule");
			}
			
			cf.waitForSeconds(10);
			cf.implicitWait(30, TimeUnit.SECONDS);
			//cf.scrollToTop();
			if (cf.isElementVisible(By.xpath("//span[contains(text(),'attendees have not been invited')]"), "attendees have not been invited"))
			{
				cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
				alertvalidate();
				
				cf.clickElement(By.xpath("//button[text()='Submit']"), "Submit");
				cf.waitForSeconds(3);
				
				String errormessage1 = driver.findElement(By.xpath("//div[@class='pbBody']//div[@class='error-span ng-scope']/div")).getText();
				System.out.println(errormessage1);
				
				if(errormessage.contains("Remote meeting can not be submitted without scheduling and starting the meeting"))
					report.updateTestLog("Verify able to see error message " + errormessage1+"", "Able to see error message " + errormessage1 + "", Status.PASS);
				else
					report.updateTestLog("Verify able to see error message  " + errormessage1 + "", "Unable to see error message " + errormessage1 + "", Status.FAIL);
				
				
				cf.clickElement(By.xpath("//div[@class='dataCol']/img"), "Info Button");
				
				String meetingName = driver.findElement(By.xpath("//div[@class='navLinks']//span[@class='menuButtonLabel']")).getText();
				System.out.println(meetingName);
				
				String meeting = meetingName +"'s Meeting - Edit";
				cf.setData(By.xpath("//td[text()='Meeting Name']/following-sibling::td/textarea"), "Meeting Name", meeting);
				
				cf.clickElement(By.xpath("//input[@name='Done']"), "Done");
				
				cf.clickElement(By.xpath("//input[@value='Send Invitation']"), "Send Invitation");

				cf.implicitWait(30, TimeUnit.SECONDS);

				cf.waitForSeconds(10);

				if (cf.isElementVisible(By.xpath("//div[text()='Send Invitation']"), "Send Invitation"))
				{
					List<WebElement> meetingLinks = driver.findElements(By.xpath("//div[@ng-if='!isBeingScheduled[attendee.data.Id]']"));
					for (int i = 0; i < meetingLinks.size(); i++)
					{
						String meetinglink = meetingLinks.get(i).getText();
						System.out.println(meetinglink);
					}

				}

				cf.clickElement(By.xpath("//a[text()='Send to All']"), "Send to All");

				cf.waitForSeconds(5);
				cf.implicitWait(30, TimeUnit.SECONDS);

				cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));

				List<WebElement> emailMissing = driver.findElements(By.xpath("//div[@class='ineligibleRecipient emailMissing error']"));
				for (int i = 0; i < emailMissing.size(); i++)
				{
					cf.clickElement(By.xpath("//a[text()='Manage Email Addresses']"), "Manage Email Addresses");

					cf.setData(By.xpath("//td[text()='Secondary Email']/following-sibling::td/input"), "Secondary Email", "jayakumar.ganesan@astrazeneca.com");

					cf.clickElement(By.xpath("//button[@id='email_mgmt_save']"), "Save");

				}

				List<WebElement> emailDropdown = driver.findElements(By.xpath("//select[@class='emailSelect']"));
				for (int i = 0; i < emailDropdown.size(); i++)
				{
					System.out.println(emailDropdown.get(i).getText());
				}

				cf.implicitWait(10, TimeUnit.SECONDS);

				List<WebElement> tabList = driver.findElements(By.xpath("//ul[@id='tabList']/li"));
				for (int i = 0; i < tabList.size(); i++)
				{
					if (i > 0)
					{
						tabList.get(i).click();
					}
				}
				
				cf.switchToFrame1(By.xpath("//iframe[@id='frame_a1n1l0000003dqyAAA']"));
				
				cf.scrollToElement(By.xpath("//td[contains(text(),'Dear Doctor')]//input[@class='requiredCustomText']"), "Scroll To Text Box");
				cf.setData(By.xpath("//td[contains(text(),'Dear Doctor')]//input[@class='requiredCustomText']"), "Text Field", "Test");	
				cf.scrollToTop();
				cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));
				//cf.switchToFrame1(By.xpath("//iframe[@id='vod_iframe']"));
				
				cf.waitForSeconds(3);
				cf.clickElement(By.xpath("//div[@class='bccBox']"), "BCC Box"); 
				cf.waitForSeconds(2);
				cf.clickElement(By.xpath("//button[contains(text(),'Send Now')]"), "Send Now");

				cf.implicitWait(30, TimeUnit.SECONDS);

				if (cf.isElementVisible(By.xpath("//span[@id='successNote']/span[1]"), "Success Note"))
				{
					cf.clickElement(By.xpath("//button[@id='cancelButton']"), "Close");
				}

				cf.implicitWait(60, TimeUnit.SECONDS);

				cf.waitForSeconds(10);

				//cf.waitForSeconds(5);

				//driver.get("https://outlook.office.com/mail/");

				//div[@class='placeholderContainer']/input

				//(//span[text()='Veeva Engage Meeting'])[1]

				//div[@aria-label='attachments']/div/div

				if (cf.isElementVisible(By.xpath("//h2[text()='Veeva Remote Detailing']"), "Veeva Remote Detailing"))
				{
					cf.clickElement(By.xpath("//input[@value='Start']"), "Start");
					cf.implicitWait(60, TimeUnit.SECONDS);
				}

				cf.waitForSeconds(10);

				String parentwindow = driver.getWindowHandle();
				Set<String> childwindows = driver.getWindowHandles();
				for (String window : childwindows)
				{
					if (!(parentwindow.equals(window)))
					{
						driver.switchTo().window(window);
						System.out.println("Window switched successfully");
						cf.waitForSeconds(5);
					}
				}

				//driver.get("https://engage.veeva.com/windowshost/VeevaCRMEngage_Launcher.exe");
				cf.waitForSeconds(5);
				driver.get("chrome://downloads/");
				report.updateTestLog("Download Page", "Download Page", Status.SCREENSHOT);
				
				Screen s = new Screen();
				Pattern p1 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\launcher.PNG");
				report.updateTestLog("Download is completed", "Download is completed", Status.SCREENSHOT);
				System.out.println(p1);
				Screen s1 = new Screen();
				Pattern p2 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\run.PNG");
				report.updateTestLog("Veeva Engage App Run", "Veeva Engage App Run is clicked", Status.SCREENSHOT);

				Screen s2 = new Screen();
				Pattern p3 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\stopvideo.PNG");

				Pattern p4 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\manageparticipents.PNG");
				Pattern p5 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\chat.PNG");
				Pattern p6 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\endmeeting.PNG");
				Pattern p7 = new Pattern("C:\\Veeva_Online\\MFA_Online\\src\\test\\java\\AutoIT\\Sikuli\\endmeetingforall.PNG");

				cf.waitForSeconds(3);

				s.click(p1);

				cf.waitForSeconds(3);

				s1.click(p2);

				cf.waitForSeconds(20);

				s2.click(p3);

				cf.waitForSeconds(3);

				s2.click(p4);

				cf.waitForSeconds(3);

				s2.click(p5);

				cf.waitForSeconds(3);
				s2.click(p6);
				cf.waitForSeconds(3);
				s2.click(p7);
				
			}

		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void myAccountsSchedule() throws Exception
	{
		cf.switchToFrame(objMyAccounts.sframe);

		cf.clickElement(By.xpath("//tr[contains(@class,'dataRow')]/td"), "Click any Account - Check box");

		cf.waitForSeconds(3);

		cf.clickElement(By.xpath("//input[@name='scheduleCallButton']"), "Schedule Call Button");

		cf.waitForSeconds(5);

		if (cf.isElementVisible(By.xpath("//h2[text()='Call Scheduler']"), "Call Scheduler"))
		{
			cf.clickElement(By.xpath("(//button[text()='Schedule'])[1]"), "Schedule");
		}

	}
}