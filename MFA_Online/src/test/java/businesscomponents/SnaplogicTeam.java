package businesscomponents;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class SnaplogicTeam extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public SnaplogicTeam(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	
	public void unDelete() throws Exception
	{
		String strWorkbenchAppUrl = properties.getProperty("WorkbenchAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strWorkbenchAppUrl);
		if (cf.isElementPresent(By.xpath("//label[text()='Environment:']"), "Workbench - Home Page"))
		{
			Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='oauth_env']")));
			dropdown.selectByVisibleText("Sandbox");
			System.out.println("Sandbox selected from Environment");
			cf.clickElement(By.xpath("(//input[@type='checkbox'])[2]"), "Agree Button");
			cf.clickElement(By.xpath("//input[@type='submit']"), "Login with salesforce");
		}
		if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
		{
			cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
		}
		
		if (cf.isElementPresent(By.xpath("//span[text()='data']")))
		{
			cf.clickElement(By.xpath("//span[text()='data']"), "data");
			cf.clickElement(By.xpath("//a[text()='Undelete']"), "Undelete");
			
			
					
		}
		if (cf.isElementPresent(By.xpath("//td[text()='Undelete']")))
		{
			cf.clickElement(By.xpath("(//input[@name='sourceType'])[2]"), "radio button");
			cf.clickElement(By.xpath("(//input[@name='file'])"), "Choose file");
			
					
							
		}
		
				
	}
	
}
