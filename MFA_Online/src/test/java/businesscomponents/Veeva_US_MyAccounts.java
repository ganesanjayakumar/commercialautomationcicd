package businesscomponents;

import supportlibraries.*;

import com.mfa.framework.Status;
import ObjectRepository.*;

import org.openqa.selenium.*;

public class Veeva_US_MyAccounts extends VeevaFunctions {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	
	public Veeva_US_MyAccounts(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * This method is used to navigate to my accounts page required
	 * @throws Exception
	 */
	public void navigateToMyAccounts() throws Exception{
		vf.clickVeevaTab("My Accounts");
		if(cf.isElementVisible(By.cssSelector("//h1[contains(text(),'My Accounts')]"), "My Accounts"))
			report.updateTestLog("Verify My Accounts Page","My Accounts page is displayed as expected", Status.PASS);
		else
			report.updateTestLog("Verify My Accounts Page","My Accounts page is NOT displayed", Status.FAIL);
	}

	/**
	 * This method will be used to click on New button and create new account
	 * @throws Exception
	 */
	public void createNewAccount() throws Exception{
		cf.clickSFButton("New","button");
		if(cf.isElementVisible(By.cssSelector("//h1[contains(text(),'New Account')]"), "New Account"))
			report.updateTestLog("Verify New Account cresation Page", "Verify if New Account creation page is displayed successfully", Status.PASS);
		else
			report.updateTestLog("Verify New Account cresation Page", "Verify if New Account creation page is not displayed", Status.FAIL);
	}
	
	
	/**
	 * Save the account Address Details in Excel Sheet.
	 * @throws Exception 
	 */
	public void saveaccountaddressmyaccounts() throws Exception{
		String Addressline1="",city="",state="",Zip="",address;
		int i;
		String account=dataTable.getData("Login", "AccountName");
		cf.switchToFrame(objMyAccounts.sframe,"MyAccounts List");
		int rows=driver.findElements(By.xpath("//table[@id='vodResultSet']//tbody/tr/td[1]")).size();
		System.out.println(rows);
		if(rows>0){
			report.updateTestLog("Verify able to see accounts in My Accounts Table.", "Able to see accounts in My Accounts Table.", Status.PASS);
			for(i=2;i<=rows;i++){
				String appaccount=getDatafromTableAccounts("Name", i);
				if((account.trim()).equals(appaccount.trim())){
					dataTable.putData("Login", "AccountName", account);
					Addressline1=getDatafromTableAccounts("Address line 1", i).replaceAll("\\s+", "");
					System.out.println(Addressline1);
					dataTable.putData("Accounts", "AddressLine", Addressline1);
					city = getDatafromTableAccounts("City",i).replaceAll("\\s+", "");
					System.out.println(city);
					dataTable.putData("Accounts", "City", city);
					state = getDatafromTableAccounts("State",i).replaceAll("\\s+", "");
					System.out.println(state);
					dataTable.putData("Accounts", "State", state);
					Zip = getDatafromTableAccounts("Zip",i).replaceAll("\\s+", "");
					System.out.println(Zip); 
					dataTable.putData("Accounts", "Zip", Zip);
					//if(!((Addressline1.trim()).isEmpty() && (city.trim()).isEmpty() && (state.trim()).equals(null)&& (Zip.trim()).equals(null))){
					//if(!list.isEmpty()){
					if(!((Addressline1.trim()).equals("") && (city.trim()).equals("") && (state.trim()).equals("")&& (Zip.trim()).equals(""))){
						report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+"", "Able to see Account Address Details beside Account Name "+account+"", Status.SCREENSHOT);
						address="Available";
						dataTable.putData("Accounts", "AddressAvailable", address);
						cf.switchToParentFrame();
						break;
					}
					else{
						report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+"", "Unable to see Account Address Details beside Account Name "+account+"", Status.SCREENSHOT);
						address="Unavailable";
						dataTable.putData("Accounts", "AddressAvailable", address);
						cf.switchToParentFrame();
						break;
					}
				}
			}
		}
		else
			report.updateTestLog("Verify able to see accounts in My Accounts Table.", "There are no accounts in My Accounts Table.", Status.FAIL);
	}
	public String getDatafromTableAccounts(String ColName,int i){
		int columns= driver.findElements(By.xpath("//table[@id='vodResultSet']//th/a")).size();
		int j,icolCount=1;
		String columnname;
		String value = null;
		for(j=2;j<=columns+1;j++){
			columnname=driver.findElement(By.xpath("//table[@id='vodResultSet']//th["+j+"]/a")).getText();
			if(ColName.equals(columnname)){
				if(ColName.equals("Name"))
					value=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td["+(icolCount+1)+"]/a")).getText();
				else
					value=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td["+(icolCount+1)+"]")).getText();

				break;
			}
			else
				icolCount++;
		}
		return value;
	}

	/**
	 * Scroll to My Preferred Address and change the Address preferences.
	 * @throws Exception
	 */
	public void changeMypreferedaddPreference() throws Exception{
		String addressavailable=dataTable.getData("Accounts", "AddressAvailable");
		cf.switchToFrame(By.xpath("//iframe[@title='Account Territory Info']"), "Account Territory Info");
		cf.scrollToElement(By.id("labelv-i-value:reference:TSF_vod__c:Address_vod__c"), "My Prefered Address Label");
		cf.clickButton(By.id("pbButtonTableColEdit"), "Click Edit Button");
		cf.switchToFrame(By.id("vod_iframe"), "Account Territory Info");
		if(cf.isElementVisible(By.id("pbButtonTableColSave"), "Save Button")){
			report.updateTestLog("Verify able to navigate to Preferred Address Change Page.", "Able to navigate to Preferred Address Change Page.", Status.PASS);
			if(addressavailable.equals("Available"))
				cf.selectData(By.id("i-value:picklist:TSF_vod_c:Address_vod__c"), "My Preffered Address", "--None--");	
			else{
				String option=driver.findElement(By.xpath("//select[@id='i-value:picklist:TSF_vod_c:Address_vod__c']//option[2]")).getText();
				cf.selectData(By.id("i-value:picklist:TSF_vod_c:Address_vod__c"), "My Preffered Address",option);
				dataTable.putData("Accounts", "AddressOptionValue", option);
			}
			cf.clickButton(By.id("pbButtonTableColSave"), "Save");
			cf.waitForSeconds(5);
			cf.switchToParentFrame();
		}
		else{
			report.updateTestLog("Verify able to navigate to Preferred Address Change Page.", "Unable to navigate to Preferred Address Change Page.", Status.FAIL);
		}
	}

	/**
	 * Validate the Changed Preferred Address value
	 * @throws Exception 
	 */
	public void validatechangedpreferredaddmyaccounts() throws Exception{
		String addressavailable=dataTable.getData("Accounts", "AddressAvailable");
		String account=dataTable.getData("Login", "AccountName");
		String Addressline1="",city="",state="",Zip="";
		int i;
		cf.switchToFrame(objMyAccounts.sframe,"MyAccounts List");
		if(cf.isElementVisible(By.id("vodResultSet"), "My Accounts Table")){
			int rows=driver.findElements(By.xpath("//table[@id='vodResultSet']//tbody/tr/td[1]")).size();
			System.out.println(rows);
			if(rows>0){
				for(i=2;i<=rows;i++){
					String appaccount=getDatafromTableAccounts("Name", i);
					if((account.trim()).equals(appaccount.trim())){
						Addressline1=getDatafromTableAccounts("Address line 1", i);
						city = getDatafromTableAccounts("City",i);
						state = getDatafromTableAccounts("State",i);
						Zip = getDatafromTableAccounts("Zip",i);
						if(addressavailable.equals("Available")){
							if((Addressline1.trim()).equals("")&& (city.trim()).equals("") && (state.trim()).equals("")&& (Zip.trim()).equals("")){
								report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", "Unable to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", Status.PASS);
								break;
							}else
								report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", "Able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", Status.FAIL);
						}
						else{
							String optionvalue=dataTable.getData("Accounts", "AddressOptionValue");
							if(optionvalue.contains(Addressline1) && optionvalue.contains(city)&& optionvalue.contains(state)&& optionvalue.contains(Zip)){
								report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", "Able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", Status.PASS); 
								break;

							}else
								report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", "Unable to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", Status.FAIL);  
						}
					}
				}
			}
		}
		else
			report.updateTestLog("Verify able to see Account List Table", "Unable to see Account List Table.",Status.FAIL);
	}


}


