package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_DxL_MedicalInquiry extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_DxL_MedicalInquiry(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	/**
	 * This method is for searching and navigating to the accounts
	 * @throws Exception
	 */
	public void searchandGo()
	{
		String account = dataTable.getData("MedInq", "Account");
		Select search = new Select(driver.findElement(By.xpath("//select[@title='Search scope']")));
		search.selectByVisibleText("Accounts");
		driver.findElement(By.className("searchTextBox")).sendKeys(account);
		report.updateTestLog("Fetch Account","Account name is entered in the search box",Status.SCREENSHOT);
		driver.findElement(By.xpath("//div[@class='standardSearchElementBody']//input[@title='Go!']")).click();
		cf.waitForSeconds(3);
	}
	

	/**
	 * This method is for clicking record a call and click more actions and navigate to Medical inquiry page
	 * @throws Exception
	 */
	public void moreActionsRecCall() throws Exception
	{
		cf.waitForSeconds(4);
		String accType = dataTable.getData("MedInq", "Account Record Type");
		String attendee = dataTable.getData("MedInq", "Attendee");
		String profile = dataTable.getData("Login", "Profile");
		report.updateTestLog("Fetch Account","Account details page is displayed",Status.SCREENSHOT);
		cf.waitForSeconds(5);
		cf.clickSFButton("Record a Call");		
		cf.waitForSeconds(3);
		report.updateTestLog("Call Recording Page","Call Recording Page is displayed",Status.SCREENSHOT);
		
		
		cf.waitForSeconds(2);
		//String attendeeError = driver.findElement(By.xpath("//div[@class='pbBody']//div[contains(@class,'error-span ng-binding ng-scope') and text()='Select One Attendee for a Medical Inquiry']")).getText();
		if(accType.equals("HCA")) //select phone for HCA
		{
				//Add attendee in record a call page for HCA and then select medical inquiry
				driver.findElement(By.xpath("//div[@name='zvod_Attendees_vod__c']//input[@type='button'][@value='Search']")).click();
				driver.findElement(By.xpath("//*[@id='searchInput']")).sendKeys(attendee);
				report.updateTestLog("Call Recording Page","Attendee name is searched",Status.SCREENSHOT);
				driver.findElement(By.xpath("//*[@value='Go!']")).click();
				cf.waitForSeconds(2);
				driver.findElement(By.xpath("//tr[@ng-repeat = 'result in searchResults']/td/input")).click();
				driver.findElement(By.xpath("//input[@id='searchInput']/parent::td/parent::tr/following-sibling::tr//input[@title='Add']")).click();
				cf.waitForSeconds(3);
				report.updateTestLog("Call Recording Page","Attendee name added",Status.SCREENSHOT);
				driver.findElement(By.xpath("//*[@id='topButtonRow']//*[@name='MoreActions']")).click();
				driver.findElement(By.xpath("//div[contains(@class,'popover bottom display_block')]//a[.='Medical Inquiry']")).click();
				report.updateTestLog("Medical Inquiry","Medical Inquiry record type selection page is opened for HCA",Status.PASS);
			
		}
		else if(accType.equals("HCP")) // select email for HCP
		{	
			cf.waitForSeconds(5);
			switch(profile)
			{
				case "Medical" :
								cf.selectData(By.id("MA_Channel_AZ__c"), "Interaction channel", "Email");
								cf.selectData(By.id("MA_Patient_Safety_Attestation_AZ__c"), "Attestation", "Any adverse event mentioned in the free-text has been reported to AZ local Patient Safety");
								driver.findElement(By.xpath("//*[@id='topButtonRow']//*[@name='MoreActions']")).click();
								cf.clickButton(By.xpath("//div[contains(@class,'popover bottom display_block')]//a[.='Medical Inquiry']"), "Medical Inquiry");
								report.updateTestLog("Medical Inquiry","Medical Inquiry record type selection page is opened for HCP",Status.PASS);
								break;
								
				case "DxL": report.updateTestLog("Medical Inquiry","Medical Inquiry record type selection page is opened for HCP",Status.PASS);
							break;
				default:report.updateTestLog("Medical Inquiry","Medical Inquiry record type selection page is not opened for HCP",Status.FAIL);
			}
			
			
			
		}
		
	}
	
	/**
	 * This method is choosing the medical inquiry record type
	 * @throws Exception
	 */
	public void selectInquiryRecordType() throws Exception{
		cf.waitForSeconds(3);
		cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"),"Record Type","Diagnostic Information Request");
		report.updateTestLog("Medical Inquiry Record Type","Medical Inquiry record type selection is displayed",Status.SCREENSHOT);
		cf.clickSFButton("Continue");
		cf.waitForSeconds(5);
	}
	
	/**
	 * This method is for entering the medical inquiry required information
	 * @throws Exception
	 */
	public void enterMIInformation() throws Exception
	{
		String acc = dataTable.getData("MedInq", "Account");
		String assignToUser = dataTable.getData("MedInq", "Assign To User");
		String product = dataTable.getData("MedInq", "Product");
		String inqText = (new Date()).toString()+":\n Medical Inquiry information is entered here";
		String accType = dataTable.getData("MedInq", "Account Record Type");
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='itarget']")));
		//cf.switchToFrame(By.name("itarget"));
		cf.waitForSeconds(5);
		if(cf.isElementVisible(By.xpath("//h3[text()='Medical Inquiry']"), "Medical Inquiry Details"))
		{
			if(accType.equals("HCP"))
			{
				report.updateTestLog("Medical Inquiry","Medical Inquiry details page is opened",Status.PASS);				
				driver.findElement(By.xpath("//td[@id='lbl_Medical_Inquiry_vod__c:Delivery Method']/following-sibling::td//select[@name='reqi-value:picklist:Medical_Inquiry_vod__c:Delivery_Method_vod__c']")).click();
				Select delvMeth = new Select(driver.findElement(By.xpath("//td[@id='lbl_Medical_Inquiry_vod__c:Delivery Method']/following-sibling::td//select[@name='reqi-value:picklist:Medical_Inquiry_vod__c:Delivery_Method_vod__c']")));
				cf.waitForSeconds(2);
				delvMeth.selectByValue("Email_vod");				
				System.out.println("Email is selected");				
				driver.findElement(By.xpath("//table[@id='innerDataDivTable-Email_vod']//select[@id='EMailSelect']")).click();
				cf.waitForSeconds(2);
				Select email = new Select(driver.findElement(By.xpath("//table[@id='innerDataDivTable-Email_vod']//select[@id='EMailSelect']")));
				email.selectByIndex(1);			
				String s = email.getFirstSelectedOption().getText();
				if(s.equalsIgnoreCase("None"))
					report.updateTestLog("Delivery Channel","There is no email assigned",Status.FAIL);
				else
					System.out.println("Email ID is selected");
			}
			else  if(accType.equals("HCA"))
			{
				report.updateTestLog("Medical Inquiry","Medical Inquiry details page is opened",Status.PASS);
				//HCA account to be selected as the account contains the attendee added
				WebElement account = driver.findElement(By.xpath("//td[@id='lbl_Medical_Inquiry_vod__c:Account']/following-sibling::td//input"));
				account.clear();
				account.sendKeys(acc);
				cf.waitForSeconds(2);
			
				driver.findElement(By.xpath("//td[@id='lbl_Medical_Inquiry_vod__c:Delivery Method']/following-sibling::td//select[@name='reqi-value:picklist:Medical_Inquiry_vod__c:Delivery_Method_vod__c']")).click();
				Select delvMeth = new Select(driver.findElement(By.xpath("//td[@id='lbl_Medical_Inquiry_vod__c:Delivery Method']/following-sibling::td//select[@name='reqi-value:picklist:Medical_Inquiry_vod__c:Delivery_Method_vod__c']")));
				cf.waitForSeconds(2);
				delvMeth.selectByValue("Phone_vod");				
				System.out.println("Phone is selected");
				
				driver.findElement(By.id("PhoneSelect")).click();
				Select phone = new Select(driver.findElement(By.id("PhoneSelect")));
				cf.waitForSeconds(2);
				phone.selectByIndex(1);
				String s = phone.getFirstSelectedOption().toString();
				if(s.equalsIgnoreCase("None"))
					report.updateTestLog("Delivery Channel","There is no phone number assigned",Status.FAIL);
				else
					System.out.println("Phone number is selected");
			}
				
				cf.clickElement(By.xpath("//span[@id='reqi-value:reference:Medical_Inquiry_vod__c:Assign_To_User_vod__c:span1']//a/img"), "Assign to user -lookup");
				cf.waitForSeconds(2);
				//switch to product search window
				String parentWindowHandler1 = driver.getWindowHandle(); // Store your parent window
				String subWindowHandler1 = null;
				Set<String> handles1 = driver.getWindowHandles(); // get all window handles
				Iterator<String> iterator1 = handles1.iterator();
				while (iterator1.hasNext())
				{
				    subWindowHandler1 = iterator1.next();
				}		
				
				driver.switchTo().window(subWindowHandler1);
				cf.waitForSeconds(3);
				cf.switchToFrame(By.id("frameSearch"));
				cf.setData(By.id("searchBox"), "Search", assignToUser);
				cf.clickButton(By.id("searchButton"), "Go");
				cf.waitForSeconds(3);
				cf.switchToParentFrame();
				cf.switchToFrame(By.id("frameResult"));
				String user = driver.findElement(By.xpath("//table[@id='listTableId']//tr[2]/th/a")).getText();
				if(user.equals(assignToUser))
					driver.findElement(By.xpath("//table[@id='listTableId']//tr[2]/th/a")).click();
				else
					report.updateTestLog("Assign to user","User not found",Status.FAIL);				
				driver.switchTo().window(parentWindowHandler1);
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='itarget']")));
				
				driver.findElement(By.xpath("//td[@id='value_Medical_Inquiry_vod__c:Product Name']//a")).click();				
				cf.waitForSeconds(5);
				//switch to product search window
				parentWindowHandler1 = driver.getWindowHandle(); // Store your parent window
				subWindowHandler1 = null;
				handles1 = driver.getWindowHandles(); // get all window handles
				iterator1 = handles1.iterator();
				while (iterator1.hasNext())
				{
				    subWindowHandler1 = iterator1.next();
				}		
				
				driver.switchTo().window(subWindowHandler1);
				cf.waitForSeconds(4);
				/*cf.switchToFrame(By.id("frameSearch"));
				cf.setData(By.id("searchBox"), "Search", product);
				cf.clickButton(By.id("searchButton"), "Go!");
				cf.waitForSeconds(5);
				cf.switchToParentFrame();*/
				cf.switchToFrame(By.id("frameResult"));
				driver.findElement(By.xpath("//a[text()='"+product+"']")).click();					
				driver.switchTo().window(parentWindowHandler1);
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='itarget']")));
				
				driver.findElement(By.xpath("//textarea[@id='reqi-value:textarea:Medical_Inquiry_vod__c:Inquiry_Text__c']")).sendKeys(inqText);;
				report.updateTestLog("Medical Inquiry","Medical Inquiry details page is opened and mandatory informations are provided",Status.PASS);
				
				//save medical inquiry - save button is clicked
				driver.findElement(By.xpath("//tr[@id='pbHeaderTableRow']//td[@id='pbButtonTableCol']/input[@value='Save']")).click();
				cf.waitForSeconds(5);
			}
		
		else
			report.updateTestLog("Medical Inquiry","Medical Inquiry details page is not opened",Status.FAIL);
}

	
	/**
	 * This method is for validating the saved medical inquiry information
	 * @throws Exception
	 */
	public void savedMIInformation() throws Exception
	{
		String medInqID ="";
		int rows;
		String status=""; 		
		Date dNow=new Date();
		String date = "";
		String profile = dataTable.getData("Login", "Profile");
		//Call Saved page is loaded
		
		if((cf.isElementVisible(By.xpath("//h1[text()='Diagnostic Interaction']"), "Saved Call Page")) || (cf.isElementVisible(By.xpath("//h1[text()='MSL Interaction']"), "Saved Call Page")))
		{
			report.updateTestLog("Call is Saved","Saved Call page is displayed",Status.PASS);
			//capture the call record ID and the medical inquiry ID
			String callID= driver.findElement(By.xpath("//h2[contains(@class,'pageDescription ng-binding')]")).getText();
			dataTable.putData("MedInq", "Call ID", callID);
		}
		
		else
			report.updateTestLog("Call is Saved","Saved Call page is not displayed",Status.FAIL);
	
			driver.findElement(By.xpath("//div[@name='Account_vod__c']/a")).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[text()='Account']")));
			cf.clickSFLinkLet("Medical Inquiries");
			//cf.verifyGetClickSearchDataFromTableOneColumn("Medical Inquiries", "Medical Inquiry Id", medInqID,"Click","LinkLet");
		//expand related list if more inquiries are present	
		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Medical Inquiries')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Show more link"))
		{
			cf.clickLink(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Medical Inquiries')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to list");
			report.updateTestLog("Medical Inquiry","Medical Inquiry related list is expanded and displayed",Status.PASS);
			rows = cf.getRowCountFromTable("list", "Medical Inquiries");
			WebElement sort = driver.findElement(By.xpath("//tr[@class='headerRow']//th[contains(@class,'DateElement zen-deemphasize')]/a"));
			sort.click();
			String sortOrder = driver.findElement(By.xpath("//tr[@class='headerRow']//th[contains(@class,'DateElement zen-deemphasize')]/a")).getAttribute("title");
			System.out.println("Created date is sorted in "+sortOrder);
			if(sortOrder.contains("Sorted ascending"))
			{
				int x= rows+1;
				medInqID =driver.findElement(By.xpath("//table[@class='list']/tbody//tr["+x+"]/th/a")).getText();
				dataTable.putData("MedInq", "MedID", medInqID);
				System.out.println(medInqID);// The last inquiry is the latest created one
				status = driver.findElement(By.xpath("//table[@class='list']/tbody//tr["+x+"]/td[4]")).getText();
				dataTable.putData("MedInq", "Status", status);
				
			}
			else if(sortOrder.contains("Sorted descending"))
			{
				medInqID =driver.findElement(By.xpath("//table[@class='list']/tbody//tr[2]/th/a")).getText();
				dataTable.putData("MedInq", "MedID", medInqID);
				System.out.println(medInqID);// The first inquiry is the latest created one
				status = driver.findElement(By.xpath("//table[@class='list']/tbody//tr[2]/td[3]")).getText();
				dataTable.putData("MedInq", "Status", status);
			}
				
		}	
		//search in the related list on My Accounts page
		else//dd.MM.YYYY 12/20/2018
		{
			report.updateTestLog("Medical Inquiry","Medical Inquiry related list is displayed",Status.PASS);
			switch (profile)
			{
				case "DxL" :
					date = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
					System.out.println("Today's date "+date);
					break;
				case "Medical":
					date = new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
					System.out.println("Today's date "+date);
					break;
			}
			//Medical inquiry ID and status are captured
			medInqID = cf.getDataFromLinkLetTable("Medical Inquiries", "Created Date", date, "Medical Inquiry Id");
			dataTable.putData("MedInq", "MedID", medInqID);
			status = cf.getDataFromLinkLetTable("Medical Inquiries", "Medical Inquiry Id", medInqID, "Status");
			dataTable.putData("MedInq", "Status", status);		
		}
	
						
}
	
	/**
	 * This method is for submitting the medical inquiry
	 * @throws Exception
	 */
public void submitMedInquiry() throws Exception
{
	String medInqID =dataTable.getData("MedInq", "MedID");
	String status="";
	int rows, i;		
	searchandGo();	
	cf.clickSFLinkLet("Medical Inquiries");	
	cf.verifyGetClickSearchDataFromTableOneColumn("Medical Inquiries", "Medical Inquiry Id", medInqID,"Click","LinkLet");
	cf.waitForSeconds(2);
	cf.switchToFrame(By.xpath("//iframe[@id='itarget']"));
	if(cf.isElementVisible(By.xpath("//h3[text()='Medical Inquiry']"), "Medical Enquiry Page"))
	{
		report.updateTestLog("Medical Inquiry","Medical Enquiry Page is  displayed",Status.PASS);
		//click edit button to submit the enquiry
		driver.findElement(By.xpath("//tr[@id='pbHeaderTableRow']//td[@id='pbButtonTableCol']/input[@value='Edit']")).click();
		cf.switchToFrame(By.xpath("//iframe[@id='itarget']"));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Medical Inquiry']")));
		report.updateTestLog("Medical Inquiry","Medical Enquiry Edit page is displayed",Status.SCREENSHOT);
		cf.clickButton(By.xpath("//tr[@id='pbHeaderTableRow']//td[@id='pbButtonTableCol']/input[@value='Submit']"), "Submit Medical Inquiry");
		cf.waitForSeconds(2);		
		cf.switchToFrame(By.xpath("//iframe[@id='itarget']"));
		if(cf.isElementVisible(By.xpath("//h3[text()='Medical Inquiry']"), "Medical Enquiry Page"))
		{
			status= driver.findElement(By.id("value_Medical_Inquiry_vod__c:Status")).getText();
			dataTable.putData("MedInq", "Status", status);
			System.out.println("Status of the inquiry is "+status);
			if(status.equals("Submitted"))
			{
				report.updateTestLog("Medical Inquiry","Medical Enquiry is submitted",Status.PASS);
				String lastModify = driver.findElement(By.xpath("//td[contains(@id,'value_Medical_Inquiry_vod__c:Last Modified By')]")).getText();
				dataTable.putData("MedInq", "Last Modified", lastModify);
				
			}
		}
		
		else 
			report.updateTestLog("Medical Inquiry","Medical Enquiry is not submitted",Status.FAIL);
	}
	
	else
		report.updateTestLog("Medical Inquiry","Medical Enquiry Page is not displayed",Status.FAIL);
			
		driver.findElement(By.xpath("//td[@id='value_Medical_Inquiry_vod__c:Account']/a")).click();
		cf.waitForSeconds(3);
		cf.clickSFLinkLet("Medical Inquiries");
		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Medical Inquiries')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Show more link"))
		{
			report.updateTestLog("Medical Inquiry","Medical Inquiry related list is expanded and displayed",Status.DONE);
			WebElement element1=driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Medical Inquiries')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Expand list");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[text()='Medical Inquiries']")));
			//rows=cf.getRowCountFromLinkLetTable("Medical Inquiries");
			rows = cf.getRowcountFromTable(By.xpath("//table[@class='list']/tbody//tr"), "Medical Inquiries")+1;
			System.out.println("There are "+rows+" medical inquiries");
		//	List<WebElement> tr = driver.findElements(By.xpath("//table[@class='list']/tbody//tr"));
			for(i=2;i<=rows;i++)
			{	
				String medID = driver.findElement(By.xpath("//table[@class='list']/tbody//tr["+i+"]//th/a")).getText();
				if(medID.equals(medInqID))
				{
					String st = driver.findElement(By.xpath("//table[@class='list']/tbody//tr["+i+"]//td[3]")).getText();
					if(st.equals(status))
					{
						report.updateTestLog("Medical Inquiry","Medical Inquiry"+medID+" is present in the related list with status as "+st,Status.PASS);
						break;
					}
				}
			}		
				
		}	
		//search in the related list
		else
		{
			report.updateTestLog("Medical Inquiry","Medical Inquiry related list is displayed",Status.DONE);
			cf.verifyDataFromLinkLetTable("Medical Inquiries", "Medical Inquiry Id", medInqID, "Status", "Submitted");
			
		}	
			
	}	
	
	/**
	 * This method is for identifying the medical inquiry created by sales or MSL in the medical inquiry fulfillment 
	 * @throws Exception
	 */
	public void identifyDIRinMIF() throws Exception
	{
		String modify = dataTable.getData("MedInq", "Last Modified");
		String[] a = modify.split(",");
		String dateTime = a[1].trim();
		System.out.println(dateTime);
		String[] b = dateTime.split(" ");
//		String date = b[0].trim();
		String time = b[1].trim();
		
	//	Date d = new SimpleDateFormat("MM.dd.YYYY").parse(date);
		String formatDate = new SimpleDateFormat("M/dd/YYYY").format(new Date()).toString();
		
		Date d1 = new SimpleDateFormat("HH:mm").parse(time);
		String formatTime = new SimpleDateFormat("h:mm a").format(d1).toString();
		
		System.out.println("Format changed "+formatDate);
		String newDateTime = formatDate.concat(" "+formatTime);
		System.out.println("New date and time "+newDateTime);
		String inqID = dataTable.getData("MedInq", "MedID");
		//navigate to medical inquiry fullfillment
		vf.clickVeevaTab("Medical Inquiry Fulfillment");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()=' Inquiry Fulfillment']")));
		report.updateTestLog("Medical Inquiry Fulfillment","Medical Inquiry Fulfillment page is displayed",Status.SCREENSHOT);
		cf.selectData(By.id("statusSelect"), "MIF filter", "Open");
		cf.clickElement(By.xpath("//a[text() = 'Initiated Datetime']"), "Initiated Datetime");	
		cf.waitForSeconds(3);
		String initDateTime="";
		String arrow = "";
		boolean order= true;		
		while(order==true)
		{
			cf.clickElement(By.xpath("//a[text() = 'Initiated Datetime']/following-sibling::span/img"), "Initiated Datetime to sort");
			arrow = driver.findElement(By.xpath("//a[text() = 'Initiated Datetime']/following-sibling::span/img")).getAttribute("src");
			if(arrow.contains("desc"))
				order=false;		
		}
		if(arrow.contains("desc"))
			 initDateTime = driver.findElement(By.xpath("//table[@class='list']//tr[2]//td[7]")).getText().trim();
		
		if(newDateTime.contains(initDateTime)) //if(initDateTime.contains(newDateTime))
		{
			String MIF = driver.findElement(By.xpath("//table[@class='list']//tr[2]//td[1]")).getText();
			dataTable.putData("MedInq", "MIF ID", MIF);
			cf.clickLink(By.xpath("//table[@class='list']//tr[2]//td[1]//a"), "MIF link");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[text()='Inquiry Fulfillment']")));
			String medInqID = driver.findElement(By.name("Medical_Inquiry_vod__c")).getText();
			if(inqID.equals(medInqID))
			{
				report.updateTestLog("Medical Inquiry Fulfillment","Medical inquiry record opened",Status.PASS);
				String mifStatus = cf.getData(By.name("Status_vod__c"), "Status", "text");
				dataTable.putData("MedInq", "MIF Status", mifStatus);
				System.out.println("Status of the fulfillment "+mifStatus);				
				
			}
			else
				report.updateTestLog("Medical Inquiry Fulfillment","Wrong medical inquiry record",Status.FAIL);
		} 
		else 
			report.updateTestLog("Medical Inquiry Fulfillment","Medical Inquiry Fulfillment record not found as the latest one",Status.FAIL);
	}
	
	/**
	 * This method is for creating the response for the  medical inquiry fulfillment created by sales or MSL in the medical inquiry fulfillment 
	 * @throws Exception
	 */
	public void createResponse() throws Exception
	{
		//String mifStatus = dataTable.getData("MedInq", "MIF Status");
		String product = dataTable.getData("MedInq", "Product");
		String accType = dataTable.getData("MedInq", "Account Record Type");
		String response = (new Date()).toString()+":\n Medical Inquiry is fulfilled by DxL"; 
		String mifID = dataTable.getData("MedInq", "MIF ID") ;
		//create response button is clicked				
		cf.clickSFButton("Create Response");
		cf.waitForSeconds(5);
		//fill in the response
		if(cf.isElementVisible(By.xpath("//h2[text()='New Medical Inquiry Fulfillment']"), "New Medical Inquiry Fulfillment"))
		{
			String pd = driver.findElement(By.xpath("//table[@name='mifTable']//div//b")).getText().trim();
			if(pd.contains(product))
				report.updateTestLog("MIF - Product","Product is "+pd,Status.DONE);
			else
				report.updateTestLog("MIF - Product","Product is not "+pd,Status.FAIL);
			
			String delvMethodChk = driver.findElement(By.name("Delivery_Method_vod__c")).getText();
			if(accType.equals("HCP"))
			{
				Select delvchannel = new Select(driver.findElement(By.xpath("//select[@id='Delivery_Channel_AZ__c']")));
				delvchannel.selectByVisibleText("Email");
				String del = delvchannel.getFirstSelectedOption().getText();
				if(delvMethodChk.contains(del))
					report.updateTestLog("Delivery channel","Delivery channel is email",Status.DONE);
				else 
					report.updateTestLog("Delivery channel","Delivery channel is not email",Status.FAIL);
			}
			if(accType.equals("HCA"))
			{
				Select delvchannel = new Select(driver.findElement(By.xpath("//select[@id='Delivery_Channel_AZ__c']")));
				delvchannel.selectByVisibleText("Phone");
				String del = delvchannel.getFirstSelectedOption().getText();
				if(delvMethodChk.contains(del))
					report.updateTestLog("Delivery channel","Delivery channel is phone",Status.DONE);
				else 
					report.updateTestLog("Delivery channel","Delivery channel is not phone",Status.FAIL);
			}
			cf.setData(By.id("Fulfillment_Response_vod__c"), "Fulfillment Response", response);		
			report.updateTestLog("New Medical Inquiry Fulfillment", "New Medical Inquiry Fulfillment response page loaded", Status.SCREENSHOT, "Full Screen", driver);
			cf.clickSFButton("Save");
			cf.waitForSeconds(3);
			report.updateTestLog("Medical Inquiry Fulfillment","Medical Inquiry Fulfillment is saved",Status.SCREENSHOT);
		}
		else
			report.updateTestLog("New Medical Inquiry Fulfillment","New Medical Inquiry Fulfillment page is not loaded",Status.FAIL);
		
		cf.clickSFButton("Edit");
		cf.waitForSeconds(4);
		cf.clickSFButton("Submit");
		String statusMI = driver.findElement(By.xpath("//*[.='"+mifID+"']//ancestor::div[@class='detail-column ng-scope']/following-sibling::div/div[@class='dataCol']//span[@name='Status_vod__c']")).getText();
		if(statusMI.equals("Completed"))
			report.updateTestLog("New Medical Inquiry Fulfillment","New Medical Inquiry Fulfillment is completed",Status.PASS);
		else
			report.updateTestLog("New Medical Inquiry Fulfillment","New Medical Inquiry Fulfillment is not in completed status",Status.FAIL);
		
	}
	
}// End of class
	