package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;

import com.mfa.framework.Status;

import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Veeva_US_LegacyCyclePlan extends ReusableLibrary{

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public Veeva_US_LegacyCyclePlan(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Navigate to Call Plans Tab.
	 * @throws Exception
	 */
	public void navigateToCallPlansTab() throws Exception{
		cf.clickButton(objHeaderPage.plussymbol, "Plus Symbol");
		cf.clickButton(By.cssSelector("img[title*='Call Plans']"), "Call Plans");
		if(cf.isElementVisible(By.xpath("//h3[contains(text(),'Recent Call Plans')]"), "Call Plan")){
			report.updateTestLog("Verify able to navigate to Call Plan Tab", "Able to navigate to Call Plan Tab.", Status.PASS);
			cf.clickButton(By.xpath("//table[@class='list']//tr[2]/th/a"), "Cycle Plan");
			cf.waitForSeconds(15);
			cf.switchToFrame(By.id("vod_iframe"));
			if(cf.isElementVisible(By.xpath("//h2[contains(text(),'Call Plan')]"), "Call Plan"))
				report.updateTestLog("Verify able to navigate to Call Plan Page", "Able to navigate to Call Plan Page.", Status.PASS);
			else
				report.updateTestLog("Verify able to navigate to Call Plan Page", "Able to navigate to Call Plan Page.", Status.FAIL);	
		}
		else
			report.updateTestLog("Verify able to naviagte to Call Plan tab", "Unable to navigate to Call Plan tab.", Status.FAIL);
	}

	/**
	 * Validate Column Name's under Call Plan Target.
	 * @throws Exception
	 */
	public void callPlanTarget() throws Exception{
		Boolean value1=false;
		List<String> callplanTarget = new ArrayList<String>(Arrays.asList("Target","Last Name","City","My Last Call Date","Sales Direction 1","SD1 A","Sales Direction 2","SD2 A"));
		new ArrayList<String>();
		//int appcallPlanTargetsize=driver.findElements(By.xpath("//h3[.='Call Plan Target']/ancestor::div[@class='pbHeader']/following-sibling::div[@class='pbBody']//tbody/tr[1]//th")).size();
		for(int i=1;i<callplanTarget.size();i++){
			String value=cf.getData(By.xpath("//h3[.='Call Plan Target']/ancestor::div[@class='pbHeader']/following-sibling::div[@class='pbBody']//tbody/tr[1]//th["+(i+2)+"]"), "Column Name", "text");
			if(value.trim().equals(callplanTarget.get(i-1).trim()))
				value1=true;
		}
		if(value1 == true)
			report.updateTestLog("Veify able to see proper Column Names Under Call Plan Target.", "Able to see proper Column Names under Call Target Plan.", Status.PASS);
		else
			report.updateTestLog("Veify able to see proper Column Names Under Call Plan Target.", "Unable to see proper Column Names under Call Target Plan.", Status.FAIL);
	}

	/**
	 * Validate the Column Name's under Information Table.
	 * @throws Exception 
	 */
	public void informationTable() throws Exception{

		cf.verifySFElementData("Information", "Name", "labeltext", "text", "Name");
		//cf.verifySFElementData("Information", "Owner", "labeltext", "text", "Owner");
		cf.verifySFElementData("Information", "Start Date", "labeltext", "text", "Start Date");
		cf.verifySFElementData("Information", "Territory", "labeltext", "text", "Territory");
		cf.verifySFElementData("Information", "End Date", "labeltext", "text", "End Date");
		cf.verifySFElementData("Information", "Active", "labeltext", "text", "Active");
		cf.verifySFElementData("Information", "Assigned Calls", "labeltext", "text", "Assigned Calls");
		cf.verifySFElementData("Information", "Completed Calls", "labeltext", "text", "Completed Calls");
		cf.verifySFElementData("Information", "Remaining", "labeltext", "text", "Remaining");

	}



}
