package businesscomponents;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import supportlibraries.*;
import com.mfa.framework.Status;
//import com.itextpdf.text.log.SysoCounter;
import ObjectRepository.*;
//import org.openqa.jetty.html.Select;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Veeva_MedicalInsight extends ReusableLibrary
{

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	Random random1 = new Random();
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_MedicalInsight(ScriptHelper scriptHelper)
	{
		super(scriptHelper);

	}

	/*public void openVeevaApp() throws Exception
	{

		String strSalesForceUrl = properties.getProperty("VeevaAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		driver.get(strSalesForceUrl);

		if (driver.findElement(ObjMedIns.loginUserName).isDisplayed())
		{
			report.updateTestLog("Verify Veeva Login page is opened", "Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is opened", Status.PASS);
		}
		else
		{
			report.updateTestLog("Verify Veeva Login page is opened", "Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is not opened", Status.FAIL);
			frameworkParameters.setStopExecution(true);
		}
	}*/

	public void accountSearch()
	{

		String accountName = dataTable.getData("Login", "AccountName");
		String locale = dataTable.getData("Login", "Locale");

		if (cf.isElementPresent(ObjMedIns.searchPanel, "Search Panel is seen"))
		{

			if (!locale.equals("CN"))
			{
				Select searchAlldrpdwn = new Select(driver.findElement(ObjMedIns.searchAll));
				searchAlldrpdwn.selectByValue("001");
			}

			try
			{
				cf.clickifElementPresent(ObjMedIns.searchBox, "Search Box selected");
				cf.setData(ObjMedIns.searchBox, "Account name entered", accountName);
				if (!locale.equals("CN"))
					cf.clickButton(ObjMedIns.go, "Go");
				else
					cf.clickLink(ObjMedIns.goSelect, "Account");
				//autocompleteMatch
			}
			catch (Exception e)
			{
				System.out.println("Search box not present");
				report.updateTestLog("Search box not present", "Account cannot be searched", Status.FAIL);
			}
			cf.waitForSeconds(4);
			report.updateTestLog("Account is fetched", "Account details page is displayed", Status.SCREENSHOT);

		}

		else
		{
			report.updateTestLog("Search Panel present", "Search Panel not seen", Status.SCREENSHOT);
			// go to My Accounts and search via list
		}
	}

	public void accountDetails()
	{
		/*Verify the New Key Medical Insight button is present and navigate to Medical Insight page */
		String locale = dataTable.getData("Login","Locale");
		
		//Updated by Murali on 29/03/2019 - Added New Medical insight button in else loop and added or condition
		
		if((locale.equalsIgnoreCase("IE")) || (locale.equalsIgnoreCase("RU")) || (locale.equalsIgnoreCase("BR")) || (locale.equalsIgnoreCase("IC"))|| (locale.equalsIgnoreCase("IW")))
		{
			if(cf.isElementPresent(By.xpath("//td[@id='topButtonRow']/input[@value='New Key Medical Insight']"), "New Key Medical Insight"))
			{
				report.updateTestLog("New Key Medical Insight", "New Key Medical Insight button is clicked",Status.DONE);
				try {
					cf.clickButton(ObjMedIns.newKeyMedicalInsight_button, "New Key Medical Insight");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Problem in clicking New Key medical Insight");
				}
		
				cf.waitForSeconds(4);
				report.updateTestLog("New Key Medical Insight details page", "New Key Medical Insight page is loaded",Status.PASS);
			}
			else{
				report.updateTestLog("New Key Medical Insight", "New Key Medical Insight button is not present or page notloaded",Status.FAIL);
			}
		}
		else
		{
			if(cf.isElementPresent(ObjMedIns.newKeyMedicalInsight_button_new, "New Key Medical/Dx Insight"))
			{
				report.updateTestLog("New Key Medical Insight", "New Key Medical Insight button is clicked",Status.DONE);
				try {
					cf.clickButton(ObjMedIns.newKeyMedicalInsight_button_new, "New Key Medical Insight");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("Problem in clicking New Key medical Insight");
				}
		
				cf.waitForSeconds(4);
				report.updateTestLog("New Key Medical Insight details page", "New Key Medical Insight page is loaded",Status.PASS);
			}
			else{
				report.updateTestLog("New Key Medical Insight", "New Key Medical Insight button is not present or page notloaded",Status.FAIL);
			}
		}
		
		
	}

	private LocalDateTime dtTime = LocalDateTime.now();

	//DateTimeFormatter formattedDate = DateTimeFormatter.ofPattern("MM/dd/YYYY");
	//String newDate = dtTime.format(formattedDate);	
	public String dateFormat() throws Exception
	{
		String newDate = "";
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		switch (locale)
		{
			case "EU2" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd.MM.YYYY"));
				break;
			case "RU" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd.MM.YYYY"));
				break;
			case "EU1" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
				break;
			case "BR" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
				break;
			case "CA" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
				break;
			case "IE" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("YYYY/MM/dd"));
				break;
			case "JP" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("YYYY/MM/dd"));
				break;
			case "CN" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("YYYY/M/d"));
				break;
			case "ANZ" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("d/MM/YYYY"));
				break;
			case "IC" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
				break;
			case "IW" :
				newDate = dtTime.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
				break;
			default :
				System.out.println("Country not mentioned");
				break;
		}

		return newDate;

	}

	boolean isAttribtuePresent(WebElement element, String attribute)
	{
		Boolean result = false;
		try
		{
			String value = element.getAttribute(attribute);
			if (value != null)
			{
				result = true;
				System.out.println("the field is mandatory" + value);
			}
			else
				System.out.println("Not a mandatory field");
		}
		catch (Exception e)
		{
		}
		return result;
	}

	public void isMandatory(By by, String elementName)
	{
		Boolean mandatory = isAttribtuePresent(driver.findElement(by), "required");
		if (mandatory == false)
			report.updateTestLog(elementName, elementName + " is not mandatory field", Status.WARNING);
		else
			report.updateTestLog(elementName, elementName + " is a mandatory field", Status.DONE);

	}

	public static String medData = "inout";

	public void newMedicalInsight() throws Exception
	{

		/*capture date and time */
		String dateofMedIns = driver.findElement(ObjMedIns.dateofinsight).getAttribute("value");//date from application
		// Get system date and compare
		System.out.println("This is the application date " + dateofMedIns);
		System.out.println("This is the system date " + dtTime);
		//Change the format of system date to Location specific format and compare
		String dt = dateFormat();
		System.out.println(dt);
		if (dateofMedIns.equals(dt))
			report.updateTestLog("Date", "Today's date " + dt + " is auto populated correctly", Status.DONE);
		else
			report.updateTestLog("Date", "Today's date populated is incorrect", Status.FAIL);

		/*Check the account name*/
		String accountName = dataTable.getData("Login", "AccountName");
		String account_name = driver.findElement(ObjMedIns.account).getAttribute("value");
		if (accountName.contains(account_name.substring(0, 2))) //TODO - checks the substring 
			report.updateTestLog("Account name", "Account name " + account_name + " is auto populated correct", Status.DONE);
		else
			report.updateTestLog("Account name", "Account name " + account_name + " is not populated correct", Status.FAIL);

		isMandatory(ObjMedIns.therapauticAreaDrpdwn, "Therapy Area");
		Select therapyArea = new Select(driver.findElement(ObjMedIns.therapauticAreaDrpdwn));
		List<WebElement> therapyAreaElements = new Select(driver.findElement(ObjMedIns.therapauticAreaDrpdwn)).getOptions();
		ArrayList<String> therapyAreaList = new ArrayList<String>();
		for (int ta = 1; ta < therapyAreaElements.size(); ta++)
		{
			String therapy = therapyAreaElements.get(ta).getText();
			therapyAreaList.add(therapy);
		}
		
		Random random = new Random();
		int r = random.nextInt(therapyAreaList.size() - 0) + 0;
		therapyArea.selectByVisibleText(therapyAreaList.get(r));
		String tA = therapyArea.getFirstSelectedOption().getText();
		dataTable.putData(medData, "Therapeutic Area", tA);
		System.out.println(tA);

		// Based on the therapy area selected the disease and indication field is populated	
		WebElement disease_indication = driver.findElement(ObjMedIns.disnind_drpdwn);
		isMandatory(ObjMedIns.therapauticAreaDrpdwn, "Disease and indication");

		if (disease_indication.isEnabled())
		{
			List<WebElement> diseaseElements = new Select(disease_indication).getOptions();
			ArrayList<String> diseaseList = new ArrayList<String>();

			for (int d = 1; d < diseaseElements.size(); d++)
			{
				String dis = diseaseElements.get(d).getText();
				diseaseList.add(dis);
			}
			Select disease = new Select(driver.findElement(ObjMedIns.disnind_drpdwn));
			Random random1 = new Random();
			int ra = random1.nextInt(diseaseList.size() - 0) + 0;
			disease.selectByVisibleText(diseaseList.get(ra));
			String d = disease.getFirstSelectedOption().getText();
			System.out.println(d);
			dataTable.putData(medData, "Disease and Indication", d);
		}

		else
		{
			System.out.println("no disease and indication");
			dataTable.putData(medData, "Disease and Indication", "");
			report.updateTestLog("Disease and Indication field is disabled", "Disease and Indication field is not applicable to this therapy area", Status.DONE);
		}

		report.updateTestLog("Disease and Indication field", "Disease and Indication field entered", Status.DONE);

		//Based on the therapy area selected, Scientific Insight Topic field is populated
		isMandatory(ObjMedIns.insightTopic_drpDwn, "Scientific Insight Topic");
		Select insightTopic = new Select(driver.findElement(ObjMedIns.insightTopic_drpDwn));
		if (tA.equals("Metabolics") || tA.equals("Oncology") || tA.equals("Respiratory") || tA.equals("Cardiovascular") || tA.equals("Renal") || tA.equals("Non-TA"))
		{
			List<WebElement> topicElements = new Select(driver.findElement(ObjMedIns.insightTopic_drpDwn)).getOptions();
			ArrayList<String> topicList = new ArrayList<String>();
			for (int t = 1; t < topicElements.size(); t++)
			{
				String topics = topicElements.get(t).getText();
				topicList.add(topics);
				System.out.println(topics);
			}

			Random random2 = new Random();
			int ran = random2.nextInt(topicList.size() - 0) + 0;
			insightTopic.selectByVisibleText(topicList.get(ran));

			String iT = insightTopic.getFirstSelectedOption().getText();
			dataTable.putData(medData, "Scientific Insight Topic", iT);
		}

		else // By default Other is displayed or selected
		{
			String iT = insightTopic.getFirstSelectedOption().getText();
			dataTable.putData(medData, "Scientific Insight Topic", iT);

		}
		report.updateTestLog("Scientific Insight Topic", "Scientific Insight topic is entered", Status.DONE);

		//Medical Insight Summary	
		isMandatory(ObjMedIns.medInsightSummary_text, "Medical Insight Summary");
		try
		{
			String summary = "Medical Insight Summary";
			cf.setData(ObjMedIns.medInsightSummary_text, "Medical Insight Summary", summary);
			dataTable.putData(medData, "Medical Insight Summary", summary);
			report.updateTestLog("Medical Insight Summary", "Text is entered", Status.DONE);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			System.out.println("Problem with Medical Insight Summary field");
		}

		//Medical Insight Description
		isMandatory(ObjMedIns.medInsightDesc_text, "Medical Insight Description");
		try
		{
			String description = "Description about medical insight";
			cf.setData(ObjMedIns.medInsightDesc_text, "Medical Insight Description", description);
			dataTable.putData(medData, "Medical Insight Description", description);
			report.updateTestLog("Medical Insight Description", "Description is entered", Status.DONE);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			System.out.println("Problem with Medical Insight Description field");
		}

		/*Core 29 - As part of this release this field is removed*/
		/*Insight Category, Sub category and Venue
		isMandatory(ObjMedIns.insightCategory_Drpdwn,"Insight Category");
		Select insightCategory = new Select(driver.findElement(ObjMedIns.insightCategory_Drpdwn));
		String cat = dataTable.getData(medData,"Insight Category");
		insightCategory.selectByVisibleText(cat); // Give any value from the drop down
		
		isMandatory(ObjMedIns.insightSubCategory_Drpdwn,"Insight Sub Category");	
		Select insightSubCategory = new Select(driver.findElement(ObjMedIns.insightSubCategory_Drpdwn));
		insightSubCategory.selectByIndex(2);
		String subCat = insightSubCategory.getFirstSelectedOption().getText();
		dataTable.putData(medData, "Insight Sub-Category", subCat); */

		//Venue
		isMandatory(ObjMedIns.venue_DrpDwn, "Venue");
		Select venue = new Select(driver.findElement(ObjMedIns.venue_DrpDwn));
		List<WebElement> venueElements = new Select(driver.findElement(ObjMedIns.venue_DrpDwn)).getOptions();
		ArrayList<String> venueList = new ArrayList<String>();
		for (int v = 1; v < venueElements.size(); v++)
		{
			String ven = venueElements.get(v).getText();
			venueList.add(ven);
			System.out.println(ven);
		}

		Random random3 = new Random();
		int rand = random3.nextInt(venueList.size() - 0) + 0;
		venue.selectByVisibleText(venueList.get(rand));
		String v = venue.getFirstSelectedOption().getText();
		System.out.println(v);
		dataTable.putData(medData, "Venue", v);
		report.updateTestLog("Venue is entered", "Insight Category, Sub category and Venue are entered", Status.DONE);
		report.updateTestLog("Mandatory fields", "All Mandatory fields are entered", Status.SCREENSHOT);

		/*venue.selectByVisibleText("Congress");	
		String v = venue.getFirstSelectedOption().getText();
		System.out.println(v);	
		dataTable.putData(medData, "Venue", v);*/

		if (v.equals("Congress"))
		{
			try
			{
				if (cf.isElementVisible(ObjMedIns.medicalEvent_Txt, "Medical Event"))
				{
					if (cf.isElementVisible(ObjMedIns.medicalEvent_search, "Medical Event Lookup"))
					{
						cf.clickButton(ObjMedIns.medicalEvent_search, "Medical Event Lookup");
						cf.waitForSeconds(6);
						if (cf.isElementVisible(ObjMedIns.medicalEvent_searchPage, "Search Medical Event Look-up"))
						{
							WebElement medeve = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody//tr/td[1]/a"));
							if (cf.isElementVisible(medeve))
								medeve.click();
							else
							{
								report.updateTestLog("Medical Event", "No Medical Event", Status.WARNING);
							}
						}

					}
				}
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Medical Event look up error");
			}
		}
		//if no medical event is available for Congress then select a different venue
		else
		{
			venue.selectByVisibleText("AZ Meeting");
			String vv = venue.getFirstSelectedOption().getText();
			System.out.println(vv);
			dataTable.putData(medData, "Venue", vv);
		}

		//Patient Safety Attestation
		isMandatory(ObjMedIns.attest_DrpDwn, "Patient Safety Attestation");
		Select attest = new Select(driver.findElement(ObjMedIns.attest_DrpDwn));
		List<WebElement> attestElements = new Select(driver.findElement(ObjMedIns.attest_DrpDwn)).getOptions();
		ArrayList<String> attestList = new ArrayList<String>();
		for (int a = 1; a < attestElements.size(); a++)
		{
			String att = attestElements.get(a).getText();
			attestList.add(att);
			System.out.println(att);
		}

		Random random4 = new Random();
		int rando = random4.nextInt(attestList.size() - 0) + 0;
		attest.selectByVisibleText(attestList.get(rando));
		String attestation = attest.getFirstSelectedOption().getText();
		System.out.println(attestation);
		dataTable.putData(medData, "Insight Patient Safety Attestation", attestation);
		report.updateTestLog("Venue is entered", "Insight Category, Sub category and Venue are entered", Status.DONE);

		/*Core 28 changes VRM 5702 - Product to medical insight is mandatory; detail group or topic is not allowed*/
		cf.scrollToTop();
		// To the product field
		String locale = dataTable.getData("Login", "Locale");
		if (!((locale.equals("RU")) || locale.equals("ANZ")))
		{
			try
			{
				By productField = By.id("MA_Product_AZ__c");
				if (cf.isElementVisible(productField, "Product field"))
				{
					cf.clickButton(By.xpath("//a[@title='Product Lookup']"), "Product look up");
					cf.waitForSeconds(15);
					String y = "Detail Topic";
					String z = "Detail Group";
					boolean p = false;
					WebElement pdtList = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody"));
					List<WebElement> pdt_tr = pdtList.findElements(By.tagName("tr"));
					int pdtLookup_Size = pdt_tr.size();
					report.updateTestLog("Product look up search", "Product search look-up is oepned", Status.SCREENSHOT);
					System.out.println(pdtLookup_Size);// no.of rows - tr
					for (int i = 2; i <= pdtLookup_Size; i++)
					{
						WebElement td = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr[" + i + "]/td[3]/span"));
						String pdtType = td.getText();
						//System.out.println(pdtType);

						if ((pdtType.equals(y) || pdtType.equals(z)))
						{
							//select the detail topic
							cf.clickElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr[" + i + "]/td[1]/a"), "Product Name Link");
							cf.waitForSeconds(3);
							cf.clickButton(ObjMedIns.save_Btn, "Save");
							//error message is generated
							cf.scrollToTop();
							String error = "Please select a valid AZ product";
							String erorMsg = driver.findElement(By.xpath("//div[@class='pbBody']//div[contains(@class,'error-span ng-scope')]")).getText();
							if (error.equals(erorMsg))
							{
								report.updateTestLog("Product Fields", "Detail Topic or Detail group are not allowed. Need to select a valid detailed product", Status.PASS);
								p = true;
								break;
							}
							else
								report.updateTestLog("Product Fields", "Incorrect error message is generated", Status.FAIL);
						}

					}

					if (p == false)
					{
						report.updateTestLog("Product Fields", "No Detail group or detail topic is present. Go to selecting a detail product", Status.WARNING);
						cf.clickElement(By.xpath("//a[@class='dialogClose']"), "Close product look-up");
						p = true;
					}

					else
					{
						cf.waitForSeconds(4);
						driver.findElement(productField).clear();
						cf.clickButton(By.xpath("//a[@title='Product Lookup']"), "Product look up");
						cf.waitForSeconds(2);
						String k = "Detail";
						if (locale.equals("BR"))
						{
							By searchPdt = By.id("MA_Product_AZ__c_searchText");
							cf.setData(searchPdt, "Search Product", "Calquence_BR");
							cf.clickButton(By.xpath("//form[@name='lookup_form']//input[@title='Go!']"), "Go!");
							cf.waitForSeconds(3);
							cf.clickElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr[2]/td/a"), "Detail product");
							report.updateTestLog("Product Field", "Detail product is selected", Status.PASS);
						}
						else
						{
							for (int i = 2; i <= pdtLookup_Size; i++)
							{
								WebElement td = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr[" + i + "]/td[3]/span"));
								String pdtType = td.getText();
								//System.out.println(pdtType);
								if (pdtType.equals(k))
								{
									//select the detail product
									cf.clickElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr[" + i + "]/td[1]/a"), "Product Name Link");
									report.updateTestLog("Product Field", "Detail product is selected", Status.PASS);
									break;
								}

							}
						}

					}
				}

				else
					report.updateTestLog("Product Field", "Product field is not present", Status.FAIL);
			}
			catch (Exception e)
			{
				System.out.println("Product field is not seen");
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		report.updateTestLog("Medical Insights", "All Mandatory fields entered", Status.SCREENSHOT, "Full Screen", driver);
	}

	public void save_MedicalInsight()
	{
		/*Save and validate save page*/
		if (cf.isElementPresent(ObjMedIns.save_Btn, "Save button"))
		{
			try
			{
				cf.clickButton(ObjMedIns.save_Btn, "Save");
				cf.waitForSeconds(10);
			}
			catch (Exception e)
			{
				System.out.println("Problem in saving the medical insight");
			}

			report.updateTestLog("Saved", "Medical insight is saved", Status.DONE);
		}
		else
		{
			report.updateTestLog("Saved", "Problem in saving the medical insight", Status.FAIL);
			//frameworkParameters.setStopExecution(true);
		}

		//Saved page is loaded
		//verify status, medical affairs and country code
		if (cf.isElementPresent(ObjMedIns.save_Status, "Status field"))
		{
			String saved_status = driver.findElement(ObjMedIns.saved_StatusValue).getText();
			if (saved_status.equals("Saved"))
			{
				report.updateTestLog("Status field", "Status appear as Saved", Status.PASS);
			}
			else
				report.updateTestLog("Status field field", "Status is not correct", Status.FAIL);
		}

		else
			report.updateTestLog("Status", "Status not correct", Status.FAIL);

		if (cf.isElementPresent(ObjMedIns.med_aff))
		{
			String affairs = driver.findElement(By.name("MA_Medical_Affairs4_AZ__c")).getText();
			if (affairs.equals("Yes"))
				report.updateTestLog("Medical Affairs", "Medical Affairs field is YES", Status.DONE);
			else
				report.updateTestLog("Medical Affairs", "Medical Affairs field is not marked", Status.FAIL);
		}
		else
			report.updateTestLog("Medical Affairs", "Medical Affairs field is not seen", Status.FAIL);

		String strDataSheet = "Login";
		String cC = dataTable.getData(strDataSheet, "Country code");

		if (cf.isElementPresent(ObjMedIns.country_code))
		{
			String country_code = driver.findElement(By.name("MA_Country_Code_AZ__c")).getText();
			if (cC.equals(country_code))
				report.updateTestLog("Country code", "Country code is " + country_code, Status.DONE);
			else
				report.updateTestLog("Country code", "Country code is blank or incorrect ", Status.FAIL);
		}
		else
			report.updateTestLog("Country code", "Country code field is not present ", Status.FAIL);

		//Verify the values of Therapy area and disease and indication fields
		valuescheck();

	}

	public void valuescheck()
	{
		String thArea = dataTable.getData(medData, "Therapeutic Area");
		String disInd = dataTable.getData(medData, "Disease and Indication");
		String insTopic = dataTable.getData(medData, "Scientific Insight Topic");
		String medSummary = dataTable.getData(medData, "Medical Insight Summary");
		String medDesc = dataTable.getData(medData, "Medical Insight Description");
		/*Core 29 - As part of this release this field is removed*/
		//String insCat = dataTable.getData(medData, "Insight Category");
		//String insSubCat = dataTable.getData(medData, "Insight Sub-Category");
		String ven = dataTable.getData(medData, "Venue");
		String patAttest = dataTable.getData(medData, "Insight Patient Safety Attestation");

		String save_TherapyArea = driver.findElement(ObjMedIns.save_TherapyArea).getText();
		String save_DisInd = driver.findElement(ObjMedIns.save_DisInd).getText();
		String save_Topic = driver.findElement(ObjMedIns.save_Topic).getText();
		String save_Summary = driver.findElement(ObjMedIns.save_Summary).getText();
		String save_Desc = driver.findElement(ObjMedIns.save_Desc).getText();

		/*Core 29 - As part of this release this field is removed*/
		/*String submit_Cat = driver.findElement(ObjMedIns.submit_Cat).getText();
		String submit_SubCat = driver.findElement(ObjMedIns.submit_SubCat).getText();*/

		String submit_Venue = driver.findElement(ObjMedIns.submit_Venue).getText();
		String submit_attest = driver.findElement(ObjMedIns.submit_attest).getText();

		if ((thArea.equals(save_TherapyArea)) && (disInd.equals(save_DisInd)) && (insTopic.equals(save_Topic)) && (medSummary.equals(save_Summary)) && (medDesc.equals(save_Desc)) && (ven.equals(submit_Venue)) && (patAttest.equals(submit_attest)))
			report.updateTestLog("Values entered are Saved", "Therapy area, Disease and indication field, insight topic, summary and description, venue and attestation are correct in saved page", Status.DONE);
		else
			report.updateTestLog("Values entered are not Saved", "Therapy area, Disease and indication field, insight topic, summary and description, venue and attestation are incorrect in saved page", Status.FAIL);

	}

	public void submit_MedicalInsight()
	{
		//Get the medical insight ID on the Submitted page and write in excel
		String medInsight_ID = "";
		medInsight_ID = driver.findElement(ObjMedIns.medicalInsightID).getText();
		dataTable.putData("MedicalInsight", "Generated Medical Insight ID", medInsight_ID);
		System.out.println("The generated Medical Insight number is " + medInsight_ID);

		/*Submit button is clicked and the submitted page is validated*/
		if (cf.isElementPresent(ObjMedIns.submit_Btn, "Submit"))
		{
			try
			{
				cf.clickButton(ObjMedIns.submit_Btn, "Submit");
				cf.waitForSeconds(10);
				
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				System.out.println("Problem with Submit button");
			}
			report.updateTestLog("Submit button is clicked", "Submitted medical insight", Status.DONE);
		}

		else
		{
			report.updateTestLog("Submit button is not present", "Submitted medical insight", Status.FAIL);
			//frameworkParameters.setStopExecution(true);
		}

		//validate status
		if (cf.isElementPresent(ObjMedIns.submit_Status))
		{
			String submit_status = driver.findElement(By.name("Status_vod__c")).getText();
			if (submit_status.equals("Submitted"))
			{
				report.updateTestLog("Submit status", "Status is submitted", Status.PASS);
			}
			else
				report.updateTestLog("Submit status", "Status is not submitted", Status.FAIL);
		}
		else
		{
			report.updateTestLog("Submit status", "Status field is not seen", Status.FAIL);
			//frameworkParameters.setStopExecution(true);
		}

		//check the remaining values entered
		valuescheck();

		//click the account link
		if (cf.isElementPresent(ObjMedIns.submit_Account, "Account name"))
		{

			try
			{
				cf.clickLink(ObjMedIns.submit_AccountLink, "Account name");
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				System.out.println("Account name link is problem");
			}
			cf.waitForSeconds(5);
			report.updateTestLog("Account name link is clicked", "Account details page is loaded", Status.PASS);
		}

		toAccount_relatedListCheck(medInsight_ID); //capture the medical insight ID and compare with the related list in Account Details

	}

	public void toAccount_relatedListCheck(String medInsight_ID)
	{
		//Account details page
		if (cf.isElementPresent(ObjMedIns.medIns_relatedListLink, "Medical Insight related list link"))
		{
			report.updateTestLog("Medical Insight link is clicked", "Takes to medical Insight related list", Status.DONE);
			try
			{
				cf.clickLink(ObjMedIns.medIns_relatedListLink, "Medical Insight related list link");
				cf.waitForSeconds(2);
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try
			{
				cf.clickElement(ObjMedIns.medIns_relatedList, "Medical Insight related List");
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Medical Insight related list is not seen");
			}
			//	report.updateTestLog("Medical Insight link not present", "scroll to the medical insight related list",Status.SCREENSHOT);
		}
		report.updateTestLog("Medical Insight link clicked", "medical Insight related list is seen", Status.SCREENSHOT);

		WebElement relList = driver.findElement(By.xpath("//h3[text()='Medical/Dx Insights']//following::div/table/tbody"));

		List<WebElement> list_tr = relList.findElements(By.tagName("tr"));
		int x = list_tr.size();
		System.out.println("No. of rows in the related list " + x);
		boolean b = false;
		System.out.println("To check the " + medInsight_ID + " is present in the related list");
		for (int i = 2; i <= x; i++)
		{
			String insightID = driver.findElement(By.xpath("//h3[text()='Medical/Dx Insights']//following::div/table/tbody/tr[" + i + "]//th/a")).getText();//from related list
			String a = dataTable.getData("MedicalInsight", "Generated Medical Insight ID");
			if (insightID.equals(a))
			{
				System.out.println("Medical insight is present in the row = " + i);
				report.updateTestLog("Medical Insight record", "The medical insight record is present", Status.PASS);
				String insightSummary = driver.findElement(By.xpath("//h3[contains(text(),'Medical/Dx Insights')]//following::div/table/tbody/tr[" + i + "]//td[2]")).getText();
				String insightDescription = driver.findElement(By.xpath("//h3[contains(text(),'Medical/Dx Insights')]//following::div/table/tbody/tr[" + i + "]//td[3]")).getText();
				String summ = dataTable.getData("inout", "Medical Insight Summary");
				String desc = dataTable.getData("inout", "Medical Insight Description");
				if ((insightSummary.equals(summ)) && (insightDescription.equals(desc)))
				{
					report.updateTestLog("Medical Insight record", "The medical insight record is present", Status.PASS);
					report.updateTestLog("Medical Insight record", "The medical insight summary and description are present in the related list", Status.DONE);
					b = true;//record present in the account detail page
					break;

				}

			}

		}
		if (b == false)//record not present in the account detail page related list view - expand the related list
		{
			//expand the related list view; click go to link
			Boolean m = false;
			try
			{
				if (cf.isElementVisible(ObjMedIns.goToLink, "Go To - expanded related list view"))
				{
					cf.clickLink(ObjMedIns.goToLink, "Go To - expanded related list view");
					cf.waitForSeconds(5);
					report.updateTestLog("Go to link is clicked", "The medical insight records expanded view is displayed", Status.DONE);

					WebElement relListExpanded = driver.findElement(By.xpath("//div[@class='listRelatedObject customnotabBlock']//div[@class='pbBody']/table/tbody"));
					List<WebElement> listExpanded = relListExpanded.findElements(By.tagName("tr"));
					int q = listExpanded.size();
					System.out.println("No. of rows in the related list " + q);
					//search the insight id in the table 
					for (int i = q; i > q - 1; i--)
					{
						String insightID = driver.findElement(By.xpath("//div/table/tbody/tr[" + i + "]//th/a")).getText();
						String a = dataTable.getData("MedicalInsight", "Generated Medical Insight ID");
						if (insightID.equals(a))
						{
							String insightSummary = driver.findElement(By.xpath("//div/table/tbody/tr[" + i + "]//td[2]")).getText();
							String insightDescription = driver.findElement(By.xpath("//div/table/tbody/tr[" + i + "]//td[3]")).getText();
							String summ = dataTable.getData("inout", "Medical Insight Summary");
							String desc = dataTable.getData("inout", "Medical Insight Description");
							if ((insightSummary.equals(summ)) && (insightDescription.equals(desc)))
							{
								b = true;//record present in the account detail page
								System.out.println("Medical insight is present in the row = " + i);
								report.updateTestLog("Medical Insight record", "The medical insight record is present", Status.PASS);
								report.updateTestLog("Medical Insight record", "The medical insight summary and description are present in the related list", Status.DONE);
							}

							m = true;
							break;
						}

					}

					if (m == false)
						report.updateTestLog("Medical Insight record", "The medical insight record is not present", Status.FAIL);

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Go to link of the expanded related list view is not working: " + e);
			}

		}

	}

	public void logOut() throws Exception
	{

		if (cf.isElementVisible(ObjMedIns.logOutUser, "Logout"))
		{

			driver.findElement(ObjMedIns.logOutUser).click();
			cf.clickLink(ObjMedIns.logOutLink, "Logout");
			if (cf.isElementVisible(ObjMedIns.loginUserName, "Login form"))
				report.updateTestLog("Verify succesfully looged out or not", "Successfully logged Out", Status.PASS);
			else
			{
				report.updateTestLog("Verify succesfully looged out or not", "Successfully not logged Out", Status.FAIL);
				//frameworkParameters.setStopExecution(true);
			}
		}
	}
}//End of class