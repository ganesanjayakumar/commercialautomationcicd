package businesscomponents;

import java.util.Date;
import org.openqa.selenium.By;
import com.mfa.framework.Status;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Veeva_US_MIR extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public Veeva_US_MIR(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}


	/**
	 * This method is used for navigating to Medical Information Request linklet
	 * @author kzmg439
	 */
	public void createMedicalInformationRequestUS() throws Exception{
		Veeva_CallsStandardizationMedical vcsm = new Veeva_CallsStandardizationMedical(scriptHelper);
		vcsm.createMedicalInformationRequest("US");
	}

	/**
	 * Saving the values required in to Excel Sheet and Validating the values in Related List.
	 * @throws Exception
	 */
	public void saveandrelatedlistMIRvalidation() throws Exception{
		String status=cf.getSFData("Status", "text", "text");
		if(status.trim().equals("Saved")){
			report.updateTestLog("Verify able to see status as Saved", "Able to see status as Saved", Status.PASS);
			dataTable.putData("MedicalInformationRequest", "Status", status);
			String createdDate=driver.findElement(By.name("CreatedDate")).getText();
			String date[]=createdDate.split(" ");
			dataTable.putData("MedicalInformationRequest", "Created Date", date[0]);
			String MIRID =cf.getSFData("Medical Information Request Id", "text", "text");
			dataTable.putData("MedicalInformationRequest", "MIRID", MIRID);
			String ProductName =cf.getSFData("Product Name", "text", "text");
			dataTable.putData("MedicalInformationRequest", "Product Name", ProductName);
			relatedlistMIRvaliadation();
		}
		else
			report.updateTestLog("Verify able to see status as Saved", "Unable to see status as Saved", Status.FAIL);
	}

	/**
	 * MIR Related List values Validation.
	 * @throws Exception
	 */
	public void relatedlistMIRvaliadation() throws Exception{
		Date dNow = new Date();
		int rows;
		Boolean validate=false;
		cf.clickSFLinkLet("Medical Information Request");
		String mircreatedDate=cf.formatDateTimeMarketWise(dNow, "US", "Commercial", "date");
		String mirProductName=dataTable.getData("MedicalInformationRequest", "Product Name");
		if(cf.isElementVisible(By.xpath("//h3[.='Medical Information Request']"),null)){
			rows=cf.getRowCountFromLinkLetTable("Medical Information Request");
			if(rows>0){
				for(int j=rows+1;j>1;j--){
					String appstatus=cf.getDataFromLinkLetTable("Medical Information Request", "Status", j);
					String appcreatedDate=cf.getDataFromLinkLetTable("Medical Information Request", "Created Date", j);
					String appMIRID=cf.getDataFromLinkLetTable("Medical Information Request", "	Medical Information Request Id", j);
					String appProductName=cf.getDataFromLinkLetTable("Medical Information Request", "Product Name", j);

					if(appstatus.equals("Saved")&& (!appMIRID.equals(" "))){
						//report.updateTestLog("Verify able to see Status and MRI ID", "Able to see proper Status and MRI ID", Status.PASS);
						if(appcreatedDate.equals(mircreatedDate)&& appProductName.equals(mirProductName)){
							//report.updateTestLog("Verify able to see Created Date and Product Name", "Able to see proper Created Date and Product Name", Status.PASS);
							validate=true;
							break;
						}
					}
				}
			}
			else
				report.updateTestLog("Verify able to see targets for the account in related list.", "There are no Survey Targets created in related list.", Status.FAIL);
		}
		if(validate == true)
			report.updateTestLog("Verify able to see Status,Created Date and Product Name properly and MRI ID is not null.", "Able to see Status,Created Date and Product Name properly and MRI ID is not null", Status.PASS);
		else
			report.updateTestLog("Verify able to see Status,Created Date and Product Name properly and MRI ID is not null.", "Unable to see Status,Created Date and Product Name properly and MRI ID is not null", Status.FAIL);	
	}
}//End of Class
