package businesscomponents;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaEmail;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class SalesforceLighting extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public SalesforceLighting(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public void veevaLogin() throws Exception
	{
		String strSalesForceUrl = properties.getProperty("VeevaAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		driver.get(strSalesForceUrl);
		if (cf.isElementVisible(By.id("username"), "login - User Name"))
		{
			report.updateTestLog("Verify Veeva Login page is opened", "Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is opened", Status.PASS);
		}
		else
		{
			throw new FrameworkException("Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is not opened");
		}

		String strLocale = dataTable.getData("Login", "Locale");
		String strCommonDataHeader = "#VeevaLogin_" + (strLocale.trim().toUpperCase()) + "_Rep";// VeevaLogin_IE_SysAdmin
		String strSysAdminUserName = dataTable.getCommonData("SF_UserName", strCommonDataHeader);
		String strSysAdminPassword = dataTable.getCommonData("SF_Password", strCommonDataHeader);
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strSysAdminUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strSysAdminPassword);
		cf.clickElement(ObjVeevaEmail.loginSubmit, "Login Submit");
		
		report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
		if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickElement(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickElement(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}
		
		cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		if (currentUrl.contains("my.salesforce"))
		{
			cf.clickElement(By.xpath("//div[@class='linkElements']/a[@class='switch-to-lightning']"), "Switch-to-lightning");
			cf.waitForSeconds(5);
		}
	}

	public void lighting() throws Exception
	{
		//Not working
		//cf.clickElement(By.xpath("//span[text()='My Schedule']"), "My Schedule");
		//cf.clickElement(By.xpath("//span[@class='photoContainer forceSocialPhoto']//img"), "Profile");
		
		//cf.clickElement(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='Global Account Search']"), "Global Account Search");
		//cf.clickElement(By.xpath("//li[@class='slds-global-actions__item slds-dropdown-trigger slds-dropdown-trigger--click']/span/button/div/span/div/span"), "Profile");
		//cf.clickElement(By.xpath("//div[@class='panel-content scrollable']//a[text()='Log Out']"), "Logout");
		cf.waitForSeconds(10);
		
		
		cf.highLightElement(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='Home']"), "Home");
		cf.clickElement(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='Home']"), "Home");
		
		System.out.println("Home over");
		cf.highLightElement(By.xpath("//span[@title='Open Tasks']"), "Open Tasks");
		System.out.println("open task hghlight over");
		cf.clickElement(By.xpath("//span[@title='Open Tasks']"), "Open Tasks");
		
		if(cf.isElementPresent(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='My Accounts']"))){
			cf.highLightElement(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='Global Account Search']"), "GAS");
			cf.clickElement(By.xpath("//div[@class='slds-context-bar']/one-app-nav-bar/nav/div/one-app-nav-bar-item-root/a/span[text()='Global Account Search']"), "GAS");
			
		}
		else{
			
			cf.highLightElement(By.xpath("span[@class='slds-p-right_small' and contains(text(),'More')]"), "More Tab");
			cf.clickElement(By.xpath("span[@class='slds-p-right_small' and contains(text(),'More')]"), "More");
			cf.waitForSeconds(3);
			
			cf.highLightElement(By.xpath("(//span[contains(text(),'Global Account Search')])[2]"), "GAS");
			cf.clickElement(By.xpath("(//span[contains(text(),'Global Account Search')])[2]"), "GAS");		
				
			
		}

		cf.clickElement(By.xpath("//div[@class='panel-content scrollable']//a[text()='Log Out']"), "Logout");
		
		
		
	}
}
