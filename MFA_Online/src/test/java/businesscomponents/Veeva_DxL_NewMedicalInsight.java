package businesscomponents;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Veeva_DxL_NewMedicalInsight extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_DxL_NewMedicalInsight(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	/**
	 * This method is for searching and navigating to the accounts
	 * @throws Exception
	 */
	public void searchAccount()
	{
		String account = dataTable.getData("MedIns", "Account");
		Select search = new Select(driver.findElement(By.xpath("//select[@title='Search scope']")));
		search.selectByVisibleText("Accounts");
		driver.findElement(By.className("searchTextBox")).sendKeys(account);
		report.updateTestLog("Fetch Account","Account name is entered in he search box",Status.SCREENSHOT);
		driver.findElement(By.xpath("//div[@class='standardSearchElementBody']//input[@title='Go!']")).click();
		cf.waitForSeconds(3);
	}
	

	/**
	 * This method is for navigating to medical Insight
	 * @throws Exception
	 */
	public void openMedicalInsight() throws Exception
	{
		cf.waitForSeconds(4);
	//	String accType = dataTable.getData("MedIns", "Account Record Type");
		report.updateTestLog("Fetch Account","Account details page is displayed",Status.SCREENSHOT);
		cf.waitForSeconds(5);
		cf.clickSFButton("New Key Medical Insight");		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='New Medical Insight']")));
		report.updateTestLog("New Medical Insight","New Medical Insight Page is displayed",Status.PASS);
	}
	
	/**
	 * This method is for entering the medical insight information
	 * @throws Exception
	 */
	
	public void enterMedInsightInfo() throws Exception
	{
		String inqText = (new Date()).toString()+":\n Diagnostic Insight information is entered here";
		Select equipment = new Select(driver.findElement(By.xpath("//select[@id='Equipment_AZ__c' and @ng-dblclick='addToChosen()']")));		
		List<WebElement> eq = equipment.getOptions();		
		ArrayList<String> equip = new ArrayList<String>(); 		
		if(equipment.isMultiple())
		{	
			for(int ta=1; ta<eq.size();ta++)
			{
				String x = eq.get(ta).getText();
				equip.add(x);
			}	
			//generating random integer value
			Random random = new Random();
			int r = random.nextInt(equip.size()- 0) + 0;int r1= random.nextInt(equip.size()- 0) + 0;		  
			equipment.selectByVisibleText(equip.get(r));
			equipment.selectByVisibleText(equip.get(r1));
			//click the right arrow to move to the other box
			driver.findElement(By.xpath("//select[@id='Equipment_AZ__c' and @ng-dblclick='addToChosen()']/following-sibling::span/a/img[@class='picklistArrowRight']")).click();
			Select eqChosen = new Select(driver.findElement(By.xpath("//select[@id='Equipment_AZ__c' and @ng-dblclick='addToAvailable()']")));
			List<WebElement> eqSelect = eqChosen.getOptions();
			ArrayList<String> equipSelected = new ArrayList<String>(); 
			int eqi = eqSelect.size();
			for(int a=0; a<eqi; a++)
			{
				String s = eqSelect.get(a).getText();
				equipSelected.add(s);
				System.out.println("Selected Equipments are"+s);
				
			}
			
		}
		else 
			report.updateTestLog("New Medical Insight","Unable to multi select Equipment",Status.FAIL);
		
		Select challOppur = new Select(driver.findElement(By.xpath("//select[@id='Challenges_Opportunities_AZ__c' and @ng-dblclick='addToChosen()']")));
		List<WebElement> ch = challOppur.getOptions();		
		ArrayList<String> op = new ArrayList<String>(); 	
		if(challOppur.isMultiple())
		{
			for(int j=0;j<ch.size();j++)
			{
				String w = ch.get(j).getText();
				op.add(w);
			}
			Random random = new Random();
			int r2 = random.nextInt(op.size()- 0) + 0;int r3= random.nextInt(op.size()- 0) + 0;		  
			challOppur.selectByVisibleText(op.get(r2));
			challOppur.selectByVisibleText(op.get(r3));
			driver.findElement(By.xpath("//select[@id='Challenges_Opportunities_AZ__c' and @ng-dblclick='addToChosen()']/following-sibling::span/a/img[@class='picklistArrowRight']")).click();
			
		}
		
		driver.findElement(By.id("Other_Insights_AZ__c")).sendKeys(inqText);
		
		cf.clickButton(By.xpath("//a[@title='Product Lookup']"), "Product look up");
		String k = "Detail";
		cf.waitForSeconds(3);
		WebElement pdtList = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody"));
		List<WebElement> pdt_tr = pdtList.findElements(By.tagName("tr"));
		int pdtLookup_Size = pdt_tr.size();
		report.updateTestLog("Product look up search", "Product search look-up is oepned",Status.SCREENSHOT);

		for(int i=2;i<=pdtLookup_Size;i++)
		{
			WebElement td = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr["+i+"]/td[3]/span"));
			String pdtType = td.getText();
			//System.out.println(pdtType);
			if(pdtType.equals(k))
			{  
				//select the detail product
				String product = driver.findElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr["+i+"]/td[1]/a")).getText();
				dataTable.putData("MedIns", "Product", product);
				cf.clickElement(By.xpath("//div[@class='pbBody lookup-results']/table/tbody/tr["+i+"]/td[1]/a"), "Product Name Link");
				report.updateTestLog("Product Field", "Detail product is selected",Status.PASS);
				break;
			}
									
		}
		
		report.updateTestLog("New Medical Insight","All necessary fields are entered",Status.PASS);

	}

	
	/**
	 * This method is for saving the medical insight information and validating the saved page
	 * @throws Exception
	 */
	
	public void saveInsight() throws Exception
	{
		String medInsID ="";
		cf.clickSFButton("Save");	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Medical Insight']")));
		medInsID= driver.findElement(By.xpath("//h2[contains(@class,'pageDescription ng-binding')]")).getText();
		dataTable.putData("MedIns", "Medical Insight ID", medInsID);
		
		String status = driver.findElement(By.name("Status_vod__c")).getText();
		if(status.equals("Saved"))
		{
			dataTable.putData("MedIns", "Status", status);
			report.updateTestLog("Medical Insight Saved", "Medical insight is saved",Status.PASS);
			
		}
		else
			report.updateTestLog("Medical Insight Saved", "Medical insight is not saved",Status.FAIL);
		
		
	}
	
	/**
	 * This method is for submitting the medical insight information and validating the submitted page
	 * @throws Exception
	 */
	
	public void submitInsight() throws Exception
	{
		String medInsID =dataTable.getData("MedIns", "Medical Insight ID");
		cf.clickSFButton("Submit");	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Medical Insight']")));
		String pdt = dataTable.getData("MedIns", "Product");
		//String st = dataTable.getData("MedIns", "Product");
		String product = driver.findElement(By.name("DX_Product_Catalog_AZ__c")).getText();		
		String status = driver.findElement(By.name("Status_vod__c")).getText();
		if(status.equals("Submitted"))
		{
			if(pdt.equals(product))
				report.updateTestLog("Medical Insight Submit", "Medical insight is submitted",Status.PASS);
			
		}
		else
			report.updateTestLog("Medical Insight Submit", "Medical insight is not submitted",Status.FAIL);
		
		driver.findElement(By.name("Account_vod__c")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Account']")));
		cf.clickSFLinkLet("Medical Insights");
		cf.verifyDataFromLinkLetTable("Medical Insights", "Medical Insight Name", medInsID, "Product", product);
		
	}
	
	
}// End of class
	