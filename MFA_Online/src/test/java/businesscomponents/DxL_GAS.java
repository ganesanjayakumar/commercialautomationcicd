package businesscomponents;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.mfa.framework.Status;
import ObjectRepository.objGlobalAccountSearchPage;
import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class DxL_GAS extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public DxL_GAS(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
  
	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToGASLinkPage() throws Exception{

		String sName = "Anagnostakos, Konstantinos";
		sName.trim();
		String[] sSplit = sName.split(",");
		sName = sSplit[1].trim() + " " +sSplit[0].trim();
		System.out.println(sName);
		
		cf.clickLink(objHeaderPage.plussymbol,"Plus Symbol");
		cf.clickLink(objHeaderPage.lnkGlobalAccountSearch, "Global Account Search Link");
		cf.waitForSeconds(10);
		if(cf.isElementVisible(objHeaderPage.lblSearchHeader, "Search for Account Header"))
			report.updateTestLog("Verify able to navigate to Search for Account page", "Navigating to Global account search page is successful", Status.PASS);
		else
			report.updateTestLog("Verify able to navigate to Search for Account page", "Unable to navigate to Surveys Home page", Status.FAIL);
	}

	/**
	 * Method to enter 'Name' and validate search results
	 * @throws Exception 
	 */
	public void searchAndValidateNameFieldDxL() throws Exception{
		cf.pageRefresh();
		String strName = dataTable.getData("GlobalAccountSearch", "Name");

		if(!strName.trim().equals("")){
			String[] arrFirstNameLastName =strName.split(";");		//barma, sajal-exact
			for(int iter = 0 ; iter < arrFirstNameLastName.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrFirstNameLastName[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(40);
				cf.scrollToTop();
				
				if(cf.isElementPresent(By.xpath("//li[contains(.,'More than 50 records found')]")))
				{
					report.updateTestLog("Warning or error message is displaying", "Message displayed successfully", Status.PASS);
				}
					else
				{
						report.updateTestLog("Warning or error message is not displaying", "Message not displaying successfully", Status.FAIL);	
				}
				cf.pageRefresh();
				
				enterSearchFieldsInGAS("Name", "Anagnos", "contains");
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(35);
				
				//verifySearchResults("Name",strSearch.trim(), strExactOrContains);
			}
		}
		else report.updateTestLog("Search and validate Name", "No search term provided", Status.WARNING);
	}


	public void enterSearchFieldsInGAS(String strFieldName, String strSearch,String strExactOrContains) throws Exception{
		if(strSearch.trim()!=""){
			if(strSearch.contains(",") && strFieldName.trim().equalsIgnoreCase("Name"))	
				strSearch = strSearch.substring(strSearch.indexOf(",")+1).trim() + " " +strSearch.substring(0, strSearch.indexOf(",")).trim();


			switch(strFieldName.trim().toUpperCase()){
			case "NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radNameExactMatch, "Name Exact match"); break;
				case	"CONTAINS":cf.actionClick(objGlobalAccountSearchPage.radNameContains, "Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radNameExactMatch, "Name Exact match"); break;
				}
				cf.setData(objGlobalAccountSearchPage.editName, "Name", strSearch.trim());
				break;
			case "FIRST NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radFirstNameExactMatch, "First Name Exact match"); break;
				case	"CONTAINS":	cf.actionClick(objGlobalAccountSearchPage.radFirstNameContains, "First Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radFirstNameExactMatch, "First Name Exact match"); break;
				}
				cf.setData(objGlobalAccountSearchPage.editFirstName, "First Name", strSearch.trim());
				break;
			case "LAST NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radLastNameExactMatch, "Last Name Exact match");	 break;
				case	"CONTAINS":cf.actionClick(objGlobalAccountSearchPage.radLastNameContains, "Last Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radLastNameExactMatch, "Last Name Exact match");	 break;
				}
				cf.setData(objGlobalAccountSearchPage.editLastName, "Last Name", strSearch.trim());
				break;
			case "SPECIALTY":
				cf.selectData(objGlobalAccountSearchPage.selectPrimarySpecialty, "Primary Specialty", strSearch);
				break;
			default:
				cf.setData(objGlobalAccountSearchPage.editName, "Name", strSearch.trim());
				break;
			}
		}


	}

public void addAndVerifySuccessfulTerritoryToAddDxL() throws Exception{
		//cf.waitForSeconds(20);
		Boolean blnBreak = false;
		List<WebElement> addToTerritoryChks = driver.findElements(objGlobalAccountSearchPage.chkFirstSearchResult);
		for (int i = 0 ; i < addToTerritoryChks.size(); i++){
			cf.clickButton(By.xpath("//table[@id='restable']//tr["+(i+2)+"]/td[1]/input"), "Account selected");
			cf.clickButton(objGlobalAccountSearchPage.btnAddToTerritory, "Add To Territory");
			cf.waitForSeconds(10);
			cf.scrollToTop();
			if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){
				String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification");
				System.out.println(lblAlert);
				if (lblAlert.trim().contains("The customer is already in your territory. No changes made.")){
					report.updateTestLog("Verify Add to Territory", "User is successfully added to territory", Status.PASS);
					blnBreak = true;
					break;
				}
				else if (i == addToTerritoryChks.size()-1)
					report.updateTestLog("Verify Add to Territory", "Add to territory successful notification message is not displayed", Status.FAIL);
			}
			else report.updateTestLog("Verify Add to Territory", "No alert message is  displayed", Status.FAIL);	
			if (blnBreak)	break;
		}
		cf.scrollToBottom();
		WebElement sSearchname = driver.findElement(By.xpath("(//table[@id='restable']//tr[2]/td[1]/input/following::td)[1]"));
		String sName = sSearchname.getText();
		sName.trim();
		String[] sSplit = sName.split(",");
		sName = sSplit[1].trim() + " " +sSplit[0].trim();
		System.out.println(sName);
		enterSearchFieldsInGAS("Name", sName, "exact");
		cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
		cf.waitForSeconds(20);
		

	}
}
