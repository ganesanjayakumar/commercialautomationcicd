package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.ExcelDataAccess;
import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objCallsForMedical;
import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_CallsStandardizationMedical extends VeevaFunctions {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 60);
	WebDriverWait waits = new WebDriverWait(driver.getWebDriver(), 200);
	// CallsStandardizationMedical csm = new
	// CallsStandardizationMedical(scriptHelper);
	ExcelDataAccess excelReport;
	/*
	 * / static HashMap<String, String> mapDiscussion_clinicalstudy = new
	 * HashMap<>(); static HashMap<String, String> mapDiscussion_collaboration =
	 * new HashMap<>(); static HashMap<String, String>
	 * mapDiscussion_diseaseawareness = new HashMap<>(); static HashMap<String,
	 * String> mapDiscussion_productvalue = new HashMap<>(); //
	 */
	static HashMap<String, String> mapDiscussion_ongoingclinicalstudy = new HashMap<>();
	static HashMap<String, String> mapDiscussion_medicalinsight = new HashMap<>();

	public Veeva_CallsStandardizationMedical(ScriptHelper scriptHelper) {
		super(scriptHelper);

	}

	/**
	 * Default Delivery channel of user will be stored in variable.
	 * 
	 * @throws Exception
	 */

	/**
	 * This method searches for an account in Search box and click Go
	 * 
	 * @throws Exception
	 */
	/*
	 * / public void searchAccountAndGo() throws Exception{
	 * 
	 * objCallsForMedical.strAccountNameForCalls =
	 * dataTable.getData("CallsStandardizationMedical", "Account");
	 * 
	 * cf.selectData(objHeaderPage.selectSearch, "Search Type", "Accounts");
	 * cf.setData(objHeaderPage.textSearch, "Search Text",
	 * objCallsForMedical.strAccountNameForCalls);
	 * cf.clickButton(objHeaderPage.btnGo, "Go button in Search");
	 * 
	 * } //
	 */
	/**
	 * Clicking Record Call Option in Account Details Page.
	 * 
	 * @throws Exception
	 */

	public void clickrecordcall() throws Exception {
		By byCallReport;
		System.out.println(objCallsForMedical.strAccountNameForCalls + " inside record call");
		wait.until(ExpectedConditions.elementToBeClickable(objCallsForMedical.btnRecordACall));
		report.updateTestLog("Verify able to see Record Call Button", "Able to see Record Call Button", Status.PASS);
		cf.clickButton(objCallsForMedical.btnRecordACall, "Record Call");
		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		/*
		 * if(strLocale.trim().equalsIgnoreCase("JP") &&
		 * strProfile.trim().equalsIgnoreCase("COMMERCIAL")) byCallReport =
		 * By.xpath("//h1[normalize-space(text())='AZ Call Detail PRC']"); else
		 * if(strLocale.trim().equalsIgnoreCase("CN") &&
		 * strProfile.trim().equalsIgnoreCase("COMMERCIAL")) byCallReport =
		 * By.xpath("//h1[normalize-space(text())='Call Report']"); else
		 * if(strLocale.trim().equalsIgnoreCase("RU") &&
		 * strProfile.trim().equalsIgnoreCase("MEDICAL")) byCallReport =
		 * By.xpath("//h2[.='New Event Call Report']"); else byCallReport =
		 * objCallsForMedical.newcallReportPage;
		 * wait.until(ExpectedConditions.elementToBeClickable(byCallReport));
		 * if(cf.isElementVisible(byCallReport, "New Call Report Page"))
		 * report.updateTestLog("Verify able to see New Call Report Page",
		 * "Able to see New Call Report Page", Status.PASS); else
		 * report.updateTestLog("Verify able to see New Call Report Page",
		 * "Unable to see New Call Report Page", Status.FAIL);
		 */

	}

	public void enterMedicalProfessionalInformation() throws Exception {
		accountblockmedical();

		selectAddress();
		selectInitiatedBy();
		selectMedicalEvent();
		selectVenue();
		selectAffilitation();
		selectChannel();
		selectDuration();
		affiliationblock();
	}

	public void validateInteractionType() throws Exception {

		List<String> expectedInitiatedBy = Arrays.asList("--None--", "Commercial", "External", "Medical", "Study Team",
				"Diagnostics");
		List<String> actualInitiatedBy = cf.getAllOptionsinDropdown(objCallsForMedical.selectInitiatedBy,
				"All Options in Initiated By");
		if (expectedInitiatedBy.equals(actualInitiatedBy)) {
			cf.selectData(objCallsForMedical.selectInitiatedBy, "Initiated By", "Diagnostics");
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are available", Status.PASS, false);
		} else {
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are not available", Status.FAIL);
		}
		cf.selectData(objCallsForMedical.recordType, "Record Type", "Sales Rep Intro meeting facilitated by MSL");
		cf.waitForSeconds(6);
		actualInitiatedBy = cf.getAllOptionsinDropdown(objCallsForMedical.selectInitiatedBy,
				"All Options in Initiated By");
		if (expectedInitiatedBy.equals(actualInitiatedBy)) {
			cf.selectData(objCallsForMedical.selectInitiatedBy, "Initiated By", "Diagnostics");
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are available", Status.PASS, false);
			cf.selectData(objCallsForMedical.selectInitiatedBy, "Initiated By", "Medical");
		} else {
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are not available", Status.FAIL);
		}
		cf.selectData(objCallsForMedical.recordType, "Record Type", "MSL Interaction");
		cf.waitForSeconds(6);
	}

	public void enterCommercialProfessionalInformation() throws Exception {
		String strLocale = dataTable.getData("Login", "Locale");
		new Veeva_CallsStandardization(scriptHelper).accountblock();
		selectAddress();
		selectAffilitation();
		dateValidationCalls();

		switch (strLocale.trim().toUpperCase()) {
		case "IE":
			break;
		case "EU1":
			new Veeva_CallsStandardization(scriptHelper).solicitedBlock();
			break;
		case "EU2":
			break;
		case "ANZ":
			String ele = driver.findElement(By.xpath("//select[@id='AZ_ANZ_Call_Type__c']//option[2]")).getText();
			cf.selectData(ObjVeevaCallsStandardization_Commercial.azsubtype, "AZ subtype", ele);
			break;
		case "RU":
			break;
		case "BR":
			break;

		}
	}

	/**
	 * Validate account name block is required and account name is present or
	 * not.
	 * 
	 * @throws Exception
	 */

	public void accountblockmedical() throws Exception {
		try {
			cf.waitForSeconds(10);
			if (cf.isElementVisible(objCallsForMedical.accountrequired, "Account Required", 10)) {
				report.updateTestLog("Verify account is required block or not", "Able to see account as requred block",
						Status.PASS);
				objCallsForMedical.strAccountNameForCalls = driver
						.findElement(By.xpath("//label[.='Account']/parent::div/following-sibling::div//a")).getText()
						.trim();
				if (!(objCallsForMedical.strAccountNameForCalls == null)) {
					report.updateTestLog("Verify account name is present", "Able to see account name", Status.PASS);
				} else {
					report.updateTestLog("Verify account name is present", "Able to see account name", Status.FAIL);
				}

			} else {
				report.updateTestLog("Verify account is required block or not",
						"Unable to see account as required block", Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * Validate Engagement Type drop-down values
	 * 
	 * @throws Exception
	 */

	public void validateEngagementTypes() throws Exception {
		Boolean validate = false;
		List<String> optionsCall;
		if (cf.isElementVisible(By.id("MA_Venue_AZ__c"), "Engagement Type")) {
			report.updateTestLog("Verify able to see Engagement Type dropdown", "Able to see Engagement Type dropdown.",
					Status.PASS);
			optionsCall = new ArrayList<String>(
					Arrays.asList("--None--", "AZ Meeting", "Congress", "Medical Engagement", "MA Led Program"));
			for (int i = 0; i < optionsCall.size(); i++) {
				String option = optionsCall.get(i).toString();
				WebElement optionele = driver
						.findElement(By.xpath("//select[@id='MA_Venue_AZ__c']//option[.='" + option + "']"));
				if (cf.isElementPresent(optionele, "Option")) {
					System.out.println(option);
					validate = true;
				} else {
					break;
				}
			}

			if (validate == true)
				report.updateTestLog("Verify able to see Engagement Types under Engagement Type Dropdown.",
						"Able to see Engagement Types under Engagement Type Dropdown.", Status.PASS);
			else
				report.updateTestLog("Verify able to see Engagement Types under Engagement Type Dropdown.",
						"Unable to see Engagement Types under Engagement Type Dropdown.", Status.FAIL);
		}
	}

	/**
	 * This method validates if all the mandatory fields are marked as
	 * "required"
	 */
	public void validateRequiredFields() {
		List<WebElement> listMandatoryFields = driver.findElements(objCallsForMedical.listMandatoryFields);
		for (WebElement elemMandatory : listMandatoryFields) {
			switch (elemMandatory.getAttribute("id").trim()) {
			case "MA_Origin_AZ__c":
				report.updateTestLog("Verify Mandatory Fields", "'Initiated By' is marked as mandatory as expected",
						Status.PASS);
				break;
			case "MA_Venue_AZ__c":
				report.updateTestLog("Verify Mandatory Fields", "'Venue' is marked as mandatory as expected",
						Status.PASS);
				break;
			case "MA_Channel_AZ__c":
				report.updateTestLog("Verify Mandatory Fields",
						"'Interaction Channel' is marked as mandatory as expected", Status.PASS);
				break;
			case "Call_Datetime_vod__c":
				report.updateTestLog("Verify Mandatory Fields", "'Date time' is marked as mandatory as expected",
						Status.PASS);
				break;
			case "MA_Patient_Safety_Attestation_AZ__c":
				report.updateTestLog("Verify Mandatory Fields",
						"'Patient safety attestation' is marked as mandatory as expected", Status.PASS);
				break;
			default:
				report.updateTestLog("Verify Mandatory Fields", "Element with ID : "
						+ elemMandatory.getAttribute("id").trim() + " is marked as mandatory but not expected ",
						Status.SCREENSHOT);

			}
		}
	}

	/**
	 * This method will choose address from Address drop down. It can be a
	 * random selection if there is no input from Data table
	 * 
	 * @throws Exception
	 */
	public void selectAddress() throws Exception {
		validateInteractionType();
		selectValueInRecordCall(objCallsForMedical.selectAddress, "Address", "None");

	}

	/**
	 * This method will choose selection from Initiated By drop down
	 */
	public void selectInitiatedBy() throws Exception {
		selectValueInRecordCall(objCallsForMedical.selectInitiatedBy, "Initiated By", "None");

	}

	public void selectMedicalEvent() throws Exception {
		String strVenue = (new Select(driver.findElement(objCallsForMedical.selectEngagementType)))
				.getFirstSelectedOption().getText();
		String strLocale = dataTable.getData("Login", "Locale");
		if (strVenue.trim().equalsIgnoreCase("Congress") || strVenue.trim().equalsIgnoreCase("AZ Meeting")) {
			if (strLocale.trim().equalsIgnoreCase("EU1") || strLocale.trim().equalsIgnoreCase("EU2"))
				cf.clickButton(objCallsForMedical.linkMeetingEventLookup, "Meeting Event Lookup");
			else
				cf.clickButton(objCallsForMedical.linkMedicalEventLookup, "Search Medical Event");
			cf.clickButton(objCallsForMedical.btnGo, "Go");
			cf.clickButton(objCallsForMedical.linkInsideSearch, "Search resutls inside medical event");
		}
	}

	/**
	 * This method will choose selection from Venue drop down
	 */
	public void selectVenue() throws Exception {
		cf.selectData(objCallsForMedical.selectEngagementType, "Venue", "Medical Engagement");

	}

	/**
	 * This method will choose selection from Venue drop down
	 */
	public void selectAffilitation() throws Exception {
		selectValueInRecordCall(objCallsForMedical.selectAffiliation, "Affiliation", "None");

	}

	/**
	 * This method will choose selection from Interaction Channel drop down
	 */
	public void selectChannel() throws Exception {
		selectValueInRecordCall(objCallsForMedical.selectChannel, "Channel", "None");

	}

	public void selectDuration() throws Exception {

		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		if (strLocale.trim().equalsIgnoreCase("CN") && strProfile.trim().equalsIgnoreCase("COMMERCIAL"))
			cf.setData(objCallsForMedical.setDuration, "Duration", "30");
		else {
			WebElement duration = driver.findElement(objCallsForMedical.selectDuration);
			if (duration.isEnabled())
				selectValueInRecordCall(objCallsForMedical.selectDuration, "Duration", "None");
		}
	}

	public void callForm() throws Exception {
		selectValueInRecordCall(objCallsForMedical.selectCallForm, "Call Form", "None");
	}

	public void azCallTypeAndDetailingPriority() throws Exception {
		String strLocale = dataTable.getData("Login", "Locale");
		String accountType = dataTable.getData("Login", "AccountType");
		String strProfile = dataTable.getData("Login", "Profile");
		String strDetailingPriority = dataTable.getData("Login", "DetailingPriority");
		String recordType = dataTable.getData("Login", "Record Type");

		try {
			if (strLocale.trim().equalsIgnoreCase("JP")) {
				System.out.println("INSIDE jp");

				if (strProfile.equalsIgnoreCase("Commercial") && accountType.equalsIgnoreCase("Hospital"))
					cf.selectData(By.xpath("//select[@name='RecordTypeId']"), "Record Type",
							"AZ Meeting Call Detail PRC");

				cf.selectData(cf.getSFElementXPath("Professional Information", "AZ Call Type", "Select"),
						"AZ Call Type", recordType);
				cf.clickSFCheckBox(strDetailingPriority, "Preceding", true); // Adding
																				// this
																				// code
																				// on
																				// 28th
																				// March
																				// 2019
																				// to
																				// fix
																				// empty
																				// rows
																				// issue
																				// -
																				// Srinidhi
				// keyMessagesAndComments(strDetailingPriority, strKeyMessages);

				// Details of Explanatory MTG
				if (accountType.equals("Hospital")) {
					cf.clickElement(By.xpath("//input[@id='Hospitals_AZ_JP__c']"), "Hospital");

					cf.setData(By.xpath("//input[@id='Number_of_Dr_Attendees_AZ_JP__c']"), "Number_of_Dr_Attendees",
							"10");

					cf.setData(By.xpath("//input[@id='Number_of_Other_Participants_AZ_JP__c']"),
							"Number_of_Dr_Attendees", "10");

					cf.setData(By.xpath("//input[@id='Total_Cost_of_Bento_Box_Drink_AZ_JP__c']"),
							"Number_of_Dr_Attendees", "100");

					cf.clickElement(By.xpath("//input[@id='Attach_Name_Book_AZ_JP__c']"), "Attach_Name_Book");

				}

			}
		} catch (Exception e) {
			report.updateTestLog("Verify Key Messages And Comments",
					"Error occurred in selecting Key messages : " + e.getMessage(), Status.FAIL);
		}

	}

	public void relevantProducts() throws Exception {
		String strLocale = dataTable.getData("Login", "Locale");
		cf.waitForSeconds(1);
		// String strProduct = dataTable.getData("Login", "Products");
		String strProduct = "c";
		cf.waitForSeconds(1);
		String strKeyMessages = dataTable.getData("Login", "KeyMessagesComments");
		cf.waitForSeconds(1);
		String strProfile = dataTable.getData("Login", "Profile");
		cf.waitForSeconds(1);
		selectProduct(strProduct);
		if (strProfile.equalsIgnoreCase("Medical")) {
			if (cf.isElementPresent(objCallsForMedical.addDiscussionMedicalInsight)) {
				cf.clickButton(objCallsForMedical.addDiscussionMedicalInsight, "Add Discussion - Medical Insight");
				cf.selectARandomOptionFromComboBox(objCallsForMedical.therapeuticArea, "Therapeutic Area");
				cf.selectARandomOptionFromComboBox(objCallsForMedical.scientificInsightTopic,
						"Scientific Insight Topic");
				cf.clickButton(objCallsForMedical.emailLookup, "Email Medical Insight To Lookup");
				emailMedicalInsight();
				cf.setData(objCallsForMedical.medicalInsightDescription, "Medical_Insight_Description",
						"Test Insight Description");
				cf.setData(objCallsForMedical.medicalInsightSummary, "Medical Insight Summary", "Test Insight Summary");
			}
			cf.clickButton(objCallsForMedical.btnClinicalDiscussionAdd, "Add Clinical Discussion");
			cf.waitForSeconds(1);
			if (cf.isElementPresent(objCallsForMedical.selectDiscussionIntent)) {
				cf.selectData(objCallsForMedical.selectDiscussionIntent, "Discussion Intent", "Proactive");
			}
			validateClinicalStudySupportActivity();

		}
		/*
		 * if (strLocale.trim().equalsIgnoreCase("CN") ||
		 * strLocale.trim().equalsIgnoreCase("JP"))
		 * keyMessagesAndComments(strProduct, strKeyMessages);
		 */

	}

	public void validateClinicalStudySupportActivity() throws Exception {
		List<String> clinicalStudyOptions = cf.getAllOptionsinDropdown(objCallsForMedical.selectClinicSupport,
				"All Options in Clinical study");
		List<String> expectedClinicalStudyOptions = Arrays.asList("--None--", "Site Identification & Feasibility",
				"Site Qualification/Initiation Visit", "AZ Sponsored Study Support", "ESR Scientific Study Support",
				"ESR Concept Study Discussion", "Site Referrals", "Study Design Concept");
		List<String> clinicalStudySupportActivityOptions = Arrays.asList("Site Identification & Feasibility",
				"Site Qualification/Initiation Visit", " AZ Sponsored Study Support", "ESR Scientific Study Support",
				"Site Referrals", "Study Design Concept");
		if (clinicalStudyOptions.equals(expectedClinicalStudyOptions)) {
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are available", Status.PASS);
		} else {
			report.updateTestLog("Verify the presence of all Clinical study options",
					"All Clinical study support actions are not available", Status.FAIL);
		}
		for (String option : clinicalStudySupportActivityOptions) {
			cf.selectData(objCallsForMedical.selectClinicSupport, "Clinical Study Support Activity", option);
			cf.clickButton(objCallsForMedical.btnSave, "Save");
			wait.until(ExpectedConditions.visibilityOfElementLocated(objCallsForMedical.clinicalStudyErrorMsg));
			if (cf.isElementVisible(objCallsForMedical.clinicalStudyErrorMsg, "clinicalStudyErrorMsg"))
				report.updateTestLog("Verify Error Message for Clinical study",
						"Error message is displayed for selecting " + option + " without providing clinical study",
						Status.PASS);
			else

				report.updateTestLog("Verify Error Message for Clinical study",
						"Error message is not displayed for selecting " + option + " without providing clinical study",
						Status.FAIL);
		}
		cf.selectData(objCallsForMedical.selectClinicSupport, "Clinical Study Support Activity",
				"ESR Concept Study Discussion");
	}

	public void keyMessagesAndComments(String strHeaderProduct, String strKeyMessages) throws Exception {
		List<WebElement> listReactions = null;
		String strLocale = dataTable.getData("Login", "Locale");

		cf.waitForSeconds(3);
		String[] arrKeyMessages = null;
		if (strKeyMessages.trim() != "") {
			arrKeyMessages = strKeyMessages.split(";");
			for (int i = 0; i < arrKeyMessages.length; i++)
				cf.clickSFCheckBox(arrKeyMessages[i].trim(), "Preceding", true);
		} else {
			cf.clickButton(By.cssSelector("td[ng-repeat*='group.keyMessages'] >input"), "Key Messages");
			arrKeyMessages = new String[] {
					cf.getData(By.cssSelector("td[ng-repeat*='group.keyMessages'] >input"), "Key Messages", "text") };
		}
		cf.scrollToBottom();
		List<WebElement> listProductNames = driver
				.findElements(By.cssSelector("table#key_message_details_vod [name='Product_vod__c']"));
		List<WebElement> listProductMessages = driver
				.findElements(By.cssSelector("table#key_message_details_vod [name='Name']"));
		if (strLocale.trim().equalsIgnoreCase("CN") || strLocale.trim().equalsIgnoreCase("JP"))
			listReactions = driver.findElements(By.cssSelector("[ng-model='row.Reaction_vod__c']"));
		for (int i = 0; i < arrKeyMessages.length; i++) {
			if (arrKeyMessages[i].trim().equalsIgnoreCase(listProductMessages.get(i).getText().trim())
					&& listProductNames.get(i).getText().trim().toLowerCase()
							.contains(strHeaderProduct.trim().toLowerCase())) {
				report.updateTestLog("Verify Key Messages And Comments",
						"'" + listProductMessages.get(i).getText() + "' displayed for " + strHeaderProduct,
						Status.PASS);
				if (strLocale.trim().equalsIgnoreCase("CN"))
					new Select(listReactions.get(i)).selectByVisibleText("Contracted");// Values
																						// changed
																						// for
																						// CN-
																						// instead
																						// of
																						// Neutral,
																						// it
																						// is
																						// now
																						// changed
																						// as
																						// Contracted
				else if (strLocale.trim().equalsIgnoreCase("JP"))
					new Select(listReactions.get(i)).selectByIndex(1); // Values
																		// included
																		// for
																		// JP
																		// due
																		// to
																		// added
																		// Discussion
																		// Intent
																		// list
																		// box
			}

		}
		// }
	}

	/**
	 * Validate affiliation field is required field and and address value is
	 * present. If affiliation value is None select any other address option
	 * from dropdown
	 * 
	 * @throws Exception
	 */
	// */
	public void affiliationblock() throws Exception {
		try {
			if (cf.isElementVisible(objCallsForMedical.affiliationrequired, "Affiliation Required")) {
				report.updateTestLog("Verify Affiliation is required block or not",
						"Able to see Affiliation as requred block", Status.PASS);
				Select se = new Select(driver.findElement(objCallsForMedical.affiliationdropdown));
				String affiliationoption = se.getFirstSelectedOption().getText().trim();
				int optionscount = se.getOptions().size();

				if (!(affiliationoption.equals("--None--"))) {
					report.updateTestLog("Verify Affiliation address value is selected as option",
							"Able to see Affiliation address value as selected as option", Status.PASS);
				} else if (optionscount > 1) {
					String option2 = driver
							.findElement(By.xpath("//div[@name='zvod_Business_Account_vod__c']/select/option[2]"))
							.getText().trim();
					cf.selectData(objCallsForMedical.affiliationdropdown, "Affiliation Dropdown", option2);
					report.updateTestLog("Verify Affiliation is selected", "Able to select Affiliation value",
							Status.PASS);
					// frameworkparameters.setStopExecution(true);
				} else {
					report.updateTestLog("Verify Affiliation value is available", "Affiliation value is not available",
							Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}

			} else {
				report.updateTestLog("Verify Affiliation is required block or not",
						"Unable to see Affiliation as required block", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Adding Attendees to the Call.
	 * 
	 * @throws Exception
	 */

	public void addAttendees() throws Exception {
		String strProfile = dataTable.getData("Login", "Profile");
		String strDataSheet = "";
		if (strProfile.trim().equalsIgnoreCase("Medical"))
			strDataSheet = "CallsStandardizationMedical";
		else
			strDataSheet = "Calls_Standardization";
		String strAttendees = dataTable.getData(strDataSheet, "Attendee");
		new Veeva_CallsStandardization(scriptHelper).addattendes(strAttendees);
		// cf.selectARandomOptionFromComboBox(objCallsForMedical.selectAdditionalAZAttendee,
		// "Additional AZ Attendee");
	}

	// */
	/**
	 * To validate below step : Scroll down to the 'Medical Discussion' section
	 * and validate that the below mentioned discussion topics details should be
	 * displayed: - Clinical Study - Collaboration - Disease Awareness - Product
	 * value
	 */
	public void validateTopicsUnderMedicalDiscussion() {
		cf.scrollToBottom();
		List<WebElement> labelsMedicalDiscussion = driver.findElements(objCallsForMedical.labelsDiscussion);
		System.out.println(labelsMedicalDiscussion.size());
		boolean blnSettings = true;
		for (WebElement labelDiscussion : labelsMedicalDiscussion) {
			String text = labelDiscussion.getText().trim();
			switch (text) {
			case "Clinical Study":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + "' TOPIC is displayed", Status.DONE);
				break;
			case "On-going Clinical Study":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + "' TOPIC is displayed", Status.DONE);
				break;
			case "Collaboration":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + " TOPIC is displayed", Status.DONE);
				break;
			case "Disease Awareness":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + " TOPIC is displayed", Status.DONE);
				break;
			case "Product Value":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + " TOPIC is displayed", Status.DONE);
				break;
			case "New Medical Insight":
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + "' TOPIC is displayed", Status.DONE);
				break;
			default:
				report.updateTestLog("Verify topics under Medical Discussion",
						"'" + labelDiscussion.getText().trim() + " is not expected", Status.FAIL);
				blnSettings = false;
				break;
			}
		}
		if (blnSettings)
			report.updateTestLog("Verify topics under Medical Discussion", "All topics are displayed as expected",
					Status.PASS);
	}

	public void validateCopyAndDelUnderEachTopic() throws Exception {

		cf.clickButton(objCallsForMedical.btnClinicalDiscussionAdd, "Add On-going Clinical Study Discussion");
		enterDiscussionFields("On-going Clinical Study");
		cf.clickButton(objCallsForMedical.btnClinicalDiscussionCopy, "Copy Clinical Study");
		cf.scrollToBottom();
		cf.clickButton(objCallsForMedical.btnClinicalDiscussionDel, "Delete Clinical Study Discussion");
		report.updateTestLog("Clinical Study Discussion", "Details entered in Clinical Study", Status.SCREENSHOT);

		cf.scrollToBottom();
		cf.clickButton(objCallsForMedical.btnMedicalInsightAdd, "Add Medical Insight");
		enterDiscussionFields("New Medical Insight");
		cf.clickButton(objCallsForMedical.btnMedicalInsightCopy, "Copy Medical Insight");
		cf.scrollToBottom();
		cf.clickButton(objCallsForMedical.btnMedicalInsightDel, "Delete Medical Insight");
		report.updateTestLog("Medical Insight Creation", "Details entered in Collaboration", Status.SCREENSHOT);

		// Commenting below methods since these topics are changed in latest
		// medical discussions
		/*
		 * / cf.clickButton(objCallsForMedical.btnCollaborationAdd,
		 * "Add Collaboration Discussion");
		 * enterDiscussionFields("Collaboration");
		 * cf.clickButton(objCallsForMedical.btnCollaborationCopy,
		 * "Copy Collaboration"); cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnCollaborationDel,
		 * "Delete Collaboration Discussion");
		 * report.updateTestLog("Collaboration Study Discussion"
		 * ,"Details entered in Collaboration",Status.SCREENSHOT);
		 * 
		 * cf.clickButton(objCallsForMedical.btnClinicalDiscussionAdd,
		 * "Add Clinical Study Discussion");
		 * enterDiscussionFields("Clinical Study");
		 * cf.clickButton(objCallsForMedical.btnClinicalDiscussionCopy,
		 * "Copy Clinical Study"); cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnClinicalDiscussionDel,
		 * "Delete Clinical Study Discussion");
		 * report.updateTestLog("Clinical Study Discussion"
		 * ,"Details entered in Clinical Study",Status.SCREENSHOT);
		 * 
		 * cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnCollaborationAdd,
		 * "Add Collaboration Discussion");
		 * enterDiscussionFields("Collaboration");
		 * cf.clickButton(objCallsForMedical.btnCollaborationCopy,
		 * "Copy Collaboration"); cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnCollaborationDel,
		 * "Delete Collaboration Discussion");
		 * report.updateTestLog("Collaboration Study Discussion"
		 * ,"Details entered in Collaboration",Status.SCREENSHOT);
		 * 
		 * cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnDiseaseAwarenessAdd,
		 * "Add Disease Awareness"); enterDiscussionFields("Disease Awareness");
		 * cf.clickButton(objCallsForMedical.btnDiseaseAwarenessCopy,
		 * "Copy Disease Awareness"); cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnDiseaseAwarenessDel,
		 * "Delete Disease Awareness");
		 * report.updateTestLog("Disease Awareness Discussion"
		 * ,"Details entered in Disease Awareness",Status.SCREENSHOT);
		 * 
		 * cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnProductValueAdd,
		 * "Add Product Value Discussion");
		 * enterDiscussionFields("Product Value");
		 * cf.clickButton(objCallsForMedical.
		 * btnProductValueCopy,"Copy Product Value"); cf.scrollToBottom();
		 * cf.clickButton(objCallsForMedical.btnProductValueDel,
		 * "Delete Product Value Discussion");
		 * report.updateTestLog("Product Subvalue Discussion"
		 * ,"Details entered in Product Subvalue",Status.SCREENSHOT);
		 * 
		 * cf.scrollToBottom();
		 * 
		 * //
		 */
		List<WebElement> copybuttons = driver.findElements(objCallsForMedical.copyButtons);
		List<WebElement> delbuttons = driver.findElements(objCallsForMedical.delButtons);

		if (copybuttons.size() >= 2 && delbuttons.size() >= 2)
			report.updateTestLog("Verify Copy and Delete", "All topics have Copy and Delete buttons", Status.PASS);
		else
			report.updateTestLog("Verify Copy and Delete", "Copy and Delete buttons are not present ", Status.FAIL);

	}

	/**
	 * Click on 'Add Discussion' button near to the 'Clinical Study' topic
	 * information' section with all of the Clinical Study topic related fields
	 * should be displayed.
	 * 
	 * @throws Exception
	 */
	public void enterDiscussionFields(String strDiscussionType) throws Exception {
		// WebElement selectDiseaseIndication;
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");

		String strDiscussionIntent = "//label[.='Discussion Intent']/parent::div/following-sibling::div//select";
		String strTherapeuticArea = "//label[.='Therapeutic Area']/parent::div/following-sibling::div//select";
		// String strDiseaseIndication="//label[.='Disease and
		// Indication']/parent::div/following-sibling::div//select";
		String strSearchClinicalStudy = "//label[text()='Clinical Study' or text()='On-going Clinical Study']/parent::div/following-sibling::div//a";
		// String
		// strSearchProduct="//label[.='Product']/parent::div/following-sibling::div//a";
		String strClinicalStudySupport = "//label[.='Clinical Study Support Activity']/parent::div/following-sibling::div//select";
		// String strEmailMedicalInsight="//label[.='Email Medical Insight
		// to']/parent::div/following-sibling::div//a";
		String strScientificInsight = "//label[.='Scientific Insight Topic']/parent::div/following-sibling::div//select";

		String strTableDiscussionType = "";
		String strTableDiscussion = "";
		switch (strDiscussionType) {
		case "On-going Clinical Study":
			if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
				strTableDiscussionType = "  Clinical Study ";
			else
				strTableDiscussionType = "  On-going Clinical Study ";
			strTableDiscussion = "//label[.='" + strTableDiscussionType
					+ "']/parent::td/parent::tr/parent::tbody/parent::table";
			// selectValueInRecordCall(By.xpath(strTableDiscussion +
			// strDiscussionIntent), "Discussion Intent", "On-going Clinical
			// Study");
			cf.clickButton(By.xpath("//input[@id='MA_Clinical_Study_AZ__c']/../a"), "Search Clinical Study");
			cf.clickButton(objCallsForMedical.btnGo, "Go");
			cf.waitForSeconds(3);
			report.updateTestLog("Search clinical study", "Clinical study pop up is expected to be opened",
					Status.SCREENSHOT);
			System.out
					.println("get data : " + cf.getData(objCallsForMedical.linkInsideSearch, "Clinical Study", "text"));
			mapDiscussion_ongoingclinicalstudy.put("On-going Clinical Study",
					cf.getData(objCallsForMedical.linkInsideSearch, "Clinical Study", "text"));
			cf.clickButton(objCallsForMedical.linkInsideSearch, "Search result");
			selectValueInRecordCall(By.xpath(strTableDiscussion + strClinicalStudySupport), "Clinical Study Support",
					"On-going Clinical Study");
			/*
			 * / cf.clickButton(By.xpath(strTableDiscussion+strSearchProduct),
			 * "Search Product"); cf.setData(objCallsForMedical.searchText,
			 * "Search", strProductToSelect);
			 * cf.clickButton(objCallsForMedical.btnGo, "Go");
			 * cf.waitForSeconds(3);
			 * cf.clickButton(objCallsForMedical.linkInsideSearch,
			 * "Search result");
			 * selectValueInRecordCall(By.xpath(strTableDiscussion+
			 * strTherapeuticArea), "Therapeutic Area", "Clinical Study");
			 * 
			 * selectDiseaseIndication =
			 * driver.findElement(By.xpath(strTableDiscussion+
			 * strDiseaseIndication)); if (selectDiseaseIndication.isEnabled())
			 * selectValueInRecordCall(By.xpath(strTableDiscussion+
			 * strDiseaseIndication),"Disease Indication", "Clinical Study");
			 * 
			 * //
			 */
			break;

		case "New Medical Insight":
			strTableDiscussionType = "  New Medical Insight ";
			strTableDiscussion = "//label[.='" + strTableDiscussionType
					+ "']/parent::td/parent::tr/parent::tbody/parent::table";
			selectValueInRecordCall(By.xpath(strTableDiscussion + strTherapeuticArea), "Therapeutic Area",
					"New Medical Insight");
			selectValueInRecordCall(By.xpath(strTableDiscussion + strScientificInsight), "Scientific Insight",
					"New Medical Insight");
			/*
			 * /
			 * 
			 * cf.clickButton(By.xpath(strTableDiscussion+strEmailMedicalInsight
			 * ), "Email Medical Insight");
			 * cf.clickButton(objCallsForMedical.btnGo, "Go");
			 * cf.waitForSeconds(3);
			 * report.updateTestLog("Search Medical insight email",
			 * "Email medical insight popup is expected to be opened",
			 * Status.SCREENSHOT);
			 * cf.clickButton(objCallsForMedical.linkInsideSearch,
			 * "Search result"); selectDiseaseIndication =
			 * driver.findElement(By.xpath(strTableDiscussion+
			 * strDiseaseIndication)); if (selectDiseaseIndication.isEnabled())
			 * selectValueInRecordCall(By.xpath(strTableDiscussion+
			 * strDiseaseIndication),"Disease Indication", "Clinical Study"); //
			 */

			break;

		/*
		 * / Removing the below discussion topics based on recent changes case
		 * "Collaboration": strTableDiscussionType="  Collaboration ";
		 * strTableDiscussion="//label[.='"+strTableDiscussionType+
		 * "']/parent::td/parent::tr/parent::tbody/parent::table";
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiscussionIntent),"Discussion Intent","Collaboration");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+strCollaboration)
		 * ,"Collaboration","Collaboration");
		 * cf.clickButton(By.xpath(strTableDiscussion+strSearchProduct),
		 * "Search Product"); cf.setData(objCallsForMedical.searchText,
		 * "Search", strProductToSelect);
		 * cf.clickButton(objCallsForMedical.btnGo, "Go"); cf.waitForSeconds(3);
		 * cf.clickButton(objCallsForMedical.linkInsideSearch, "Search result");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strTherapeuticArea),"Therapeutic Area","Collaboration");
		 * selectDiseaseIndication =
		 * driver.findElement(By.xpath(strTableDiscussion+strDiseaseIndication))
		 * ; if (selectDiseaseIndication.isEnabled())
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiseaseIndication),"Disease Indication","Collaboration"); break;
		 * case "Disease Awareness":
		 * strTableDiscussionType="  Disease Awareness ";
		 * strTableDiscussion="//label[.='"+strTableDiscussionType+
		 * "']/parent::td/parent::tr/parent::tbody/parent::table";
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiscussionIntent),"Discussion Intent","Disease Awareness");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiseaseAwareness),"Disease Awareness","Disease Awareness");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strTherapeuticArea),"Therapeutic Area","Disease Awareness");
		 * selectDiseaseIndication =
		 * driver.findElement(By.xpath(strTableDiscussion+strDiseaseIndication))
		 * ; if (selectDiseaseIndication.isEnabled())
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiseaseIndication),"Disease Indication","Disease Awareness");
		 * break; case "Product Value":
		 * strTableDiscussionType="  Product Value ";
		 * strTableDiscussion="//label[.='"+strTableDiscussionType+
		 * "']/parent::td/parent::tr/parent::tbody/parent::table";
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiscussionIntent),"Discussion Intent","Product Value");
		 * cf.clickButton(By.xpath(strTableDiscussion+strSearchProduct),
		 * "Search Product"); cf.setData(objCallsForMedical.searchText,
		 * "Search", strProductToSelect);
		 * cf.clickButton(objCallsForMedical.btnGo, "Go"); cf.waitForSeconds(3);
		 * cf.clickButton(objCallsForMedical.linkInsideSearch, "Search result");
		 * 
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+strProductsValue)
		 * ,"Product Value","Product Value");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strProductSubValue),"Product Value Sub type","Product sub Value");
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strTherapeuticArea),"Therapeutic area","Therapeutic area");
		 * 
		 * selectDiseaseIndication =
		 * driver.findElement(By.xpath(strTableDiscussion+strDiseaseIndication))
		 * ; if (selectDiseaseIndication.isEnabled())
		 * selectValueInRecordCall(By.xpath(strTableDiscussion+
		 * strDiseaseIndication),"Disease Indication","Product Value"); break;
		 * 
		 * //
		 */
		}
	}

	public void selectValueInRecordCall(By by, String strElementDescription, String strDiscussionType) {
		try {
			if (cf.isElementPresent(by)) {
				Select selectElement = new Select(driver.findElement(by));
				List<WebElement> eOptions = selectElement.getOptions();
				int iOptionCount = eOptions.size();
				int iRandomNumber = cf.getRandomNumber(0, iOptionCount - 1);
				String strRandomOption = eOptions.get(iRandomNumber).getText();
				if (selectElement.getFirstSelectedOption().getText().trim().equalsIgnoreCase("--None--"))
					if (!strRandomOption.trim().equalsIgnoreCase("--None--")) {
						selectElement.selectByVisibleText(strRandomOption);
						report.updateTestLog("Select inside table",
								strRandomOption + " is selected for " + strElementDescription, Status.DONE);
					} else {
						selectElement.selectByVisibleText(eOptions.get(iRandomNumber + 1).getText());
						report.updateTestLog("Select inside table", selectElement.getFirstSelectedOption().getText()
								+ " is selected for " + strElementDescription, Status.DONE);
					}
				switch (strDiscussionType) {
				case "On-going Clinical Study":
					mapDiscussion_ongoingclinicalstudy.put(strElementDescription,
							selectElement.getFirstSelectedOption().getText());
					break;
				case "New Medical Insight":
					mapDiscussion_medicalinsight.put(strElementDescription,
							selectElement.getFirstSelectedOption().getText());
					break;
				/*
				 * / case "Clinical Study":
				 * mapDiscussion_clinicalstudy.put(strElementDescription,
				 * selectElement.getFirstSelectedOption().getText()); break;
				 * case "Collaboration":
				 * mapDiscussion_collaboration.put(strElementDescription,
				 * selectElement.getFirstSelectedOption().getText()); break;
				 * case "Disease Awareness":
				 * mapDiscussion_diseaseawareness.put(strElementDescription,
				 * selectElement.getFirstSelectedOption().getText()); break;
				 * case "Product Value":
				 * mapDiscussion_productvalue.put(strElementDescription,
				 * selectElement.getFirstSelectedOption().getText()); break; //
				 */
				}
			}

		} catch (Exception e) {
			report.updateTestLog("Error on selecting inside table",
					"Unable to select for : " + strElementDescription + "'.Error : " + e.getMessage(), Status.FAIL);
		}
	}

	public void selectSafetyAttestation() throws Exception {
		cf.selectData(objCallsForMedical.selectSafetyAttestation, "Safety Attestation",
				"No adverse event has been mentioned in any free-text field");
	}

	public void saveCall() throws Exception {
		cf.clickButton(objCallsForMedical.btnSave, "Save");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.visibilityOfElementLocated(objCallsForMedical.statusLabel));
		cf.waitForSeconds(15);
		WebElement status = driver.findElement(objCallsForMedical.statusLabel);
		if (status.getText().trim().equalsIgnoreCase("Saved"))
			report.updateTestLog("Save medical discussion", "Medical discussion status is saved", Status.PASS);
		else
			report.updateTestLog("Save medical discussion", "Medical discussion status is NOT saved", Status.FAIL);

	}

	public void verifyAccountDetailsInSavedPage() {
		cf.waitForSeconds(5);
		String strAccountInSavedPage = driver.findElement(objCallsForMedical.linkAccountNameInDetailsPage).getText()
				.trim();
		if (objCallsForMedical.strAccountNameForCalls.trim().contains(strAccountInSavedPage))
			report.updateTestLog("Verify Account In Saved Page",
					"'" + objCallsForMedical.strAccountNameForCalls + "' is displayed as expected", Status.PASS);
		else
			report.updateTestLog("Verify Account In Saved Page",
					"'" + objCallsForMedical.strAccountNameForCalls + "' is NOT displayed", Status.FAIL);
	}

	public void clickMedicalInquiryAndEnterData() {
		String strLocale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");

		try {
			cf.clickButton(objCallsForMedical.btnMoreActions, "More Actions");

			if (profile.equals("Commercial")) {
				if (strLocale.equals("IW") || strLocale.equals("EU1") || strLocale.equals("EU2")
						|| strLocale.equals("BR") || strLocale.equals("RU"))
					cf.clickButton(
							By.xpath(
									"//div[@class='popover bottom veeva-more-actions-top display_block']//a[.='Medical/Dx Inquiry']"),
							"Medical/Dx Inquiry");
				else
					cf.clickButton(
							By.xpath(
									"//div[@class='popover bottom veeva-more-actions-top display_block']//a[.='Medical Inquiry']"),
							"Medical Inquiry");

			}

			if (profile.equals("Medical")) {
				if (strLocale.equals("IW") || strLocale.equals("EU1") || strLocale.equals("EU2")
						|| strLocale.equals("BR") || strLocale.equals("IE") || strLocale.equals("RU")
						|| strLocale.equals("ANZ"))
					cf.clickButton(
							By.xpath(
									"//div[@class='popover bottom veeva-more-actions-top display_block']//a[.='Medical/Dx Inquiry']"),
							"Medical/Dx Inquiry");
				else
					cf.clickButton(
							By.xpath(
									"//div[@class='popover bottom veeva-more-actions-top display_block']//a[text()='Medical Inquiry']"),
							"Medical Inquiry");
			}

			if (cf.isElementPresent(By.xpath("//h2[.=' Select Medical Inquiry Record Type']"))) {
				if (strLocale.trim().equalsIgnoreCase("CA"))
					// cf.selectData(objCallsForMedical.selectMedicalInquiryType,
					// "Medical Inquiry Record Type", "E-mail - CA");
					cf.selectData(objCallsForMedical.selectMedicalInquiryType, "Medical Inquiry Record Type",
							"Medical Information Request");
				else
					cf.selectData(objCallsForMedical.selectMedicalInquiryType, "Medical Inquiry Record Type",
							"Medical Information Request");
				cf.clickButton(objCallsForMedical.btnContinueMedicalInquiry, "Continue");
			}

			if (cf.isElementPresent(By.xpath("//h2[.=' Select Medical/Dx Inquiry Record Type']"))) {

				cf.selectData(By.xpath("//select[@name='p3']"), "Medical Inquiry Record Type",
						"Medical Information Request");
				cf.clickButton(objCallsForMedical.btnContinueMedicalInquiry, "Continue");
			}

			/*
			 * if (strLocale.trim().equalsIgnoreCase("CA") ||
			 * strLocale.trim().equalsIgnoreCase("ANZ") ||
			 * strLocale.trim().equalsIgnoreCase("IC") ||
			 * strLocale.trim().equalsIgnoreCase("IW"))
			 * cf.switchToFrame(By.id("itarget")); else
			 */
			cf.switchToFrame(By.id("vod_iframe"));

			if (strLocale.trim().equalsIgnoreCase("CA")) {
				cf.selectData(objCallsForMedical.selectDeliveryMethod, "Delivery Method", "Email");
				cf.clickButton(objCallsForMedical.checksendToNewEmailMedicalInquiry, "New Email Id");
				cf.setData(objCallsForMedical.textNewEmailMedicalInquiry, "New Email ID", "test@test.com");

			} else
				cf.selectData(objCallsForMedical.selectDeliveryMethod, "Delivery Method", "Medical Affairs");
			Set<String> priorHandles = driver.getWindowHandles();
			String parentWindow = driver.getWindowHandle();
			cf.clickButton(objCallsForMedical.searchProductName, "Product Name Search");
			Set<String> newHandles = driver.getWindowHandles();
			if (newHandles.size() > priorHandles.size())
				for (String newHandle : newHandles)
					if (!priorHandles.contains(newHandle)) {
						driver.switchTo().window(newHandle);
						cf.switchToFrame(By.id("frameSearch"));
						cf.clickButton(objCallsForMedical.btnGoInMedicalInquiry, "Go");
						cf.switchToParentFrame();
						cf.switchToFrame(By.id("frameResult"));
						try {
							cf.clickButton(objCallsForMedical.linkSearchResultsInMedicalInquiry, "Search results");
							driver.switchTo().defaultContent();
						} catch (Exception e) {
							Set<String> currentHandles = driver.getWindowHandles();
							for (String currentHandle : currentHandles)
								if (currentHandle.trim().equalsIgnoreCase(parentWindow.trim())) {
									driver.getWebDriver().switchTo().window(parentWindow);
									report.updateTestLog("Product Search", "Switch to medical inquiry done",
											Status.SCREENSHOT);
								}
						}
					}

			cf.switchToParentFrame();

			/*
			 * if (strLocale.trim().equalsIgnoreCase("CA") ||
			 * strLocale.trim().equalsIgnoreCase("ANZ") ||
			 * strLocale.trim().equalsIgnoreCase("IC") ||
			 * strLocale.trim().equalsIgnoreCase("IW"))
			 * cf.switchToFrame(By.id("itarget")); else
			 */
			cf.switchToFrame(By.id("vod_iframe"));
			cf.setData(objCallsForMedical.textareaInquiry, "Inquiry text",
					"Testing purpose:" + (new Date()).toString());
			cf.clickButton(objCallsForMedical.btnSubmitInMedicalInquiry, "Submit in Medical Inquiry");
			// cf.clickButton(objCallsForMedical.btnSaveInMedicalInquiry,
			// "Submit in Medical Inquiry");
			cf.switchToParentFrame();
		} catch (Exception e) {
			report.updateTestLog("Error in Medical Inquiry", "Error occurred : " + e.getMessage(), Status.FAIL);
		}
	}

	public void editMedicalDiscussion() throws Exception {
		wait.until(ExpectedConditions.visibilityOfElementLocated(objCallsForMedical.btnEditInSavedPage));
		cf.clickButton(objCallsForMedical.btnEditInSavedPage, "Edit");
		cf.waitForSeconds(10);

	}
	
	public void editClinicalStudy1() throws Exception {

		cf.selectData(objCallsForMedical.selectClinicSupport, "Clinical Study Support Activity", "Site Referrals");
		enterDiscussionFields("On-going Clinical Study");
	}

	public void editClinicalStudy() throws Exception {

		cf.selectData(objCallsForMedical.selectClinicSupport, "Clinical Study Support Activity", "Site Referrals");
		enterDiscussionFields("On-going Clinical Study");
		cf.clickButton(objCallsForMedical.btnSubmit, "Submit");
		if (cf.isElementVisible(objCallsForMedical.errorPatientSafety, "clinicalStudyErrorMsg"))
			report.updateTestLog("Verify Error Message for Clinical study",
					"Error message is displayed for not selecting Patient Safety Attestation", Status.PASS);
		else

			report.updateTestLog("Verify Error Message for Clinical study",
					"Error message is not displayed for not selecting Patient Safety Attestation", Status.FAIL);

		selectSafetyAttestation();
		// cf.clickButton(objCallsForMedical.btnSubmit, "Submit");
		submitMedicalDiscussion();
		getValuesFromCreatedCall();
	}
	public void getValuesFromCreatedCall() throws Exception {
		try{
			String therapeuticArea = driver.findElement(By.xpath("//span[@name='MA_Therapeutic_Area_AZ__c']")).getText();
			String clinicalStudy = driver.findElement(By.xpath("//a[@name='MA_Clinical_Study_AZ__c']")).getText();
			dataTable.putData("CallsStandardizationMedical", "TherapeuticArea", therapeuticArea);
			dataTable.putData("CallsStandardizationMedical", "ClinicalStudy", clinicalStudy);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	public void submitMedicalDiscussion() throws Exception {
		wait.until(ExpectedConditions.visibilityOfElementLocated(objCallsForMedical.btnSubmit));
		cf.clickButton(objCallsForMedical.btnSubmit, "Submit");
		cf.waitForSeconds(30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(objCallsForMedical.statusLabel));
		WebElement status = driver.findElement(objCallsForMedical.statusLabel);
		String callReportId = driver.findElement(By.xpath("//h2[@class='pageDescription ng-binding']")).getText();
		dataTable.putData("Login", "CallReportId", callReportId);
		if (status.getText().trim().equalsIgnoreCase("Submitted"))
			report.updateTestLog("Submit medical discussion", "Medical discussion status is submitted", Status.PASS);
		else
			report.updateTestLog("Submit medical discussion", "Medical discussion status is NOT submitted",
					Status.FAIL);

	}

	public void selectProduct(String strProduct) throws Exception {

		if (cf.isElementVisible(By.xpath("(//div[@name='zvod_Detailing_vod__c']//input[@class='checkbox-indent'])[1]"),
				"Products")) {
			cf.clickButton(By.xpath("(//div[@name='zvod_Detailing_vod__c']//input[@class='checkbox-indent'])[1]"),
					"Products");
		}

		else {

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Add Other...')]")));
			cf.clickButton(By.xpath("//a[contains(text(),'Add Other...')]"), "Select Product");
			cf.setData(By.cssSelector("input#addOtherDetail"), "Search Product", strProduct);
			cf.waitForSeconds(2);

			cf.clickElement(By.xpath("(//div[@ng-repeat='result in searchResults']/input[@type='checkbox'])[1]"),
					"Search Results Checkbox");
			report.updateTestLog("Verify able to see Product is selected", "Able to see Product is selected",
					Status.SCREENSHOT);
			List<WebElement> addElements = driver.findElements(By.cssSelector("input[title='Add'][type='button']"));
			addElements.get(addElements.size() - 1).click();
		}
		if (cf.isElementVisible(
				By.xpath("(//div[@name='zvod_Key_Messages_vod__c']//input[@class='checkbox-indent'])[1]"),
				"Products")) {
			cf.clickButton(By.xpath("(//div[@name='zvod_Key_Messages_vod__c']//input[@class='checkbox-indent'])[1]"),
					"Products");
		}
		cf.selectData(By.xpath("//select[@ng-model='row.Reaction_vod__c']"), "Discussion Intent", "Proactive");
	}

	public void clickAccountInMedicalDiscussionsPage() throws Exception {
		cf.clickButton(By.cssSelector("div[name='Account_vod__c']>a"), "Account Name");

	}

	public void medicalDiscussionValidation() throws Exception {
		String CallReportidapp;
		int rows;
		String datetimecalls = "";
		String strcallReportID = dataTable.getData("Login", "CallReportId");
		String expectedTherapeuticArea = dataTable.getData("CallsStandardizationMedical", "TherapeuticArea");
		String expectedClinicalStudy = dataTable.getData("CallsStandardizationMedical", "ClinicalStudy");
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		VeevaFunctions vf = new VeevaFunctions(scriptHelper);
		datetimecalls = vf.formatDateTimeMarketWiseCalls(new Date(), strLocale, strProfile, "date");
		boolean callid  = false;
		String datesent = "";
		String strTherapeuticArea = "", strOngoingClinicalStudy = "";
		try {
			cf.clickSFLinkLet("Medical Discussions");
			if (cf.isElementVisible(
					By.xpath(
							"//td[@class='pbTitle']//*[starts-with(text(),'Medical Discussions')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"),
					"Go to List")) {
				WebElement element1 = driver.findElement(By.xpath(
						"//td[@class='pbTitle']//*[starts-with(text(),'Medical Discussions')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
				cf.clickElement(element1, "Go to list");
				cf.waitForSeconds(6);
				for (int i = 0; i < 2; i++) {
					if (cf.isElementPresent(By.xpath("//a[@title='Date - Sorted ascending']"))) {
						cf.clickElement(By.xpath("//a[@title='Date - Sorted ascending']"), "Sorting");
					}
				}
				rows = cf.rowscountexpanded("Medical Discussions");
				for (int j = 2; j <= rows + 1; j++) {
					CallReportidapp = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Interaction", j);
//					if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
//					else if (strLocale.trim().equalsIgnoreCase("CN"))
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
//					else
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Datetime", j);
					
					if(cf.isElementPresent(By.xpath("//a[contains(text(), Date) and contains(@title, 'Datetime')]")))
						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Datetime", j);
					else
						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
					
					if ((datetimecalls.toString().trim()).equals(datesent.trim())
							&& CallReportidapp.equals(strcallReportID)) {
						callid = true;
						strTherapeuticArea = cf.getDataFromLinkLetTableexpanded("Medical Discussions",
								"Therapeutic Area", j);
						 
						if (expectedTherapeuticArea.equalsIgnoreCase(strTherapeuticArea)){
							report.updateTestLog("Validate TherapeuticArea is displayed correct : " , expectedTherapeuticArea, Status.DONE);
							break;
						}
						else
							report.updateTestLog("Validate TherapeuticArea is displayed correct : " , expectedTherapeuticArea, Status.FAIL);
					}
				}
					
					
				for (int j = 2; j <= rows + 1; j++) {
					CallReportidapp = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Interaction", j);
					if(cf.isElementPresent(By.xpath("//a[contains(text(), Date) and contains(@title, 'Datetime')]")))
						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Datetime", j);
					else
						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
					if((datetimecalls.toString().trim()).equals(datesent.trim())
							&& CallReportidapp.equals(strcallReportID)){
						if (cf.isElementVisible(By.xpath("//th/a[text()='Clinical Study']"), "")){
							strOngoingClinicalStudy = cf.getDataFromLinkLetTableexpanded("Medical Discussions",
									"Clinical Study", j);
							if (expectedClinicalStudy.equalsIgnoreCase(strOngoingClinicalStudy))
								report.updateTestLog("Validate Clinical Study is displayed correct : " , expectedClinicalStudy, Status.DONE);
							else if (strOngoingClinicalStudy.equalsIgnoreCase(""))
								System.out.println("Clinical study is empty - expected");
							else
								report.updateTestLog("Validate Clinical Study is displayed correct : " , expectedClinicalStudy, Status.FAIL);
						
						}else{
							strOngoingClinicalStudy = cf.getDataFromLinkLetTableexpanded("Medical Discussions",
									"On-going Clinical Study", j);
							if (expectedClinicalStudy.equalsIgnoreCase(strOngoingClinicalStudy)){
								report.updateTestLog("Validate Clinical Study is displayed correct : " , expectedClinicalStudy, Status.DONE);
								break;
							}else if (strOngoingClinicalStudy.equalsIgnoreCase(""))
								System.out.println("Clinical study is empty - expected");
							else
								report.updateTestLog("Validate Clinical Study is displayed correct : " , expectedClinicalStudy, Status.FAIL);
						
						}
						
						
					}
					
				}
			} else if (cf.isElementVisible(By.xpath("//h3[.='Medical Discussions']"), null)) {
				rows = cf.getRowCountFromLinkLetTable("Medical Discussions");
				for (int j = 2; j <= rows + 1; j++) {
					CallReportidapp = cf.getDataFromLinkLetTable("Medical Discussions", "Interaction", j);
					if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
						datesent = cf.getDataFromLinkLetTable("Medical Discussions", "Date", j);
					else if (strLocale.trim().equalsIgnoreCase("CN"))
						datesent = cf.getDataFromLinkLetTable("Medical Discussions", "Date", j);
					else
						datesent = cf.getDataFromLinkLetTable("Medical Discussions", "Date", j);

					String datesentt[] = datesent.split(" ");
					if ((datetimecalls.trim()).equals(datesentt[0].trim())) {
						if (CallReportidapp.equals(strcallReportID)) {
							callid = true;
							strTherapeuticArea = cf.getDataFromLinkLetTable("Medical Discussions", "Therapeutic Area",
									j);
							if (strLocale.trim().equalsIgnoreCase("ANZ"))
								strOngoingClinicalStudy = cf.getDataFromLinkLetTable("Medical Discussions",
										"On-going Clinical Study", j);
							else
								strOngoingClinicalStudy = cf.getDataFromLinkLetTable("Medical Discussions",
										"Clinical Study", j);
							if (!strTherapeuticArea.trim().equalsIgnoreCase(""))
								validateMapValues(mapDiscussion_medicalinsight, "Therapeutic Area", strTherapeuticArea,
										"Therapeutic Area");
							if (!strOngoingClinicalStudy.trim().equalsIgnoreCase("")) {
								if (strLocale.trim().equalsIgnoreCase("ANZ"))
									validateMapValues(mapDiscussion_ongoingclinicalstudy, "On-going Clinical Study",
											strOngoingClinicalStudy, "Clinical Study");
								else
									validateMapValues(mapDiscussion_ongoingclinicalstudy, "Clinical Study",
											strOngoingClinicalStudy, "Clinical Study");
							}

						}
					}
				}
			}

			if (callid)
				report.updateTestLog("Medical discussion validation",
						"Medical discussion '" + strcallReportID + "' is found in related list", Status.SCREENSHOT);
			else
				report.updateTestLog("Medical discussion validation",
						"Medical discussion '" + strcallReportID + "' is not found in related list", Status.FAIL);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void medicalDiscussionValidationHCA() throws Exception {
		String CallReportidapp;
		int rows=0;
		String datetimecalls = "";
		String strcallReportID = dataTable.getData("Login", "CallReportId");
		String expectedTherapeuticArea = dataTable.getData("CallsStandardizationMedical", "TherapeuticArea");
		String expectedClinicalStudy = dataTable.getData("CallsStandardizationMedical", "ClinicalStudy");
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		VeevaFunctions vf = new VeevaFunctions(scriptHelper);
		datetimecalls = vf.formatDateTimeMarketWiseCalls(new Date(), strLocale, strProfile, "date");
		boolean callid  = false;
		String datesent = "";
		String strTherapeuticArea = "", strOngoingClinicalStudy = "";
		
			cf.clickSFLinkLet("Medical Discussions");
			if (cf.isElementVisible(
					By.xpath(
							"//td[@class='pbTitle']//*[starts-with(text(),'Medical Discussions')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"),
					"Go to List")) {
				WebElement element1 = driver.findElement(By.xpath(
						"//td[@class='pbTitle']//*[starts-with(text(),'Medical Discussions')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
				cf.clickElement(element1, "Go to list");
				cf.waitForSeconds(6);
				
				
				for (int j = 2; j <= 5; j++) {
					CallReportidapp = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Interaction", j);
//					if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
//					else if (strLocale.trim().equalsIgnoreCase("CN"))
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Date", j);
//					else
//						datesent = cf.getDataFromLinkLetTableexpanded("Medical Discussions", "Datetime", j);
					
					
					
					
						 
						if (CallReportidapp.equalsIgnoreCase(strcallReportID)){
							report.updateTestLog("Validate TherapeuticArea is displayed correct : " , expectedTherapeuticArea, Status.PASS);
							break;
						}
						else
							report.updateTestLog("Validate TherapeuticArea is displayed correct : " , expectedTherapeuticArea, Status.FAIL);
					}
				
				}
					
					
				
			} 

		

	public void validateMapValues(HashMap<String, String> mapToValidate, String strMapElement, String strValueToCompare,
			String strElementDesc) {
		if (mapToValidate.containsKey(strMapElement)) {
			if (mapToValidate.get(strMapElement).trim().equalsIgnoreCase(strValueToCompare))
				report.updateTestLog("Validate " + strElementDesc,
						strElementDesc + " is displayed correct : " + strValueToCompare, Status.DONE);
			else
				report.updateTestLog("Validate " + strElementDesc,
						strElementDesc + " is not displayed correct .Expected:" + mapToValidate.get(strMapElement)
								+ "; Actual : " + strValueToCompare,
						Status.FAIL);
		} else
			report.updateTestLog("Validate " + strElementDesc,
					strElementDesc + " is not created during Call Report creation", Status.FAIL);

	}

	public void medicalInquiryValidation() throws Exception {
		String CallReportidapp;
		int rows;
		String datetimecalls = "";
		String strcallReportID = dataTable.getData("Login", "CallReportId");
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		VeevaFunctions vf = new VeevaFunctions(scriptHelper);
		datetimecalls = vf.formatDateTimeMarketWiseCalls(new Date(), strLocale, strProfile, "date");
		boolean callid = false;
		String datesent = "";

		try {
			cf.clickSFLinkLet("Medical/Dx Inquiry");
			if (cf.isElementVisible(
					By.xpath(
							"//td[@class='pbTitle']//*[starts-with(text(),'Medical/Dx Inquiry')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"),
					"Go to List")) {
				WebElement element1 = driver.findElement(By.xpath(
						"//td[@class='pbTitle']//*[starts-with(text(),'Medical/Dx Inquiry')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
				cf.clickElement(element1, "Go to list");
				cf.waitForSeconds(6);
				for (int i = 0; i < 2; i++) {
					if (cf.isElementPresent(By.xpath("//a[@title='Date - Sorted ascending']"))) {
						cf.clickElement(By.xpath("//a[@title='Date - Sorted ascending']"), "Sorting");
					}
				}
				rows = cf.rowscountexpanded("Medical/Dx Inquiry");
				for (int j = 2; j <= rows + 1; j++) {
					CallReportidapp = cf.getDataFromLinkLetTableexpanded("Medical/Dx Inquiry", "Product Name", j);
					if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
						datesent = cf.getDataFromLinkLetTableexpanded("Medical/Dx Inquiry", "Created Date", j);
					else if (strLocale.trim().equalsIgnoreCase("CN"))
						datesent = cf.getDataFromLinkLetTableexpanded("Medical/Dx Inquiry", "Created Date", j);
					else
						// datesent =
						// cf.getDataFromLinkLetTableexpanded("Medical/Dx
						// Inquiry", "Datetime", j);
						datesent = cf.getDataFromLinkLetTableexpanded("Medical/Dx Inquiry", "Created Date", j);

					System.out.println("datetimecalls 1 : " + datetimecalls.toString().trim());
					System.out.println("datesent 1 : " + datesent.trim());

					if ((datetimecalls.toString().trim()).equals(datesent.trim())
							&& CallReportidapp.equals(strcallReportID)) {
						callid = true;
					}
				}
			} else if (cf.isElementVisible(By.xpath("//h3[.='Medical/Dx Inquiry']"), null)) {
				rows = cf.getRowCountFromLinkLetTable("Medical/Dx Inquiry");
				for (int j = 2; j <= rows + 1; j++) {
					CallReportidapp = cf.getDataFromLinkLetTable("Medical/Dx Inquiry", "Product Name", j);
					if (strProfile.trim().equalsIgnoreCase("Medical") && strLocale.trim().equalsIgnoreCase("JP"))
						datesent = cf.getDataFromLinkLetTable("Medical/Dx Inquiry", "Created Date", j);
					else if (strLocale.trim().equalsIgnoreCase("CN"))
						datesent = cf.getDataFromLinkLetTable("Medical/Dx Inquiry", "Created Date", j);
					else
						datesent = cf.getDataFromLinkLetTable("Medical/Dx Inquiry", "Created Date", j);

					String datesentt[] = datesent.split(" ");
					if ((datetimecalls.trim()).equals(datesentt[0].trim())) {
						if (CallReportidapp.equals(strcallReportID)) {

							report.updateTestLog("Medical discussion validation",
									"Medical discussion '" + strcallReportID + "' is found in related list",
									Status.SCREENSHOT);

						} else {
							report.updateTestLog("Medical discussion validation",
									"Medical discussion '" + strcallReportID + "' is not found in related list",
									Status.FAIL);
						}
					}
				}

			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void selectFromMyAccount() throws Exception {

		if (cf.isElementVisible(By.xpath("//table[@id='vodResultSet']/tbody/tr[2]/td/input[@type='checkbox']"),
				"First Account")) {
			driver.findElement(By.xpath("//table[@id='vodResultSet']/tbody/tr[2]/td/input[@type='checkbox']")).click();
			report.updateTestLog("Able to select the account", "Selecting account is successful", Status.PASS);
		} else {
			report.updateTestLog("Able to select the account", "Selecting account is failed", Status.FAIL);
		}
	}

	public void addSampleAndPromotionalItems() throws Exception {
		String strProducts = dataTable.getData("Login", "Products");

		cf.clickButton(By.xpath("//h3[.='Promotional']/parent::div/following-sibling::div//button"),
				"Add Promotional Line");
		int iProductCol = getColumnNumberFromTableWithHeader("call2_sample_vod__c", "Product");
		int iItemCol = getColumnNumberFromTableWithHeader("call2_sample_vod__c", "Item");
		// int iRowCount=
		// driver.findElements(By.cssSelector("[name='call2_sample_vod__c']
		// tr")).size()-2;

		String[] arrProducts = strProducts.split(";");
		for (int i = 0; i < arrProducts.length; i++) {
			cf.selectData(By.xpath(
					"//table[@name='call2_sample_vod__c']//tr[" + (i + 2) + "]/td[" + (iProductCol) + "]//select"),
					"Product Name", arrProducts[i]);
			selectValueInRecordCall(
					By.xpath(
							"//table[@name='call2_sample_vod__c']//tr[" + (i + 2) + "]/td[" + (iItemCol) + "]//select"),
					"Item", "None");
			if ((i + 1) < arrProducts.length)
				cf.clickButton(By.xpath("//h3[.='Promotional']/parent::div/following-sibling::div//button"),
						"Add Promotional Line");
		}
	}

	public int getColumnNumberFromTableWithHeader(String strTableDesc, String strColumnName) throws Exception {
		List<WebElement> headerRows = driver.findElements(By.cssSelector("[name='" + strTableDesc + "'] th"));
		for (int i = 0; i < headerRows.size(); i++)
			if (headerRows.get(i).getText().trim().equalsIgnoreCase(strColumnName.trim()))
				return i + 1;
			else if (i == headerRows.size() - 1) {
				report.updateTestLog("Find column number in Table", strColumnName + " column header is not found",
						Status.FAIL);
				return -1;
			}

		return -1;
	}

	public void createMedicalInformationRequest(String strLocale) throws Exception {
		if (!strLocale.equals("US")) {
			cf.switchToFrame(By.id("itarget"));
		} else {
			cf.clickSFLinkLet("Medical Information Request");
			// List<WebElement>
			// button=driver.findElements(By.xpath("//input[@value='New Medical
			// Information Request']"));
			// button.get(0).click();
			cf.clickButton(By.xpath("//input[@value='New Medical Information Request']"),
					"New Medical Information Request");
			cf.waitForSeconds(5);
			cf.switchToFrame(By.id("itarget"));

		}
		if (strLocale.trim().equalsIgnoreCase("CA")) {
			cf.selectData(objCallsForMedical.selectDeliveryMethod, "Delivery Method", "Email");
			cf.clickButton(objCallsForMedical.checksendToNewEmailMedicalInquiry, "New Email Id");
			cf.setData(objCallsForMedical.textNewEmailMedicalInquiry, "New Email ID", "test@test.com");
		} else if (strLocale.equals("US")) {
			cf.selectData(By.xpath("//select[@id='reqi-value:picklist:Medical_Inquiry_vod__c:Delivery_Method_vod__c']"),
					"Delivery Method", "Medical Affairs");
		} else
			cf.selectData(objCallsForMedical.selectDeliveryMethod, "Delivery Method", "Medical Affairs");
		report.updateTestLog("Create Medical Information Request", "Selected Delivery Method", Status.SCREENSHOT);
		Set<String> priorHandles = driver.getWindowHandles();
		String parentWindow = driver.getWindowHandle();
		cf.clickButton(objCallsForMedical.searchProductName, "Product Name Search");
		Set<String> newHandles = driver.getWindowHandles();
		if (newHandles.size() > priorHandles.size())
			for (String newHandle : newHandles)
				if (!priorHandles.contains(newHandle)) {
					driver.switchTo().window(newHandle);
					cf.switchToFrame(By.id("frameSearch"));
					if (strLocale.equals("US")) {
						String mirProductName = dataTable.getData("MedicalInformationRequest", "Product Name");
						cf.setData(By.name("searchBox"), "Search Box", mirProductName);
					}
					cf.clickButton(objCallsForMedical.btnGoInMedicalInquiry, "Go");
					cf.switchToParentFrame();
					cf.switchToFrame(By.id("frameResult"));
					report.updateTestLog("Enter product in MIR", "Product is searched in Medical Information Request",
							Status.SCREENSHOT);
					try {
						cf.clickButton(objCallsForMedical.linkSearchResultsInMedicalInquiry, "Search results");
						driver.switchTo().defaultContent();
					} catch (Exception e) {
						Set<String> currentHandles = driver.getWindowHandles();
						for (String currentHandle : currentHandles)
							if (currentHandle.trim().equalsIgnoreCase(parentWindow.trim())) {
								driver.getWebDriver().switchTo().window(parentWindow);
								report.updateTestLog("Product Search", "Switch to medical inquiry done",
										Status.SCREENSHOT);
							}
					}
				}

		cf.switchToParentFrame();
		cf.switchToFrame(By.id("itarget"));
		cf.setData(objCallsForMedical.textareaInquiry, "Inquiry text", "Testing purpose:" + (new Date()).toString());
		report.updateTestLog("Verify MIR Inquiry Text", "Verify if Inquiry Text is entered", Status.SCREENSHOT);
		if (strLocale.equals("US")) {
			cf.clickButton(objCallsForMedical.btnSaveInMedicalInquiry, "Submit in Medical Inquiry");
		} else {
			cf.clickButton(objCallsForMedical.btnSubmitInMedicalInquiry, "Submit in Medical Inquiry");
		}
		cf.switchToParentFrame();
	}
	
	public void emailMedicalInsight() throws Exception {
		
		String emailName = dataTable.getData("CallsStandardizationMedical", "EmailUser");
		cf.setData(By.id("MA_Email_Medical_Insight_to_AZ__c_searchText"), "Search Box", emailName);
		cf.clickButton(objCallsForMedical.go, "Go");
		cf.waitForSeconds(3);
		cf.clickButton(By.xpath("//td/a[text()='"+emailName+"']"), "Search results");
	}

	public void selectaccountSchedule() throws Exception {

		cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"), "MyAccounts List");
		cf.clickLink(By.xpath("//table[@id='vodResultSet']//tr[2]/td[1]/input"), "Account Check box");

		String account = driver.findElement(By.xpath("//table[@id='vodResultSet']//tr[2]/td[2]/a")).getText();

		// if (account.contains(","))
		// {
		// String[] account1 = account.split(",");
		// account = account1[1] + " " + account1[0];
		// System.out.println("Spilt using (,) Acocunt Name: " + account);
		// }
		dataTable.putData("CallsStandardizationMedical", "RandomAccount", account);

	}

	/**
	 * This method covers the alternate entry point for record call
	 */
	public void validateScheduleCall() throws Exception {

		String account = dataTable.getData("CallsStandardizationMedical", "RandomAccount");
		cf.clickButton(objCallsForMedical.scheduleCallButton, "Schedule a Call");
		cf.waitForSeconds(4);
		cf.clickElement(objCallsForMedical.scheduleCall, "Schedule");
		cf.waitForSeconds(20);

		cf.switchToFrame("vod_iframe");
		waits.until(ExpectedConditions.elementToBeClickable(objCallsForMedical.week));
		report.updateTestLog("Verify navigation of Calls Interaction page", "Inside Call Interaction page",
				Status.PASS);
		System.out.println("Go to call");
		cf.actionRightClick(By.xpath("//span[@class='fc-event-title-name' and contains(text(),'" + account + "')]"),
				"Right click on Scheduled call");
		cf.clickElement(By.xpath("//span[@class='fc-event-title-name' and contains(text(),'" + account + "')]"),
				"Scheduled Call");
		waits.until(ExpectedConditions.elementToBeClickable(objCallsForMedical.callInteractionHeader));
		if (cf.isElementPresent(By.xpath("//h1[@class='pageType ng-binding']"))) {
			report.updateTestLog("Verify nvaigation of Calls Interaction page", "Inside Call Interaction page",
					Status.PASS);
		} else {
			report.updateTestLog("Verify nvaigation of Calls Interaction page",
					"Unable to navigate to Call Interaction page", Status.FAIL);
		}
	}

	/**
	 * Validation of error message
	 */
	public void validateErrorNotification(String strMissingType) {
		/**********
		 * Save with empty information and validate mandatory field error
		 * messages
		 **********/

		/**********
		 * Save with no attendees and validate error message
		 ******************/

	}

	public void navigateToMyAccountsBreadCrumb() throws Exception {
		cf.clickButton(By.cssSelector("div.ptBreadcrumb > a"), "My Accounts");
	}

	public void getMDMIds() throws Exception {
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		String accounttype = dataTable.getData("Login", "AccountType");
		String account = dataTable.getData("Login", "AccountName");
		String[] acc = new String[5];
		String MDMID = "";
		if (account.contains(";")) {
			acc = account.split(";");
			account = acc[0];
			System.out.println("Spilt using (;) Acocunt Name: " + account);
		}

		int i = 1;
		for (String accountt : acc) {
			if (accountt.contains(",")) {
				String[] account1 = accountt.split(",");
				account = account1[1] + " " + account1[0];
				System.out.println("Spilt using (,) Acocunt Name: " + account);
			} else {
				account = accountt;
			}
			if (cf.isElementPresent(By.id("sen"))) {
				cf.selectData(By.id("sen"), "Search Module Dropdown", "Accounts");
				cf.setData(objHeaderPage.textSearch, "Search Box", account);
				if (cf.isElementPresent(By.xpath("//strong[contains(text(),'" + account + "')]")))
					cf.clickElement(By.xpath("//strong[contains(text(),'" + account + "')]"),
							"Search Account Suggestions");
				else
					cf.clickButton(objHeaderPage.btnGo, "Search GO");
			}

			cf.waitForVisible(driver, By.xpath("//h2[text()='Account Detail']"), 60);

			if (locale.equalsIgnoreCase("RU") || locale.equalsIgnoreCase("JP"))
				MDMID = driver.findElement(By.xpath("(//tr/td[text()='External ID']/following-sibling::td/div)[1]"))
						.getText();
			else
				MDMID = driver
						.findElement(By.xpath("(//tr/td[contains(text(),'MDM ID')]/following-sibling::td/div)[1]"))
						.getText();

			dataTable.putData("Login", "MDMID" + i, account + "-" + MDMID);
			i++;

		}
	}
}
