package businesscomponents;

import supportlibraries.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import com.mfa.framework.Status;
import ObjectRepository.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class veevaMyScheduleCalls extends VeevaFunctions
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	static String weekft;
	Date dNow = new Date();
	static int iDayRandom;
	//static String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
	static String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda']";
	static WebElement account;
	static String street;
	static String pincode;
	static String state;
	static String accounttext;
	Actions builder = new Actions(driver.getWebDriver());
	static String scheduledcallTime;
	static WebElement mouseovercall;
	String callaccount;
	String date;
	static int iDay_Position;
	static String accountdraganddrop;
	static String scheduledcallTimedraganddrop;
	static String finalaccountnamedrag;

	public veevaMyScheduleCalls(ScriptHelper scriptHelper)
	{
		super(scriptHelper);

	}

	public void clickMySchedule() throws Exception
	{
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
		{
			cf.waitForSeconds(2);
			clickVeevaTab("My Schedule");
			driver.manage().window().maximize();

			cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));
			
			if (cf.isElementVisible(ObjVeevaEmail.scheduletab, "Schedule Tab"))
				report.updateTestLog("Verify able to see Schedule Tab", "Clicking on Schedule link is successful", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to see Schedule Tab", "Clicking on Schedule link is unsuccessful", Status.FAIL);
			}

		}
	}

	public void checkviews() throws Exception
	{

		if (cf.isElementVisible(ObjVeevaEmail.dayView, "Able to see Day View"))
		{
			Boolean view = false;
			List<String> defaultlist = new ArrayList<String>(Arrays.asList("Day", "Week", "Month"));
			//	List<WebElement> views=driver.findElements(By.xpath("//div[@class='tabDiv']//a"));
			List<String> checklist = new ArrayList<String>();
			for (int a = 1; a < 4; a++)
			{
				String value = driver.findElement(By.xpath("//div[@class='tabDiv']//div[" + a + "]//a")).getText();
				checklist.add(value);
			}
			System.out.println(checklist);
			System.out.println(defaultlist);
			if (defaultlist.equals(checklist))
			{
				report.updateTestLog("Verify able to see all views", "Able to see " + checklist + " views", Status.PASS);
				view = true;
			}
			else
			{
				report.updateTestLog("Verify able to see all views", "Unable to see " + checklist + " views", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
			if (view == true)
			{
				String text = driver.findElement(By.xpath("//a[.='Day']/parent::div")).getAttribute("class");
				if (text.equals("tabAnchor currentView"))
				{
					report.updateTestLog("Verify Day view is selected by default", "Day view is selected by default", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify Day view is selected by default", "Day view is  not selected by default", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Day View", "Not able to see Day View", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void checkcurrentdate()
	{
		SimpleDateFormat ft = new SimpleDateFormat("EEEE, MMMM d, YYYY");
		SimpleDateFormat week = new SimpleDateFormat("EEEE");
		weekft = week.format(dNow).toString();

		String date = ft.format(dNow).toString();
		System.out.println(date);
		cf.waitForSeconds(10);
		String datetext = driver.findElement(By.className("fc-header-title")).getText();
		System.out.println(datetext);
		if (date.equals(datetext))
			report.updateTestLog("Verify able to see cureent date", "Able to see current date", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to see curent date", "Unable to see current date", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void checkweekView() throws Exception
	{
		Boolean weekviews = false;
		cf.clickLink(ObjVeevaEmail.weekView, "Week view");
		spinner();

		List<String> weeklist = new ArrayList<String>(Arrays.asList("Show work week", "Show full week"));
		List<String> checkweekradio = new ArrayList<String>();
		List<WebElement> weekradio = driver.findElements(By.xpath("//div[@class='view-week']//label"));
		for (int c = 1; c <= weekradio.size(); c++)
		{
			String value = driver.findElement(By.xpath("//div[@class='view-week']//label[" + c + "]/span")).getText();
			if (driver.findElement(By.xpath("//div[@class='view-week']//label[" + c + "]/input")).getAttribute("type").equals("radio"))
			{
				checkweekradio.add(value);
				weekviews = true;
			}
			else
			{
				break;
			}
		}

		if (weeklist.equals(checkweekradio))
		{
			report.updateTestLog("Verify able to see all views", "Able to see " + weeklist + " radio buttons", Status.PASS);
			if (weekviews == true)
			{
				String text = driver.findElement(ObjVeevaEmail.workweek).getAttribute("checked");
				if (text.equals("true"))
				{
					report.updateTestLog("Verify Show work week radio button is selected by default", "Show work week radiobutton is selected by default", Status.PASS);
				}
				else
				{
					report.updateTestLog("Verify Day Show work week radio button is selected by default", "Show work week radio button  is  not selected by default", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
		}
		else
		{
			report.updateTestLog("Verify able to see all views", "Unable to see " + weeklist + " radio buttons", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void currentdayhighlightweek()
	{
		Boolean highlightv = false;
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("d EEEE");
		String date = ft.format(dNow).toString();
		System.out.println(date);

		List<WebElement> days = driver.findElements(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//tr[1]//th//table"));

		for (int b = 2; b <= (days.size() + 1); b++)
		{

			String dleft = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//tr[1]//th[" + b + "]")).getText();
			//String dright=driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//tr[1]//th["+b+"]//td[2]")).getText();
			//String dayhighlight=dleft+" "+dright;
			String highlight = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//tr[1]//th[" + b + "]")).getAttribute("class");
			if (date.equals(dleft))
			{
				if (highlight.endsWith("today"))
				{
					highlightv = true;
				}
			}
		}

		if (highlightv == true)
			report.updateTestLog("Verify current date is highlighted in week view", "Current Date is highlighted in week view", Status.PASS);
		else
		{
			report.updateTestLog("Verify current date is highlighted in week view", "Current Date is not  highlighted in week view", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void verifyFullweek() throws Exception
	{

		String strDataSheet = "Login";
		String location = dataTable.getData(strDataSheet, "Locale");
		Boolean day = false;
		cf.pageRefresh();
		cf.waitForSeconds(10);
		cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"), "vod_iframe");
		List<WebElement> links = driver.findElements(By.xpath("//a"));
		System.out.println(links.size());
		for (WebElement link : links)
		{
			String text = link.getText().trim();
			System.out.println(text);
			if (text.equals("Week"))
			{
				cf.clickLink(By.xpath("//a[.='" + text + "']"), text);
				break;
			}
		}
		cf.clickElement(ObjVeevaEmail.fullweek, "Clicked on Full week Radio Button");
		cf.implicitWait(60, TimeUnit.SECONDS);
		cf.waitForSeconds(12);
		spinner();
		List<WebElement> days = driver.findElements(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda' and not (contains(@style,'display: none;'))]/div[@class='fc-agenda-head']//tr[1]//th"));
		System.out.println(days.size());
		String stday = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda' and not (contains(@style,'display: none;'))]/div[@class='fc-agenda-head']//tr[1]//th[2]//td[2]")).getText();
		String enday = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda' and not (contains(@style,'display: none;'))]/div[@class='fc-agenda-head']//tr[1]//th[" + (days.size() - 1) + "]//td[2]")).getText();

		if (location.equals("EU1") || location.equals("EU2") || location.equals("ANZ") || location.equals("RU") || location.equals("BR") || location.equals("IC") || location.equals("IW"))
		{
			if (stday.contains("Mon"))
			{
				if (enday.contains("Sun"))
				{
					day = true;
				}
			}
			if (day == true)
			{
				report.updateTestLog("Verify the start day as Monday and end day day is Sunday in the Week", "Able to see start day as Monday and end day day as Sunday in the Week", Status.PASS);
				cf.switchToParentFrame();

			}
			else
			{
				report.updateTestLog("Verify the start day as Monday and end day day is Sunday in the Week", "Unable to see start day as Monday and end day day as Sunday in the Week", Status.FAIL);
			}

		}

		else
		{
			if (stday.contains("Sun"))
			{
				if (enday.contains("Sat"))
				{
					day = true;
				}
			}
			if (day == true)
			{
				report.updateTestLog("Verify the start day is Sunday and end day day is Saturday in the Week", "Able to see start day as Sunday and end day day as Saturday in the Week", Status.PASS);
				cf.switchToParentFrame();
			}
			else
			{
				report.updateTestLog("Verify the start day is Sunday and end day day is Saturday in the Week", "Unable to see start day as Sunday and end day day as Saturday in the Week", Status.FAIL);
			}

		}

	}

	public void verifyNewCall() throws Exception
	{
		Boolean schedulecalls = false;
		Boolean view = false;
		String scheduleoption = null;
		cf.waitForSeconds(10);
		WebElement ele = driver.findElement(ObjVeevaEmail.workweek);
		cf.clickElement(ele, "work week Radio Button");
		//String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
		By byDay = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt]");
		//		By byTime = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr");

		By byCalendar_LeftMost = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[1]");
		int iDayCount = driver.findElements(byDay).size();
		//	int itimeCount = driver.findElements(byTime).size();
		iDayRandom = 0;
		String time = new SimpleDateFormat("HH").format(dNow).toString();
		int timen = Integer.parseInt(time);
		
		for (int i = 1; i <= iDayCount; i++)
		{

			String weekname = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[2]")).getText();
			if (weekft.equals(weekname))
			{
				iDayRandom = i;
				WebElement delete = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[3]/div"));
				if (cf.isElementVisible(delete))
				{
					cf.clickElement(delete, "Delete all planned calls");
					driver.switchTo().alert().accept();
					schedulecalls = true;
				}

				break;
			}

		}
		if (schedulecalls == true)
		{
			if (timen <= 18)
			{
				int iTimeRandom = cf.getRandomNumber(timen * 2, 30);//int iTimeRandom = cf.getRandomNumber(1,itimeCount);

				By byDayPosition = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]");
				By byTimePostion = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/td");

				By byDayValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]//tr");
				String strDayValue = cf.getData(byDayValue, "Day Value", "text");
				System.out.println("strDayValue: " + strDayValue);
				By byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/th");
				String strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
				if (strTimeValue.equals(""))
				{
					byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + (iTimeRandom - 1) + "]/th");
					strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
					strTimeValue = strTimeValue.replace("00", "30");
				}
				System.out.println("strTimeValue: " + strTimeValue);

				String strDataSheet = "Login";
				String locale = dataTable.getData(strDataSheet, "Locale");
				String profile = dataTable.getData(strDataSheet, "Profile");
				if (locale.equals("CN"))
				{
					if (strTimeValue.contains("PM") || strTimeValue.contains("AM"))
					{
						String endtime = addMinsToDatechina(strTimeValue, 30);
						scheduledcallTime = strTimeValue + " - " + endtime;
						System.out.println(scheduledcallTime);
					}
				}
				else if ((locale.equals("IE") && profile.equals("Commercial")) || (locale.equals("CA")) || locale.equals("US") || locale.equals("EU2"))
				{
					System.out.println(strTimeValue + " -strTimeValue");
					if (strTimeValue.contains("PM") || strTimeValue.contains("AM"))
					{
						String endtime = vf.addMinsToDate12(strTimeValue, 30);
						scheduledcallTime = strTimeValue + " - " + endtime;
					}
				}
				else if (locale.equals("RU"))
				{
					//String starttime=timeconvertToRailwayTimeru(sttime);
					String endtime = addMinsToDateru(strTimeValue, 30);
					scheduledcallTime = strTimeValue + " - " + endtime;
				}
				else
				{
					String endtime = vf.addMinsToDate(strTimeValue, 30);
					scheduledcallTime = strTimeValue + " - " + endtime;
				}

				///Find x offset Width to move
				int iCalendar_LeftMost_Position = driver.findElement(byCalendar_LeftMost).getLocation().getX();
				System.out.println("iCalendar_LeftMost_Position: " + iCalendar_LeftMost_Position);

				iDay_Position = driver.findElement(byDayPosition).getLocation().getX();
				System.out.println(strDayValue + " - iDay_Position: " + iDay_Position);
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				WebElement eTime = driver.findElement(byTimePostion);
				int iTimeWidth = eTime.getSize().getWidth();
				System.out.println(iTimeWidth);
				cf.scrollToElement(byTimePostion, "byTimePostion: " + strTimeValue, 50);
				// Wait for Loading Spinner to disappear
				spinner();

				cf.waitForSeconds(6);

				builder.moveToElement(eTime).moveByOffset(iDay_Position - 370, -10).contextClick().perform();

				List<WebElement> calls = driver.findElements(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li"));
				view = false;
				scheduleoption = "New Call";
				if (calls.size() > 0)
				{
					for (int a = 1; a <= calls.size(); a++)
					{
						String value = driver.findElement(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[" + a + "]")).getText();
						if (scheduleoption.equals(value))
						{
							view = true;
							report.updateTestLog("Verify able to see call option", "Able to see " + scheduleoption + " option", Status.PASS);
							break;
						}
					}
				}

			}

			if (view == true)
				report.updateTestLog("Verify able to see call option", "Able to see " + scheduleoption + " option", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to see call option", "Unable to see " + scheduleoption + " option", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}

		else
		{
			report.updateTestLog("Verify able to create call with in working hrs", "Can't create call after working hrs", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public String addMinsToDatechina(String startTime, int mins) throws ParseException
	{
		SimpleDateFormat sdf2 = new SimpleDateFormat("ah:mm");
		Date date1 = new SimpleDateFormat("ah:mm").parse(startTime);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.MINUTE, mins); //Number of Days to add	 	    
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public void selectAccount() throws Exception
	{
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		String account = dataTable.getData("Login", "AccountName");
		if (account.contains(";"))
		{
			String[] acc = account.split(";");
			account = acc[0];
			System.out.println("Spilt using (;) Acocunt Name: " + account);
		}
		if (account.contains(","))
		{
			String[] account1 = account.split(",");
			account = account1[1] + " " + account1[0];
			System.out.println("Spilt using (,) Acocunt Name: " + account);
		}

		else if (locale.equals("RU"))
		{
			if (account.contains(" "))
			{
				String account1[] = account.split(" ");
				account = account1[1] + " " + account1[0];
			}
			else
				account = account.trim();
		}
		else if (locale.equals("JP"))
		{
			account = account.trim();
		}
		try
		{
			searchAccount(locale, profile, account);
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void searchAccount(String locale, String profile, String account) throws Exception
	{
		if (cf.isElementVisible(ObjVeevaEmail.searchacccall, "Input  in Search"))
		{
			cf.setData(ObjVeevaEmail.searchacccall, "Input  in Search", account);
			cf.clickButton(ObjVeevaEmail.gosearchcall, "GO");

			cf.waitForSeconds(3);
			if (cf.isElementVisible(By.xpath("//div[text()='No matches found.']"), "No matches found"))
			{
				for (int i = 1; i <= 2; i++)
				{
					account = dataTable.getData("Login", "AccountName");
					if (account.contains(";"))
					{
						String[] acc = account.split(";");
						account = acc[i];
						System.out.println("Spilt using (;) Acocunt Name: " + account);
					}
					if (account.contains(","))
					{
						String[] account1 = account.split(",");
						account = account1[1] + " " + account1[0];
						System.out.println("Spilt using (,) Acocunt Name: " + account);
					}
					else if (locale.equals("RU") && profile.equals("Commercial"))
					{
						String account1[] = account.split(" ");
						account = account1[1] + " " + account1[0];
					}
					else if (locale.equals("JP"))
					{
						account = account.substring(2, 5) + account.substring(0, 2);
					}

					cf.setData(ObjVeevaEmail.searchacccall, "Input  in Search", account);
					cf.clickButton(ObjVeevaEmail.gosearchcall, "GO");

					if (cf.isElementPresent(By.xpath("//div[text()='No matches found.']")) != true)
					{
						break;
					}
				}

			}
		}

		else
		{
			report.updateTestLog("Verify able to see search Box for entering the account", "Unable to see search Box for entering the account", Status.FAIL);
		}
	}

	public void createcall() throws Exception
	{
		Boolean select = false;
		account = null;
		String serachvalue = dataTable.getData("Login", "AccountName");
		String locale = dataTable.getData("Login", "Locale");

		if (serachvalue.contains(";"))
		{
			String[] acc = serachvalue.split(";");
			serachvalue = acc[0];
			System.out.println("Spilt using (;) Acocunt Name: " + serachvalue);
		}
		if (serachvalue.contains(","))
		{
			String[] serachvalue1 = serachvalue.split(",");
			serachvalue = serachvalue1[1] + " " + serachvalue1[0];
			System.out.println("Spilt using (,) Acocunt Name: " + serachvalue);
		}

		else if (locale.equals("RU"))
		{
			serachvalue = serachvalue.trim();
		}
		else if (locale.equals("JP"))
		{
			serachvalue = serachvalue.trim();
		}

		serachvalue = serachvalue.trim();
		cf.waitForSeconds(6);
		cf.clickLink(ObjVeevaEmail.newwCall, "New Call");
		if (cf.isElementVisible(ObjVeevaEmail.searchacccall, "Input in search"))
		{
			selectAccount();
			cf.waitForSeconds(12);
			if (cf.isElementVisible(By.xpath("//table[@id='selectRows']//tr"), "Table"))
			{
				List<WebElement> checkaccount = driver.findElements(By.xpath("//table[@id='selectRows']//tr"));
				if (!locale.equals("BR"))
				{
					for (int e = 1; e <= checkaccount.size(); e++)
					{
						account = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[2]/a"));
						accounttext = account.getText();
						String[] accounttext1 = accounttext.split("@");
						accounttext = accounttext1[0].trim();

						//Change on Aug 16
						if (accounttext.contains(serachvalue))
						{

							WebElement ele = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[1]/input[@type='checkbox']"));

							if (ele.getAttribute("checked") == null)
							{
								cf.clickElement(ele, "Select an account");
							}
							select = true;
							break;
						}
					}
				}
				else
				{
					for (int e = 1; e <= checkaccount.size(); e++)
					{
						account = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[2]"));
						accounttext = account.getText();
						//Change on Aug 16
						if (accounttext.contains(serachvalue) && accounttext.contains("@"))
						{
							WebElement ele = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[1]/input[@type='checkbox']"));

							if (ele.getAttribute("checked") == null)
							{
								cf.clickElement(ele, "Select an account");
							}
							select = true;
							break;
						}
					}
				}
			}
			else
			{
				report.updateTestLog("Verify able to see the account searched", "Unable to see the account searched", Status.FAIL);
			}

			if (select == true)
			{
				Boolean button = false;
				report.updateTestLog("Verify able to select an account for a call", "Able to select an account for a call", Status.PASS);
				if (cf.verifyElementPresent(ObjVeevaEmail.addCall, "Add Button"))
				{
					if (cf.verifyElementPresent(ObjVeevaEmail.cancelcall, "Cancel Button"))
						button = true;
				}
				if (button == true)
				{
					report.updateTestLog("Verify able to see Add and Cancel Buttons", "Able to see Add and Cancel Buttons", Status.PASS);
					cf.clickButton(ObjVeevaEmail.addCall, "Add");
					spinner();
					cf.waitForSeconds(15);
					mouseOverCallRecord();
					if ((cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='Call Report']"), "Call Report")) || (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='Customer Activity']"), "Customer Activity")) || (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='MSL Interaction']"), "MSL Interaction")) || (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='AZ Call Detail PRC']"), "AZ Call Detail PRC"))
						|| (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='Selling']"), "Selling")) || (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[text()='Diagnostic Interaction']"), "Diagonastic Interaction")) || (cf.isElementVisible(By.xpath("//div[@class='vodtooltip']//div[.='MA Interaction']"), "MA Interaction")))

					{
						report.updateTestLog("Verify to see details of call", "Able to see details of call", Status.PASS);

					}
					else
					{
						report.updateTestLog("Verify to see details of call", "Unable to see details of call", Status.FAIL);
					}
				}
				else
				{
					report.updateTestLog("Verify able to see Add and Cancel Buttons", "Unable to see Add and Cancel Buttons", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to select an account for a call", "Unable to select an account for a call", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}

		else
		{
			report.updateTestLog("Verify able to see account search box", "Unable to see account search box", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		
		
		// Changes done by Nishok - Temporary changes
		String profile = dataTable.getData("Login", "Profile");
		if(profile.equalsIgnoreCase("Medical")){
			clickVeevaTab("My Schedule");
			cf.waitForSeconds(15);
			cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));
			
		}
	}

	public void createCallEngage() throws Exception
	{
		Boolean select = false;
		account = null;
		String serachvalue = dataTable.getData("Login", "AccountName");
		String locale = dataTable.getData("Login", "Locale");

		if (serachvalue.contains(";"))
		{
			String[] acc = serachvalue.split(";");
			serachvalue = acc[0];
			System.out.println("Spilt using (;) Acocunt Name: " + serachvalue);
		}
		if (serachvalue.contains(","))
		{
			String[] serachvalue1 = serachvalue.split(",");
			serachvalue = serachvalue1[1] + " " + serachvalue1[0];
			System.out.println("Spilt using (,) Acocunt Name: " + serachvalue);
		}

		else if (locale.equals("RU"))
		{
			serachvalue = serachvalue.trim();
		}
		else if (locale.equals("JP"))
		{
			serachvalue = serachvalue.trim();
		}

		serachvalue = serachvalue.trim();
		cf.waitForSeconds(6);
		cf.clickLink(ObjVeevaEmail.newwCall, "New Call");
		if (cf.isElementVisible(ObjVeevaEmail.searchacccall, "Input in search"))
		{
			selectAccount();
			cf.waitForSeconds(12);
			if (cf.isElementVisible(By.xpath("//table[@id='selectRows']//tr"), "Table"))
			{
				List<WebElement> checkaccount = driver.findElements(By.xpath("//table[@id='selectRows']//tr"));
				if (!locale.equals("BR"))
				{
					for (int e = 1; e <= checkaccount.size(); e++)
					{
						account = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[2]/a"));
						accounttext = account.getText();
						String[] accounttext1 = accounttext.split("@");
						accounttext = accounttext1[0].trim();

						//Change on Aug 16
						if (accounttext.contains(serachvalue))
						{

							WebElement ele = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[1]/input[@type='checkbox']"));

							if (ele.getAttribute("checked") == null)
							{
								cf.clickElement(ele, "Select an account");
							}
							select = true;
							break;
						}
					}
				}
				else
				{
					for (int e = 1; e <= checkaccount.size(); e++)
					{
						account = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[2]"));
						accounttext = account.getText();
						//Change on Aug 16
						if (accounttext.contains(serachvalue) && accounttext.contains("@"))
						{
							WebElement ele = driver.findElement(By.xpath("//table[@id='selectRows']//tr[" + e + "]//td[1]/input[@type='checkbox']"));

							if (ele.getAttribute("checked") == null)
							{
								cf.clickElement(ele, "Select an account");
							}
							select = true;
							break;
						}
					}
				}
			}
			else
			{
				report.updateTestLog("Verify able to see the account searched", "Unable to see the account searched", Status.FAIL);
			}

			if (select == true)
			{
				Boolean button = false;
				report.updateTestLog("Verify able to select an account for a call", "Able to select an account for a call", Status.PASS);
				if (cf.verifyElementPresent(ObjVeevaEmail.addCall, "Add Button"))
				{
					if (cf.verifyElementPresent(ObjVeevaEmail.cancelcall, "Cancel Button"))
						button = true;
				}
				if (button == true)
				{
					report.updateTestLog("Verify able to see Add and Cancel Buttons", "Able to see Add and Cancel Buttons", Status.PASS);
					cf.clickButton(ObjVeevaEmail.addCall, "Add");
					spinner();
					cf.waitForSeconds(15);
					mouseOverCallRecordEngage();
					
				}
				else
				{
					report.updateTestLog("Verify able to see Add and Cancel Buttons", "Unable to see Add and Cancel Buttons", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to select an account for a call", "Unable to select an account for a call", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}

		else
		{
			report.updateTestLog("Verify able to see account search box", "Unable to see account search box", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}
	
	public void mouseOverCallRecordEngage() throws Exception
	{

		Boolean mouseover = false;
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String accountn = dataTable.getData(strDataSheet, "AccountName");
		String profile = dataTable.getData(strDataSheet, "Profile");

		List<WebElement> callaccounts = driver.findElements(By.xpath("//span[@class='fc-event-title-name']"));
		//for (int d = 0; d < callaccounts.size(); d++)
			
		int tot = callaccounts.size();
		System.out.println(tot);
		for (int d = tot - 1; d >= 0; d--)
		{
			System.out.println(callaccounts.size());
			System.out.println(accounttext);

			if (accounttext.contains(","))
			{
				String[] accounttext1 = accounttext.split(",");
				accounttext = accounttext1[1] + " " + accounttext1[0];
				System.out.println("Spilt using (,) Acocunt Name: " + accounttext);
			}

			else if (locale.equals("RU"))
			{
				if (accounttext.contains(" "))
				{
					String accounttext1[] = accounttext.split(" ");
					accounttext = accounttext1[1] + " " + accounttext1[0];
				}
				else
					accounttext = accounttext.trim();
			}
			else if (locale.equals("JP"))
			{
				accounttext = accounttext.substring(2, 5) + accounttext.substring(0, 2);
			}

			String recordedcall = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']")).getText();
			System.out.println(recordedcall);

			if (recordedcall.contains(","))
			{
				String[] accounttext1 = recordedcall.split(",");
				recordedcall = accounttext1[1] + " " + accounttext1[0];
				System.out.println("Spilt using (,) Acocunt Name: " + recordedcall);
			}

			if (locale.equals("CA") && (profile.equals("Medical")) || (locale.equals("RU") && profile.equals("Medical")))
			{
				if ((recordedcall.trim()).contains(accounttext.trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
					mouseovercall = driver.findElement(call);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					cf.waitForSeconds(3);
					String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}
				}
			}

			else if (locale.equals("JP"))
			{
				if ((recordedcall.trim()).contains(accountn.substring(0, 2).trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
					mouseovercall = driver.findElement(call);
					cf.waitForSeconds(8);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}
				}
			}

			else if (locale.equals("RU"))
			{
				if (accounttext != null)
				{
					if ((recordedcall.trim()).contains(accounttext.trim()))
					{
						By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
						mouseovercall = driver.findElement(call);
						Actions builder = new Actions(driver.getWebDriver());
						builder.moveToElement(mouseovercall).perform();
						cf.waitForSeconds(3);
						String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
						date = dateformat();
						String dateandtime = date + ", " + scheduledcallTime;
						System.out.println(dateandtime);
						System.out.println(calltime);
						if ((dateandtime.trim()).equals(calltime.trim()))
						{
							mouseover = true;
							break;
						}
					}
				}
			}
			else if (accounttext != null)
			{
				if ((accounttext.trim()).equals(recordedcall.trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");

					mouseovercall = driver.findElement(call);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					cf.waitForSeconds(3);
					mouseover = true;
					String calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip']//tbody/tr[1]/td[1])[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}
				}
			}
			else if ((accounttext.trim()).equals(recordedcall.trim()))
			{
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
				date = dateformat();
				String dateandtime = date + ", " + scheduledcallTime;
				System.out.println(dateandtime);
				System.out.println(calltime);
				if ((dateandtime.trim()).equals(calltime.trim()))
				{
					mouseover = true;
					break;
				}
			}

		}

		if (mouseover == true)
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Able to mouseover and validated the details on created call", Status.PASS);
			
			cf.actionRightClick(mouseovercall, "Right click on Scheduled call");
			cf.clickElement(mouseovercall, "Scheduled Call");
		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Unable to mouseover and validated the details  on created call", Status.FAIL);
		}

	}
	public void mouseOverCallRecord() throws Exception
	{

		Boolean mouseover = false;
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String accountn = dataTable.getData(strDataSheet, "AccountName");
		String profile = dataTable.getData(strDataSheet, "Profile");

		List<WebElement> callaccounts = driver.findElements(By.xpath("//span[@class='fc-event-title-name']"));
		for (int d = 0; d < callaccounts.size(); d++)
		{
			System.out.println(callaccounts.size());
			System.out.println(accounttext);

			if (accounttext.contains(","))
			{
				String[] accounttext1 = accounttext.split(",");
				accounttext = accounttext1[1] + " " + accounttext1[0];
				System.out.println("Spilt using (,) Acocunt Name: " + accounttext);
			}

			else if (locale.equals("RU"))
			{
				if (accounttext.contains(" "))
				{
					String accounttext1[] = accounttext.split(" ");
					accounttext = accounttext1[1] + " " + accounttext1[0];
				}
				else
					accounttext = accounttext.trim();
			}
			else if (locale.equals("JP"))
			{
				accounttext = accounttext.substring(2, 5) + accounttext.substring(0, 2);
			}

			String recordedcall = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']")).getText();
			System.out.println(recordedcall);

			if (recordedcall.contains(","))
			{
				String[] accounttext1 = recordedcall.split(",");
				recordedcall = accounttext1[1] + " " + accounttext1[0];
				System.out.println("Spilt using (,) Acocunt Name: " + recordedcall);
			}

			if (locale.equals("CA") && (profile.equals("Medical")) || (locale.equals("RU") && profile.equals("Medical")))
			{
				if ((recordedcall.trim()).contains(accounttext.trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
					mouseovercall = driver.findElement(call);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					cf.waitForSeconds(3);
					String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}
				}
			}

			else if (locale.equals("JP"))
			{
				if ((recordedcall.trim()).contains(accountn.substring(0, 2).trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
					mouseovercall = driver.findElement(call);
					cf.waitForSeconds(8);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}
				}
			}

			else if (locale.equals("RU"))
			{
				if (accounttext != null)
				{
					if ((recordedcall.trim()).contains(accounttext.trim()))
					{
						By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
						mouseovercall = driver.findElement(call);
						Actions builder = new Actions(driver.getWebDriver());
						builder.moveToElement(mouseovercall).perform();
						cf.waitForSeconds(3);
						String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
						date = dateformat();
						String dateandtime = date + ", " + scheduledcallTime;
						System.out.println(dateandtime);
						System.out.println(calltime);
						if ((dateandtime.trim()).equals(calltime.trim()))
						{
							mouseover = true;
							break;
						}
					}
				}
			}
			else if (accounttext != null)
			{
				if ((accounttext.trim()).equals(recordedcall.trim()))
				{
					//By call = By.xpath("//div[@class='fc-agenda-body']/div/div[1]//span[@class='fc-event-title-name']");
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");

					mouseovercall = driver.findElement(call);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					cf.waitForSeconds(3);
					mouseover = true;
					/*String calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip' and contains(@style,'display: none;')]//tbody/tr[1]/td[1])[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTime;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if ((dateandtime.trim()).equals(calltime.trim()))
					{
						mouseover = true;
						break;
					}*/
				}
			}
			else if ((accounttext.trim()).equals(recordedcall.trim()))
			{
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
				date = dateformat();
				String dateandtime = date + ", " + scheduledcallTime;
				System.out.println(dateandtime);
				System.out.println(calltime);
				if ((dateandtime.trim()).equals(calltime.trim()))
				{
					mouseover = true;
					break;
				}
			}

		}

		if (mouseover == true)
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Able to mouseover and validated the details on created call", Status.PASS);
			
			cf.actionRightClick(mouseovercall, "Right click on Scheduled call");
			cf.clickElement(mouseovercall, "Scheduled Call");
		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Unable to mouseover and validated the details  on created call", Status.FAIL);
		}

	}

	public void verifyAccounts() throws Exception
	{

		String strDataSheet = "Login";
		String view = dataTable.getData(strDataSheet, "View");

		cf.selectData(ObjVeevaEmail.accountselectionschedular, "Selecting Account from Schedular", view);

		int size = driver.findElements(By.xpath("//tbody[@id='accountsTableBody']/tr")).size();
		if (size > 0)
			report.updateTestLog("Verify able to see accounts are displayed ", "Able to see Accounts", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to see accounts are displayed ", "Unable to see Accounts", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void verifycallDetails()
	{
		Boolean calldetails = false;
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String accountn = dataTable.getData(strDataSheet, "AccountName");
		String profile = dataTable.getData(strDataSheet, "Profile");

		callaccount = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[2]/td[1]")).getText();
		System.out.println(callaccount);
		String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
		date = dateformat();
		String dateandtime = date + ", " + scheduledcallTime;
		System.out.println(dateandtime);
		if (locale.equals("JP"))
		{
			if ((callaccount).contains(accountn))
			{
				if (dateandtime.contains(calltime))
				{
					String hcpstatus;
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					System.out.println("hcpstatus-" + hcpstatus);
					if ((hcpstatus.contains("Planned")) || (hcpstatus.contains("Past")) || (hcpstatus.contains("Planned (Teal)")) || (hcpstatus.contains("Teal")) || (hcpstatus.contains("Planned (Past)")))
					{
						calldetails = true;
					}
				}
			}
		}
		else if (locale.equals("RU") && profile.equals("Commercial"))
		{
			String[] accountnn = accountn.split(" ");
			if ((callaccount).contains(accountnn[0]))
			{
				if (dateandtime.contains(calltime))
				{
					String hcpstatus;
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					System.out.println(size);
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					if (hcpstatus.contains("Planned") || hcpstatus.contains("Past") || hcpstatus.contains("Planned (Teal)") || hcpstatus.contains("Teal") || hcpstatus.contains("Planned (Past)"))
					{
						calldetails = true;

					}
				}
			}
		}
		else if (locale.equals("US"))
		{
			String accountnameformat[] = accounttext.split(" ");
			String value = accountnameformat[1] + ", " + accountnameformat[0];
			System.out.println(value);
			if (callaccount.equals(value))
				if (dateandtime.contains(calltime))
				{
					String hcpstatus;
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					System.out.println(size);
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					System.out.println(hcpstatus);
					if (hcpstatus.contains("Planned") || hcpstatus.contains("Past") || hcpstatus.contains("Planned (Teal)") || hcpstatus.contains("Teal") || hcpstatus.contains("Planned (Past)"))
					{
						calldetails = true;

					}
				}
		}
		else if ((callaccount).equals(accounttext))
		{
			if (dateandtime.contains(calltime))
			{
				int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
				String hcastatus;
				hcastatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
				if (hcastatus.contains("Planned") || hcastatus.contains("Past") || hcastatus.contains("Planned (Teal)") || hcastatus.contains("Teal") || hcastatus.contains("Planned (Past)"))
				{
					calldetails = true;
				}
			}
		}
		else if (accounttext != null)
		{
			if ((callaccount).contains(accounttext))
			{
				if (dateandtime.contains(calltime))
				{
					cf.waitForSeconds(5);
					String hcpstatus;
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					if (hcpstatus.contains("Planned") || hcpstatus.contains("Past") || hcpstatus.contains("Planned (Teal)") || hcpstatus.contains("Teal") || hcpstatus.contains("Planned (Past)"))
					{
						calldetails = true;
					}
				}
			}
		}

		if (calldetails == true)
			report.updateTestLog("Validate details of call", "Able to validate the staus of call", Status.PASS);
		else
		{
			report.updateTestLog("Validate details of call", "Unable to validate the status of call", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void optonScheduledcall() throws Exception
	{
		Boolean optionvisible = false;
		String locale = dataTable.getData("Login", "Locale");
		//mouseOverCallRecord();
		cf.actionRightClick(mouseovercall, "Right click on Scheduled call");
		cf.waitForSeconds(4);
		//Previously when right clicked the following options are seen:- Open Call, Open Account, Color, Delete, Forward as Email, Map this address, View Recent Activity		
		List<String> optionsCall = new ArrayList<String>(Arrays.asList("Open Call", "Open Account", "Delete", "Forward as Email", "Map this address", "View Recent Activity"));
		cf.scrollToBottom();
		System.out.println(optionsCall.size());
		for (int i = 0; i < optionsCall.size(); i++)
		{
			String option = optionsCall.get(i).toString();
			WebElement optionele = driver.findElement(By.xpath("//a[.='" + option + "']"));
			if (cf.isElementPresent(optionele, "Option"))
			{
				System.out.println(option);
				optionvisible = true;
			}
			else
			{
				break;
			}
		}

		if (optionvisible == true)
		{
			report.updateTestLog("Validate the options on clicking on a scheduled call", "Able to validate the options on a scheduled call", Status.PASS);
			cf.waitForSeconds(15);
			//WebElement openCall=driver.findElement(By.xpath("//a[.='Open Call']"));
			try
			{
				driver.findElement(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all fg-menu-flyout']//a[.='Open Call']")).click();
			}
			catch (Exception e)
			{
				List<WebElement> opencall = driver.findElements(By.xpath("//a[.='Open Call']"));
				System.out.println(opencall.size());
				String litext = opencall.get(1).getText();
				System.out.println(litext);
				opencall.get(1).click();
			}
			//cf.clickElement(openCall, "Open Call");
			cf.waitForSeconds(25);
			if (cf.isElementVisible(ObjVeevaEmail.callreportpage, "Call Report Page") || cf.isElementVisible(ObjVeevaEmail.jpCalldetail, "AZ Call Detail PRC") || cf.isElementVisible(ObjVeevaEmail.mslPage, "MSL Interaction Page") || cf.isElementVisible(ObjVeevaEmail.custAct, "Customer Activity Page") || cf.isElementVisible(ObjVeevaEmail.selAct, "Selling") || cf.isElementVisible(ObjVeevaEmail.dxlcall, "Diagonastic Interaction"))
			{
				report.updateTestLog("Verify able to navigate to Call Report Page", "Able to navigate to Call Report or MSL Interaction Page or AZ Call Detail PRC", Status.PASS);
				//navigation();
				if (!locale.equals("US"))
				{
					cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@name='Cancel']"), "Cancel");
					cf.waitForSeconds(4);
					cf.switchToFrame(ObjVeevaEmail.sframe);
					if (cf.isElementVisible(ObjVeevaEmail.dayView, "Day view"))
						report.updateTestLog("Verify able to navigate to My Schedule Calls Page", "Able to navigate to My Schedule Calls Page", Status.PASS);
					else
					{
						report.updateTestLog("Verify able to navigate to My Schedule Calls Page", "Unable to navigate to My Schedule Calls Page", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}

				}
			}
			else
			{
				report.updateTestLog("Verify able to navigate to Call Report Page", "Unable to navigate to Call Report Page", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}

		}
		else
		{
			report.updateTestLog("Validate the options on clicking on a scheduled call", "Unable to validate the options on a scheduled call", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		
		
		
	}

	public void monthView() throws Exception
	{
		
		cf.clickLink(ObjVeevaEmail.monthView, "Month View");
		cf.waitForSeconds(12);
		spinner();
		if (cf.isElementVisible(By.xpath("//div[@id='calendar']"), "Calendar"))
		{
			List<WebElement> calls = driver.findElements(By.xpath("//div[@class='fc-view fc-view-month fc-grid']//span[@class='fc-event-title-name']"));
			if (calls.size() > 0)
				report.updateTestLog("Verify able to see calls in the month view", "Able to see calls in the month view", Status.SCREENSHOT);
			else
			{

				report.updateTestLog("Verify able to see calls in the month view", "There are no planned calls in the month view", Status.SCREENSHOT);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see monthly view", "Unable to see Mmonthly view", Status.FAIL);
		}

	}

	public void schedularsection() throws Exception
	{

		String strDataSheet = "Login";
		String view = dataTable.getData(strDataSheet, "View");
		cf.waitForSeconds(7);
		cf.clickLink(ObjVeevaEmail.weekView, "Week View");
		if (cf.isElementVisible(ObjVeevaEmail.schedularSection, "Scheduler Section"))
		{
			report.updateTestLog("Verify able to see Schedular section", "Able to see Schedular section", Status.PASS);
			cf.selectData(ObjVeevaEmail.accountselectionschedular, "Selecting Account from Schedular", view);
			cf.waitForSeconds(8);
			//draganddropaccount();
		}
		else
		{
			report.updateTestLog("Verify able to see Schedular section", "Able to see Schedular section", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void draganddropaccount() throws Exception
	{
		try
		{
			//	By byTime = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr");

			By byCalendar_LeftMost = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[1]");
			//int iDayCount = driver.findElements(byDay).size();
			//		int itimeCount = driver.findElements(byTime).size();
			//iDayRandom = 0;
			String time = new SimpleDateFormat("HH").format(dNow).toString();
			int timen = Integer.parseInt(time);
			if (timen <= 18)
			{
				int iTimeRandom = cf.getRandomNumber(timen * 2, 30);//int iTimeRandom = cf.getRandomNumber(1,itimeCount);
				By byTimePostion = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/td");
				By byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/th");
				String strTimeValuedraganddrop = cf.getData(byTimeValue, "Time Value", "text");
				if (strTimeValuedraganddrop.equals(""))
				{
					byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + (iTimeRandom - 1) + "]/th");
					strTimeValuedraganddrop = cf.getData(byTimeValue, "Time Value", "text");
					strTimeValuedraganddrop = strTimeValuedraganddrop.replace("00", "30");
				}
				System.out.println("strTimeValuedraganddrop: " + strTimeValuedraganddrop);
				String strDataSheet = "Login";
				String location = dataTable.getData(strDataSheet, "Locale");
				String profile = dataTable.getData(strDataSheet, "Profile");
				if (location.equals("CN"))
				{
					if (strTimeValuedraganddrop.contains("PM") || strTimeValuedraganddrop.contains("AM"))
					{
						String endtime = addMinsToDatechina(strTimeValuedraganddrop, -30);
						scheduledcallTimedraganddrop = endtime + " - " + strTimeValuedraganddrop;
					}
				}

				else if ((location.equals("IE") && profile.equals("Commercial")) || (location.equals("CA")) || location.equals("EU2") || location.equals("US") && profile.equals("Medical"))
				{
					if (strTimeValuedraganddrop.contains("PM") || strTimeValuedraganddrop.contains("AM"))
					{
						String endtime = vf.addMinsToDate12(strTimeValuedraganddrop, -30);
						scheduledcallTimedraganddrop = endtime + " - " + strTimeValuedraganddrop;
					}
				}
				else
				{
					String endtime = vf.addMinsToDate(strTimeValuedraganddrop, -30);
					scheduledcallTimedraganddrop = endtime + " - " + strTimeValuedraganddrop;
				}

				///Find x offset Width to move
				int iCalendar_LeftMost_Position = driver.findElement(byCalendar_LeftMost).getLocation().getX();
				System.out.println("iCalendar_LeftMost_Position: " + iCalendar_LeftMost_Position);

				//iDay_Position = driver.findElement(byDayPosition).getLocation().getX();
				//System.out.println(strDayValue + " - iDay_Position: " + iDay_Position);		
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				WebElement eTimedraganddrop = driver.findElement(byTimePostion);
				int iTimeWidth = eTimedraganddrop.getSize().getWidth();
				System.out.println(iTimeWidth);
				cf.scrollToElement(byTimePostion, "byTimePostion: " + strTimeValuedraganddrop, 50);
				WebElement source = driver.findElement(By.xpath("//tbody[@id='accountsTableBody']/tr[1]/td[1]"));
				accountdraganddrop = driver.findElement(By.xpath("//tbody[@id='accountsTableBody']/tr[1]/td[1]/div/h3")).getText();
				builder.clickAndHold(source).moveToElement(eTimedraganddrop).moveByOffset(iDay_Position - 370, -10).release().build().perform();

				mouseoverdrapanddrop();

			}
			else
			{
				report.updateTestLog("Verify able to create call with in working hrs", "Can't create call after working hrs", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void mouseoverdrapanddrop() throws Exception
	{
		cf.waitForSeconds(5);
		Boolean mouseoverdraganddrop = false;
		String strDataSheet = "Login";
		String location = dataTable.getData(strDataSheet, "Locale");
		List<WebElement> calldraganddropaccounts = driver.findElements(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div//span[@class='fc-event-title-name']"));
		for (int d = 0; d < calldraganddropaccounts.size(); d++)
		{
			System.out.println(calldraganddropaccounts.size());
			String hcpstatus;
			String hcastatus;
			if (accountdraganddrop.contains("@"))
			{
				String accountcallname[] = accountdraganddrop.split("@");
				if (location.equals("CN"))
				{
					finalaccountnamedrag = accountcallname[0];
				}
				else
				{
					String accountnameformat[] = accountcallname[0].split(" ");
					finalaccountnamedrag = accountnameformat[1] + ", " + accountnameformat[0];
				}
			}
			String recordedcall = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']")).getText();
			System.out.println(recordedcall);
			if (!(finalaccountnamedrag == null))
			{
				if ((finalaccountnamedrag.trim()).equals(recordedcall.trim()))
				{
					By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
					mouseovercall = driver.findElement(call);
					Actions builder = new Actions(driver.getWebDriver());
					builder.moveToElement(mouseovercall).perform();
					cf.waitForSeconds(5);
					String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					System.out.println(size);
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					date = dateformat();
					String dateandtime = date + ", " + scheduledcallTimedraganddrop;
					System.out.println(dateandtime);
					System.out.println(calltime);
					if (dateandtime.equals(calltime))
					{
						if (hcpstatus.contains("Planned") || hcpstatus.contains("Past"))
						{
							mouseoverdraganddrop = true;
							break;
						}
					}
				}
			}

			else if ((accountdraganddrop.trim()).equals(recordedcall.trim()))
			{
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']/div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);
				String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
				int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//div[@class='vodtooltipbody']/table/tbody/tr")).size();
				System.out.println(size);

				hcastatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();

				date = dateformat();
				String dateandtime = date + ", " + scheduledcallTimedraganddrop;
				System.out.println(dateandtime);
				System.out.println(calltime);
				if (dateandtime.equals(calltime))
				{
					if (hcastatus.contains("Planned") || hcastatus.contains("Past"))
					{
						mouseoverdraganddrop = true;
						break;
					}
				}
			}
		}
		if (mouseoverdraganddrop == true)
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Able to mouseover and validated the details on  drag and dropped created call", Status.PASS);
			cf.switchToParentFrame();
		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the created call", "Unable to mouseover and validated the details  on drag and dropped  created call", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void previousandNextweek() throws Exception
	{

		//	cf.switchToFrame(By.id("itarget"));
		String currentweek = driver.findElement(By.xpath("//h2[@class='fc-header-title']")).getText();
		String splitweek[] = currentweek.split("�");
		String startday = splitweek[0];
		//cf.clickifElementPresent(By.xpath("//a[@id='vodNextBtn']//img[@class='nextCalArrow' and @title='Next']"), "NextWeek");
		cf.clickElement(By.xpath("//a[@id='vodNextBtn']/img"), "NextWeek");
		cf.waitForSeconds(8);
		String nextweekstartdays = addDaysToDatepreviousNext(startday, 7);
		String nexttweek = driver.findElement(By.xpath("//h2[@class='fc-header-title']")).getText();
		String nextweek[] = nexttweek.split("�");
		String nextweekstartday = nextweek[0];
		System.out.println(nextweekstartday);
		System.out.println(nextweekstartdays);

		if ((nextweekstartday.trim()).contains(nextweekstartdays.trim()))
		{
			report.updateTestLog("Verify able to validate the Next week Button ", "Able to validate the next week Button", Status.PASS);
			Boolean dadzc = cf.isElementVisible(By.xpath("//a[@id='vodPrevBtn']/img"), "Previous");
			System.out.println(dadzc);
			cf.clickElement(By.xpath("//a[@id='vodPrevBtn']/img"), "PreviousWeek");
			cf.waitForSeconds(8);
			System.out.println("Clicked Previous Button");
			String previousweek = driver.findElement(By.xpath("//h2[@class='fc-header-title']")).getText();
			//String previousweek = addDaysToDatepreviousNext(nextweekstartday, -7);
			if ((previousweek.trim()).contains(startday.trim()))
			{
				report.updateTestLog("Verify able to validate the previous week button ", "Able to validate the previous week button", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to validate the previous week button", "Unable to validate the previous week button", Status.FAIL);
			}
		}

		else
		{
			report.updateTestLog("Verify able to validate the Next week Button ", "Unable to validate the next week Button", Status.FAIL);
			//frameworkparameters.setStopExecution(true); 
		}

	}

	public String addDaysToDatepreviousNext(String strStartDate, int iDays) throws ParseException
	{
		SimpleDateFormat sdf2 = new SimpleDateFormat("MMMM d");
		Date date1 = new SimpleDateFormat("MMMM d").parse(strStartDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		cal.add(Calendar.DAY_OF_MONTH, iDays); //Number of Days to add	 	    
		String nextweekstartdate = sdf2.format(cal.getTime());
		return nextweekstartdate;
	}

	public void spinner() throws Exception
	{
		By calendar_Spinner = By.xpath("//div[@id='calendarSpinner'][@style]");
		cf.waitUntilElementContains(calendar_Spinner, "Calendar Spinner", "style", "display: none;", 40);
	}

	public void verifyCalldetailssaveus() throws Exception
	{
		Boolean calldetails = false;
		String strDataSheet = "Login";
		//		String strDataSheet1="MyScheduleCalls";
		cf.switchToFrame(ObjVeevaEmail.sframe, "I target");
		mouseOverCallRecord();
		String locale = dataTable.getData(strDataSheet, "Locale");
		callaccount = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[2]/td[1]")).getText();
		System.out.println(callaccount);
		String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[1]/td[1]")).getText();
		date = dateformat();
		String dateandtime = date + ", " + scheduledcallTime;
		System.out.println(dateandtime);
		if (locale.equals("US"))
		{
			String accountnameformat[] = accounttext.split(" ");
			String value = accountnameformat[1] + ", " + accountnameformat[0];
			System.out.println(value);
			if (callaccount.equals(value))
				if (dateandtime.contains(calltime))
				{
					String hcpstatus;
					int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
					System.out.println(size);
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					if (hcpstatus.contains("Saved"))
					{
						calldetails = true;

					}
				}
		}
		if (calldetails == true)
			report.updateTestLog("Validate details of call", "Able to validate the saved status of call", Status.PASS);
		else
		{
			report.updateTestLog("Validate details of call", "Unable to validate the saved status of call", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}
}
