package businesscomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_Studies extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_Studies(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public static String surveyName;

	//Creating new survey for employee by logging with Ops admin.
	public void createNewStudy() throws Exception
	{
		vf.clickVeevaTab("Studies");
		if (cf.isElementVisible(cf.getSFElementXPath("New", "pbButton"), "New Button"))
		{
			cf.clickSFButton("New", "pbButton");
			String strDataSheet = "Studies";
			if (vf.verifyVeevaPageOpened("New Study", "Study Edit"))
			{
				String strCountry_Code = dataTable.getData(strDataSheet, "Country_Code");
				String strStudyName = "Study_" + cf.createUnique();
				dataTable.putData(strDataSheet, "Study_Name", strStudyName);
				cf.setSFData("Study Name", "textbox", strStudyName);
				cf.scrollToBottom();
				cf.clickSFCheckBox("Active", "Following", true);
				cf.setSFData("Medical Affairs", "Select", "Yes");
				cf.setSFData("Country Code", "textbox", strCountry_Code);
				//report.updateTestLog("createNewStudy", "create New Study Screenshot", Status.SCREENSHOT);
				cf.clickSFButton("Save", "pbButtonb");

				//By byCountryCode = By.xpath("//td[contains(@class,'labelCol')][normalize-space(text())='Country Code']/following::td[1][contains(@class,'dataCol')]");

				if (vf.verifyVeevaPageOpened(strStudyName, "Study"))
				{
					//Verify Study Name
					//By byStudyName = cf.getSFElementXPath("Study Name", "text");
					By byStudyName = By.xpath("//td[@class='labelCol'][normalize-space(text())='Study Name']/following::td[1][starts-with(@class,'dataCol')]");
					cf.verifyPageElement(byStudyName, "Study Name", "text", strStudyName, "Exact", false);
					By byMedicalAffairs = By.xpath("//td[@class='labelCol'][normalize-space(text())='Medical Affairs']/following::td[1][starts-with(@class,'dataCol')]");
					cf.verifyPageElement(byMedicalAffairs, "Medical Affairs", "text", "Yes", "Exact", false);

					By byCountryCode = cf.getSFElementXPath("Country Code", "text");
					cf.verifyPageElement(byCountryCode, "Country Code", "text", strCountry_Code, "Exact", false);
					cf.verifyCheckBoxStatus("Active", "Following", true);

					By byMedicalInsight_Lnk = cf.getSFElementXPath("New Medical Insight", "pbButton");
					if (cf.isElementVisible(byMedicalInsight_Lnk, "New Medical Insight"))
					{
						/* Core 28 -VRM 6120*/
						//When a new Medical Insight is added directly from a Study page, the Study name field should auto-populate in the Insight page.
						cf.scrollToElement(By.xpath("//input[@value='New Medical Insight']"), "Medical Insight");
						cf.clickSFButton("New Medical Insight", "pbButton");
						cf.waitForSeconds(2);
						report.updateTestLog("Medical Insight", "Medical Insight page is loaded", Status.PASS);
						Select therapyArea = new Select(driver.findElement(By.xpath("//label[contains(text(),'Therapeutic Area')]/parent::span/parent::td/following-sibling::td//select")));
						therapyArea.selectByVisibleText("Autoimmune");
						String summary = "Medical Insight Summary";
						cf.clickElement(By.xpath("//label[contains(text(),'Medical Insight Summary')]/parent::span/parent::td/following-sibling::td//input"), "Medical Insight Summary");
						cf.setData(By.xpath("//label[contains(text(),'Medical Insight Summary')]/parent::span/parent::td/following-sibling::td//input"), "Medical Insight Summary", summary);
						String description = "Description about medical insight";
						cf.switchToFrame(By.xpath("//*[@id='cke_1_contents']/iframe"));
						cf.clickElement(By.xpath("//html[@dir='ltr']//body"), "Medical Insight Description");
						cf.setData(By.xpath("//html[@dir='ltr']//body"), "Medical Insight Description", description);
						//cf.setData(By.xpath("//*[contains(@class,'cke_editable cke_editable_themed cke_contents_ltr cke_show_borders')]"),"Medical Insight Description", description);
						cf.switchToParentFrame();
						cf.scrollToTop();
						Select venue = new Select(driver.findElement(By.xpath("//label[contains(text(),'Venue')]/parent::span/parent::td/following-sibling::td//select")));
						venue.selectByVisibleText("Study Site");
						report.updateTestLog("Medical Insight", "Medical Insight values are entered", Status.SCREENSHOT);
						cf.scrollToBottom();
						//Updated by Murali - Xpath changes
						Select attest = new Select(driver.findElement(By.xpath("//label[text()='Insight Patient Safety Attestation']/parent::span/parent::td/following-sibling::td//select")));
						attest.selectByVisibleText("No adverse event has been mentioned in any free-text field");
						// verify if the respective study is auto populated
						By autoStudy = By.xpath("//span[@class='lookupInput']/a[@title='Study Lookup (New Window)']//preceding-sibling::input");
						//By autoStudy = By.cssSelector("[tabindex='13']"); //span[@class='lookupInput']/a[@title='Study Lookup (New Window)']//preceding-sibling::input[@tabindex='14']
						report.updateTestLog("Medical Insight", "Medical Insight values are entered", Status.PASS);
						if (cf.isElementPresent(autoStudy))
						{
							String autoStudyID = driver.findElement(autoStudy).getAttribute("Value");
							System.out.println(autoStudyID);
							System.out.println(strStudyName);
							if (autoStudyID.equals(strStudyName))
								report.updateTestLog("Study Auto populated", "Study value is auto populated in the medical insight that is created from the studies page", Status.DONE);
							else
								report.updateTestLog("Study Auto populated", "Study value is not auto populated in the medical insight that is created from the studies page", Status.FAIL);

						}
						else
						{
							report.updateTestLog("Study Auto populated", "Study field is missing", Status.FAIL);
						}
						cf.clickButton(By.xpath("//td[@id='bottomButtonRow']//input[@name='save']"), "Save");
						cf.waitForSeconds(10);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Medical/Dx Insights']")));
						By savedMedInsightStudy = By.name("MA_Clinical_Trial_AZ__c");
						//div[@id='Name_ileinner']
						String savedautoStudyID = driver.findElement(savedMedInsightStudy).getText();
						if (savedautoStudyID.equals(strStudyName))
							report.updateTestLog("Study Auto populated", "Study value is retained in the saved medical insight page", Status.PASS);
						else
							report.updateTestLog("Study Auto populated", "Study value is not retained in the saved medical insight page", Status.FAIL);
						cf.clickLink(savedMedInsightStudy, "Study Link");
						/*Core 28 change ends*/
					}
					else
					{
						report.updateTestLog("create New Study", "Medical Insight Link not present", Status.DONE);
					}

					////Create Study Site
					cf.clickSFButton("New Study Site", "pbButton");
					String strStudy_Site_Name = strStudyName + "_Site";
					dataTable.putData(strDataSheet, "Study_Site_Name", strStudy_Site_Name);
					cf.setSFData("Study Site Name", "textbox", strStudy_Site_Name);
					cf.setSFData("Country Code", "textarea", strCountry_Code);
					cf.clickSFCheckBox("Active", "Following", true);
					report.updateTestLog("createNewStudy", "create New Study Site Screenshot", Status.SCREENSHOT);
					cf.clickSFButton("Save", "pbButtonb");
					if (vf.verifyVeevaPageOpened(strStudy_Site_Name, "Study Site"))
					{
						//						By byStudySiteName = cf.getSFElementXPath("Study Site Name", "text");
						By byStudySiteName = By.xpath("//td[@class='labelCol'][normalize-space(text())='Study Site Name']/following::td[1][starts-with(@class,'dataCol')]");
						cf.verifyPageElement(byStudySiteName, "Study Site Name", "text", strStudy_Site_Name, "Exact", false);
						By bySiteCountryCode = cf.getSFElementXPath("Country Code", "text");
						cf.verifyPageElement(bySiteCountryCode, "Country Code", "text", strCountry_Code, "Exact", false);
						cf.verifyCheckBoxStatus("Active", "Following", true);
					} //Study Site Name
				} //Study Name
			} //Study Edit
		}
		else
		{//New Button Not Found
			throw new FrameworkException("New Button not found in Studies Page");
		}
	}//createNewStudy()

	public void mslUserRecordACallWithStudy() throws Exception
	{
		cf.clickSFButton("Record a Call", "pbButton");
		String strDataSheet = "Studies";
		if (vf.verifyVeevaPageOpened("New MSL Interaction", "MSL Interaction"))
		{
			String strInteraction_Channel = dataTable.getData(strDataSheet, "Interaction_Channel");
			String strDuration_Mins = dataTable.getData(strDataSheet, "Duration_Mins");
			cf.setSFData("Interaction Channel", "Select", strInteraction_Channel);
			cf.setSFData("Duration (mins)", "Select", strDuration_Mins);

			String strLocale = dataTable.getData("Login", "Locale");
			
			By byClinicalStudy_AddDiscussion = By.xpath("//div[@name='zvod_Medical_Discussion_vod__c']//label[contains(text(),'Clinical Study' )]/following-sibling::input");
			cf.clickButton(byClinicalStudy_AddDiscussion, "Clinical Study - Add Discussion");
			String strStudy_Name = dataTable.getData(strDataSheet, "Study_Name");
			String strStudy_Site_Name = dataTable.getData(strDataSheet, "Study_Site_Name");
			String strDiscussion_Intent = dataTable.getData(strDataSheet, "Discussion_Intent");

			cf.setSFData("Discussion Intent", "Select", strDiscussion_Intent);
			By byClinicalStudy_Lookup = cf.getSFElementXPath("Clinical Study", "lookupicon");
			By byClinicalStudySite_Lookup = cf.getSFElementXPath("Clinical Study Site", "lookupicon");
			cf.scrollToElement(byClinicalStudy_Lookup, "ClinicalStudy_Lookup", 400);
			vf.getLookupDataWithSearch(byClinicalStudy_Lookup, "Clinical Study Lookup Icon", strStudy_Name);
			vf.getLookupDataWithSearch(byClinicalStudySite_Lookup, "Clinical Study Site Lookup Icon", strStudy_Site_Name);

			String strClinical_Study_Support_Activity = dataTable.getData(strDataSheet, "Clinical_Study_Support_Activity");
			String strPatient_Safety_Attestation = dataTable.getData(strDataSheet, "Patient_Safety_Attestation");
			cf.setSFData("Clinical Study Support Activity", "Select", strClinical_Study_Support_Activity);
			cf.setSFData("Patient Safety Attestation", "Select", strPatient_Safety_Attestation);

			report.updateTestLog("Record a Call with Study", "Call Record - Full Screenshot", Status.SCREENSHOT, "Full Screen", driver);
			cf.clickSFButton("Submit", "pbButton");
			cf.waitForSeconds(15);
			if (cf.isElementVisible(cf.getSFElementXPath("Unlock", "pbButtonb"), "Unlock Button"))
			{
				//Verify
				By byDuration_Mins = cf.getSFElementXPath("Duration (mins)", "text");
				By byInteraction_Channel = cf.getSFElementXPath("Interaction Channel", "text");
				By byDiscussion_Intent = cf.getSFElementXPath("Discussion Intent", "text");
				By byClinical_Study_Support_Activity = cf.getSFElementXPath("Clinical Study Support Activity", "text");
				By byPatient_Safety_Attestation = cf.getSFElementXPath("Patient Safety Attestation", "text");

				By byCallClinicalStudy = cf.getSFElementXPath("Medical Discussions", "On-going Clinical Study", "Link");
				if (!cf.isElementVisible(byCallClinicalStudy, "On-going Clinical Study", 5))
				{
					byCallClinicalStudy = cf.getSFElementXPath("Medical Discussions", "Clinical Study", "Link");
				}
				By byCallClinicalStudySite = cf.getSFElementXPath("Medical Discussions", "Clinical Study Site", "Link");
				cf.verifyPageElement(byCallClinicalStudy, "Call - On-going Clinical Study", "Text", strStudy_Name, "Exact", true);
				cf.verifyPageElement(byCallClinicalStudySite, "Call - Clinical Study Site", "Text", strStudy_Site_Name, "Exact", false);

				cf.verifyPageElement(byDuration_Mins, "Call - Duration Mins", "Text", strDuration_Mins, "Exact", false);
				cf.verifyPageElement(byInteraction_Channel, "Call - Interaction Channel", "Text", strInteraction_Channel, "Exact", false);
				cf.verifyPageElement(byDiscussion_Intent, "Call - Discussion Intent", "Text", strDiscussion_Intent, "Exact", false);
				cf.verifyPageElement(byClinical_Study_Support_Activity, "Call - byClinical_Study_Support_Activity", "Text", strClinical_Study_Support_Activity, "Exact", false);
				cf.waitForSeconds(10);
				if (cf.verifyElementVisible(byPatient_Safety_Attestation, "Call - Patient_Safety_Attestation", 5))
				{
					cf.verifyPageElement(byPatient_Safety_Attestation, "Call - Patient_Safety_Attestation", "Text", strPatient_Safety_Attestation, "Exact", false);
				}
			}
			else
			{
				report.updateTestLog("Record a Call with Study", "Record a Call with Study Not Created.", Status.FAIL, "Full Screen", driver);
			} //verify Call Created
		} //Record a Call - Edit
	}//mslUserRecordACallWithStudy()

}//End of Class
