package businesscomponents;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.objGlobalAccountSearchPage;
import ObjectRepository.objHCMSearchAndAdd;
import ObjectRepository.objHeaderPage;
import ObjectRepository.objVeevaUSGAS;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Veeva_US_HCMSearchAndAdd extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	private Veeva_US_GlobalAccountSearch usgas=new Veeva_US_GlobalAccountSearch(scriptHelper);
	private GlobalAccountSearchPage rowgas=new GlobalAccountSearchPage(scriptHelper);
	
	public Veeva_US_HCMSearchAndAdd(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}


	/**
	 * This method is used to navigate to HCM Search and Add tab and verify if HCM Search page is displayed successfully
	 * @throws Exception
	 */
	public void navigateToHCMSearchAndAdd() throws Exception{
	cf.waitForSeconds(3);	
		cf.clickLink(objHeaderPage.plussymbol,"Plus Symbol");
	cf.waitForSeconds(3);
	//cf.clickLink(objHeaderPage.linkHCMSearchAndAdd, "HCM Search And Add");
	cf.clickLink(objHeaderPage.linkHCMSearchAndAdd1, "HCM Search");
		if(cf.isElementVisible(By.cssSelector("input[value*='HCM Search']"),"HCM Search And Add"))
			report.updateTestLog("Verification of HCM Search And Add", "HCM Search and Add page is loaded successfully", Status.PASS);
		else
			report.updateTestLog("Verification of HCM Search And Add", "HCM Search and Add page is loaded successfully", Status.PASS);
	}

	/**
	 * This method is used to click on HCA tab in HCM Search page
	 */
	public void clickHCATabInHCMSearch() throws Exception{
		cf.clickButton(By.cssSelector("[id*='HCA_cell'] [id*=tabHCA_lbl]"), "HCA tab");
	}

	/**
	 * This method is used to click on HCP tab in HCM Search page
	 */
	public void clickHCPTabInHCMSearch() throws Exception{
		cf.clickButton(By.cssSelector("[id*='HCP_cell'] [id*=tabHCP_lbl]"), "HCP Tab");
	}


	/**
	 * This method is used to enter empty values in HCM Search page to invoke GAS assuming that the service is down
	 * @throws Exception
	 */
	public void testGASWhenHCMDown() throws Exception{
		enterValuesInHCMSearch("HCP","","","","");	//This value must be retrieved from 
	}

	/**
	 * This method is used to enter values in HCM Search page based on entry provided in data table
	 * @throws Exception
	 */
	public void enterHCMSearchValues() throws Exception{
		String strExternalID=dataTable.getData("HCMSearchAndAdd", "ExternalID");
		String strFirstName=dataTable.getData("HCMSearchAndAdd", "FirstName");
		String strLastName=dataTable.getData("HCMSearchAndAdd", "LastName");
		String strState=dataTable.getData("HCMSearchAndAdd", "State");
		String strSearchType=dataTable.getData("HCMSearchAndAdd", "AccountType");
		enterValuesInHCMSearch(strSearchType,strExternalID, strFirstName, strLastName, strState);
	}

	/**
	 * This method is used to enter values in HCM search based on parameters passed
	 * @param strExternalID
	 * @param strFirstName
	 * @param strLastName
	 * @param strState
	 * @throws Exception
	 */
	public void enterValuesInHCMSearch(String strSearchType,String strExternalID,String strFirstName,String strLastName,String strState) throws Exception{
		cf.waitForSeconds(3);
		String strExternalIDPath="",strFirstNamePath="",strLastNamePath="",strStatePath="";
		String strTablePath="";
		if(strSearchType.trim().equalsIgnoreCase("HCP"))
		{
			strTablePath=objHCMSearchAndAdd.strHCPTable;
			strFirstNamePath=strTablePath+objHCMSearchAndAdd.xpathFirstName;
			strLastNamePath=strTablePath+objHCMSearchAndAdd.xpathLastName;
			cf.setData(By.xpath(strFirstNamePath), "First Name", strFirstName);
			cf.setData(By.xpath(strLastNamePath), "Last Name", strLastName);
		}
		else{
			strTablePath=objHCMSearchAndAdd.strHCATable;
		}

		strExternalIDPath=strTablePath+objHCMSearchAndAdd.xpathExternalID;
		strStatePath=strTablePath+objHCMSearchAndAdd.strState;

		cf.setData(By.xpath(strExternalIDPath), "External ID", strExternalID);

		if(strState.trim()=="")
			cf.selectARandomOptionFromComboBox(By.xpath(strStatePath), "State");
		else
			cf.selectData(By.xpath(strStatePath), "State", strState);
		cf.clickButton(By.cssSelector("input[value*='HCM Search']"), "HCM Search");
		waitForSearchResultsSpinner();
	}

	/**
	 * This method is used to verify headers in HCM Search results table
	 */
	public void verifyHCMSearchResultsHeader() throws Exception{
		if(cf.isElementVisible(By.cssSelector("table.ResultTable tr>th"), "Results Table)")){
			List<WebElement> resultTableHeaderNames=driver.findElements(By.cssSelector("table.ResultTable tr>th"));
			ArrayList<String> columnNames=new ArrayList<String>();
			columnNames.add("AZ ID");
			columnNames.add("First Name");
			columnNames.add("Last Name");
			columnNames.add("Phone#");
			columnNames.add("Status");
			columnNames.add("Cust Type");
			columnNames.add("Cust Sub Type");
			columnNames.add("ASOC");
			columnNames.add("In Veeva?");
			columnNames.add("# of Calls");
			columnNames.add("Address");
			int nameCol=-1;
			String missingColumnNames="";
			for(String strColumnName : columnNames){
				for (int idx=0; idx < resultTableHeaderNames.size(); idx++){
					WebElement th = resultTableHeaderNames.get(idx);
					if (th.getText().trim().contains(strColumnName)){
						nameCol = idx;
						break;
					}
				}
				if(nameCol!=-1)
					report.updateTestLog("Verification of Column Names in GAS", "Column name: '" + strColumnName +"' is displayed in : "+ nameCol +" column", Status.DONE);
				else if(nameCol==-1)
					missingColumnNames=strColumnName + "," +missingColumnNames;
			}
			if(!missingColumnNames.trim().equalsIgnoreCase(""))
				report.updateTestLog("Verification of HCM Header", missingColumnNames +" columns are missing in result table", Status.FAIL);
		}
		else{
			if(cf.isElementVisible(objVeevaUSGAS.objHCMServiceDownMessage, "HCM Search service down")){
				usgas.navigateToUSGASinHCMSearchServiceDown();
			}
		}
	}
	/**
	 * This method is used to add an account to current user's territory
	 * @throws Exception
	 */
	public void addAccountToUserTerritory() throws Exception{

		if(cf.isElementVisible(By.cssSelector("table.ResultTable"),"HCM Search Results")){
			List<WebElement> chkBoxList=driver.findElements(By.cssSelector("table.ResultTable input.inpChkBox"));
			for(int i=0;i<chkBoxList.size();i++){
				cf.clickButton(By.cssSelector("table.ResultTable input.inpChkBox:nth-of-type("+(i+1)+")"), "Check box");
				cf.clickSFButton("Add to Territory","button");
				//waitForSearchResultsSpinner();
				cf.waitForSeconds(3);
				WebElement errorMessage=driver.findElement(By.cssSelector("td.messageCell>div"));
				if(errorMessage.getText().trim().contains("The customer is already in your territory. No changes made.")){
					report.updateTestLog("Verify Message Details", "'The customer is already in your territory. No changes made.' is displayed as expected", Status.PASS);
					String strGASHistoryAccount="";
					String strFirstName="";
					String strLastName="";
					String strOrgName="";
					List<WebElement> resultTableData=driver.findElements(By.cssSelector("table.ResultTable tr>td>span"));
					List<WebElement> resultTableHeaderNames=driver.findElements(By.cssSelector("table.ResultTable tr>th"));
					boolean blnFirstName=false,blnLastName=false;//blnOrgName=false;
					for (int idx=0; idx < resultTableHeaderNames.size(); idx++){
						WebElement th = resultTableHeaderNames.get(idx);
						if (th.getText().trim().contains("First Name")){
							strFirstName=resultTableData.get(idx-2).getText();	//TD span tag is not available for check box and additional column corresponding to header

							report.updateTestLog("Verify First Name in Result", "First name is recorded in search results as : '" +strFirstName + "'", Status.DONE);
							blnFirstName=true;
							if(blnLastName)	break;
						}
						if(th.getText().trim().contains("Last Name")){
							strLastName=resultTableData.get(idx-2).getText();
							report.updateTestLog("Verify Last Name in Result", "Last name is displayed in search results as : '" +strLastName +"'", Status.DONE);
							blnLastName=true;
							if(blnFirstName)	break;
						}
						if(th.getText().trim().contains("Org Name")){
							strOrgName=resultTableData.get(idx-2).getText();
							report.updateTestLog("Verify Org Name in Result", "Org name is displayed in search results as : '" +strOrgName +"'", Status.DONE);
							break;
						}

					}
					if(!strOrgName.trim().equalsIgnoreCase(""))
						strGASHistoryAccount=strOrgName;
					else
						strGASHistoryAccount=strFirstName +" " + strLastName;
					if(!strGASHistoryAccount.trim().equals(""))
						dataTable.putData("GlobalAccountSearch", "GASHistoryAccount",strGASHistoryAccount);
				}
				else if(i==chkBoxList.size()-1)
					report.updateTestLog("Verify Message Details", "'The customer is already in your territory. No changes made.' is not displayed", Status.FAIL);
			}
		}
		else if (cf.isElementVisible(objGlobalAccountSearchPage.tblSearchResults, "GAS Search Results")){
			usgas.verifyGASResultsHeader();
			rowgas.addAndVerifyAlreadyTerritoryAdded();
		}
	}

	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToUSGASinHCMSearchServiceDown() throws Exception{
		if(cf.isElementVisible(objVeevaUSGAS.objHCMServiceDownMessage, "HCM Search service down")){
			report.updateTestLog("Verify able to HCM Search Results", "HCM Search And Add service is down", Status.SCREENSHOT);
			cf.clickSFButton("Global Account Search", "submit");
			if(cf.isElementVisible(objVeevaUSGAS.objUSGasHeader, "Global Account Search Header"))
				report.updateTestLog("Verify GAS page navigation", "Navigation to Global account search page is successful",Status.PASS);
			else
				report.updateTestLog("Verify able to navigate to Search for Account page", "Unable to navigate to Surveys Home page", Status.FAIL);
		}
		else
			report.updateTestLog("Verify HCM Search Results navigation", "Unexpected search results page is displayed", Status.FAIL);
	}

	/**
	 * This method will wait for spinner to disappear after HCM Search is clicked
	 * @throws Exception
	 */
	public void waitForSearchResultsSpinner() throws Exception{
		WebDriverWait waitSpinner=new WebDriverWait(driver.getWebDriver(), 5);
		waitSpinner.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img[src*=loading]")));
		for(int i=0;i<5;i++){
			try{
				waitSpinner=new WebDriverWait(driver.getWebDriver(), 30);
				waitSpinner.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img[src*=loading]")));
				break;
			}catch(Exception e){
				System.out.println("spinner wait continues");
			}
		}
	}

	
}
