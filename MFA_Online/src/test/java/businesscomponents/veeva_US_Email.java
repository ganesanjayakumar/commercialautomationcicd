package businesscomponents;

import java.util.List;
import supportlibraries.*;

import com.mfa.framework.Status;

//import com.itextpdf.text.log.SysoCounter;

//import org.openqa.jetty.html.Select;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

public class veeva_US_Email  extends ReusableLibrary{

	//private ConvenienceFunctions cf = new ConvenienceFunctions(driver, report, dataTable);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	public veeva_US_Email(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	public void chooseMoreActionsOpt_US() throws Exception{

		Boolean option=true;
		String strDataSheet = "ApprovedEmail";
		String moreActionsOpt=dataTable.getData(strDataSheet, "MoreActonsOption");
		if(cf.isElementVisible(By.name("MoreActions"),"More options Button")){
			cf.clickButton(By.name("MoreActions"), "More Actions");
		}
		else{
			report.updateTestLog("Veriy able to see More Actions Dropdown", "Unable to see More Actions Dropdown", Status.FAIL);
		}
		cf.waitForSeconds(5);
		List<WebElement> options=driver.findElements(By.xpath("//div[@class='popover bottom display_block']/div/div/a"));
		for(int i=1;i<=options.size();i++){
			String optionname=driver.findElement(By.xpath("//div[@class='popover bottom display_block']/div/div["+(i+1)+"]/a")).getText();
			if(moreActionsOpt.equals(optionname)){
				option=true;
				driver.findElement(By.xpath("//div[@class='popover bottom display_block']/div/div["+(i+1)+"]/a")).click();
				cf.waitForSeconds(10);
				break;
			}
			else
				option=false;
		}
		if(option==true)
			report.updateTestLog("Able to click on SendEmail Option", "Clicking SendEmail option is successful", Status.PASS);
		else{
			report.updateTestLog("Able to click on SendEmail Option", "Clicking SendEmail option is unsuccessful", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}	

   public void unselectallproducts() throws Exception{
	   int r;
	   if(cf.isElementVisible(By.id("vod_iframe"), "vod_iframe")){
		   cf.waitForSeconds(5);
			cf.switchToFrame(By.id("vod_iframe"));
			 cf.waitForSeconds(5);
		}
	  // List productass=driver.findElements(By.xpath("//tbody[@id='productSelection']//td/input"));
	   List<WebElement> products=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr"));
	   System.out.println(products);
	   for(r=1;r<=products.size();r++){
		   int columnproductssize=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr["+r+"]/td")).size();
			for(int s=1;s<=columnproductssize;s++){
				String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
			//String productoption=driver.findElement(By.xpath("//tbody[@id='productSelection']//td["+r+"]/input")).getText();
			System.out.println(productoption);
			cf.waitForSeconds(2);
			if(driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).isSelected())
				driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).click();	
			}
	   }
	   
   }
}


