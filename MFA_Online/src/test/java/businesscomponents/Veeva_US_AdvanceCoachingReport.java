package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_US_AdvanceCoachingReport extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	public Veeva_US_AdvanceCoachingReport(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	/**
	 * Navigate to Coaching Tab.
	 * @throws Exception
	 */
	public void coachingTabUS() throws Exception{
		vf.clickVeevaTab("Coaching");
		if(cf.isElementVisible(objVeevaAdvanceCoachingReport.manNewcoachingReport, "New Coaching Report"))
			report.updateTestLog("Verify able to navigate to Coaching Tab", "Able to navigate to Coaching Tab.", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to Coaching tab", "Unable to navigate to Coaching tab.", Status.FAIL);
	}

	/**
	 * Creating new Coaching Report by logging with Manager.
	 * @throws Exception
	 */
	public void advancenewcoachingManagerUS() throws Exception{
		WebDriverWait wait =new WebDriverWait(driver.getWebDriver(),30);
		cf.clickLink(objVeevaAdvanceCoachingReport.manNewcoachingReport, "New Coaching Report");
		cf.waitForSeconds(20);
		int rows=driver.findElements(By.xpath("//table[@class='ttable']//tbody//tr")).size();
		if(rows>0){
			Date exte =new Date();
			String ex=new SimpleDateFormat("ddMMhhmmss").format(exte).toString();
			cf.clickLink(By.cssSelector("table.ttable tbody tr:nth-child(1) td input"), "Selecting Coaching Report");
			cf.clickLink(objVeevaAdvanceCoachingReport.mannewcoachingReportButtoninNewcoachingRepPage, "New Coaching Report Button in New Coaching Report Page");
			cf.waitForSeconds(20);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[@class='pageType ng-binding' and text()='Survey Target']")));
			if(cf.isElementVisible(By.xpath("//h1[@class='pageType ng-binding' and text()='Survey Target']"), "Survey Target Detail Page")) {//or //h1[contains(text(),'Survey Target')]
				report.updateTestLog("Verify able to navigate to Coaching Report Page", "Able to open as New Coaching Report", Status.PASS);
				String CoachingReportName="AdvanceCoachingReport"+ex;
				dataTable.putData("AdvanceCoachingReport", "CoachingReportName", CoachingReportName);
				cf.setData(By.name("Name"), "CoachingReportName",CoachingReportName);
			}
			else{
				report.updateTestLog("Verify able to navigate to Coaching Report Page", "Unable to open as New Coaching Report", Status.FAIL);
			}

		}
		else{
			report.updateTestLog("Verify able to see Coaching Reports.", "Unable to see Coaching to Select.", Status.FAIL);
			throw new Exception("No Coaching Reports to select");
		}
	}

	/**
	 * Validating Survey Name and Manager Fields are Auto Populated 
	 * and select Start date and End Date
	 * @throws Exception
	 */
	public void surveyTargetdetailautopopulatedUS() throws Exception{
		Boolean details=true;
		cf.waitForSeconds(1);
		if(driver.findElement(objVeevaAdvanceCoachingReport.surveydetailsurveyName).getAttribute("value") != null){				
			if(driver.findElement(objVeevaAdvanceCoachingReport.surveydetailmanager).getAttribute("value") != null){
				//	if(driver.findElement(By.name("Report_Status_vod__c")).getAttribute("value") != null){
				String startdate=cf.getData(By.cssSelector("span#Start_Date_vod__c span a"), "Start Date", "text");
				dataTable.putData("AdvanceCoachingReport", "Start Date", startdate);
				cf.clickLink(By.cssSelector("span#Start_Date_vod__c span a"), "Start Date");
				String enddate=cf.getData(By.cssSelector("span#End_Date_vod__c span a"), "Start Date", "text");
				dataTable.putData("AdvanceCoachingReport", "End Date", enddate);
				cf.clickLink(By.cssSelector("span#End_Date_vod__c span a"), "EndDate");
				cf.selectData(By.id("Follows_AstraZeneca_Policies_AZ_US__c"), "Astrazeneca Policies", "Yes");
				cf.selectData(By.id("Adhering_to_Driving_Policy_AZ_US__c"), "Astrazeneca Policies", "Yes");
				details=true;
				//}
			}
		}
		if(details == true)
			report.updateTestLog("Verify survey name and manager fields are autopopulated","survey name and manager fields are autopopulated" , Status.PASS);
		else{
			report.updateTestLog("Verify survey name and manager fields are autopopulated","survey name and manager fields are not autopopulated" , Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void fieldlevel() throws Exception{
		
		cf.noneditable("Survey Target Detail", "Survey Target", "non-editable", "ng-if");
		cf.noneditable("Survey Target Detail", "Start Date", "non-editable", "ng-if");
		cf.noneditable("Survey Target Detail", "End Date", "non-editable", "ng-if");
		cf.noneditable("Survey Target Detail", "Owner", "non-editable", "ng-if");
		cf.noneditable("Survey Target Detail", "Duration of Field Visit", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Manager", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Employee", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Review Date", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Employee PRID", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Report Status", "non-editable", "ng-if");
		cf.noneditable("Coaching Details", "Report ID", "non-editable", "ng-if");
		cf.noneditable("Compliance", "Follows AstraZeneca Policies", "non-editable", "ng-if");
		cf.noneditable("Compliance", "Adheres to Driving Policy", "non-editable", "ng-if");
	}
}
