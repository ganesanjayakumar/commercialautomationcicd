package businesscomponents;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objCallsForMedical;
import ObjectRepository.objHeaderPage;
import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class VeevaAccountPlansOneKAM extends ReusableLibrary
{

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public VeevaAccountPlansOneKAM(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToAccountPlans() throws Exception
	{
		vf.clickVeevaTab("Account Plans");
	}

	public void clickNewAccountPlan() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		if (cf.isElementVisible(objVeevaOneKAM.accountPlanPageTitle, "Account Plan Page"))
			cf.clickSFButton("New");
		if (strLocale.trim().equalsIgnoreCase("BR") || strLocale.trim().equalsIgnoreCase("RU") || strLocale.trim().equalsIgnoreCase("CA") || strLocale.trim().equalsIgnoreCase("EU2") || strLocale.equals("EU1") && strProfile.equals("DxL")) // Fix: Kusuma 07/05/2019
			selectAccountPlanRecordType();
	}

	public void searchAccountPlan() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String accounttype = dataTable.getData("OneKAMAccountPlans", "AccountType");
		String accountPlan = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		searchAccountPlan(strLocale, accounttype, accountPlan);
	}

	private void searchAccountPlan(String locale, String accounttype, String accountPlan) throws Exception
	{

		for (int count = 1; count <= 10; count++)
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 60);
			if (cf.isElementPresent(objHeaderPage.selectSearch))
			{
				cf.selectData(objHeaderPage.selectSearch, "Search Type", "Account Plans");
				cf.setData(objHeaderPage.textSearch, "Search Box", accountPlan);
				cf.clickButton(objHeaderPage.btnGo, "Search GO");
			}
			else
			{
				cf.clickButton(By.xpath("//a[.='Advanced Search...']"), "Advanced Search");
				wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".searchBoxInput>input[value='Search']")));
				cf.setData(By.cssSelector(".searchBoxInput>input"), "Search in Advanced Search", accountPlan);
				cf.clickButton(By.xpath("//label[.='Account Plans']/preceding-sibling::input[@type='checkbox']"), "Choose Account");
				cf.clickButton(By.cssSelector(".searchBoxInput>input[value='Search']"), "Search Button");
			}
			int iAccountTypeCol = 0;
			if (locale.equals("BR"))
				iAccountTypeCol = 2;
			else if (locale.equals("CN"))
				iAccountTypeCol = 4;

			if (cf.isElementPresent(By.cssSelector(".oRight > div > strong")))
			{
				if (driver.findElement(By.cssSelector(".oRight > div > strong")).getText().trim().equalsIgnoreCase("There are no matching: Account Plans"))
					cf.pageRefresh();
			}
			else if (driver.findElements(By.cssSelector(".bPageTitle h1")).size() > 0)
			{
				if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("search results"))
				{
					int rows = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
					for (int i = 2; i <= rows; i++)
					{
						String appaccounttype = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]/td[" + iAccountTypeCol + "]")).getText().trim();
						if (appaccounttype.equals(accounttype))
						{
							cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//th/a"), "Account type");
							break;
						}
					}
				}
				else if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("account plan"))
				{
					break;
				}
			}
		}
		if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("account plan"))
		{
			report.updateTestLog("Verify Account Plan page", "Account plan page displayed", Status.DONE);
		}
		else
			throw new FrameworkException("Account plan not displayed");
	}

	public void selectAccountPlanRecordType() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		if (!strLocale.equals("US"))
		{
			cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"), "Record Type", "Account Plan AZ Core 01");
		}
		else
		{
			cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"), "Record Type", "Account Plan AZ US Sales");

		}
		cf.clickSFButton("Continue");
	}

	public synchronized void enterAccountPlanInformation() throws Exception
	{
		System.out.println("Inside enterAcctPlan Informatin");
		String strLocale = dataTable.getData("Login", "Locale");
		String strDetailTopic = "";
		Select accounttype;
//		if (cf.isElementVisible(By.xpath("//h2[contains(text(),'Select Account Plan Record Type')]"), "Select Account Plan Record Type"))
//		{
//			
//			
//			cf.waitForSeconds(1);
//			cf.clickElement(By.xpath("//input[@title='Continue']"), "Continue");
		if (cf.isElementVisible(By.xpath("//h2[contains(text(),'Account Plan Edit')]"), "Select Account Plan Record Type")){
			
			String accountName = dataTable.getData("OneKAMAccountPlans", "Account");
			String accountPlanName = "AccountPlan" + new SimpleDateFormat("ddmmss").format(new Date()).toString();
			cf.waitForSeconds(1);
			dataTable.putData("OneKAMAccountPlans", "AccountPlan", accountPlanName);

			if (cf.isElementVisible(By.xpath("//h2[contains(text(),'New Account Plan')]"), "New Account Plan"))
			{
				cf.setSFData("Information", "Account Plan Name", "input", accountPlanName);
				vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Account", "lookupimage"), "Account", accountName);
				vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Detail Product", "lookupimage"), "Detail Product", dataTable.getData("OneKAMAccountPlans", "Product"));
				vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Therapeutic Area", "lookupimage"), "Therapeutic Area", dataTable.getData("OneKAMAccountPlans", "AccountTherapeuticArea"));
				//Below script modified for US region - Srinidhi 29/04/2019
				if (!strLocale.trim().equalsIgnoreCase("US"))
				{
					strDetailTopic = vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Detail Topic", "lookupimage"), "Detail Topic", dataTable.getData("OneKAMAccountPlans", "DetailTopic"));
					// Fix By Sindiya - 25/02/2019

					accounttype = new Select(driver.findElement(By.xpath("//label[text()='Account Type']/parent::td//following-sibling::td[contains(@class,'dataCol last col02')]//select")));
					accounttype.selectByIndex(2);
					Select accountPriority = new Select(driver.findElement(By.xpath("//label[text()='Account Priority']/parent::td//following-sibling::td[contains(@class,'last dataCol')]//select")));
					accountPriority.selectByIndex(2);
				}
			}

			cf.clickSFButton("Information", "Start Date", "dateformat");
			String strAccountPlanStartDate = cf.getSFData("Start Date", "textbox", "Text");
			if (strLocale.trim().equalsIgnoreCase("CN"))
			{
				cf.clickSFButton("Information", "End Date", "dateformat");
				String strAccountPlanEndDate = cf.getSFData("Start Date", "textbox", "Text");
				dataTable.putData("OneKAMAccountPlans", "AccountPlanEndDate", strAccountPlanEndDate);
			}
			cf.waitForSeconds(1);
			//dataTable.putData("OneKAMAccountPlans", "AccountTherapeuticArea", strTherapeuticArea);
			cf.waitForSeconds(1);
			dataTable.putData("OneKAMAccountPlans", "AccountPlanStartDate", strAccountPlanStartDate);
			cf.waitForSeconds(1);
		}

		else
			report.updateTestLog("Account plan edit page", "Account plan edit page is not displayed", Status.FAIL);
	}

	public synchronized void enterBackgroundInformation() throws Exception
	{
		String strBackground = "Background information set at " + (new Date()).toString();
		cf.setSFData("Background", "Background", "textarea", strBackground);
		dataTable.putData("OneKAMAccountPlans", "Background", strBackground);
	}

	public void enterSWOTAnalysis() throws Exception
	{

		String strStrengths = "Strengths information set at " + (new Date()).toString();//cf.getSFData("Strengths", "textarea", "Text");
		String strWeaknesses = "Weaknesses information set at " + (new Date()).toString();//cf.getSFData("Weaknesses", "textarea", "Text");
		String strOpportunities = "Opportunities information set at " + (new Date()).toString();//cf.getSFData("Opportunities", "textarea", "Text");
		String strThreats = "Threats information set at " + (new Date()).toString();//cf.getSFData("Threats", "textarea", "Text");

		cf.setSFData("SWOT Analysis", "Strengths", "textarea", strStrengths);
		cf.setSFData("SWOT Analysis", "Weaknesses", "textarea", strWeaknesses);
		cf.setSFData("SWOT Analysis", "Opportunities", "textarea", strOpportunities);
		cf.setSFData("SWOT Analysis", "Threats", "textarea", strThreats);

		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "Strengths", strStrengths);
		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "Weaknesses", strWeaknesses);
		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "Opportunities", strOpportunities);
		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "Threats", strThreats);
		cf.waitForSeconds(1);
	}

	public void enterGoalsInformation() throws Exception
	{

		String strGoalsCSF = "Critical Success Factors information set at " + (new Date()).toString();
		cf.waitForSeconds(1);
		String strGoals = "Goals information set at " + (new Date()).toString();
		cf.waitForSeconds(1);
		String strLocale = dataTable.getData("Login", "Locale");

		if (strLocale.trim().equalsIgnoreCase("CN"))
		{

			cf.setSFData("SWOT Analysis", "Critical Success Factors", "textarea", strGoalsCSF);
		}
		else
		{
			cf.setSFData("Goals", "Critical Success Factors", "textarea", strGoalsCSF);
			cf.setSFData("Goals", "Goal", "textarea", strGoals);
		}
		dataTable.putData("OneKAMAccountPlans", "CriticalSuccessFactors", strGoalsCSF);
		dataTable.putData("OneKAMAccountPlans", "Goals", strGoals);
	}

	public void enterConfidenceStatus() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");

		cf.scrollToBottom();
		String strConfidenceReason = "Confidence Reason information set at " + (new Date()).toString();
		String strConfidenceStatus = "";
		String strConfidenceLevel = "";
		if (strLocale.trim().equalsIgnoreCase("CN"))
		{
			strConfidenceStatus = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Status", "Status", "select"), "Confidence Status");
			strConfidenceLevel = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Status", "Confidence Level", "select"), "Confidence Level");
		}
		else
		{
			strConfidenceStatus = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Confidence Status", "Status", "select"), "Confidence Status");
			strConfidenceLevel = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Confidence Status", "Confidence Level", "select"), "Confidence Level");
			cf.setSFData("Confidence Status", "Confidence Reason", "textarea", strConfidenceReason);

		}

		dataTable.putData("OneKAMAccountPlans", "ConfidenceStatus", strConfidenceStatus);
		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "ConfidenceLevel", strConfidenceLevel);
		cf.waitForSeconds(1);
		dataTable.putData("OneKAMAccountPlans", "ConfidenceReason", strConfidenceReason);
	}

	public void verifyAccountPlanStatuses() throws Exception
	{
		cf.verifyOptionsPresentinSelectBox(cf.getSFElementXPath("Information", "Status", "select"), "Status", "Active;Inactive");
	}

	public void saveAccountPlanAndValidateKAMPage() throws Exception
	{
		report.updateTestLog("Verify Account Plan Creation", "Account plan details are entered", Status.SCREENSHOT);
		cf.clickSFButton("Save");
		cf.waitForSeconds(5);
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".bPageTitle h1")));

			if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("account plan"))
			{

				report.updateTestLog("Verify Account Plan page", "Account plan page displayed as expected", Status.PASS);

				String strAccountPlanName = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
				if (cf.getSFData("Account Plan Name", "div", "Text").toLowerCase().trim().equalsIgnoreCase(strAccountPlanName))
					report.updateTestLog("Verify Account Plan Name", "Account plan name : ' " + strAccountPlanName + "' displayed as expected", Status.PASS);
				else
					report.updateTestLog("Verify Account Plan Name", strAccountPlanName + " not displayed", Status.FAIL);
			}
			else
				throw new FrameworkException("Account plan page not displayed");
		}
		catch (Exception e)
		{
			cf.pageRefresh();
			cf.waitForSeconds(3);
			if (driver.findElement(By.cssSelector(".bPageTitle h1")).getText().trim().toLowerCase().contains("account plan"))
			{

				report.updateTestLog("Verify Account Plan page", "Account plan page displayed as expected", Status.PASS);

				String strAccountPlanName = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
				if (cf.getSFData("Account Plan Name", "div", "Text").toLowerCase().trim().equalsIgnoreCase(strAccountPlanName))
					report.updateTestLog("Verify Account Plan Name", "Account plan name : ' " + strAccountPlanName + "' displayed as expected", Status.PASS);
				else
					report.updateTestLog("Verify Account Plan Name", strAccountPlanName + " not displayed", Status.FAIL);
			}
			else
			{

				throw new FrameworkException("Account plan page not displayed");
			}
		}

	}

	public void verifySavedAccountPlanInformation() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strBackground = "";
		String strCSF = "";
		String strGoal = "";
		String strConfidenceStatus = "";
		String strConfidenceLevel = "";
		String strConfidenceReason = "";

		String strStrength = dataTable.getData("OneKAMAccountPlans", "Strengths");
		cf.waitForSeconds(1);
		String strWeakness = dataTable.getData("OneKAMAccountPlans", "Weaknesses");
		cf.waitForSeconds(1);
		String strOpportunity = dataTable.getData("OneKAMAccountPlans", "Opportunities");
		cf.waitForSeconds(1);
		String strThreat = dataTable.getData("OneKAMAccountPlans", "Threats");

		cf.scrollToBottom();
		cf.waitForSeconds(10);
		cf.verifySFElementData("SWOT Analysis", "Strengths", "div", "Text", strStrength);
		cf.verifySFElementData("SWOT Analysis", "Weaknesses", "div", "Text", strWeakness);
		cf.verifySFElementData("SWOT Analysis", "Opportunities", "div", "Text", strOpportunity);
		cf.verifySFElementData("SWOT Analysis", "Threats", "div", "Text", strThreat);

		strBackground = dataTable.getData("OneKAMAccountPlans", "Background");
		cf.verifySFElementData("Background", "Background", "div", "Text", strBackground);

		strCSF = dataTable.getData("OneKAMAccountPlans", "CriticalSuccessFactors");
		strGoal = dataTable.getData("OneKAMAccountPlans", "Goals");
		strConfidenceStatus = dataTable.getData("OneKAMAccountPlans", "ConfidenceStatus");
		strConfidenceLevel = dataTable.getData("OneKAMAccountPlans", "ConfidenceLevel");
		strConfidenceReason = dataTable.getData("OneKAMAccountPlans", "ConfidenceReason");

		if (strLocale.trim().equalsIgnoreCase("CN"))
		{
			cf.verifySFElementData("SWOT Analysis", "Critical Success Factors", "div", "Text", strCSF);
			cf.verifySFElementData("Status", "Status", "div", "Text", strConfidenceStatus);
			cf.verifySFElementData("Status", "Confidence Level", "div", "Text", strConfidenceLevel);
		}
		else
		{

			cf.verifySFElementData("Goals", "Critical Success Factors", "div", "Text", strCSF);
			cf.verifySFElementData("Goals", "Goal", "div", "Text", strGoal);
			cf.verifySFElementData("Confidence Status", "Status", "div", "Text", strConfidenceStatus);
			cf.verifySFElementData("Confidence Status", "Confidence Level", "div", "Text", strConfidenceLevel);
			cf.verifySFElementData("Confidence Status", "Confidence Reason", "div", "Text", strConfidenceReason);
		}
	}

	/**
	 * This method will create decision making unit from Account Plans page and validate the related list on successful creation
	 * @throws Exception
	 */
	public void createAndValidateDMU() throws Exception
	{
		cf.clickSFLinkLet("Decision Making Units");
		cf.clickSFButton("New Decision Making Unit", "pbButton");
		String strLocale = dataTable.getData("Login", "Locale");
		String strDecisionMakingUnit = "";
		strDecisionMakingUnit = dataTable.getData("OneKAMAccountPlans", "DecisionMakingUnit");
		if (strDecisionMakingUnit.contains(","))
			strDecisionMakingUnit = strDecisionMakingUnit.substring(strDecisionMakingUnit.indexOf(",") + 1).trim() + " " + strDecisionMakingUnit.substring(0, strDecisionMakingUnit.indexOf(",")).trim();
		if (strLocale.trim().equalsIgnoreCase("EU1") || strLocale.trim().equalsIgnoreCase("EU2") || strLocale.trim().equalsIgnoreCase("BR") || strLocale.trim().equalsIgnoreCase("RU"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Understanding Stakeholders", "Key Stakeholder", "lookupimage"), "Decision Making Unit", strDecisionMakingUnit);
		}
		else
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Understanding Stakeholders", "Decision Making Unit", "lookupimage"), "Decision Making Unit", strDecisionMakingUnit);
		}
		String strTherapeutic = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Specialty / Therapeutic area", "select"), "Therapeutic Area");
		String strRole = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Role", "select"), "Role");
		String strBelief = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Belief", "select"), "Belief");
		String strSignificance = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Significance", "select"), "Significance");
		String strTrust = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Trust and Confidence", "select"), "TrustAndConfidence");
		String strAttitudetoTesting = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Attitude to Testing", "select"), "AttitudeTesting");
		cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Advocacy", "select"), "Advocacy");
		String strAttitudeToProduct = cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Understanding Stakeholders", "Attitude to the Product", "select"), "Attitude to the Product");
		cf.setSFData("Stakeholder Funding", "Stakeholder Needs", "textarea", "sample Stakeholder Needs at " + (new Date()).toString());
		cf.setSFData("Stakeholder Funding", "Stakeholder Pain Points", "textarea", "sample Stakeholder pain points at " + (new Date()).toString());
		cf.setSFData("Stakeholder Funding", "Stakeholder Concerns", "textarea", "sample Stakeholder Concerns " + (new Date()).toString());
		report.updateTestLog("Decision Making Unit Details", "Details are entered", Status.SCREENSHOT);
		cf.clickSFButton("Save");
		String strDecisionMakingUnitID = driver.findElement(By.cssSelector(".bPageTitle h2")).getText().trim();
		dataTable.putData("OneKAMAccountPlans", "DecisionMakingUnitID", strDecisionMakingUnitID);
		cf.clickSFButton("Understanding Stakeholders", "Account Plan", "link");
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "ID", strDecisionMakingUnitID);
		if (strLocale.trim().equalsIgnoreCase("EU1") || strLocale.trim().equalsIgnoreCase("EU2") || strLocale.trim().equalsIgnoreCase("BR") || strLocale.trim().equalsIgnoreCase("RU"))
			cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Key Stakeholder", strDecisionMakingUnit);
		else
			cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Decision Making Unit", strDecisionMakingUnit);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Role", strRole);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Trust and Confidence", strTrust);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Belief", strBelief);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Significance", strSignificance);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Specialty / Therapeutic area", strTherapeutic);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Attitude to Testing", strAttitudetoTesting);
		cf.verifyDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnitID, "Attitude to the Product", strAttitudeToProduct);
	}

	/**
	 * To verify if account plan is listed in DMU Hierarchy
	 * @throws Exception
	 */
	public void viewHierarchyInDMU() throws Exception
	{
		cf.clickSFButton("View Hierarchy", "pbButtonb");
		String strAccountPlan = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		cf.switchToFrame("vod_iframe");
		String strSelectedAccPlan = cf.getData(By.id("accountPlanSelect"), "Account Plan", "select");
		cf.verifyActualExpectedData("Account Plan", strSelectedAccPlan, strAccountPlan);
		cf.clickButton(By.cssSelector("#ptBreadcrumb a"), "Back to Account Plans");
		cf.switchToParentFrame();

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void addFromRelationshipMapInDMU() throws Exception
	{
		int iBeforeRows = cf.getRowCountFromLinkLetTable("Decision Making Units");
		cf.clickSFButton("Add from Relationship Map", "pbButton");
		cf.switchToFrame("vod_iframe");
		String strDeleteLinkPath = "tr.dataRow>td>a[href*='delete']";
		cf.clickButton(By.cssSelector(strDeleteLinkPath), "Delete DMU");
		cf.handleAlert();
		cf.waitForSeconds(3);
		cf.switchToParentFrame();
		cf.switchToFrame("vod_iframe");
		cf.clickButton(By.cssSelector(".ptBreadcrumb>a"), "Back to Account Plans");
		cf.switchToParentFrame();
		int iAfterRows = cf.getRowCountFromLinkLetTable("Decision Making Units");
		if ((--iBeforeRows) == iAfterRows)
			report.updateTestLog("Verify DMU in Related List", "Decision Making Unit is successfully deleted", Status.PASS);
		else
			report.updateTestLog("Verify DMU in Related List", "Decision Making Unit is not deleted", Status.FAIL);
	}

	/**
	 * This method will add a team member and validate in related list
	 * @throws Exception Fix By Sindiya : 27/02/2019
	 */
	public void addTeamMemberAndValidate() throws Exception
	{
		String strAccountTeamMember = dataTable.getData("OneKAMAccountPlans", "TeamMember");
		String planName = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		String strLinkletName = "";
		if (driver.getCurrentUrl().toLowerCase().contains("usfull"))
			strLinkletName = "Account Team Members";
		else
			strLinkletName = "Account Plan Members";
		cf.clickSFLinkLet(strLinkletName);
		cf.clickSFButton("New Account Plan Member", "pbButton");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Account Plan Member Edit']")));
		//Add a team member with edit access
		String accPlan = driver.findElement(By.xpath("//span[@class='lookupInput']/input")).getAttribute("value");
		if (accPlan.equalsIgnoreCase(planName))
			report.updateTestLog("Account Plan Member Edit", "Account plan name is auto populated", Status.DONE);
		else
			report.updateTestLog("Account Plan Member Edit", "Account plan name is not auto populated", Status.FAIL);

		vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Team Member", "lookupimage"), "Team Member", strAccountTeamMember);
		Select accessLevel = new Select(driver.findElement(By.xpath("//label[text()='Access Level']/parent::td/following-sibling::td//select")));
		accessLevel.selectByVisibleText("Edit");

		if (driver.getCurrentUrl().toLowerCase().contains("usfull"))
		{
			cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "RACI", "select"), "RACI");
		}
		cf.clickSFButton("Save", "pbButtonb");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Account Plan Member Detail']")));
		String id = driver.findElement(By.className("pageDescription")).getText().trim();
		System.out.println(id);
		driver.findElement(By.xpath("//td[text()='Account Plan']/following-sibling::td//a")).click();
		cf.waitForSeconds(3);
		//verify the related list

		cf.clickSFLinkLet(strLinkletName);
		//	cf.clickSFLinkLet("Account Plan Members");
		if (driver.getCurrentUrl().toLowerCase().contains("usfull"))
		{
			cf.verifyDataFromLinkLetTable(strLinkletName, "ID", id, "Team Member", strAccountTeamMember);
		}
		else
			cf.verifyDataFromLinkLetTable(strLinkletName, "ID", id, "Team Member Name", strAccountTeamMember);
		cf.verifyDataFromLinkLetTable(strLinkletName, "ID", id, "Access Level", "Read");//Read access provided to user

	}

	/**
	 * This method will validate account plan by logging in as team member and ensure the access level
	 * @throws Exception  Fix By Sindiya : 27/02/2019
	 */

	public void validateAsTeamMember() throws Exception
	{
		String planName = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		searchAccountPlan(); // fetches the account plan created by Rep
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'" + planName + "')]")));
		report.updateTestLog("Account Plan ", "Account plan page is displayed", Status.SCREENSHOT);
		cf.clickSFButton("Edit");
		if (cf.isElementVisible(By.xpath("//span[text()='Insufficient Privileges']"), "Insufficient Privileges"))
			report.updateTestLog("Account Plan ", "Not able to validate account plan as only Read privilege was given to user", Status.PASS);
		else
			report.updateTestLog("Account Plan ", "Able to validate account plan", Status.FAIL);
	}

	/**
	 * This method will click on the given account plan name from account plans view page
	 * @throws Exception
	 */
	public void viewAccountPlanFromPlansPage() throws Exception
	{
		String strAccountPlan = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		if (cf.isElementVisible(objVeevaOneKAM.accountPlanPageTitle, "Account Plan Page"))
		{
			cf.clickButton(By.cssSelector("#bodyCell input[title='Go!']"), "Go!");
			cf.clickButton(By.xpath("//*[@class='listBody']//table//a[.='" + strAccountPlan + "']"), "Account Plan-" + strAccountPlan);
		}
	}

	public void createNewAccountTactics() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		cf.waitForSeconds(1);
		String strProfile = dataTable.getData("Login", "Profile");
		cf.waitForSeconds(1);
		String strDateString = new SimpleDateFormat("mmss").format(new Date()).toString();
		String strTacticName = "TestAutomationTactic" + strDateString;
		String strTacticDescription = "Sample tactic at" + (new Date()).toString();
		String strAccount = dataTable.getData("OneKAMAccountPlans", "Account");
		String strObjectiveName = "testobjective" + strDateString;
		String strObjectiveDescription = "sample objective at " + (new Date()).toString();
		String strTacticEndDate = "";
		String strLinkletName = "";
		Date dNow = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dNow);
		cal.add(Calendar.DATE, 5);
		int intSequenceNumber = 0;//Integer.parseInt(dataTable.getData("OneKAMAccountPlans", "SequenceNumber").replaceAll(" ", ""));
		String strTacticStatus = "In Progress";
		if (strLocale.trim().equalsIgnoreCase("US"))
			strLinkletName = "Account Objectives/Goals";
		else
			strLinkletName = "Account Tactics";
		cf.clickSFLinkLet(strLinkletName);
		cf.clickSFButton("New Account Objective/Tactic", "pbButton");
		if (strLocale.trim().equalsIgnoreCase("EU1") || strLocale.trim().equalsIgnoreCase("EU2"))
		{
			cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"), "Record Type", "Account Plan Tactic");
			cf.clickSFButton("Continue", "pbButtonb");
		}
		//	strTacticStartDate=vf.formatDateTimeMarketWise(dNow, strLocale, strProfile, "date");
		strTacticEndDate = vf.formatDateTimeMarketWise(cal.getTime(), strLocale, strProfile, "date");

		cf.setSFData("Information", "Tactic Name", "input", strTacticName);
		if (!strLocale.trim().equalsIgnoreCase("US"))
		{
			cf.setSFData("Information", "Tactic Description", "textarea", strTacticDescription);
			cf.setSFData("Information", "Objective Name", "input", strObjectiveName);
			cf.setSFData("Information", "Objective Description", "textarea", strObjectiveDescription);
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Account", "lookupimage"), "Account", strAccount);
			//cf.setSFData("Information", "Sequence", "input",Integer.toString(++intSequenceNumber));
			//cf.setSFData("Information", "Tactic Start Date","textbox",strTacticStartDate);
			cf.setSFData("Information", "Tactic End Date", "textbox", strTacticEndDate);
			cf.selectData(cf.getSFElementXPath("Information", "Tactic Status", "select"), "Tactic Status", strTacticStatus);
		}

		else
		{
			//	cf.setSFData("Information", "Start Date","textbox",strTacticStartDate);
			cf.setSFData("Information", "End Date", "textbox", strTacticEndDate);

		}

		cf.clickSFButton("Save");
		clickAccountPlanInTacticPage();

		cf.clickSFLinkLet(strLinkletName);
		//cf.clickSFLinkLet("Account Tactics");

		cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Tactic Name", strTacticName);
		dataTable.putData("OneKAMAccountPlans", "TacticName", strTacticName);

		if (!strLocale.trim().equalsIgnoreCase("CN") && !strLocale.trim().equalsIgnoreCase("US"))
		{
			cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Tactic Description", strTacticDescription);
			cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Objective Name", strObjectiveName);
			cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Objective Description", strObjectiveDescription);

		}
		if (!strLocale.trim().equalsIgnoreCase("US"))
		{
			//	cf.verifyDataFromLinkLetTable(strLinkletName,"Tactic Name",strTacticName,"Tactic Start Date",strTacticStartDate);
			cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Tactic End Date", strTacticEndDate);
			//cf.verifyDataFromLinkLetTable(strLinkletName,"Tactic Name",strTacticName,"Sequence",Integer.toString(intSequenceNumber));
			cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "Tactic Status", strTacticStatus);
		}
		else
		{
			String usertye = dataTable.getData("Login", "UserType");
			if (!usertye.equals("Rep"))
				cf.verifyDataFromLinkLetTable(strLinkletName, "Tactic Name", strTacticName, "End Date", strTacticEndDate);
		}

	}

	public void addAccountTacticsTaskAndSave() throws Exception
	{

		String strLinkletName;
		if (driver.getCurrentUrl().toLowerCase().contains("usfull"))
			strLinkletName = "Account Objectives/Goals";
		else
			strLinkletName = "Account Tactics";
		cf.clickSFLinkLet(strLinkletName);
		//cf.clickSFLinkLet("Account Tactics");

		if (cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Account Tactics')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List"))
		{
			WebElement element1 = driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Account Tactics')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[1]"));
			cf.clickElement(element1, "Show More");
			cf.waitForSeconds(2);
		}
		String strTacticName = dataTable.getData("OneKAMAccountPlans", "TacticName");
		String strAccount = dataTable.getData("OneKAMAccountPlans", "Account");

		String strDateString = new SimpleDateFormat("mmss").format(new Date()).toString();
		String strTacticTaskName = "AutomationTacticTask" + strDateString;
		String strTacticComments = "Comments for Task" + (new Date()).toString();

		cf.clickButton(By.xpath("//th[(normalize-space(@class)='dataCell')]/a[normalize-space(text())='" + strTacticName.trim() + "']"), "Tactic Name");
		cf.clickSFButton("New Task", "pbButton");
		if (driver.getCurrentUrl().toLowerCase().contains("usfull"))
		{
			String strActivityName = dataTable.getData("OneKAMAccountPlans", "ActivityName");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Activity", "lookupimage"), "Activity", strActivityName);
			cf.setSFData("In the \"Task Name\" Below Please Type the Word \"Activity\"", "Task Name", "input", strTacticTaskName);
		}
		else
			cf.setSFData("Information", "Task Name", "input", strTacticTaskName);
		cf.clickSFButton("Information", "Due Date", "dateformat");
		String locale = dataTable.getData("Login", "Locale");
		if (locale.equals("US"))
		{
			String usertype = dataTable.getData("Login", "UserType");
			if (!usertype.equals("Rep"))
				cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "Status", "select"), "Tactic Status");
		}
		else
		{
			cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "Status", "select"), "Tactic Status");
		}
		if (!driver.getCurrentUrl().toLowerCase().contains("usfull"))
		{
			cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "Priority", "select"), "Tactic Priority");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Assigned To", "lookupimage"), "Assigned To", "");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Account", "lookupimage"), "Account", strAccount);
			cf.setSFData("Information", "Comment", "textarea", strTacticComments);
		}
		cf.clickSFButton("Save");
		clickAccountTactic();
		clickAccountPlanInTacticPage();
		cf.clickSFLinkLet(strLinkletName);
	}

	/**
	 * Core 28 VRM-5984 Account plan owner deletes the created tactic
	 */

	public void deleteTactics() throws Exception
	{
		cf.clickSFLinkLet("Account Tactics");
		String strTacticName = dataTable.getData("OneKAMAccountPlans", "TacticName");
		cf.waitForSeconds(5);
		driver.findElement(By.xpath("//th[contains(.,'" + strTacticName + "')]/preceding-sibling::td/a[contains(.,'Del')]"));
		By tacticDelete = By.xpath("//th[contains(.,'" + strTacticName + "')]/preceding-sibling::td/a[contains(.,'Del')]");
		if (cf.isElementVisible(tacticDelete, "Delete Tactics"))
		{
			cf.waitForSeconds(5);
			driver.findElement(tacticDelete).click();
			cf.waitForSeconds(5);
			//cf.handleAlert();
			cf.acceptAlert();
			cf.waitForSeconds(5);
			String d = cf.verifyDataFromLinkLetTable_deleteRecord("Account Tactics", "Tactic Name", strTacticName, "Tactic Name", strTacticName);
			if (d == null)
			{
				report.updateTestLog("Account Tactics", "Account Tactics is deleted", Status.PASS);
				System.out.println("Tactic is deleted");
			}
			else if (d.equals(strTacticName))
			{
				report.updateTestLog("Account Tactics", "Account Tactics is not deleted", Status.FAIL);
			}
		}

		else
			report.updateTestLog("Account Tactics", "Delete link not available", Status.FAIL);

	}
	/*Core -28 changes ends*/

	public void recordACallAndTagAccountPlan() throws Exception
	{
		String strAccount = dataTable.getData("OneKAMAccountPlans", "Account");
		cf.waitForSeconds(1);
		String strAccountPlan = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		cf.waitForSeconds(1);
		By page = null;
		String strAccountType = dataTable.getData("OneKAMAccountPlans", "AccountType");
		String strlocale = dataTable.getData("Login", "Locale");
		String strprofile = dataTable.getData("Login", "Profile");
		Veeva_CallsStandardization vcs = new Veeva_CallsStandardization(scriptHelper);
		Veeva_CallsStandardizationMedical vcsm = new Veeva_CallsStandardizationMedical(scriptHelper);

		vcs.searchAccountForRecordCall(strlocale, strprofile, strAccountType, strAccount);

		try
		{

			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.recordCallButton));
			if (cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call"))
			{
				report.updateTestLog("Verify able to see Record Call Button", "Able to see Record Call Button", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call");
				if (strprofile.equals("DxL"))
				{
					page = ObjVeevaEmail.dxlcall;
				}
				else
				{
					page = ObjVeevaCallsStandardization_Commercial.newcallReportPage;

					//wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.recordType));
					if (!(new Select(driver.findElement(ObjVeevaCallsStandardization_Commercial.recordType)).getFirstSelectedOption().getText().trim().equalsIgnoreCase("Call Report")))
					{
						cf.selectData(ObjVeevaCallsStandardization_Commercial.recordType, "Record Type", "Call Report");
						wait.until(ExpectedConditions.elementToBeClickable(page));
					}
				}
				wait.until(ExpectedConditions.elementToBeClickable(page));
				if (cf.isElementVisible(page, "New Call Report Page"))
				{
					report.updateTestLog("Verify able to see New Call Report Page", "Able to see New Call Report Page", Status.DONE);
					vcsm.enterCommercialProfessionalInformation();

					//if(strlocale.trim().equalsIgnoreCase("EU2")||strlocale.trim().equalsIgnoreCase("ANZ")){
					cf.clickButton(By.xpath("//*[normalize-space(text())='Account Plan']/following::span[@class='lookupInput']//a"), "Search Account Plan");
					cf.waitForSeconds(2);
					report.updateTestLog("Search Account Plan page", "is displayed", Status.SCREENSHOT);
					cf.setData(By.xpath("//label[.='Search']//following::input[1]"), "Search Account Plan", strAccountPlan);
					report.updateTestLog("Search Account Plan data", "is Entered", Status.SCREENSHOT);
					cf.clickButton(objCallsForMedical.btnGo, "Go");
					cf.waitForSeconds(3);
					cf.clickButton(objCallsForMedical.linkInsideSearch, "Account Plan");
					//}
					/*/
					else
						cf.setSFData("Professional Information", "Account Plan", "select", strAccountPlan);
						//*/
					if (strlocale.trim().equalsIgnoreCase("ANZ"))
					{
						String ele = driver.findElement(By.xpath("//select[@id='AZ_ANZ_Call_Type__c']//option[2]")).getText();
						cf.selectData(ObjVeevaCallsStandardization_Commercial.azsubtype, "AZ subtype", ele);
					}
				}
				else
					report.updateTestLog("Verify able to see New Call Report Page", "Unable to see New Call Report Page", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			report.updateTestLog("Verify able to see New Call Report Page", "Error occurred : " + e.getMessage(), Status.FAIL);
		}
	}

	public void addAttendeesForCallReport() throws Exception
	{

		Veeva_CallsStandardization vcs = new Veeva_CallsStandardization(scriptHelper);
		String strAttendees = dataTable.getData("OneKAMAccountPlans", "DecisionMakingUnit");
		vcs.addattendes(strAttendees);
	}

	/**
	 * This method will submit call report and put the data value inside one kam datatable
	 */
	public void submitCallReportForOneKAM()
	{
		String strCallReportID = (new Veeva_CallsStandardization(scriptHelper).submitrecordcallInsideCallReports());
		dataTable.putData("OneKAMAccountPlans", "CallReportID", strCallReportID);

	}

	/**
	 * @throws Exception 
	 * 
	 */
	public void addProductsForCallReport() throws Exception
	{

		Veeva_CallsStandardization vcs = new Veeva_CallsStandardization(scriptHelper);
		Veeva_CallsStandardizationMedical vcsm = new Veeva_CallsStandardizationMedical(scriptHelper);

		String strProduct = dataTable.getData("OneKAMAccountPlans", "Product");
		String strKeyMessages = dataTable.getData("OneKAMAccountPlans", "KeyMessagesComments");

		String strlocale = dataTable.getData("Login", "Locale");

		if (strlocale.trim().equalsIgnoreCase("CN"))
		{
			vcsm.selectProduct(strProduct);
			vcsm.keyMessagesAndComments(strProduct, strKeyMessages);
		}
		else
		{
			int size = driver.findElements(By.xpath("//table[@id='vod_detailing']//td[2]")).size();
			if (size > 0)
				vcs.addproducts();
		}
	}

	/**
	 * Validate alert is present or not if yes accept the alert else skip
	 */
	public void alertvalidate()
	{
		try
		{
			driver.switchTo().alert().accept();
		}
		catch (Exception e)
		{
		}
	}

	public void validateCallReportInAccountPlans() throws Exception
	{
		String strCallReportID = dataTable.getData("OneKAMAccountPlans", "CallReportID");
		String strAccount = dataTable.getData("OneKAMAccountPlans", "Account");
		cf.clickSFLinkLet("Calls (Account Plan)");
		cf.verifyDataFromLinkLetTable("Calls (Account Plan)", "Call Name", strCallReportID, "", "");
		cf.verifyDataFromLinkLetTable("Calls (Account Plan)", "Call Name", strCallReportID, "Account", strAccount);
		cf.verifyDataFromLinkLetTable("Calls (Account Plan)", "Call Name", strCallReportID, "Status", "Submitted");
	}

	public void editAccountTacticsAndVerifyDaysDelay() throws Exception
	{
		String strTacticName = dataTable.getData("OneKAMAccountPlans", "TacticName");
		cf.waitForSeconds(1);
		String strLocale = dataTable.getData("Login", "Locale");
		cf.waitForSeconds(1);
		String strProfile = dataTable.getData("Login", "Profile");

		cf.clickSFLinkLet("Account Tactics");
		cf.clickLinkFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName);
		cf.clickSFButton("Edit", "pbButton");

		Date dNow = new Date();
		GregorianCalendar calStartDate = new GregorianCalendar();
		calStartDate.setTime(dNow);
		calStartDate.add(Calendar.DATE, 5);
		GregorianCalendar calEndDate = new GregorianCalendar();
		calEndDate.setTime(dNow);
		calEndDate.add(Calendar.DATE, 10);

		String strTacticStartDate = vf.formatDateTimeMarketWise(calStartDate.getTime(), strLocale, strProfile, "date");
		String strTacticEndDate = vf.formatDateTimeMarketWise(calEndDate.getTime(), strLocale, strProfile, "date");
		cf.setSFData("Information", "Tactic Start Date", "textbox", strTacticStartDate);
		cf.setSFData("Information", "Tactic End Date", "textbox", strTacticEndDate);
		String strBaselineDate = cf.getSFData("Information", "Baseline End Date", "div", "text");
		Date dBaseline = vf.formatAndGetDate(strBaselineDate, strLocale, strProfile);
		Date dTacticEndDate = vf.formatAndGetDate(strTacticEndDate, strLocale, strProfile);
		long daysDiff = getDaysBetweenDates(dBaseline, dTacticEndDate);
		dataTable.putData("OneKAMAccountPlans", "DaysDelay", Long.toString(daysDiff));
		cf.clickSFButton("Save");
		searchAccountPlan();
		cf.clickSFLinkLet("Account Tactics");
		cf.verifyDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Days Delay", Long.toString(daysDiff));
	}

	private long getDaysBetweenDates(Date d1, Date d2)
	{
		return Math.abs(TimeUnit.MILLISECONDS.toDays(d1.getTime() - d2.getTime()));
	}

	public void editAccountTacticsStatusAndVerify() throws Exception
	{
		String strTacticName = dataTable.getData("OneKAMAccountPlans", "TacticName");
		cf.clickSFLinkLet("Account Tactics");
		cf.clickLinkFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName);
		cf.clickSFButton("Edit", "pbButton");
		String strTacticStatus = "Completed";
		cf.selectData(cf.getSFElementXPath("Information", "Tactic Status", "select"), "Tactic Status", strTacticStatus);
		cf.clickSFButton("Save");
		clickAccountPlanInTacticPage();
		cf.clickSFLinkLet("Account Tactics");
		cf.verifyDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Tactic Name", strTacticName);
		cf.verifyDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Tactic Status", strTacticStatus);
	}

	public void modifyConfidenceStatus() throws Exception
	{
		By byConfidenceStatus = cf.getSFElementXPath("Confidence Status", "Status", "div");
		//By byConfidenceLevel=cf.getSFElementXPath("Confidence Status", "Confidence Level", "div");
		By byConfidenceReason = cf.getSFElementXPath("Confidence Status", "Confidence Reason", "div");
		cf.scrollToElement(byConfidenceStatus, "Confidence Status");
		doubleClickAndModifyData(byConfidenceStatus, "Status", "select", "Critical");
		//doubleClickAndModifyData(byConfidenceLevel,"Confidence Level","select","Very High");
		doubleClickAndModifyData(byConfidenceReason, "Confidence Reason", "textarea", "Reason changed for test");
		cf.clickSFButton("Save", "pbButton");
		cf.scrollToElement(byConfidenceStatus, "Confidence Status");
		validateConfidenceStatus("red_square");

		doubleClickAndModifyData(byConfidenceStatus, "Status", "select", "At Risk");
		cf.clickSFButton("Save", "pbButton");
		cf.scrollToElement(byConfidenceStatus, "Confidence Status");
		validateConfidenceStatus("yellow_triangle");

		doubleClickAndModifyData(byConfidenceStatus, "Status", "select", "On Schedule");
		cf.clickSFButton("Save", "pbButton");
		cf.scrollToElement(byConfidenceStatus, "Confidence Status");
		validateConfidenceStatus("green_circle");

		doubleClickAndModifyData(byConfidenceStatus, "Status", "select", "Unspecified");
		cf.clickSFButton("Save", "pbButton");
		cf.scrollToElement(byConfidenceStatus, "Confidence Status");
		validateConfidenceStatus("gray_square");
	}
	
	

	private void doubleClickAndModifyData(By byElement, String strElementDesc, String strElementType, String strValueToSet) throws Exception
	{
		Actions action = new Actions(driver.getWebDriver());
		WebElement element = driver.findElement(byElement);
		action.doubleClick(element).perform();
		if (strElementType.trim().equalsIgnoreCase("select"))
			cf.selectData(By.xpath("//td[normalize-space(text())='" + strElementDesc + "']/following::select"), strElementDesc, strValueToSet);
		else
		{
			cf.setData(By.xpath("//h2[.='" + strElementDesc + "']/parent::div/parent::div/following-sibling::div//textarea"), strElementDesc, strValueToSet);
			cf.clickButton(By.xpath("//h2[.='" + strElementDesc + "']/parent::div/parent::div/following-sibling::div//input[@value='OK']"), "OK");
		}
	}

	private void validateConfidenceStatus(String expectedStatusIndicator) throws Exception
	{
		try
		{
			String strStatusIndicator = cf.getData(By.xpath("//h3[normalize-space(text())='Confidence Status']/parent::div/following-sibling::div[@class='pbSubsection']//td[.='Status Indicator']/following-sibling::td//img"), "Status Indicator", "src");
			if (strStatusIndicator.trim().toLowerCase().contains(expectedStatusIndicator))
				report.updateTestLog("Validate Confidence Status Indicator", "Status indicator is set to '" + expectedStatusIndicator + "' as expected", Status.PASS);
			else
				report.updateTestLog("Validate Confidence Status Indicator", "Status is not changed.Expected: '" + expectedStatusIndicator + "' ;Actual : '" + strStatusIndicator, Status.FAIL);
		}
		catch (Exception e)
		{
			report.updateTestLog("Validate Confidence Status Indicator", "Error occurred : " + e.getMessage(), Status.FAIL);
		}
	}

	public void enterValueProposition() throws Exception
	{
		doubleClickAndModifyData(cf.getSFElementXPath("Value Proposition", "Patients", "div"), "Patients", "textarea", "Patients info changed for test");
		doubleClickAndModifyData(cf.getSFElementXPath("Value Proposition", "Customer/External Stakeholders", "div"), "Customer/External Stakeholders", "textarea", "customer info changed for test");
		doubleClickAndModifyData(cf.getSFElementXPath("Value Proposition", "AstraZeneca", "div"), "AstraZeneca", "textarea", "sample test");
		cf.clickSFButton("Save", "pbButton");
	}

	public void enterAccountQualityAndComplianceAndSave() throws Exception
	{
		int iScore = 0;
		cf.clickSFLinkLet("Account Quality and Compliance");
		cf.clickSFButton("New Account Quality and Compliance", "pbButton");
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Uncover Opportunities", "Opportunities identified & tested?", "select"), "Opportunities identified & tested").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Uncover Opportunities", "Key insights included?", "select"), "Key insights included").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Uncover Opportunities", "Patient journey flow identified?", "select"), "Patient journey flow identified").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Uncover Opportunities", "Diagnostics environment identified?", "select"), "Diagnostics environment identified").replaceAll(" ", ""));

		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Uncover Opportunities']/following::div[1]//*[starts-with(@class,'labelCol')]//*[contains(text(),'behavior identified?')]/following::select"), "Customer behavior identified").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Uncover Opportunities", "Account SWOT leverage insights?", "select"), "Account SWOT leverage insights").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Uncover Opportunities']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Pharmacy insights identified')]/following::select"), "Pharmacy Insights identified").replaceAll(" ", ""));

		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Set Direction", "Key stakeholders mapped?", "select"), "Opportunities identified & tested").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Set Direction", "Critical success factors aligned?", "select"), "Critical success factors aligned").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Set Direction']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Value proposition identified')]/following::select"), "Value proposition identified & aligned").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Set Direction", "Strategic Objectives follow SMART?", "select"), "Strategic Objectives follow SMART").replaceAll(" ", ""));

		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Plan", "External stakeholder map alignment?", "select"), "External stakeholder map alignment").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Plan']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Demonstrate input')]/following::select"), "Demonstrate input & leverage knowledge").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Plan", "Identified competitor activity?", "select"), "Identified competitor activity").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Plan']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Identified process')]/following::select"), "Identified process & timeline").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Plan']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Tactics and Tasks defined')]/following::select"), "Tactics and Tasks defined & aligned").replaceAll(" ", ""));

		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Deliver']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Cross-functional consultation')]/following::select"), "Cross-functional consultation & work").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Deliver", "Specific projects been completed?", "select"), "Specific projects been completed").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(By.xpath("//*[text()='Deliver']/following::div[1]//*[contains(@class,'labelCol')]//*[contains(text(),'Frequency of review')]/following::select"), "Frequency of review & update").replaceAll(" ", ""));
		iScore = iScore + Integer.parseInt(cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Deliver", "Contingency planning for barriers?", "select"), "Contingency planning for barriers").replaceAll(" ", ""));

		cf.selectData(cf.getSFElementXPath("Compliance", "Adheres to free text principles?", "select"), "Adheres to free text principles", "Yes");
		String region = dataTable.getData("Login", "Locale");
		String calculatedScore = "";
		if ((region.trim().equalsIgnoreCase("eu2")) || (region.trim().equalsIgnoreCase("BR")) || (region.trim().equalsIgnoreCase("RU")))
			calculatedScore = Integer.toString(iScore) + ",00";
		else
			calculatedScore = Integer.toString(iScore) + ".00";
		cf.clickSFButton("Save", "pbButtonb");

		String strAQCName = cf.getData(By.xpath("//*[normalize-space(text())='Account Quality and Compliance Name']/following-sibling::td/div"), "Quality Compliance Name", "text");
		String displayedScore = cf.getData(By.xpath("//*[normalize-space(text())='Score']/parent::td/following-sibling::td/div"), "Score", "text");
		if (calculatedScore.trim().equalsIgnoreCase(displayedScore))
			report.updateTestLog("Validate Quality Compliance Score", "Score is '" + displayedScore + "' as expected", Status.PASS);
		else
			report.updateTestLog("Validate Quality Compliance Score", "Score is not correct. Actual :" + displayedScore + ". expected: " + calculatedScore, Status.FAIL);

		dataTable.putData("OneKAMAccountPlans", "AQCName", strAQCName);
		dataTable.putData("OneKAMAccountPlans", "QualityComplianceScore", calculatedScore);
		System.out.println("completed aqc as manager score : " + calculatedScore);
	}

	public void validateQualityComplianceScore() throws Exception
	{
		cf.waitForSeconds(1);
		String strAQCName = dataTable.getData("OneKAMAccountPlans", "AQCName");
		cf.waitForSeconds(1);
		String calculatedScore = dataTable.getData("OneKAMAccountPlans", "QualityComplianceScore");
		System.out.println("entered aqc as rep ");
		cf.clickSFLinkLet("Account Quality and Compliance");
		cf.verifyDataFromLinkLetTable("Account Quality and Compliance", "Account Quality and Compliance Name", strAQCName, "Account Quality and Compliance Name", strAQCName);
		cf.verifyDataFromLinkLetTable("Account Quality and Compliance", "Account Quality and Compliance Name", strAQCName, "Score", calculatedScore);
		cf.verifyDataFromLinkLetTable("Account Quality and Compliance", "Account Quality and Compliance Name", strAQCName, "Adheres to free text principles?", "Yes");
		String acqDate = cf.getDataFromLinkLetTable("Account Quality and Compliance", "Account Quality and Compliance Name", strAQCName, "Created Date");
		String textRelList = cf.getDataFromLinkLetTable("Account Quality and Compliance", "Account Quality and Compliance Name", strAQCName, "Adheres to free text principles?");
		System.out.println("The ACQ creation date " + acqDate);
		System.out.println("completed aqc as rep score : " + calculatedScore);
		//Fix By Sindiya : 25/02/2019
		int intScore = new Double(calculatedScore.replace(",", ".")).intValue();
		System.out.println("intScore = " + intScore);
		String acq = driver.findElement(By.xpath("//td[contains(@class,'last labelCol') and text()='Score']/following-sibling::td/div")).getText();
		String acqSecDate = driver.findElement(By.xpath("//span[@class='helpButton' and text()='Last Review Date']/parent::td/following-sibling::td[contains(@class,'dataCol col02 inlineEditLock')]/div")).getText();
		String acqSecText = driver.findElement(By.xpath("//td[contains(@class,'labelCol') and text()='Adheres to free text principles?']/following-sibling::td/div")).getText();
		int scoreInACQ = new Double(acq).intValue();
		System.out.println("The score seen in the ACQ section " + scoreInACQ);
		System.out.println("The date in ACQ section - " + acqSecDate);
		cf.scrollToElement(By.xpath("//h3[text()='Account Quality and Compliance']/preceding-sibling::img"), "Account Quality and Compliance section");
		if ((acqSecDate.equals(acqDate)) && (textRelList.equalsIgnoreCase(acqSecText)))
			report.updateTestLog("Quality and compliance date check and Adheres to free text principles field in the Account Quality and Compliance section", "Date is " + acqSecDate + "as expected and the value in Adheres to free text principles is " + acqSecText, Status.DONE);
		else
			report.updateTestLog("Quality and compliance date check and Adheres to free text principles field in the Account Quality and Compliance section", "Date is " + acqSecDate + "not as expected and the value in Adheres to free text principles is " + acqSecText, Status.DONE);

		if (intScore == scoreInACQ)
			report.updateTestLog("Quality and compliance score update in the Account Quality and Compliance section", "Score is " + intScore + "as expected", Status.PASS);
		else
			report.updateTestLog("Quality and compliance score update in the Account Quality and Compliance section", "Score is not" + scoreInACQ + " as expected", Status.FAIL);
	}

	public void addNoteAndAttachment() throws Exception
	{
		cf.clickSFLinkLet("Notes & Attachments");
		cf.clickSFButton("New Note", "pbButton");
		String strDateString = new SimpleDateFormat("mmss").format(new Date()).toString();
		cf.setSFData("Note Information", "Title", "textbox", "SampleNote" + strDateString);
		cf.setSFData("Note Information", "Body", "textarea", "Sample Body for automation at " + (new Date()).toString());
		report.updateTestLog("Notes for account plan", "Notes are entered", Status.SCREENSHOT);
		cf.clickSFButton("Note Information", "Save", "pbButtonb");

		try
		{
			File file = new File("C:\\workspace\\samplefiletoattach.txt");
			boolean fvar = file.createNewFile();
			if (fvar)
				System.out.println("File has been created successfully");
			else
				System.out.println("File already present at the specified location");
			FileOutputStream outputStream = new FileOutputStream("C:\\workspace\\samplefiletoattach.txt");
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-16");
			BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
			bufferedWriter.write("Text added for testing");
			bufferedWriter.newLine();
			bufferedWriter.write("Welcome to Veeva Sales Force!");
			bufferedWriter.close();
		}
		catch (IOException e)
		{
			System.out.println("Exception Occurred:");
			e.printStackTrace();
		}

		cf.clickSFLinkLet("Notes & Attachments");
		cf.clickSFButton("Attach File", "pbButton");
		driver.findElement(By.id("file")).sendKeys("C:\\workspace\\samplefiletoattach.txt");
		driver.findElement(By.id("Attach")).click();
		report.updateTestLog("Attachments for account plan", "File attachment is done", Status.SCREENSHOT);
		cf.clickButton(By.cssSelector("input[title='Done']"), "Attachment Done");
		cf.clickSFLinkLet("Notes & Attachments");
		cf.verifyDataFromLinkLetTable("Notes & Attachments", "Title", "SampleNote" + strDateString, "", "");
		cf.verifyDataFromLinkLetTable("Notes & Attachments", "Title", "samplefiletoattach.txt", "", "");
	}

	/**
	 * This method will goto Link related list, create link and validate in account plan related list
	 * @throws Exception
	 */
	public void addLinkAndValidate() throws Exception
	{

		cf.clickSFLinkLet("Link");
		cf.clickSFButton("New Link", "pbButton");

		String strDateString = new SimpleDateFormat("mmss").format(new Date()).toString();
		String strLinkName = "SampleLink" + strDateString;
		String strLinkDescription = "http://" + "Linkdescription" + strDateString + ".com";
		cf.setSFData("Information", "Link Name", "textbox", strLinkName);
		cf.setSFData("Information", "Link", "textbox", strLinkDescription);
		cf.clickSFButton("Save", "pbButtonb");
		//if(cf.getSFPageTitle().trim().equalsIgnoreCase("Link"))
		cf.clickButton(By.xpath("//td[@class='labelCol'][.='Account Plan']/following-sibling::td//a"), "Account Plan");
		cf.clickSFLinkLet("Link");

		cf.verifyDataFromLinkLetTable("Link", "Link Name", strLinkName, "Link Name", strLinkName);
		cf.verifyDataFromLinkLetTable("Link", "Link Name", strLinkName, "Link", strLinkDescription);
	}

	/**
	 * This method will click account tactic in Account tactics task saved page
	 * @throws Exception
	 */
	public void clickAccountTactic() throws Exception
	{
		if (driver.getCurrentUrl().contains("usfull"))
			cf.clickButton(By.xpath("//td[@class='labelCol'][.='Account Objective/Goal']/following-sibling::td//a"), "Account Tactic");
		else
			cf.clickButton(By.xpath("//td[@class='labelCol'][.='Account Tactic']/following-sibling::td//a"), "Account Tactic");
	}

	/**
	 * This method will click account plan in account tactics saved page 
	 * @throws Exception
	 */
	public void clickAccountPlanInTacticPage() throws Exception
	{
		cf.clickButton(By.xpath("//td[@class='labelCol'][.='Account Plan']/following-sibling::td//a"), "Account Plan");
	}

	public void clickAccountHCAForOneKAM() throws Exception
	{
		String strAttendee = dataTable.getData("OneKAMAccountPlans", "DecisionMakingUnit");
		new Veeva_CallsStandardization(scriptHelper).clickaccountHCA(strAttendee);
	}

	/**
	 * This method will enter values for Patient Insight Summary related list and validate the same and the section of Patient Insight Summary
	 * @throws Exception  - Fix by Sindiya 26/02/2019
	 */

	public void patientInsightSummary() throws Exception
	{
		String accPlanName = dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		String accPlan = null;
		//Create 2 insight records 
		for (int j = 1; j <= 2; j++)
		{
			cf.clickSFLinkLet("Patient Insights");
			cf.clickSFButton("New Patient Insight", "pbButton");
			//Patient Insight Edit page is loaded
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Patient Insight Edit']")));
			report.updateTestLog("Patient Insight Summary Edit", "Patient Insight Summary edit page is loaded", Status.SCREENSHOT);
			driver.findElement(By.xpath("//span[@class='dateFormat']/a")).click();
			accPlan = driver.findElement(By.xpath("//span[@class='lookupInput']/input")).getAttribute("value");
			if (accPlan.equalsIgnoreCase(accPlanName))
				report.updateTestLog("Patient Insight Summary Edit", "Account plan name is auto populated", Status.DONE);
			else
				report.updateTestLog("Patient Insight Summary Edit", "Account plan name is not auto populated", Status.FAIL);
			cf.setSFData("Number of Unique Patients", "input", "5");
			cf.setSFData("Number of New Patients", "input", "4");
			cf.setSFData("Number of Stopped Patients", "input", "2");
			cf.setSFData("Number of Cumulative Patients", "input", "1");
			cf.clickSFButton("Save");

			//Patient Insight Detail page is loaded
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Patient Insight Detail']")));
			String insightNum = driver.findElement(By.className("pageDescription")).getText().trim();
			System.out.println("Insight number " + insightNum);

			int unq = Integer.parseInt(driver.findElement(By.xpath("//span[text()='Number of Unique Patients']/parent::td/following-sibling::td/div")).getText().trim());
			int newPat = Integer.parseInt(driver.findElement(By.xpath("//span[text()='Number of New Patients']/parent::td/following-sibling::td/div")).getText().trim());
			int stop = Integer.parseInt(driver.findElement(By.xpath("//span[text()='Number of Stopped Patients']/parent::td/following-sibling::td/div")).getText().trim());
			int cumml = Integer.parseInt(driver.findElement(By.xpath("//span[text()='Number of Cumulative Patients']/parent::td/following-sibling::td/div")).getText().trim());

			cf.clickLink(By.xpath("//td[text()='Account Plan']/following-sibling::td[contains(@class,'dataCol col02 inlineEditLock')]/div/a"), "Account Plan");
			cf.clickSFLinkLet("Patient Insights");
			cf.verifyDataFromLinkLetTable("Patient Insights", "Patient Insight Name", insightNum, "Number of New Patients", Integer.toString(newPat));
			cf.verifyDataFromLinkLetTable("Patient Insights", "Patient Insight Name", insightNum, "Number of Unique Patients", Integer.toString(unq));
			cf.verifyDataFromLinkLetTable("Patient Insights", "Patient Insight Name", insightNum, "Number of Stopped Patients", Integer.toString(stop));
			cf.verifyDataFromLinkLetTable("Patient Insights", "Patient Insight Name", insightNum, "Number of Cumulative Patients", Integer.toString(cumml));
		}

		int rows = cf.getRowCountFromLinkLetTable("Patient Insights");
		System.out.println("Number of records" + rows);
		int newPatient = 0, uniquePatients = 0, stopPatient = 0, cummPatient = 0;
		//Adding the values of the 2 records from the related list
		for (int i = 1; i <= rows; i++)
		{
			int w = Integer.parseInt(cf.getDataFromLinkLetTable("Patient Insights", "Number of New Patients", i + 1));
			newPatient = newPatient + w;
			int x = Integer.parseInt(cf.getDataFromLinkLetTable("Patient Insights", "Number of Unique Patients", i + 1));
			uniquePatients = uniquePatients + x;
			int y = Integer.parseInt(cf.getDataFromLinkLetTable("Patient Insights", "Number of Stopped Patients", i + 1));
			stopPatient = stopPatient + y;
			int z = Integer.parseInt(cf.getDataFromLinkLetTable("Patient Insights", "Number of Cumulative Patients", i + 1));
			cummPatient = cummPatient + z;
		}

		System.out.println("total of new patients" + newPatient);
		System.out.println("total of unique patients" + uniquePatients);
		System.out.println("total of stopped patients" + stopPatient);
		System.out.println("total of cummulaitve patients" + cummPatient);

		cf.scrollToElement(By.xpath("//h3[text()='Patient Insight Summary']"), "Patient Insight Summary section in account plan detail");
		cf.waitForSeconds(2);

		//From the section Patient insight summary
		int nPatients = Integer.parseInt(driver.findElement(By.xpath("//td[text()='Sum of New Patients This Year']/following-sibling::td[contains(@class,'dataCol col02 inlineEditLock')]/div")).getText().trim());
		int unique = Integer.parseInt(driver.findElement(By.xpath("//td[text()='Sum of Unique Patients This Year']/following-sibling::td[contains(@class,'dataCol col02 inlineEditLock')]/div")).getText().trim());
		int stopped = Integer.parseInt(driver.findElement(By.xpath("//td[text()='Sum of Stopped Patients This Year']/following-sibling::td[contains(@class,'dataCol col02 inlineEditLock')]/div")).getText().trim());
		int cummulative = Integer.parseInt(driver.findElement(By.xpath("//td[text()='Sum of Cumulative Patients This Year']/following-sibling::td[contains(@class,'dataCol last col02 inlineEditLock')]/div")).getText().trim());

		if ((uniquePatients == unique) && (nPatients == newPatient) && (stopped == stopPatient) && (cummulative == cummPatient))
			report.updateTestLog("Patient Insight Summary Section", "Pateint insight summary values are updated in the section", Status.PASS);
		else
			report.updateTestLog("Patient Insight Summary Section", "Pateint insight summary values are not updated in the section", Status.FAIL);

	}
}
