package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaEmail;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Maill extends ReusableLibrary {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	public Maill(ScriptHelper scriptHelper) {
		super(scriptHelper);

	}
	String strPageTitle=null; 
	String mailpage=null;
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	
	public void optInConfimration(String emailID, String veevaPage) throws Exception
	{
		boolean emailFound = true;
		String mailPage="";
		System.out.println("Veeva Page title: "+veevaPage);
		String subject = "AstraZeneca email opt-in confirmation";
		cf.openUrlInNewTab("https://www.mailinator.com/");
		if(cf.isElementVisible(ObjVeevaEmail.inputEmail,"Mailinator - Loginpage"))
		{
			report.updateTestLog("Verify Mailinator login page is displayed", "Mailinator loginpage is displayed", Status.PASS);
			cf.setData(ObjVeevaEmail.inputEmail, "Email ID textbox", emailID);
			WebElement go=driver.findElement(ObjVeevaEmail.searchmail);
			cf.clickElement(go, "Go!");
			String strXPath = "//div[@id='inboxpane']//table/tbody/tr";	
			int iEmailCount = driver.findElements(By.xpath(strXPath)).size();
			System.out.println("iEmailCount: " + iEmailCount); 							
			String subInEmail = driver.findElement(By.xpath("//table[contains(@class,'table table-striped jambo_table')]/tbody/tr[1]/td[4]")).getText().trim();
			System.out.println("The latest email recieved is "+subInEmail);
			do{
				if(subInEmail.equals(subject))
				{
					emailFound = true;
					report.updateTestLog("Opt in confirmation email", "Opt - in confirmation email with subject - "+subInEmail+" is found", Status.PASS);
				}
				else 
				{
					System.out.println("Email not yet recieved");
					cf.pageRefresh();
					cf.pageRefresh();
					emailFound = false;
				}
			} while(!emailFound);
			
			if(emailFound = true)
			{
				driver.findElement(By.xpath("//table[contains(@class,'table table-striped jambo_table')]/tbody/tr[1]/td[4]")).click();//click the email subject
				//report.updateTestLog("Email opened", "Subject - "+subInEmail+" is clicked to open the email", Status.PASS);
				if(cf.isElementVisible(By.id("msg_body"), "Frame"))
					cf.switchToFrame(By.id("msg_body"), "msg_body");
				if(cf.isElementVisible(By.xpath("//a[text()='Click here to confirm']"), "Link to confirm"))
				{
					report.updateTestLog("Email opened", "Link to confirm opt - in is seen and clicked", Status.PASS);
					driver.findElement(By.xpath("//a[text()='Click here to confirm']")).click();
					mailPage = driver.getTitle();
					System.out.println("Mailinator page title "+mailPage);
					cf.waitForSeconds(12);
					cf.switchToLastOpenedPage();
					Boolean flag = false;
					do{	
						By ack = By.xpath("//td[contains(text(),'Thank you for providing confirmation of your email address and for opting-in')]");
						//wait.until(ExpectedConditions.visibilityOfElementLocated(ack));
						if(cf.isElementVisible(ack,"Opt-in Acknowledged"))
						{
							String confrimation_text = driver.findElement(ack).getText().trim();
							System.out.println(confrimation_text);
							flag=true;
							report.updateTestLog("Email opt-in confirmation", "Opt-in confirmation acknowledged", Status.PASS);
							break;
						}
						else
						{
							cf.switchToPage(mailPage);
							cf.switchToFrame(By.id("msg_body"), "msg_body");
							driver.findElement(By.xpath("//a[text()='Click here to confirm']")).click();
							cf.waitForSeconds(10);
							cf.switchToLastOpenedPage();
						}
					}while(flag==false);
					driver.close();
					
				}
			}
			 			
		}
		else
		{
			report.updateTestLog("Verify Mailinator login page", "Mailinator login page is not displayed", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		
		cf.switchToPage(mailPage);
		driver.close();
		driver.switchTo().window(veevaPage);
	}

	public Boolean openMailinatorEmail(String strEmail_Id, String strEmail_Subject) throws Exception{
		String strCurrentMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String strCurrentClassName = Thread.currentThread().getStackTrace()[1].getFileName();
		System.out.println("businesscomponents." + strCurrentClassName + "." + strCurrentMethodName + " method is called");
		Boolean bOpenMailinatorEmail = false;
		//String strEmailId = dataTable.getData(strDataSheet, "Login_EmailAddress");
		//String strEmail_Subject = dataTable.getData(strDataSheet, "Login_ForgotPwd_MailSubject");
		//cf.switchToParentFrame();
		strEmail_Subject = strEmail_Subject.replace("Email: ", "");
		//rf.openUrlInNewTab(strMailinatorUrl);	
		strPageTitle = driver.getTitle();
		cf.openUrlInNewTab("https://www.mailinator.com/");	
		Boolean bMailinatorLoginPage = false;
		if(cf.isElementVisible(ObjVeevaEmail.inputEmail,"Mailinator - Loginpage")){
			report.updateTestLog("Verify Mailinator login page is displayed", "Mailinator loginpage is displayed", Status.PASS);
			bMailinatorLoginPage = true; 			
		}else{
			report.updateTestLog(strCurrentMethodName, "Mailinator login page is not displayed", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		Boolean bEmailFound = false;
		By byEmailSubject = null;
		String strAppSubject = null;
		if(bMailinatorLoginPage){
			cf.setData(ObjVeevaEmail.inputEmail, "Email ID textbox", strEmail_Id);
			WebElement go=driver.findElement(ObjVeevaEmail.searchmail);
			cf.clickElement(go, "Go!");
			String strXPath = "//div[@id='inboxpane']//table/tbody/tr";	
			int iEmailCount = driver.findElements(By.xpath(strXPath)).size();
			System.out.println("iEmailCount: " + iEmailCount); 			
			for(int iEmail = 1; iEmail <= iEmailCount; iEmail++){
				//			By byEmailFrom = By.xpath(strXPath + "[" + iEmail + "]//div[@title='FROM']");
				//			byEmailSubject = By.xpath(strXPath + "[" + iEmail + "]//div[@class='all_message-min_text all_message-min_text-3']");
				//			By byEmailTime = By.xpath(strXPath + "[" + iEmail + "]//div[@class='all_message-min_datte all_message-min_datte-3 ng-binding']");
				By byEmailFrom = By.xpath(strXPath + "[" + iEmail + "]/td[3]");
				byEmailSubject = By.xpath(strXPath + "[" + iEmail + "]/td[4]");
				By byEmailTime = By.xpath(strXPath + "[" + iEmail + "]/td[5]");                     
				strAppSubject = cf.getData(byEmailSubject, iEmail +"EmailSubject", "text").replace("Sandbox: ", "");
				String strAppEmailFrom = cf.getData(byEmailFrom, iEmail +"EmailFrom", "text");
				String strAppEmailTime = cf.getData(byEmailTime, iEmail +"EmailTime", "text");
				if(strAppSubject.equals(strEmail_Subject)){
					bEmailFound = true;
					cf.isElementVisible(byEmailSubject, "EmailSubject: " + strEmail_Subject);
					cf.waitForSeconds(3); // to take screenshot to visible in test results
					report.updateTestLog(strCurrentMethodName, "Email came from " + strAppEmailFrom + " with subject as " 
							+ strEmail_Subject + " in time: " + strAppEmailTime, Status.PASS);
					System.out.println("Email came from " + strAppEmailFrom + " with subject as " 
							+ strEmail_Subject + " in time: " + strAppEmailTime);
					break;
				}
			}//for loop
			if(!bEmailFound){
				report.updateTestLog(strCurrentMethodName, "Email is not came from " + strEmail_Id + " with subject as " + strEmail_Subject, Status.FAIL);
			}
		}//bMailinatorLoginPage
		Boolean bFrame_MsgBody = false;
		if(bEmailFound){	
			//Click on the Email Subject
			WebElement click=driver.findElement(byEmailSubject);
			cf.clickElement(click, "Email Subject: " + strAppSubject);
			mailpage=driver.getTitle();
			System.out.println(mailpage);
			cf.waitForSeconds(3); // to take screenshot to visible in test results
			report.updateTestLog(strCurrentMethodName, "Email with subject as " + strEmail_Subject + " is Opened", Status.PASS);
			System.out.println("Email with subject as " + strEmail_Subject + " is Opened");			
			By byFrame_MsgBody = By.xpath("//iframe[@id='msg_body']");
			bFrame_MsgBody = cf.switchToFrame(byFrame_MsgBody, "byFrame_MsgBody");	
			System.out.println(bFrame_MsgBody);
			System.out.println(strPageTitle + "Page title");
			cf.switchToParentFrame();
			cf.waitForSeconds(3);
			driver.switchTo().defaultContent();
			cf.switchToPage(strPageTitle);
			cf.waitForSeconds(5);
			System.out.println(strPageTitle);
			
			//cf.switchBacktoOriginalTab();
			
		}//bEmailFound
		if(bFrame_MsgBody){
			bOpenMailinatorEmail = true;			
		}//bbyFrame_MsgBody
		return bOpenMailinatorEmail;
	}//openMailinatorEmail() method

	public void deleteAllMailsFromMailinator() throws Exception{
		driver.switchTo().defaultContent();
		By byInbox = By.xpath("//div[@class='lb-container']//span[@id='inbox_button']/i");
		cf.clickByJSE(byInbox,"Mailinator Inbox");
		String strXPath = "//section[@class='single_mail']//ul[@id='inboxpane']/li";
		By byInboxPane = By.xpath(strXPath);
		cf.isElementVisible(byInboxPane,"Mailinator InboxPane");
		int iEmailCount = driver.findElements(By.xpath(strXPath)).size();
		System.out.println("iEmailCount: " + iEmailCount);	
		for(int iEmail = 1; iEmail <= iEmailCount; iEmail++){	
			//delete mail
			By byMailSelectCheckBox = By.xpath(strXPath + "[" + iEmail + "]//i[contains(@id,'checkoff')]");
			cf.clickByJSE(byMailSelectCheckBox, iEmail + " - Mail Checkbox");		
		}//for loop
		By byMailDelete = By.xpath("//span[@title='Delete Emails']/i[@id='trash_but']");
		cf.clickByJSE(byMailDelete,"byMailDelete");
		cf.switchToPage(strPageTitle);

		//waitForSeconds(3);
		//iEmailCount = driver.findElements(By.xpath(strXPath)).size();
		//System.out.println("iEmailCount: " + iEmailCount);
	}


	public void verifyEmailSentToGmail(String strEmailId, String strPassword, String strSubject) throws Exception{
		cf.switchToParentFrame();
		//String strEmailId = "peptest@mailinator.com"; peptestautomation@gmail.com String strSubject = "Email: Here’s the item you asked for";
		strSubject = strSubject.replace("Email: ", "");
		String strMailUrl ="https://www.google.com/gmail/about/#";
		//https://accounts.google.com/
		strPageTitle = driver.getTitle();
		cf.openUrlInNewTab(strMailUrl);	

		By byGmailSignIn = By.xpath("//a[text()='Sign In']");
		cf.clickifElementPresent(byGmailSignIn, "byGmailSignIn");	

		String strXPath = "//div[@role='tabpanel']//table/tbody/tr";
		By byGmailInboxMails = By.xpath(strXPath);
		Boolean bGmailInboxFound = false;
		if(cf.isElementPresent(ObjVeevaEmail.usernamegmail, "Gmail Username")){
			report.updateTestLog("verifyEmailSentToGmail", "Gmail Login Page", Status.PASS);
			cf.isElementVisible(ObjVeevaEmail.usernamegmail, "Gmail UserName",50);
			cf.setData(ObjVeevaEmail.usernamegmail, "Gmail UserName", strEmailId);
			cf.clickButton(ObjVeevaEmail.gmailPasswordNextBtn, "gmailUsernameNextBtn");
			cf.isElementVisible(ObjVeevaEmail.gmailPassword, "gmailPassword",50);
			cf.setData(ObjVeevaEmail.gmailPassword, "gmailPassword", strPassword);
			cf.clickButton(ObjVeevaEmail.gmailPasswordNextBtn, "gmailPasswordNextBtn");		
			if(cf.isElementPresent(byGmailInboxMails, "GmailInboxMails")){
				bGmailInboxFound = true;
				report.updateTestLog("verifyEmailSentToGmail", "Gmail Inbox", Status.PASS);
			}else{
				report.updateTestLog("Verify Gmail login page is displayed", "Login to Gmail Id: <b>" + strEmailId + "</b> failed.",Status.FAIL);
			}
		}
		String strCurrentMonth = new SimpleDateFormat("MMM").format(new Date());
		if(bGmailInboxFound){
			int iEmailCount = driver.findElements(byGmailInboxMails).size();
			System.out.println("iEmailCount: " + iEmailCount);
			Boolean bEmailFound = false;		
			for(int iEmail = 1; iEmail <= iEmailCount; iEmail++){
				By byEmailFrom = By.xpath(strXPath + "[" + iEmail + "]/td[4]/div[2]/span");
				By byEmailSubject = By.xpath(strXPath + "[" + iEmail + "]/td[6]//span[@class='bog']");
				By byEmailTime = By.xpath(strXPath + "[" + iEmail + "]/td[8]//span");
				String strAppSubject = cf.getData(byEmailSubject, iEmail +"EmailSubject", "text").replace("Sandbox: ", "");
				String strAppEmailFrom = cf.getData(byEmailFrom, iEmail +"EmailFrom", "email");
				String strAppEmailTime = cf.getData(byEmailTime, iEmail +"EmailTime", "text");
				if(strAppEmailFrom.toLowerCase().contains(strCurrentMonth)){
					break;
				}
				if(strAppSubject.equals(strSubject)){
					bEmailFound = true;
					cf.highLightElement(byEmailSubject, strAppSubject);
					String strMailPageTitle = driver.getTitle();
					cf.switchToPage(strMailPageTitle);
					//report.updateTestLog("verify Email to Patient's Email Id:"," <b>", strEmailId + "</b> - Email came from Email Id: <b>" + strAppEmailFrom + "</b> with subject as <b>" + strSubject + "</b> in time: " + strAppEmailTime, Status.PASS);
					//report.updateTestLog("verify Email to Patient's Email Id:", "Email came from Email Id: " + strAppEmailFrom + " with subject as " + strSubject + " in time: " + strAppEmailTime  ,Status.PASS );
					report.updateTestLog("Verify the Email", "Email came with "+strSubject+"   from "+strAppEmailFrom+"on "+strAppEmailTime+"  time ", Status.PASS);

					System.out.println("Email came from Email Id: " + strAppEmailFrom + " with subject as " + strSubject + " in time: " + strAppEmailTime);
					WebElement clickel=driver.findElement(By.xpath("//table[@id=':30']//td[4]"));
					cf.clickElement(clickel, "Click on Email");
					cf.waitForSeconds(3);
					report.updateTestLog("Verify able to view the  mail", "Able to view the mail", Status.SCREENSHOT);
					break;

				}
			}//for loop	
			if(!bEmailFound){
				report.updateTestLog("Verify Gmail login page is displayed", "<b>" + strEmailId + "</b> - Email is not came from " + "Your Advogate" + " with subject as <b>" + strSubject + "</b>", Status.FAIL);		
			}
		}


		//.switchToPage(strPageTitle);
	}//verifyEmailSentToGmail

	public void deletegmailmails() throws Exception{
		if(cf.isElementVisible(By.xpath("//div[@class='ar6 T-I-J3 J-J5-Ji']"), "Back to inbox")){
			WebElement ele=driver.findElement(By.xpath("//div[@class='ar6 T-I-J3 J-J5-Ji']"));
			cf.clickElement(ele, "Back to Inbox");
		}
		cf.clickifElementPresent(By.xpath("//div[@class='T-Jo-auh' and@role='presentation']"), "Select all mails for deletion");
		cf.clickifElementPresent(By.xpath("//div[@class='ar9 T-I-J3 J-J5-Ji']"), "Delete all mails");
		cf.switchToPage(strPageTitle);



	}

}