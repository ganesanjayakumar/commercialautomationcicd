package businesscomponents;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.mfa.framework.Status;

import ObjectRepository.objGlobalAccountSearchPage;
import ObjectRepository.objVeevaUSGAS;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Veeva_US_GlobalAccountSearch extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	public Veeva_US_GlobalAccountSearch(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * This method is used to enter empty values in HCM Search page to invoke GAS assuming that the service is down
	 * @throws Exception
	 */
	public void testGASWhenHCMDown() throws Exception{
		Veeva_US_HCMSearchAndAdd hcmsearch=new Veeva_US_HCMSearchAndAdd(scriptHelper);
		hcmsearch.enterValuesInHCMSearch("HCP","","","","PENNSYLVANIA");
		for(int i=0;i<4;i++){
			if(cf.isElementVisible(By.cssSelector("td.messageCell>div"), "Error Message")){
				String errorMessage=driver.findElement(By.cssSelector("td.messageCell>div")).getText();
				if(errorMessage.trim().contains("Your search returned too many results"))
					hcmsearch.enterValuesInHCMSearch("HCP","","","","");
			}
			else
				break;
		}
	}


	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToUSGASinHCMSearchServiceDown() throws Exception{
		if(cf.isElementVisible(objVeevaUSGAS.objHCMServiceDownMessage, "HCM Search service down")){
			report.updateTestLog("Verify able to HCM Search Results", "HCM Search And Add service is down", Status.SCREENSHOT);
			cf.clickSFButton("Global Account Search", "submit");
			if(cf.isElementVisible(objVeevaUSGAS.objUSGasHeader, "Global Account Search Header"))
				report.updateTestLog("Verify GAS page navigation", "Navigation to Global account search page is successful",Status.PASS);
			else
				report.updateTestLog("Verify able to navigate to Search for Account page", "Unable to navigate to Surveys Home page", Status.FAIL);
		}
		else
			report.updateTestLog("Verify HCM Search Results navigation", "Unexpected search results page is displayed", Status.FAIL);
	}

	/**
	 * This method is used to verify column name headers in GAS results page
	 */
	public void verifyGASResultsHeader() throws Exception{
		ArrayList<String> columnNames=new ArrayList<String>();
		columnNames.add("Created Date");
		columnNames.add("Record Type");
		columnNames.add("Name");
		columnNames.add("Type");
		columnNames.add("Sub Type");
		columnNames.add("Specialty");
		columnNames.add("Status");
		columnNames.add("AZ ID");
		columnNames.add("License #");
		columnNames.add("Account Phone");
		columnNames.add("Address line");
		columnNames.add("City");
		columnNames.add("Zip");
		WebElement tblSearchRes = driver.findElement(objGlobalAccountSearchPage.tblSearchResults);
		WebElement headerRow = tblSearchRes.findElement(By.tagName("tr"));
		List<WebElement> listName = headerRow.findElements(By.tagName("td"));
		int nameCol=-1;
		String missingColumnNames="";
		for(String strColumnName : columnNames){
			for (int idx=0; idx < listName.size(); idx++){
				WebElement td = listName.get(idx);
				if (td.getText().trim().contains(strColumnName)){
					nameCol = idx;
					break;
				}
			}
			if(nameCol!=-1)
				report.updateTestLog("Verification of Column Names in GAS", "Column name: '" + strColumnName +"' is displayed in : "+ nameCol +" column", Status.DONE);
			else if(nameCol==-1)
				missingColumnNames=strColumnName + "," +missingColumnNames;
		}
		if(missingColumnNames.trim()!="")
			report.updateTestLog("Verification of Column names in GAS", "The following columns are not displayed : "+missingColumnNames, Status.FAIL);
	}

	/**
	 * This method is used to verify account in GAS History based on value provided in datatable
	 * @throws Exception
	 */
	public void verifyGASAlignmentHistoryUS() throws Exception{
		if(cf.isElementVisible(By.cssSelector("[alt*='GAS Alignment History']"), "GAS Alignment History")){
			WebElement link=driver.findElement(By.xpath("//img[contains(@alt,'GAS Alignment History')]/parent::a"));
			new Actions(driver.getWebDriver()).keyDown(Keys.CONTROL).click(link)	.keyUp(Keys.CONTROL).build().perform();
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(1));
			String strAccountInGASHistory=cf.getData(By.xpath("//td[contains(@class,'pbTitle')]//*[text()='GAS Alignment History Detail']/following::div[@class='pbSubsection']//tr//*[normalize-space(text())='Account']/following::td[1]"), "Account Name", "text");//cf.getSFData("GAS Alignment History Detail", "Name", "Text", "text");
			String strGASHistorySaved=dataTable.getData("GlobalAccountSearch","GASHistoryAccount");
			if(strGASHistorySaved.trim().contains(","))
				strGASHistorySaved=strGASHistorySaved.split(",")[1]+" "+strGASHistorySaved.split(",")[0];
			if(strGASHistorySaved.trim().equalsIgnoreCase(strAccountInGASHistory))
				report.updateTestLog("Verification of GAS History", strGASHistorySaved +"' account is added successfully", Status.PASS);
			else
				report.updateTestLog("Verification of GAS History", strGASHistorySaved +"' account is not displayed in GAS History", Status.FAIL);
			driver.close();
			driver.switchTo().window(tabs.get(0));
		}
		else
			report.updateTestLog("Verification of GAS History", "GAS History page is not displayed", Status.FAIL);
	}

	
}
