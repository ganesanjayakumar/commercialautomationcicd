package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.objMyAccounts;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class DxL_MyAccounts extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	public DxL_MyAccounts(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	/**
	 * Method to navigate to Global Account Search page
	 * 
	 * @throws Exception
	 */
	public void navigateToMyAccounts() throws Exception
	{
		vf.clickVeevaTab("My Accounts");
	}
	// Click the hospital name from My Accounts Page
	public void openAccount_DXL() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		vf.clickVeevaTab("Home");
		vf.clickVeevaTab("My Accounts");
		if (strAccount_Name == null || strAccount_Name.isEmpty())
		{
			report.updateTestLog("Account name is missing in the sheet", "Kindly update the account name ", Status.FAIL);
		}
		else
		{
			openAccount_DXL(strAccount_Name);
		}
	}// openAccount()
	public void openAccount_DXL(String strAccount_Name) throws Exception
	{
		Boolean bOpenAccount = false;
		String strAccount_Name_Returned = vf.searchAndGo("Accounts", strAccount_Name);
		By account_TopName = By.xpath("//h2[contains(text(),'" + strAccount_Name_Returned + "')]");
		if (!(strAccount_Name_Returned == null || strAccount_Name_Returned.isEmpty()))
		{
			if (cf.isElementVisible(account_TopName, "Account Details" + strAccount_Name_Returned))
			{
				String strAppAccount_Name = cf.getSFData("Name", "text", "text");
				if (strAppAccount_Name.contains(strAccount_Name_Returned))
				{
					report.updateTestLog("Verify Account Details Opened", "Account Details Opened for Account: " + strAccount_Name_Returned, Status.PASS);
					bOpenAccount = true;
				}
			}
			else
			{
				
				// Verify Search Results page is opened.
				By byPageType = cf.getSFElementXPath("PageType", "PageType");
				if (cf.isElementVisible(byPageType, "Search Results Page"))
				{
					// Get Search Account
					String strTableXPath = cf.getTableXPath("Search", "Accounts");
					if (cf.isElementVisible(By.xpath(strTableXPath), "Accounts - Search Table"))
					{
						String strAppAccount_Name = cf.verifyGetClickSearchDataFromTable("Accounts", "Name", strAccount_Name, "", "", "Click", "Search");
						if (strAppAccount_Name != null)
						{
							if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name_Returned, 5))
							{
								strAppAccount_Name = cf.getSFData("Name", "text", "text");
								strAppAccount_Name = strAppAccount_Name.replace("[View Hierarchy]", "").trim();
								if (strAppAccount_Name.equals(strAccount_Name_Returned))
								{
									report.updateTestLog("Verify Account Details Opened", "Account Details Opened for Account: " + strAccount_Name_Returned, Status.PASS);
									bOpenAccount = true;
								}
							}
						}
					}
				}
			}
		}
		if (!bOpenAccount)
		{
			throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name_Returned);
		}
	}
	public void checkMenuButtons_DXL() throws Exception
	{
		String sMenuButton = "";
		// List<String> optionsCall=new
		// ArrayList<String>(Arrays.asList("Edit","Calendar","View
		// Hierarchy","View Overview","Record a Call","New Key Medical
		// Insight"));
		List<String> optionsCall = new ArrayList<String>(Arrays.asList("Edit", "Calendar", "View Hierarchy", "View Overview", "Record a Call"));
		System.out.println(optionsCall.size());
		for (int i = 0; i < optionsCall.size(); i++)
		{
			String option = optionsCall.get(i).toString();
			System.out.println(option);
			List<WebElement> optionele = driver.findElements(By.xpath("//td[@class='pbButton' and @id='topButtonRow']/input[contains(@onclick,'navigateToUrl')]"));
			System.out.println(optionele.size());
			for (int j = i; j < optionele.size(); j++)
			{
				sMenuButton = optionele.get(j).getAttribute("value").toString();
				System.out.println(sMenuButton);
				sMenuButton = sMenuButton.trim();
				if (option.equalsIgnoreCase(sMenuButton))
				{
					report.updateTestLog("The required menu button is available ", "The" + sMenuButton + "button is available: ", Status.PASS);
				}
				else
				{
					report.updateTestLog("The required menu button is not available ", "The" + sMenuButton + "button is not available: ", Status.FAIL);
				}
				break;
			}
		}
	}
	public void checkMenuButtons_dxl_us() throws Exception
	{
		String sMenuButton = "";
		// List<String> optionsCall=new
		// ArrayList<String>(Arrays.asList("Edit","Calendar","View
		// Hierarchy","View Overview","Record a Call","New Key Medical
		// Insight"));
		List<String> optionsCall = new ArrayList<String>(Arrays.asList("Edit", "Calendar", "View Hierarchy", "View Timeline", "Record a Call"));
		System.out.println(optionsCall.size());
		for (int i = 0; i < optionsCall.size(); i++)
		{
			String option = optionsCall.get(i).toString();
			System.out.println(option);
			List<WebElement> optionele = driver.findElements(By.xpath("//td[@class='pbButton' and @id='topButtonRow']/input[contains(@onclick,'navigateToUrl')]"));
			System.out.println(optionele.size());
			for (int j = i; j < optionele.size(); j++)
			{
				sMenuButton = optionele.get(j).getAttribute("value").toString();
				System.out.println(sMenuButton);
				sMenuButton = sMenuButton.trim();
				if (option.equalsIgnoreCase(sMenuButton))
				{
					report.updateTestLog("The required menu button is available ", "The" + sMenuButton + "button is available: ", Status.PASS);
				}
				else
				{
					report.updateTestLog("The required menu button is not available ", "The" + sMenuButton + "button is not available: ", Status.FAIL);
				}
				break;
			}
		}
	}
	public void viewOverview_DXL() throws Exception
	{
		cf.scrollToTop();
		By byViewOverview = cf.getSFElementXPath("View Overview", "pbButton");
		if (cf.isElementVisible(byViewOverview, "View Overview", 5))
		{
			// String strAccount_Category = dataTable.getData("MyAccounts",
			// "Account_Category");
			// By byAccount_Name = cf.getSFElementXPath("Name", "text");
			String strAccount_Name = cf.getData(objMyAccounts.accountDetails_TopName, "Account_Name", "Text");
			String strUserName = cf.getData(objMyAccounts.login_UserNavLabel, "User Name", "Text");
			cf.clickSFButton("View Overview", "pbButton");
			cf.waitForSeconds(5);
			Boolean bFrameSwitch = cf.switchToFrame(objMyAccounts.frame_VOD, "frame_VOD");
			if (bFrameSwitch)
			{
				By byPageLoad_Spinner = By.xpath("//div[@class='spinner']/parent:://span[@us-spinner][@class='ng-scope ng-hide']");
				cf.isElementPresent(byPageLoad_Spinner, "Page Load Spinner");
				By bylistViewportWrapper = By.xpath("//div[@class='listViewportWrapper']");
				By bytimelineLegend = By.xpath("//div[@class='timelineLegend']");
				if (cf.isElementVisible(bylistViewportWrapper, "Overview"))
				{
					report.updateTestLog("Verify Account Details Overview", "Account Details Overview Opened for Account: " + strAccount_Name, Status.PASS);
					// Verify Current Month
					By byCurrentMonth = By.xpath("//div[@class='monthHeader currentMonth']/span[@class='ng-binding']");
					String strCurrentMonth = cf.getData(byCurrentMonth, "Current Month", "text");
					report.updateTestLog("Verify Account Details Overview", "Account Details - Current Month " + strCurrentMonth, Status.DONE);
					// cf.verifyPageElement(byCurrentMonth, "Current Month",
					// "text", strExpectedCurrentMonth, "Exact", false);
					// Uncheck all key events and verify No Activities
					// present////////////////////////////////////////////////////////////////////////////
					if (cf.isElementVisible(bytimelineLegend, "Show Timeline section"))
					{
						////// Filter with Show only my activities and verify it
						cf.clickByJSE(objMyAccounts.filterByOwnerCheckbox, "filterByOwnerCheckbox");
						By byMonths = By.xpath("//div[@class='timeline']/div");
						int iMonthCount = driver.findElements(byMonths).size();
						for (int i = 2; i <= iMonthCount; i++)
						{
							String strXPath = "//div[@class='timeline']/div[" + i + "]";
							cf.scrollToElement(By.xpath(strXPath), (i - 1) + "Month");
							if (verifyMonthEmpty(strXPath))
							{
								continue;
							}
							By byMonth_Expand_Status = By.xpath(strXPath + "/div[contains(@class,'monthTimeline')]");
							By byMonth_Id = By.xpath(strXPath);
							String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
							report.updateTestLog("verifyViewOverview", strMonth_Id + " - Activity Items", Status.SCREENSHOT);
							String strmonthTimeline = cf.getData(byMonth_Expand_Status, "Month_Collapse_Status", "class");
							if (!strmonthTimeline.equalsIgnoreCase("monthTimeline"))
							{
								// Expand
								By byMonth_Header = By.xpath(strXPath + "/div[contains(@class,'monthHeader')]/span[@class='arrow']");
								cf.clickButton(byMonth_Header, "Expand Time line Activities for " + strMonth_Id);
							} /// Expand the Month View
							/////// Each Day in Month
							By byDays = By.xpath(strXPath + "/div[contains(@class,'monthTimeline')]/div");
							int iDaysCount = driver.findElements(byDays).size();
							for (int iDay = 1; iDay <= iDaysCount; iDay++)
							{
								String strDayXPath = strXPath + "/div[contains(@class,'monthTimeline')]/div[" + iDay + "]";
								// By byDay = By.xpath(strDayXPath);
								By byDay_timelineItem = By.xpath(strDayXPath + "/div[contains(@class,'timelineItem')]");
								// By byDay_IsEmpty = By.xpath(strDayXPath +
								// "/div[@class='emptyDay']");
								// int iEmptyDay =
								// driver.findElements(byDay_IsEmpty).size();
								driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
								int iItem_Count = driver.findElements(byDay_timelineItem).size();
								// cf.isElementPresent(byDay_IsEmpty,"Day is
								// Empty");
								if (iItem_Count > 0)
								{///////// Day is not Empty
									By byItem_Divs = By.xpath(strDayXPath + "/div");
									int iItem_DivsCount = driver.findElements(byItem_Divs).size();
									for (int iItem_Div = 1; iItem_Div <= iItem_DivsCount; iItem_Div++)
									{// Each Div in Day
										By byItem_Div = By.xpath(strDayXPath + "/div[" + iItem_Div + "]");
										String strItem_Div = cf.getData(byItem_Div, iItem_Div + "Item_Div", "class");
										if (strItem_Div.contains("timelineItemRight") || strItem_Div.contains("timelineItemLeft"))
										{
											By byOwner = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//span[@ng-show='timelineItem.owner.name' or @ng-show='timelineItem.createdBy.name']");
											String strOwner = cf.getData(byOwner, "Item - Owner", "text");
											strOwner = strOwner.replace("- ", "");
											By byItem_Date = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//div[@class='timelineSubtext ng-binding']");
											String strItem_Date = cf.getData(byItem_Date, "Item - Date", "text");
											By byItem_Name = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//div[@class='timelineActivityName ng-binding']");
											String strItem_Name = cf.getData(byItem_Name, "Item - Name", "text");
											strItem_Name = strItem_Name.replace(" - " + strUserName, "");
											if (strOwner.equals(strUserName))
											{
												// cf.verifyActualExpectedData("Item
												// Owner", strOwner,
												// strUserName,"Exact",false);
												report.updateTestLog("Verify Owner for Activity Name: " + strItem_Name + " on Date: " + strItem_Date,
														"Actual Value: (" + strOwner + ") matches with Expected Value: (" + strUserName + ").", Status.DONE);
											}
											else
											{////// Error
												report.updateTestLog("Verify Owner for Activity Name: " + strItem_Name + " on Date: " + strItem_Date,
														"Actual Value: (" + strOwner + ") not matches with Expected Value: (" + strUserName + ").", Status.FAIL);
											}
										}
									} // For iItem_DivsCount loop
								} // Day is not Empty
							} // for iDay loop
						} // for Month loop
						////// UnFilter with Show only my activities and verify
						////// it
						cf.clickByJSE(objMyAccounts.filterByOwnerCheckbox, "filterByOwnerCheckbox");
						// Uncheck all Key Events
						// cf.scrollToTop();
						// cf.scrollToTop();
						cf.scrollElementToMiddlePage(objMyAccounts.timeLine_Calls, "Calls");
						// cf.scrollToElement(objMyAccounts.timeLine_Calls,
						// "timeLine_Calls", 400);
						// UnCheck all the Items
						By byTimeLineObjects = By.xpath("//div[@class='timelineLegend']/div");
						int iTimeLineObjectsCount = driver.findElements(byTimeLineObjects).size();
						for (int i = 2; i <= iTimeLineObjectsCount; i++)
						{
							By byTimeLineObj = By.xpath("//div[@class='timelineLegend']/div[" + i + "]//input");
							String strTimeLineObj_Label = cf.getData(By.xpath("//div[@class='timelineLegend']/div[" + i + "]//label//span[@class='labelText ng-binding']"), (i - 1) + "TimeLineObj",
									"text");
							cf.clickByJSE(byTimeLineObj, strTimeLineObj_Label);
						}
						cf.waitForSeconds(3);
						report.updateTestLog("View Overview", "View Overview - Screenshot", Status.SCREENSHOT);
						for (int i = 2; i <= iMonthCount; i++)
						{
							String strXPath = "//div[@class='timeline']/div[" + i + "]";
							if (!verifyMonthEmpty(strXPath))
							{
								By byMonth_Id = By.xpath(strXPath);
								String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
								report.updateTestLog("Verify Activities displayed for the Month ", " Activities displayed the Month: " + strMonth_Id + " is not Expected.", Status.FAIL);
							}
						}
					}
					else
					{
						throw new FrameworkException("Overview Details - Show Timeline section not found.");
					}
				}
				else
				{
					throw new FrameworkException("Overview Details not Opened for Account: " + strAccount_Name);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				cf.clickLink(objMyAccounts.overView_AccountLink, "overView_AccountLink");
				if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
				{
					report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.PASS);
				}
				else
				{
					// report.updateTestLog("Verify app is back to Account
					// Details Page", "Account Details not Opened for Account: "
					// + strAccount_Name, Status.FAIL);
					throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
				}
			} // bFrameSwitch
		}
		else
		{
			report.updateTestLog("Verify View Overview", "View Overview Button not Present", Status.FAIL);
		}
		//
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	public Boolean verifyMonthEmpty(String strMonthXPath) throws Exception
	{
		Boolean bMonthEmpty = false;
		By byMonthSummary_Class = By.xpath(strMonthXPath + "/div[contains(@class,'monthSummary')]");
		By byMonth_Id = By.xpath(strMonthXPath);
		String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
		String strMonth_Summary = cf.getData(byMonthSummary_Class, "byMonthSummary_Class", "class");
		if (!strMonth_Summary.equalsIgnoreCase("monthSummary"))
		{
			// Collapse
			By byMonth_Collapse = By.xpath(strMonthXPath + "/div[contains(@class,'monthHeader')]/span[@class='arrow']");
			cf.clickButton(byMonth_Collapse, "Collapse Activities for " + strMonth_Id);
		}
		By byMonth_emptyMonth = By.xpath(strMonthXPath + "/div[contains(@class,'monthSummary')]/div[contains(@class,'emptyMonth')][normalize-space(text())='No Activities']");
		String strMonth_emptyMonth = cf.getData(byMonth_emptyMonth, "Month_Empty Activities Status", "class");
		if (strMonth_emptyMonth.equalsIgnoreCase("emptyMonth"))
		{// No Activities
			bMonthEmpty = true;
			report.updateTestLog("Verify Activities displayed for the Month: " + strMonth_Id, "No Activities displayed the Month: " + strMonth_Id, Status.DONE);
		}
		return bMonthEmpty;
	}
	public void viewRecordaCall_DXL() throws Exception
	{
		cf.scrollToTop();
		By byRecordaCall = cf.getSFElementXPath("Record a Call", "pbButton");
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		cf.clickSFButton("Record a Call", "pbButton");
		cf.waitForSeconds(10);
		report.updateTestLog("Record a Call", "Record a Call page is displaying  successfully", Status.PASS);
		WebElement bSave = driver.findElement(By.xpath("//td[@id='topButtonRow']/span/input[@type='button' and @name='Save']"));
		if (cf.isElementVisible(bSave))
		{
			cf.clickSFButton("Save", "pbButton");
			report.updateTestLog("Record a Call", "Record a Call is saved successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Record a Call", "Record a Call is not saved successfully", Status.FAIL);
		}
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Name + "']"), "After Successfull Record call Save"))
		{
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Name + "']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void viewRecordaCallUS_DXL() throws Exception
	{
		cf.scrollToTop();
		By byRecordaCall = cf.getSFElementXPath("Record a Call", "pbButton");
		String strDataSheet = "MyAccounts_DXL";
		// added by Mainak on 13th June 2019 for validation of US_DXL_Account
		// sript
		String strDataSheet1 = "Login";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		//
		String strusdxl = dataTable.getData(strDataSheet1, "Locale");
		if (cf.isElementVisible(byRecordaCall, "Record a Call", 10))
		{
			cf.clickSFButton("Record a Call", "pbButton");
			cf.waitForSeconds(10);
			report.updateTestLog("Record a Call", "Record a Call page is displaying  successfully", Status.PASS);
			// added by Mainak on 13th June 2019 for validation of
			// US_DXL_Account sript
			if (strusdxl.equals("US"))
			{
				cf.selectData(By.id("Call_Type_AZ_US__c"), "AZ Call Type", "Email");
				report.updateTestLog("AZ Call Type is Cliked", "" + "Email" + "Dropdwon option is selected", Status.PASS);
			}
			else
			{
				report.updateTestLog("AZ Call Type is not Cliked", "" + "Email" + "Dropdwon option is not selected", Status.FAIL);
			}
			WebElement bSave = driver.findElement(By.xpath("//td[@id='topButtonRow']/span/input[@type='button' and @name='Save']"));
			if (cf.isElementVisible(bSave))
			{
				cf.clickSFButton("Save", "pbButton");
				report.updateTestLog("Record a Call", "Record a Call is saved successfully", Status.PASS);
			}
			else
			{
				report.updateTestLog("Record a Call", "Record a Call is not saved successfully", Status.FAIL);
			}
		}
		// added by Mainak on 25th June 2019
		// $x("//a[contains(@class,'actionLink') and
		// //*[contains(text(),strAccount_Name)]]")
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and //*[contains(text(),strAccount_Name)]]"), "After Successfull Record call Save"))
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Name+"']"), "After Successfull Record call
		// Save"))
		{
			// added by Mainak on 25th June 2019
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and //*[contains(text(),strAccount_Name)]]"), "Go to Account home page");
			// cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and
			// text()='"+strAccount_Name+"']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void accountClassificationTip_DXL() throws Exception
	{
		cf.waitForSeconds(5);
		// cf.switchToFrame(By.name("01Nd0000000YbFX"));
		// added by mainak on 14th June 2019
		cf.switchToFrame(By.name("01NU0000000J7ft"));
		cf.scrollToElement(By.xpath("//li/a[text()='Common']"), "Edit Account Classification");
		if (cf.isElementVisible(By.xpath("//li[@class='ui-state-default ui-corner-top']/a[contains(@href,'edit')]"), "Pencil Tip icon is visible to edit"))
		{
			cf.clickLink(By.xpath("//li[@class='ui-state-default ui-corner-top']/a[contains(@href,'edit')]"), "Go to Account home page");
			report.updateTestLog("Pencil Tip icon is visible to edit ", " Pencil Tip icon is visible to edit and it is clicked successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Pencil Tip icon is visible to edit ", " Pencil Tip icon is visible to edit but it is not clicked successfully", Status.FAIL);
		}
		WebElement sClickText = driver.findElement(By.xpath("(//label[contains(@class,'button') and @aria-pressed='true'])[1]/span"));
		String sClick = sClickText.getText();
		if (sClick.equalsIgnoreCase("Off"))
		{
			cf.clickElement(By.xpath("(//label[contains(@class,'button') and @aria-pressed='false'])[1]/span"), "Click On Button");
		}
		else
		{
			cf.isElementPresent(By.xpath("(//label[contains(@class,'button') and @aria-pressed='false'])[1]/span"));
			report.updateTestLog("The Off button is present", " The Off button is ready to click", Status.PASS);
			driver.findElement(By.xpath("(//label[contains(@class,'button') and @aria-pressed='false'])[1]/span")).click();
			// cf.clickElement(By.xpath("(//label[contains(@class,'button') and
			// @aria-pressed='false'])[1]/span"), "Click Off Button");
		}
		cf.switchToParentFrame();
		cf.waitForSeconds(5);
		isAlertPresent("OK", 20);
		// cf.switchToFrame(By.name("01Nd0000000YbFX"));
		// added by mainak on 14th June 2019
		cf.switchToFrame(By.name("01NU0000000J7ft"));
		cf.clickElement(By.xpath("//tr/th[text()='Detail Groups Selector']/following-sibling::th/img"), "Click Close");
		cf.switchToParentFrame();
	}
	/**
	 * Description: isAlertPresent
	 * 
	 * @param String
	 *            strAlertDesc - Description of the alert iWaitTimeInSeconds -
	 *            Wait time in Seconds
	 * @throws Exception
	 */
	public void isAlertPresent(String strAlertDesc, int iWaitTimeInSeconds) throws Exception
	{
		Boolean bAlert = true;
		try
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 15);
			wait.until(ExpectedConditions.alertIsPresent());
			// bAlert = true;
			// driver.switchTo().alert().getText();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			// e.printStackTrace();
			bAlert = false;
		}
		if (bAlert)
		{
			Alert alert = driver.switchTo().alert();
			String strAlert_Text = alert.getText();
			alert.accept();
			report.updateTestLog("verifyExitRamp " + strAlert_Text + " is clicked", "Alert window is Closed. ", Status.DONE);
			driver.switchTo().defaultContent();
		}
	}
	public void editTerritorySpecific_DXL() throws Exception
	{
		cf.switchToFrame(By.name("01Nd0000000YbFW"));
		cf.scrollToElement(By.xpath("//td/input[@id='pbButtonTableColEdit']"), "EDIT part is displaying");
		driver.findElement(By.xpath("//input[@id='pbButtonTableColEdit' and @name='Edit']")).click();
		cf.switchToParentFrame();
		cf.waitForSeconds(6);
		cf.switchToFrame(By.id("vod_iframe"));
		if (cf.isElementVisible(By.xpath("//h3[text()='Information']"), "Territory Field Edit Page"))
		{
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'My_Target') and text()='My Target']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'TSF') and text()='TSF Name']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Account') and text()='Account']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Preferred') and text()='My Preferred Account']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Address') and text()='My Preferred Address']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Last') and text()='Last Activity Date']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'YTD') and text()='YTD Activity']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Territory') and text()='Territory']/following::td[1]"));
			cf.isElementPresent(By.xpath("//tr/td/label[contains(@id,'Priority') and text()='Priority Biomarker']/following::td[1]"));
			Select sBiomarker = new Select(driver.findElement(By.xpath("//select[contains(@name,'Priority')]")));
			sBiomarker.selectByValue("EGFR");
			cf.clickElement(By.xpath("//input[@id='pbButtonTableColSave' and @name='Save']"), "Click the save button");
		}
		cf.switchToParentFrame();
	}
	public void accountNewTargetSurvey_DXL() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		// added by Mainak on 26th June 2019
		String strAccount_Tactic_Name = dataTable.getData(strDataSheet, "Account_Tactic_Name");
		// added by Mainak on 14/06/2019
		cf.scrollToElement(By.xpath("//input[@class='btn' and @value='New Survey Target']"), "Click New Survey Target Button present");
		cf.clickElement(By.xpath("//input[@class='btn' and @value='New Survey Target']"), "Click New Survey Target Button");
		cf.waitForSeconds(5);
		cf.switchToFrame(By.id("vod_iframe"));
		if (cf.isElementPresent(By.xpath("//h2[@class='pageDescription' and text()='Add Survey Target']")))
		{
			cf.clickElement(By.xpath("(//input[@name='surveyRadio'])[1]"), "Click Radio Button");
			cf.clickElement(By.id("AddSurveyTarget"), "AddSurveyTarget Button");
			cf.switchToParentFrame();
			cf.waitForSeconds(25);
			/*
			 * cf.scrollToElement(By.
			 * xpath("//div[contains(@class,'qlist') and text()='Tracking year']"
			 * ),"Enter tracking year"); Select sTrackYear = new
			 * Select(driver.findElement(By.
			 * xpath("(//div[contains(@class,'qlist') and text()='Tracking year']/following::div/div/following::select)[1]"
			 * ))); sTrackYear.selectByVisibleText("2019"); Select sTrackMonth =
			 * new Select(driver.findElement(By.
			 * xpath("(//div[contains(@class,'qlist') and text()='Tracking year']/following::div/div/following::select)[2]"
			 * ))); sTrackMonth.selectByVisibleText("Jan");
			 */
			cf.clickSFButton("Submit", "button");
			cf.waitForSeconds(20);
		}
		if (cf.isElementPresent(By.xpath("//input[@type='button' and @value='Update Responses']"), "Update Response button"))
		{
			report.updateTestLog("Survey target is saved ", "Survey target is saved successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Survey target is saved ", "Survey target is not saved successfully", Status.FAIL);
		}
		// updated by Mainak on 26th June 2019
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Tactic_Name+"']"), "After Successfull Record
		// call Save"))
		cf.waitForSeconds(2);
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Name+"']"), "After Successfull Record call
		// Save"))
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Tactic_Name + "']"), "After Successfull Record call Save"))
		{
			// added by Mainak on 26th June 2019
			cf.waitForSeconds(5);
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Tactic_Name + "']"), "Go to Account home page");
			// cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and
			// text()='"+strAccount_Name+"']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
		}
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void accountNewTargetSurvey_DXL_US() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		// added by Mainak on 26th June 2019
		String strAccount_Tactic_Name = dataTable.getData(strDataSheet, "Account_Tactic_Name");
		// added by Mainak on 14/06/2019
		cf.scrollToElement(By.xpath("//input[@class='btn' and @value='New Survey Target']"), "Click New Survey Target Button present");
		cf.clickElement(By.xpath("//input[@class='btn' and @value='New Survey Target']"), "Click New Survey Target Button");
		cf.waitForSeconds(5);
		cf.switchToFrame(By.id("vod_iframe"));
		if (cf.isElementPresent(By.xpath("//h2[@class='pageDescription' and text()='Add Survey Target']")))
		{
			cf.clickElement(By.xpath("(//input[@name='surveyRadio'])[1]"), "Click Radio Button");
			cf.clickElement(By.id("AddSurveyTarget"), "AddSurveyTarget Button");
			cf.switchToParentFrame();
			cf.waitForSeconds(25);
			/*
			 * cf.scrollToElement(By.
			 * xpath("//div[contains(@class,'qlist') and text()='Tracking year']"
			 * ),"Enter tracking year"); Select sTrackYear = new
			 * Select(driver.findElement(By.
			 * xpath("(//div[contains(@class,'qlist') and text()='Tracking year']/following::div/div/following::select)[1]"
			 * ))); sTrackYear.selectByVisibleText("2019"); Select sTrackMonth =
			 * new Select(driver.findElement(By.
			 * xpath("(//div[contains(@class,'qlist') and text()='Tracking year']/following::div/div/following::select)[2]"
			 * ))); sTrackMonth.selectByVisibleText("Jan");
			 */
			cf.clickSFButton("Submit", "button");
			cf.waitForSeconds(20);
		}
		if (cf.isElementPresent(By.xpath("//input[@type='button' and @value='Update Responses']"), "Update Response button"))
		{
			report.updateTestLog("Survey target is saved ", "Survey target is saved successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Survey target is saved ", "Survey target is not saved successfully", Status.FAIL);
		}
		// updated by Mainak on 26th June 2019
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Tactic_Name+"']"), "After Successfull Record
		// call Save"))
		cf.waitForSeconds(2);
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Name+"']"), "After Successfull Record call
		// Save"))
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Tactic_Name + "']"), "After Successfull Record call Save"))
		{
			// added by Mainak on 26th June 2019
			cf.waitForSeconds(5);
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Tactic_Name + "']"), "Go to Account home page");
			// cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and
			// text()='"+strAccount_Name+"']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
		}
		else
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void newMedicalInsight_DxL() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		cf.clickElement(By.xpath("//input[@class='btn' and contains(@value,'Medical Insight')]"), "Click New Medical Insight Button");
		cf.waitForSeconds(5);
		if (cf.isElementVisible(By.xpath("(//h2[contains(.,'Medical Insight')])[1]"), "New Nedical Insight"))
		{
			cf.clickLink(By.xpath("//span[@class='dateFormat']/a"), "Click date");
			cf.clickElement(By.xpath("//input[@class='btn' and @name='save']"), "Click Save");
		}
		cf.waitForSeconds(10);
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Name + "']"), "After Successfull Record call Save"))
		{
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Name + "']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
			cf.waitForSeconds(5);
		}
		else
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void newMedicalInsight_US_DxL() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		// added by Mainak on 14/06/2019
		cf.waitForSeconds(5);
		cf.scrollToElement(By.xpath("//input[@class='btn' and contains(@value,'New Medical')]"), "Click New Medical Insight Button present");
		cf.waitForSeconds(5);
		cf.clickElement(By.xpath("//input[@class='btn' and contains(@value,'New Medical')]"), "Click New Medical Insight Button");
		cf.waitForSeconds(5);
		// added by Mainak on 26th June 2019
		String strAccount_Tactic_Name = dataTable.getData(strDataSheet, "Account_Tactic_Name");
		// if(cf.isElementVisible(By.xpath("(//h2[contains(.,'Medical/Dx
		// Insight')])[1]"), "New Nedical"))
		// added by mainak-18th june 2019
		if (cf.isElementVisible(By.xpath("//h2[contains(.,'New Medical/Dx Insight')]"), "New Medical/Dx Insight"))
		{
			// *[@id="Date_vod__c"]/input
			// added by Mainak on 26th June 2019
			cf.waitForSeconds(5);
			cf.clickLink(By.xpath("//*[@id='Date_vod__c']/input"), "Click date");
			// cf.clickLink(By.xpath("//span[@class='dateFormat']/a"), "Click
			// date");
			// cf.clickElement(By.xpath("//input[@class='btn' and
			// @name='Save']"), "Click Save");
			// added by Mainak on 26th June 2019
			cf.clickElement(By.xpath("//input[@type='button' and @name='Save']"), "Click Save");
		}
		cf.waitForSeconds(10);
		// added by Mainak on 26th June 2019
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and //*[contains(text(),strAccount_Name)]]"),"After Successfull
		// Record call Save"))
		if (cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink') and text()='" + strAccount_Tactic_Name + "']"), "After Successfull Record call Save"))
		// if(cf.isElementVisible(By.xpath("//a[contains(@class,'actionLink')
		// and text()='"+strAccount_Name+"']"), "After Successfull Record call
		// Save"))
		{
			// added by Mainak on 26th June 2019
			cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and //*[contains(text(),strAccount_Name)]]"), "Go to Account home page");
			// cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and
			// text()='"+strAccount_Name+"']"), "Go to Account home page");
			report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
			cf.waitForSeconds(5);
		}
		else
		{
			report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
		}
	}
	public void newDiagnostic_DXL() throws Exception
	{
		String strDataSheet = "MyAccounts_DXL";
		String strAccount_Name = dataTable.getData(strDataSheet, "Account_Name");
		// added by Mainak on 14/06/2019
		cf.scrollToElement(By.xpath("//input[@class='btn' and @value='New Diagnostic']"), "Click New Diagnostic Button present");
		cf.clickElement(By.xpath("//input[@class='btn' and @value='New Diagnostic']"), "Click New Diagnostic Button");
		cf.waitForSeconds(5);
		if (cf.isElementVisible(By.xpath("//h2[text()=' Select Diagnostic Record Type']"), "Diagnostic Record Type Page"))
		{
			cf.isElementVisible(By.xpath("//label[text()='Record Type of new record']/following::td/div/select"), "Record type list");
			Select sSelectRecordType = new Select(driver.findElement(By.xpath("//label[text()='Record Type of new record']/following::td/div/select")));
			sSelectRecordType.selectByVisibleText("Laboratory Challenge");
			cf.clickElement(By.xpath("//input[@class='btn' and @value='Continue']"), "Click continue to fill record type page");
			cf.waitForSeconds(5);
			if (cf.isElementVisible(By.xpath("//h2[text()=' New Diagnostic']"), "New Diagnostic Page"))
			{
				cf.isElementPresent(By.xpath("//td/label[text()='Biomarker']/following::td[1]"));
				Select sBiomarker = new Select(driver.findElement(By.xpath("//td/label[text()='Biomarker']/following::td[1]/div/span/select")));
				sBiomarker.selectByValue("EGFR");
				Select sChallenges = new Select(driver.findElement(By.xpath("(//select[contains(@title,'Challenges')])[1]")));
				sChallenges.selectByVisibleText("Turnaround Time");
				cf.clickElement(By.xpath("(//td[@class='multiSelectPicklistCell']/a)[1]"), "Click the add button");
				cf.clickElement(By.xpath("(//input[@class='btn' and @title='Save'])[1]"), "Click the save button");
				cf.waitForSeconds(5);
				if (cf.isElementVisible(By.xpath("//div/a[text()='" + strAccount_Name + "']"), "After Successfull Record call Save"))
				{
					// added by Mainak on 26th June 2019
					cf.clickLink(By.xpath("//a[contains(@class,'actionLink') and //*[contains(text(),strAccount_Name)]]"), "Go to Account home page");
					// cf.clickLink(By.xpath("//div/a[text()='"+strAccount_Name+"']"),
					// "Go to Account home page");
					report.updateTestLog("Account home page link is clicked ", "" + strAccount_Name + " Account home page is displaying successfully", Status.PASS);
					cf.waitForSeconds(5);
				}
				else
				{
					report.updateTestLog("Account home page link is not clicked", "" + strAccount_Name + " Account home page is not displaying successfully", Status.FAIL);
				}
			}
		}
	}
}// End of Class
