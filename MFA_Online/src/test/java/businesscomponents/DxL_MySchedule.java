package businesscomponents;

import supportlibraries.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import ObjectRepository.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import com.mfa.framework.Status;


public class DxL_MySchedule extends VeevaFunctions {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	private veevaMyScheduleCalls ms = new veevaMyScheduleCalls(scriptHelper);
	private veevaEmail ve = new veevaEmail(scriptHelper);


	static String weekft;
	Date dNow = new Date();
	static int iDayRandom;
	static String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
	static By byDayPosition;
	static WebElement account;
	static String street;
	static String pincode;
	static String state;
	static String accounttext;
	Actions builder = new Actions(driver.getWebDriver());
	static String scheduledcallTime;
	static WebElement mouseovercall;
	String callaccount;
	String date;
	static int iDay_Position;
	static String accountdraganddrop;
	static String scheduledcallTimedraganddrop;
	static String finalaccountname;
	static String finalaccountnamedrag;

	public DxL_MySchedule(ScriptHelper scriptHelper) {
		super(scriptHelper);

	}
	
	public void dXLMySchedule() throws Exception{
		ms.clickMySchedule();
		ms.checkviews();
		ms.checkcurrentdate();
		ms.checkweekView();
		ms.currentdayhighlightweek();
		ms.previousandNextweek();
		ms.verifyNewCall();
		ms.createcall();
		ms.verifycallDetails();
		ms.optonScheduledcall();
		ms.monthView();	
		ms.verifyAccounts();	
		ms.schedularsection();
		ms.verifyFullweek();	
		ve.logOut();

	}

	

}


