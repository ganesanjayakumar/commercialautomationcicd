package businesscomponents;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.ObjVeevaSurveys;
import ObjectRepository.objHeaderPage;
import ObjectRepository.objMyAccounts;
//import ObjectRepository.objMyAccounts;
import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_Profiling_Surveys extends VeevaFunctions{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public String sAccountID = null;
	VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	//public String questionbankquetext = null;
	public Veeva_Profiling_Surveys(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	//public static String surveyName;

	//Creating new survey for employee by logging with Ops admin.

	public void newsurvey() throws Exception{
		String surveyName;
		Date exte =new Date();
		String ex=new SimpleDateFormat("mmss").format(exte).toString();
		String strDataSheet = "Surveys";
		String strDataSheet1="Login";
		String SelectTerritory_SalesRep = dataTable.getData(strDataSheet, "SelectTerritory_SalesRep");
		String profile=dataTable.getData(strDataSheet1, "Profile");
		String countrycode=dataTable.getData(strDataSheet, "Country_Code");
		String locale=dataTable.getData(strDataSheet1, "Locale");
		String affairs;
		try{
			clickVeevaTab("Surveys");
			cf.clickifElementPresent(objVeevaAdvanceCoachingReport.newButton, "New Button");
			cf.waitForSeconds(5);
			cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", "One Time");
			report.updateTestLog("Verify able to select Coaching Report", "Able to select Coaching Report", Status.SCREENSHOT);
			cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
			cf.waitForSeconds(10);
			cf.switchToFrame(ObjVeevaEmail.mailFrame);

			if(cf.isElementVisible(By.xpath("//h2[.='New Survey']"), "NSurvey")){
				surveyName="OneTimeSurvey"+locale+ex;
				report.updateTestLog("Verify able to see New Survey Target Page", "Able to see survey Target Page", Status.PASS);
				cf.setData(objVeevaAdvanceCoachingReport.surveyname, "Survey Name", surveyName);
				dataTable.putData("Surveys", "SurveyName", surveyName);
				cf.clickLink(ObjVeevaSurveys.channelclm, "Channel CLM");
				cf.clickLink(ObjVeevaSurveys.channelcrm, "Channel CRM");
				cf.clickLink(By.xpath("//td[@id='value_Survey_vod__c:Channels']//img[@title='Add']"), "Adding Channel");

				//For JP_Commercial in Ops Admin need to select AZ Sharing BU as All for populating the created survey in Sales Rep
				if(locale.equals("JP") && profile.equals("Commercial")){
					cf.selectData(By.id("i-value:picklist:Survey_vod__c:AZ_Sharing_BU_AZ_JP__c"), "AZ Sharing BU", "All");

				}
				cf.selectData(ObjVeevaSurveys.assignmentType, "Assignment Type", "Territory");
				cf.clickLink(ObjVeevaSurveys.allowuserschoosetargets, "Allow Users to choose targets");
				cf.waitForSeconds(2);
				selectterittory(SelectTerritory_SalesRep);
				if(cf.isElementVisible(By.xpath("//input[@title='Insert Selected']"), "Insert")){
					cf.clickLink(By.xpath("//input[@title='Insert Selected']"), "Insert Selected");
					if(profile.equals("Commercial")){
						affairs="No";
					}
					else{
						affairs="Yes";
					}
					if(!locale.equals("JP")){
						cf.selectData(ObjVeevaSurveys.medicalaffairs, "Medical Affairs", affairs);
					}
					if(!locale.equals("JP")){
						cf.setData(ObjVeevaSurveys.countrycode, "Countrycode", countrycode);
					}
					report.updateTestLog("Verify all details entered", "All details entered", Status.SCREENSHOT);
					cf.clickLink(objVeevaAdvanceCoachingReport.mansaveReport, "Save");
					cf.waitForSeconds(6);
					cf.switchToFrame(ObjVeevaEmail.mailFrame);
					if(cf.isElementVisible(By.xpath("//h3[.='Survey Detail']"), "Survey Detail Page")){
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
					}
					else{
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
				else{
					report.updateTestLog("Verify able to see New Survey Page", "Unable to see survey  Page", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else{
				report.updateTestLog("Verify able to see Insert Selected Button", "Unable to see Insert Selected Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	//Adding segment while creating survey by Ops admin
	public void editsegment() throws Exception{
		boolean details=false;
		try{
			cf.clickButton(ObjVeevaSurveys.editsegment, "Edit Segment");
			cf.waitForSeconds(2);
			cf.clickButton(ObjVeevaSurveys.addsegment, "Add Segment");
			cf.waitForSeconds(2);
			cf.setData(ObjVeevaSurveys.segmentno, "Segment no", "1");
			cf.setData(ObjVeevaSurveys.minscore, "Minimum Score", "0");
			cf.setData(ObjVeevaSurveys.maxscore, "Maximum Score", "1");
			cf.clickButton(ObjVeevaSurveys.savesegment, "Save Segment");
			cf.waitForSeconds(3);
			if(cf.getElementValue(ObjVeevaSurveys.staticsegmentno, "Segment no").equals("1")){
				if(cf.getElementValue(ObjVeevaSurveys.staticminscore, "Minimum score").equals("0")){
					if(cf.getElementValue(ObjVeevaSurveys.staticmaxscore, "Maximum Score").equals("1"))
						details=true;
				}
			}

			if(details==true)
				report.updateTestLog("Verify Segment no. ,minimum score and maximum score details are updated correctly", "Segment no. ,minimum score and maximum score details are updated correctly", Status.PASS);
			else{
				report.updateTestLog("Verify Segment no. ,minimum score and maximum score details are updated correctly", "Segment no. ,minimum score and maximum score details are not updated correctly", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	//Selecting an Account in New Survey Target while creating survey by ops admin
	public void newsurveytarget() throws Exception{
		String strDataSheet = "Surveys";
		String account = dataTable.getData(strDataSheet, "Account");
		String view = dataTable.getData(strDataSheet, "View");

		try{
			cf.clickButton(ObjVeevaSurveys.newsurveytarget, "New Survey Target");
			cf.selectData(By.xpath("//div[@id='T_addSurveyTargetDialog']/div/select"), "View", view);
			cf.waitForSeconds(20);
			cf.setData(ObjVeevaSurveys.searchaccount, "Search", account);
			cf.clickButton(ObjVeevaSurveys.searchaccount, "Search");
			//if(strLocale.trim().equalsIgnoreCase("EU1")|| strLocale.trim().equalsIgnoreCase("EU2")){
			WebDriverWait dr=new WebDriverWait(driver.getWebDriver(),30);
			//dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Results truncated. Please refine view.']")));
			dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='T_accountsTable']")));
			//}

			cf.clickLink(By.xpath("//table[@id='T_accountsTable']//tr/td[1]/input"), "Selecting account");
			report.updateTestLog("Verify able to select account in the New Survey Target.", "Able to select account in the new Survey Target", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaSurveys.addselectedaccount, "Add Selected");
			cf.clickLink(ObjVeevaSurveys.closesurveytarget, "Close");
			if(cf.isElementVisible(By.xpath("//table[@id='T_targetsTable']//td[.='"+account+"']"), "Account Selected")){
				report.updateTestLog("Verify able to see added account", "Able to see added account", Status.PASS);
			}
			else{
				report.updateTestLog("Verify able to see added account", "Unable to see added account", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	//Validating Warning after clicking on publish button by Ops admin
	public void publishandwarning() throws Exception{
		String warning="Publishing will lock the survey and reassign owners based on assignment rules. You will receive an email when processing is complete.";
		try{
			cf.switchToFrame("vod_iframe");
			cf.clickLink(objVeevaAdvanceCoachingReport.opspublish, "Publish");
			String warningmessage=cf.getElementValue(By.xpath("//div[@id='publishDialog']/div[1]"), "Warning Message");
			System.out.println(warningmessage);
			if((warningmessage.trim()).equals(warning.trim())){
				report.updateTestLog("Verify able to see warning message", "Able to see warning message", Status.PASS);
				cf.clickLink(By.id("publishDialog_ok"), "OK");
			}
			else{
				report.updateTestLog("Verify able to see warning message", "Unable to see warning message", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	//validating publish status.
	public  void publishstatus() throws Exception{
		try{

			driver.navigate().refresh();
			cf.waitForSeconds(15);
			driver.navigate().refresh();
			cf.waitForSeconds(15);
			driver.navigate().refresh();
			cf.switchToFrame(ObjVeevaEmail.mailFrame);
			String publishstatus=cf.getElementValue(By.id("value_Survey_vod__c:Status"), "One time survey Status");
			if((publishstatus.trim()).equals("Published")){
				report.updateTestLog("Verify able to see Survey status as Published", "Able to see survey status as Published", Status.PASS);
				cf.switchToParentFrame();
			}else
			{
				report.updateTestLog("Verify able to see Survey status as Published", "Unable to see survey status as Published", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public void selectSurveyTarget() throws Exception{
		String surveyName=dataTable.getData("Surveys", "SurveyName");
		cf.switchToFrame(ObjVeevaSurveys.frameAddSurveyTarget);		
		List<WebElement> listSurveyTargetNames = driver.findElements(ObjVeevaSurveys.txtSurveyNames);
		List<WebElement> listSurveyRadio = driver.findElements(ObjVeevaSurveys.radioSurveyOption);

		for (int i = 0 ; i < listSurveyTargetNames.size(); i++){
			if(listSurveyTargetNames.get(i).getText().equalsIgnoreCase(surveyName)){
				listSurveyRadio.get(i).click();
				cf.clickButton(ObjVeevaSurveys.btnAddSurveyTarget, "Add Survey Target");
				report.updateTestLog("Click on Survey targets","Survey target : '" + surveyName + "' is clicked", Status.DONE);
				break;
			}
			else if(i==listSurveyTargetNames.size()-1)
				report.updateTestLog("Click on Survey Targets", "Survey target : '" + surveyName + "' is not listed", Status.FAIL);
		}
		//cf.switchToParentFrame();
	}
	//Login as Representative and click on newly created survey by ops admin and select the answers
	public void surveyTargetsrep() throws Exception{
		String surveyName=dataTable.getData("Surveys", "SurveyName");
		try{
			clickVeevaTab("Survey Targets");
			cf.selectData(ObjVeevaSurveys.surveytargetsdropdown, "Survey Target", "My Survey");
			cf.clickButton(ObjVeevaEmail.lookupReasonGo, "Go");
			//String des=cf.getElementValue(By.xpath("//div[@title='Created Date']/parent::td"), "class");
			//System.out.println(des);
			cf.clickButton(By.xpath("//div[@class='listButtons']//input[@class='btn refreshListButton']"), "Refresh Button");
			cf.waitForSeconds(8);
			if(cf.isElementVisible(By.xpath("//div[@title='Created Date']/parent::td[@class='x-grid3-hd x-grid3-cell x-grid3-td-CREATED_DATE ASC']"), "Sorting order is in Ascending Order")){
				cf.clickLink(By.xpath("//div[@title='Created Date']/img"), "Sorting icon");
			}
			if(cf.isElementVisible(By.xpath("//div[@class='listBody']//span[.='"+surveyName+"']"), "Created Survey")){
				report.updateTestLog("Verify able to see created survey "+surveyName+" in Survey Targets", "Able to see created survey "+surveyName+" in Survey Targets", Status.PASS);
				cf.clickLink(By.xpath("//div[@class='listBody']//span[.='"+surveyName+"']"), "Created OneTime Survey");
				cf.waitForSeconds(15);
				cf.switchToFrame(ObjVeevaEmail.mailFrame);
				validatesurveydetailsandselectanswers();
			}
			else{
				report.updateTestLog("Verify able to see created survey "+surveyName+" in Survey Targets", "Unable to see created survey "+surveyName+" in Survey Targets", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	//public String answerpicklist;
	//Select the answers for the questions created by ops admin and submit the answers and validate segment
	public void validatesurveydetailsandselectanswers() throws Exception{
		String surveyName=dataTable.getData("Surveys", "SurveyName");
		String answerpicklist;
		try{
			cf.waitForSeconds(15);
			//cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if(cf.isElementVisible(By.xpath("//h2[.='"+surveyName+"']"), "Created Survey")){

				report.updateTestLog("Verify able to see navigate to survey "+surveyName+" in Survey Targets", "Able to navigate to survey "+surveyName+" in Survey Targets", Status.PASS);
				//Select se=new Select(driver.findElement(By.xpath("//div[@id='Q_questionslist']//select")));
				//answerpicklist=driver.findElement(By.xpath("//div[@id='Q_questionslist']//select/option[2]")).getText();
				driver.findElement(By.xpath("//select[@id='answer']/option[2]")).click();
				answerpicklist =driver.findElement(By.xpath("//select[@id='answer']/option[2]")).getText();
				dataTable.putData("Surveys", "AnswerPickList", answerpicklist);
				//cf.selectData(By.xpath("//select[@id='answer']/option[2]"), "Answer for Question Bank Question", answerpicklist);
				//cf.clickLink(By.xpath("//div[@class='qresponsediv'][2]//label[.='Yes']/preceding-sibling::input"), "Answer for New Survey Question");
				cf.clickLink(By.xpath("//div[@class='qresponselistdiv']/form/div[2]//veev-edit-field//label[contains(text(),'Yes')]/preceding-sibling::input"), "Answer for New Survey Question");
				report.updateTestLog("Verify responses are selected for Questions", "Responses for Questions are selected", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaSurveys.submitanssurveytarget, "Submit");
				//Add Code for validation
				cf.waitForSeconds(20);
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]")));
				String strSubmissionStatus = cf.getData(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]"), "Submission status", "text");
				if(strSubmissionStatus.trim().equalsIgnoreCase("Submitted"))
					report.updateTestLog("Verify Survey Target submission status", "Survey target " + surveyName+" is submitted as expected", Status.PASS);
				else
					report.updateTestLog("Verify Survey Target submission status ", "Survey target " +surveyName+" status is not in submitted status", Status.FAIL);

				//Segment need to be validated-Step 41
			}
			else{
				report.updateTestLog("Verify able to see navigate to survey "+surveyName+" in Survey Targets", "Unable to navigate to survey "+surveyName+" in Survey Targets", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
			cf.switchToParentFrame();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	//Login as Ops admin and click the submitted survey and verify the answers given by representative.
	public void opssurveytarget() throws Exception{
		try{

			validatesurveydetailsandverifyanswers();

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	//Login as Ops admin and verify the answers given by representative in the submitted survey.
	public void validatesurveydetailsandverifyanswers() throws Exception{
		//Score and Segment will be in visible mode as sales rep doesnot have that part it wont be visible here.-Step 46
		String answerpicklist=dataTable.getData("Surveys", "AnswerPickList");
		try{
			//cf.switchToFrame(ObjVeevaEmail.mailFrame);
			Select se=new Select(driver.findElement(By.id("answer")));
			String selectedoption=se.getFirstSelectedOption().getText().trim();
			WebElement radio=driver.findElement(By.xpath("//div[@class='qresponselistdiv']/form/div[2]//veev-edit-field//label[contains(text(),'Yes')]/preceding-sibling::input"));
			if((selectedoption.trim()).equals(answerpicklist.trim()) && (radio.isSelected()))
				report.updateTestLog("Verify able to see answers correctly provided by the Representative", "Able to see answers correctly provided by the Representative",Status.PASS);
			else
				report.updateTestLog("Verify able to see answers correctly provided by the Representative", "Unable to see answers correctly provided by the Representative",Status.FAIL);
			cf.switchToParentFrame();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}


	public void searchAndGo() throws Exception{
		/**
        String strSearchType = dataTable.getData("Search", "Search Type");
        String strSearchText = dataTable.getData("Search", "Search Text");
		 */
		String surveyName=dataTable.getData("Surveys", "SurveyName");
		cf.clickElement(By.xpath("//a[text()='Home']"), "Go to home page to search");
		for (int count = 1; count<=9; count++){
			WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 60);
			if(cf.isElementPresent(objHeaderPage.selectSearch)){

				cf.selectData(objHeaderPage.selectSearch, "Search Type", "Survey Targets");
				cf.setData(objHeaderPage.textSearch, "Search Text", surveyName);

				cf.clickButton(objHeaderPage.btnGo, "Go button in Search");}
			else{				
				cf.clickButton(By.xpath("//a[.='Advanced Search...']"), "Advanced Search");
				wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".searchBoxInput>input[value='Search']")));
				cf.setData(By.cssSelector(".searchBoxInput>input"),"Search in Advanced Search",surveyName);
				cf.clickButton(By.xpath("//label[.='Survey Targets']/preceding-sibling::input[@type='checkbox']"), "Choose Survey Target");
				cf.clickButton(By.cssSelector(".searchBoxInput>input[value='Search']"), "Search Button");
				cf.waitForSeconds(10);
			}
			cf.waitForSeconds(5);
			if(cf.isElementPresent(By.xpath("//h2[contains(text(),'Survey Target Detail')]"))){
				cf.switchToParentFrame();
				break;
			}
			else
				cf.pageRefresh();

		}

	}

	public void newProfilingsurvey() throws Exception{
		String surveyName;
		Date exte =new Date();
		String ex=new SimpleDateFormat("mmss").format(exte).toString();
		String strDataSheet = "Surveys";
		String strDataSheet1="Login";
		String SelectTerritory_SalesRep = dataTable.getData(strDataSheet, "SelectTerritory_SalesRep");
		String profile=dataTable.getData(strDataSheet1, "Profile");
		String countrycode=dataTable.getData(strDataSheet, "Country_Code");
		String locale=dataTable.getData(strDataSheet1, "Locale");
		String sSurveyType = dataTable.getData(strDataSheet, "SurveyType");
		String sProfilingType = dataTable.getData(strDataSheet, "ProfilingType");
		String sProductName = dataTable.getData(strDataSheet, "Product_Name");
		String affairs;
		try{
			clickVeevaTab("Surveys");
			cf.clickifElementPresent(objVeevaAdvanceCoachingReport.newButton, "New Button");
			cf.waitForSeconds(5);
			cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", sSurveyType);
			report.updateTestLog("Verify able to select Coaching Report", "Able to select Coaching Report", Status.SCREENSHOT);
			cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
			cf.waitForSeconds(20);
			cf.switchToFrame(ObjVeevaEmail.mailFrame);

			if(cf.isElementVisible(By.xpath("//h2[.='New Survey']"), "NSurvey")){
				surveyName="OneTimeSurvey"+locale+ex;
				report.updateTestLog("Verify able to see New Survey Target Page", "Able to see survey Target Page", Status.PASS);
				cf.setData(objVeevaAdvanceCoachingReport.surveyname, "Survey Name", surveyName);
				dataTable.putData("Surveys", "SurveyName", surveyName);
				cf.clickLink(ObjVeevaSurveys.channelclm, "Channel CLM");
				cf.clickLink(ObjVeevaSurveys.channelcrm, "Channel CRM");
				cf.clickLink(By.xpath("//td[@id='value_Survey_vod__c:Channels']//img[@title='Add']"), "Adding Channel");

				//For JP_Commercial in Ops Admin need to select AZ Sharing BU as All for populating the created survey in Sales Rep
				if(locale.equals("JP") && profile.equals("Commercial")){
					cf.selectData(By.id("i-value:picklist:Survey_vod__c:AZ_Sharing_BU_AZ_JP__c"), "AZ Sharing BU", "All");

				}

				String productmetric;
				String sFirstLovName = dataTable.getData("Surveys", "Lov One");
				String sSecondLovName = dataTable.getData("Surveys", "Lov Two");
				/**if(locale.equalsIgnoreCase("RU") || locale.equalsIgnoreCase("EU1") || locale.equalsIgnoreCase("ANZ") || locale.equalsIgnoreCase("IE") || locale.equalsIgnoreCase("BR")){
					String[] sFirstName = sFirstLovName.split("AT ");
					productmetric = sFirstName[1].trim();

				}
				else{
					String[] sSecondName = sSecondLovName.split("AT ");
					productmetric = sSecondName[1].trim();
				}**/


				if(cf.isElementVisible(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Select the product metric")){
					cf.clickElement(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Input the product metric");
					cf.setData(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Product Metric", sFirstLovName.split("AT ")[1]);

				}
				cf.clickElement(By.xpath("//span/input[contains(@id,'Profiling_Survey_AZ') and @type='checkbox']"),"Check the profiling Survey");
				cf.selectData(By.xpath("//select[contains(@id,'Profiling_Type_AZ')]"), "Profiling Type", sProfilingType);
				cf.selectData(By.xpath("//select[contains(@id,'Choose_the_Operation_AZ')]"), "Choose the Operation", "SUM");
				cf.selectData(ObjVeevaSurveys.assignmentType, "Assignment Type", "Product and Territory");
				cf.clickElement(By.xpath("(//span/input[contains(@id,'Product_vod')])[1]"),"Select Product");
				cf.setData(By.xpath("(//span/input[contains(@id,'Product_vod')])[1]"), "Product", sProductName); 

				if(!(locale.equals("IE") && profile.equals("Medical"))){
					cf.clickElement(By.xpath("(//span/input[contains(@id,'Detail_Group_vod__c')])[1]"), "Detail Group");
					cf.setData(By.xpath("(//span/input[contains(@id,'Detail_Group_vod__c')])[1]"), "Detail Group", sProductName);
				}

				if(locale.equals("RU")){
					cf.setData(By.xpath("//input[@id='i-value:string:Survey_vod__c:Share_Team_vod__c']"), "Share Team", "[RU_PHARMACY_KAS],[RU_PTA]");
				}

				cf.clickLink(ObjVeevaSurveys.allowuserschoosetargets, "Allow Users to choose targets");
				cf.waitForSeconds(2);
				selectterittory(SelectTerritory_SalesRep);
				if(cf.isElementVisible(By.xpath("//input[@title='Insert Selected']"), "Insert")){
					cf.clickLink(By.xpath("//input[@title='Insert Selected']"), "Insert Selected");
					if(profile.equals("Commercial")){
						affairs="No";
					}
					else{
						affairs="Yes";
					}
					if(!locale.equals("JP")){
						cf.selectData(ObjVeevaSurveys.medicalaffairs, "Medical Affairs", affairs);
					}
					else if(!(locale.equals("IE") && profile.equals("Medical"))){
						cf.setData(ObjVeevaSurveys.countrycode, "Countrycode","ID");
					}
					if(!locale.equals("JP")){
						cf.setData(ObjVeevaSurveys.countrycode, "Countrycode", countrycode);
					}
					report.updateTestLog("Verify all details entered", "All details entered", Status.SCREENSHOT);
					cf.clickLink(objVeevaAdvanceCoachingReport.mansaveReport, "Save");
					//cf.waitForSeconds(6);
					
					cf.switchToFrame("vod_iframe");
					if(cf.isElementVisible(By.xpath("//h3[text()='Survey Detail']"), "Survey Detail Page")){
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
					}
					else{
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
				else{
					report.updateTestLog("Verify able to see New Survey Page", "Unable to see survey  Page", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else{
				report.updateTestLog("Verify able to see Insert Selected Button", "Unable to see Insert Selected Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public void navigateToGlobalLOV() throws Exception{
		System.out.println("Inside fglobal");
		vf.clickVeevaTab("Global LOVs");
		cf.clickElement(By.xpath("//span/input[@class='btn' and @name='go']"), "Click GO");
		cf.waitForSeconds(10);
	}

	public void selectLOV() throws Exception{
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		String strDataSheet = "Surveys";
		String sFirstLovName = dataTable.getData(strDataSheet, "Lov One");
		String sSecondLovName = dataTable.getData(strDataSheet, "Lov Two");

		/*if(strLocale.equalsIgnoreCase("RU") || strLocale.equalsIgnoreCase("EU1") || strLocale.equalsIgnoreCase("ANZ") || strLocale.equalsIgnoreCase("IE") || strLocale.equalsIgnoreCase("BR") || strLocale.equalsIgnoreCase("CA")){
		String[] sFirstName = sFirstLovName.split("AT ");
		sFirstLovName = sFirstName[1].trim();

		String[] sSecondName = sSecondLovName.split("AT ");
		sSecondLovName = sSecondName[1].trim();
		}

		if(cf.isElementPresent(By.xpath("//div[contains(@class,'grid')]/a/span[text()='"+sFirstLovName+"']"))){
			cf.clickElement(By.xpath("//td/div[contains(@class,'grid')]/a/span[text()='"+sFirstLovName+"']/../../../../td/div/input"), "Click the check box");
		}

		if(cf.isElementPresent(By.xpath("//div[contains(@class,'grid')]/a/span[text()='"+sSecondLovName+"']"))){
			cf.clickElement(By.xpath("//td/div[contains(@class,'grid')]/a/span[text()='"+sSecondLovName+"']/../../../../td/div/input"), "Click the check box");
		}*/

		vf.clickVeevaTab("Profiling Designer");
		cf.waitForSeconds(10);
	}


	public void createProfilingQues() throws Exception{
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		String sFirstLovName = dataTable.getData("Surveys", "Lov One");
		String sSecondLovName = dataTable.getData("Surveys", "Lov Two");
		String sQues = dataTable.getData("Surveys", "Question_Text");
		String[] sFirstName = sFirstLovName.split("AT ");
		String prodmetric = sFirstName[1].trim();


		if(cf.isElementPresent(By.xpath("//td/h2[text()='Creating Profiling Questions']"))){

			cf.clickElement(By.xpath("(//td/span[@class='lookupInput']/input[contains(@id,'mulques')])[1]"), "Input to look up icon");
			cf.setData(By.xpath("(//td/span[@class='lookupInput']/input[contains(@id,'mulques')])[1]"), "Product Metric Look Up", prodmetric);
			// Enter question text
			cf.clickElement(By.xpath("(//td/input[@type='text'])[1]"), "Input to the question field");
			cf.setData(By.xpath("(//td/input[@type='text'])[1]"), "Input to the question field", sQues);
			WebElement sSelect = driver.findElement(By.xpath("//select[contains(@name,'mulques')]"));
			Select dropDown = new Select(sSelect);
			dropDown.selectByValue("Number_vod");

			cf.clickElement(By.xpath("//td/input[@value='Create All Added Question']"), "Click create added questions");	
			cf.waitForSeconds(10);

			if(cf.isElementVisible(By.xpath("//td/span[text()='"+sQues+"']"), "Saved Questions")){
				report.updateTestLog("Verify able to see Created Question:", "Able to see the created Question "+sQues+ " in the Table", Status.PASS);
			}else{
				report.updateTestLog("Verify able to see Created Question:", "Unable to see the created Question "+sQues+ " in the Table", Status.FAIL);
			}


		}

	}

	public void surveysquestionCrearionQuestionBank() throws Exception
	{
		String questionbankquetext;
		String secondquestionbankquetext;
		String sQues = dataTable.getData("Surveys", "Question_Text");
		cf.switchToFrame("vod_iframe");
		cf.clickLink(objVeevaAdvanceCoachingReport.opsquestionbankbutton, "Question Bank button");
		cf.waitForSeconds(10);
		if (cf.isElementVisible(By.xpath("//span[.='Question Bank']"), "Question Bank"))
		{
			cf.selectData(By.xpath("//div[@id='Q_bankDialog']/div/select"), "Type Picklist", "Number");
			int noofquestions = driver.findElements(By.xpath("//table[@id='Q_questionsTable']//tr/td[1]/div[1]/input")).size();
			try
			{
				for (int i = 1; i <= noofquestions; i++)
				{
					String type = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[3]")).getText().trim();
					if (type.equals("Number"))
					{
						//
						//cf.clickElement(By.xpath("(//span[@class='tpagemiddle']/input)[1]"), "Click Search button");
						//cf.setData(By.xpath("(//span[@class='tpagemiddle']/input)[1]"),"Search", sQues);
						cf.clickLink(By.xpath("//table[@id='Q_questionsTable']/tbody/tr["+i+"]/td[1]//input[@type='checkbox']"), "Question Selected");
						//cf.clickElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i+1 + "]/td[1]//input[@type='checkbox']"), "Question Selected");
						cf.clickElement(By.xpath("//table[@id='Q_questionsTable']//tr[2]/td[2]/preceding-sibling::td/div[1]/input"), "Question Selected");

						//	cf.clickLink(By.xpath("//table[@id='Q_questionsTable']//tr[" + i+1 + "]/td[1]//input[@type='checkbox']"), "Question Selected");
						questionbankquetext = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[2]")).getText();
						dataTable.putData("Login", "questionbankquetext", questionbankquetext);
						secondquestionbankquetext = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[2]/td[2]")).getText();
						dataTable.putData("Login", "secondquestionbankquetext", secondquestionbankquetext);
						WebElement ele = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[1]//input[@type='checkbox']"));
						if (ele.isSelected())
						{
							report.updateTestLog("Verify selected question from question bank", "Able tnoo select question from question from question bank", Status.PASS);
							cf.clickLink(By.id("Q_addSelected"), "Add Selected");
							cf.clickLink(By.xpath("//span[.='Question Bank']/parent::div//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']"), "Close");
							cf.waitForSeconds(2);
							if (cf.isElementVisible(By.xpath("//div[@id='Q_questionslist']//span[.='" + questionbankquetext + "']"), "Question bank Question"))
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Able to see added Question Bank Question", Status.PASS);
								break;
							}
							else
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Unable to see added Question Bank Question", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify selected question from question bank", "Unable to select question from question from question bank", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			catch (Exception e)
			{
				report.updateTestLog("Verify selected question from question bank", "No Questions were found", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void surveydetailsanswersAndvalidation() throws Exception{
		String surveyName=dataTable.getData("Surveys", "SurveyName");
		String questionbankquetext = dataTable.getData("Login", "questionbankquetext");
		String secondquestionbankquetext = dataTable.getData("Login", "secondquestionbankquetext");
		String sAnswers = dataTable.getData("Surveys", "AnswerPickList");
		String sSecondAnswers = dataTable.getData("Surveys", "AnswerPickList_2");
		String answerpicklist;
		try{
			
			//cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if(cf.isElementVisible(By.xpath("//h2[.='"+surveyName+"']"), "Created Survey")){

				report.updateTestLog("Verify able to see navigate to survey "+surveyName+" in Survey Targets", "Able to navigate to survey "+surveyName+" in Survey Targets", Status.PASS);
				//driver.findElement(By.xpath("(//div[text()='"+questionbankquetext+"']/following-sibling::div//span/input)[1]")).click();
				cf.setData(By.xpath("//div[@ng-class='getResponseOuterDivClass(qResponse)'][1]//veev-edit-field//input"), "Question Response", sAnswers);
				//driver.findElement(By.xpath("//div[text()='"+secondquestionbankquetext+"']/following-sibling::div//span/input")).click();
				cf.setData(By.xpath("//div[@ng-class='getResponseOuterDivClass(qResponse)'][2]//veev-edit-field//input"), "Question Response", sSecondAnswers);
				report.updateTestLog("Verify responses are selected for Questions", "Responses for Questions are selected", Status.SCREENSHOT);

				cf.clickButton(ObjVeevaSurveys.submitanssurveytarget, "Submit");
				//Add Code for validation
				cf.waitForSeconds(20);
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]")));
				String strSubmissionStatus = cf.getData(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]"), "Submission status", "text");
				if(strSubmissionStatus.trim().equalsIgnoreCase("Submitted"))
					report.updateTestLog("Verify Survey Target submission status", "Survey target " + surveyName+" is submitted as expected", Status.PASS);
				else
					report.updateTestLog("Verify Survey Target submission status ", "Survey target " +surveyName+" status is not in submitted status", Status.FAIL);

				//Segment need to be validated-Step 41
			}
			else{
				report.updateTestLog("Verify able to see navigate to survey "+surveyName+" in Survey Targets", "Unable to navigate to survey "+surveyName+" in Survey Targets", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
			cf.switchToParentFrame();

			if(cf.isElementVisible(By.xpath("//div/span//span//span/a[@name='Account_vod__c']"), "Account Name displaying")){
				cf.clickElement(By.xpath("//div/span//span//span/a[@name='Account_vod__c']"), "Click Account Name");
				cf.waitForSeconds(10);
			}

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public void surveyschooseAccountFromMyAccounts() throws Exception{
		        

		System.out.println("Inside Method");
		String strDataSheet1="Login";
		String locale = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		String sAccountID = null;
		if(locale.equals("US")){
			//if(view.equals("My Business Accounts"))
			cf.waitForSeconds(5);
			cf.switchToFrame(objMyAccounts.sframe);
		}
		else{
			cf.switchToFrame(objMyAccounts.sframe);
		}
		if(locale.equals("JP")&& profile.equals("Medical")){
			cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
		}
		if(locale.equals("JP") && profile.equals("Commercial")){
			//driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[3]")).getText();
			cf.clickButton(By.xpath("//table[@id='vodResultSet']//tr[2]//td[3]/a"), "Click any Account");
			cf.waitForSeconds(8);
		}
		else{
			cf.clickButton(objMyAccounts.aAccountLink, "Click any Account");
			cf.waitForSeconds(2);
			
		}
		//cf.waitForSeconds(3);
		//cf.switchToParentFrame();
	}

public void retrivingAccNumber() throws Exception{
		cf.waitForSeconds(5);
		//cf.switchToParentFrame();
		//String dName=driver.getTitle();
		String sAccountNumber = driver.getCurrentUrl();
		System.out.println(sAccountNumber);
		String[] sACC = sAccountNumber.split("/");
		sAccountID = sACC[3];
		System.out.println(sAccountID);
		dataTable.putData("Login", "AccountID", sAccountID);
	}

	public void workBenchValidation() throws Exception {
		
		String strLocale=dataTable.getData("Login","Locale");
		String sFirstLovName = dataTable.getData("Surveys", "Lov One");
		String sSecondLovName = dataTable.getData("Surveys", "Lov Two");
		String sSumOfAnswers = dataTable.getData("Surveys", "SumOfAnswers");
		String sProductID = dataTable.getData("Login", "ProductID");
		String sAccountIDUpdated = dataTable.getData("Login", "AccountID");
		String sResults = null,productmetric;
		String sProfileType = dataTable.getData("Surveys", "ProfilingType");

		if(!sFirstLovName.contains("#")){
			String[] sFirstName = sFirstLovName.split("AT ");
			productmetric = sFirstName[1].trim();
		}
		else{
			String[] sSecondName = sSecondLovName.split("AT ");
			productmetric = sSecondName[1].trim();
		}

		cf.openUrlInNewTab("https://workbench.developerforce.com/login.php?startUrl=%2Fquery.php");
		cf.waitForSeconds(10);
		cf.isElementVisible(By.xpath("//div/input[@value='Login with Salesforce']"), "WorkBench Page is displayed");
		cf.selectData(By.xpath("//select[@id='oauth_env']"), "Environment", "Sandbox");
		cf.selectData(By.xpath("//select[@id='oauth_apiVersion']"), "API Version", "47.0");
		cf.clickElement(By.xpath("//input[@id='termsAccepted']"), "Click Agree and Login");
		cf.clickElement(By.xpath("//input[@id='loginBtn' and @value='Login with Salesforce']"), "Click Login");
		cf.waitForSeconds(10);


		//String strCommonDataHeader="#VeevaLogin_"+(strLocale.trim().toUpperCase())+"_SysAdmin";//VeevaLogin_IE_SysAdmin
		String strSysAdminUserName=dataTable.getData("Login","SF_UserName");
		String strSysAdminPassword=dataTable.getData("Login","SF_Password");
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strSysAdminUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strSysAdminPassword);
		cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
		report.updateTestLog("login To Veeva App","Login Page", Status.SCREENSHOT);
		if(cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel",5)){
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		if(cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}			

		cf.waitForSeconds(10);

		if(cf.isElementVisible(By.xpath("//tr/td[text()='SOQL Query']"), "SQL WorkBench Home Page")){
			cf.selectData(By.xpath("//select[@id='QB_object_sel']"), "Object", "Product_Metrics_vod__c");
			cf.waitForSeconds(7);
			WebElement Field = driver.findElement(By.xpath("//p/select[@id='QB_field_sel']"));
			Select sField = new Select(Field);
			sField.selectByValue(productmetric);

			//cf.selectData(By.xpath("//p/select[@id='QB_field_sel']"), "Fields", sFirstLovName);

			cf.waitForSeconds(7);
			cf.selectData(By.xpath("//select[@id='QB_filter_field_0']"), "Filter results by", "Account_vod__c");
			cf.waitForSeconds(7);
			cf.clickElement(By.xpath("//input[@type='text' and @id='QB_filter_value_0']"), "Account Value");
			cf.setData(By.xpath("//input[@type='text' and @id='QB_filter_value_0']"), "Filter results by", sAccountIDUpdated);

			cf.clickElement(By.xpath("//td[@id='filter_plus_cell_0']"), "Click + symbol to add query");
			cf.waitForSeconds(6);
			cf.selectData(By.xpath("//select[@id='QB_filter_field_1']"), "Filter results by", "Products_vod__c");
			cf.waitForSeconds(6);
			cf.clickElement(By.xpath("//input[@type='text' and @id='QB_filter_value_1']"), "Account Value");
			cf.setData(By.xpath("//input[@type='text' and @id='QB_filter_value_1']"), "Filter results by", sProductID);
			cf.waitForSeconds(6);
			cf.clickElement(By.xpath("//input[@type='submit' and @name='querySubmit']"), "Click Query button to execute");
			cf.waitForSeconds(15);

			if(cf.isElementVisible(By.xpath("//h2[text()='Query Results']"), "SQL Query Results Window")){
				cf.isElementPresent(By.xpath("//th[text()='"+sFirstLovName+"']"), "Field Name");
				sResults = driver.findElement(By.xpath("//table[@id='query_results']/tbody/tr[2]/td[2]")).getText();
				if(sResults.equalsIgnoreCase(sSumOfAnswers)){
					System.out.println(sResults);
					System.out.println(sSumOfAnswers);
					report.updateTestLog("The "+sProfileType+" Survey Score "+sSumOfAnswers+" is equals with: ", " "+sResults+" Score ", Status.PASS);
				}
				else{
					report.updateTestLog("The "+sProfileType+" Survey Score "+sSumOfAnswers+" is not equals with: ", " "+sResults+" score ", Status.FAIL);
				}

			}
			else
			{
				report.updateTestLog("The "+sProfileType+" Survey Score Validation is not done: ", " "+sResults+" is displayde but not validated ", Status.FAIL);
			}
		}



	}
}
