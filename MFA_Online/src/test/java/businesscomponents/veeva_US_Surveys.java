package businesscomponents;

import java.util.List;
import supportlibraries.*;

import com.mfa.framework.Status;

//import com.itextpdf.text.log.SysoCounter;

import ObjectRepository.*;

//import org.openqa.jetty.html.Select;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class veeva_US_Surveys  extends ReusableLibrary{

	//private ConvenienceFunctions cf = new ConvenienceFunctions(driver, report, dataTable);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
//	private ReuseFunctions rf = new ReuseFunctions(scriptHelper);

	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	public veeva_US_Surveys(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Selecting by default the Survey Target of that Account.
	 * @throws Exception
	 */
	public void selectandaddsurveyTargetUS() throws Exception{
		cf.switchToFrame(ObjVeevaSurveys.frameAddSurveyTarget);		
		List<WebElement> listSurveyTargetNames = driver.findElements(ObjVeevaSurveys.txtSurveyNames);
		if((listSurveyTargetNames.size())>0){
			report.updateTestLog("Verify able to see targets for the account.", "Able to see targets for the account.", Status.PASS);	
			WebElement ele=driver.findElement(By.xpath("//table[@id='surveyTable']//tr[1]/td[1]/input"));
			cf.clickElement(ele, "Survey Target is Selected");
			if(ele.isSelected()){
				report.updateTestLog("Verify whether the Survey Target is selected.", "Able to see Survey Target as selected.",Status.PASS);
				cf.clickButton(ObjVeevaSurveys.btnAddSurveyTarget, "Add Survey Target");
				if(cf.isElementVisible(By.xpath("//h2[.='Survey Target Detail']"), "Survey Detail Page"))
					report.updateTestLog("Verify able to navigate to Survey Target Detail Page", "Able to navigate to survey Target detail page", Status.PASS);
				else
					report.updateTestLog("Verify able to navigate to Survey Target Detail Page", "Unable to navigate to survey Target detail page", Status.FAIL);
			}
			else 	
				report.updateTestLog("Verify whether the Survey Target is selected.", "Unable to see Survey Target as selected.",Status.FAIL);
		}
		else
			report.updateTestLog("Verify able to see targets for the account.", "There are no Survey Targets.", Status.FAIL);
		// throw new Exception("No Survey Targets");
	}

	/**
	 * Submit the answers for Survey Target.
	 */
	public void submitanswers() throws Exception{
		int noque=driver.findElements(By.xpath("//div [@ng-repeat='qResponse in questionResponses']")).size();
		for(int i=1;i<=noque;i++){

			if(cf.isElementVisible(By.xpath("//div[@ng-repeat='qResponse in questionResponses']["+i+"]//veev-edit-field//input"), " Question"+i)){
				cf.setData(By.xpath("//div[@ng-repeat='qResponse in questionResponses']["+i+"]//veev-edit-field//input"), "Answer"+i, "1234");
			}
			else if(cf.isElementVisible(By.xpath("//div[@ng-repeat='qResponse in questionResponses']["+i+"]//veev-edit-field//select"), " Question"+i)){
				String value=driver.findElement(By.xpath("//div[@ng-repeat='qResponse in questionResponses']["+i+"]//veev-edit-field//select//option[2]")).getText();
				cf.selectData(By.xpath("//div[@ng-repeat='qResponse in questionResponses']["+i+"]//veev-edit-field//select"), "Selecting Answer from Dropdown", value);
			}

		}
	}

	/**
	 * Validate the status of Survey Target and Submit the Target
	 * Store the details of Survey Target in Excel Sheet.
	 * @throws Exception
	 */
	public void validatestatusandsubmit() throws Exception{

		String status1=cf.getSFData("Status", "text", "text");
		if(status1.trim().equals("Pending")){
			report.updateTestLog("Verify able to see status as Pending", "Able to see status as Pending", Status.PASS);
			submitanswers();
			report.updateTestLog("Verify able to see answers", "Able to see answers", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaSurveys.submitanssurveytarget, "Submit Button");
			WebDriverWait wait=new WebDriverWait(driver.getWebDriver(), 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@title='Update Responses']")));
			String status=cf.getSFData("Status", "text", "text");
			dataTable.putData("Surveys", "Status", status);
			String surveyTarget=cf.getSFData("Survey Target", "text", "text");
			dataTable.putData("Surveys", "Survey Target", surveyTarget);
			String owner=driver.findElement(By.name("OwnerId")).getText();
			dataTable.putData("Surveys", "Owner Name", owner);
			String createdDate=driver.findElement(By.name("CreatedDate")).getText();
			String date[]=createdDate.split(" ");
			dataTable.putData("Surveys", "Created Date", date[0]);
			String startDate=cf.getSFData("Start Date", "text", "text");
			dataTable.putData("Surveys", "Start Date", startDate);
			String endDate=cf.getSFData("End Date", "text", "text");
			dataTable.putData("Surveys", "End Date", endDate);


		}
		else
			report.updateTestLog("Verify able to see status as Pending", "Unable to see status as Pending", Status.FAIL);
	}

	public void surveyrelatedListvalidation() throws Exception{
		int rows;
		cf.clickSFLinkLet("Survey History");
		String sustatus=dataTable.getData("Surveys", "Status");
		String susurveyTarget=dataTable.getData("Surveys", "Survey Target");
		String suOwnert=dataTable.getData("Surveys", "Owner Name");
		//String suCreatedDate=dataTable.getData("Surveys", "Created Date");
		String suStartDate=dataTable.getData("Surveys", "Start Date");
		String suEndDate=dataTable.getData("Surveys", "End Date");
		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Survey History')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List")){
			WebElement element1=driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Survey History')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Go to list");
			cf.waitForSeconds(6);
		}
		if(cf.isElementVisible(By.xpath("//h3[.='Survey History']"),null)){
			rows=cf.getRowCountFromLinkLetTable("Survey History");
			if(rows>0){
				for(int j=rows+1;j>1;j--){
					String appstatus=cf.getDataFromLinkLetTable("Survey History", "Status", j);
					String appsurveyTarget=cf.getDataFromLinkLetTable("Survey History", "Survey Target", j);
					String appOwnert=cf.getDataFromLinkLetTable("Survey History", "Owner Name", j);
					//String appCreatedDate=cf.getDataFromLinkLetTable("Survey History", "Created Date", j);
					String appEndDate=cf.getDataFromLinkLetTable("Survey History", "End Date", j);
					String appStartDate=cf.getDataFromLinkLetTable("Survey History", "Start Date", j);

					if(appstatus.equals(sustatus)&& appsurveyTarget.equals(susurveyTarget)&&appOwnert.equals(suOwnert)){
						report.updateTestLog("Verify able to see Status, Survey Target and Owner Name", "Able to see proper Status, Survey Traget and Owner Name", Status.PASS);
						if(appStartDate.equals(suStartDate)&& appEndDate.equals(suEndDate)){
							report.updateTestLog("Verify able to see Created Date, Start Date and End Date", "Able to see proper Created Date, Start Date and End Date", Status.PASS);
							break;
						}else
							report.updateTestLog("Verify able to see Created Date, Start Date and End Date", "Unable to see Created Date, Start Date and End Date", Status.FAIL);
					}
					else
						report.updateTestLog("Verify able to see Status, Survey Target and Owner Name", "Unable to see Status, Survey Traget and Owner Name", Status.FAIL);

				}
			}
			else{
				report.updateTestLog("Verify able to see targets for the account in related list.", "There are no Survey Targets created in related list.", Status.FAIL);
			}
		}

	}
}



