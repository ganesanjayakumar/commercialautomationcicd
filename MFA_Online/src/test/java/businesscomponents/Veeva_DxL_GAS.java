package businesscomponents;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;

import ObjectRepository.objGlobalAccountSearchPage;
import ObjectRepository.objHeaderPage;
import io.appium.java_client.functions.ExpectedCondition;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_DxL_GAS extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	

	public Veeva_DxL_GAS(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToGASLink() throws Exception{

		cf.clickLink(objHeaderPage.plussymbol,"Plus Symbol");
		cf.clickLink(objHeaderPage.lnkGlobalAccountSearch, "Global Account Search Link");
		if(cf.isElementVisible(objHeaderPage.lblSearchHeader, "Search for Account Header"))
			report.updateTestLog("Verify able to navigate to Search for Account page", "Navigating to Global account search page is successful", Status.PASS);
		else
			report.updateTestLog("Verify able to navigate to Search for Account page", "Unable to navigate to Surveys Home page", Status.FAIL);
	}

	public void enterSearchFieldsInGAS(String strFieldName, String strSearch,String strExactOrContains) throws Exception{
		if(strSearch.trim()!=""){
			if(strSearch.contains(",") && strFieldName.trim().equalsIgnoreCase("Name"))	
				strSearch = strSearch.substring(strSearch.indexOf(",")+1).trim() + " " +strSearch.substring(0, strSearch.indexOf(",")).trim();


			switch(strFieldName.trim().toUpperCase()){
			case "NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radNameExactMatch, "Name Exact match"); break;
				case	"CONTAINS":cf.actionClick(objGlobalAccountSearchPage.radNameContains, "Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radNameExactMatch, "Name Exact match"); break;
				}
				cf.setData(objGlobalAccountSearchPage.editName, "Name", strSearch.trim());
				break;
			case "FIRST NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radFirstNameExactMatch, "First Name Exact match"); break;
				case	"CONTAINS":	cf.actionClick(objGlobalAccountSearchPage.radFirstNameContains, "First Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radFirstNameExactMatch, "First Name Exact match"); break;
				}
				cf.setData(objGlobalAccountSearchPage.editFirstName, "First Name", strSearch.trim());
				break;
			case "LAST NAME":
				switch(strExactOrContains.trim().toUpperCase()){
				case	"EXACT":cf.actionClick(objGlobalAccountSearchPage.radLastNameExactMatch, "Last Name Exact match");	 break;
				case	"CONTAINS":cf.actionClick(objGlobalAccountSearchPage.radLastNameContains, "Last Name Contains");break;
				default:cf.actionClick(objGlobalAccountSearchPage.radLastNameExactMatch, "Last Name Exact match");	 break;
				}
				cf.setData(objGlobalAccountSearchPage.editLastName, "Last Name", strSearch.trim());
				break;
			case "SPECIALTY":
				cf.selectData(objGlobalAccountSearchPage.selectPrimarySpecialty, "Primary Specialty", strSearch);
				break;
			default:
				cf.setData(objGlobalAccountSearchPage.editName, "Name", strSearch.trim());
				break;
			}
		}


	}

	/**
	 * Method to enter 'Name' and validate search results
	 * @throws Exception 
	 */
	public void searchAndValidateNameField() throws Exception{
		cf.pageRefresh();
		String strName = dataTable.getData("GlobalAccountSearch", "Name");

		if(!strName.trim().equals("")){
			String[] arrFirstNameLastName =strName.split(";");		//barma, sajal-exact
			for(int iter = 0 ; iter < arrFirstNameLastName.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrFirstNameLastName[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(30);
				
				if(cf.isElementPresent(By.xpath("//li[contains(.,'More than 50 records found')]")))
				{
					report.updateTestLog("Warning or error message is displaying", "Message displayed successfully", Status.PASS);
				}
					else
				{
						report.updateTestLog("Warning or error message is not displaying", "Message not displaying successfully", Status.FAIL);	
				}
				cf.pageRefresh();
				
				enterSearchFieldsInGAS("Name", "Norbert Bley", "exact");
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				
				
				//verifySearchResults("Name",strSearch.trim(), strExactOrContains);
			}
		}
		else report.updateTestLog("Search and validate Name", "No search term provided", Status.WARNING);
	}

	/**
	 * Method to enter 'First name' and 'Last Name' and validate search results
	 * @throws Exception 
	 */
	public void searchAndValidateFirstNameLastName() throws Exception{

		String strFirstNameLastName = dataTable.getData("GlobalAccountSearch", "FirstNameLastName");		
		if (strFirstNameLastName.trim()!=""){
			String[] arrFirstNameLastName =strFirstNameLastName.split(";");					//barma, sajal-exact; barma, sajal-contains;sajal-contains
			String strFirstName="", strLastName = "";
			for(int iter = 0 ; iter < arrFirstNameLastName.length; iter++){		
				cf.pageRefresh();
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrFirstNameLastName[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				if(strSearch.contains(",")){
					strFirstName = strSearch.substring(strSearch.indexOf(",")+1).trim();//	strSearch.split(",")[1].trim();		//barma
					strLastName = strSearch.substring(0, strSearch.indexOf(",")).trim();//	strSearch.split(",")[0].trim();		//sajal
					enterSearchFieldsInGAS("First Name", strFirstName, strExactOrContains);
					enterSearchFieldsInGAS("Last Name", strLastName, strExactOrContains);
				}else {
					enterSearchFieldsInGAS("First Name", strSearch, strExactOrContains);
				}

				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(10);
				verifySearchResults("Name",strSearch, strExactOrContains);
			}
		}	
		else report.updateTestLog("Search and validate First / Last Name", "No search term provided", Status.WARNING);
	}

	public void searchAndValidateSpeciality() throws Exception{
		String strSpeciality = dataTable.getData("GlobalAccountSearch", "Specialty");
		if(strSpeciality.trim()!=""){
			String[] arrSpecialities =strSpeciality.split(";");		
			for(int iter = 0 ; iter < arrSpecialities.length; iter++){
				String strSearchSpecialty = arrSpecialities[iter];
				cf.pageRefresh();
				cf.selectData(objGlobalAccountSearchPage.selectPrimarySpecialty, "Primary Specialty", strSpeciality);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");	
				cf.waitForSeconds(10);
				cf.scrollToBottom();
				verifySearchResults("Primary Specialty",strSearchSpecialty, "Exact");		
			}

		}
		else report.updateTestLog("Search and validate Speciality", "No search term provided", Status.WARNING);
	}

	public void searchAndValidateInactiveAccount() throws Exception{
		cf.pageRefresh();
		String strInactiveSearch = dataTable.getData("GlobalAccountSearch", "InactiveSearch");
		String strProfile = dataTable.getData("Login", "Profile") ;

		if(!strInactiveSearch.trim().equals("")){
			String[] arrSearch =strInactiveSearch.split(";");		
			for(int iter = 0 ; iter < arrSearch.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrSearch[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				switch (strProfile.trim().toLowerCase()){
				case "medical":
					verifySearchResults("Name", strSearch, strExactOrContains);
					break;
				default:
					WebDriverWait dr=new WebDriverWait(driver.getWebDriver(),60);
					//dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Results truncated. Please refine view.']")));
					dr.until(ExpectedConditions.visibilityOfElementLocated(objGlobalAccountSearchPage.lblAlerts)); 

					cf.scrollToTop();
					if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){

						String lblAlert =cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification");
						if (lblAlert.trim().equals("No matching records found. Please modify your search criteria."))
							report.updateTestLog("Verify Inactive account search", "Inactive account verification is successful", Status.PASS);
						else
							report.updateTestLog("Verify Inactive account search", "Inactive account alert message is not displayed", Status.FAIL);			
					}
					else
						report.updateTestLog("Verify Inactive account search", "No alert message is  displayed", Status.FAIL);	
				}

			}
		}
		else report.updateTestLog("Search and validate Inactive account", "No search term provided", Status.WARNING);
	}

	public void searchAndValidateMoreRecords() throws Exception{
		cf.pageRefresh();
		String strErrorSearch = dataTable.getData("GlobalAccountSearch", "ErrorSearch");
		String strLocale = dataTable.getData("Login", "Locale");

		String strLocaleErrorSearch="";	

		switch(strLocale.trim().toUpperCase()){
		case "CA":
		case "IE":		
		case "ANZ":
		case "BR":
		case "JP":	
			strLocaleErrorSearch="More than 20 records found. Please narrow your search criteria. ( Found ";break;
		case "EU1":	
		case "EU2":
			strLocaleErrorSearch="More than 50 records found. Please narrow your search criteria. ( Found ";break;

		case "CN":
		case "RU":
			strLocaleErrorSearch="More than 100 records found. Please narrow your search criteria. ( Found ";break;

		}
		if(!strErrorSearch.trim().equals("")){
			String[] arrSearch =strErrorSearch.split(";");		//barma, sajal-exact
			for(int iter = 0 ; iter < arrSearch.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrSearch[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);

				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				WebDriverWait dr=new WebDriverWait(driver.getWebDriver(),120);
				//dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Results truncated. Please refine view.']")));

				dr.until(ExpectedConditions.visibilityOfElementLocated(objGlobalAccountSearchPage.lblAlerts)); 

				cf.scrollToTop();
				if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){
					String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification");
					if (lblAlert.trim().contains(strLocaleErrorSearch))
						report.updateTestLog("Verify More than 20 records search", "More records search result verification is successful", Status.PASS);
					else
						report.updateTestLog("Verify More than 20 records search", "Notification message for more records is not displayed", Status.PASS);
				}else{

					cf.pageRefresh();
					enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);

					cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
					dr=new WebDriverWait(driver.getWebDriver(),120);
					//dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Results truncated. Please refine view.']")));

					dr.until(ExpectedConditions.visibilityOfElementLocated(objGlobalAccountSearchPage.lblAlerts)); 

					if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){
						String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification");
						if (lblAlert.trim().contains(strLocaleErrorSearch))
							report.updateTestLog("Verify More records search", "More records search result verification is successful", Status.PASS);
						else
							report.updateTestLog("Verify More records search", "Notification message for more records is not displayed", Status.PASS);
					}else
						report.updateTestLog("Verify Error message", "No alert message is  displayed", Status.FAIL);	

				}

			}
		}
		else report.updateTestLog("Search and validate More records", "No search term provided", Status.WARNING);
	}

	public void searchAndValidateTooManyQueryRows() throws Exception{
		cf.pageRefresh();
		String strTooManyRows = dataTable.getData("GlobalAccountSearch", "TooManyQueryRowsSearch");		
		if(!strTooManyRows.trim().equals("")){
			String[] arrSearch =strTooManyRows.split(";");		//barma, sajal-exact
			for(int iter = 0 ; iter < arrSearch.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrSearch[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(5);
				cf.scrollToTop();

				if(cf.isElementVisible(objGlobalAccountSearchPage.lblPageErrorAlert, "Error Notification")){
					String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblPageErrorAlert,"Error Notification");
					if (lblAlert.trim().contains("Too many query rows: 50001"))
						report.updateTestLog("Verify Too many Query rows", "Too many query rows: 50001 message is displayed as expected", Status.PASS);
					else
						report.updateTestLog("Verify Too many Query rows", "Too many query rows: 50001 message is not displayed", Status.FAIL);			
				}
			}
		}else report.updateTestLog("Search and validate Too many rows", "No search term provided", Status.WARNING);

	}
	public void searchForTerritory() throws Exception{
		cf.pageRefresh();
		String strName = dataTable.getData("GlobalAccountSearch", "TerritoryName");	
		if(!strName.trim().equals("")){
			String[] arrSearch =strName.split(";");		//barma, sajal-exact
			for(int iter = 0 ; iter < arrSearch.length; iter++){					
				String strExactOrContains="";
				String strSearch="";
				String inpSearch=arrSearch[iter];
				if ((inpSearch.contains("-") && inpSearch.toLowerCase().contains("exact")) || (inpSearch.contains("-") && inpSearch.toLowerCase().contains("contains"))){
					strExactOrContains = inpSearch.split("-")[1].trim();
					strSearch = inpSearch.split("-")[0].trim();			
				}
				else {strSearch = inpSearch;strExactOrContains="Exact";}
				enterSearchFieldsInGAS("Name", strSearch, strExactOrContains);
				cf.clickButton(objGlobalAccountSearchPage.lnkSearch, "Search");
				cf.waitForSeconds(5);
			}
		}
		else report.updateTestLog("Search and add Territory", "No search term provided", Status.WARNING);
	}

	public void addAndVerifySuccessfulTerritoryToAdd() throws Exception{
		Boolean blnBreak = false;
		List<WebElement> addToTerritoryChks = driver.findElements(objGlobalAccountSearchPage.chkFirstSearchResult);
		for (int i = 0 ; i < addToTerritoryChks.size(); i++){
			cf.clickButton(By.xpath("//table[@id='restable']//tr["+(i+2)+"]/td[1]/input"), "Account selected");
			cf.clickButton(objGlobalAccountSearchPage.btnAddToTerritory, "Add To Territory");
			cf.waitForSeconds(5);
			cf.scrollToTop();
			if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){
				String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification");
				System.out.println(lblAlert);
				if (lblAlert.trim().contains("The customer is already in your territory. No changes made.")){
					report.updateTestLog("Verify Add to Territory", "User is successfully added to territory", Status.PASS);
					blnBreak = true;
					break;
				}
				else if (i == addToTerritoryChks.size()-1)
					report.updateTestLog("Verify Add to Territory", "Add to territory successful notification message is not displayed", Status.FAIL);
			}
			else report.updateTestLog("Verify Add to Territory", "No alert message is  displayed", Status.FAIL);	
			if (blnBreak)	break;
		}
		

	}

	public void addAndVerifyAlreadyTerritoryAdded() throws Exception{

		Boolean blnBreak = false;
		vf.clickVeevaTab("Global Account Search");//Fix : Sindiya 31/01/2019
		searchForTerritory();
		List<WebElement> addToTerritoryChks = driver.findElements(objGlobalAccountSearchPage.chkFirstSearchResult);
		for (int i = 0 ; i < addToTerritoryChks.size(); i++){ 
			boolean check = driver.findElement(By.xpath("//table[@id='restable']//tr["+(i+2)+"]/td[1]/input")).isSelected();
			if(check == false)//Fix : Sindiya 31/01/2019
				cf.clickButton(By.xpath("//table[@id='restable']//tr["+(i+2)+"]/td[1]/input"), "Account selected");
			
			cf.clickButton(objGlobalAccountSearchPage.btnAddToTerritory, "Add To Territory");
			cf.waitForSeconds(5);
			cf.scrollToTop();
			if(cf.isElementVisible(objGlobalAccountSearchPage.lblAlerts, "Error Notification")){
				String lblAlert = cf.getElementValue(objGlobalAccountSearchPage.lblAlerts,"Error Notification").trim();
				System.out.println(lblAlert);
				String s1 = "The customer is already in your territory. No changes made.";
				String s2 = "The customer is already in your territory" ;
			
				if ((lblAlert.contains(s1)) || (lblAlert.contains(s2)) ){//Fix : Sindiya 31/01/2019
					report.updateTestLog("Verify Add to Territory", "Already added to territory message is displayed as expected", Status.PASS);
					blnBreak = true;
					break;
				}
				else if (i == addToTerritoryChks.size()-1)
					report.updateTestLog("Verify if already added to Territory", "Already added to territory message is not displayed", Status.FAIL);			
			}
			else report.updateTestLog("Verify Add to Territory", "No alert message is  displayed", Status.FAIL);	
			if (blnBreak)	break;
			
		}
	}

	/**
	 * Method to validate the search results displayed in GAS page
	 * @param strSearch
	 * @param strExactOrContains
	 */
	public void verifySearchResults(String strColumnName, String strSearch, String strExactOrContains) throws Exception{		//barma, sajal

		String strFirstName = "", strLastName = "";
		boolean blnFailure = false;
		String strLocale = dataTable.getData("Login", "Locale");

		if  (strSearch.contains(",")){
			strFirstName = strSearch.substring(strSearch.indexOf(",")+1).trim();//	strSearch.split(",")[1].trim();		//barma
			strLastName = strSearch.substring(0, strSearch.indexOf(",")).trim();//	strSearch.split(",")[0].trim();		//sajal
		}
		else if(strLocale.trim().equalsIgnoreCase("RU") || strLocale.trim().equalsIgnoreCase("JP") || strLocale.trim().equalsIgnoreCase("CN")){
			if (strSearch.contains(" ")) {strFirstName = strSearch.substring(0,strSearch.indexOf(" ")).trim();
			strLastName = strSearch.substring(strSearch.indexOf(" ")).trim();
			}
			else strFirstName = strSearch.trim();
		}
		else strFirstName = strSearch.trim();


		ArrayList<String> strNameResults = new ArrayList<String>(1);

		int nameCol = 0;
		
		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 120);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				int elementCount = driver.findElement(objGlobalAccountSearchPage.tblSearchResults).findElements(By.tagName("tr")).size();
				if (elementCount > 1)					return true;
				else					return false;
			}
		});
		WebElement tblSearchRes = driver.findElement(objGlobalAccountSearchPage.tblSearchResults);
		WebElement headerRow = tblSearchRes.findElement(By.tagName("tr"));
		List<WebElement> listName = headerRow.findElements(By.tagName("td"));
		for (int idx=0; idx < listName.size(); idx++){
			WebElement td = listName.get(idx);
			if (td.getText().trim().contains(strColumnName)){
				nameCol = idx;	
				break;
			}
			else if (idx==listName.size())
				throw new FrameworkException(strColumnName + " not found in search results table");
		}

		List<WebElement> rows = tblSearchRes.findElements(By.tagName("tr"));
		if (rows.size() > 1)
			for (int iRow = 1; iRow < rows.size(); iRow ++)	{				
				List<WebElement> cols = rows.get(iRow).findElements(By.tagName("td"));
				strNameResults.add(cols.get(nameCol).getText().toString());
			}
		else{
			strNameResults = null;
			blnFailure = true;
			report.updateTestLog("Verify Global account search results", "Search results are not retrieved", Status.FAIL);
		}

		for (int i = 0 ; i < strNameResults.size(); i++){
			String resFirstName = "", resLastName  = "";
			blnFailure = false;

			if(strLocale.trim().equalsIgnoreCase("RU") || strLocale.trim().equalsIgnoreCase("JP") || strLocale.trim().equalsIgnoreCase("CN")){
				resFirstName = strNameResults.get(i).trim();
				resLastName = strNameResults.get(i).trim();
			}
			else if (strNameResults.get(i).contains(",")) {
				resFirstName =  strNameResults.get(i).substring(strNameResults.get(i).indexOf(",")+1).trim();//	strSearch.split(",")[1].trim();		//barma
				resLastName =  strNameResults.get(i).substring(0,  strNameResults.get(i).indexOf(",")).trim();//	strSearch.split(",")[0].trim();		//sajal
			}
			else		resFirstName = strNameResults.get(i).trim();

			switch(strExactOrContains.trim().toUpperCase()){
			case "EXACT":

				switch (strLocale){
				case "RU":
				case "JP":
				case "CN":
					if (!strFirstName.trim().isEmpty() && !resFirstName.trim().toLowerCase().contains(strFirstName.toLowerCase().trim()))	{
						report.updateTestLog("Validate First Name in Search results", "'" + strFirstName + "' does not match in row : " + (i+1),Status.FAIL);
						blnFailure = true;
						break;
					}

					if (!strLastName.trim().isEmpty()&& !resLastName.trim().toLowerCase().contains(strLastName.toLowerCase().trim()))	{
						report.updateTestLog("Validate Last Name in Search results", "'" + strLastName + "' does not match in row : " + (i+1),Status.FAIL);
						blnFailure = true;
						break;
					}
					break;
				default:
					if (!strFirstName.trim().isEmpty() && !resFirstName.trim().equalsIgnoreCase(strFirstName.trim()))	{
						report.updateTestLog("Validate First Name in Search results", "'" + strFirstName + "' does not match exactly in row : " + (i+1),Status.FAIL);
						blnFailure = true;
						break;
					}

					if (!strLastName.trim().isEmpty() && !resLastName.trim().equalsIgnoreCase(strLastName.trim())){	
						report.updateTestLog("Validate Last Name in Search results", "'" + strLastName + "' does not match exactly in row : " + (i+1),Status.FAIL);
						blnFailure = true;
						break;
					}
				}
				break;
			case "CONTAINS":

				if (!strFirstName.trim().isEmpty() && !resFirstName.trim().toLowerCase().contains(strFirstName.toLowerCase().trim()))	{
					report.updateTestLog("Validate First Name in Search results", "'" + strFirstName + "'does not match partially in row : " + (i+1),Status.FAIL);
					blnFailure = true;
					break;
				}

				if (!strLastName.trim().isEmpty() && !resLastName.trim().toLowerCase().contains(strLastName.toLowerCase().trim()))	{
					report.updateTestLog("Validate Last Name in Search results", "'" + strLastName + "'  does not match partially in row : " + (i+1),Status.FAIL);
					blnFailure = true;
					break;
				}
				break;
			}
		}
		if (!blnFailure)
			report.updateTestLog("Validate search results", "'" + strSearch + "' matches with search results",Status.PASS);

	}

}
