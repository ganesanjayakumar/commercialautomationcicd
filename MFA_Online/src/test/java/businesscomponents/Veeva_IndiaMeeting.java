package businesscomponents;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;

import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_IndiaMeeting extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_IndiaMeeting(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Meeting object
	 * @throws Exception 
	 */
	public void navigateToMeeting() throws Exception{
		vf.clickVeevaTab("Meetings");
		if(cf.isElementVisible(By.xpath("//h1[normalize-space(text())='Meetings']"),"Meeting Home"))
			cf.clickSFButton("New");
	}

	
	public void selectMeetingRecordType() throws Exception{
		cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"),"Record Type","New");
		cf.clickSFButton("Continue");
	}
	
	/**
	 * Method to enter the mandatory fields to create a new meeting
	 * @throws Exception 
	 */

	public synchronized void enterNewMeetingInformation() throws Exception{
		if(cf.isElementVisible(By.xpath("//h1[normalize-space(text())='Meeting Edit']"),"Meeting Edit Page")){
			
			report.updateTestLog("Mandatory details Edit", "Meeting details Edit page is displayed", Status.SCREENSHOT, "Full Screen", driver);
			String name= "Parent Meeting";
			String meetingName=name+new SimpleDateFormat("ddmmss").format(new Date()).toString();
			cf.waitForSeconds(1);
			dataTable.putData("IndiaMeeting", "Parent Meeting", meetingName);	
			cf.setSFData("Information","Meeting Name", "input",meetingName);
			//Meeting type
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Meeting Type", "lookupimage"), "Meeting Type","05/10_Auto Without NS"); 
			//All Mandatory fields are entered
			cf.setSFData("Information","Location", "textarea","Chennai");
			cf.setSFData("Information","Contracts Required", "select","No new Contract required");
			Select status = new Select(driver.findElement(By.xpath("//label[contains(text(),'Status')]/parent::td/following-sibling::td//span/select")));
			String meetStatus = status.getFirstSelectedOption().getText();
			if(meetStatus.equals("New"))
				report.updateTestLog("Meeting status", "Meeting status is autopopulated as New", Status.DONE);
			driver.findElement(By.xpath("//label[text()='Start Date']/parent::td/following-sibling::td//span/a")).click();
			cf.setSFData("Information","Start Time", "select","03:00");
			driver.findElement(By.xpath("//label[text()='End Date']/parent::td/following-sibling::td//span/a")).click();
			cf.setSFData("Information","End Time", "select","05:00");
			String accName = dataTable.getData("IndiaMeeting", "Speaker Name");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Venue Details", "Account", "lookupimage"), "Account",accName);
			vf.getLookupDataInWindow(cf.getSFElementXPath("Venue Details", "Venue", "lookupimage"), "Venue","Test Demo");
			cf.setSFData("Meeting Attendee Details","# Planned Attendees", "input","1");
			cf.setSFData("System Information","Cluster Country", "input","IN");
						
			
	}
}
	
	/**
	 * Method to enter the mandatory fields to create a new child meeting by a sales rep
	 * @throws Exception 
	 */

	public synchronized void childMeetingCreation() throws Exception{
		if(cf.isElementVisible(By.xpath("//h1[normalize-space(text())='Meeting Edit']"),"Meeting Edit Page")){
			String name= "Child Meeting";
			String parent = dataTable.getData("IndiaMeeting", "Parent Meeting");
			String meetingName=name+new SimpleDateFormat("ddmmss").format(new Date()).toString();
			cf.waitForSeconds(1);
			dataTable.putData("IndiaMeeting", "Child Meeting", meetingName);	
			cf.setSFData("Information","Meeting Name", "input",meetingName);
			//Meeting type
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Meeting Type", "lookupimage"), "Meeting Type","05/10_Auto With NS"); 
			//All Mandatory fields are entered
			cf.setSFData("Information","Location", "textarea","Chennai");
			cf.setSFData("Information","Contracts Required", "select","No new Contract required");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Parent Meeting", "lookupimage"), "Parent Meeting",parent);
			Select status = new Select(driver.findElement(By.xpath("//label[contains(text(),'Status')]/parent::td/following-sibling::td//span/select")));
			String meetStatus = status.getFirstSelectedOption().getText();
			if(meetStatus.equals("New"))
				report.updateTestLog("Meeting status", "Meeting status is autopopulated as New", Status.DONE);
			driver.findElement(By.xpath("//label[text()='Start Date']/parent::td/following-sibling::td//span/a")).click();
			cf.setSFData("Information","Start Time", "select","03:00");
			driver.findElement(By.xpath("//label[text()='End Date']/parent::td/following-sibling::td//span/a")).click();
			cf.setSFData("Information","End Time", "select","05:00");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Venue Details", "Account", "lookupimage"), "Account","VIJAYA BABU");
			vf.getLookupDataInWindow(cf.getSFElementXPath("Venue Details", "Venue", "lookupimage"), "Venue","Test Demo");
			cf.setSFData("Meeting Attendee Details","# Planned Attendees", "input","1");
			cf.clickSFButton("Save");
			cf.waitForSeconds(5);
			
			if (driver.findElement(By.xpath("//h1[text()='Meeting']")).toString().contains("Meeting"))
			{
				report.updateTestLog("Meeting Page","Child meeting is created",Status.PASS,"Full Screen",driver);
				String parMeet = driver.findElement(By.xpath("//td[text()='Parent Meeting']/following-sibling::td/div/a")).getText();
				System.out.println(parMeet);
				if(parent.equals(parMeet))
				{
					report.updateTestLog("Meeting Page","Parent meeting is linked to the child",Status.DONE);
				}
				else
					report.updateTestLog("Meeting Page","Parent meeting is not linked to the child",Status.FAIL);
				
			}
			else
				report.updateTestLog("Meeting Page","Child meeting is not created",Status.FAIL);
			
	}
}

	/**
	 * Method to save meeting and validate the details entered during meeting creation
	 * @throws Exception 
	 */
	
	public void saveMeetingPage() throws Exception{
		report.updateTestLog("Mandatory fields are entered", "Meeting details Edit page with all the mandatory values entered", Status.SCREENSHOT, "Full Screen", driver);
		cf.clickSFButton("Save");
		cf.waitForSeconds(5);
		try{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".bPageTitle h1")));//Meeting page is loaded
			if (driver.findElement(By.xpath("//h1[text()='Meeting']")).toString().contains("Meeting"))
			{
				report.updateTestLog("Meeting Page","Meeting detail page is displayed",Status.PASS,"Full Screen",driver);
				String meetingName=dataTable.getData("IndiaMeeting", "Parent Meeting");	
				if(cf.getSFData("Meeting Name", "div", "Text").toLowerCase().trim().equalsIgnoreCase(meetingName))
					report.updateTestLog("Verify Meeting Name","Meeting Name is : ' " +meetingName+"' displayed as expected",Status.DONE);
				else
					report.updateTestLog("Verify Meeting Name",meetingName+ " not displayed",Status.FAIL);
			}else
				throw new FrameworkException("Meeting page not displayed");
		}catch(Exception e) // re-load the meeting detail page
		{
			cf.pageRefresh();
			cf.waitForSeconds(3);
			
			if (driver.findElement(By.xpath("//h1[text()='Meeting']")).toString().contains("Meeting"))
			{
				report.updateTestLog("Meeting Page","Meeting detail page is displayed",Status.PASS,"Full Screen",driver);
				String meetingName=dataTable.getData("IndiaMeeting", "Parent Meeting");	
				if(cf.getSFData("Meeting Name", "div", "Text").toLowerCase().trim().equalsIgnoreCase(meetingName))
					report.updateTestLog("Verify Meeting Name","Meeting Name is : ' " +meetingName+"' displayed as expected",Status.DONE);
				else
					report.updateTestLog("Verify Meeting Name",meetingName+ " not displayed",Status.FAIL);
			}
			else
				throw new FrameworkException("Meeting page not displayed");
		}
}
	
	/**
	 * This method is for developing the script and debugging. This uses one created meeting
	 * @throws Exception
	 */
	public void sG()
	{
		String meetingName = dataTable.getData("IndiaMeeting", "Parent Meeting");
		Select search = new Select(driver.findElement(By.xpath("//select[@title='Search scope']")));
		search.selectByVisibleText("Meetings");
		driver.findElement(By.className("searchTextBox")).sendKeys(meetingName);
		driver.findElement(By.xpath("//div[@class='standardSearchElementBody']//input[@title='Go!']")).click();
		cf.waitForSeconds(3);
	}
	

	/**
	 * This method will create attendee through manage attendee
	 * @throws Exception
	 */
	
	public void manageAttendee() throws Exception
	{
		cf.scrollToTop();
		String att = "";
		driver.findElement(By.xpath("//*[@id='topButtonRow']//input[@name='manage_attendees_vod']")).click();
		cf.waitForSeconds(7);
		cf.switchToFrame(By.name("vod_iframe"));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pageDescription")));//Manage attendee page is loaded
		if(cf.isElementVisible(By.xpath("//h2[text()='Manage Attendees']"), "Manage Attendee page"))
		{
			report.updateTestLog("Manage Attendee","Manage Attendee page is loaded",Status.PASS);
			Select accType = new Select(driver.findElement(By.id("filterViewId")));
			accType.selectByVisibleText("All Person Accounts");
			cf.waitForSeconds(15);
			/*cf.setData(By.xpath("//table[@id='SearchInputtable']/tbody//tr//td/input[@id='SearchBoxValue']"), "Search Box", accName);
			cf.clickButton(By.id("goButton"), "Go!");
			cf.waitForSeconds(5);*/
			driver.findElement(By.xpath("//table[@id='AttendeeListTable']/tbody//tr[2]/td/input")).click();
			cf.clickButton(By.id("addSelButton"), "Add Selected Attendee");
			cf.waitForSeconds(3);
			//verify if the selected attendee is displayed in the Meeting attendee section
			att = driver.findElement(By.xpath("//table[@id='eventAttendeeSelectedTable']/tbody//tr[2]//td[contains(@id,'name')]")).getText();
			/*String[] atte = att.split(", ");
			System.out.println(atte[0]);
			System.out.println(atte[1]);
			
			String[] acco = accName.split(" ");
			if((acco[1].contains(atte[0])) && (acco[0].contains(atte[1])))
			{
				System.out.println(accName+" Attendee is added");*/
				dataTable.putData("IndiaMeeting", "Manage Attendees", att);
				report.updateTestLog("Manage Attendees", "Attendee "+att+" is added to the meeting", Status.PASS);
			/*}
			else
			{
				report.updateTestLog("Manage Attendees", "Attendee "+accName+" is not added to the meeting", Status.FAIL);
			}
			*/
			cf.clickLink(By.xpath("//div[@id='ptBreadcrumb']/a"), "Back to Meetings");
			cf.waitForSeconds(5);
		}
		
		else
			report.updateTestLog("Manage Attendee","Manage Attendee page is not loaded properly",Status.FAIL);
		//Meeting detail page - Validate the related list
		if (driver.findElement(By.xpath("//h1[text()='Meeting']")).toString().contains("Meeting"))
		{
			System.out.println("Returned to meeting detail page");
			report.updateTestLog("Meeting Details Page", "Returnred to meeting details page", Status.DONE);
		}
		else
			System.out.println("Meeting details page is not loaded");
		
		cf.clickSFLinkLet("Meeting Attendees");
		cf.verifyDataFromLinkLetTable("Meeting Attendees","Attendee Name",att,"Status","Invited");
		
	}	
	/**
	 * This method will create meeting speakers
	 * @throws Exception
	 */
	
	public void meetingSpeaker() throws Exception
	{
		String accName = dataTable.getData("IndiaMeeting", "Speaker Name");

		String[] a =  accName.split(" ");
		System.out.println(a[0]);System.out.println(a[1]);
		String speakerAcc = a[1].concat(", ").concat(a[0]);
		System.out.println(speakerAcc);
		
		String speakerID = "";
		cf.scrollToTop();
		cf.clickSFLinkLet("Meeting Speakers");
		report.updateTestLog("Meeting Speaker", "Meeting Speaker related list", Status.SCREENSHOT);
		
		//1. Account Meeting Speaker		
		cf.clickSFButton("New Meeting Speaker", "pbButton");
		cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"),"Speaker Type","Account Meeting Speaker");
		cf.clickSFButton("Continue");
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Speaker Edit']"), "Speaker Edit Page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Account", "lookupimage"), "Account",accName);
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Speaker Role", "lookupimage"), "Speaker Role","Test Speaker Role_01");	
			cf.waitForSeconds(3);
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Address", "lookupimage"), "Address","");
			cf.setSFData("Speaker/Meeting Information", "Duration of the Service", "input", "2");
			cf.setSFData("Speaker/Meeting Information", "Preparation Time (hrs)", "input", "1");
			cf.setSFData("Speaker/Meeting Information", "Contract Status", "select", "No Contract Required");
			report.updateTestLog("Account Speaker details", "Account speaker details are entered in the Speaker Edit Page", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Speaker']"), "Meeting Speaker Saved Page"))
			{
				speakerID = driver.findElement(By.id("Name_ileinner")).getText();
				System.out.println(speakerID);
				//return to meeting to check in the related list
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);
							
			}
			else
				report.updateTestLog("Speaker details save", "Speaker details are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Speakers");
			cf.verifyDataFromLinkLetTable("Meeting Speakers","Meeting Speaker Name",speakerID,"Account",accName);
			String speakerChkBox = cf.getDataAttributeFromLinkLetTableCheckbox("Meeting Attendees", "Attendee Name",speakerAcc, "Speaker", "title");
			if(speakerChkBox.equalsIgnoreCase("checked"))
			{
				report.updateTestLog("Meeting Attendess", "Account Meeting Speaker is also added in the meeting attendee section", Status.DONE);
				report.updateTestLog("Meeting Speaker", "Account Meeting Speaker is added", Status.DONE);
			}
			
			else
				report.updateTestLog("Meeting Attendess", "Account Meeting Speaker is not added in the meeting attendee section", Status.FAIL);
		}
		
		else 
			report.updateTestLog("Speaker Edit Page", "Speaker edit page is not loaded", Status.FAIL);
			
			
		//2. Non-Account Meeting Speaker		
				cf.clickSFButton("New Meeting Speaker", "pbButton");
				cf.selectData(cf.getSFElementXPath("Record Type of new record", "select"),"Speaker Type","Non-Account Meeting Speaker");
				cf.clickSFButton("Continue");
				if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Speaker Edit']"), "Speaker Edit Page"))
				{
					vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Non-Account Speaker", "lookupimage"), "Non-Account Speaker","Test Non Account Speaker 01");
					vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Speaker Role", "lookupimage"), "Speaker Role","Test Speaker Role_01");
					cf.setSFData("Information", "Status", "select", "Attended");
					cf.setSFData("Speaker/Meeting Information", "Duration of the Service", "input", "2");
					cf.setSFData("Speaker/Meeting Information", "Preparation Time (hrs)", "input", "1");
					cf.setSFData("Speaker/Meeting Information", "Contract Status", "select", "No Contract Required");
					report.updateTestLog("Non-Account Speaker details", "Non-Account speaker details are entered in the Speaker Edit Page", Status.PASS);
					cf.clickSFButton("Save");
					cf.waitForSeconds(2);
					if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Speaker']"), "Meeting Speaker Saved Page"))
					{
						speakerID = driver.findElement(By.id("Name_ileinner")).getText();
						System.out.println(speakerID);
						report.updateTestLog("Meeting Speaker", "Account Meeting Speaker is saved", Status.SCREENSHOT);
						//return to meeting to check in the related list
						cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
						cf.waitForSeconds(4);
									
					}
					else
						report.updateTestLog("Speaker details save", "Speaker details are not saved", Status.FAIL);
					
					cf.clickSFLinkLet("Meeting Speakers");
					cf.verifyDataFromLinkLetTable("Meeting Speakers","Meeting Speaker Name",speakerID,"Non-Account Speaker","Test Non Account Speaker 01");
					report.updateTestLog("Meeting Speaker", "Account Meeting Speaker is added", Status.DONE);
				}
				
				else 
					report.updateTestLog("Speaker Edit Page", "Speaker edit page is not loaded", Status.FAIL);
	}
	
	/**
	 * This method will create meeting additional attendees
	 * @throws Exception
	 */	
	public void additionalAttendees() throws Exception
	{
		cf.scrollToTop();
		String addAtt = "";
		cf.clickSFLinkLet("Meeting Additional Attendees");
		report.updateTestLog("Meeting Additional Attendees", "Meeting Additional Attendees related list", Status.SCREENSHOT);
		cf.clickSFButton("New Meeting Additional Attendee", "pbButton");
		cf.waitForSeconds(3);
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Additional Attendee Edit']"), "Meeting Additional Attendee Edit page"))
		{
			cf.setSFData("Information", "First Name", "input", "Firstname");
			cf.setSFData("Information", "Last Name", "input", "Lastname");
			cf.setSFData("Information", "Primary Account (Place of work)", "input", "AZOffice");
			cf.setSFData("Information", "Status", "select", "Attended"); // status by default is attended.
			cf.setSFData("Information", "Reason", "input", "Test");
			cf.setSFData("Information", "Type", "select", "External guest (non HCP)");
			report.updateTestLog("Meeting Additional Attendees", "All Mandatory fields are entered in the Meeting Additional Attendee Edit page ", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Additional Attendee']"), "Meeting Additional Attendees saved page"))
			{
				report.updateTestLog("Meeting Additional Attendees", "Meeting Additional Attendees is saved", Status.SCREENSHOT);
				addAtt = driver.findElement(By.id("Name_ileinner")).getText();
				System.out.println(addAtt);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);				
			}
			else
				report.updateTestLog("Meeting Additional Attendees", "Meeting Additional Attendees are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Additional Attendees");
			cf.verifyDataFromLinkLetTable("Meeting Additional Attendees","Additional Attendee Name",addAtt,"Type","External guest (non HCP)");
			report.updateTestLog("Meeting Additional Attendees", "Meeting Additional Attendees is added", Status.DONE);
			
		}
		else
			report.updateTestLog("Meeting Additional Attendee Edit page", "Meeting Additional Attendee Edit page is not loaded", Status.FAIL);
				
	}
	
	
	/**
	 * This method will add new meeting documents or materials
	 * @throws Exception
	 */	
	public void documentsMaterials() throws Exception
	{
		cf.scrollToTop();
		String docName = "";
		String docNum = "";
		cf.clickSFLinkLet("Meeting Documents/Materials");
		report.updateTestLog("Meeting Documents/Materials", "Meeting Documents/Materials related list", Status.SCREENSHOT);
		cf.clickSFButton("New Meeting Document/Material", "pbButton");
		cf.waitForSeconds(3);
		
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Document/Material Edit']"), "Meeting Document/Material Edit page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Meeting Document", "lookupimage"), "Meeting Document","");
			report.updateTestLog("Meeting Documents/Materials", "Meeting document is added", Status.PASS);
			docName = driver.findElement(By.xpath("//span[@class='lookupInput']/input")).getText();
			System.out.println(docName);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Document/Material']"), "Meeting Document/Material saved page"))
			{
				report.updateTestLog("Meeting Documents/Materials", "Meeting Documents/Materials is saved", Status.SCREENSHOT);
				docNum = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Document Number']/following-sibling::td[1]/div")).getText();
				System.out.println(docNum);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);	
			}
			
			else
				report.updateTestLog("Meeting Documents/Materials", "Meeting Documents/Materials are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Documents/Materials");
			//cf.verifyDataFromLinkLetTable("Meeting Documents/Materials","Meeting Document",docName,"Document Number",docNum);
			report.updateTestLog("Meeting Documents/Materials", "Meeting Documents/Materials is added", Status.PASS);
			
			
		}
		else
			report.updateTestLog("Meeting Document/Material Edit page", "Meeting Document/Material Edit page is not loaded/displayed", Status.FAIL);
		
	}
	
	/**
	 * This method will add new meeting budget
	 * @throws Exception
	 */	
	public void meetingBudget() throws Exception
	{
		cf.scrollToTop();
		String budgetName ="";
		String budgetRemainingAmount ="";
		cf.clickSFLinkLet("Meeting Budgets");
		report.updateTestLog("Meeting Budgets", "Meeting Budgets related list", Status.SCREENSHOT);
		cf.clickSFButton("New Meeting Budget", "pbButton");
		cf.waitForSeconds(3);
		
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Budget Edit']"), "Meeting Budget Edit page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Budget", "lookupimage"), "Budget","Auto_Test Budget");
			cf.setSFData("Information", "Allocation %", "input", "100");
			report.updateTestLog("Meeting Budgets", "Meeting Budgets values are added", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Budget']"), "Meeting Budget saved page"))
			{
				report.updateTestLog("Meeting Budgets", "Meeting Budget is saved", Status.SCREENSHOT);
				budgetName = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Event Budget Name']/following-sibling::td[1]/div")).getText();
				budgetRemainingAmount = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Budget Remaining Amount']/following-sibling::td[1]/div")).getText();
				System.out.println(budgetName);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);	
			}
			
			else
				report.updateTestLog("Meeting Budgets", "Meeting Budgets are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Budgets");
			cf.verifyDataFromLinkLetTable("Meeting Budgets","Event Budget Name",budgetName,"Budget Remaining Amount",budgetRemainingAmount);
			report.updateTestLog("Meeting Budgets", "Meeting Budgets is added", Status.DONE);
			
			
		}
		else
			report.updateTestLog("Meeting Budgets Edit Page", "Meeting Budgets Edit page is not loaded/displayed", Status.FAIL);
		
	}
	
	/**
	 * This method will add new meeting cost
	 * @throws Exception
	 */	
	public void meetingCost() throws Exception
	{
		cf.scrollToTop();
		String costID ="";
		String plannedCost ="";
		cf.clickSFLinkLet("Meeting Costs");
		report.updateTestLog("Meeting Costs", "Meeting Costs related list", Status.SCREENSHOT);
		cf.clickSFButton("New Meeting Cost", "pbButton");
		cf.waitForSeconds(3);
		
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Cost Edit']"), "Meeting Cost Edit page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Cost Category", "lookupimage"), "Cost Category","");
			cf.setSFData("Information", "Planned Cost", "input", "500");
			report.updateTestLog("Meeting Costs", "Meeting Costs values are entered", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Cost']"), "Meeting Cost saved page"))
			{
				report.updateTestLog("Meeting Costs", "Meeting Cost is saved", Status.SCREENSHOT);
				costID = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Cost ID']/following-sibling::td[1]/div")).getText();
				dataTable.putData("IndiaMeeting", "Cost ID", costID);
				plannedCost = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Planned Cost']/following-sibling::td[1]/div")).getText();
				System.out.println(costID);
				System.out.println(plannedCost);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);	
			}
			
			else
				report.updateTestLog("Meeting Costs", "Meeting Costs are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Costs");
			cf.verifyDataFromLinkLetTable("Meeting Costs","Cost ID",costID,"Planned Cost",plannedCost);
			report.updateTestLog("Meeting Costs", "Meeting Cost is added", Status.DONE);
			
			
		}
		else
			report.updateTestLog("Meeting Cost Edit Page", "Meeting Cost Edit page is not loaded/displayed", Status.FAIL);
		
	}
	
	/**
	 * This method will add new meeting product
	 * @throws Exception
	 */	
	public void meetingProduct() throws Exception
	{
		cf.scrollToTop();
		String pdtID ="";
		String pdt ="";
		cf.clickSFLinkLet("Meeting Products");
		report.updateTestLog("Meeting Products", "Meeting Products related list", Status.SCREENSHOT);
		cf.clickSFButton("New Meeting Product", "pbButton");
		cf.waitForSeconds(3);
		
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Product Edit']"), "Meeting Product Edit page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Products", "lookupimage"), "Products","Nexium_IN");
			report.updateTestLog("Meeting Products", "Meeting Product is entered", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Product']"), "Meeting Product saved page"))
			{
				report.updateTestLog("Meeting Product", "Meeting Product is saved", Status.SCREENSHOT);
				pdtID = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Event Product ID']/following-sibling::td[1]/div")).getText();
				pdt = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Products']/following-sibling::td[1]/div")).getText();
				System.out.println(pdtID);
				System.out.println(pdt);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(4);	
			}
			
			else
				report.updateTestLog("Meeting Products", "Meeting Products are not saved", Status.FAIL);
			
			cf.clickSFLinkLet("Meeting Products");
			cf.verifyDataFromLinkLetTable("Meeting Products","Event Product ID",pdtID,"Products",pdt);
			report.updateTestLog("Meeting Products", "Meeting Product is added", Status.DONE);
			
			
		}
		else
			report.updateTestLog("Meeting Products Edit Page", "Meeting Products Edit page is not loaded/displayed", Status.FAIL);
		
	}
	
	/**
	 * This method will add notes and attachments
	 * @throws Exception
	 */	
	
	public void addNoteAndAttachment() throws Exception{
		cf.clickSFLinkLet("Notes & Attachments");
		cf.clickSFButton("New Note", "pbButton");
		String strDateString=new SimpleDateFormat("mmss").format(new Date()).toString();
		cf.setSFData("Note Information", "Title", "textbox", "SampleNote"+strDateString);
		cf.setSFData("Note Information", "Body", "textarea", "Sample Body for automation at "+(new Date()).toString());
		report.updateTestLog("Notes for account plan", "Notes are entered", Status.SCREENSHOT);
		cf.clickSFButton("Note Information", "Save", "pbButtonb");

		try {
			File file = new File("C:\\workspace\\samplefiletoattach.txt");
			boolean fvar = file.createNewFile();
			if (fvar)	System.out.println("File has been created successfully");
			else				System.out.println("File already present at the specified location");
			FileOutputStream outputStream = new FileOutputStream("C:\\workspace\\samplefiletoattach.txt");
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-16");
			BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
			bufferedWriter.write("Text added for testing");
			bufferedWriter.newLine();
			bufferedWriter.write("Welcome to Veeva Sales Force!");
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println("Exception Occurred:");
			e.printStackTrace();
		}

		cf.clickSFLinkLet("Notes & Attachments");
		cf.clickSFButton("Attach File", "pbButton");
		driver.findElement(By.id("file")).sendKeys("C:\\workspace\\samplefiletoattach.txt");
		driver.findElement(By.id("Attach")).click();
		report.updateTestLog("Attachments for account plan", "File attachment is done", Status.SCREENSHOT);
		cf.clickButton(By.cssSelector("input[title='Done']"), "Attachment Done");

		cf.clickSFLinkLet("Notes & Attachments");
		cf.verifyDataFromLinkLetTable("Notes & Attachments","Title","SampleNote"+strDateString,"","");

		cf.verifyDataFromLinkLetTable("Notes & Attachments","Title","samplefiletoattach.txt","","");

	}
	
	/**
	 * This method will add new task and assign it to Even Desk user
	 * @throws Exception
	 */	
	public void openActivities() throws Exception
	{
		cf.scrollToTop();
		
	//	String strDateString=new SimpleDateFormat("mmss").format(new Date()).toString();
		String meetingName= dataTable.getData("IndiaMeeting", "Parent Meeting");
		String opsAdminComments=(new Date()).toString()+":\n"+meetingName+" needs Event Desk Approval";
		String subject ="";
		String status="";
		String taskID="";
		cf.clickSFLinkLet("Open Activities");
		report.updateTestLog("Open Activities", "Open Activities related list", Status.SCREENSHOT);
		cf.clickSFButton("New Task", "pbButton");
		cf.waitForSeconds(3);
		
		if(cf.isElementVisible(By.xpath("//h2[text()=' New Task']"), "Task Edit page"))
		{
			vf.getLookupDataInWindow(cf.getSFElementXPath("Task Information", "Assigned To", "lookupimage"), "Assigned To","Nipun Jain");	
			driver.findElement(By.className("comboboxIcon")).click();
			cf.waitForSeconds(8);
			//Select subject value which is a combo box
			String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
			String subWindowHandler = null;
			Set<String> handles = driver.getWindowHandles(); // get all window handles
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext())
			{
			    subWindowHandler = iterator.next();
			}		
			
			driver.switchTo().window(subWindowHandler);
			cf.waitForSeconds(5);
			
			/*Pop up combo box code here*/
			List<WebElement> itemsInComboBox = driver.findElements(By.xpath("//div[contains(@class,'choicesBox tertiaryPalette brandSecondaryBrd')]/ul/li"));
			int size = itemsInComboBox.size()-1;
			int random = ThreadLocalRandom.current().nextInt(0, size); //Generate random number within the size of the items
			subject = driver.findElement(By.xpath("//div[contains(@class,'choicesBox tertiaryPalette brandSecondaryBrd')]/ul/li["+random+"]/a")).getText(); 
			driver.findElement(By.xpath("//div[contains(@class,'choicesBox tertiaryPalette brandSecondaryBrd')]/ul/li["+random+"]/a")).click();		
			System.out.println("The subject selected is "+subject);
			dataTable.putData("IndiaMeeting", "Task Subject", subject);
			driver.switchTo().window(parentWindowHandler); 
			/*Pop up combo box code ends here*/
			
			cf.waitForSeconds(2);
			WebElement st= driver.findElement(By.xpath("//label[text()='Status']/parent::td/following-sibling::td//div//select"));
			Select statusDrpDwn = new Select(st);
			status = statusDrpDwn.getFirstSelectedOption().getText();
			System.out.println("Task status is in "+status);
			dataTable.putData("IndiaMeeting", "Task Status", status);
			cf.setSFData("Task Information", "Priority", "select", "Normal");
			cf.setSFData("Description Information", "Comments", "textarea", opsAdminComments);
			cf.setSFData("IContact Inside Sales", "Type", "select", "Meeting");
			cf.setSFData("System Information", "Country Code", "input", "IN");
			cf.setSFData("System Information", "Medical Affairs", "select", "No");
			
			report.updateTestLog("Open Activities", "Task is entered", Status.PASS);
			cf.clickSFButton("Save");
			cf.waitForSeconds(2);
			
			//Go to meeting details page
			cf.clickSFLinkLet("Open Activities");
			//Get Task ID
			taskID = cf.getDataFromLinkLetTable("Open Activities", "Subject",subject, "Task ID");
			System.out.println(taskID);
			dataTable.putData("IndiaMeeting", "Task ID", taskID);
			cf.verifyDataFromLinkLetTable("Open Activities","Task ID",taskID,"Status",status);
			
			report.updateTestLog("Open Activities", "Task is seen in the open activities related list", Status.PASS);	
			
		}
		else
			report.updateTestLog("Task Edit page Page", "Task Edit page is not loaded/displayed", Status.FAIL);
	}
	
	/**
	 * This method is for Event Desk user to select the task created by Ops Admin
	 * @throws Exception
	 */	
	
	public void myTask() throws Exception
	{
		Boolean b=false;
		int i;
		String relatedMeeting = dataTable.getData("IndiaMeeting", "Parent Meeting");
		String taskSubject = dataTable.getData("IndiaMeeting", "Task Subject");
		System.out.println("The meeting created opsAdmin is "+relatedMeeting+" and the task assigned to event desk user is with subject "+taskSubject);
		if(cf.isElementVisible(By.xpath("//h3[text()='My Tasks']"), "My Task section"))
		{
			cf.scrollToElement(By.xpath("//h3[text()='My Tasks']"), "My Task section");
			report.updateTestLog("My Task", "Event Desk User is having My Task section", Status.PASS);	
			//Get all the rows
			List<WebElement> tr = driver.findElements(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr"));
			int noOfTasks = tr.size();
			System.out.println("No. of task seen for the event desk user = "+noOfTasks);
			for(i=2;i<=noOfTasks;i++)
			{
				String meetingName = driver.findElement(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr["+i+"]/td[5]")).getText();
				System.out.println(meetingName);				
				if(relatedMeeting.equalsIgnoreCase(meetingName))
				{
					cf.waitForSeconds(2);
					String subject = driver.findElement(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr["+i+"]/td[3]")).getText();
					System.out.println(subject);
					if(taskSubject.equalsIgnoreCase(subject))
					{
						report.updateTestLog("My Task", "Related meeting task and subject are present in the My Task section of Event Desk User", Status.PASS);
						b=true;
						break;
					}
				}
			}
			
			if(b==false)
			{
				report.updateTestLog("My Task", "Related meeting task and subject are not present in the My Task section of Event Desk User", Status.FAIL);
			}
			
			else
			{
				//click and go to the task details page
				driver.findElement(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr["+i+"]/td[3]/a")).click();
				cf.waitForSeconds(6);
			}
			
		}
		
		else
			report.updateTestLog("My Task section", "My Task section loaded/displayed for event desk user", Status.FAIL);
	}
	
	/**
	 * This method is for Event Desk user responds to the comments put by opsadmin. Changes the status of the task as completed
	 * @throws Exception
	 */	
	
	public void taskDetail() throws Exception
	{
		String taskID = dataTable.getData("IndiaMeeting", "Task ID");
		String tID="";
		int i;
		Boolean t=false;
		String meetingName = dataTable.getData("IndiaMeeting", "Parent Meeting");
		String taskSubject = dataTable.getData("IndiaMeeting", "Task Subject");
		if(cf.isElementVisible(By.xpath("//h1[text()='Task']"), "Task Edit Page"))
		{
			report.updateTestLog("My Task", "Task Edit page is displayed", Status.DONE);
			tID =driver.findElement(By.xpath("//*[text()='Task ID']/following-sibling::td/div")).getText();
			
			if(taskID.equals(tID))
			{
				report.updateTestLog("My Task", "Related meeting task ID is seen", Status.PASS);
			}
			else
				report.updateTestLog("My Task", "Related meeting task ID is not seen", Status.FAIL);
		}
		else
			report.updateTestLog("My Task", "Task Edit Page is not opened", Status.FAIL);
		
		cf.clickSFButton("Edit");
		//Event desk user is responding to the comments of ops admin
		cf.waitForSeconds(3);
		String comments = "\n\n\n"+(new Date()).toString()+":\n"+meetingName+" is approved by Event Desk Admin";
		WebElement commentArea = driver.findElement(By.xpath("//label[text()='Comments']/parent::td/following-sibling::td/textarea"));
		commentArea.sendKeys(comments);
		dataTable.putData("IndiaMeeting", "Event User Comments", comments);
		//Comments entered and the status of the task is changed to completed
		cf.setSFData("Task Information","Status", "select","Completed");
		Select st = new Select(driver.findElement(By.xpath("//label[text()='Status']/parent::td/following-sibling::td//select")));
		String status = st.getFirstSelectedOption().getText();
		dataTable.putData("IndiaMeeting", "Task Status", status);
		report.updateTestLog("My Task", "Event Desk user completes the task by entering the repsonse", Status.SCREENSHOT);
		cf.clickSFButton("Save");
		cf.waitForSeconds(3);
		report.updateTestLog("My Task", "Task is completed", Status.PASS);		
		
		//completed task is removed from the home page task list
		//go to home page
		vf.clickVeevaTab("Home");
		List<WebElement> tr = driver.findElements(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr"));
		int noOfTasks = tr.size();
		System.out.println("No. of task seen for the event desk user = "+noOfTasks);
		for(i=2;i<=noOfTasks;i++)
		{
			String meeting = driver.findElement(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr["+i+"]/td[5]")).getText();
			System.out.println(meeting);
			if(meeting.equalsIgnoreCase(meetingName))
			{
				String subject = driver.findElement(By.xpath("//h3[text()='My Tasks']/following::table/tbody/tr["+i+"]/td[3]")).getText();
				if(taskSubject.equalsIgnoreCase(subject))
				{					
					t=true;
					break;
				}
				t =true;
			}
			
		}
		if(t==false)
		{
			report.updateTestLog("My Task", "Completed task is not present in the task list", Status.PASS);
		}
		else
			report.updateTestLog("My Task", "Completed task is still present in the task list", Status.FAIL);
		
	}
	
	/**
	 * This method is for verifying whether the event desk user completed task is reflected in the ops admin related list
	 * @throws Exception
	 */	
	
	
	
	public void verifyTask() throws Exception
	{
		String status = dataTable.getData("IndiaMeeting", "Task Status");
	//	String taskID = dataTable.getData("IndiaMeeting", "Task ID");
		String comments = dataTable.getData("IndiaMeeting", "Event User Comments");
		String taskSub = dataTable.getData("IndiaMeeting", "Task Subject");
		String taskStatus="";
		sG(); //opens the parent meeting created by opsAdmin. 
		cf.clickSFLinkLet("Activity History");
		report.updateTestLog("Activity History", "Activity History related list", Status.SCREENSHOT);
		
		//TODO //open activity does not contain the completed task. It is moved to Activity history related list
		
		//cf.clickLinkFromLinkLetTable("Activity History", "Task ID",taskID, "Subject");//open the task		
		cf.clickLinkFromLinkLetTable("Activity History","Subject",taskSub) ;
		cf.waitForSeconds(3);
		
		//Get the comments and status present there in a string
		taskStatus = driver.findElement(By.xpath("//td[text()='Status']/following-sibling::td/div")).getText();			
		String comm = driver.findElement(By.xpath("//td[text()='Comments']/following-sibling::td/div")).getText();
		System.out.println(comm);
		
		if(status.equals(taskStatus))
		{
			report.updateTestLog("Open Task", "Task is in completed status", Status.PASS);
			
			if(comm.contains(comments))
				report.updateTestLog("Open Activities", "Task comment entered by Event Desk user is present", Status.DONE);
			else
				report.updateTestLog("Open Activities", "Task comment entered by Event Desk user is not present", Status.FAIL);
		}
		
		else
			report.updateTestLog("Open Task", "Task is not in completed status", Status.FAIL);
		
	}
	

	/**
	 * This method is for verifying whether the child meeting is visible in the parent meeting's related list
	 * @throws Exception
	 */	
	
	public void verifyChildinParent() throws Exception
	{
		sG(); //opens the parent meeting created by opsAdmin. 
		String childMeeting = dataTable.getData("IndiaMeeting", "Child Meeting");
		cf.waitForSeconds(4);
		cf.verifyDataFromLinkLetTable("Meetings", "Meeting Name", childMeeting, "Status", "New");
		report.updateTestLog("Meeting", "Child meeting is present in the parent meeting related list", Status.DONE);
		
	}
	/**
	 * This method is for changing the meeting attendee to attended status for completing the meeting
	 * @throws Exception
	 */	
	
	public void changeAttendeeStatus() throws Exception
	{
		/*String attendee = dataTable.getData("IndiaMeeting", "Manage Attendees");
		String speakerAtt = dataTable.getData("IndiaMeeting", "Speaker Name");

		String[] a =  speakerAtt.split(" ");
		System.out.println(a[0]);System.out.println(a[1]);
		String speakerAttendee = a[1].concat(", ").concat(a[0]);
		System.out.println(speakerAttendee);*/
		
		cf.clickSFLinkLet("Meeting Attendees");
		int x= cf.getRowCountFromLinkLetTable("Meeting Attendees");
		for(int i=1; i<=x; i++)
		{
			String name = cf.getDataFromLinkLetTable("Meeting Attendees", "Attendee Name", i+1);			
			cf.clickLinkFromLinkLetTable("Meeting Attendees", "Attendee Name", name);
			if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Attendee']"), "Meeting Attendee page"))
			{
				cf.clickSFButton("Edit");
				cf.setSFData("Information","Status", "select","Attended");
				cf.clickSFButton("Save");
				cf.waitForSeconds(3);
				report.updateTestLog("Meeting Attendee", "Meeting Attendee status is changed", Status.PASS);
				cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
				cf.waitForSeconds(3);
				cf.clickSFLinkLet("Meeting Attendees");
				cf.verifyDataFromLinkLetTable("Meeting Attendees", "Attendee Name",name, "Status", "Attended");
				cf.waitForSeconds(5);
			}	
			else
				report.updateTestLog("Meeting Attendee", "Meeting Attendee page is not loaded", Status.FAIL);	
		}
	}
	
	/**
	 * This method is for adding the actual Cost
	 * @throws Exception
	 */	
	
	public void addActualCost() throws Exception
	{
		String costID = dataTable.getData("IndiaMeeting", "Cost ID");
		cf.clickSFLinkLet("Meeting Costs");
		cf.clickLinkFromLinkLetTable("Meeting Costs","Cost ID",costID) ;
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Cost']"), "Meeting Attendee page"))
		{
			report.updateTestLog("Meeting Cost", "Meeting Cost page is loaded", Status.PASS);
		//	String plannedCost = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Planned Cost']/following-sibling::td[1]/div")).getText();
			cf.clickSFButton("Edit");
			cf.waitForSeconds(3);
			cf.setSFData("Information","Actual Cost", "input","400");
			cf.clickSFButton("Save");
			cf.waitForSeconds(3);
			cf.clickLink(By.xpath("//td[@class='labelCol' and text()='Meeting']/following-sibling::td/div/a"), "Meeting");
			cf.waitForSeconds(3);
			cf.clickSFLinkLet("Meeting Costs");
			cf.verifyDataFromLinkLetTable("Meeting Costs", "Cost ID",costID, "Actual Cost", "INR 400.00");
			cf.waitForSeconds(5);
		}	
		else
			report.updateTestLog("Meeting Cost", "Meeting Cost Edit page is not loaded", Status.FAIL);
		
	}
	
	/**
	 * This method is for entering all the features to approve a meeting
	 * @throws Exception
	 */	
	public void approveMeeting() throws Exception
	{
		String status ="";
		/*Attendees status are changed to Attended to approve the meeting*/
		changeAttendeeStatus();
		
		//click submit for email approval
		cf.scrollToTop();
		report.updateTestLog("Meeting Approval", "Before submit for approval", Status.SCREENSHOT);
		driver.findElement(By.xpath("//*[@id='topButtonRow']//input[@name='submit_for_email_approval_az']")).click();
		cf.waitForSeconds(5);		
		status = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Status']/following-sibling::td/div")).getText();
		
		if(status.equals("Approved"))
		{
			report.updateTestLog("Meeting Approval", "Meeting is in approved status", Status.PASS);
		}
		else
			report.updateTestLog("Meeting Approval", "Meeting is not in approved status", Status.FAIL);
		/*Enter the actual meeting costs*/
		addActualCost();
		cf.clickSFButton("Edit"); //Edit the meeting
		cf.waitForSeconds(3);
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting Edit']"), "Meeting edit page"))
		{
			report.updateTestLog("Meeting Approval", "Meeting edit page is displayed with meeting status as approved", Status.PASS);
			cf.setSFData("Information","Status", "select","Completed (all costs entered)");
			cf.clickSFButton("Save");
			cf.waitForSeconds(4);
			
		}
		
		else
			report.updateTestLog("Meeting Approval", "Meeting edit page is not displayed", Status.FAIL);
		
		if(cf.isElementVisible(By.xpath("//h1[text()='Meeting']"), "Meeting Home page"))
		{
			status = driver.findElement(By.xpath("//td[@class='labelCol' and text()='Status']/following-sibling::td/div")).getText();
			if(status.equals("Completed (all costs entered)"))
			{
				report.updateTestLog("Meeting Approval", "Meeting is in Completed (all costs entered) status", Status.PASS,"Full Screen",driver);
			}
			else
				report.updateTestLog("Meeting Approval", "Meeting is not in Completed (all costs entered) status", Status.FAIL);
		}
		else
			report.updateTestLog("Meeting Approval", "Meeting home page is not displayed", Status.FAIL);
	}
	
	
}//End
