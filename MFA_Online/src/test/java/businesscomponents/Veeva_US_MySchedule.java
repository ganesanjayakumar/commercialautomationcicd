package businesscomponents;

import supportlibraries.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.mfa.framework.Status;
import ObjectRepository.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Veeva_US_MySchedule extends VeevaFunctions {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);


	String weekft;
	Date dNow;
	int iDayRandom;
	String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
	By byDayPosition;
	WebElement account;
	String street;
	String pincode;
	String state;
	String accounttext;
	Actions builder = new Actions(driver.getWebDriver());
	//static String scheduledcallTime;
	WebElement mouseovercall;
	String callaccount;
	String date;
	int iDay_Position;
	String accountdraganddrop;
	String scheduledcallTimedraganddrop;
	String finalaccountname;
	String finalaccountnamedrag;

	public Veeva_US_MySchedule(ScriptHelper scriptHelper) {
		super(scriptHelper);

	}

	public void verifyNewCalendarEntry() throws Exception{
		Boolean schedulecalls=false;
		Boolean view=false;
		String scheduleoption=null;
		cf.waitForSeconds(10);
		WebElement ele=driver.findElement(ObjVeevaEmail.workweek);
		cf.clickElement(ele, "Clicked on work week Radio Button");
		//String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
		By byDay = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt]");
	//	By byTime = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr");

		By byCalendar_LeftMost = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[1]");
		int iDayCount = driver.findElements(byDay).size();
	///	int itimeCount = driver.findElements(byTime).size();
		iDayRandom = 0;
		dNow = new Date();
		String time=new SimpleDateFormat("HH").format(dNow).toString();
		int timen=Integer.parseInt(time);
		SimpleDateFormat week = new SimpleDateFormat ("EEEE");
		String weekft=week.format(dNow).toString();

		for(int i=1;i<=iDayCount;i++){

			String weekname=driver.findElement(By.xpath(strCalendarXPath+"/div[@class='fc-agenda-head']//tr[@class='fc-first']/th["+(i+1)+"]//td[2]")).getText();
			if(weekft.equals(weekname)){
				iDayRandom=i;
				WebElement delete=driver.findElement(By.xpath(strCalendarXPath+"/div[@class='fc-agenda-head']//tr[@class='fc-first']/th["+(i+1)+"]//td[3]/div"));
				if(cf.isElementVisible(delete)){
					cf.clickElement(delete, "Delete all planned calls");
					driver.switchTo().alert().accept();
					schedulecalls=true;
				}

				break;
			}

		}
		if(schedulecalls == true){
			if(timen<=18){
				int iTimeRandom = cf.getRandomNumber(timen*2,30);//int iTimeRandom = cf.getRandomNumber(1,itimeCount);


				By byDayPosition = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" +iDayRandom +"]");
				By byTimePostion = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/td");

				By byDayValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]//tr");
				String strDayValue = cf.getData(byDayValue, "Day Value", "text");
				System.out.println("strDayValue: " + strDayValue);
				By byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/th");
				String strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
				if(strTimeValue.equals("")){ 
					byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + (iTimeRandom - 1) + "]/th");
					strTimeValue = cf.getData(byTimeValue, "Time Value", "text");			
					strTimeValue = strTimeValue.replace("00", "30");			
				}
				System.out.println("strTimeValue: " + strTimeValue);

				String endtime=vf.addMinsToDate12(strTimeValue, 30);
				String scheduledcallTime=strTimeValue+" - "+endtime;
				System.out.println(scheduledcallTime);
				///Find x offset Width to move
				int iCalendar_LeftMost_Position = driver.findElement(byCalendar_LeftMost).getLocation().getX();
				System.out.println("iCalendar_LeftMost_Position: " + iCalendar_LeftMost_Position);

				iDay_Position = driver.findElement(byDayPosition).getLocation().getX();
				System.out.println(strDayValue + " - iDay_Position: " + iDay_Position);		

				WebElement eTime = driver.findElement(byTimePostion);
				int iTimeWidth = eTime.getSize().getWidth();	
				System.out.println(iTimeWidth);
				cf.scrollToElement(byTimePostion, "byTimePostion: " + strTimeValue, 50);		
				// Wait for Loading Spinner to disappear
				veevaMyScheduleCalls vmsc = new veevaMyScheduleCalls(scriptHelper);
				vmsc.spinner();

				cf.waitForSeconds(6);

				builder.moveToElement(eTime).moveByOffset(iDay_Position - 370, -10).contextClick().perform();

				List<WebElement> calls=driver.findElements(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li"));
				view=false;
				scheduleoption="New Calendar Entry";
				if(calls.size()>0){
					for(int a=1;a<=calls.size();a++){
						String value=driver.findElement(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li["+a+"]")).getText();
						if(scheduleoption.equals(value))
						{
							view=true;
							report.updateTestLog("Verify able to see call option", "Able to see "+ scheduleoption+" option", Status.PASS);
							break;
						}
					}
				}

			}

			if(view)
				report.updateTestLog("Verify able to see call option", "Able to see "+ scheduleoption+" option", Status.PASS);
			else{
				report.updateTestLog("Verify able to see call option", "Unable to see "+ scheduleoption+" option", Status.FAIL);
			}
		}

		else{
			report.updateTestLog("Verify able to create call with in working hrs", "Can't create call after working hrs", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void addNewCalendarEntry() throws Exception{
		cf.clickLink(objVeevaUSMySchedule.newCalendarEntry,"New Calendar Entry");
		WebDriverWait wait=new WebDriverWait(driver.getWebDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(objVeevaUSMySchedule.subjectPopUpButton));
		driver.findElement(objVeevaUSMySchedule.subjectPopUpButton).click();
		ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
		int iWindowcount = availableWindows.size();
		System.out.println("iWindowcount:" + iWindowcount);		    
		for(int i=1; i< iWindowcount; i++){
			String strHandle = availableWindows.get(i);
			driver.switchTo().window(strHandle);
			String strTmpPageTitle = cf.getSFPageTitle();
			System.out.println(i + " PageTitle: " + strTmpPageTitle);
			if((strTmpPageTitle.toLowerCase().trim()).startsWith("combobox")){    	
				report.updateTestLog("Verify switch page", "Switching to window is successful", Status.SCREENSHOT);
				break;
			}
			//			else{
			//				cf.switchToParentFrame();
			//				cf.switchToPage("Calendar: New SFDC Event ~ Salesforce - Unlimited Edition");
			//				wait.until(ExpectedConditions.visibilityOfElementLocated(objVeevaUSMySchedule.subjectPopUpButton));
			//				driver.findElement(objVeevaUSMySchedule.subjectPopUpButton).click();
			//			}
		} 
		//cf.switchToPage("");
		String strSubject = null;
		try{
			strSubject=cf.getData(objVeevaUSMySchedule.subjectType, "Subject", "text");
			driver.findElement(objVeevaUSMySchedule.subjectType).click();
		}catch(Exception e){
			cf.switchToParentFrame();
		}
		cf.switchToParentFrame();
		cf.switchToPage("Calendar: New SFDC Event ~ Salesforce - Unlimited Edition");
		//strSubject=cf.getSFData("Subject", "textbox", "text");
		String strStartTime="10:00 AM";
		String strStartDate=cf.getData(objVeevaUSMySchedule.eventStartDate, "Event Start Date", "value");
		cf.setData(objVeevaUSMySchedule.eventStartTime, "Event Start Time", strStartTime);
		String strEndTime="11:00 AM";
		cf.setData(objVeevaUSMySchedule.eventEndTime, "Event End Time", strEndTime);
		String strScheduleDateAndTime=strStartDate + ", " +strStartTime+" - " + strEndTime;
		dataTable.putData("MyScheduleCalls", "ScheduledDateAndTime", strScheduleDateAndTime);
		dataTable.putData("MyScheduleCalls", "Subject", strSubject);
		cf.clickSFButton("Save");
	}

	public void verifyCalendarEntry() throws Exception{
		cf.clickLink(By.xpath("//a[.='My Schedule']"), "My schedule is clicked");
		//		String endtime = null;
		cf.switchToFrame(ObjVeevaEmail.sframe);
		cf.clickLink(ObjVeevaEmail.weekView, "Week View");
		cf.waitForSeconds(3);
//		Veeva_TOT vtot = new Veeva_TOT(scriptHelper);
		String strSubject=dataTable.getData("MyScheduleCalls", "Subject");
		String scheduleddateandtime=dataTable.getData("MyScheduleCalls", "ScheduledDateAndTime");
		List<WebElement> callTOT=driver.findElements(By.xpath("//span[@class='fc-event-title-name']"));
		for(int i = 0; i < callTOT.size(); i++){
			String recordedcall=callTOT.get(i).getText();
			if(recordedcall.equals(strSubject)){
				By call=By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']//div[@class='fc-agenda-body']/div/div["+(i+1)+"]//span[@class='fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);
				String calltime=driver.findElement(By.xpath("//div[@class='vodtooltip' and contains(@style,'display: block;')]//tbody/tr[2]/td[1]")).getText();
				System.out.println(scheduleddateandtime);
				System.out.println(calltime);
				int size=driver.findElements(By.xpath("//div[@class='vodtooltip' and contains(@style,'display: block;')]//tbody/tr")).size();
				System.out.println(size);
				if(scheduleddateandtime.equals(calltime)){
					report.updateTestLog("Verify Calendar Entry", "Calendar Entry verified successfully",Status.PASS);
					break;
				}
				else if(i==callTOT.size())
					report.updateTestLog("Verify Calendar Entry", "Calendar Entry is not scheduled with expected date and time", Status.FAIL);
			}
			else if(i==callTOT.size())
				report.updateTestLog("Verify Calendar Entry", "No calendar entry found with subject : " +strSubject, Status.FAIL);
		}
	}
	
	
}


