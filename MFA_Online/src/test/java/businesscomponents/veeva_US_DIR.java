package businesscomponents;


import supportlibraries.*;

import com.mfa.framework.Status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

public class veeva_US_DIR  extends ReusableLibrary{

	//private ConvenienceFunctions cf = new ConvenienceFunctions(driver, report, dataTable);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	public veeva_US_DIR(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Navigating to DIR Linklet and click on DIR Button.
	 * @throws Exception
	 */
	public void newDIR() throws Exception{

		cf.clickSFLinkLet("Data Investigation Requests");
		cf.clickSFButton("New Data Investigation Request","button");
		if(cf.isElementVisible(By.name("p3"), "New Data Investigation Request"))
			report.updateTestLog("Verify able to navigate to New Data Investigation Request Record Type", "Able to navigate to New Data Investigation Request Record Type", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to New Data Investigation Request Record Type", "Unable to navigate to New Data Investigation Request Record Type.", Status.FAIL);

	}
	/**
	 * Validate the Record Types for DIR.
	 * @throws Exception
	 */
	public void validateRecordTypesDIR() throws Exception{
		Boolean validate=false;
		//	ArrayList<String> values=null;
		if(cf.isElementVisible(By.id("p3"), "Record Type")){
			report.updateTestLog("Verify able to see Record Type dropdown", "Able to see Record Type dropdown.", Status.PASS);
			List<String> optionsCall=new ArrayList<String>(Arrays.asList("Address Verification","Follow-Up on DCR and DIR","Merge Account","New External Identifier","Request to Change Specialty","Sampling Investigation"));
			for(int i=0;i<optionsCall.size();i++){
				String option=optionsCall.get(i).toString();
				WebElement optionele=driver.findElement(By.xpath("//select[@id='p3']//option[.='"+option+"']"));
				if(cf.isElementPresent(optionele, "Option")){
					System.out.println(option);
					validate=true;
				}
				else{
					break;
				}
			}

			if(validate == true)
				report.updateTestLog("Verify able to see Record Types under Record Type Dropdown.", "Able to see Record Types under Record Type Dropdown.", Status.PASS);
			else
				report.updateTestLog("Verify able to see Record Types under Record Type Dropdown.", "Unable to see RecordTypes under Record Type Dropdown.", Status.FAIL);
		}
	}

	/**
	 * This method is used to select record type for DIR
	 * @throws Exception
	 */
	public void selectDIRRecordType() throws Exception{
		String strRecordType=dataTable.getData("DataInvestigationRequest", "RecordType");
		cf.selectData(By.xpath("//div[@class='pbSubsection']//tr//*[normalize-space(text())='Record Type of new record']/following::td[1]//select"), "DIR Record Type",strRecordType);
		cf.clickSFButton("Continue","button");
		if(cf.isElementVisible(By.xpath("//h2[.=' New Data Investigation Request']"),"New Data Investigation Request" ))
			report.updateTestLog("Verify able to navigate to New Data Investigation Request.", "Able to navigate to New Data Investigation Request.", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to New Data Investigation Request.", "Unable to navigate to New Data Investigation Request.", Status.FAIL);

	}

	/**
	 * Fill the values for DIR Address Verification
	 * @throws Exception 
	 */
	public void selectaddressverificationDIR() throws Exception{
		String address=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Address (non-verified)", "lookupimage"), "Address","");
		System.out.println(address);
		if(address.trim()!=""){
			report.updateTestLog("Select Address", address +"' is selected", Status.PASS);
			//dataTable.putData("DataInvestigationRequest", "Address", address);
		}
		else
			report.updateTestLog("Verification of Address", "No Address selected.", Status.FAIL);
	}

	/**
	 * Fill the values for DIR Follow Up DCR Verification
	 * @throws Exception 
	 */
	public void selectDCRverificationDIR() throws Exception{
		String DCR=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Data Change Request", "lookupimage"), "Address","");
		System.out.println(DCR);
		if(DCR.trim()!=""){
			report.updateTestLog("Select DCR", DCR +"' is selected", Status.PASS);
			//dataTable.putData("DataInvestigationRequest", "Address", address);
		}
		else
			report.updateTestLog("Verification of DCR", "No DCR selected.", Status.FAIL);
	}

	/**
	 * Fill the values for DIR for IC Address Change.
	 * @throws Exception 
	 */
	public void selectICAddressDIR() throws Exception{
		String address=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "New IC Address", "lookupimage"), "Address","");
		System.out.println(address);
		if(address.trim()!=""){
			report.updateTestLog("Select  IC Address", address +"' is selected", Status.PASS);
			//dataTable.putData("DataInvestigationRequest", "Address", address);
		}
		else
			report.updateTestLog("Verification of IC Address", "No IC Address selected.", Status.FAIL);
	}

	/***
	 * DIR RelatedList Validations.
	 * @throws Exception 
	 */

	public void relatedListDIR() throws Exception{
		int rows;
		Boolean validate=false;
		cf.clickSFLinkLet("Data Investigation Requests");
		String DIRID=dataTable.getData("DataInvestigationRequest", "DIR_ID");
		String recordtype=dataTable.getData("DataInvestigationRequest", "RecordType");
		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Data Investigation Requests')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List")){
			WebElement element1=driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Data Investigation Requests')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Go to list");
			cf.waitForSeconds(6);
		}
		if(cf.isElementVisible(By.xpath("//h3[.='Data Investigation Requests']"),"Linklet")){
			rows=cf.getRowCountFromLinkLetTable("Data Investigation Requests");
			if(rows>0){
				for(int j=rows+1;j>1;j--){
					String appdrid=cf.getDataFromLinkLetTable("Data Investigation Requests", "Data Investigation Request Name", j);
					String apprecordtype=cf.getDataFromLinkLetTable("Data Investigation Requests", "Record Type", j);
					String appstatus=cf.getDataFromLinkLetTable("Data Investigation Requests", "Status", j);
					if(appstatus.equals("Submitted")&& DIRID.equals(appdrid) &&recordtype.equals(apprecordtype)){
						validate=true;
						break;

					}
				}
			}
			else
				report.updateTestLog("Verify able to see DIR's for the account in related list.", "There are no DIR's created in related list.", Status.FAIL);
		}
		else{
			rows=cf.rowscountexpanded("Data Investigation Requests");
			if(rows>0){
				cf.clickLink(By.xpath("//a[contains(@title,'Created Date')]"), "Sorted Ascending");
				if(cf.isElementVisible(By.xpath("//a[@title='Created Date - Sorted ascending']"), "Sorted Desending"))
					cf.clickLink(By.xpath("//a[@title='Created Date - Sorted ascending']"), "Sorted Ascending");
				cf.waitForSeconds(4);
				for(int j=2;j<=rows+1;j++){
					String appdrid=cf.getDataFromLinkLetTableexpanded("Data Investigation Requests", "Data Investigation Request Name", j);
					String apprecordtype=cf.getDataFromLinkLetTableexpanded("Data Investigation Requests", "Record Type", j);
					String appstatus=cf.getDataFromLinkLetTableexpanded("Data Investigation Requests", "Status", j);
					if(appstatus.equals("Submitted")&& DIRID.equals(appdrid) &&recordtype.equals(apprecordtype)){
						validate=true;
						break;

					}
				}
			}
			else
				report.updateTestLog("Verify able to see DIR's for the account in related list.", "There are no DIR's created in related list.", Status.FAIL);

		}
		if(validate == true)
			report.updateTestLog("Verify able to see Status,DIR ID and Record Type properly", "Able to see Status,DIR ID and Record Type properly", Status.PASS);
		else
			report.updateTestLog("Verify able to see Status,DIR ID and Record Type properly", "Unable to see Status,DIR ID and Record Type properly", Status.FAIL);
	}

	/**
	 * This method is used to select account in Merge Account DIR type
	 */
	public void selectAccountInMergeAccount() throws Exception{
		//String strMergeAccount=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Account Merge", "lookupimage"), "Account","");
		String strmergeAccount=dataTable.getData("DataInvestigationRequest", "MergeAccountName");
		vf.searchInLookupWindow(By.xpath("//img[@alt='Account Merge Lookup (New Window)']"), "Merge Account",strmergeAccount );

		//		if(strMergeAccount.trim()!=""){
		//			report.updateTestLog("Select Merge Account", strMergeAccount +"' is selected", Status.SCREENSHOT);
		//			dataTable.putData("DataInvestigationRequest", "MergeAccountName", strMergeAccount);
		//		}
		//		else
		//			report.updateTestLog("Verification of Merge Account", "No account selected", Status.FAIL);
	}
	/**
	 * This method is used to enter random notes information in DIR creation page
	 * @throws Exception
	 */
	public void enterNotesInformationInDIR() throws Exception{
		String strAutoGeneratedNotes="This DIR is created for automation pupose at " + new SimpleDateFormat("ddmmss").format(new Date()).toString();
		cf.setSFData("Notes", "Notes", "textarea", strAutoGeneratedNotes);
	}
	/**
	 * This method is used to click on Save in DIR Creation page
	 */
	public void clickSaveInDIRCreation() throws Exception{
		cf.clickSFButton("Save", "submit");
	}

	/**
	 * This method is used to verify the status of DIR creation
	 */
	public void verifyDIRCreatedStatus(String strStatusToVerify) throws Exception{
		cf.verifySFElementData("Data Investigation Status", "Status", "div", "text", strStatusToVerify);
	}

	/**
	 * This method is used capture DIR ID in DIR creation page
	 */
	public void captureDIRCreatedIDPage() throws Exception{
		String strDIRID = cf.getData(By.cssSelector("h2.pageDescription"), "Data Investigation Request ID", "text");
		if(!strDIRID.trim().equalsIgnoreCase("")){
			report.updateTestLog("Capture DIR ID", "Data Investigateion Request ID is captured as : '"+strDIRID +"'", Status.SCREENSHOT);
			dataTable.putData("DataInvestigationRequest", "DIR_ID", strDIRID);
		}
		else
			report.updateTestLog("Capture DIR ID", "No DIR ID available", Status.FAIL);
	}


	/**
	 * This method is used to generate Workflow in MergeAccount record type DIR creation
	 */
	public void createMergeAccountDIR() throws Exception{
		selectAccountInMergeAccount();
		enterNotesInformationInDIR();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	public void createAddressVerDIR() throws Exception{
		selectaddressverificationDIR();
		enterNotesInformationInDIR();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	public void createExternalIdentiferDIR() throws Exception{
		cf.setData(By.xpath("//label[.='SLN']/parent::td/following-sibling::td/input"), "SLN", "1234");
		cf.selectData(By.xpath("//label[.='State']/parent::td/following-sibling::td/span/select"), "State", "AS");
		enterNotesInformationInDIR();
		clickSaveInDIRCreation();
		cf.verifySFElementData("Data Investigation Request Detail", "SLN", "header labels", "text", "1234");
		cf.verifySFElementData("Data Investigation Request Detail", "State", "header labels", "text", "AS");
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	public void createFolllowDCRDIR() throws Exception{

		selectDCRverificationDIR();
		enterNotesInformationInDIR();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}
	public void createICAddressDIR() throws Exception{
		selectICAddressDIR();
		enterNotesInformationInDIR();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	/**
	 * This method is used to click on the account in DIR Submitted page
	 */
	public void clickAccountInDIRSubmittedPage() throws Exception{
		cf.waitForSeconds(3);
		cf.clickButton(cf.getSFElementXPath("Data Investigation Request Detail", "Account", "header links"), "Account Name");
	}

	/**
	 * This method is used to select SLN and state field in DIR creation page
	 */
	public void enterSamplingInvestigationInformation() throws Exception{
		String strAddress=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Address", "lookupimage"), "Address", "");
		cf.verifySFElementData("Information", "Address", "textbox", "value", strAddress);
		cf.setSFData("Information", "SLN","textbox",  Long.toString(cf.getRandomNumber(0, 100000)));
		cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "State", "select"), "State");
		enterNotesInformationInDIR();
	}

	/**
	 * This method is used to verify  message in DIR creation page
	 */
	public void verifyMessageInDIRCreation() throws Exception{
		String strMessage=cf.getData(By.cssSelector("div.pbError"), "Error Message", "text");
		if(strMessage.toLowerCase().contains("this data investigation request is not applicable for business account."))
			report.updateTestLog("Error validation for Business Account", "Error message : 'This data investigation request is not applicable for business account.' is displayed as expected", Status.PASS);
		else
			report.updateTestLog("Error validation for Business Account", "Error message : 'This data investigation request is not applicable for business account.' is not displayed", Status.FAIL);
	}

	/**
	 * This method is used to create Sampling Investigation DIR data
	 */
	public void createSamplingInvestigationDIR() throws Exception{
		enterSamplingInvestigationInformation();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	/**
	 * This method is used to navigate to DIR linklet and click new type of DIR record type
	 */
	public void createDIRRecordTypeFromMyAccounts() throws Exception{
		newDIR();
		validateRecordTypesDIR();
		selectDIRRecordType();
	}

	/**
	 * This method is used to create Request to Change Specialty DIR
	 */
	public void createRequestToChangeSpecialityDIR() throws Exception{
		enterRequestToChangeSpecialityData();
		clickSaveInDIRCreation();
		verifyDIRCreatedStatus("Submitted");
		captureDIRCreatedIDPage();
	}

	/**
	 * This method is used to enter data in Request To Change speciality DIR creation
	 */
	public void enterRequestToChangeSpecialityData() throws Exception{
		cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Information", "Requested Specialty", "select"), "Requested Speciality");
		enterNotesInformationInDIR();
	}

}


