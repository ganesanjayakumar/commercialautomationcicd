package businesscomponents;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mfa.framework.Status;

import ObjectRepository.objVeevaUSLegacyCoaching;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_US_LegacyCoachingReport extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	public Veeva_US_LegacyCoachingReport(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}


	/**
	 * This method is used for navigating to Medical Information Request linklet
	 * @author kzmg439
	 */
	public void navigateToCoachingReportsTab() throws Exception{
		vf.clickVeevaTab("Coaching Reports");
		cf.waitForSeconds(10);
	    if(cf.isElementVisible(By.xpath("//h1[contains(text(),'Coaching Reports')]"), "New Coaching Report"))
			report.updateTestLog("Verify able to navigate to Coaching Reports Tab", "Able to navigate to Coaching Reports Tab.", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to Coaching Reports tab", "Unable to navigate to Coaching Reports tab.", Status.FAIL);
	}

	/**
	 * Creating New Coaching Report.
	 * @throws Exception 
	 */
	public void newlegacyCoachingReport() throws Exception{

		String employeapp=dataTable.getData("LegacyCoachingReport", "Coaching Employee");
		cf.clickButton(By.xpath("//input[@value=' New ']"), "New Coaching Report");
		cf.waitForSeconds(5);
		//fieldLevelValidations();
		selectLegacyCoachingEmployee(employeapp);
		By statusexpath=cf.getSFElementXPath("Status", "select");
		cf.selectData(statusexpath, "Status", "In Progress");
		cf.clickLink(By.cssSelector("span.dateFormat>a"), "Review Date");
		By fieldduration=cf.getSFElementXPath("Field Ride Duration", "input");
		cf.setData(fieldduration, "Field Ride Duration", "3");
		report.updateTestLog("Verify details has filled for Information Module.", "Able to see details in Information Module.", Status.SCREENSHOT);
		By followsastrazeneca=cf.getSFElementXPath("Follows AstraZeneca policies", "select");
		By Adherestodrivingpolicy=cf.getSFElementXPath("Adheres to driving policy", "select");
		cf.selectData(followsastrazeneca, "Follows Astrazeneca Policies", "Yes");
		cf.selectData(Adherestodrivingpolicy, "Adheres to driving policy", "Yes");
		cf.clickButton(By.cssSelector("td#topButtonRow > input[name='save']"), "Save");
		if(cf.isElementVisible(By.xpath("//a[@class='linklet']//span[contains(text(),'Coaching Lines')]"), "After clicking Save")){
			report.updateTestLog("Verify able to see Coaching Lines Linket Table after saving.", "Able to see Coaching Lines Linket Table after saving.", Status.PASS);
			String coachingReportId= cf.getSFData("Coaching Report Name", "header labels", "text");
			dataTable.putData("LegacyCoachingReport", "CoachingReportName", coachingReportId);

		}else
			report.updateTestLog("Verify able to see Coaching Lines Linket Table after saving.", "Unable to see Coaching Lines Linket Table after saving.", Status.PASS);
	}

	/***
	 * Select the Employee from Look Up Search.
	 * @param EmployeeName
	 * @throws Exception
	 */
	public void selectLegacyCoachingEmployee(String EmployeeName) throws Exception{
		String Employee=vf.getLookupDataInWindow(cf.getSFElementXPath("Information", "Representative/Employee", "lookupimage"), "Employee",EmployeeName);
		System.out.println(Employee);
		if(Employee.trim()!=""){
			report.updateTestLog("Select  Employee", Employee +"' is selected", Status.PASS);
		}
		else
			report.updateTestLog("Verification of Employee", "No Employee selected.", Status.FAIL);
	}
	/**
	 * This method is used to click on Edit button
	 */
	public void clickEditButtonInCoachingReport() throws Exception{
		cf.clickSFButton("Edit", "button");
	}


	/**
	 * This method is used to do field level validations in Edit Coaching report page
	 */
	public void editCoachingReport() throws Exception{

		cf.verifySFElementData("Information" , "Coaching Report Name", "text", "", "");
		cf.verifySFElementData("Information" , "Author", "text", "", "");
		cf.verifySFElementData("Information" , "Status", "text", "text", "Submitted");
		cf.verifySFElementData("Information" , "Manager", "text", "", "");
		cf.verifySFElementData("Information" , "Representative/Employee", "text", "", "");

		cf.verifySFElementData("Evaluation Scale (Refer to the 1AZSF Behavioral Roadmap)" , "Mastery Behaviors", "text", "", "");
		cf.verifySFElementData("Evaluation Scale (Refer to the 1AZSF Behavioral Roadmap)" , "Proficient Behaviors", "text",  "", "");
		cf.verifySFElementData("Evaluation Scale (Refer to the 1AZSF Behavioral Roadmap)" , "Basic Behaviors", "text","","");
		cf.verifySFElementData("Evaluation Scale (Refer to the 1AZSF Behavioral Roadmap)" , "Developing Behaviors", "text","","");
		cf.verifySFElementData("Evaluation Scale (Refer to the 1AZSF Behavioral Roadmap)" , "Opportunity Behaviors", "text","","");
		int iNotHidden=0;
		List<WebElement> inputElementList=driver.findElements(By.cssSelector(".detailList td[class*='data'][class*='Col'] > input"));
		for(WebElement inputTag : inputElementList){
			if (!inputTag.getAttribute("type").equalsIgnoreCase("hidden") && !inputTag.getAttribute("type").equalsIgnoreCase("checkbox")) iNotHidden++;
			if(inputTag.getAttribute("type").equalsIgnoreCase("checkbox")	 && inputTag.getAttribute("value").equalsIgnoreCase("0") )	iNotHidden++;
		}
		if(iNotHidden>0)
			report.updateTestLog("Field verification in Coaching Report", "There are " +iNotHidden +" fields which are found Editable in Coaching Report page as Employee", Status.FAIL);
		else
			report.updateTestLog("Field verification in Coaching Report", "All fields are found not editable in Coaching Report page as Employee", Status.PASS);
		/*/
		cf.verifySFElementData("Pre-Call Plan" , "Pre-Call Planning Evaluation", "text","","");

		cf.verifySFElementData("Engage" , "Engage Evaluation", "text","","");
		cf.verifySFElementData("Discover" , "Discover Evaluation", "text","","");

		cf.verifySFElementData("Close" , "Close Evaluation", "text","","");
		cf.verifySFElementData("Close" , "Managed Market Pull-Through Evaluation", "text","","");

		cf.verifySFElementData("Post-Call Assessment" , "Post-Call Assessment Evaluation", "text", "", "");

		cf.verifySFElementData("Compliance" , "Follows AstraZeneca policies", "text","","");
		cf.verifySFElementData("Compliance" , "Adheres to driving policy", "text","","");

		cf.verifySFElementData("Performance Metrics" , "Performance Metrics Evaluation", "text", "", "");
		cf.verifySFElementData("Performance Metrics" , "Performance Assessment", "text", "", "");

		cf.verifySFElementData("Performance Metrics" , "Performance Assessment", "text", "", "");

		cf.verifySFElementData("Development Summary" , "Field Ride Summary & Next Steps", "text", "", "");
		cf.verifySFElementData("Development Summary" , "Manager comments on Skill Development", "text", "", "");
		cf.verifySFElementData("Development Summary" , "Field Ride Summary & Next Steps", "text","","");

		cf.verifySFElementData("Acknowledgement" , "I confirm that I reviewed this form", "text","","");
		//*/
	}

	/**
	 * This method is used to click on Save button in Edit Coaching report page
	 */
	public void clickSaveInCoachingReport() throws Exception{
		cf.clickSFButton("Save", "submit");
	}
	/**
	 * This method is used to view the coaching report
	 */
	public void selectCoachingReportForSalesRep() throws Exception{
		cf.selectData(objVeevaUSLegacyCoaching.selectCoachingReportView, "Coaching Report View", "All Coaching Reports");
		cf.clickButton(objVeevaUSLegacyCoaching.goButton, "Go View Filter");
		String strCoachingReportName=dataTable.getData("LegacyCoachingReport", "CoachingReportName");
		cf.clickButton(By.xpath("//table[@class='x-grid3-row-table']//tr//td//span[contains(text(),'"+strCoachingReportName.trim()+"')]//parent::a"), strCoachingReportName);
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'Coaching Report')]"), "Open Change Request"))
			report.updateTestLog("Verify able to navigate to Coaching Report", "Able to navigate to Coaching Reports.", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to Coaching Report", "Unable to navigate to Coaching Reports.", Status.FAIL);
	}

	/**
	 * Scroll to Coaching Lines Related List
	 * and Click New Button and Validate the Error without Evaluate Value.
	 * @throws Exception
	 */
	public void coachingLines() throws Exception{

		cf.clickSFLinkLet("Coaching Lines");
		cf.clickButton(By.xpath("//input[@value='New Coaching Line']"), "New Coaching Line");
		if(cf.isElementVisible(By.xpath("//h2[contains(text(),' New Coaching Line')]"), " New Coaching Line")){
			report.updateTestLog("Verify able to navigate to  New Coaching Line", "Able to navigate to  New Coaching Line", Status.PASS);
			cf.clickButton(By.cssSelector("td#topButtonRow > input[name='save']"), "Save");
			String actual=cf.getData(By.id("errorDiv_ep"), "Evaluation Error", "text");
			String expected="Error: Invalid Data."+"\n"+"Review all error messages below to correct your data.";
			cf.verifyActualExpectedData("Error", actual, expected);
			By evaluate=cf.getSFElementXPath("Evaluation Scale", "select");
			cf.selectData(evaluate, "Evaluate Error", "Yes");
			report.updateTestLog("Verify able to see the added details", "Able to see Added Details", Status.SCREENSHOT);
			cf.clickButton(By.cssSelector("td#topButtonRow > input[name='save']"), "Save");
			if(cf.isElementVisible(By.cssSelector("td#topButtonRow > input[name='edit']"), "Coaching Line Edit Button"))
				report.updateTestLog("Verify able to see Edit Button in Coaching Line Detail", "Able to see to Edit Button in Coaching Line Detail.", Status.PASS);
			else
				report.updateTestLog("Verify able to see  Edit Button in Coaching Line Detail", "Unable to see to Edit Button in Coaching Line Detail", Status.FAIL);

		}else
			report.updateTestLog("Verify able to naviagte to  New Coaching Line", "Unable to navigate to  New Coaching Line", Status.FAIL);

	}

	/**
	 * Field Level Validations.
	 * @throws Exception 
	 */
	public void fieldLevelValidations() throws Exception{

		//cf.verifySFElementData("Information", "Author", "labeltext", "text", "Author");
		//cf.verifySFElementData("Information", "Manager", "labeltext", "text", "Manager");
		//cf.verifySFElementData("Information", "Representative/Employee", "lookup", "", "");
		//cf.verifySFElementData("Information", "Status", "select", "text", "In Progress");
		cf.verifySFElementData("Information", "Review Date", "input", "text", "");
	}

	public void editchangestatus() throws Exception{
		By statusexpath=cf.getSFElementXPath("Status", "select");
		String Reportid=dataTable.getData("LegacyCoachingReport", "CoachingReportName");
		cf.clickLink(By.linkText(""+Reportid+""), "Coaching Report");
		cf.clickButton(By.cssSelector("td#topButtonRow>input[name='edit']"), "Edit Button");
		cf.selectData(statusexpath, "Status", "Submitted");
		cf.clickButton(By.cssSelector("td#topButtonRow > input[name='save']"), "Save");
		if(cf.isElementVisible(By.xpath("//a[@class='linklet']//span[contains(text(),'Coaching Lines')]"), "After clicking Save")){
			String status=cf.getSFData("Status", "text", "text");
			if(status.equals("Submitted")){
			report.updateTestLog("Verify able to see Coaching Lines Linket Table after saving with submitted status.", "Able to see Coaching Lines Linket Table after saving with submitted status.", Status.PASS);
			String coachingReportId= cf.getSFData("Coaching Report Name", "header labels", "text");
			dataTable.putData("LegacyCoachingReport", "CoachingReportName", coachingReportId);

		}
			else
				report.updateTestLog("Verify able to see Submitted status for Coaching Report.", "Unable to see the Submitted status for Coaching Report.", Status.FAIL);
			
		}else
			report.updateTestLog("Verify able to see Coaching Lines Linket Table after saving.", "Unable to see Coaching Lines Linket Table after saving.", Status.PASS);
	}


}//End of Class
