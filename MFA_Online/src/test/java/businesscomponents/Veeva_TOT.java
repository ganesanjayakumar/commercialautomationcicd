package businesscomponents;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import supportlibraries.*;
import com.mfa.framework.Status;
import ObjectRepository.*;
//import org.openqa.jetty.html.Select;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Veeva_TOT extends VeevaFunctions
{

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	private veevaMyScheduleCalls sc = new veevaMyScheduleCalls(scriptHelper);

	public Veeva_TOT(ScriptHelper scriptHelper)
	{
		super(scriptHelper);

	}

	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
	String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda']";
	static int iDayRandom;
	Date dNow = new Date();
	Actions builder = new Actions(driver.getWebDriver());
	static String dateselection;
	static String reasonmsl;

	public void newTimeOffTerritory() throws Exception
	{
		boolean schedulecalls = false;
		cf.clickLink(ObjVeevaEmail.weekView, "Week View");
		WebElement ele = driver.findElement(ObjVeevaEmail.workweek);
		cf.clickElement(ele, "Clicked on work week Radio Button");
		cf.waitForSeconds(3);
		//String strCalendarXPath = "//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']";
		By byDay = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt]");
		//		By byTime = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr");

		By byCalendar_LeftMost = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[1]");
		int iDayCount = driver.findElements(byDay).size();
		//	int itimeCount = driver.findElements(byTime).size();
		iDayRandom = 0;
		SimpleDateFormat week = new SimpleDateFormat("EEEE");
		String weekft = week.format(dNow).toString();
		for (int i = 1; i <= iDayCount; i++)
		{

			String weekname = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[2]")).getText();
			if (weekft.contains(weekname))
			{//changed
				WebElement delete = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[3]/div"));
				if (cf.isElementVisible(delete))
				{
					cf.clickElement(delete, "Delete all planned calls");
					try
					{
						wait.until(ExpectedConditions.alertIsPresent());
						driver.switchTo().alert().accept();
						System.out.println("Alert was present and accepted");
						schedulecalls = true;
					}
					catch (Exception e)
					{
						System.out.println("Alert was not present");
						schedulecalls = true;
					}

				}
				iDayRandom = i;
				break;
			}

		}
		String time = new SimpleDateFormat("HH").format(dNow).toString();
		int timen = Integer.parseInt(time);
		if (schedulecalls == true)
		{
			if (timen <= 18)
			{
				int iTimeRandom = cf.getRandomNumber((timen * 2), 32);
				By byDayPosition = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]");
				By byTimePostion = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/td");

				By byDayValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]//tr");
				String strDayValue = cf.getData(byDayValue, "Day Value", "text");
				System.out.println("strDayValue: " + strDayValue);
				By byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/th");
				String strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
				if (strTimeValue.equals(""))
				{
					byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + (iTimeRandom - 1) + "]/th");
					strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
					strTimeValue = strTimeValue.replace("00", "30");
				}

				///Find x offset Width to move
				int iCalendar_LeftMost_Position = driver.findElement(byCalendar_LeftMost).getLocation().getX();
				System.out.println("iCalendar_LeftMost_Position: " + iCalendar_LeftMost_Position);

				int iDay_Position = driver.findElement(byDayPosition).getLocation().getX();
				System.out.println(strDayValue + " - iDay_Position: " + iDay_Position);
				///////////////////////////////////////////////////////////////////////

				WebElement eTime = driver.findElement(byTimePostion);
				int iTimeWidth = eTime.getSize().getWidth();
				System.out.println(iTimeWidth);
				cf.scrollToElement(byTimePostion, "byTimePostion: " + strTimeValue, 50);

				builder.moveToElement(eTime).moveByOffset(iDay_Position - 370, -10).contextClick().perform();

				List<WebElement> calls = driver.findElements(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li"));
				Boolean view = false;
				String scheduleoption = "New Time Off Territory";
				for (int a = 1; a <= calls.size(); a++)
				{
					String value = driver.findElement(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[" + a + "]")).getText();
					if (scheduleoption.equals(value))
					{
						view = true;
						break;
					}

				}
				if (view == true)
					report.updateTestLog("Verify able to see call option", "Able to see " + scheduleoption + " option", Status.PASS);
				else
				{
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{

				report.updateTestLog("Verify able to create TOT after working hrs", "Unable to create TOT after working hrs", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void verify_working() throws Exception
	{
		boolean schedulecalls = false;
		cf.clickLink(ObjVeevaEmail.weekView, "Week View");
		WebElement ele = driver.findElement(ObjVeevaEmail.workweek);

		cf.waitForSeconds(3);

		By byDay = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt]");

		By byCalendar_LeftMost = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[1]");
		int iDayCount = driver.findElements(byDay).size();
		iDayRandom = 0;
		SimpleDateFormat week = new SimpleDateFormat("EEEE");
		String weekft = week.format(dNow).toString();
		for (int i = 1; i <= iDayCount; i++)
		{

			String weekname = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[2]")).getText();
			if (weekft.contains(weekname))
			{
				WebElement delete = driver.findElement(By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[" + (i + 1) + "]//td[3]/div"));

				iDayRandom = i;
				break;
			}

		}
		String time = new SimpleDateFormat("HH").format(dNow).toString();
		int timen = Integer.parseInt(time);
		if (schedulecalls == true)
		{
			if (timen <= 18)
			{
				int iTimeRandom = cf.getRandomNumber((timen * 2), 32);
				By byDayPosition = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]");
				By byTimePostion = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/td");

				By byDayValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-head']//tr[@class='fc-first']/th[@colcnt][" + iDayRandom + "]//tr");
				String strDayValue = cf.getData(byDayValue, "Day Value", "text");
				System.out.println("strDayValue: " + strDayValue);
				By byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + iTimeRandom + "]/th");
				String strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
				if (strTimeValue.equals(""))
				{
					byTimeValue = By.xpath(strCalendarXPath + "/div[@class='fc-agenda-body']//tr[" + (iTimeRandom - 1) + "]/th");
					strTimeValue = cf.getData(byTimeValue, "Time Value", "text");
					strTimeValue = strTimeValue.replace("00", "30");
				}

				///Find x offset Width to move
				int iCalendar_LeftMost_Position = driver.findElement(byCalendar_LeftMost).getLocation().getX();
				System.out.println("iCalendar_LeftMost_Position: " + iCalendar_LeftMost_Position);

				int iDay_Position = driver.findElement(byDayPosition).getLocation().getX();
				System.out.println(strDayValue + " - iDay_Position: " + iDay_Position);
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				WebElement eTime = driver.findElement(byTimePostion);
				int iTimeWidth = eTime.getSize().getWidth();
				System.out.println(iTimeWidth);
				cf.scrollToElement(byTimePostion, "byTimePostion: " + strTimeValue, 50);

				builder.moveToElement(eTime).moveByOffset(iDay_Position - 370, -10).contextClick().perform();

				List<WebElement> calls = driver.findElements(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li"));
				Boolean view = false;
				String scheduleoption = "New Time Off Territory";
				for (int a = 1; a <= calls.size(); a++)
				{
					String value = driver.findElement(By.xpath("//div[@class='fg-menu-container ui-widget ui-widget-content ui-corner-all' and @style='width: 150px; bottom: auto; top: 10px; right: auto; left: 0px;']//li[" + a + "]")).getText();
					if (scheduleoption.equals(value))
					{
						view = true;
						break;
					}

				}
				if (view == true)
					report.updateTestLog("Verify able to see call option", "Able to see " + scheduleoption + " option", Status.PASS);
				else
				{
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{

				report.updateTestLog("Verify able to create TOT after working hrs", "Unable to create TOT after working hrs", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void verifyTOTPage() throws Exception
	{
		cf.clickLink(ObjVeevaEmail.newtot, " New Time Off Territory");
		if (cf.isElementVisible(By.xpath("//h2[.=' New Time Off Territory']"), " New Time Off Territory"))
		{
			report.updateTestLog("Verify able to see  New Time Off Territory Page", "Able to see  New Time Off Territory Page", Status.PASS);
		}
		else
		{
			report.updateTestLog("Verify able to see  New Time Off Territory Page", "Unable to see  New Time Off Territory Page", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void creatingTOT() throws Exception
	{
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");
		if (cf.isElementVisible(ObjVeevaEmail.reasonRequiredInputsales, "Reason field as mandatory") || cf.isElementVisible(ObjVeevaEmail.reasonRequiredmsl, "Reason field as mandatory"))
		{
			report.updateTestLog("Verify able to see Reason field as mandatory", "Able to see Reason field as Mandatory", Status.PASS);
			selectReasonfromLookUp();
			if (profile.equals("Commercial"))
			{
				if (!(locale.equals("JP") || locale.equals("CN")))
				{
					defaultStatus();
				}
			}
			dateSelectionTimeVerification();
			hrsselandsave();

		}
		else
		{
			report.updateTestLog("Verify able to see Reason field as mandatory", "Unable to see Reason field as Mandatory", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void selectReasonfromLookUp() throws Exception
	{
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");
		if (profile.equals("Commercial") && !(locale.equals("JP") || locale.equals("CN")))
		{
			//if((profile.equals("Commercial") && (!locale.equals("JP")))||(profile.equals("Commercial") && (!locale.equals("CN")))){
			if (cf.isElementVisible(By.xpath("//img[@class='lookupIcon']"), "Look Up"))
			{
				driver.findElement(By.xpath("//img[@class='lookupIcon']")).click();
				cf.waitForSeconds(5);
				String parentwindow = driver.getWebDriver().getWindowHandle();
				Set<String> windows = driver.getWebDriver().getWindowHandles();
				for (String childwindow : windows)
				{
					if (!(parentwindow.equals(childwindow)))
					{
						driver.switchTo().window(childwindow);
						String title = driver.getTitle();
						System.out.println(title);
						if (title.equals("Search ~ Salesforce - Unlimited Edition"))
						{
							driver.switchTo().frame("resultsFrame");
							if (cf.isElementVisible(By.xpath("//table[@class='list']//tr[2]//a"), "Reason"))
							{
								report.updateTestLog("Verify able to see the list of Reasons", "Able to see List of reasons", Status.PASS);
								WebElement reason = driver.findElement(By.xpath("//table[@class='list']//tr[2]//a"));
								cf.clickElement(reason, "Reason");
								driver.switchTo().window(parentwindow);
								break;
							}
							else
							{
								report.updateTestLog("Verify able to see the list of Reasons", "Unable to see List of reasons", Status.FAIL);
							}
						}
						else
						{
							report.updateTestLog("Verify able to switch to Reason window", "Unable to switch to Reason Window", Status.FAIL);
							//frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			else
			{
				report.updateTestLog("Verify able to see lookup icon", "Unable to see look up icon", Status.FAIL);
				//frameworkparameters.setStopExecution(true);

			}
		}
		else
		{//For JP and China

			if (driver.findElements(By.xpath("//label[contains(text(),'Reason')]/parent::td/parent::tr//select/option")).size() > 0)
			{
				reasonmsl = driver.findElement(By.xpath("//label[contains(text(),'Reason')]/parent::td/parent::tr//select/option[6]")).getText();
				cf.selectData(ObjVeevaEmail.reasonRequiredmsl, "Reason", reasonmsl);
			}
			else
			{
				report.updateTestLog("Verify able to see thee reasons in the dropdown", "Unable to see reasons in the dropdown", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void defaultStatus()
	{
		WebElement sta = driver.findElement(ObjVeevaEmail.defaultstatus);
		Select s = new Select(sta);
		String status = s.getFirstSelectedOption().getText();
		if (status.endsWith("Planned"))
			report.updateTestLog("Verify able to see default status as Planned", "Able to see default status as Planned", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to see default status as Planned", "Unable to see default status as Planned", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void dateSelectionTimeVerification() throws Exception
	{
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");
		dateselection = driver.findElement(By.xpath("//span[@class='dateFormat']/a")).getText();
		cf.clickLink(By.xpath("//span[@class='dateFormat']/a"), "Date selected");
		if (!((locale.equals("JP") && profile.equals("Commercial")) || locale.equals("CN")))
		{
			timeverification();
		}

	}

	public void timeverification() throws Exception
	{
		Boolean timez = false;
		String profile = dataTable.getData("Login", "Profile");
		List<String> time = new ArrayList<String>(Arrays.asList("AM Off", "PM Off", "All Day"));
		for (int t = 0; t < time.size(); t++)
		{
			String Timezone = time.get(t);
			cf.selectData(ObjVeevaEmail.selectTime, "Time", Timezone);
			if (Timezone.equals("Hourly"))
			{
				List<String> disa = new ArrayList<String>(Arrays.asList("Hours off", "Start Time"));
				for (int d = 0; d <= disa.size(); d++)
				{
					WebElement el = driver.findElement(By.xpath("//label[.='" + (disa.get(d)) + "']/parent::td//following-sibling::td//select"));
					if (el.getAttribute("disabled").equals("false"))
						timez = true;
				}
			}
			else
			{
				if (profile.equals("Medical"))
				{
					timez = true;
				}
				else
				{
					List<String> disa = new ArrayList<String>(Arrays.asList("Hours off", "Start Time"));
					for (int d = 0; d < disa.size(); d++)
					{
						WebElement el = driver.findElement(By.xpath("//label[.='" + (disa.get(d)) + "']/parent::td//following-sibling::td//select"));
						if (el.getAttribute("disabled").equals("true"))
							timez = true;
					}
				}
			}
		}

		if (timez == true)
		{
			report.updateTestLog("Verify able to validate disabled fields for all Time except Hourly", "Able to validate disabled fields for all Time except Hourly", Status.PASS);
		}
		else
		{
			report.updateTestLog("Verify able to validate disabled fields for all Time except Hourly", "Unable to validate disabled fields for all Time except Hourly", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	static String hrsno;
	static String sttime;
	static String scheduleddateandtime;
	static WebElement mouseovercall;

	public void hrsselandsave() throws Exception
	{
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");
		if (locale.equals("CN") && profile.equals("Commercial"))
		{
			cf.selectData(By.xpath("//label[contains(text(),'Time')]/parent::td[@class='labelCol last requiredInput']//following-sibling::td//select"), "Time", "All Day");
		}
		else if (locale.equals("CN") && profile.equals("Medical"))
		{
			cf.selectData(By.xpath("//label[.='Time']/parent::td/following-sibling::td//select"), "Time Zone", "All Day");
		}
		else if (!((locale.equals("JP") && profile.equals("Commercial"))))
		{
			cf.selectData(ObjVeevaEmail.selectTime, "Time", "Hourly");

			if (profile.equalsIgnoreCase("Commercial"))
			{
				hrsno = driver.findElement(By.xpath("//label[.='Hours off']/parent::td//following-sibling::td//select//option[2]")).getText();
				cf.selectData(ObjVeevaEmail.hrsoff, "Select Hrs off", hrsno);
			}

			sttime = driver.findElement(By.xpath("//label[.='Start Time']/parent::td//following-sibling::td//select//option[4]")).getText();
			cf.selectData(ObjVeevaEmail.strttime, "Start Time", sttime);
			report.updateTestLog("Verify able to see fields in Enabled mode for Hourly", "Able to see fields in Enabled Mode for Hourly", Status.SCREENSHOT);
		}
		else
		{
			cf.selectData(By.xpath("//label[contains(text(),'Time')]/parent::td[@class='labelCol requiredInput']//following-sibling::td//select"), "Time", "All Day");
		}
		WebElement save = driver.findElement(ObjVeevaEmail.saveTOT);
		cf.clickElement(save, "Save");
		deleteandTOTid();
	}

	public void verifyTOT() throws Exception
	{
		String strDataSheet = "Login";
		int mins = 0;
		String locale = dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");

		if (cf.isElementPresent((By.xpath("//a[.='My Schedule']")), "My Schedule"))
			cf.clickLink(By.xpath("//a[.='My Schedule']"), "My schedule is clicked");
		else
			clickVeevaTab("My Schedule");
		String endtime = null;

		cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));

		cf.clickLink(ObjVeevaEmail.weekView, "Week View");
		sc.spinner();
		cf.waitForSeconds(8);

		if (!((locale.equals("JP") && profile.equals("Commercial")) || locale.equals("CN") || profile.equals("Medical")))
		{
			mins = Integer.parseInt(hrsno) * 60;
		}
		if (locale.equals("IE") && profile.equals("Commercial") || (locale.equals("CA") || locale.equals("EU2") || locale.equals("RU")))
		{
			if (sttime.contains("AM") || sttime.contains("PM"))
			{
				endtime = vf.addMinsToDate12(sttime, mins);
				scheduleddateandtime = dateselection + ", " + sttime + " - " + endtime;
			}
		}
		else if ((locale.equals("JP") && profile.equals("Commercial")) || locale.equals("CN"))
		{
			scheduleddateandtime = dateselection;
		}
		else if ((locale.equals("JP") && profile.equals("Medical")))
		{
			String starttime = timeconvertToRailwayTimeru(sttime);
			endtime = addMinsToDateru(sttime, mins);
			scheduleddateandtime = dateselection + ", " + starttime + " - " + endtime;
		}
		else
		{
			String starttime = vf.timeconvertToRailwayTime(sttime);
			endtime = vf.addMinsToDate(sttime, mins);
			scheduleddateandtime = dateselection + ", " + starttime + " - " + endtime;
		}

		System.out.println("verifytot schedule date: " + scheduleddateandtime);

		if (!((locale.equals("JP") && profile.equals("Commercial")) || locale.equals("CN")))
		{
			System.out.println("inside mouseoverPlannedTOT");
			mouseoverPlannedTOT();
		}
		else
		{
			mouseoverPlannedTOTimmersive();
		}
	}

	public void mouseoverPlannedTOT() throws Exception
	{
		Boolean mouseoverdraganddrop = false;
		String reasonplanned, calltime = "";
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");

		List<WebElement> callTOT;

		if (profile.equalsIgnoreCase("Medical"))
		{
			System.out.println(reasonmsl);
			callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title-name' and text()='" + reasonmsl + "']"));
		}
		else
			callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title-name' and text()='Time Off Territory']"));

		int tot = callTOT.size();
		System.out.println(tot);
		for (int d = tot - 1; d >= 0; d--)
		{
			System.out.println(d);
			String hcpstatus = null;
			String recordedcall = callTOT.get(d).getText();

			if (profile.equals("Commercial"))
			{
				reasonplanned = "Time Off Territory";
			}
			else
			{
				reasonplanned = reasonmsl;
				//reasonplanned = "Administration";
			}

			if (recordedcall.equals(reasonplanned))
			{
				//By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[contains(@class,'Time_Off_Territory_vod__c')][" + (d + 1) + "]");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);

				if (profile.equalsIgnoreCase("Medical"))
					calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='" + reasonmsl + "']/parent::tr/following-sibling::tr/td[@class='vodtooltipwhen'])[" + (tot - d) + "]")).getText();
				else
					calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='Time Off Territory']/parent::tr/following-sibling::tr/td[@class='vodtooltipwhen'])[" + (tot - d) + "]")).getText();

				//calltime = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[2]/td[@class='vodtooltipwhen']")).getText();
				calltime = calltime.replace("AM", "");
				String[] att = calltime.split(" - ");
				calltime = att[0];

				if (locale.equalsIgnoreCase("IW") || (locale.equalsIgnoreCase("ANZ")))
				{
					calltime = calltime.replace(" ", "");
					String[] dateTime = calltime.split(",");
					String[] d1 = dateTime[1].split(":");
					calltime = d1[0].substring(0);

					if (calltime.contains("0"))
						calltime = dateTime[0] + ", " + d1[0] + d1[1];
					else
						calltime = dateTime[0] + ", " + d1[0].replace(d1[0].substring(0), "0" + d1[0].substring(0)) + ":" + d1[1];

				}
				else if (locale.equalsIgnoreCase("EU2"))
				{
					calltime = calltime.replace(" ", "");
					String[] dateTime = calltime.split(",");
					String[] d1 = dateTime[1].split(":");
					calltime = d1[0].substring(0);

					if (calltime.contains("0"))
						calltime = dateTime[0] + ", " + d1[0].replace(d1[0].substring(0), "") + d1[0].substring(1) + ":" + d1[1];
					else
						calltime = dateTime[0] + ", " + d1[0] + d1[1];

				}

				System.out.println("calltime: " + calltime);

				if (profile.equalsIgnoreCase("Medical"))
					hcpstatus = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='" + reasonmsl + "']/parent::tr/following-sibling::tr[2]/td/div)[" + (tot - d) + "]")).getText();
				else
					hcpstatus = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='Time Off Territory']/parent::tr/following-sibling::tr[2]/td/div)[" + (tot - d) + "]")).getText();

				System.out.println("hcpstatus: " + hcpstatus);
				//size = driver.findElements(By.xpath("//div[@class='vodtooltipbody']//tbody/tr")).size();
				//size = driver.findElements(By.xpath("//div[@class='vodtooltipbody']//tbody/tr/td/div")).size();
				//System.out.println(size);
				if (scheduleddateandtime.trim().contains(calltime.trim()))
				{
					if (profile.equalsIgnoreCase("Medical"))
					{
						if (hcpstatus.contains("Submitted"))
						{
							mouseoverdraganddrop = true;
							break;
						}
					}
					else
					{
						if ((hcpstatus.contains("Planned")) || (hcpstatus.contains("Planned (Blue)")))
						{
							mouseoverdraganddrop = true;
							break;
						}
					}

				}
			}

		}
		if (mouseoverdraganddrop == true)
		{
			report.updateTestLog("Verify able to mouseover on the created TOT", "Able to mouseover and validated the details on created TOT", Status.PASS);

		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the created TOT", "Unable to mouseover and validated the details on created TOT", Status.FAIL);
		}
	}

	public void mouseoverPlannedTOTimmersive()
	{
		Boolean mouseoverdraganddrop = false;
		String reasonplanned;
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");
		String status = null;
		if (locale.equals("JP") && profile.equals("Commercial"))
			status = "AZ Time Off";
		else if (locale.equals("CN") && profile.equals("Commercial"))
		{
			status = "Rep Created";
		}
		else if (locale.equals("CN") && profile.equals("Medical"))
		{
			status = "MA TOT";
		}
		else if (locale.equals("JP") && profile.equals("Medical"))
		{
			status = "Planned";
		}

		List<WebElement> callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title fc-event-title-name']"));
		for (int d = 0; d < callTOT.size(); d++)
		{
			String hcpstatus = null;
			String recordedcall = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-head']/div[" + (d + 1) + "]//span[@class='fc-event-title fc-event-title-name']")).getText();
			reasonplanned = reasonmsl;
			if (recordedcall.equals(reasonplanned))
			{
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-head']/div[" + (d + 1) + "]//span[@class='fc-event-title fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);
				String calltime = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[2]/td[1]")).getText();
				System.out.println(scheduleddateandtime);
				System.out.println(calltime);
				int size = driver.findElements(By.xpath("//div[@class='vodtooltip']//tbody/tr")).size();
				System.out.println(size);
				if (scheduleddateandtime.equals(calltime))
				{
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltip']//tbody/tr[" + size + "]/td[1]")).getText();
					if (hcpstatus.contains(status))
					{
						mouseoverdraganddrop = true;
						break;
					}
				}
			}
		}
		if (mouseoverdraganddrop == true)
		{
			report.updateTestLog("Verify able to mouseover on the created TOT", "Able to mouseover and validated the details on created TOT", Status.PASS);

		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the created TOT", "Unable to mouseover and validated the details on created TOT", Status.FAIL);
			//frameworkparameters.setStopExecution(true);

		}
	}

	static String recordedcall;

	public void mouseoverSubmitTOTold()
	{
		Boolean mouseoverdraganddrop = false;
		String strDataSheet = "Login";
		String calltime = "";
		String profile = dataTable.getData(strDataSheet, "Profile");
		int size = 0;
		String reasonplanned;
		if (profile.equals("Commercial"))
		{
			reasonplanned = "Time Off Territory";
		}
		else
		{
			reasonplanned = reasonmsl;
		}
		List<WebElement> callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title-name']"));
		for (int d = 0; d < callTOT.size(); d++)
		{
			String hcpstatus = null;
			recordedcall = driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']")).getText();
			if (recordedcall.equals(reasonplanned))
			{
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);
				calltime = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[2]/td[1]")).getText();
				System.out.println(scheduleddateandtime);
				System.out.println(calltime);
				size = driver.findElements(By.xpath("//div[@class='vodtooltipbody']//tbody/tr")).size();
				System.out.println(size);
				if (scheduleddateandtime.equals(calltime))
				{
					hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[" + size + "]/td[1]")).getText();

					if (hcpstatus.contains("Submitted"))
					{
						mouseoverdraganddrop = true;
						break;
					}

				}
			}
		}
		if (mouseoverdraganddrop == true)
		{
			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Able to mouseover and validated the details on submitted TOT", Status.PASS);

		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Unable to mouseover and validated the details on subitted TOT", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void mouseoverSubmitTOT()
	{
		Boolean mouseoverdraganddrop = false;
		String reasonplanned, calltime = "";
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");

		List<WebElement> callTOT;

		if (profile.equalsIgnoreCase("Medical"))
		{
			System.out.println(reasonmsl);
			callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title-name' and text()='" + reasonmsl + "']"));
		}
		else
			callTOT = driver.findElements(By.xpath("//span[@class='fc-event-title-name' and text()='Time Off Territory']"));

		int tot = callTOT.size();
		System.out.println("Count of callTOT: " + tot);
		for (int d = tot - 1; d >= 0; d--)
		{
			System.out.println("Count of d: " + d);
			String hcpstatus = null;
			String recordedcall = callTOT.get(d).getText();

			if (profile.equals("Commercial"))
			{
				reasonplanned = "Time Off Territory";
			}
			else
			{
				reasonplanned = reasonmsl;
			}

			if (recordedcall.equals(reasonplanned))
			{
				//By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[" + (d + 1) + "]//span[@class='fc-event-title-name']");
				By call = By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda']//div[@class='fc-agenda-body']/div/div[contains(@class,'Time_Off_Territory_vod__c')][" + (d + 1) + "]");
				mouseovercall = driver.findElement(call);
				Actions builder = new Actions(driver.getWebDriver());
				builder.moveToElement(mouseovercall).perform();
				cf.waitForSeconds(5);

				System.out.println("Count of tot - d: " + (tot - d));

				if (profile.equalsIgnoreCase("Medical"))
					calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='" + reasonmsl + "']/parent::tr/following-sibling::tr/td[@class='vodtooltipwhen'])[" + (tot - d) + "]")).getText();
				else
					calltime = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='Time Off Territory']/parent::tr/following-sibling::tr/td[@class='vodtooltipwhen'])[" + (tot - d) + "]")).getText();

				calltime = calltime.replace("AM", "");
				String[] att = calltime.split(" - ");
				calltime = att[0];

				if (locale.equalsIgnoreCase("IW") || (locale.equalsIgnoreCase("ANZ")))
				{
					calltime = calltime.replace(" ", "");
					String[] dateTime = calltime.split(",");
					String[] d1 = dateTime[1].split(":");
					calltime = d1[0].substring(0);

					if (calltime.contains("0"))
						calltime = dateTime[0] + ", " + d1[0] + d1[1];
					else
					{
						calltime = dateTime[0] + ", " + d1[0].replace(d1[0].substring(0), "0" + d1[0].substring(0)) + ":" + d1[1];
					}
				}

				else if (locale.equalsIgnoreCase("EU2"))
				{
					calltime = calltime.replace(" ", "");
					String[] dateTime = calltime.split(",");
					String[] d1 = dateTime[1].split(":");
					calltime = d1[0].substring(0);

					if (calltime.contains("0"))
						calltime = dateTime[0] + ", " + d1[0].replace(d1[0].substring(0), "") + d1[0].substring(1) + ":" + d1[1];
					else
						calltime = dateTime[0] + ", " + d1[0] + d1[1];

				}

				System.out.println("calltime: " + calltime);

				if (profile.equalsIgnoreCase("Medical"))
					hcpstatus = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='" + reasonmsl + "']/parent::tr/following-sibling::tr[2]/td/div)[" + (tot - d) + "]")).getText();
				else
					hcpstatus = driver.findElement(By.xpath("(//div[@class='vodtooltip']/div[@class='vodtooltipbody']//tbody/tr/td[text()='Time Off Territory']/parent::tr/following-sibling::tr[2]/td/div)[" + (tot - d) + "]")).getText();

				System.out.println("hcpstatus: " + hcpstatus);
				//size = driver.findElements(By.xpath("//div[@class='vodtooltipbody']//tbody/tr")).size();
				//size = driver.findElements(By.xpath("//div[@class='vodtooltipbody']//tbody/tr/td/div")).size();
				//System.out.println(size);

				if (scheduleddateandtime.contains(calltime))
				{
					//hcpstatus = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[" + size + "]/td[1]")).getText();

					if ((hcpstatus.contains("Submitted")) || (hcpstatus.equals("Submitted (Bleu)")) || (hcpstatus.equals("Submitted (Blue)")))
					{
						mouseoverdraganddrop = true;
						break;
					}

				}
			}

		}
		if (mouseoverdraganddrop == true)
		{
			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Able to mouseover and validated the details on submitted TOT", Status.PASS);

		}
		else
		{
			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Unable to mouseover and validated the details on subitted TOT", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void submitTOT() throws Exception
	{
		cf.actionRightClick(mouseovercall, "Right click on Scheduled call");
		cf.clickElement(mouseovercall, "Scheduled Call");
		//cf.clickLink(By.xpath("//a[.='Open Time-Off-Territory']"), " Open Time Off Territory");
		if (cf.isElementPresent((By.xpath("//h2[.='Time Off Territory Edit']")), "Time Off Territory Edit"))
		{
			report.updateTestLog("Verify able to see  Time Off Territory Edit page", "Able to see Time Off Territory Edit", Status.PASS);
			cf.selectData(By.xpath("//label[.='Status']/parent::td//following-sibling::td//select"), "Submitted", "Submitted");
			WebElement save = driver.findElement(ObjVeevaEmail.saveTOT);
			cf.clickElement(save, "Save");
			clickVeevaTab("My Schedule");
			//cf.clickLink(By.xpath("//a[.='My Schedule']"), "My Schedule");
			cf.waitForSeconds(6);

			cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));

			cf.clickLink(ObjVeevaEmail.weekView, "Week View");
			sc.spinner();
			cf.waitForSeconds(5);
			mouseoverSubmitTOT();

		}
		else
		{
			report.updateTestLog("Verify able to Time Off Territory Edit page", "Unable to see Time Off Territory Edit page", Status.FAIL);
			//frameworkparameters.setStopExecution(true);

		}

	}

	public void deleteandTOTid() throws Exception
	{

		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");
		if (profile.equals("Commercial") && (!(locale.equals("JP") || locale.equals("CN"))))
		{
			if (cf.isElementVisible(ObjVeevaEmail.deleteTOT, "Delete TOT"))
			{
				report.updateTestLog("Verify able to see delete Button", "Able to see Delete Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
			else
			{
				report.updateTestLog("Verify able to see delete Button", "Unable to see Delete Button", Status.PASS);

			}
		}
		else
		{
			if (cf.isElementVisible(ObjVeevaEmail.deleteTOT, "Delete TOT"))
			{
				report.updateTestLog("Verify able to see delete Button", "Able to see Delete Button", Status.PASS);

			}
			else
			{
				report.updateTestLog("Verify able to see delete Button", "Unable to see Delete Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);

			}

		}

	}

	public void verifysubmitstatus()
	{
		String tot = null;
		String strDataSheet = "Login";
		String status1;
		String locale = dataTable.getData(strDataSheet, "Locale");
		//String profile=dataTable.getData(strDataSheet, "Profile");
		String accountrecord = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[1]/td[1]")).getText();
		if (!(locale.equals("JP") || locale.equals("CN")))
		{
			tot = "Time Off Territory";
		}
		else
		{
			tot = "AZ Time Off";
		}

		if (!(locale.equals("JP") || locale.equals("CN")))
		{
			status1 = "Submitted";
		}
		else
		{
			status1 = "AZ Time Off";
		}

		if (tot.equals(accountrecord))
		{
			String status;
			try
			{
				status = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[3]/td[1]")).getText();
			}
			catch (Exception e)
			{
				status = driver.findElement(By.xpath("//div[@class='vodtooltipbody']//tbody/tr[4]/td[1]")).getText();

			}
			if (status.contains(status1))
			{
				//			recordstatus=true;
				report.updateTestLog("Verify able to see status as Submitted for Saved TOT", "Able to see status as Submitted for Saved TOT", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to see status as Submitted for Saved TOT", "Able to see status as Submitted for Saved TOT", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see TOT", "Unable to see TOT", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}

	}

	public void dayViewEditvalidations() throws Exception
	{
		Boolean editvalueboolean = false;
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String locale = dataTable.getData(strDataSheet, "Locale");
		String reasonplanned, tottext;
		String editvalue2;
		if (profile.equals("Commercial") && (!(locale.equals("JP") || locale.equals("CN"))))
		{
			reasonplanned = "Time Off Territory";
		}
		else
		{
			reasonplanned = reasonmsl;
		}

		if (profile.equals("Commercial") && (!(locale.equals("JP") || locale.equals("CN"))))
		{
			editvalue2 = "Edit";
		}
		else
		{
			editvalue2 = "Edit | Del";
		}

		cf.clickLink(ObjVeevaEmail.dayView, "Day View");
		cf.waitForSeconds(5);
		List<WebElement> totvalues = driver.findElements(By.xpath("//table[@id='vodDayViewTbl']//tr//td[5]"));
		System.out.println(totvalues.size());
		for (int i = 1; i <= totvalues.size(); i++)
		{
			if (locale.equals("CN") && profile.equals("Commercial"))
			{
				tottext = driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr[" + (i + 1) + "]//td[4]")).getText();
			}
			else
			{
				tottext = driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr[" + (i + 1) + "]//td[5]")).getText();
			}
			if (tottext.contains(reasonplanned))
			{
				String editvalue = driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr[" + (i + 1) + "]//td[1]")).getText();
				if (editvalue.equals(editvalue2))
				{
					editvalueboolean = true;
				}
				else
				{
					editvalueboolean = false;
				}
			}

		}
		if (editvalueboolean == true)
			report.updateTestLog("Verify able to see Edit Button is present before each Time off Territory Period", "Able to see Edit Button is present before each Time off Territory Period", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to see Edit Button is present before each Time off Territory Period", "Unable to see Edit Button is present before each Time off Territory Period", Status.FAIL);
			//frameworkparameters.setStopExecution(true);

		}
	}

	public void editToT() throws Exception
	{
		Boolean clickedit = false;
		String strDataSheet = "Login";
		String profile = dataTable.getData(strDataSheet, "Profile");
		String reasonplanned;
		if (profile.equals("Commercial"))
		{
			reasonplanned = "Time Off Territory";
		}
		else
		{
			reasonplanned = reasonmsl;

		}

		cf.waitForSeconds(5);
		List<WebElement> totvalues = driver.findElements(By.xpath("//table[@id='vodDayViewTbl']//tr"));
		for (int i = 2; i <= totvalues.size(); i++)
		{
			String tottext = driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr[" + i + "]//td[5]")).getText();
			System.out.println(tottext);
			if (tottext.equals("Submitted: " + reasonplanned))
			{
				String editvalue = driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr[" + i + "]//td[1]")).getText();
				if (editvalue.equals("Edit"))
				{
					//WebElement edit=driver.findElement(By.xpath("//table[@id='vodDayViewTbl']//tr["+i+"]//td[1]"));
					cf.clickByJSE(By.xpath("//table[@id='vodDayViewTbl']//tr[" + i + "]//td[1]/a"), "Click Edit");
					cf.waitForSeconds(5);
					if (cf.isElementVisible(By.xpath("//h2[.='Time Off Territory Edit']"), "Edit Page"))
					{
						clickedit = true;
						break;
					}
				}
			}

		}
		if (clickedit == true)
			report.updateTestLog("Verify able to click on Edit Button before Time off Territory Period with status as Submitted", "Able to click on Edit Button before Time off Territory Period with status as Submitted", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to click on Edit Button before Time off Territory Period with status as Submitted", "Unable to click on Edit Button before Time off Territory Period with status as Submitted", Status.FAIL);
			//frameworkparameters.setStopExecution(true);

		}
	}

	public void changedateandsave() throws Exception
	{

		if (cf.isElementVisible(By.xpath("//h2[.='Time Off Territory Edit']"), "Edit Page"))
		{
			report.updateTestLog("Verify able to Edit page", "Able to see Edit Page", Status.PASS);

			String change = addDaysToDate(dNow, 1);
			cf.setData(ObjVeevaEmail.dateselectionTOT, "Date", change);
			WebElement save = driver.findElement(ObjVeevaEmail.saveTOT);
			cf.clickElement(save, "Save");
			if (cf.isElementVisible(By.xpath("//div[@id='errorDiv_ep']"), "Able to see Error Message"))
			{
				report.updateTestLog("Verify able to see Error It is not permitted to change the Date when the record is in a Status of Submitted.", "Able to see Error It is not permitted to change the Date when the record is in a Status of Submitted.", Status.PASS);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see Error It is not permitted to change the Date when the record is in a Status of Submitted.", "Unable to see Error It is not permitted to change the Date when the record is in a Status of Submitted.", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}

	}
	SimpleDateFormat dateformatvariable;

	public Date dateformat1()
	{

		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");

		if (locale.equals("EU1") || locale.equals("ANZ") || locale.equals("JP") || locale.equals("BR") || locale.equals("CA"))
		{
			dateformatvariable = new SimpleDateFormat("dd/MM/YYYY");
		}
		else if (locale.equals("IC"))
		{
			dateformatvariable = new SimpleDateFormat("MM/dd/YYYY");
		}
		else if (locale.equals("IW"))
		{
			dateformatvariable = new SimpleDateFormat("dd-MM-YYYY");
		}
		else if (locale.equals("IE") || locale.equals("EU2") || locale.equals("RU"))
		{
			dateformatvariable = new SimpleDateFormat("d/M/YYYY");

		}
		else if (locale.equals("CN"))
		{
			dateformatvariable = new SimpleDateFormat("YYYY/M/d");

		}
		return dNow;
	}

	public String addDaysToDate(Date dtDate, int iDays) throws ParseException
	{
		dateformat1();
		SimpleDateFormat sdf2 = dateformatvariable;
		Calendar cal = Calendar.getInstance();
		cal.setTime(dtDate);
		cal.add(Calendar.DAY_OF_MONTH, iDays); //Number of Days to add	 	    
		String strEndDate = sdf2.format(cal.getTime());
		return strEndDate;
	}

	public static String timeconvertToChina(String strTime) throws Exception
	{
		SimpleDateFormat displayFormat = new SimpleDateFormat("ah:mm");
		SimpleDateFormat parseFormat = new SimpleDateFormat("h:mm a");
		Date date = parseFormat.parse(strTime);
		System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
		String startd = displayFormat.format(date);
		return startd;
	}

	//	public void  mouseoverSubmitTOTimmersive(){
	//		Boolean mouseoverdraganddrop=false;
	//		String strDataSheet="Login";
	//		String profile=dataTable.getData(strDataSheet, "Profile");
	//		String reasonplanned;
	//		reasonplanned=reasonmsl;
	//		List<WebElement> callTOT=driver.findElements(By.xpath("//span[@class='fc-event-title fc-event-title-name']"));
	//		for(int d=0;d<callTOT.size();d++){
	//			String hcpstatus = null;
	//			String recordedcall=driver.findElement(By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']//div[@class='fc-agenda-head']/div["+(d+1)+"]//span[@class='fc-event-title fc-event-title-name']")).getText();
	//			if(recordedcall.equals(reasonplanned)){
	//				By call=By.xpath("//div[@class='fc-view fc-view-agendaWeek fc-agenda'][@style='position: relative; display: block;' or @style='position: relative;']//div[@class='fc-agenda-head']/div["+(d+1)+"]//span[@class='fc-event-title fc-event-title-name']");
	//				mouseovercall = driver.findElement(call);
	//				Actions builder = new Actions(driver.getWebDriver());
	//				builder.moveToElement(mouseovercall).perform();
	//				cf.waitForSeconds(5);
	//				String calltime=driver.findElement(By.xpath("//div[@class='vodtooltip' and contains(@style,'display: block;')]//tbody/tr[2]/td[1]")).getText();
	//				System.out.println(scheduleddateandtime);
	//				System.out.println(calltime);
	//				int size=driver.findElements(By.xpath("//div[@class='vodtooltip' and contains(@style,'display: block;')]//tbody/tr")).size();
	//				System.out.println(size);
	//				if(scheduleddateandtime.equals(calltime)){
	//					hcpstatus=driver.findElement(By.xpath("//div[@class='vodtooltip' and contains(@style,'display: block;')]//tbody/tr["+size+"]/td[1]")).getText();
	//					if(hcpstatus.contains("Submitted")){
	//						mouseoverdraganddrop=true;
	//						break;
	//					}
	//				}
	//			}
	//		}
	//		if(mouseoverdraganddrop == true){
	//			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Able to mouseover and validated the details on submitted TOT", Status.PASS);
	//
	//		}
	//		else{
	//			report.updateTestLog("Verify able to mouseover on the submitted TOT", "Unable to mouseover and validated the details on subitted TOT", Status.FAIL);
	//			//frameworkparameters.setStopExecution(true);
	//		}
	//	}

}//End of Class
