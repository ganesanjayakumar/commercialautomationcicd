package businesscomponents;

import org.openqa.selenium.support.ui.WebDriverWait;

import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_DxL_AccountPlansOneKAM extends ReusableLibrary {

	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	private VeevaAccountPlansOneKAM ok = new VeevaAccountPlansOneKAM(scriptHelper);
	private veevaEmail ve = new veevaEmail(scriptHelper);
	private HeaderPage hp = new HeaderPage(scriptHelper);
	
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_DxL_AccountPlansOneKAM(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToAccountPlans() throws Exception{
		vf.clickVeevaTab("Account Plans");
	}

	public void newDxLoneKAM() throws Exception{
		ok.clickNewAccountPlan();
		ok.enterAccountPlanInformation();
		ok.verifyAccountPlanStatuses();
		ok.enterBackgroundInformation();
		ok.enterSWOTAnalysis();
		ok.enterGoalsInformation();
		ok.enterConfidenceStatus();
		ok.saveAccountPlanAndValidateKAMPage();
		ok.verifySavedAccountPlanInformation();
		ok.createAndValidateDMU();
		ok.viewHierarchyInDMU();
		ok.addFromRelationshipMapInDMU();
		ok.searchAccountPlan();
		ok.patientInsightSummary();
		ok.searchAccountPlan();
		ok.createNewAccountTactics();
		ok.addAccountTacticsTaskAndSave();
		ok.recordACallAndTagAccountPlan();
		ok.addProductsForCallReport();
		ok.submitCallReportForOneKAM();
		ok.searchAccountPlan();
		ok.validateCallReportInAccountPlans();
		ok.editAccountTacticsAndVerifyDaysDelay();
		ok.editAccountTacticsStatusAndVerify();
		ok.addLinkAndValidate();
		ok.deleteTactics();
		ok.addNoteAndAttachment();
		ok.modifyConfidenceStatus();
		ok.enterValueProposition();
		ok.searchAccountPlan();
		ok.addTeamMemberAndValidate();
		ve.logOut();
		hp.launchAndLoginToVeeva();
		ok.searchAccountPlan();
		ok.enterAccountQualityAndComplianceAndSave();
		ve.logOut();
		hp.launchAndLoginToVeeva();
		ok.validateAsTeamMember();
		ve.logOut();
		hp.launchAndLoginToVeeva();
		ok.searchAccountPlan();
		ok.validateQualityComplianceScore();
		ve.logOut();

		
	}



	
}

