package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class VeevaAdvanceCoachingReport extends VeevaFunctions
{
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 60);

	public VeevaAdvanceCoachingReport(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	// private veeva_US_Surveys vs=new veeva_US_Surveys(scriptHelper);
	// public static SimpleDateFormat dateformatvariable;
	public void newsurveyCoachingReport() throws Exception
	{
		String CoachingReportName;
		Date exte = new Date();
		String ex = new SimpleDateFormat("mmss").format(exte).toString();
		clickVeevaTab("Surveys");
		cf.clickifElementPresent(objVeevaAdvanceCoachingReport.newButton, "New Button");
		cf.waitForSeconds(5);
		cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", "Coaching Report");
		report.updateTestLog("Verify able to select Coaching Report", "Able to select Coaching Report", Status.SCREENSHOT);
		cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
		cf.waitForSeconds(40);
		cf.switchToFrame(ObjVeevaEmail.mailFrame);

		if (cf.isElementVisible(By.xpath("//h2[.='New Survey']"), "New Survey"))
		{
			CoachingReportName = "CoachingReport" + ex;
			dataTable.putData("AdvanceCoachingReport", "CoachingReportName", CoachingReportName);
			report.updateTestLog("Verify able to see New Survey Target Page", "Able to see survey Target Page", Status.PASS);
			cf.setData(objVeevaAdvanceCoachingReport.surveyname, "Coaching Report Name", CoachingReportName);
			String strDataSheet = "AdvanceCoachingReport";
			String SelectTerritory_Manager = dataTable.getData(strDataSheet, "SelectTerritory_Manager");
			String strDataSheet1 = "Login";
			String locale = dataTable.getData(strDataSheet1, "Locale");
			String profile = dataTable.getData(strDataSheet1, "Profile");
			/*String countrycode = dataTable.getData("AdvanceCoachingReport", "Country_Code");
			String affairs;

			if (profile.equals("Commercial"))
			{
				affairs = "No";
			}
			else
			{
				affairs = "Yes";
			}
			if (!locale.equalsIgnoreCase("JP"))
			{
				if (cf.isElementVisible(objVeevaAdvanceCoachingReport.cnmedicalaffairs, "Medical Affairs"))
				{
					cf.selectData(objVeevaAdvanceCoachingReport.cnmedicalaffairs, "Medical Affairs", affairs);
				}
			}
			if (locale.equalsIgnoreCase("JP") || (locale.equalsIgnoreCase("CN")))
				System.out.println("Country code not present - Expected");
			else if (cf.isElementVisible(By.xpath("//td[@id='value_Survey_vod__c:Country Code']/input"), "Country Code"))
			{
				cf.setData(By.xpath("//td[@id='value_Survey_vod__c:Country Code']/input"), "Countrycode", countrycode);
			}*/

			if(locale.equals("IE") && profile.equals("Commercial"))
				cf.setData(By.xpath("//td[@id='value_Survey_vod__c:Country Code']/input"), "Country Code", "ID");
			if (locale.equals("CN") && profile.equals("Medical"))
			{
				cf.selectData(objVeevaAdvanceCoachingReport.cnmedicalaffairs, "Medical Affairs", "Yes");
			}
			
			selectterittory(SelectTerritory_Manager);
			cf.clickLink(By.xpath("//input[@title='Insert Selected']"), "Insert Selected");
			cf.clickLink(objVeevaAdvanceCoachingReport.mansaveReport, "Save");
			cf.waitForSeconds(15);
			cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if (cf.isElementVisible(By.xpath("//h3[.='Survey Detail']"), "Survey Detail Page"))
			{
				report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see New Survey Page", "Unable to see survey  Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void questionsCreationNewsurveyquestion() throws Exception
	{
		String questiontext;
		cf.clickLink(objVeevaAdvanceCoachingReport.opsNewQuestion, "New Survey Question");
		cf.waitForSeconds(15);
		if (cf.isElementVisible(By.xpath("//h3[.='Type ']/parent::div//select"), "New Survey Question Page"))
		{
			report.updateTestLog("Verify able to navigate to New Survey Question Page", "Able to navigtae to New Survey Question Page", Status.PASS);
			cf.selectData(By.xpath("//h3[.='Type ']/parent::div//select"), "Select Type", "Radio");
			questiontext = "Choose yes or no?";
			dataTable.putData("Login", "questiontext", questiontext);
			cf.setData(objVeevaAdvanceCoachingReport.opssurveyQuestionarea, "Question Text", questiontext);
			cf.setData(objVeevaAdvanceCoachingReport.opsanswerchoice1, "Answer Choice 1", "Yes");
			cf.setData(objVeevaAdvanceCoachingReport.opsanswerchoice2, "Answer Choice 2", "No");
			cf.setData(objVeevaAdvanceCoachingReport.opsweight1, "Weight 1", "1");
			cf.setData(objVeevaAdvanceCoachingReport.opsweight2, "Weight 2", "2");
			cf.clickLink(By.xpath("//textarea[@class='qlisttextarea displaysError']"), "Text area");
			cf.waitForSeconds(10);
			report.updateTestLog("Verify entered all the deatils", "All details are entered", Status.SCREENSHOT);
			if (cf.isElementVisible(By.xpath("//div[@id='Q_newDialog']//input[@id='Q_EditSave']"), "Save"))
			{
				cf.clickLink(By.xpath("//div[@id='Q_newDialog']//input[@id='Q_EditSave']"), "Question save");
				cf.waitForSeconds(15);
				if (cf.isElementVisible(By.xpath("//h3[.='Survey Detail']"), "Survey Detail Page"))
				{
					report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
					if (cf.isElementVisible(By.xpath("//div[@id='Q_questionslist']//span[.='" + questiontext + "']"), "New Survey Question"))
					{
						report.updateTestLog("Verify able to see added New Survey Question", "Able to see added New Survey Question", Status.PASS);
					}
					else
					{
						report.updateTestLog("Verify able to see added New Survey Question", "Unable to see added New Survey Question", Status.FAIL);
						// frameworkparameters.setStopExecution(true);
					}
				}
				else
				{
					report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see save option in Enable mode", "Unable to see save option in Enabled mode", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to navigate to New Survey Question Page", "Unable to navigtae to New Survey Question Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void questionCrearionQuestionBank() throws Exception
	{
		String questionbankquetext;
		cf.clickLink(objVeevaAdvanceCoachingReport.opsquestionbankbutton, "Question Bank button");
		cf.waitForSeconds(10);
		String locale = dataTable.getData("Login", "Locale");

		if (cf.isElementVisible(By.xpath("//span[.='Question Bank']"), "Question Bank"))
		{
			if (locale.equals("ANZ"))
				cf.selectData(By.xpath("//div[@id='Q_bankDialog']/div/select"), "Type Picklist", "Number");
			else
				cf.selectData(By.xpath("//div[@id='Q_bankDialog']/div/select"), "Type Picklist", "Picklist");
			int noofquestions = driver.findElements(By.xpath("//table[@id='Q_questionsTable']//tr/td[1]/div[1]/input")).size();
			try
			{
				for (int i = 2; i <= noofquestions; i++)
				{
					String type = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[3]")).getText().trim();
					if (type.equals("Picklist") || type.equals("Number"))
					{
						cf.clickLink(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[1]//input[@type='checkbox']"), "Question Selected");
						questionbankquetext = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[2]")).getText();
						dataTable.putData("Login", "questionbankquetext", questionbankquetext);
						WebElement ele = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[1]//input[@type='checkbox']"));
						if (ele.isSelected())
						{
							report.updateTestLog("Verify selected question from question bank", "Able to select question from question from question bank", Status.PASS);
							cf.clickLink(By.id("Q_addSelected"), "Add Selected");
							cf.clickLink(By.xpath("//span[.='Question Bank']/parent::div//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']"), "Close");
							cf.waitForSeconds(20);
							cf.implicitWait(60, TimeUnit.SECONDS);
							if (cf.isElementVisible(By.xpath("//div[@id='Q_questionslist']//span[.='" + questionbankquetext + "']"), "Question bank Question"))
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Able to see added Question Bank Question", Status.PASS);
								break;
							}
							else
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Unable to see added Question Bank Question", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify selected question from question bank", "Unable to select question from question from question bank", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			catch (Exception e)
			{
				report.updateTestLog("Verify selected question from question bank", "No Questions were found", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void calculateSharing() throws Exception
	{
		cf.clickLink(objVeevaAdvanceCoachingReport.opscalcsharing, "Calculate Sharing");
		cf.implicitWait(60, TimeUnit.SECONDS);
		cf.waitForSeconds(15);
		cf.scrollToElement(By.id("A_save"), "Save");
		cf.clickLink(By.id("A_save"), "Save Button in Advance Sharing");
		cf.waitForSeconds(10);
		// cf.scrollToElement(By.xpath("//div[@id='A_pbBody']//table[@id='A_sharesTable']/tbody//td[1]"),
		// "User in Advance Sharing Table");
		cf.scrollToElement(By.xpath("(//a[@class='linkbutton tpageturner' and contains(.,'Prev')])[1]"), "User in Advance Sharing Table and Prev link displaying");
		// Boolean namesvalue=false;
		String strDataSheet = "AdvanceCoachingReport";
		String NamesinAdvanceSharing = dataTable.getData(strDataSheet, "NamesinAdvanceSharing");
		cf.waitForSeconds(1);
		String names[] = NamesinAdvanceSharing.split(";");
		cf.selectData(By.xpath("//div[@id='A_sharesTableWrapper']//select"), "Select Dafault Value", "100");
		for (int i = 0; i < names.length; i++)
		{
			String name = names[i];
			System.out.println(name);
			if (cf.isElementPresent(By.xpath("//div[@id='A_pbBody']//table[@id='A_sharesTable']/tbody//td[.='" + name + "']"), name + " is present in Advance Sharing Table"))
			{
				// namesvalue=true;
			}
		}
		/*
		 * if(namesvalue==true)
		 * report.updateTestLog("Verify able to see the users "
		 * +names+"in Advance sharing Table",
		 * "Able to see users in Advance Sharing Table", Status.PASS); else{
		 * report.updateTestLog("Verify able to see the users "
		 * +names+"in Advance sharing Table",
		 * "Unable to see users in Advance Sharing Table", Status.FAIL);
		 * //frameworkparameters.setStopExecution(true); }
		 */
	}

	public void publish() throws Exception
	{
		String questiontext = dataTable.getData("Login", "questiontext");
		cf.waitForSeconds(1);
		String questionbankquetext = dataTable.getData("Login", "questionbankquetext");
		cf.waitForSeconds(1);
		Boolean visible = false;
		cf.clickLink(objVeevaAdvanceCoachingReport.opspublish, "Publish");
		cf.waitForSeconds(18);
		ArrayList<String> questions = new ArrayList<>(Arrays.asList(questiontext, questionbankquetext));
		cf.switchToFrame(ObjVeevaEmail.mailFrame);
		for (int i = 0; i < questions.size(); i++)
		{
			String que = questions.get(i).trim();
			if (cf.isElementVisible(By.xpath("//span[.='" + que + "']"), "Questions"))
			{
				report.updateTestLog("Verify able to see question " + que + "", "Question is displayed", Status.DONE);
				driver.switchTo().defaultContent();
				visible = true;
			}
		}
		if (visible == true)
			report.updateTestLog("Verify able to see questions after publishing", "Able to see questions after Publishing", Status.PASS);
		else
		{
			report.updateTestLog("Verify able to see questions after publishing", "Unable to see questions after Publishing", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void newCoachingReport() throws Exception
	{
		// String secondanswer;
		clickVeevaTab("Advanced Coaching Reports");
		if (cf.isElementVisible(objVeevaAdvanceCoachingReport.manNewcoachingReport, "New Coaching Report"))
		{
			String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
			System.out.println(CoachingReportName);
			cf.clickLink(objVeevaAdvanceCoachingReport.manNewcoachingReport, "New Coaching Report");
			cf.waitForSeconds(60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='New Coaching Report']")));
			cf.clickLink(By.xpath("//span[.='" + CoachingReportName + "']/parent::td/parent::tr//input"), CoachingReportName + " is selected");

			// New Coaching report button is clicked
			cf.clickLink(objVeevaAdvanceCoachingReport.mannewcoachingReportButtoninNewcoachingRepPage, "New Coaching Report Button in New Coaching Report Page");
			cf.waitForSeconds(60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='" + CoachingReportName + "']")));
			// cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if (cf.isElementVisible(By.xpath("//h1[@class='pageType ng-binding' and text()='Survey Target']"), "Survey Target Detail Page"))
				report.updateTestLog("Verify able to navigate to Coaching Report Page", "Able to open as New Coaching Report", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to navigate to Coaching Report Page", "Unable to open as New Coaching Report", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see New Coaching Report Button", "Unable to see New Coaching Report Button", Status.FAIL);
			throw new Exception("Unable see Caoching Report radio button");
		}
	}

	public void surveyTargetdetailautopopulated()
	{
		Boolean details = false;
		// String strDataSheet="Login";
		// String locale=dataTable.getData(strDataSheet, "Locale");
		cf.waitForSeconds(1);
		// String profile=dataTable.getData(strDataSheet, "Profile");
		cf.waitForSeconds(1);
		if (driver.findElement(objVeevaAdvanceCoachingReport.surveydetailsurveyName).getAttribute("value") != null)
		{
			if (driver.findElement(objVeevaAdvanceCoachingReport.surveydetailstartdate).getAttribute("value") != null)
			{
				if (driver.findElement(objVeevaAdvanceCoachingReport.surveydetailenddate).getAttribute("value") != null)
				{
					if (driver.findElement(objVeevaAdvanceCoachingReport.surveydetailmanager).getAttribute("value") != null)
					{
						details = true;
					}
				}
			}
		}
		if (details == true)
			report.updateTestLog("Verify survey name ,startdate,end date and manager fields are autopopulated", "survey name ,startdate,end date and manager fields are autopopulated", Status.PASS);
		else
		{
			report.updateTestLog("Verify survey name ,startdate,end date and manager fields are autopopulated", "survey name ,startdate,end date and manager fields are not autopopulated", Status.PASS);
			// frameworkparameters.setStopExecution(true);
		}
	}

	// Changed Method on April 29 by Kusuma
	public void surveyTargetDetails() throws Exception
	{
		String reviewDate, employeepridid;
		String strDataSheet = "Login";
		String locale = dataTable.getData(strDataSheet, "Locale");
		cf.waitForSeconds(1);
		String profile = dataTable.getData(strDataSheet, "Profile");
		String secondanswer = null;
		cf.waitForSeconds(1);
		if (!locale.equals("US"))
		{
			secondanswer = dataTable.getData("AdvanceCoachingReport", "secondanswer");
		}
		dateformatrev();
		reviewDate = dataTable.getData("AdvanceCoachingReport", "reviewdate1");
		cf.waitForSeconds(1);
		dataTable.putData("AdvanceCoachingReport", "reviewDate", reviewDate);
		if (!(locale.equals("JP") && profile.equals("Commercial")))
		{
			cf.selectData(objVeevaAdvanceCoachingReport.surveydetaildurationvisit, "Duration Visit", "1");
		}
		if ((locale.equals("CN")))
		{
			cf.setData(By.id("Time_Spent_Coaching_AZ__c"), "Time Spent on Coaching", "1");
		}
		cf.clickElement(objVeevaAdvanceCoachingReport.surveydetailReviewdate, "Review Date");
		// cf.setData(objVeevaAdvanceCoachingReport.surveydetailReviewdate,
		// "Review Date", reviewDate);
		cf.selectData(objVeevaAdvanceCoachingReport.engagementType, "Engagement Type", "Virtual");
		searchEmployee();
		if (!locale.equals("US"))
		{
			cf.clickLink(By.xpath("//div[@class='qselectablediv ng-scope'][1]//input[@value='Yes']"), "Answer for Question 1 in Survey Target Detail Page");
			cf.clickElement(By.xpath("//select[@id='answer']/option[2]"), "Answer for Question 2 in Survey Target Detail Page");
			
			//cf.selectData(By.xpath("//select[@id='answer']/option"), "Answer for Question 2 in Survey Target Detail Page", secondanswer);
		}
		else
		{
			// vs.submitanswers();
		}
		// cf.switchToFrame(ObjVeevaEmail.mailFrame);
		cf.clickLink(objVeevaAdvanceCoachingReport.surveysave, "Save the Survey Target");
		cf.waitForSeconds(15);
		// cf.switchToFrame(ObjVeevaEmail.mailFrame);
		if (!(locale.equals("CN")))
		{
			if (cf.isElementVisible(By.name("Employee_PRID_AZ__c"), "Employee PRID"))
			{
				employeepridid = driver.findElement(By.name("Employee_PRID_AZ__c")).getText();
				System.out.println(employeepridid);
				if (!(employeepridid == null))
				{
					report.updateTestLog("Verify PRID ID is autopoulated", "PRID ID is autopopulated", Status.PASS);

					cf.clickLink(objVeevaAdvanceCoachingReport.surveysendtoEmployee, "Send To Employee");
					cf.waitForSeconds(15);
					if (cf.isElementVisible(objVeevaAdvanceCoachingReport.recall, "Recall", 10))
					{
						report.updateTestLog("Verify able to see Recall Button", "Able to see Recall Button", Status.PASS);

					}
					else
					{
						report.updateTestLog("Verify able to see Recall Button", "Unable to see Recall Button", Status.FAIL);

					}
				}
				else
				{
					report.updateTestLog("Verify PRID ID is autopoulated", "PRID ID is not autopopulated", Status.FAIL);

				}
			}
			else
			{
				report.updateTestLog("Verify PRID ID is present", "PRID ID is not present", Status.FAIL);

			}
		}
		else
		{
			cf.clickLink(objVeevaAdvanceCoachingReport.surveysendtoEmployee, "Send To Employee");
			cf.waitForSeconds(15);
			if (cf.isElementVisible(objVeevaAdvanceCoachingReport.recall, "Recall", 10))
			{
				report.updateTestLog("Verify able to see Recall Button", "Able to see Recall Button", Status.PASS);

			}
		}
	}

	// Changed Method on April 29 by Kusuma
	public void reportVerificationInEmployeeUser() throws Exception
	{
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		if (!locale.equals("US"))
			clickVeevaTab("Advanced Coaching Reports");
		else
		{
			clickVeevaTab("Coaching");
		}
		cf.waitForSeconds(15);
		if (cf.isElementVisible(By.xpath("//table[@class='list']"), "Table of Surveys"))
		{
			cf.selectData(By.xpath("//select[@ng-model='timeFrameObj.selected']"), "Review Date", "Future");
			cf.waitForSeconds(18);
			String option = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']"))).getFirstSelectedOption().getText();
			System.out.println("Future is selected................+" + option);
			if (!option.trim().equals("Future"))
			{
				Select se = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
				se.selectByVisibleText("Future");
				cf.waitForSeconds(10);
			}
			if (cf.isElementVisible(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next Button"))
			{
				cf.clickLink(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next");
				cf.waitForSeconds(12);
				reportVerificationEmployeeuseriteration();
			}
			else
			{
				reportVerificationEmployeeuseriteration();
			}
		}
		else
		{
			report.updateTestLog("Verify able to navigate to Coaching Page", "Unable to navigate to coaching Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void reportVerificationEmployeeuseriteration() throws Exception
	{
		String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		cf.waitForSeconds(1);
		int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
		for (int i = reportssize; i > 1; i--)
		{
			if (cf.isElementVisible(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Survey Report"))
			{
				report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
				if (cf.isElementVisible(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Under Employee Review']"), "Under employee Review"))
				{
					report.updateTestLog("Verify able to see Coaching Report with Report status as Under Employee Review ", "Able to see Coaching Report  with Report status as Under Employee Review for " + CoachingReportName + "", Status.PASS);
					cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), CoachingReportName);
					cf.waitForSeconds(15);
					break;
				}
				else
				{
					report.updateTestLog("Verify able to see Coaching Report with Report status as Under Employee Review ", "Unable to see Coaching Report  with Report status as Under Employee Review for " + CoachingReportName + "", Status.FAIL);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Coaching Report", "Unable to see Coaching Report " + CoachingReportName + "", Status.FAIL);
				// throw new Exception("Unable to see Coaching Report");
			}
		}
	}

	// Changed on 30 TH April BY Kusuma.
	public void reportstatusEmployee() throws Exception
	{
		// cf.switchToFrame(ObjVeevaEmail.mailFrame);
		String coachingReport = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		if (cf.isElementVisible(By.xpath("//h2[text()='" + coachingReport + "']"), "Coaching Report"))
		{
			report.updateTestLog("Verify able to navigate to Coaching report Page", "Able to navigate to Coaching report Page", Status.PASS);
			cf.clickLink(objVeevaAdvanceCoachingReport.employeesurveytargetacknowledge, "Acknowledge");
			cf.waitForSeconds(30);
			// cf.switchToFrame(ObjVeevaEmail.mailFrame);
			String reportStatus = driver.findElement(By.name("Report_Status_vod__c")).getText();
			if (reportStatus.equals("Completed"))
			{
				report.updateTestLog("Verify able to see report status as Completed", "Able to see report status as Completed in Survey Detail Page", Status.PASS);
				// go to all tabs
				String locale = dataTable.getData("Login", "Locale");
				String profile = dataTable.getData("Login", "Profile");
				if (!locale.equals("US"))
				{
					clickVeevaTab("Advanced Coaching Reports");
				}
				else
				{
					new Veeva_US_AdvanceCoachingReport(scriptHelper).fieldlevel();
					clickVeevaTab("Coaching");
				}
				if (cf.isElementVisible(By.xpath("//h2[.='Coaching']"), "Coaching") || cf.isElementVisible(By.xpath("//h2[.='Coaching Reports']"), "Coaching Reports"))
				{
					cf.selectData(By.xpath("//select[@ng-model='timeFrameObj.selected']"), "Review Date", "Future");
					cf.waitForSeconds(10);
					String option = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']"))).getFirstSelectedOption().getText();
					System.out.println("Future is selected................+" + option);
					if (!option.trim().equals("Future"))
					{
						Select se = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
						se.selectByVisibleText("Future");
						cf.waitForSeconds(6);
					}
					cf.waitForSeconds(3);
					report.updateTestLog("Verify able to navigate to Coaching Page", "Able to navigate to coaching Page", Status.PASS);
					if (cf.isElementVisible(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next Button"))
					{
						cf.clickLink(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next");
						cf.waitForSeconds(15);
						reportStatusEmployeeiteration();
					}
					else
					{
						reportStatusEmployeeiteration();
					}
				}
				else
				{
					report.updateTestLog("Verify able to navigate to Coaching Page", "Unable to navigate to coaching Page", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see report status as Completed", "Unable to see report status as Completed in Survey Detail Page", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey Detail Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void reportStatusEmployeeiteration() throws Exception
	{
		String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		cf.waitForSeconds(1);
		int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
		for (int i = reportssize; i > 1; i--)
		{
			if (cf.isElementVisible(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Survey Report"))
			{
				report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
				if (cf.isElementVisible(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Completed']"), "Completed"))
				{
					cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Completed']"), "Scroll to Survey");
					cf.waitForSeconds(4);
					report.updateTestLog("Verify able to see Coaching Report with Report status as Completed ", "Able to see Coaching Report  with Report status as Completed for " + CoachingReportName + "", Status.PASS);
					break;
				}
				else
				{
					report.updateTestLog("Verify able to see Coaching Report with Report status as Completed ", "Unable to see Coaching Report  with Report status as Under Completed for " + CoachingReportName + "", Status.FAIL);
					// frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.FAIL);
				throw new Exception("Unable to see Coaching Report");
			}
		}
	}

	public void managerreportstatus() throws Exception
	{
		int i;
		String strDataSheet = "AdvanceCoachingReport";
		String manager = dataTable.getData(strDataSheet, "Manager");
		cf.waitForSeconds(1);
		String employee = dataTable.getData(strDataSheet, "Employee");
		cf.waitForSeconds(1);
		String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		cf.waitForSeconds(1);
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		if (!locale.equals("US"))
		{
			clickVeevaTab("Advanced Coaching Reports");
		}
		else
		{
			clickVeevaTab("Coaching");
		}
		cf.waitForSeconds(15);
		if (cf.isElementVisible(By.xpath("//h2[.='Coaching']"), "Coaching") || cf.isElementVisible(By.xpath("//h2[.='Coaching Reports']"), "Coaching Reports"))
		{
			report.updateTestLog("Verify able to see Coaching Page", "Able to see Coaching Page", Status.PASS);
			cf.selectData(By.xpath("//select[@ng-model='selectedEmployee.Id']"), "Select Employee", employee);
			cf.selectData(By.xpath("//select[@ng-model='timeFrameObj.selected']"), "Review Date", "Future");
			cf.waitForSeconds(6);
			String option = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']"))).getFirstSelectedOption().getText();
			System.out.println("Future is selected................+" + option);
			if (!option.trim().equals("Future"))
			{
				Select se = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
				se.selectByVisibleText("Future");
				cf.waitForSeconds(12);
			}
			// int
			// reportssize=driver.findElements(By.xpath("//table[@class='list']//tr")).size();
			if (cf.isElementVisible(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next Button"))
			{
				cf.clickLink(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next");
				cf.waitForSeconds(15);
				int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
				for (i = reportssize; i > 1; i--)
				{
					// elescroll=driver.findElement(By.xpath("//table[@class='list']//tr["+i+"]//td[2]/a[.='"+CoachingReportName+"']"));
					if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Survey Report"))
					{
						cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Scroll");
						report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
						if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Completed']"), "Completed"))
						{
							cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Scroll");
							// highlight
							report.updateTestLog("Verify able to see Coaching Report with Report status as Completed ", "Able to see Coaching Report  with Report status as Completed for " + CoachingReportName + "", Status.PASS);
							String employeevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[4]/div")).getText();
							String managervalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[3]/div")).getText();
							if (employeevalue.equals(employee) && managervalue.equals(manager))
							{
								// cf.scrollToElement(By.xpath("//table[@class='list']//tr["+i+"]//td[2]/a[.='"+CoachingReportName+"']"),
								// "Scroll");
								report.updateTestLog("Verify Employee and manager details are displayed correctly", "Able to see Employee and manager details are displaying correctly", Status.PASS);
								cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), CoachingReportName);
								break;
							}
							else
							{
								report.updateTestLog("Verify Employee and manager details are displayed correctly", "Unable to see Employee and manager details are displaying correctly", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify able to see Coaching Report with Report status as   Completed ", "Unable to see Coaching Report  with Report status as Under Completed for " + CoachingReportName + "", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			else
			{
				int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
				for (i = reportssize; i > 1; i--)
				{
					// elescroll=driver.findElement(By.xpath("//table[@class='list']//tr["+i+"]//td[2]/a[.='"+CoachingReportName+"']"));
					if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Survey Report"))
					{
						cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Scroll");
						report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
						if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Completed']"), "Completed"))
						{
							cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Scroll");
							report.updateTestLog("Verify able to see Coaching Report with Report status as Completed ", "Able to see Coaching Report  with Report status as Completed for " + CoachingReportName + "", Status.PASS);
							String employeevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[4]/div")).getText();
							String managervalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[3]/div")).getText();
							if (employeevalue.equals(employee) && managervalue.equals(manager))
							{
								cf.scrollToElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Scroll");
								report.updateTestLog("Verify Employee and manager details are displayed correctly", "Able to see Employee and manager details are displaying correctly", Status.PASS);
								cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), CoachingReportName);
								cf.waitForSeconds(20);
								break;
							}
							else
							{
								report.updateTestLog("Verify Employee and manager details are displayed correctly", "Unable to see Employee and manager details are displaying correctly", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify able to see Coaching Report with Report status as   Completed ", "Unable to see Coaching Report  with Report status as Under Completed for " + CoachingReportName + "", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
		}
		else
		{
			report.updateTestLog("Verify able to see Coaching Page", "Unable to see Coaching Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void managerclone() throws Exception
	{
		String coachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		if (cf.isElementVisible(By.xpath("//h2[text()='" + coachingReportName + "']"), "Coaching Report"))
		{
			report.updateTestLog("Verify able to navigate to Coaching report", "Able to navigate to Coaching Report Page", Status.PASS);
			cf.clickLink(objVeevaAdvanceCoachingReport.managerclone, "Clone");
			cf.waitForSeconds(5);
			if (cf.isElementVisible(objVeevaAdvanceCoachingReport.surveydetailReviewdate, "Review Date"))
			{
				daterev();
				String daterev = dataTable.getData("AdvanceCoachingReport", "daterev");
				cf.waitForSeconds(1);
				System.out.println(daterev);
				cf.setData(objVeevaAdvanceCoachingReport.surveydetailReviewdatedev, "daterev", daterev);
				cf.clickLink(objVeevaAdvanceCoachingReport.surveysendtoEmployee, "Send To Employee");
				cf.waitForSeconds(10);
				if (cf.isElementVisible(objVeevaAdvanceCoachingReport.recall, "Recall"))
				{
					report.updateTestLog("Verify able to see Recall Button after cloning", "Able to see Recall Button after cloning", Status.PASS);
					//
					String reportstatus = driver.findElement(objVeevaAdvanceCoachingReport.mansurveyclonestatus).getText();
					if (reportstatus.equals("Under Employee Review"))
					{
						report.updateTestLog("Verify able to see status as Under Employee Review", "Able to see status as Under Employee Review", Status.PASS);
						cf.clickLink(objVeevaAdvanceCoachingReport.recall, "Recall");
						cf.waitForSeconds(12);
						report.updateTestLog("Verify status after Recall", "Status of recall is captured after clicking on recall button as Manager", Status.SCREENSHOT);

					}
					else
					{
						report.updateTestLog("Verify able to see status as Under Employee Review", "Unable to see status as Under Employee Review", Status.FAIL);
					}
				}
				else
				{
					report.updateTestLog("Verify able to see Recall Button after cloning", "Unable to see Recall Button after cloning", Status.FAIL);
				}
			}
		}
		else
		{
			report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey Detail Page", Status.FAIL);
		}
	}
	
	
		
	

	public void checkrecalledreport() throws Exception
	{
		Boolean checkrecall = false;
		dataTable.putData("AdvanceCoachingReport", "checkrecall", checkrecall.toString());
		clickVeevaTab("Advanced Coaching Reports");
		if (cf.isElementVisible(By.xpath("//h2[.='Coaching']"), "Coaching") || cf.isElementVisible(By.xpath("//h2[.='Coaching Reports']"), "Coaching Reports"))
		{
			cf.selectData(By.xpath("//select[@ng-model='timeFrameObj.selected']"), "Review Date", "Future");
			String option = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']"))).getFirstSelectedOption().getText();
			System.out.println("Future is selected....+" + option);
			if (!option.trim().equals("Future"))
			{
				Select se = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
				se.selectByVisibleText("Future");
				cf.waitForSeconds(6);
			}
			cf.waitForSeconds(3);
			checkrecallreportiteration();
			if (cf.isElementVisible(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next"))
			{
				cf.clickLink(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next");
				cf.waitForSeconds(10);
				checkrecallreportiteration();
			}
		}
		else
		{
			report.updateTestLog("Verify able to see Coaching Report page", "Unable to see Coaching Report Page", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
		Boolean checkrecallupdated = Boolean.parseBoolean(dataTable.getData("AdvanceCoachingReport", "checkrecallupdated"));
		if (checkrecallupdated == true)
		{
			report.updateTestLog("Verify able to see  recalled report by logging with Sales Rep", "Able to see recalled report by logging with sales rep", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
		else
		{
			report.updateTestLog("Verify able to see  recalled report by logging with Sales Rep", "Unable to see recalled report by logging with sales rep", Status.PASS);
		}
	}

	public void checkrecallreportiteration() throws Exception
	{
		Boolean checkrecallupdated = Boolean.parseBoolean(dataTable.getData("AdvanceCoachingReport", "checkrecall"));
		String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		cf.waitForSeconds(1);
		int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
		for (int i = reportssize; i > 1; i--)
		{
			dateformatrev();
			cf.waitForSeconds(1);
			String recalldate = dataTable.getData("AdvanceCoachingReport", "reviewdate1");
			cf.waitForSeconds(1);
			System.out.println(recalldate);
			if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Coaching Report"))
			{
				report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
				String recalldatevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[7]/div")).getText();
				if (recalldatevalue.contains(recalldate))
				{
					checkrecallupdated = true;
					dataTable.putData("AdvanceCoachingReport", "checkrecallupdated", checkrecallupdated.toString());
					break;
				}
			}
		}
	}

	public void recall() throws Exception
	{
		int i;
		String strDataSheet = "AdvanceCoachingReport";
		String manager = dataTable.getData(strDataSheet, "Manager");
		String employee = dataTable.getData(strDataSheet, "Employee");
		String CoachingReportName = dataTable.getData("AdvanceCoachingReport", "CoachingReportName");
		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		Date dNow = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dNow);
		String daterev = dataTable.getData("AdvanceCoachingReport", "daterev");
		cf.waitForSeconds(1);
		clickVeevaTab("Advanced Coaching Reports");
		if (cf.isElementVisible(By.xpath("//h2[.='Coaching']"), "Coaching") || cf.isElementVisible(By.xpath("//h2[.='Coaching Reports']"), "Coaching Reports"))
		{
			report.updateTestLog("Verify able to see Coaching Page", "Able to see Coaching Page", Status.PASS);
			Select emp = new Select(driver.findElement(By.xpath("//select[@ng-model='selectedEmployee.Id']")));
			emp.selectByVisibleText(employee);
			Select revdt = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
			revdt.selectByVisibleText("Future");
			cf.waitForSeconds(5);
			// review date View is selected as future
			String option = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']"))).getFirstSelectedOption().getText();
			System.out.println("Future is selected................+" + option);
			if (!option.trim().equals("Future"))
			{
				Select se = new Select(driver.findElement(By.xpath("//select[@ng-model='timeFrameObj.selected']")));
				se.selectByVisibleText("Future");
				cf.waitForSeconds(6);
			}
			if (cf.isElementVisibleforToggle(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next"))
			{
				cf.clickLink(By.xpath("//span[@class='prevNext ng-binding nextPrevSome']"), "Next");
				cf.waitForSeconds(12);
				int reportssize = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
				for (i = reportssize; i > 1; i--)
				{
					if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Survey Report"))
					{
						report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
						if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Under Employee Review']"), "Under Employee Review"))
						{
							report.updateTestLog("Verify able to see Coaching Report with Report status as Under Employee Review after recalled ", "Able to see Coaching Report  with Report status as Under Employee Review for " + CoachingReportName + " after recalled", Status.PASS);
							String employeevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[4]/div")).getText();
							System.out.println("Employee Value is: " + employeevalue);
							String managervalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[3]/div")).getText();
							System.out.println("Manager Value is: " + managervalue);
							String recalldatevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[7]/div")).getText();
							System.out.println("Recall Date Value is: " + recalldatevalue);
							String reviewdatevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[6]/div")).getText();
							System.out.println("Review Date Value is: " + reviewdatevalue);
							String recalldate = dataTable.getData("AdvanceCoachingReport", "reviewDate1");
							System.out.println(recalldate);
							if (employeevalue.equals(employee) && managervalue.equals(manager) && recalldatevalue.contains(recalldate) && reviewdatevalue.equals(daterev))
							{
								report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Able to see proper recalldatevalue with Under Employee Review Status", Status.PASS);
								cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), CoachingReportName);
								cf.waitForSeconds(15);
								if (cf.isElementVisible(objVeevaAdvanceCoachingReport.managerclone, "Clone"))
								{
									report.updateTestLog("Verify able to see Clone Button", "Clone button is clicked sucessfully", Status.PASS);
									break;
								}
								else
								{
									report.updateTestLog("Verify able to see Clone Button", "Clone button is not clicked sucessfully", Status.FAIL);
									// frameworkparameters.setStopExecution(true);
								}
							}
							else
							{
								report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Unable to see proper recalldatevalue with Under Employee Review Status", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Unable to see proper recalldatevalue with Under Employee Review Status", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			else
			{
				int reportssize2 = driver.findElements(By.xpath("//table[@class='list']//tr")).size();
				System.out.println(reportssize2);
				for (i = reportssize2; i > 1; i--)
				{
					if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), "Coaching Report"))
					{
						report.updateTestLog("Verify able to see Coaching Report", "Able to see Coaching Report " + CoachingReportName + "", Status.PASS);
						if (cf.isElementVisibleforToggle(By.xpath("//table[@class='list']//tr[" + i + "]//td[5]/div[.='Under Employee Review']"), "Under Employee Review"))
						{
							report.updateTestLog("Verify able to see Coaching Report with Report status as Under Employee Review after recalled ", "Able to see Coaching Report  with Report status as Under Employee Review for " + CoachingReportName + " after recalled", Status.PASS);
							String employeevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[4]/div")).getText();
							System.out.println("Employee Value is: " + employeevalue);
							String managervalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[3]/div")).getText();
							System.out.println("Manager Value is: " + managervalue);
							String recalldatevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[7]/div")).getText();
							// Added by Jk on Jul 23 2019 - To Trim time values
							String recallD[] = recalldatevalue.split("\\s");
							recalldatevalue = recallD[0];
							System.out.println("Recall Date Value is: " + recalldatevalue);
							String reviewdatevalue = driver.findElement(By.xpath("//table[@class='list']//tr[" + i + "]//td[6]/div")).getText();
							System.out.println("Review Date Value is: " + reviewdatevalue);
							String recalldate = vf.formatDateTimeMarketWiseACR(cal.getTime(), strLocale, strProfile, "date");
							System.out.println(recalldate);
							if (employeevalue.equals(employee) && managervalue.equals(manager) && recalldatevalue.contains(recalldate) && reviewdatevalue.equals(daterev))
							{
								report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Able to see proper recalldatevalue with Under Employee Review Status", Status.PASS);
								cf.clickLink(By.xpath("//table[@class='list']//tr[" + i + "]//td[2]/a[.='" + CoachingReportName + "']"), CoachingReportName);
								cf.waitForSeconds(15);

								report.updateTestLog("Verify able to see report", "report is seen", Status.PASS);
								break;
							}
							else
							{
								report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Unable to see proper recalldatevalue with Under Employee Review Status", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify able to see proper recall date with Under Employee Review Status", "Unable to see proper recalldatevalue with Under Employee Review Status", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
		}
		else
		{
			report.updateTestLog("Verify able to see Coaching Report with Report status as Under Employee Review after recalled ", "Unable to see Coaching Report  with Report status as Under Employee Review for " + CoachingReportName + " after recalled", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void searchEmployee() throws Exception
	{
		String strDataSheet = "AdvanceCoachingReport";
		String employee = dataTable.getData(strDataSheet, "Employee");
		cf.waitForSeconds(5);
		cf.clickLink(By.xpath("//input[@name='Employee_vod__c']/following-sibling::a "), "Employee Look UP"); // Fix:
																												// Murali
																												// 30/01/2019
		cf.waitForSeconds(15);
		/*
		 * String parentwindow=driver.getWindowHandle(); Set <String>
		 * childwindows=driver.getWindowHandles(); for(String
		 * window:childwindows){ if(!(parentwindow.equals(window))){
		 * driver.switchTo().window(window);
		 */
		WebElement title = driver.findElement(By.xpath("//div[.='Employee']"));
		String searchTitle = title.getText();
		if (searchTitle.equals("Employee"))
		{
			// driver.switchTo().frame("frameSearch");
			cf.setData(By.xpath("//input[@id='Employee_vod__c_searchText']"), "Input of Employee Name", employee);
			cf.clickLink(objVeevaAdvanceCoachingReport.lookupemployeego, "GO");
			cf.waitForSeconds(12);
			/*
			 * cf.switchToParentFrame(); WebElement
			 * ele=driver.findElement(By.xpath("//frame[@id='frameResult']"));
			 * //cf.switchToFrame("frameset1"); driver.switchTo().frame(ele);
			 */
			if (cf.isElementVisible(By.xpath("//table[@class='list']//td//a[.='" + employee + "']"), employee))
			{
				// report.updateTestLog("Verify able to see Manage Name", "Able
				// to see Manager Name", Status.PASS);
				// WebElement
				// employeename=driver.findElement(By.xpath("//table[@class='list']//td//a[.='"+employee+"']"));
				cf.clickLink(By.xpath("//table[@class='list']//td//a[.='" + employee + "']"), employee);
				cf.waitForSeconds(10);
				report.updateTestLog("Verify able to see Employee Name", "Able to see Employee Name", Status.PASS);
				// driver.switchTo().window(parentwindow);
			}
			else
			{
				report.updateTestLog("Verify able to see the Employee Name", "Unable to see Employee Name", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
		else
		{
			report.updateTestLog("Verify able to switch to Employee window", "Unable to switch to Employee Window", Status.FAIL);
			// frameworkparameters.setStopExecution(true);
		}
	}

	public void dateformatrev()
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		Date dNow = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dNow);
		String reviewdate1 = vf.formatDateTimeMarketWiseACR(cal.getTime(), strLocale, strProfile, "date");
		dataTable.putData("AdvanceCoachingReport", "reviewdate1", reviewdate1);
	}

	public void daterev()
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strProfile = dataTable.getData("Login", "Profile");
		Date dNow = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dNow);
		if (!strLocale.equals("CN"))
		{
			cal.add(Calendar.DATE, 1);
		}
		String daterev = vf.formatDateTimeMarketWiseACR(cal.getTime(), strLocale, strProfile, "date");
		System.out.println(daterev);
		dataTable.putData("AdvanceCoachingReport", "daterev", daterev);
	}
}
