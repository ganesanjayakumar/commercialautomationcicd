package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_CSL_MinorUpdates  extends VeevaFunctions  {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(),30);
	public Veeva_CSL_MinorUpdates (ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	public void clickrecordcall() throws Exception{
		String datasheet="Login";
		dataTable.getData(datasheet, "Record Type");
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.recordCallButton));
			if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call")){
				report.updateTestLog("Verify able to see Record Call Button", "Able to see Record Call Button", Status.PASS);
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call");
				cf.waitForSeconds(30);
				report.updateTestLog("Verify able to see New Call Report Page", "Able to see New Call Report Page", Status.PASS);
			}
			else{
				report.updateTestLog("Verify able to see Record Call Button", "Unable to see Record Call Button", Status.FAIL);
			} 
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void groupCall() throws Throwable{
		String callType = dataTable.getData("Call", "AZCallType");
		
		cf.selectData(By.xpath("//select[@name='RecordTypeId']"), "Record Type", "AZ Meeting Call Detail PRC");
		
		System.out.println("INSIDE jp");
		cf.selectData(cf.getSFElementXPath("Professional Information", "AZ Call Type", "Select"), "AZ Call Type", callType);
		
		if(cf.isElementVisible(By.xpath("//label[text()='Event Usage ID']"), "Event Usage ID")){
			report.updateTestLog("Verify able to see Event Usage ID field", "Able to see Event Usage ID field", Status.PASS);
		}
		
		if(cf.isElementVisible(By.xpath("//h3[contains(text(),'Details of Explanatory')]"), "Details of Explanatory")){
			report.updateTestLog("Verify able to see Details of Explanatory field", "Able to see Details of Explanatory field", Status.PASS);
		}
		cf.verifyElementInPage(By.xpath("//label[text()='Hospitals']"), "Hospital");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Locations that are approved venues')]"), "Locations that are approved venues");
		cf.verifyElementInPage(By.xpath("//span/child::input[contains(@id,'Venue_AZ_JP')]"), "Venue");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'of Dr Attendees')]"), "Num of Dr Attendees");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Total Cost of Bento Box, Drink')]"), "Total Cost of Bento Box Drink");
		cf.verifyElementInPage(By.xpath("//label[text()='# of Other Participants']"), "Number of Other Participants");   
		cf.verifyElementInPage(By.xpath("//label[text()='# of Other Participants']"), "Number of Other Participants");
		cf.verifyElementInPage(By.xpath("//label[text()='# of Discarded Bento Box and Drink']"), "Number of Discarded Bento Box and Drink");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Total Attendees')]"), "Total Attendees");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Unit Price of Bento Box & Drink')]"), "Unit Price of Bento Box & Drink");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Attach Name Book')]"), "Attach Name Book");
		cf.verifyElementInPage(By.xpath("//label[contains(text(),'Report over 3240')]"), "Report over 3240");
		cf.waitForSeconds(6);
		cf.clickButton(By.xpath("//input[contains(@id,'Hospitals_AZ_JP')]"), "Hospital Checkbox");
		cf.setData(By.xpath("//input[contains(@id,'Number_of_Dr_Attendees_AZ_JP')]"), "Dr_Attendees", "5");
		cf.setData(By.xpath("//input[contains(@id,'Total_Cost_of_Bento_Box')]"), "Total_Cost_of_Bento_Box", "3240");
		cf.setData(By.xpath("//input[contains(@id,'Number_of_Other_Participants')]"), "Number_of_Other_Participants", "5");
		cf.clickButton(By.xpath("//input[contains(@id,'Attach_Name_Book')]"), "Attach Name Book");
		cf.clickButton(By.xpath("(//input[@type='button' and @name='Save'])[1]"), "save");
		cf.waitForSeconds(10);
		
		String expectederror="Call Attendee is a required field";
		if(cf.isElementVisible(By.xpath("//div[contains(text(),'"+expectederror+"')]"), "Call Attendee is a required field Error MSG")){
			report.updateTestLog("Verify able to see Proper Error Message "+expectederror+"", "Able to see Proper Error Message "+expectederror+"", Status.PASS);
			cf.clickButton(By.xpath("(//td/child::input[@class='checkbox-indent'])[1]"), "select product");
//			cf.clickButton(By.xpath("//input[contains(@class,'ng-valid ng-scope ng-dirty ng-valid')]"), "select Attendee");
			cf.clickButton(By.xpath("(//input[contains(@class,'ng-pristine ng-untouched ng-valid ng-scope') and @type='checkbox'])[1]"), "select Attendee");
			cf.clickButton(By.xpath("//span/child::input[contains(@id,'Locations_that_are_approved_venues')]"), "Locations_that_are_approved_venues"); 
//			cf.setData(By.xpath("//input[contains(@id,'Venue_AZ_JP')]"), "Venue", "Test");
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Save'])[1]"), "save");
			cf.waitForSeconds(10);
		}else{
			report.updateTestLog("Verify able to see Proper Error Message "+expectederror+"", "unable to see Proper Error Message "+expectederror+"", Status.FAIL);
		}
			String expectederrortwo="Please put a check mark in any one of";
			if(cf.isElementVisible(By.xpath("//div[contains(text(),'"+expectederrortwo+"')]"), "Error")){
				report.updateTestLog("Verify able to see Proper Error Message "+expectederrortwo+"", "Able to see Proper Error Message "+expectederrortwo+"", Status.PASS);
//			cf.clickButton(By.xpath("//span/child::input[contains(@id,'Locations_that_are_approved_venues')]"), "uncheck Locations_that_are_approved_venues");
			cf.clickButton(By.xpath("//input[contains(@id,'Hospitals_AZ_JP')]"), "Hospital UnCheckbox");
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Save'])[1]"), "save");
			cf.waitForSeconds(10);
			}else{
				report.updateTestLog("Verify able to see Proper Error Message "+expectederrortwo+"", "unable to see Proper Error Message "+expectederrortwo+"", Status.FAIL);
		}
			String expectederrorthird="Please enter the venue name.";
			if(cf.isElementVisible(By.xpath("//div[contains(text(),'"+expectederrorthird+"')]"), "Error")){
				report.updateTestLog("Verify able to see Proper Error Message "+expectederrorthird+"", "Able to see Proper Error Message "+expectederrorthird+"", Status.PASS);
			cf.clickButton(By.xpath("//input[contains(@id,'Hospitals_AZ_JP')]"), "Hospital");
			cf.clickButton(By.xpath("//span/child::input[contains(@id,'Locations_that_are_approved_venues')]"), "Locations_that_are_approved_venue UnCheckbox");
			cf.clickButton(By.xpath("//input[contains(@id,'Attach_Name_Book')]"), "Attach Name Book UnCheckbox");
			cf.clickButton(By.xpath("//input[@type='button' and @name='Submit']"), "Submit");
			cf.waitForSeconds(10);
			}
			String expectederrorfour="Check \"Please attach a good name book\"";
			if(cf.isElementVisible(By.xpath("//div[contains(text(),'"+expectederrorfour+"')]"), "Error")){
				report.updateTestLog("Verify able to see Proper Error Message "+expectederrorfour+"", "Able to see Proper Error Message "+expectederrorfour+"", Status.PASS);
				cf.clickButton(By.xpath("//input[contains(@id,'Attach_Name_Book')]"), "Attach Name Book");
				cf.clickButton(By.xpath("(//input[@type='button' and @name='Save'])[1]"), "save");
				cf.waitForSeconds(15);
				cf.verifyElementInPage(By.xpath("//span[text()='Saved']"), "saved");
				cf.verifyElementInPage(By.xpath("//span/span[contains(text(),'C0')]"), "Call Id");
				cf.clickButton(By.xpath("//input[@type='button' and @name='Edit']"), "Edit");
				cf.waitForSeconds(8);
			}
			cf.setData(By.xpath("//input[contains(@id,'Total_Cost_of_Bento_Box')]"), "Total_Cost_of_Bento_Box", "324000");
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Save'])[1]"), "save");
			cf.waitForSeconds(10);
	
			String expectederrorfifth="The unit price exceeds 3,240 yen (including tax). Please check with your boss and report it to the compliance department through the line if it exceeds.";
			if(cf.isElementVisible(By.xpath("//div[contains(text(),'"+expectederrorfifth+"')]"), "Error")){
				report.updateTestLog("Verify able to see Proper Error Message "+expectederrorfifth+"", "Able to see Proper Error Message "+expectederrorfifth+"", Status.PASS);
				cf.setData(By.xpath("//input[contains(@id,'Total_Cost_of_Bento_Box')]"), "Total_Cost_of_Bento_Box", "3240");
				cf.clickButton(By.xpath("//input[@type='button' and @name='Submit']"), "Submit");
				cf.waitForSeconds(15);
				cf.verifyElementInPage(By.xpath("//span[text()='Submitted']"), "Submitted");
			}
		}
	
	
	public void createAZPatientInformation() throws Exception
	{	
		cf.waitForSeconds(20);
		String dropout = dataTable.getData("Call", "AZPatientDropOutDate");
		cf.clickButton(By.xpath("//span[text()='AZ Patient Information (HCA)']"), "AZ Patient Information");
		cf.waitForSeconds(5);
		if(cf.isElementVisible(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "AZ Patient Information - Home"))
		{
			cf.clickButton(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "Create");
			cf.selectData(By.xpath("//select[@id='p3']"), "Record Type", "Respi");
			cf.clickButton(By.xpath("//input[@value='Continue' and @class='btn']"), "Continue");
			if (cf.isElementVisible(By.xpath("//td[text()='Respi']"), "Respi"))
			{
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				
				String status = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status.contains("Finding"))
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "able to see AZ patient status is "+status, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "unable to see AZ Patient Information is NOT "+status+"", Status.FAIL);

				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				cf.selectData(By.xpath("//span/child::select[@id='00N0I00000KTU8D']"), "AZ patient Identification", "identified patient");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status2 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status2.contains("Targeted"))
					report.updateTestLog("Verify able to see AZ patient status is "+status2+"", "able to see AZ patient status is "+status2, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status2+"", "unble to see AZ Patient Information is "+status2+"", Status.FAIL);
				
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='39']"), "AZ patient Fasenra start plan date");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status3 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status3.contains("Standing by"))
					report.updateTestLog("Verify able to see AZ patient status is "+status3+"", "Verify AZ patient status is "+status3, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status3+"", "unble to see AZ Patient Information is "+status3, Status.FAIL);
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='41']"), "AZ patient start date");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status4 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status4.contains("Prescribed"))
					report.updateTestLog("Verify able to see AZ patient status is "+status4+"", "able to see AZ patient status is "+status4, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status4+"", "unable to see AZ Patient Information is "+status4, Status.FAIL);
				
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='40']"), "AZ patient drop out date");
				cf.selectData(By.xpath("//span/select[@id='00N0I00000KTU8S']"), "AZ patient reasons for Drop Out", dropout);
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				String status5 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status5.contains("Drop out"))
					report.updateTestLog("Verify abl eto see AZ patient status is "+status5+"", "able to see AZ patient status is "+status5, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status5+"", "u nble to see AZ Patient Information is "+status5, Status.FAIL);
			}
		}
	}
			public void backTrackTheStatus()  throws Exception{
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(6);
				cf.setData(By.xpath("//span/child::input[@id='00N0I00000KTU8H']"), "AZ patient drop out date clear", "");
				cf.selectData(By.xpath("//span/select[@id='00N0I00000KTU8S']"), "AZ patient reasons for Drop Out", "--None--");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status.contains("Prescribed"))
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "able to see AZ patient status is "+status, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "unable to see AZ Patient Information is "+status, Status.FAIL);
				
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				cf.setData(By.xpath("//input[@id='00N0I00000KTU8U']"), "AZ patient start date", "");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status2 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status2.contains("Standing by"))
					report.updateTestLog("Verify able to see AZ patient status is "+status2+"", "Verify AZ patient status is "+status2, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status2+"", "unble to see AZ Patient Information is "+status2, Status.FAIL);
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(6);
				
				cf.setData(By.xpath("//input[@id='00N0I00000KTU8B']"), "AZ patient Fasenra start plan date", "");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				
				
				String status3 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status3.contains("Targeted"))
					report.updateTestLog("Verify able to see AZ patient status is "+status3+"", "able to see AZ patient status is "+status3, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status3+"", "unble to see AZ Patient Information is "+status3+"", Status.FAIL);
				
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
				
				cf.selectData(By.xpath("//span/child::select[@id='00N0I00000KTU8D']"), "AZ patient Identification", "--None--");
				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
				cf.waitForSeconds(6);
				
				String status4 = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div")).getText();
				if (status4.contains("Finding"))
					report.updateTestLog("Verify able to see AZ patient status is "+status4+"", "able to see AZ patient status is "+status4, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status4+"", "unable to see AZ Patient Information is NOT "+status4+"", Status.FAIL);
				
	}
			
			
			public void createAZPatientInfoandChallenge() throws Exception{
		
				String account = dataTable.getData("Call", "AccountSelected");
				cf.clickButton(By.xpath("//span[text()='AZ Patient Information (HCA)']"), "AZ Patient Information");
				cf.waitForSeconds(5);
				if(cf.isElementVisible(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "AZ Patient Information - Home"))
				{
					cf.clickButton(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "Create");
//					cf.selectData(By.xpath("//select[@id='p3']"), "Record Type", "Respi");
//					cf.clickButton(By.xpath("//input[@value='Continue' and @class='btn']"), "Continue");
					
					if (cf.isElementVisible(By.xpath("//td[text()='Respi']"), "Respi"))
					{
		                report.updateTestLog("Verify able to see the Respi Page.", "Able to see the Respi Page", Status.PASS);
		                cf.selectData(By.xpath("//span//select[@id='00N0I00000KTU85']"), "AZ bio prescription", "Xolair");
		                cf.setData(By.xpath("//input[@id='00N0I00000KTU86']"), "AofZ eosinophils in blood", "6");
		                cf.setData(By.xpath("//input[@id='00N0I00000KTU89']"), " AZ number  exacerbation", "5");
//		                cf.selectData(By.xpath("//select[@id='00N0I00000KTU84']"), "AZ use of OCS", "continuous use");
		                cf.setData(By.xpath("//input[@id='00N0I00000KTU7u']"), "AZ Dosage of OCS", "7");
		                cf.setData(By.xpath("//input[@id='00N0I00000K3ZCX']"), "AZ FeNO", "8");
		              
		                cf.selectData(By.xpath("//select[@id='00N0I00000KTU8F']"), "AZ Patient Age", "20");
		                cf.selectData(By.xpath("//select[@id='00N0I00000KTU8C']"), "AZ Patient Gender", "Male");

		           //Validating the OPtions under AZ Patient Occupation
		                cf.clickButton(By.xpath("//select[@id='00N0I00000KTU80']"), "AZ Patient Occupation");
		                By publicofficer=By.xpath("//select[@id='00N0I00000KTU80']//option[text()='Public Officer']");
				        By otherhcp=By.xpath("//select[@id='00N0I00000KTU80']//option[text()='Other health care professional']");
				           
				        if(cf.isElementVisible(otherhcp, "Other HCP") && cf.isElementVisible(publicofficer, "Public Officer")){
		                report.updateTestLog("Verify able to see the Other HCP and Public Officer Options under AZ Patient Occupation.", "Able to see the Other HCP and Public Officer Options under AZ Patient Occupation.", Status.PASS);
		                cf.clickElement(otherhcp, "Other HCP");
				        }
				        else
				        	report.updateTestLog("Verify able to see the Other HCP and Public Officer Options under AZ Patient Occupation.", "Unable to see the Other HCP and Public Officer Options under AZ Patient Occupation.", Status.FAIL);

		           //Validating Options under AZ OCS Side Effects.

		      //     icf.clickElementIOS(By.xpath("//XCUIElementTypeStaticText[starts-with(@id,'{AZ OCS Side Effects')]/preceding-sibling::XCUIElementTypeButton[1]"), "AZ OCS Side Effects");

		           List<String> options=new ArrayList<>(Arrays.asList("Diabetes","Osteoporosis","Infections","Cataract","Mood disorder","Weight gain","Non"));

		           List<WebElement> sideeffectapp=driver.findElements(By.xpath("//select[@multiple='multiple' and @tabindex='31']/optgroup[@style='text-decoration:none;']/option"));	
		           Boolean sideeffects=false;
		           for(int i=0;i<options.size();i++){
		                sideeffects=false;
		                //String optionvalue=icf.getData(By.xpath("//XCUIElementTypeNavigationBar[@text='SunrisePicklistView']/following-sibling::XCUIElementTypeOther//XCUIElementTypeTable/XCUIElementTypeCell["+i+"]/XCUIElementTypeStaticText"), "Option value", "text");
		                String optionvalue=sideeffectapp.get(i).getText();
		                if(optionvalue.contains(options.get(i)))
		                     sideeffects=true;
		                else{
		                     sideeffects=false;
		                     break;
		                }
		           }

		           if(sideeffects==true){
		                report.updateTestLog("Verify able to see the Options properly under AZ OCS Side Effects.", "able to see the Options properly under AZ OCS Side Effects.", Status.PASS);
		                cf.clickElement(By.xpath("//optgroup//option[text()='Infections']"), "Infections");
		                cf.clickElement(By.xpath("(//img[@class='picklistArrowRight' and @title='Add'])[3]"), "Add");
		           }else
		                report.updateTestLog("Verify able to see the Options properly under AZ OCS Side Effects.", "Unable to see the Options properly under AZ OCS Side Effects.", Status.FAIL);

		           cf.verifyElementInPage(By.xpath("//label[text()='AZ patient progress after 4 weeks']//parent::td/following-sibling::td/span/select"), "AZ Patient Progress after 4 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ Dosage of OCS After 4 weeks']//parent::td/following-sibling::td/input"), "AZ Dosage of OCS After 4 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ eosinophils in blood After 4 weeks']//parent::td/following-sibling::td/input"), "AZ eosinophils in blood After 4 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ patient progress after 8 weeks']//parent::td/following-sibling::td/span/select"), "AZ Patient Progress after 8 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ Dosage of OCS After 8 weeks']//parent::td/following-sibling::td/input"), "AZ Dosage of OCS After 8 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ eosinophils in blood After 8 weeks']//parent::td/following-sibling::td/input"), "AZ eosinophils in blood After 8 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ patient progress after 16 weeks']//parent::td/following-sibling::td/span/select"), "AZ Patient Progress after 16 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ Dosage of OCS After 16  weeks']//parent::td/following-sibling::td/input"), "AZ Dosage of OCS After 16 Weeks");
		           cf.verifyElementInPage(By.xpath("//label[text()='AZ eosinophils in blood After 16 weeks']//parent::td/following-sibling::td/input"), "AZ eosinophils in blood After 16 Weeks");


		           cf.clickButton(By.xpath("//label[text()='AZ Informed Consent']//parent::td/following-sibling::td/span/select"), "AZ Informed Consent");
		           By Icpath=By.xpath("//select[@id='00N9D000000oi4i']/option[text()='IC']");
		           By Icfailure=By.xpath("//select[@id='00N9D000000oi4i']/option[text()='IC failure']");
		           By waitingreply=By.xpath("//select[@id='00N9D000000oi4i']/option[text()='Waiting for reply']");

		           if(cf.isElementVisible(waitingreply, "Waiting for reply") && cf.isElementVisible(Icfailure, "IC Failure") && cf.isElementVisible(Icpath, "IC")){
		                report.updateTestLog("Verify able to see the Options properly under AZ Informed Consent.", "able to see the Options properly under AZ Informed Consent.", Status.PASS);
		                cf.clickButton(By.xpath("//select[@id='00N9D000000oi4i']/option[text()='IC']"), "IC");
		           }else
		                report.updateTestLog("Verify able to see the Options properly under AZ Informed Consent", "Unable to see the Options properly under AZ Informed Consent.", Status.FAIL);

		           cf.clickButton(By.xpath("//input[@type='submit' and @name='save']"), "Save");
		           cf.waitForSeconds(8);
		           
		           String identificationerror="you need to check the identification of the patient";
		           String appbioprescription=cf.getData(By.xpath("//div[@id='errorDiv_ep']"), "Error", "text");
		           if(appbioprescription.contains(identificationerror)){
		        	   report.updateTestLog("Verify able to see Proper Error Message "+identificationerror+"", "Able to see Proper Error Message "+identificationerror+"", Status.PASS);
					}else
						report.updateTestLog("Verify able to see Proper Error Message "+identificationerror+"", "Unable to see Proper Error Message "+identificationerror+"", Status.FAIL);
		           }
		           cf.selectData(By.xpath("//select[@id='00N0I00000KTU8D']"), "AZ patient Identification", "identified patient");
		           cf.clickButton(By.xpath("//input[@type='submit' and @name='save']"), "Save");
		           cf.waitForSeconds(7);
		           
		   		String status = driver.findElement(By.xpath("//div[text()='Targeted']")).getText();
				if (status.contains("Targeted"))
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "able to see AZ patient status is "+status, Status.PASS);
				else
					report.updateTestLog("Verify able to see AZ patient status is "+status+"", "unble to see AZ Patient Information is "+status+"", Status.FAIL);
				
				cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
				cf.waitForSeconds(7);
		           
		           String appbioprescription=cf.getData(By.xpath("//span//select[@id='00N0I00000KTU85']"), "AZ bio prescription", "text");
		           String appazeosinophils=cf.getData(By.xpath("//input[@id='00N0I00000KTU86']"), "AZ eosinophils", "text");
		           String appazexacerabation=cf.getData(By.xpath("//input[@id='00N0I00000KTU89']"), "AZ number of exacerbation", "text");
		           String appazuseofOCS=cf.getData(By.xpath("//select[@id='00N0I00000KTU84']"), "AZ use of OCS", "text");
		           String apppatientidentification=cf.getData(By.xpath("//select[@id='00N0I00000KTU8D']"), "AZ PATIENT Identification", "text");
		           String appAZdosageofOCS=cf.getData(By.xpath("//input[@id='00N0I00000KTU7u']"), "AZ Dosage of OCS", "text");
		           String apPAZFeNo=cf.getData(By.xpath("//input[@id='00N9D000000oi4j']"), "AZ FeNO", "text");
		            appazuseofOCS = "Oral continuous use";
		           if(appbioprescription.equals("Xolair") && appazeosinophils.equals("6") && appazexacerabation.equals("5") && appazuseofOCS.equals("Oral continuous use")){
		                if(apppatientidentification.equals("identified patient") && appAZdosageofOCS.equals("7.0") && apPAZFeNo.equals("8.0")){
		                     report.updateTestLog("Verify the values autopulated properly for Identify Target of Fasenra Information Module.", "The values are autopopulated for Identify target of Fasenra Information", Status.PASS);
		                }
		                else
		                     report.updateTestLog("Verify the values autopulated properly for Identify Target of Fasenra Information Module.", "The values are not autopopulated for Identify target of Fasenra Information", Status.FAIL);
		           }
		           else
		                report.updateTestLog("Verify the values autopulated properly for Identify Target of Fasenra Information Module.", "The values are not autopopulated for Identify target of Fasenra Information", Status.PASS);

		           
		           
		           String appazpatientage=cf.getData(By.xpath("//select[@id='00N0I00000KTU8F']"), "AZ patient age	", "text");
		           String appazpatientgender=cf.getData(By.xpath("//select[@id='00N0I00000KTU8C']"), "AZ patient Gender	", "text");
		           String appAZPatientOccupation=cf.getData(By.xpath("//select[@id='00N0I00000KTU80']"), "AZ Patient Occupation", "text");
		           String appAZOcsSideEffects=cf.getData(By.xpath("//optgroup//option[text()='Infections']"), "AZ OCS Side Effects", "text");
		         
		  //         String accountName = dataTable.getData("Login", "AccountName");
		           if(appazpatientage.equals("20") && appazpatientgender.equals("Male") && appAZPatientOccupation.contains("Other health") && appAZOcsSideEffects.equals("Infections")){
		                report.updateTestLog("Verify able to see Patient Age,Gender Occupation and Side Effects.", "Able to see the patient age,Gender,Occupation and Side Effects Properly", Status.PASS);
		           }
		           else
		                report.updateTestLog("Verify able to see Patient Age,Gender Occupation and Side Effects.", "Unable to see the patient age,Gender,Occupation and Side Effects Properly", Status.PASS);

		           cf.clickButton(By.xpath("//input[@type='submit' and @name='save']"), "Save");
		           cf.waitForSeconds(8);

		           By chanllengepath=By.xpath("//span[text()='AZ Challenge']");

		           if(cf.isElementVisible(chanllengepath,"AZ Challenge")){
		                report.updateTestLog("Verify able to see the the AZ Challenge with 1 Record has been created.", "Able to see the the AZ Challenge with 1 Record has created.", Status.PASS);
		                cf.clickButton(chanllengepath, "AZ Challenge");
		                
		                By createchallenge=By.xpath("//span[text()='New AZ Challenge']");
		                if(!cf.isElementVisible(createchallenge," Create AZ Challenge")){
		                     report.updateTestLog("Verify able to see the Create Button for AZ Challenge","Unable to see the create Button for AZ Challenge" ,Status.PASS);
		                     
		                     cf.clickButton(By.xpath("//tr[@class='headerRow']//following-sibling::tr//th/a"), "Record ID First Row");
		                     
		                     String recordno=cf.getData(By.xpath("//td[text()='Record No']/following-sibling::td/div"), "Record No", "value");
		                     String recordid=cf.getData(By.xpath("//td[contains(text(),'AZ FSN patient')]/following-sibling::td/div/a"), "Record ID", "value");
		                     if(recordid!=null && recordno!=null){
		                           report.updateTestLog("Verify able to see the Record No and AZ FSN patient info autopopulated.", "Able to see the Record No and FSN patient info autopopulated", Status.PASS);

		                       		cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
		                       		cf.waitForSeconds(4);
		                       		cf.clickButton(By.xpath("//label[text()='AZ choice of Bio treatment']/parent::td/following-sibling::td/input"), "AZ Choice of Bio treatment");
		                       		cf.clickButton(By.xpath("//input[@type='submit' and @name='save']"), "Save");
		                       		cf.waitForSeconds(7);

		                           By checkboxonpath=By.xpath("//img[@title='Checked']");
		                           if(cf.isElementVisible(checkboxonpath, "AZ Choice of Bio Treatment")){
		                                report.updateTestLog("Verify able to see the updated values.", "Able to see the updated values", Status.PASS);
		                                cf.clickButton(By.xpath("//td[text()='AZ FSN patient info']/following-sibling::td/div/a"),recordid);
		                                
		                                if (cf.isElementVisible(By.xpath("//td[text()='Account']/following-sibling::td//a"), account)){
		                                cf.clickButton(By.xpath("//td[text()='Account']/following-sibling::td//a"), "Back");
		                                }
		                                cf.waitForSeconds(8);
		                           }else
		                                report.updateTestLog("Verify able to see the updated values.", "Unable to see the updated values", Status.FAIL);
		                     }
		                      else
		                           report.updateTestLog("Verify able to see the Record No and AZ FSN patient info autopopulated.", "Unable to see the Record No and FSN patient info autopopulated", Status.FAIL);
		                }
		                else
		                     report.updateTestLog("Verify able to see the Create Button for AZ Challenge","Aable to see the create Button for AZ Challenge" ,Status.FAIL);
		           }
		           else
		                report.updateTestLog("Verify able to see the the AZ Challenge with 1 Record has been created.", "Unable to see the the AZ Challenge with 1 Record has created.", Status.FAIL);   
		     }

			}

			
//	public void saveBackToEdit() throws Exception
//	{
//		String accountName = dataTable.getData("Call", "AccountSelected");
//		cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
//
//		String saved = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@id='Saved']")).getText();
//		if (saved.equals("Saved"))
//			report.updateTestLog("Verify AZ Patient Information is Saved", "AZ Patient Information is Saved", Status.PASS);
//		else
//			report.updateTestLog("Verify AZ Patient Information is Saved", "AZ Patient Information is NOT Saved", Status.FAIL);
//		if (cf.isElementVisible(By.xpath("//XCUIElementTypeButton[@id='"+accountName+"']"), accountName))
//		{
//			cf.clickElementIOS(By.xpath("//XCUIElementTypeButton[@id='"+accountName+"']"), accountName+" - Back");
//		}
//		cf.waitForSeconds(15);
//		cf.clickElementIOS(By.xpath("//XCUIElementTypeButton[@id='My Accounts']"), "My Accounts");
//		driver.hideKeyboard();
//		
//		com.iOSsearchAccount();
//		com.iOSselectAccountFromMyAccounts();
//		
//		cf.clickElementIOS(By.xpath("//XCUIElementTypeStaticText[contains(@id,'AZ Patient Information')]"), "AZ Patient Information");
//		cf.clickElementIOS(By.xpath("//XCUIElementTypeOther[@id='Last Modified Date']//XCUIElementTypeStaticText[1]"), "Last Modified Date");
//		cf.clickElementIOS(By.xpath("//XCUIElementTypeOther[@id='Last Modified Date']//XCUIElementTypeStaticText[1]"), "Last Modified Date");
//		cf.clickElementIOS(By.xpath("//XCUIElementTypeOther[@id='Last Modified Date']/following-sibling::XCUIElementTypeCell[1]"), "Last Modified Date - First Row");
//	}
//			
			public void createAZPatientInfo() throws Exception{

				cf.clickButton(By.xpath("//span[text()='AZ Patient Information']"), "AZ Patient Information");
				cf.waitForSeconds(5);
				if(cf.isElementVisible(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "AZ Patient Information - Home"))
				{
					cf.clickButton(By.xpath("(//input[@value='New AZ Patient Information' and @class='btn'])[1]"), "Create");
//					cf.selectData(By.xpath("//select[@id='p3']"), "Record Type", "Respi");
//					cf.clickButton(By.xpath("//input[@value='Continue' and @class='btn']"), "Continue");
					if (cf.isElementVisible(By.xpath("//td[text()='Respi']"), "Respi"))
					{
		                     report.updateTestLog("Verify able to see the Respi Page.", "Able to see the Respi Page", Status.PASS);
		                     cf.clickButton(By.xpath("//input[@type='submit' and @name='save']"), "Save");

		                     String findingstatus = driver.findElement(By.xpath("//td[text()='AZ patient status']/following-sibling::td/div[text()='Finding']")).getText();
		                     if(findingstatus.equals("Finding"))
		                           report.updateTestLog("Verify AZ patient status is Finding", "Verify AZ patient status is Finding", Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is Finding", "Unable to see the Finding Status", Status.FAIL);

		                    cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
	                       	cf.waitForSeconds(5);
	                     	cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='39']"), "AZ patient Fasenra start plan date");
	                       	cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='41']"), "AZ patient start date");
	        				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	        				cf.waitForSeconds(6);

		                     String prescribedstatus = driver.findElement(By.xpath("//div[text()='Prescribed']")).getText();
		                     if (prescribedstatus.contains("Prescribed"))
		                           report.updateTestLog("Verify AZ patient status is Prescribed", "Verify AZ patient status is "+prescribedstatus, Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is Prescribed", "Unable to see the Prescribed Status", Status.FAIL);

		                     cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
	                       	 cf.waitForSeconds(5);
	                       	 
	                       	cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='40']"), "AZ patient drop out date");
	        				cf.selectData(By.xpath("//span/select[@id='00N0I00000KTU8S']"), "AZ patient reasons for Drop Out", "Other");
	        				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	        				cf.waitForSeconds(6);
	        				
		                     String dropoutstatus = driver.findElement(By.xpath("//div[text()='Drop out']")).getText();
		                     if (dropoutstatus.contains("Drop out"))
		                           report.updateTestLog("Verify AZ patient status is Drop Out", "Verify AZ patient status is Drop Out", Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is DropOut", "Verify AZ patient status is Drop Out", Status.FAIL);
		                     cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
	                       	 cf.waitForSeconds(5);
	                       	 
	                       	cf.setData(By.xpath("//span/child::input[@id='00N0I00000KTU8H']"), "AZ patient drop out date clear", "");
	        				cf.selectData(By.xpath("//span/select[@id='00N0I00000KTU8S']"), "AZ patient reasons for Drop Out", "--None--");
	        			//	cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='40']"), "AZ patient start date");
	        				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	        				cf.waitForSeconds(6);
	        				
		                     String prescribeddstatus = driver.findElement(By.xpath("//div[text()='Prescribed']")).getText();
		                     if (prescribeddstatus.contains("Prescribed"))
		                           report.updateTestLog("Verify AZ patient status is Prescribed", "Verify AZ patient status is Prescribed", Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is Prescribed", "Verify AZ patient status is Prescribed", Status.FAIL);
		                     cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
	                       	 cf.waitForSeconds(5);
	                       	 
	                       	 cf.selectData(By.xpath("//span/child::select[@id='00N0I00000KTU8D']"), "AZ patient Identification", "not to be");
	                       	 cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	                       	 cf.waitForSeconds(6);
	        				
		                     String outoftarget = driver.findElement(By.xpath("//div[text()='Out of Target Patient']")).getText();
		                     if (outoftarget.contains("Out of Target Patient"))
		                           report.updateTestLog("Verify AZ patient status is Out of Target Patient", "Verify AZ patient status is Out of Target", Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is Out of Target Patient", "Verify AZ patient status is Out of Target", Status.FAIL);

		                     cf.clickButton(By.xpath("(//input[@value=' Edit ' and @class='btn'])[1]"), "Edit");
	                       	 cf.waitForSeconds(5);

	                       	 cf.clickButton(By.xpath("//label[text()='Restart of Fasenra Pres. Administration']/parent::td/following-sibling::td//span/a"), "Restart of Fasenra Pres. Administration");
	                       	 cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	                       	 cf.waitForSeconds(6);
		                     
		                     String errormessage="Restart date value should be input after drop out status.";
		                     String Apperror = driver.findElement(By.xpath("//div[@class='errorMsg']")).getText();
		                     
		     				if (Apperror.contains(errormessage))
		     					report.updateTestLog("Verify able to see AZ patient status is "+errormessage+"", "able to see AZ patient status is "+errormessage, Status.PASS);
		     				else
		     					report.updateTestLog("Verify able to see AZ patient status is "+errormessage+"", "unble to see AZ Patient Information is "+errormessage+"", Status.FAIL);
		     				
		     				 cf.setData(By.xpath("//label[text()='Restart of Fasenra Pres. Administration']/parent::td/following-sibling::td//span/input"), "Clear Restart of Fasenra Pres. Administration", "");
		     				 cf.setData(By.xpath("//span[@class='dateInput dateOnlyInput']/input[@id='00N0I00000KTU8U']"), "AZ patient start date", "");
		     				cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='40']"), "AZ patient drop out date");
	        				cf.selectData(By.xpath("//span/select[@id='00N0I00000KTU8S']"), "AZ patient reasons for Drop Out", "Other");
	        				cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
	        				cf.waitForSeconds(6);
	
		                     String errormessage2="No Prescription start date with drop out date , you can not save patient record";
		                     String Apperror2 = driver.findElement(By.xpath("//div[@id='errorDiv_ep']")).getText();
		                   
		                     if (Apperror2.contains(errormessage2))
			     					report.updateTestLog("Verify able to see AZ patient status is "+errormessage2+"", "able to see AZ patient status is "+errormessage2, Status.PASS);
			     				else
			     					report.updateTestLog("Verify able to see AZ patient status is "+errormessage2+"", "unble to see AZ Patient Information is "+errormessage2+"", Status.FAIL);

		                     cf.clickButton(By.xpath("//span[@class='dateFormat']/a[@tabindex='41']"), "AZ patient start date");
		                     cf.clickButton(By.xpath("(//input[@class='btn' and @name='save'])[1]"), "Save");
		        			 cf.waitForSeconds(6);
		                     
		                     String dropoutstatus2 = driver.findElement(By.xpath("//div[text()='Drop out']")).getText();
		                     if (dropoutstatus2.contains("Drop out"))
		                           report.updateTestLog("Verify AZ patient status is Drop Out", "Verify AZ patient status is Drop Out", Status.PASS);
		                     else
		                           report.updateTestLog("Verify AZ patient status is DropOut", "Verify AZ patient status is Drop Out", Status.FAIL);
		                }
		                else
		                     report.updateTestLog("Verify able to see the Respi Page.", "Unable to see the Respi Page.", Status.FAIL);
		           }
		     }
			
//			public void engageMeeting() throws Throwable{
//				cf.selectData(By.xpath("//select[@id='RecordTypeId']"), "RecordType", "HCO remote meeting");
//				
//				if (cf.isElementVisible(By.xpath("//h2[text()='HCO remote meeting']"), "Veeva Remote Detailing page"))
//				{
//	                     report.updateTestLog("Verify able to see the Veeva Remote Detailing Page.", "Able to see the Veeva Remote Detailing Page", Status.PASS);
//	                     
//	                     cf.clickButton(By.xpath("(//input[contains(@class,'ng-valid ng-scope ng-dirty ng-valid') and @type='checkbox'])[1]"), "attendee checkbox");
//	                     cf.waitForSeconds(2);
//	                     cf.setData(By.xpath("//input[contains(@id,'Number_of_Dr_Attendees_AZ_JP')]"), "Number_of_Dr_Attendees", "5");
//	                     cf.setData(By.xpath("//input[contains(@id,'Number_of_Other_Participants_AZ_JP')]"), "Number_of_Other_Participants", "5");
//	                     cf.setData(By.xpath("//input[contains(@id,'Total_Cost_of_Bento_Box_Drink_AZ_JP')]"), "otal Cost of Bento Box, Drink", "3,240");
//	                   
//	                     cf.setData(By.xpath(""), "", "");
//	                     
//				}
//	                     else
//		                     report.updateTestLog("Verify able to see the Veeva Remote Detailing Page.", "Unable to see the Veeva Remote Detailing Page.", Status.FAIL);
//			}
			
			
			
			
			
			
	}
