package businesscomponents;

import supportlibraries.*;

import com.mfa.framework.Status;
import ObjectRepository.*;
import org.openqa.selenium.*;


public class Veeva_US_DeAlignAccounts extends VeevaFunctions {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	
	public Veeva_US_DeAlignAccounts(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Click on DeAlign Accounts and navigate to DeAlign Accounts Page.
	 * @throws Exception
	 */
	public void clickDeAlignAccounts() throws Exception{
		cf.clickLink(objHeaderPage.plussymbol,"Plus Symbol");
		clickVeevaTab("Dealign Accounts");
		if(cf.isElementVisible(By.xpath("//input[@value='Dealign Selected']"), "Dealign Selected"))
			report.updateTestLog("Verify able to navigate to DeAlign Accounts Page", "Able to see DeAlign Accounts Page.", Status.PASS);
		else
			report.updateTestLog("Verify able to navigate to DeAlign Accounts Page", "Able to see DeAlign Accounts Page.", Status.PASS);	
	}

	/**
	 * Save the account Address Details in Excel Sheet.
	 */
	public void saveaccountaddress(){
		String address;
		String Addressline1,city,state,Zip;
		int rows=driver.findElements(By.xpath("//tbody[@id='j_id0:j_id1:j_id3:table:tb']/tr")).size();
		System.out.println(rows);
		if(rows>0){
			report.updateTestLog("Verify able to see accounts in DeAlign Accounts Table.", "Able to see accounts in DeAlign Accounts Table.", Status.PASS);
			//for(i=1;i<=rows;i++){
			String accountname = getDatafromTable("Name",1);
			dataTable.putData("Login", "AccountName", accountname);
			Addressline1=getDatafromTable("Address line 1", 1);
			dataTable.putData("Accounts", "AddressLine", Addressline1);
			city = getDatafromTable("City",1);
			dataTable.putData("Accounts", "City", city);
			state = getDatafromTable("State",1);
			dataTable.putData("Accounts", "State", state);
			Zip = getDatafromTable("Zip",1);
			dataTable.putData("Accounts", "Zip", city);
			System.out.println(Zip);
			if(((Addressline1.trim()).equals("")&& (city.trim()).equals("") && (state.trim()).equals("")&& (Zip.trim()).equals(""))){
				report.updateTestLog("Verify able to see Account Address Details beside Account Name "+accountname+"", "Unable to see Account Address Details beside Account Name "+accountname+"", Status.SCREENSHOT);
				address="Unavailable";
				dataTable.putData("Accounts", "AddressAvailable", address);
			}
			else{
				report.updateTestLog("Verify able to see Account Address Details beside Account Name "+accountname+"", "Able to see Account Address Details beside Account Name "+accountname+"", Status.SCREENSHOT);
				address="Available";
				dataTable.putData("Accounts", "AddressAvailable", address);
			}
		}
		else
			report.updateTestLog("Verify able to see accounts in DeAlign Accounts Table.", "There are no accounts in DeAlign Accounts Table.", Status.FAIL);

	}

	public String getDatafromTable(String ColName,int i){
		int columns= driver.findElements(By.xpath("//table[@id='j_id0:j_id1:j_id3:table']//th/div")).size();
		int j,icolCount=1;
		String value = null;
		for(j=1;j<=columns;j++){
			String columnname=driver.findElement(By.xpath("//table[@id='j_id0:j_id1:j_id3:table']//th["+j+"]/div")).getText();
			if(ColName.equals(columnname)){
				value=driver.findElement(By.xpath("//table[@id='j_id0:j_id1:j_id3:table']//tr["+i+"]//td["+icolCount+"]")).getText();
			}
			else
				icolCount++;
		}
		return value;
	}

	/**
	 * Scroll to My Preferred Address and change the Address preferences.
	 * @throws Exception
	 */
	public void changeMypreferedaddPreference() throws Exception{
		String addressavailable=dataTable.getData("Accounts", "AddressAvailable");
		cf.switchToFrame(By.xpath("//iframe[@title='Account Territory Info']"), "Account Territory Info");
		cf.scrollToElement(By.id("labelv-i-value:reference:TSF_vod__c:Address_vod__c"), "My Preffered Address Label");
		cf.clickButton(By.id("pbButtonTableColEdit"), "Click Edit Button");
		cf.switchToFrame(By.id("vod_iframe"), "Account Territory Info");
		if(cf.isElementVisible(By.id("pbButtonTableColSave"), "Save Button")){
			report.updateTestLog("Verify able to navigate to Preferred Address Change Page.", "Able to navigate to Preferred Address Change Page.", Status.PASS);
			if(addressavailable.equals("Available"))
				cf.selectData(By.id("i-value:picklist:TSF_vod_c:Address_vod__c"), "My Preffered Address", "--None--");	
			else{
				String option=driver.findElement(By.xpath("//select[@id='i-value:picklist:TSF_vod_c:Address_vod__c']//option[2]")).getText();
				cf.selectData(By.id("i-value:picklist:TSF_vod_c:Address_vod__c"), "My Preffered Address",option);
				dataTable.putData("Accounts", "AddressOptionValue", option);
			}
			cf.clickButton(By.id("pbButtonTableColSave"), "Save");
			cf.waitForSeconds(5);
			cf.switchToParentFrame();
		}
		else{
			report.updateTestLog("Verify able to navigate to Preferred Address Change Page.", "Unable to navigate to Preferred Address Change Page.", Status.FAIL);
		}
	}

	/**
	 * Validate the Changed Preferred Address value
	 */
	public void validatechangedpreferredadd(){
		String addressavailable=dataTable.getData("Accounts", "AddressAvailable");
		String account=dataTable.getData("Login", "AccountName");
		int i;
		String Addressline1,city,state,Zip;
		int rows=driver.findElements(By.xpath("//tbody[@id='j_id0:j_id1:j_id3:table:tb']/tr")).size();
		System.out.println(rows);
		if(rows>0){
			for(i=1;i<=rows;i++){
				String appaccount=getDatafromTable("Name", i);
				if(account.equals(appaccount)){
					Addressline1=getDatafromTable("Address line 1", i);
					city = getDatafromTable("City",i);
					state = getDatafromTable("State",i);
					Zip = getDatafromTable("Zip",i);
					if(addressavailable.equals("Available")){
						if((Addressline1.trim()).equals("")&& (city.trim()).equals("") && (state.trim()).equals("")&& (Zip.trim()).equals("")){
							report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", "Unable to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", Status.PASS);
							break;
						}else
							report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", "Able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as None.", Status.FAIL);
					}
					else{
						String optionvalue=dataTable.getData("Accounts", "AddressOptionValue");
						if(optionvalue.contains(Addressline1) && optionvalue.contains(city)&& optionvalue.contains(state)&& optionvalue.contains(Zip)){
							report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", "Able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", Status.PASS); 
							break;

						}else
							report.updateTestLog("Verify able to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", "Unable to see Account Address Details beside Account Name "+account+" after changing Preferred Address as "+optionvalue+".", Status.FAIL);  
					}
				}
			}
		}
	}

}


