package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_DxL_RecCall extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	Date dNow=new Date();
	
	public Veeva_DxL_RecCall(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	
	/**
	 * This method is for searching and navigating to the accounts
	 * @throws Exception
	 */
	public void searchandFind() throws Exception
	{
		String account = dataTable.getData("RecCall", "Account");
		Select search = new Select(driver.findElement(By.xpath("//select[@title='Search scope']")));
		search.selectByVisibleText("Accounts");
		driver.findElement(By.className("searchTextBox")).sendKeys(account);
		report.updateTestLog("Fetch Account","Account name is entered in the search box",Status.SCREENSHOT);
		driver.findElement(By.xpath("//div[@class='standardSearchElementBody']//input[@title='Go!']")).click();
		cf.waitForSeconds(3);
		
	}
	
	/**
	 * This method is for recording the call
	 * @throws Exception
	 */
	public void recCall() throws Exception
	{
	//	String label = dataTable.getData("RecCall", "Offlabel");
		cf.clickSFButton("Record a Call");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Diagnostic Interaction']")));
		if(cf.isElementVisible(By.xpath("//h1[text()='Diagnostic Interaction']"), "Record a Call page"))
			report.updateTestLog("Record a call","Record a call page is displayed",Status.PASS);
		else
			report.updateTestLog("Record a call","Record a call page is not displayed",Status.FAIL);
		//mandatory fields are validated
		mandatoryFields();
		
		//Topic for next call
		String preCall = (new Date()).toString()+":\n Notes before call is entered";
		String nextCall = (new Date()).toString()+":\n Notes for the next call is entered";
		driver.findElement(By.id("Pre_Call_Notes_vod__c")).sendKeys(preCall);
		driver.findElement(By.id("Next_Call_Notes_vod__c")).sendKeys(nextCall);
		
		//Add attendee
		String attendee= dataTable.getData("RecCall", "Attendee");
		addattendes(attendee);
		
		//Add products
		addProducts();
		
		//Details about the products 
		productDetails();
		
		//Items Dropped
		itemsDropped();
		
		//Follow up
		followUp();
		
		report.updateTestLog("Rec Call", "All details are entered", Status.SCREENSHOT, "Full Screen", driver);
	}
	
		
	/**
	 * This method is for adding two products and enter the required fields
	 * @throws Exception
	 */
	public void addProducts() throws Exception
	{
		if(cf.isElementVisible(By.id("vod_detailing"), "Products"))
		{
			String label = dataTable.getData("RecCall", "Offlabel");
			if(!label.equalsIgnoreCase("Yes")) //onlabel products
			{
				//select the first available product's check box
				driver.findElement(By.xpath("//*[@id='vod_detailing']//tbody[2]/tr/td[2]/input")).click();
				//get the product name 
				String product_1 = driver.findElement(By.xpath("//*[@id='vod_detailing']//tbody[2]/tr/td[2]/label")).getText().trim();
				report.updateTestLog("Products","Product "+product_1+" is selected",Status.DONE);
				System.out.println("Product_1 - selected as listed "+product_1);
				dataTable.putData("RecCall", "Product_1", product_1);
			
			//select another product
			String product_2 = dataTable.getData("RecCall", "Product_2");
			System.out.println("Product_2 selected via detail selector "+product_2);
			WebElement addOther = driver.findElement(By.xpath("//*[@class='ng-scope']//td[@class='transparent']/a[contains(text(),'Add Other')]"));
			addOther.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Detail Selector')]")));
			driver.findElement(By.id("addOtherDetail")).sendKeys(product_2);
			driver.findElement(By.xpath("//*[@id='addOtherDetail']/parent::td/parent::tr/following-sibling::tr/td//div/input")).click();
			report.updateTestLog("Products","Product "+product_2+" is searched and fetched",Status.SCREENSHOT);
			driver.findElement(By.xpath("//*[@id='addOtherDetail']/parent::td/parent::tr/following-sibling::tr[3]/td/input[@title='Add']")).click();
			
			if(cf.isElementVisible(By.xpath("//table[contains(@class,'zvod ng-scope')]"), "Added Products"))
			{
				String pdt1 = driver.findElement(By.xpath("//table[contains(@class,'zvod ng-scope')]/tbody//tr[2]//td[3]")).getText().trim();
				String pdt2 = driver.findElement(By.xpath("//table[contains(@class,'zvod ng-scope')]/tbody//tr[3]//td[3]")).getText().trim();
				String[] a = pdt1.split(" ");
				String[] b = pdt2.split(" ");
				System.out.println(a[0]); System.out.println(b[0]);
				if((a[0].trim().equalsIgnoreCase(product_1)) && ((b[0].trim().equalsIgnoreCase(product_2))))
					report.updateTestLog("Products","Chosen Products are displayed",Status.PASS);
				else
					report.updateTestLog("Products","Products other than the chosen ones are displayed",Status.FAIL);
				
			}
			else
				report.updateTestLog("Products","Chosen Products are not displayed",Status.FAIL);
		
		}
		else//Off-label products
		{
			String product_2 = dataTable.getData("RecCall", "Product_2");
			System.out.println("Product_2 selected via detail selector "+product_2);
			WebElement addOther = driver.findElement(By.xpath("//*[@class='ng-scope']//td[@class='transparent']/a[contains(text(),'Add Other')]"));
			addOther.click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Detail Selector')]")));
			driver.findElement(By.id("addOtherDetail")).sendKeys(product_2);
			driver.findElement(By.xpath("//*[@id='addOtherDetail']/parent::td/parent::tr/following-sibling::tr/td//div/input")).click();
			report.updateTestLog("Products","Product "+product_2+" is searched and fetched",Status.SCREENSHOT);
			driver.findElement(By.xpath("//*[@id='addOtherDetail']/parent::td/parent::tr/following-sibling::tr[3]/td/input[@title='Add']")).click();		
			
			if(cf.isElementVisible(By.xpath("//table[contains(@class,'zvod ng-scope')]"), "Added Products"))
			{
				String pdt2 = driver.findElement(By.xpath("//table[contains(@class,'zvod ng-scope')]/tbody//tr[2]//td[3]")).getText().trim();
				String[] b = pdt2.split(" ");
				System.out.println(b[0]);
				if(b[0].trim().equalsIgnoreCase(product_2))
					report.updateTestLog("Products","Chosen Products are displayed",Status.PASS);
				else
					report.updateTestLog("Products","Products other than the chosen ones are displayed",Status.FAIL);
				
			}
			else
				report.updateTestLog("Products","Chosen Products are not displayed",Status.FAIL);
		}
	}
	else
		report.updateTestLog("Products","Products are not available",Status.FAIL);
		
}
	
	/**
	 * This method is for noting the details of the chosen products
	 * @throws Exception
	 */
	public void productDetails() throws Exception
	{
		
		WebElement pdtList = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[7]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/span/span/div/table//table/tbody"));
		List<WebElement> pdt_tr = pdtList.findElements(By.tagName("tr"));
		System.out.println("Details of "+pdt_tr.size()+" products to be entered");
		for(int i=1;i<=pdt_tr.size();i++)
		{
			prodOptions("Access",i);
			prodOptions("Awareness",i);
			prodOptions("Quality",i);
			prodOptions("Diagnostic Specific Discussion",i);
			
		}
		
		report.updateTestLog("Products Information","Details of the product are captured",Status.SCREENSHOT);
	}
	
	
	/**
	 * This method is for adding fields for the selected products
	 * @throws Exception
	 */
	
	public void prodOptions(String l,int tr) throws Exception
	{
		Select pdt = new Select (driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[7]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/span/span/div/table//table/tbody/tr["+tr+"]//label[text()='Product']/parent::div/following-sibling::div//select")));
		String product = pdt.getFirstSelectedOption().getText();
		System.out.println("The details for the "+product+" are entered");
		String label = driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[7]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/span/span/div/table//table/tbody/tr["+tr+"]//label[text()='"+l+"']")).getText();
		System.out.println(label+" is entered");
		Select selectBox = new Select(driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[7]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/span/span/div/table//table/tbody/tr["+tr+"]//label[text()='"+l+"']//parent::div/following-sibling::div//select[1]")));		
		List<WebElement> eq = selectBox.getOptions();		
		ArrayList<String> equip = new ArrayList<String>(); 		
		if(selectBox.isMultiple())
		{	
			for(int ta=1; ta<eq.size();ta++)
			{
				String x = eq.get(ta).getText();
				equip.add(x);
			}	
			//generating random integer value
			Random random = new Random();
			int r = random.nextInt(equip.size()- 0) + 0; int r1= random.nextInt(equip.size()- 0) + 0;		  
			selectBox.selectByVisibleText(equip.get(r));
			selectBox.selectByVisibleText(equip.get(r1));
			//click the right arrow to move to the other box
			driver.findElement(By.xpath("//*[@id='veeva-app']/div/div/form/div/div[2]/span[7]/div/div/div/div[2]/div[1]/span/div/div/span/span/span/span/span/div/table//table/tbody/tr["+tr+"]//label[text()='"+l+"']/parent::div/following-sibling::div//span[@class='multiSelectPicklistCell']/a/img[@class='picklistArrowRight']")).click();
			cf.waitForSeconds(1);
		}
	}
	
	/**
	 * This method is for entering the follow up field
	 * @throws Exception
	 */
	public void followUp() throws Exception
	{
		if(cf.isElementVisible(By.xpath("//input[@value='Add Line']"), "Add Follow-up"))
		{
			cf.clickButton(By.xpath("//input[@value='Add Line']"), "Add Follow-up");
			String dateLink = driver.findElement(By.xpath("//*[@class='dateFormat ng-scope']/a")).getText();
			Date dNow=new Date();
			String date = new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
			if(date.equals(dateLink))
			{
				driver.findElement(By.xpath("//*[@class='dateFormat ng-scope']/a")).click();
				String comments = (new Date()).toString()+":\n Diagnostic Insight information is entered here";
				cf.setData(By.id("Description"), "Follow up comments", comments);
			}
		}
	}
	
	/**
	 * This method is for adding Items dropped for the product
	 * @throws Exception
	 */
	
	public void itemsDropped() throws Exception
	{
		//MSL Leave Behind
		int r; int d;
		if(cf.isElementVisible(By.xpath("//h3[text()='MSL Leave-behind']"), "MSL Leave Behind"))
		{
			//expand the section
			cf.clickButton(By.xpath("//h3[text()='MSL Leave-behind']/parent::div/img"), "Expand MSL leave behind items");
			WebElement items = driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody"));
			List<WebElement> item_tr = items.findElements(By.tagName("tr"));
			System.out.println("Details of "+item_tr.size());
			//select a row
			Random random = new Random();
			r = random.nextInt(item_tr.size()-2) + 2;
			System.out.println("Row "+r);
			//select a random td
			WebElement items_td = driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody/tr["+r+"]"));
			List<WebElement> item_td = items_td.findElements(By.tagName("td"));
			Random rand = new Random();
			d = rand.nextInt(item_td.size()-1) + 1;
			
			driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody/tr["+r+"]/td["+d+"]/input")).click();
			String labelName = driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody/tr["+r+"]/td["+d+"]/label")).getText();
			System.out.println(labelName+" item is dropped by MSL");
			
			if (cf.isElementVisible(By.xpath("//span[@ng-switch-when='ProductFullName']"), "Selected item is displayed"))
			{
				String quantity = driver.findElement(By.xpath("//span[@ng-switch-when='Quantity_vod__c']")).getText();
				if(!(quantity.equals("1")))
					cf.setData(By.xpath("//span[@ng-switch-when='Quantity_vod__c']/input"), "Quantity", "1");
			}
			else
				report.updateTestLog("MSL Leave Behind","Selected item is not seen",Status.FAIL);
			
		}
		else
			report.updateTestLog("MSL Leave Behind","MSL Leave Behind section is not present",Status.WARNING);
		
	//Other Print Materials
		if(cf.isElementVisible(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody[2]/tr[1]//h3[text()='Other Print Materials']"), "Other Print Materials"))
		{
			cf.clickButton(By.xpath("//h3[text()='Other Print Materials']/parent::div/img"), "Expand Other Print Materials");
			driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody[2]/tr[2]/td/input")).click();
			String material = driver.findElement(By.xpath("//table[@name='call2_sample_vod__c']//table/tbody[2]/tr[2]/td/label")).getText().trim();
			System.out.println(material+" is selected");
			report.updateTestLog("Other Print Materials","Other Print Materials are added",Status.DONE);
			
		}
		else
			report.updateTestLog("Other Print Materials","Other Print Materials section is not present",Status.WARNING);
		report.updateTestLog("Items dropped","Items dropped section is entered",Status.SCREENSHOT);
	}

	/**
	 * This method is for validating whether the mandatory fields are auto populated
	 * @throws Exception
	 */
	public void mandatoryFields() throws Exception
	{
		String account = dataTable.getData("RecCall", "Account");
		String acc = driver.findElement(By.xpath("//*[@name='Account_vod__c']/a")).getText();
		String userType = dataTable.getData("RecCall", "Account Record Type");
		
		if(userType.equals("HCP"))
		{
			String[] hcp = account.split(" ");
			String[] prof = acc.split(",");
			if((hcp[0].trim().equals(prof[1].trim())) && (hcp[1].trim().equals(prof[0].trim())))
				report.updateTestLog("Call Recording - Account","Account is autopopulated",Status.DONE);
		}
		else{
			if(account.equals(acc))
				report.updateTestLog("Call Recording - Account","Account is autopopulated",Status.DONE);
			else
				report.updateTestLog("Call Recording - Account","Account is not autopopulated",Status.FAIL);
		}
		//Delivery channel		
		Select delvch = new Select(driver.findElement(By.id("Delivery_Channel_AZ__c")));
		String defaultdeliverchannel=delvch.getFirstSelectedOption().getText();
		if(defaultdeliverchannel.equalsIgnoreCase("F2F"))
			report.updateTestLog("Call Recording - Delivery channel","Delivery channel is defaulted to F2F",Status.DONE);
		else
			report.updateTestLog("Call Recording - Delivery channel","Delivery channel is not defaulted to F2F",Status.FAIL);
			
		//date and time
		vf.dateValidationCalls();
		
		//Initiated By
		Select init = new Select(driver.findElement(By.id("Initiated_by_AZ__c")));
		String initiatdBy = init.getFirstSelectedOption().getText();
		if(initiatdBy.equals("Diagnostics"))
			report.updateTestLog("Call Recording - Initiated By","Initiated by field is mandatory and is auto populated",Status.DONE);
		else
			report.updateTestLog("Call Recording - Initiated By","Initiated by field is is not having proper value",Status.FAIL);
		//venue
		Select v = new Select(driver.findElement(By.id("MA_Venue_AZ__c")));
		String venue = v.getFirstSelectedOption().getText();
		if(venue.equals("Office/Hospital/Lab"))
			report.updateTestLog("Call Recording - Venue","Venue field is mandatory and is auto populated",Status.DONE);
		else
			report.updateTestLog("Call Recording - Venue","Venue field is not having proper value",Status.FAIL);
	}
	/**
	 * This method is save the call and validating the save page 
	 * @throws Exception
	 */
	public void saveCallDxL() throws Exception
	{
		String label = dataTable.getData("RecCall", "Offlabel");		
		cf.clickSFButton("Save");
		cf.waitForSeconds(3);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Diagnostic Interaction']")));
		String callID = driver.findElement(By.xpath("//div[@class='bPageTitle']//h2")).getText();
		System.out.println("Call ID is "+callID);
		dataTable.putData("RecCall", "Call ID", callID);
		
		String status = driver.findElement(By.name("Status_vod__c")).getText();
		if(status.equals("Saved"))
			report.updateTestLog("Saved Call", "Status is saved", Status.PASS);
		else
			report.updateTestLog("Saved Call", "Status is not saved", Status.FAIL);
		if(!label.equalsIgnoreCase("Yes"))
		{
			String product_1 = dataTable.getData("RecCall", "Product_1");
			String product_2 = dataTable.getData("RecCall", "Product_2");
			String pdt_1 = driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[2]/td[2]")).getText();
			String pdt_2 = driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[3]/td[2]")).getText();
			
			if((pdt_1.contains(product_1))&&(pdt_2.contains(product_2)))
				report.updateTestLog("Saved Call", "Products entered are present", Status.DONE);
			else
				report.updateTestLog("Saved Call", "Products entered are not present", Status.FAIL);
		}
		else
		{
			String product = dataTable.getData("RecCall", "Product_2");
			String pdt= driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[2]/td[2]")).getText();
			if(pdt.contains(product))
				report.updateTestLog("Saved Call", "Products entered are present", Status.DONE);
			else
				report.updateTestLog("Saved Call", "Products entered are not present", Status.FAIL);
		}
			
	}
	
	/**
	 * This method is for adding additional attendees
	 * @throws Exception
	 */
	public void addattendes(String attendee) throws Exception
	{
		cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendesSearch, "Search");
		cf.setData(ObjVeevaCallsStandardization_Commercial.attendeesearchinput, "Attendee Name", attendee);
		cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendeesearchgo, "Go");
		cf.waitForSeconds(3);
		cf.clickLink(ObjVeevaCallsStandardization_Commercial.attendeecheckbox, "Select Attendee");
		report.updateTestLog("Verify able to see Attendee is selected", "Able to see attendee is selected", Status.SCREENSHOT);
		cf.clickButton(ObjVeevaCallsStandardization_Commercial.addattendee, "Add Attendee");
		cf.waitForSeconds(3);
	}
	
	
	/**
	 * This method is submitting the call and validating the submit page and the related list
	 * @throws Exception
	 */
	public void submitCall() throws Exception
	{
		String label = dataTable.getData("RecCall", "Offlabel");
		cf.clickSFButton("Edit");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Diagnostic Interaction']")));
		report.updateTestLog("Submit Call", "Call to be submitted", Status.SCREENSHOT, "Full Screen", driver);
		cf.clickSFButton("Submit");
		cf.waitForSeconds(3);
		
		//page layout check
		String pageName = driver.findElement(By.xpath("//div[@class='bPageTitle']//h1")).getText();
		if(label.equalsIgnoreCase("Yes"))
		{
			if(pageName.equalsIgnoreCase("Diagnostic Comm Interaction"))
				report.updateTestLog("Page Layout", "Offlabel product page layout "+pageName+" is loaded", Status.PASS);
			else
				report.updateTestLog("Page Layout", "Offlabel product page layout "+pageName+" is not loaded", Status.FAIL);
		}
		else
		{
			if(pageName.equalsIgnoreCase("Diagnostic Interaction"))
				report.updateTestLog("Page Layout", "Onlabel product page layout "+pageName+" is loaded", Status.PASS);
			else
				report.updateTestLog("Page Layout", "Onlabel product page layout "+pageName+" is not loaded", Status.FAIL);
		}
		
		String status = driver.findElement(By.name("Status_vod__c")).getText();
		if(status.equals("Submitted"))
			report.updateTestLog("Submit Call", "Status is submitted", Status.DONE);
		else
			report.updateTestLog("Submit Call", "Status is not as submitted", Status.FAIL);
		
		String attendeeName = driver.findElement(By.xpath("//*[@name='zvod_Attendees_vod__c']//table//td[2]/a")).getText().trim();
		String[] attnedee = attendeeName.split(",");
		System.out.println(attnedee[0]);
		System.out.println(attnedee[1]);
		String att = dataTable.getData("RecCall", "Attendee");
		String[] atte = att.split(" ");
		System.out.println(atte[0]);
		System.out.println(atte[1]);
		
		if((attnedee[0].trim().equals(atte[1].trim())) && (attnedee[1].trim().equals(atte[0].trim())))
			report.updateTestLog("Submit Call", "Attendee Name is present", Status.DONE);
		else
			report.updateTestLog("Submit Call", "Attendee Name is incorrect", Status.FAIL);
		
		if(!label.equalsIgnoreCase("Yes"))
		{
			String product_1 = dataTable.getData("RecCall", "Product_1");
			String product_2 = dataTable.getData("RecCall", "Product_2");
			String pdt_1 = driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[2]/td[2]")).getText();
			String pdt_2 = driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[3]/td[2]")).getText();
			
			if((pdt_1.contains(product_1))&&(pdt_2.contains(product_2)))
				report.updateTestLog("Submit Call", "Products entered are present", Status.DONE);
			else
				report.updateTestLog("Submit Call", "Products entered are not present", Status.FAIL);
		}
		else
		{
			String product = dataTable.getData("RecCall", "Product_2");
			String pdt= driver.findElement(By.xpath("//div[@name='zvod_Detailing_vod__c']/table//tr[2]/td[2]")).getText();
			if(pdt.contains(product))
				report.updateTestLog("Submit Call", "Products entered are present", Status.DONE);
			else
				report.updateTestLog("Submit Call", "Products entered are not present", Status.FAIL);
		}
		
		//Validate the related list

		cf.clickLink(By.xpath("//*[@name='Account_vod__c']/a"), "Account Name link");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Account']")));
		cf.clickSFLinkLet("Calls");
		String callID = dataTable.getData("RecCall", "Call ID");
		cf.verifyDataFromLinkLetTable("Calls", "Call Name", callID, "Status", "Submitted");
		String product_1 = dataTable.getData("RecCall", "Product_1");
		String product_2 = dataTable.getData("RecCall", "Product_2");
		String getPdt = cf.getDataFromLinkLetTable("Calls", "Call Name", callID, "Detailed Products");
		System.out.println(getPdt);
		
		
		
		if(!(label.equalsIgnoreCase("yes")))
		{
			String[] pdt = getPdt.split(" ");
			if((pdt[0].trim().contains(product_1)) && (pdt[1].trim().contains(product_2)))
			{
				report.updateTestLog("Submit Call", "Product "+product_1+" is presesnt", Status.DONE);
				report.updateTestLog("Submit Call", "Product "+product_2+" is presesnt", Status.DONE);
			}
			else
			{
				report.updateTestLog("Submit Call", "Product "+product_1+" is not presesnt", Status.FAIL);
				report.updateTestLog("Submit Call", "Product "+product_2+" is not presesnt", Status.FAIL);
			}
		}
		else
		{
			if((getPdt.contains(product_2)))
			{
				report.updateTestLog("Submit Call", "Product "+getPdt+" is presesnt", Status.DONE);
			}
			else
			{
				report.updateTestLog("Submit Call", "Product "+getPdt+" is not presesnt", Status.FAIL);
			}
		}
			
		
	}
	
	/**
	 * This method is to verify the call recorded by DxL user by logging in as sales rep for an off-label product
	 * @throws Exception
	 */
	public void repVerify() throws Exception
	{
		String callID = dataTable.getData("RecCall", "Call ID"); 
		String owner = dataTable.getData("RecCall", "OwnerName");
		String offPdt = dataTable.getData("RecCall", "Product_2");
		String label= dataTable.getData("RecCall", "Offlabel");
		cf.clickSFLinkLet("Calls");
		cf.verifyDataFromLinkLetTable("Calls", "Call Name", callID, "Status", "Submitted");
		cf.verifyDataFromLinkLetTable("Calls", "Call Name", callID, "Owner Name", owner);
		cf.clickLinkFromLinkLetTable("Calls", "Call Name", callID);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='"+callID+"']")));
		String pageName = driver.findElement(By.xpath("//div[@class='bPageTitle']//h1")).getText();
		if(label.equalsIgnoreCase("yes"))
		{
			if(pageName.equals("Diagnostic Comm Interaction"))
			{
				report.updateTestLog("Verify Call", "Diagnostic Comm Interaction page is displayed", Status.DONE);
				if(cf.isElementNotVisible(By.xpath("//*[contains(text(),'"+offPdt+"')]"), "Product", 2))
					report.updateTestLog("Off label product should not be present", "OffLabel product "+offPdt+" is not present", Status.PASS);
				else
					report.updateTestLog("Off label product should not be present", "OffLabel product "+offPdt+" is present", Status.FAIL);
			}
			else
				report.updateTestLog("Verify Call", "Diagnostic Comm Interaction page not displayed", Status.FAIL);
		}
		else //on label product check
		{
			String product_1 = dataTable.getData("RecCall", "Product_1");
			String product_2 = dataTable.getData("RecCall", "Product_2");			
			if(pageName.equals("Diagnostic Interaction"))
			{
				report.updateTestLog("Verify Call", "Diagnostic Interaction page is displayed", Status.DONE);
				if((cf.isElementVisible(By.xpath("//*[contains(text(),'"+product_1+"')]"), product_1)) && (cf.isElementVisible(By.xpath("//*[contains(text(),'"+product_2+"')]"), product_2)) )
					report.updateTestLog("On label product should be present", "OnLabel product "+product_1+" and "+product_2+" are present", Status.PASS);
				else
					report.updateTestLog("On label product should be present", "OnLabel product "+product_1+" and "+product_2+" are not present", Status.FAIL);
			}
			else
				report.updateTestLog("Verify Call", "Diagnostic Interaction page not displayed", Status.FAIL);
			
		}
		
	}
}//End of Class