package businesscomponents;


import supportlibraries.*;

import com.mfa.framework.Status;

import ObjectRepository.objHeaderPage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Veeva_US_DCR  extends ReusableLibrary{

	//private ConvenienceFunctions cf = new ConvenienceFunctions(driver, report, dataTable);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	public Veeva_US_DCR(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Navigating to DIR Linklet and click on DIR Button.
	 * @throws Exception
	 */
	public void newDCR() throws Exception{
		driver.findElement(By.cssSelector("input[value='New']")).click();
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'New Data Change Request')]"), "New Data Change Request"))
			report.updateTestLog("Verify able to navigate to New Data Change Request Record Type", "Able to navigate to New Data Change Request Record Type", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to New Data Change Request Record Type", "Unable to navigate to New Data Change Request Record Type.", Status.FAIL);
	}

	/**
	 * This method is used for clicking on New button in My Accounts page and verify if new account page is displayed
	 */
	public void newAccountDCR() throws Exception{
		cf.switchToFrame("itarget");
		cf.clickSFButton("New", "button");
		cf.switchToParentFrame();
		cf.switchToFrame("itarget");
		cf.selectData(By.id("recordTypeSelect"), "Record Type Select", dataTable.getData("Login", "AccountType"));
		verifyRecordtypesInNewAccount();
		cf.clickSFButton("Continue", "button");
		cf.switchToParentFrame();

		cf.switchToFrame("itarget");
		if(cf.isElementVisible(By.xpath("//h3[contains(text(),'Account Edit')]"), "Account Edit"))
			report.updateTestLog("Verify able to navigate to Account Edit", "Able to navigate to New Account DCR request creation", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to Account Edit", "Unable to navigate to New Account DCR Request creation.", Status.FAIL);
		cf.switchToParentFrame();

	}
	/**
	 * This method is used to navigate to DIR tab
	 */
	public void navigateToDCRTab() throws Exception{
		cf.clickButton(objHeaderPage.plussymbol, "Plus Symbol");
		cf.clickButton(By.cssSelector("img[title*='Data Change Requests']"), "Data Change Request");
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'Data Change Request')]"), "New Data Change Request"))
			report.updateTestLog("Verify able to navigate to Data Change Request Tab", "Able to navigate to Data Change Request tab", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to Data Change Request tab", "Unable to navigate to Data Change Request tab.", Status.FAIL);
	}

	/**
	 * Verify the record types displayed in creation of new account in Veeva US page
	 */
	public void verifyRecordtypesInNewAccount() throws Exception{
		ArrayList<String> recordTypes=new ArrayList<String>(Arrays.asList("Hospital","Hospital Department","Organization","Pharmacy","OutPatient Entity","Payer","Prescriber","Non-Prescriber","IDN"));
		List<WebElement> appRecordTypes=driver.findElements(By.cssSelector("select#recordTypeSelect > option"));
		int i=0;
		boolean blnEquals=true; //blnBreak=false;
		String strMissingRecordTypes="";
		for(String strRecordType : recordTypes){
			i=0;
			for(WebElement recordType : appRecordTypes){
				String appRecordType=recordType.getText();i++;
				if(strRecordType.trim().equalsIgnoreCase(appRecordType.trim()))					break;
				
				else if(i==appRecordTypes.size()){
					strMissingRecordTypes=strMissingRecordTypes+ ", " + strRecordType;
					blnEquals=false;
				}
			}
			//if(blnBreak)	break;
		}
		if (blnEquals)
			report.updateTestLog("Validate Record Types in New Account", "All record types are displayed as expected", Status.PASS);
		else
			report.updateTestLog("Validate Record Types in New Account", "Record types : '" + strMissingRecordTypes +"' are not present in drop down", Status.FAIL);
	}

	/**
	 * This method is used to select a specific record type passed from datatable in New Account creation page
	 */
	public void selectRecordTypeInAccountCreationPage() throws Exception{
		String strRecordType=dataTable.getData("Login", "AccountType");
		cf.selectData(By.cssSelector("select#recordTypeSelect"), strRecordType, strRecordType);
		cf.clickSFButton("Continue", "button");
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'Account Edit')]"), "Account Edit Page"))
			report.updateTestLog("Verify Accounts Edit Page", "Entering details for New Account creation page is displayed successfully", Status.PASS);
		else
			report.updateTestLog("Verify Accounts Edit Page", "New Account creation details entry page is not displayed", Status.FAIL);
	}

	/**
	 * Verify if the mandatory fields are displayed in accounts creation entry page
	 */
	public void verifyMandatoryFieldsInAccountsCreationPage() throws Exception{
		cf.switchToFrame("itarget");
		cf.verifySFElementData("Account Required Information", "Record Type", "dcr-text", "text", "Hospital");
		cf.verifySFElementData("Account Required Information", "Name", "dcr-textbox", "id", "reqreqi-value:string:Account:Name");
		cf.verifySFElementData("Account Required Information", "Customer Sub Type", "dcr-select", "id", "reqreqi-value:picklist:Account:Customer_Sub_Type_AZ_US__c");
		cf.verifySFElementData("Account Required Information", "Customer Type", "dcr-select", "id", "reqreqi-value:picklist:Account:Customer_Type_AZ_US__c");
		cf.verifySFElementData("Account Required Information", "Territory", "dcr-select", "id", "Account:Territory_vod__c");
		cf.verifySFElementData("Address Required Information", "Address line 1", "dcr-textbox", "id", "reqreqi-value:string:Address_vod__c:Name");
		cf.verifySFElementData("Address Required Information", "Suite# / Address line 2", "dcr-textbox", "id", "i-value:string:Address_vod__c:Address_line_2_vod__c");
		cf.verifySFElementData("Address Required Information", "City", "dcr-textbox", "id", "i-value:string:Address_vod__c:City_vod__c");
		cf.verifySFElementData("Address Required Information", "State", "dcr-select", "name", "i-value:picklist:Address_vod__c:State_vod__c");
		//	cf.verifySFElementData("Address Required Information", "Address", "dcr-textbox", "id", "reqreqi-value:string:Address_vod__c:Zip_vod__c");
		cf.verifySFElementData("Address Required Information", "Country", "dcr-select", "name", "i-value:picklist:Address_vod__c:Country_vod__c");
		//	cf.verifySFElementData("Address Required Information", "Address Type(s)", "dcr-textbox", "id", "reqreqi-value:multipicklist:Address_vod__c:Address_Type_AZ_US__c");//removed in application
		cf.verifySFElementData("Notes", "Notes", "dcr-textarea", "id", "dcrNotes");
		cf.verifyPageElement(By.xpath("//h3[contains(text(),'Notes')]/parent::div/following-sibling::span"), "Notes Information", "dcr-text", dataTable.getData("DataChangeRequest", "NotesInformationRedText"), "equalsignorecase", true);
		cf.switchToParentFrame();
	}

	/**
	 * This method is to fill in mandatory fields in DCR creation page
	 */
	public void enterMandatoryFieldsForDCRCreation() throws Exception{
		cf.switchToFrame("itarget");
		String strAccountName="DCRRequestAuto" + new SimpleDateFormat("MMddYYYY").format(new Date());
		cf.setSFData("Account Required Information", "Name", "dcr-textbox", strAccountName);
		cf.setSFData("Account Required Information", "Customer Type", "dcr-select",dataTable.getData("Login", "AccountType"));
		cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("Account Required Information", "Customer Sub Type", "dcr-select"), "Customer Sub Type");
		cf.setSFData("Address Required Information", "Address line 1", "dcr-textbox", dataTable.getData("DataChangeRequest", "AddressLine1"));
		cf.setSFData("Address Required Information", "Suite# / Address line 2", "dcr-textbox", dataTable.getData("DataChangeRequest", "AddressLine2"));
		cf.setSFData("Address Required Information", "City", "dcr-textbox", dataTable.getData("DataChangeRequest", "City"));
		cf.setSFData("Address Required Information", "State", "dcr-select", dataTable.getData("DataChangeRequest", "State"));
		cf.setSFData("Address Required Information", "Zip", "dcr-textbox", dataTable.getData("DataChangeRequest", "Zip"));
		//	cf.setSFData("Address Required Information", "Address Type", "dcr-multiselect", dataTable.getCommonData("DataChangeRequest", "AddressType(s)"));
		cf.setSFData("Address Required Information", "Country", "dcr-select", dataTable.getData("DataChangeRequest", "Country"));
		String strAutoGeneratedNotes="Automation New Account " + new SimpleDateFormat("MMddYYYYHHmm").format(new Date());
		cf.setSFData("Notes", "Notes", "dcr-textarea",strAutoGeneratedNotes );
		dataTable.putData("DataChangeRequest", "Notes", strAutoGeneratedNotes);
		cf.switchToParentFrame();
	}

	/**
	 * This method is used click on submit button in DCR creation page
	 */
	public void clickSubmitButton() throws Exception{
		cf.switchToFrame("itarget");
		cf.clickSFButton("Submit", "button");
		cf.switchToParentFrame();
	}


	/**
	 * This method is used to enter random notes information in DIR creation page
	 * @throws Exception
	 */
	public void enterNotesInformationInDCR() throws Exception{
		String strAutoGeneratedNotes="This DIR is created for automation pupose at " + new SimpleDateFormat("ddmmss").format(new Date()).toString();
		cf.setData(By.id("dcrNotes"), "Notes", strAutoGeneratedNotes);
		dataTable.putData("DataChangeRequest", "Notes", strAutoGeneratedNotes);
	}
	/**
	 * This method is used to click on Save in DIR Creation page
	 */
	public void clickSaveInDCRCreation() throws Exception{
		cf.switchToFrame("itarget");
		cf.clickSFButton("Save", "submit");
		cf.switchToParentFrame();
	}

	/**
	 * This method is used to navigate to Reports tab for viewing DCR
	 */
	public void navigateToReports() throws Exception{
		vf.clickVeevaTab("Reports");
		cf.setData(By.cssSelector("#folderTreePanel input.quickfindInput"), "Find Reports", "AZ DCR User Reports");
		cf.waitForSeconds(3);
		cf.clickButton(By.cssSelector("#folderTreePanel a.x-tree-node-anchor"), "AZ DCR User Report Node");
	}

	/**
	 * This method is used to view new customer created DCRs in Reports tab
	 */
	public void viewNewCreatedDCRs() throws Exception{
		int i=0, iNotesCol=-1, iDCRColumn=-1;
		String strNotes=dataTable.getData("DataChangeRequest", "Notes");
		String strDCRRequestName="";
		cf.waitForSeconds(5);
		cf.clickButton(By.xpath("//span[contains(text(),'My DCRs')]/parent::a"), "My DCRs");
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'My DCRs')]"), "DCR Reports for New Customer Page")){
			List<WebElement> headerRowElements=driver.findElements(By.cssSelector(".bGeneratedReport .reportTable [id*=headerRow] > th"));
			for(WebElement headerElement : headerRowElements){
				if(headerElement.getText().trim().equalsIgnoreCase("Notes")){
					iNotesCol=i;
					break;
				}
				i++;
			}//END OF NOTES COLUMN FIND FOR LOOP
			i=0;
			for(WebElement headerElement : headerRowElements){
				if(headerElement.getText().trim().contains("Data Change Request Name")){
					iDCRColumn=i;
					break;
				}
				i++;
			}//END OF DCR COLUMN FIND LOOP
			List<WebElement> tableRows=driver.findElements(By.cssSelector(".bGeneratedReport .reportTable tr"));
			for(i=1;i<tableRows.size();i++){
				List<WebElement> dataElements=tableRows.get(i).findElements(By.tagName("td"));
				if(strNotes.trim().equalsIgnoreCase(dataElements.get(iNotesCol).getText().trim())){
					strDCRRequestName=dataElements.get(iDCRColumn).findElement(By.tagName("a")).getText();
					dataTable.putCommonFieldData("DataChangeRequest", "DCRRequestName", strDCRRequestName);
					break;
				}
				else if(i==tableRows.size()-1)
					report.updateTestLog("Update DCR Request ID", "DCR request name is not found", Status.FAIL);
			}//END OF TABLE ROWS FOR LOOP
		}//END OF IF CONDITION
	}

	/**
	 * This method is used to click on Address linklet and click on New address
	 */
	public void clickNewAddress() throws Exception{
		cf.clickSFLinkLet("Addresses");
		cf.clickSFButton("New", "button");
		if(cf.isElementVisible(By.xpath("//h1[contains(text(),'New Address')]"), "New Address"))
			
			report.updateTestLog("Verify able to navigate to New Address", "Able to navigate to New Address page", Status.PASS);
		else
			report.updateTestLog("Verify able to naviagte to New Address", "Unable to navigate to New Address page", Status.FAIL);
		//cf.verifyActualExpectedSFData("heading", "New Address Page", "New Address", "text", "equalsignorecase");
	}

	/**
	 * This method is used to verify elements in New Address page
	 */
	public void verifyElementsInNewAddressPage() throws Exception{
		String strAccount=dataTable.getData("Login", "AccountName");
		cf.verifyActualExpectedData("Account", By.xpath("//div[contains(@class,'pbSubheader')]//*[text()='Information']/following::div[1]//*[contains(text(),'Account')]//following::td/a"), "text", strAccount, "equalsIgnoreCase");
		cf.verifyActualExpectedData("Verified Checkbox", By.xpath("//div[contains(@class,'pbSubheader')]//*[text()='Information']/following::div[1]//*[contains(text(),'Verified')]/parent::td/following-sibling::td//input"), "disabled", "disabled", "equalsIgnoreCase");

		//elements under Information part 2
		cf.verifySFElementData("Information part 2", "Address line 1", "textbox", "id", "reqi-value:string:Address_vod__c:Name");
		cf.verifySFElementData("Information part 2", "Address line 2", "textbox", "id", "i-value:string:Address_vod__c:Address_line_2_vod__c");
		cf.verifySFElementData("Information part 2", "Address line 3", "textbox", "id", "i-value:string:Address_vod__c:Address_line_3_AZ__c");

		//elements under Information part 3
		cf.verifySFElementData("Information part 3", "City", "textbox", "id", "reqi-value:string:Address_vod__c:City_vod__c");
		cf.verifySFElementData("Information part 3", "State", "select", "id", "reqi-value:picklist:Address_vod__c:State_vod__c");
		cf.verifySFElementData("Information part 3", "Zip", "textbox", "id", "reqi-value:string:Address_vod__c:Zip_vod__c");
		cf.verifySFElementData("Information part 3", "Zip + 4", "div", "id", " ");
		cf.verifyOptionsPresentinSelectBox(By.xpath("//*[contains(text(),'Country')]//following::select[contains(@id,'Country')]"), "Country", "--None--;USA");

		//elements under Type
		cf.verifySFElementData("Type", "Address Type", "multiselect", "text", "Calling;Mailing;Shipping;Billing;Sampling;Other;");
	}

	public void populateMandatoryFieldsInNewAddress() throws Exception{
		cf.switchToFrame("vod_iframe");
		
		cf.setSFData("Information part 2", "Address line 1", "dcr-select",dataTable.getData("DataChangeRequest", "AddressLine1"));
		cf.setSFData("Information part 2", "Suite# / Address line 2", "dcr-textbox", dataTable.getData("DataChangeRequest", "AddressLine2"));
		
		cf.setSFData("Information part 3", "City", "dcr-textbox", dataTable.getData("DataChangeRequest", "City"));
		cf.setSFData("Information part 3", "State", "dcr-select", dataTable.getData("DataChangeRequest", "State"));
		cf.setSFData("Information part 3", "Zip", "dcr-textbox", dataTable.getData("DataChangeRequest", "Zip"));
		cf.setSFData("Information part 3", "Country", "dcr-select", dataTable.getData("DataChangeRequest", "Country"));
		
		cf.setSFData("Type", "Address Type", "dcr-multiselect", dataTable.getData("DataChangeRequest", "AddressType(s)"));
		
		String strAutoGeneratedNotes="Automation New Address " + new SimpleDateFormat("MMddYYYYHHmm").format(new Date());
		cf.setSFData("Data Change Request", "Notes", "dcr-textarea",strAutoGeneratedNotes );
		dataTable.putData("DataChangeRequest", "Notes", strAutoGeneratedNotes);
		cf.switchToParentFrame();
	}
	/**
	 * Validate the affliations Affiliations HCA -Affiliations HCA/HCP
	 * @throws Exception 
	 */
	public void validateaffliations() throws Exception{
		Boolean value=false;
		cf.clickSFLinkLet("Affiliations HCA");
		if(cf.isElementVisible(By.xpath("//h3[.='Affiliations HCA']"), "Affliations HCA")){
			if(cf.isElementVisible(By.xpath("//h3[.='Affiliations HCA/HCP']"), "Affiliations HCA/HCP"))
				value=true;
		}

		if(value == true)
			report.updateTestLog("Verify able to see affliation related list displays on  Business account page layout", "Able to see affliation related list displays on  Business account page layout", Status.PASS);
		else
			report.updateTestLog("Verify able to see affliation related list displays on  Business account page layout", "Unable to see affliation related list displays on  Business account page layout", Status.FAIL);
	}

	/**
	 * Verify and Click New Child Account in Affiliations HCA/HCP.
	 * @throws Exception 
	 */
	public void verifyclickchildAccount() throws Exception{

		if(cf.isElementVisible(By.xpath("//input[@value='New Child Account']"), "New Child Account")){
			report.updateTestLog("Verify able to see New Child Account", "Able to see New Child Account Button", Status.PASS);
			cf.clickButton(By.xpath("//input[@value='New Child Account']"), "New Child Account Button");
			cf.waitForSeconds(5);
			cf.switchToFrame(By.id("vod_iframe"), "Child Account Page");
			if(cf.isElementVisible(By.xpath("//h3[.='New Child Account']"), "New Child Account Page"))
				report.updateTestLog("Verify able to see New Child Account", "Able to see New Child Account Page", Status.PASS);
			else
				report.updateTestLog("Verify able to see New Child Account", "Unable to see New Child Account Page", Status.PASS);
		}
		else
			report.updateTestLog("Verify able to see New Child Account", "Unable to see New Child Account Button", Status.FAIL);
	}

	/**
	 * Fill details for the New Child Account Page.
	 * @throws Exception 
	 */
	public void fillChildaccountdetails() throws Exception{

		String parentaccount=cf.getData(By.xpath("//input[@name='reqi-value:reference:Child_Account_vod__c:Parent_Account_vod__c']"), "Parent Account", "value");
		String childaccount=dataTable.getData("Login", "AccountName");
		if(!parentaccount.equals("")){
			report.updateTestLog("Verify able to see value in parent Account", "Able to see value in Paarent Account", Status.PASS);
			searchlookupaccount(childaccount);
			cf.switchToFrame(By.id("vod_iframe"), "Child Account Page");
			if(cf.isElementVisible(By.xpath("//label[.='Role Group : Role Type']"), "Role Group:Role Type")){
				report.updateTestLog("Verify able to see Role Group:Role Type", "Able to see Role Group:Role Type", Status.PASS);
				selectroleGroupValidation();
				enterNotesInformationInDCR();
				cf.clickButton(By.xpath("//input[@id='i-value:boolean:Child_Account_vod__c:Inactive_AZ_US__c']"), "DeleteAffliation Block");
				validateerror();
				clickSaveInDCRCreation();
			}
			else
				report.updateTestLog("Verify able to see Role Group:Role Type", "Able to see Role Group:Role Type", Status.FAIL);  
		}
		else
			report.updateTestLog("Verify able to see value in parent Account", "Unable to see value in Paarent Account",Status.FAIL); 
	}

	/**
	 * Validate the values on Role Group and Role Type.
	 * @throws Exception 
	 */
	public void selectroleGroupValidation() throws Exception{

		Select se= new Select(driver.findElement(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Group_Code_AZ_US__c']")));
		String defaultoption=se.getFirstSelectedOption().getText();
		if(defaultoption.equals("--None--")){
			// if(cf.isElementVisible(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Type_Code_AZ_US__c']"), "Role Type")
			Boolean roletype=driver.findElement(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Type_Code_AZ_US__c']")).isEnabled();
			if(roletype.equals(false)){
				report.updateTestLog("Verify able to see the Role Type as disabled when Role Group Type is None.", "Able to see Role Type as disabled when Role Group Type is None", Status.PASS);
				cf.selectARandomOptionFromComboBox(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Group_Code_AZ_US__c']"), "Role Group");
				Boolean roletype1=driver.findElement(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Type_Code_AZ_US__c']")).isEnabled();
				if(roletype1.equals(true)){
					report.updateTestLog("Verify able to see the Role Type as enabled when Role Group Type is Random selected value.", "Able to see Role Type as enabled when Role Group Type is random selected value", Status.PASS);  
					cf.selectARandomOptionFromComboBox(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Type_Code_AZ_US__c']"), "Role Type");

				}	else
					report.updateTestLog("Verify able to see the Role Type as enabled when Role Group Type is Random selected value.", "Unable to see Role Type as enabled when Role Group Type is random selected value", Status.FAIL);  


			} else
				report.updateTestLog("Verify able to see the Role Type as disabled when Role Group Type is None.", "Unable to see Role Type as disabled when Role Group Type is None", Status.FAIL); 
		}
	}

	/**
	 * Validate Error Message in DataChange Requests Module.
	 * @throws Exception 
	 */
	public void validateerror() throws Exception{
		String error="Provide details to help facilitate this request, such as external IDs, relevant weblinks, etc. Screenshots are not supported.";
		String actualdata=driver.findElement(By.id("notesMessage")).getText();
		cf.verifyActualExpectedData("Validate Error Message", actualdata, error);
	}

	/**
	 * Searching the value in lookup
	 * @param account
	 * @throws Exception
	 */
	public void searchlookupaccount(String account) throws Exception{

		try{
			driver.findElement(By.xpath("//input[@name='i-value:reference:Child_Account_vod__c:Child_Account_vod__c']/following-sibling::a ")).click();
		}
		catch(Exception e){
			driver.findElement(By.xpath("//input[@name='reqi-value:reference:Child_Account_vod__c:Child_Account_vod__c']/following-sibling::a ")).click();
		}
		cf.waitForSeconds(15);
		String parentwindow=driver.getWindowHandle();
		Set <String> childwindows=driver.getWindowHandles();
		for(String window:childwindows){
			if(!(parentwindow.equals(window))){
				driver.switchTo().window(window);
				String searchTitle = driver.getTitle();
				if(searchTitle.equals("Search")){
					//cf.switchToParentFrame();
					driver.switchTo().frame("frameSearch");
					cf.setData(By.id("searchBox"), "Input of Account Name", account);
					cf.clickLink(By.name("searchButton"), "GO");
					cf.waitForSeconds(12);
					cf.switchToParentFrame();
					cf.switchToFrame(By.id("frameResult"), "Result Frame");

					if(cf.isElementVisible(By.xpath("//table[@id='listTableId']//th//a[.='"+account.trim()+"']"), account)){
						report.updateTestLog("Verify able to see Account Name", "Able to see Account Name", Status.PASS);
						driver.findElement(By.xpath("//table[@id='listTableId']//th//a[.='"+account.trim()+"']")).click();
						cf.waitForSeconds(10);
						driver.switchTo().window(parentwindow);

					}
					else{
						report.updateTestLog("Verify able to see the Account Name", "Unable to see Account Name", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
				else{
					report.updateTestLog("Verify able to switch to Account window", "Unable to switch to Account Window", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
		}
	}

	/**
	 * Search and Navigate to Account for DS User.
	 * @throws Exception 
	 */
	public void searchandnavigateUS(String type,String value) throws Exception{

		cf.setData(By.xpath("//input[@title='Search...']"), "Search", value);
		cf.clickButton(By.id("phSearchButton"), "Search");
		cf.clickLink(By.xpath("//span[.='"+type+"']"), type);
		cf.clickLink(By.xpath("//a[.='"+value+"']"), value);
		if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+value+"')]"), "Navigate to Account Page"))
			report.updateTestLog("Verify able to navigate to  "+value+"", "able to navigate to "+value+"", Status.PASS);
		else
			report.updateTestLog("Verify able to navigate to account "+value+"", "Unable to navigate to"+value+"", Status.FAIL);
	}

	/**
	 * Login with DS edit the DCR and Change the Status.
	 * @param status - Which status u need to be Changed.
	 * @throws Exception
	 */
	public void editchangestatus(String status) throws Exception{

		cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@name='edit']"), "Edit Button in DCR");
		cf.selectData(By.id("00NU0000003vV5P"), "Status", status);
		cf.clickButton(By.xpath("//td[@id='topButtonRow']/input[@name='save']"), "Save");
	}

	/**
	 * Validate the individual changed status in Data Change Request History.
	 * @throws Exception
	 */
	public void validateDCRLine(String previousstatus,String ChangedStatus) throws Exception{
		cf.clickSFLinkLet("Data Change Request History");
		int rows;
		String appaction,action = null;
		Boolean validate=false;
		if(cf.isElementVisible(By.xpath("//h3[.='Data Change Request History']"),"Linklet")){
			rows=cf.getRowCountFromLinkLetTable("Data Change Request History");
			if(rows>1){
				for(int j=2;j<rows+1;j++){
					appaction =cf.getDataFromLinkLetTable("Data Change Request History", "Action", j);
					action = "Changed Status from "+previousstatus+" to "+ChangedStatus+".";
					if(action.equals(appaction)){
						validate=true;
						break;

					}
				}
			}
			else
				report.updateTestLog("Verify able to see DIR's for the account in related list.", "There are no DIR's created in related list.", Status.FAIL);
		}
		else
			report.updateTestLog("Verify able to see the Data Change Request History Related List.", "Unable to see the Data Change Request History Related List", Status.FAIL);

		if(validate == true)
			report.updateTestLog("Verify able to see DCR Line item "+action+"", "Able to see DCR Line item "+action+"", Status.PASS);
		else
			report.updateTestLog("Verify able to see DCR Line item "+action+"", "Unable to see DCR Line item "+action+"", Status.FAIL);

	}

	/**
	 * Login with DS and click on DCR Line item and Change FieldAPI
	 * and validate Error and add New Value and validtae in DCR Line Item.
	 * @throws Exception 
	 */

	public void fielAPIErrorandNewValueValidation() throws Exception{

		cf.clickSFLinkLet("Data Change Request Lines");
		int rows;
		By fieldname=By.xpath("//label[contains(text(),'Field API Name')]/parent::td/following-sibling::td//input");
		By finalvaluepath=By.xpath("//label[contains(text(),'Final Value')]/parent::td/following-sibling::td//textarea");
		if(cf.isElementVisible(By.xpath("//h3[.='Data Change Request Lines']"),"Linklet")){
			rows=cf.getRowCountFromLinkLetTable("Data Change Request Lines");
			String DCRLineID,action,fieldapi;
			if(rows>1){
				report.updateTestLog("Verify able to see DCR Request Lines's for the DCR in related list.", "Able to see DCR Request Lines's for the DCR in related list.", Status.PASS);
				String linklet=cf.getTableXPath("linklet", "Data Change Request Lines");
				DCRLineID =cf.getDataFromLinkLetTable("Data Change Request Lines", "DCR Line ID", 2);
				action=cf.getDataFromLinkLetTable("Data Change Request Lines", "Action", 2);
				if(action.equals("Edit")){
					cf.clickLink(By.xpath(linklet+"//tr[2]/td/a"), "Edit Link of DCR Request line");
					fieldapi=cf.getData(fieldname, "FieldAPI", "value");
					cf.setData(fieldname, "Field Name API", fieldapi+"c");
					cf.clickButton(By.xpath("//td[@id='topButtonRow']/input[@name='save']"), "Save");
					String actualdata=cf.getData(By.id("errorDiv_ep"), "Error Value", "Text");
					System.out.println(actualdata);
					String expecteddata="Error: Invalid Data."+"\n"+"Review all error messages below to correct your data."+"\n"+"You cannot modify the Field API Name.";
					cf.verifyActualExpectedData("Error", actualdata, expecteddata);
					cf.setData(fieldname, "Field Name API", fieldapi);
					cf.setData(finalvaluepath, "Final Value", "Final Value");
					cf.clickButton(By.xpath("//td[@id='topButtonRow']/input[@name='save']"), "Save");
					cf.clickSFLinkLet("Data Change Request Lines");
					String savedcrlineid=cf.getDataFromLinkLetTable("Data Change Request Lines", "DCR Line ID", 2);
					String oldvalue=cf.getDataFromLinkLetTable("Data Change Request Lines", "Old Value", 2);
					String newvalue=cf.getDataFromLinkLetTable("Data Change Request Lines", "New Value", 2);
					String finalvalue=cf.getDataFromLinkLetTable("Data Change Request Lines", "Final Value", 2);
					if(savedcrlineid.equals(DCRLineID)&& oldvalue.equals("")&& !newvalue.equals("") && finalvalue.equals("Final Value"))
						report.updateTestLog("Verify able to see Old Value as "+oldvalue+" and new value as "+newvalue+" and final value as "+finalvalue+"", "Able to see Old Value as "+oldvalue+" and new value as "+newvalue+" and final value as "+finalvalue+"", Status.PASS);
					else
						report.updateTestLog("Verify able to see Old Value as "+oldvalue+" and new value as "+newvalue+" and final value as "+finalvalue+"", "Unable to see Old Value as "+oldvalue+" and new value as "+newvalue+" and final value as "+finalvalue+"", Status.FAIL);
				}
			}
			else
				report.updateTestLog("Verify able to see DIR's for the account in related list.", "There are no DIR's created in related list.", Status.FAIL);
		}
		else

			report.updateTestLog("Verify able to see Data Change Request Lines related list.", "Able to see Data Change Request Lines related list.", Status.FAIL);

	}
	/**
	 * Login with ds to accept the status for Affliation.
	 * @throws Exception
	 */
	public void dsaffacceptstatus() throws Exception{
		String dcrid=dataTable.getData("DataChangeRequest", "DCRRequestName");
		searchandnavigateUS("Data Change Requests",dcrid);
		//		vf.clickVeevaTab("Data Change Requests");
		//		clickDCRRequestInDCRTab();
		editchangestatus("Under Review");
		validateDCRLine("Submitted","Under Review");
		editchangestatus("Accepted");
		validateDCRLine("Under Review","Accepted");
	}
	/**
	 * Login with ds to Reject the status for Affiliation.
	 * @throws Exception
	 */
	public void dsaffrejectstatus() throws Exception{
		String dcrid=dataTable.getData("DataChangeRequest", "DCRRequestName");
		searchandnavigateUS("Data Change Requests",dcrid);
		//		vf.clickVeevaTab("Data Change Requests");
		//		clickDCRRequestInDCRTab();
		editchangestatus("Under Review");
		validateDCRLine("Accepted","Under Review");
		editchangestatus("Rejected");
		validateDCRLine("Under Review","Rejected");
	}

	/**
	 * Edit Affiliation with affiliation as parameter
	 * @param affliation
	 * @throws Exception
	 */
	public void editaff(String affliation) throws Exception{

		cf.clickSFLinkLet(""+affliation+"");
		int rows;
		if(cf.isElementVisible(By.xpath("//h3[.='"+affliation+"']"),"Linklet")){
			rows=cf.getRowCountFromLinkLetTable(""+affliation+"");
			if(rows>1){
				String childaccount=dataTable.getData("DataChangeRequest", "EditAccountName");
				String affliationid=cf.getDataFromLinkLetTable(""+affliation+"", "Name", 2);
				cf.clickLink(By.xpath("//a[.='"+affliationid+"']"), affliationid);
				cf.switchToFrame(By.id("vod_iframe"), "Edit Child Account");
				cf.clickButton(By.cssSelector("tr#pbHeaderTableRow input[name='Edit_Child_Account_DCR_vod']"), "Edit Child Account DCR");
				cf.switchToFrame(By.id("vod_iframe"), "Edit Child Account");
				String existaccount=cf.getData(By.xpath("//span[@id='i-value:reference:Child_Account_vod__c:Child_Account_vod__c:span1']/input[1]"), "Child Account Value", "value");
				searchlookupaccount(childaccount); 
				cf.switchToFrame(By.id("vod_iframe"), "Edit Child Account");
				cf.clickButton(By.xpath("//tr[@id='pbHeaderTableRow']//input[@name='Save']"), "Save");
				enterNotesInformationInDCR();
				cf.clickButton(By.id("submitButton"), "Submit Button");
				cf.waitForSeconds(5);
				String expected="Failed to save DCR records.You cannot modify the Parent or Child of an existing Affiliation Record";
				String actual=cf.getData(By.cssSelector("div#errorDiv"), "Error Message", "text");
				cf.verifyActualExpectedData("Error", actual, expected);
				searchlookupaccount(existaccount);
				cf.switchToFrame(By.id("vod_iframe"), "Edit Child Account");
				cf.selectARandomOptionFromComboBox(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Group_Code_AZ_US__c']"), "Role Group");
				cf.selectARandomOptionFromComboBox(By.xpath("//select[@id='i-value:picklist:Child_Account_vod__c:Role_Type_Code_AZ_US__c']"), "Role Type");
				String deleteflag=dataTable.getData("DataChangeRequest", "DeleteFlag");
				if(deleteflag.equals("Y"))
					deleteaff("1");
				else
					deleteaff("0");
				cf.clickButton(By.xpath("//tr[@id='pbHeaderTableRow']//input[@name='Save']"), "Save");
				enterNotesInformationInDCR();
				cf.clickButton(By.id("submitButton"), "Submit Button");
			}
			else
				report.updateTestLog("Verify able to see "+affliation+" for the account in related list.", "There are no "+affliation+" created in related list.", Status.FAIL);
		}
		else
			report.updateTestLog("Verify able to see "+affliation+" related list.", "Able to see "+affliation+" related list.", Status.FAIL);
	}

	/**
	 * Delete Role Flag with boolean value.
	 * @param value
	 * @throws Exception
	 */
	public void deleteaff(String value) throws Exception{
		String booleanvalue=cf.getData(By.id("i-value:boolean:Child_Account_vod__c:Inactive_AZ_US__c"), "Delete", "value");
		if(!value.equals(booleanvalue))
			cf.clickLink(By.id("i-value:boolean:Child_Account_vod__c:Inactive_AZ_US__c"), "Delete Affiliation CheckBox");
	}

	/**
	 * Updating the HCA-HCP Affiliations
	 * @throws Exception
	 */
	public void updateAffHCAHCA() throws Exception{
		editaff("Affiliations HCA/HCP");
		relatedListDCR();
	}
	/**
	 * Updating HCA Affiliations.
	 * @throws Exception
	 */
	public void updateAffHCAHCP() throws Exception{
		editaff("Affiliations HCA");
		relatedListDCR();
	}


	/**
	 * This method is used to populate the mandatory fields for creation of new address
	 */
	public void updateMandatoryFields() throws Exception{
		cf.setSFData("Information part 2", "Address line 1", "textbox", "1111 Address Line 1");
		cf.setSFData("Information part 2", "Address line 2", "textbox", "Address Line 2");
		cf.setSFData("Information part 3", "City", "textbox", dataTable.getData("DataChangeRequest", "City"));
		cf.setSFData("Information part 3", "State", "select", dataTable.getData("DataChangeRequest", "State"));
		cf.setSFData("Information part 3", "Zip", "textbox", dataTable.getData("DataChangeRequest", "Zip"));
		cf.setSFData("Information part 3", "Country", "select", dataTable.getData("DataChangeRequest", "Country"));
		cf.setSFData("Type", "Address Type", "multiselect", dataTable.getData("DataChangeRequest", "Address Type"));
		cf.setSFData("Data Change Request", "Notes", "textarea","Automation testing at "+new SimpleDateFormat("MMddYYYY").format(new Date()));
	}

	/**
	 * This method is to verify the address values in Data Change request related list
	 */
	public void verifyFieldsInAddressDCRRelatedList() throws Exception{
		String strAddressCreatedDate = dataTable.getData("DataChangeRequest", "DCRCreatedDateTime");
		String strTableName="Data Change Requests";
		cf.clickSFLinkLet(strTableName);
		String strAddressDCRID=cf.getDataFromLinkLetTable(strTableName, "DCR User Created DateTime", strAddressCreatedDate, "Data Change Request Name");
		String strRecordType=cf.getDataFromLinkLetTable(strTableName, "Data Change Request Name", strAddressDCRID, "Record Type");
		String strType=cf.getDataFromLinkLetTable(strTableName, "Data Change Request Name", strAddressDCRID, "Type");
		if(strRecordType.trim().equalsIgnoreCase("Address") && strType.trim().equalsIgnoreCase("New"))
			report.updateTestLog("Verify DCR Related list","DCR request"+strAddressDCRID+" is sucessfully created with record type Address and type 'New'", Status.PASS);
		else
			report.updateTestLog("Verify DCR Related list","DCR request"+strAddressDCRID+" is not created with Address record type and type as New", Status.FAIL);
	}

	/**
	 * This method is to click on DCR address request and verify DCR status in detail view
	 */
	public void verifyDCRStatusInDCRView() throws Exception{
		String strAddressCreatedDate = dataTable.getData("DataChangeRequest", "DCRCreatedDateTime");
		cf.clickLinkFromLinkLetTable("Data Change Requests", "DCR User Created DateTime", strAddressCreatedDate, "Data Change Request Name");
		cf.verifyActualExpectedSFData("header labels", "Status", "Submitted", "text", "equalsignorecase");
	}

	public void clickDCRRequestInDCRTab() throws Exception{
		String strDCRRequestName=dataTable.getData("DataChangeRequest", "DCRRequestName");
		String strType=dataTable.getData("DataChangeRequest", "Type");
		String strRecordType=dataTable.getData("DataChangeRequest", "RecordType");
		String strStatus=dataTable.getData("DataChangeRequest", "DCRStatus");
		clickDCRRequestInDCRTab(strDCRRequestName,strType,strRecordType,strStatus);

	}
	/**
	 * This method is used to click on the DCR request which is of record type {@strRecordType} and status {@strDCRStatus} and verify if DCR page is displayed as a Data Steward user
	 * @param strDCRName
	 * @param strType
	 * @param strRecordType
	 * @param strDCRStatus
	 * @throws Exception
	 */
	public String clickDCRRequestInDCRTab(String strDCRRequestName,String strType, String strRecordType, String strDCRStatus) throws Exception{
		/*
		 * Data Steward take the DCR Change request which
		 *  are in "Submitted" status and click on the Data Change request Name (Id ) field 
		 *  on the list to open the Data Change Request which has Type "New" and Record Type "Account"
		 */
		int iColRecType=0, iColDCRStatus=0,iColType=0, iColDCRReqName=0;
		if(!strDCRRequestName.trim().equalsIgnoreCase("")){
			cf.clickLinkFromLinkLetTable("Recent Data Change Requests", "Data Change Request Name", strDCRRequestName);
			report.updateTestLog("Click DCR Request", "DCR Request '" +strDCRRequestName+ "' is clicked", Status.SCREENSHOT);
		}
		else{
			iColRecType=cf.getColNumFromTable(By.cssSelector(".bRelatedList .list tr.headerRow > th"), "Recent Data change request", "Record Type")-1;
			iColDCRStatus=cf.getColNumFromTable(By.cssSelector(".bRelatedList .list tr.headerRow > th"), "Recent Data change request", "Status")-1;
			iColType=cf.getColNumFromTable(By.cssSelector(".bRelatedList .list tr.headerRow > th"), "Recent Data change request", "Type")-1;
			int iLoop=0;
			List<WebElement> dataRows = driver.findElements(By.cssSelector(".bRelatedList .list tr.dataRow"));
			for(WebElement dataRow : dataRows){
				List<WebElement> elements = dataRow.findElements(By.cssSelector(".dataCell"));
				if(strRecordType.trim().equalsIgnoreCase(elements.get(iColRecType).getText().trim())
						&& strDCRStatus.trim().equalsIgnoreCase(elements.get(iColDCRStatus).getText().trim())
						&& strType.trim().equalsIgnoreCase(elements.get(iColType).getText().trim())){
					iColDCRReqName=cf.getColNumFromTable(By.cssSelector(".bRelatedList .list tr.headerRow"), "Recent Data change request", "Data Change Request Name")-1;
					strDCRRequestName = elements.get(iColDCRReqName).getText().trim();
					elements.get(iColDCRReqName).click();
					report.updateTestLog("Click DCR Request", "DCR Request '" +strDCRRequestName+ "' is clicked", Status.SCREENSHOT);
					break;
				}
				else if(iLoop == dataRows.size()-1)
					report.updateTestLog("Unable to find DCR Request", "There is no DCR request found with record type, status and type as : " + strRecordType +","+strDCRStatus+","+strType, Status.FAIL);
				iLoop++;
			}
		}
		return strDCRRequestName;
	}


	/***
	 * DCR RelatedList Validations.
	 * @throws Exception 
	 */
	public void relatedListDCR() throws Exception{
		int rows;
		Boolean validate=false;
		cf.clickSFLinkLet("Data Change Requests");
		String notes=dataTable.getData("DataChangeRequest", "Notes");
		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Data Change Requests')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List")){
			WebElement element1=driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Data Change Requests')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Go to list");
			cf.waitForSeconds(6);
		}
		if(cf.isElementVisible(By.xpath("//h3[.='Data Change Requests']"),"Linklet")){
			rows=cf.getRowCountFromLinkLetTable("Data Change Requests");
			if(rows>0){
				for(int j=rows+1;j>1;j--){
					String appnotes=cf.getDataFromLinkLetTable("Data Change Requests", "Notes", j);
					String appstatus=cf.getDataFromLinkLetTable("Data Change Requests", "Status", j);
					if(appstatus.equals("Submitted")&& notes.equals(appnotes)){
						String appdcrid=cf.getDataFromLinkLetTable("Data Change Requests", "Data Change Request Name", j);	
						dataTable.putData("DataChangeRequest", "DCRRequestName", appdcrid);
						validate=true;
						break;

					}
				}
			}
			else
				report.updateTestLog("Verify able to see DCR's for the account in related list.", "There are no DCR's created in related list.", Status.FAIL);
		}
		else{
			rows=cf.rowscountexpanded("Data Change Requests");
			if(rows>0){
				cf.clickLink(By.xpath("//a[contains(@title,'Created Date')]"), "Sorted Ascending");
				if(cf.isElementVisible(By.xpath("//a[@title='Created Date - Sorted ascending']"), "Sorted Desending"))
					cf.clickLink(By.xpath("//a[@title='Created Date - Sorted ascending']"), "Sorted Ascending");
				cf.waitForSeconds(4);
				for(int j=2;j<=rows+1;j++){
					String appnotes=cf.getDataFromLinkLetTableexpanded("Data Change Requests", "Notes", j);
					String appstatus=cf.getDataFromLinkLetTableexpanded("Data Change Requests", "Status", j);
					if(appstatus.equals("Submitted")&& notes.equals(appnotes)){
						String appdcrid=cf.getDataFromLinkLetTableexpanded("Data Change Requests", "Data Change Request Name", j);
						dataTable.putData("DataChangeRequest", "DCRRequestName", appdcrid);
						validate=true;
						break;

					}
				}
			}
			else
				report.updateTestLog("Verify able to see DCR's for the account in related list.", "There are no DCR's created in related list.", Status.FAIL);

		}
		if(validate == true)
			report.updateTestLog("Verify able to see Status and Notes "+notes+" properly", "Able to see Status and Notes "+notes+" properly", Status.PASS);
		else
			report.updateTestLog("Verify able to see Status and Notes "+notes+"properly", "Unable to see Status and Notes "+notes+" properly", Status.FAIL);
	}


	/**
	 * Validate the Error Messages of External Type and External Type Value.
	 * Result Field is pick list or not.
	 * @throws Exception
	 */
	public void externalandResultvvalidations() throws Exception{
		String dcrid=dataTable.getData("DataChangeRequest", "DCRRequestName");
		searchandnavigateUS("Data Change Requests", dcrid);
		cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@name='edit']"), "Edit Button in DCR");
		Resultvalidations();
		externalidvalidations();
	}
	public void Resultvalidations() throws Exception{
		By resultxpath=cf.getSFElementXPath("Result", "select");
		//String optionpath=resultxpath+"//option";
		if(cf.isElementVisible(resultxpath, "Result Dropdown")){
			report.updateTestLog("Verify able to see Result as PickList", "Able to see Result as picklist", Status.PASS);
			List<WebElement> options=driver.findElements(By.xpath("//label[.='Result']/parent::td/following-sibling::td//select//option"));
			if(options.size()>0)
				report.updateTestLog("Verify able to see options in Result PickList", "Able to see options in Result PickList", Status.PASS);
			else
				report.updateTestLog("Verify able to see options in Result PickList", "Unable to see options in Result PickList", Status.FAIL);
		}
		else
			report.updateTestLog("Verify able to see Result as PickList", "Unable to see Result as picklist", Status.FAIL);
	}
	public void externalidvalidations() throws Exception{

		By ExternalId=cf.getSFElementXPath("External Id Type", "select");
		List<WebElement> options=driver.findElements(By.xpath("//label[.='External Id Type']/parent::td/following-sibling::td//select//option")); 

		if(options.size()>1){
			By ExternalIdvalue=cf.getSFElementXPath("External Id Type Value", "input");
			cf.setData(ExternalIdvalue, "Externalidvalue", "abcd");
			cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@name='save']"), "Save");

			String typeactualdata=cf.getData(By.id("errorDiv_ep"), "Error Value", "Text");
			System.out.println(typeactualdata);
			String typeexpecteddata="Error: Invalid Data."+"\n"+"Review all error messages below to correct your data."+"\n"+"If a value is entered in External Id Type Value, then a value is required for External Id Type.";
			cf.verifyActualExpectedData("Error", typeactualdata, typeexpecteddata);

			cf.setData(ExternalIdvalue, "Externalidvalue", "");
			cf.selectData(ExternalId, "Externalidvalue", "SLN");
			cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@name='save']"), "Save");

			String valueactualdata=cf.getData(By.id("errorDiv_ep"), "Error Value", "Text");
			System.out.println(valueactualdata);
			String valueexpecteddata="Error: Invalid Data."+"\n"+"Review all error messages below to correct your data."+"\n"+"If a value is entered in External Id Type, then a value is required for External Id Type Value.";
			cf.verifyActualExpectedData("Error", valueactualdata, valueexpecteddata);
		}
		else
			report.updateTestLog("Verify able to see Options under External Id Type dropdown", "Unable to see Options under External Id Type dropdown", Status.FAIL);

	}



}


