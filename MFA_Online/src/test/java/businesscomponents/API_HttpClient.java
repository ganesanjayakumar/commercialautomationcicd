package businesscomponents;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.RestAssured.*;
import io.restassured.response.Response;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class API_HttpClient extends ReusableLibrary
{
	public API_HttpClient(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public static void apacheHttpClientGet()
	{
		try
		{
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("https://www.google.com/");
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			int res = response.getStatusLine().getStatusCode();
			System.out.println("Response Code: " + res);
			if (response.getStatusLine().getStatusCode() != 200)
			{
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null)
			{
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (ClientProtocolException e)
		{

			e.printStackTrace();

		}
		catch (IOException e)
		{

			e.printStackTrace();
		}
	}

	public static void apacheHttpClientPost()
	{
		try
		{

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://www.google.com/gmail/");

			StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			int res = response.getStatusLine().getStatusCode();
			System.out.println("Response Code: " + res);

			if (response.getStatusLine().getStatusCode() != 201)
			{
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null)
			{
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{

			e.printStackTrace();

		}

	}

	public void verifyEndPoints() throws JSONException, IOException
	{

		String sEndPont = dataTable.getData("Login", "EndPoint");
		String sMethod = dataTable.getData("Login", "Method");
		String sContentType = dataTable.getData("Login", "Content Type");
		String sArgumentsBody = dataTable.getData("Login", "Arguments Body");
		String sExpectedStatusCode = dataTable.getData("Login", "Expected StatusCode");
		String sExpectedStatusMsg = dataTable.getData("Login", "Expected StatusMsg");

		dataTable.putData("Result", "EndPoint", sEndPont);
		dataTable.putData("Result", "Method", sMethod);
		dataTable.putData("Result", "Content Type", sContentType);
		dataTable.putData("Result", "Arguments Body", sArgumentsBody);
		dataTable.putData("Result", "Expected StatusCode", sExpectedStatusCode);
		dataTable.putData("Result", "Expected StatusMsg", sExpectedStatusMsg);

		//define variables 
		Response oResponse = null;
		String sActStatusCode = "";
		String sExpStatusCode = sExpectedStatusCode.trim();
		String sActStatusMsg = "";
		String sExpStatusMsg = "";

		//define the response content type
		String sContType = "";
		if (!sContentType.isEmpty())
			sContType = sContentType + "; charset=UTF-8";

		if (("get").equalsIgnoreCase(sMethod.trim()))
			oResponse = RestAssured.get(sEndPont.trim());
		else if (("post").equalsIgnoreCase(sMethod.trim()))
			oResponse = RestAssured.given().auth().oauth2(sMethod).contentType(sContType).body(sArgumentsBody.trim()).when().post(sEndPont.trim());
		else if (("put").equalsIgnoreCase(sMethod.trim()))
			oResponse = RestAssured.given().contentType(sContType).body(sArgumentsBody.trim()).when().put(sEndPont.trim());
		else if (("delete").equalsIgnoreCase(sMethod.trim()))
			oResponse = RestAssured.delete(sEndPont.trim());

		//Verify status code 
		sActStatusCode =String.valueOf(oResponse.getStatusCode()).trim();

		System.out.println(sActStatusCode);

		dataTable.putData("Result", "Actual StatusCode", sActStatusCode);
		Assert.assertEquals(sActStatusCode, sExpStatusCode, "EndPoint status code is incorrect");

		//Verify response message
		sActStatusMsg = URLDecoder.decode((oResponse.asString().replace("\n", "").replace("&amp;", "&").toLowerCase().trim()), "UTF-8");
		//System.out.println(sActStatusMsg);
		dataTable.putData("Result", "Actual StatusMsg", sActStatusMsg);
		if (!sExpectedStatusMsg.isEmpty())
			sExpStatusMsg = sExpectedStatusMsg.replace("\n", "").replace("&amp;", "&").toLowerCase().trim();
		Assert.assertTrue(sActStatusMsg.replaceAll("\\s+", "").contains(sExpStatusMsg.replaceAll("\\s+", "")), "EndPoint response message is incorrect. Expected: " + sExpStatusMsg + " but Actual: " + sActStatusMsg);

		//Verify response content type
		//Assert.assertEquals(oResponse.getContentType().trim(), "application/json", "EndPoint content type is incorrect");

	}

	public static void main(String[] args)
	{
		apacheHttpClientPost();
	}

}
