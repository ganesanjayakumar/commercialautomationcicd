package businesscomponents;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.Status;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_Events extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_Events(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public void createEvents() throws Exception
	{
		vf.clickVeevaTab("Events");
		String recordType = dataTable.getData("Events", "RecordType");
		String country = dataTable.getData("Events", "Country");
		String topic = dataTable.getData("Events", "Topic");

		if (cf.isElementVisible(cf.getSFElementXPath("New", "pbButton"), "New Button"))
		{
			cf.clickSFButton("New", "pbButton");

			if (cf.isElementVisible(By.xpath("//h2[text()=' Select Event Record Type']"), "Select Event Record Type"))
			{
				cf.selectData(By.xpath("//select[@id='p3']"), "Record Type: " + recordType, recordType);

				cf.clickElement(By.xpath("//input[@value='Continue']"), "Continue");

				report.updateTestLog("Verify Record Type " + recordType + " is Selected", "Record Type " + recordType + " is Selected", Status.PASS);
			}
			else
				report.updateTestLog("Verify Record Type " + recordType + " is Selected", "Record Type " + recordType + " is NOT Selected", Status.FAIL);

			if (cf.isElementVisible(By.xpath("//h2[text()='New Event']"), "New Event"))
			{
				DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
				Date date = new Date();
				String d = dateFormat.format(date);
				System.out.println(d);

				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				c.add(Calendar.DATE, 1);
				String d1 = dateFormat.format(c.getTime());
				System.out.println(d1);

				cf.setData(By.xpath("//span[@id='Start_Time_vod__c']/input"), "Start Date", d);

				cf.clickElement(By.xpath("//h2[text()='New Event']"), "New Event - Date Disapper");

				cf.setData(By.xpath("//span[@id='End_Time_vod__c']/input"), "End Date", d1);

				cf.clickElement(By.xpath("//h2[text()='New Event']"), "New Event - Date Disapper");

				cf.selectData(By.xpath("//select[@id='Country_vod__c']"), "Country: " + country, country);

				cf.clickElement(By.xpath("//input[@value='Continue']"), "Continue");

				report.updateTestLog("Verify Start Date & End Date is Entered & Submitted", "Start Date & End Date is Entered & Submitted", Status.PASS);
			}
			else
				report.updateTestLog("Verify Start Date & End Date is Entered & Submitted", "Verify Start Date & End Date is NOT Entered & Submitted", Status.FAIL);
		}
		else
			report.updateTestLog("Verify Events - New button is Apper and Clicked", "Verify Events - New button is NOT Apper and Clicked", Status.FAIL);

		if (cf.isElementVisible(By.xpath("//h3[text()='Information']"), "Information"))
		{
			cf.setData(By.xpath("//input[@id='Name']"), "Name", "Events");

			cf.setData(By.xpath("//input[@id='Estimated_Attendance_vod__c']"), "Estimated Attendance", "10");			
			
			cf.clickElement(By.xpath("//input[@id='Topic_vod__c']/parent::span/a"), "Topic");
			
			cf.setData(By.xpath("//input[@id='Topic_vod__c_searchText']"), "Topic Search Text", topic);
			
			cf.clickElement(By.xpath("//input[@name='go']"), "Go");
			
			cf.clickElement(By.xpath("//tr[@class='dataRow even last first ng-scope']/td[2]/span[text()='"+topic+"']/parent::td/preceding-sibling::td[1]/a"), "Topic Search - Select");
		
			cf.clickButton(By.xpath("//input[@name='Save']"), "Save");
		}

	}
}
