package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.ObjVeevaSurveys;
import ObjectRepository.objHeaderPage;
//import ObjectRepository.objMyAccounts;
import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_Surveys extends VeevaFunctions
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	public Veeva_Surveys(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	//public static String surveyName;

	//Creating new survey for employee by logging with Ops admin.

	public void existingsurvey() throws Exception
	{
		Date exte = new Date();
		String ex = new SimpleDateFormat("mmss").format(exte).toString();
		String strDataSheet = "Surveys";
		String strDataSheet1 = "Login";
		String SelectTerritory_SalesRep = dataTable.getData(strDataSheet, "SelectTerritory_SalesRep");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		String locale = dataTable.getData(strDataSheet1, "Locale");
		
		clickVeevaTab("Surveys");
		cf.waitForSeconds(20);
		
		cf.selectData(By.xpath("//select[@id='hotlist_mode']"), "Recently Created", "Recently Created");
		cf.waitForSeconds(2);
		
		if (cf.isElementVisible(By.xpath("//a[text()='"+surveyName+"']"),surveyName))
		{
			cf.clickElement(By.xpath("//a[text()='"+surveyName+"']"), surveyName);
			cf.waitForSeconds(20);
			cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));
			
		}	
	}

	public void newsurvey() throws Exception
	{
		String surveyName;
		Date exte = new Date();
		String ex = new SimpleDateFormat("mmss").format(exte).toString();
		String strDataSheet = "Surveys";
		String strDataSheet1 = "Login";
		String SelectTerritory_SalesRep = dataTable.getData(strDataSheet, "SelectTerritory_SalesRep");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		String countrycode = dataTable.getData(strDataSheet, "Country_Code");
		String locale = dataTable.getData(strDataSheet1, "Locale");
		String affairs;
		JavascriptExecutor js = ((JavascriptExecutor) driver.getWebDriver());
		
		clickVeevaTab("Surveys");
		cf.clickifElementPresent(objVeevaAdvanceCoachingReport.newButton, "New Button");
		cf.waitForSeconds(5);
		cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", "One Time");
		report.updateTestLog("Verify able to select Coaching Report", "Able to select Coaching Report", Status.SCREENSHOT);
		cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
		cf.waitForSeconds(10);
		cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"));

		if (cf.isElementVisible(By.xpath("//h2[.='New Survey']"), "NSurvey"))
		{
			surveyName = "OneTimeSurvey" + locale + ex;
			report.updateTestLog("Verify able to see New Survey Target Page", "Able to see survey Target Page", Status.PASS);
			cf.setData(objVeevaAdvanceCoachingReport.surveyname, "Survey Name", surveyName);
			dataTable.putData("Surveys", "SurveyName", surveyName);
			
			cf.clickElement(ObjVeevaSurveys.channelclm, "Channel CLM");
			cf.clickElement(ObjVeevaSurveys.channelcrm, "Channel CRM");
			
			cf.clickByJSE(By.xpath("//td[@id='value_Survey_vod__c:Channels']//img[@title='Add']"), "Adding Channel");
			
			//For JP_Commercial in Ops Admin need to select AZ Sharing BU as All for populating the created survey in Sales Rep
			if (locale.equals("JP") && profile.equals("Commercial"))
				cf.selectData(By.id("i-value:picklist:Survey_vod__c:AZ_Sharing_BU_AZ_JP__c"), "AZ Sharing BU", "All");
			
			cf.selectData(ObjVeevaSurveys.assignmentType, "Assignment Type", "Territory");
			
			js.executeScript("document.getElementById('i-value:boolean:Survey_vod__c:Open_vod__c').click();");

			//cf.clickLink(By.xpath("//td[@id='value_Survey_vod__c:Allow users to choose targets?']//input[1]"), "Allow Users to choose targets");

			cf.waitForSeconds(2);
			selectterittory(SelectTerritory_SalesRep);

			cf.clickLink(By.xpath("//input[@title='Insert Selected']"), "Insert Selected");
			
			if (locale.equals("RU") && profile.equals("Commercial"))
			{
				js.executeScript("document.getElementById('i-value:string:Survey_vod__c:Share_Team_vod__c').value = '[RU_PHARMACY_KAS],[RU_PTA]';");
				
				js.executeScript("document.getElementById('i-value:string:Survey_vod__c:Share_Team_vod__c').setAttribute('value', '[RU_PHARMACY_KAS],[RU_PTA]')");
				//cf.setData(By.id("//td[@id='value_Survey_vod__c:Share Team']/input"), "Share Team", "[RU_PHARMACY_KAS],[RU_PTA]");
			}
			
			if (profile.equals("Commercial") || profile.equals("DXL"))
			{
				affairs = "No";
			}
			else
			{
				affairs = "Yes";
			}
			if (!locale.equalsIgnoreCase("JP"))
			{
				cf.selectData(ObjVeevaSurveys.medicalaffairs, "Medical Affairs", affairs);
			}
			if (locale.equalsIgnoreCase("JP"))
				System.out.println("Country code not present - Expected");
			else
				cf.setData(ObjVeevaSurveys.countrycode, "Countrycode", countrycode);
			
			report.updateTestLog("Verify all details entered", "All details entered", Status.PASS);
			cf.clickLink(objVeevaAdvanceCoachingReport.mansaveReport, "Save");
			cf.waitForSeconds(15);
			
			cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if (cf.isElementVisible(By.xpath("//h3[.='Survey Detail']"), "Survey Detail Page"))
			{
				report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Verify able to see New Survey Page", "Unable to see survey  Page", Status.FAIL);
		}
	}
	//Adding segment while creating survey by Ops admin
	public void editsegment() throws Exception
	{
		boolean details = false;
		try
		{

			cf.clickButton(ObjVeevaSurveys.editsegment, "Edit Segment");
			cf.waitForSeconds(2);
			cf.clickButton(ObjVeevaSurveys.addsegment, "Add Segment");
			cf.waitForSeconds(2);
			cf.setData(ObjVeevaSurveys.segmentno, "Segment no", "1");
			cf.setData(ObjVeevaSurveys.minscore, "Minimum Score", "0");
			cf.setData(ObjVeevaSurveys.maxscore, "Maximum Score", "1");
			cf.clickButton(ObjVeevaSurveys.savesegment, "Save Segment");
			cf.waitForSeconds(3);
			cf.implicitWait(30, TimeUnit.SECONDS);
			if (cf.getElementValue(ObjVeevaSurveys.staticsegmentno, "Segment no").equals("1"))
			{
				if (cf.getElementValue(ObjVeevaSurveys.staticminscore, "Minimum score").equals("0"))
				{
					if (cf.getElementValue(ObjVeevaSurveys.staticmaxscore, "Maximum Score").equals("1"))
						details = true;
				}
			}

			if (details == true)
				report.updateTestLog("Verify Segment no. ,minimum score and maximum score details are updated correctly", "Segment no. ,minimum score and maximum score details are updated correctly", Status.PASS);
			else
			{
				report.updateTestLog("Verify Segment no. ,minimum score and maximum score details are updated correctly", "Segment no. ,minimum score and maximum score details are not updated correctly", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//Selecting an Account in New Survey Target while creating survey by ops admin
	public void newsurveytarget() throws Exception
	{
		String strDataSheet = "Surveys";
		String account = dataTable.getData(strDataSheet, "Account");
		String view = dataTable.getData(strDataSheet, "View");

		try
		{
			cf.clickButton(ObjVeevaSurveys.newsurveytarget, "New Survey Target");
			cf.selectData(By.xpath("//div[@id='T_addSurveyTargetDialog']/div/select"), "View", view);
			cf.waitForSeconds(20);
			cf.setData(ObjVeevaSurveys.searchaccount, "Search", account);
			cf.clickButton(ObjVeevaSurveys.searchaccount, "Search");
			//if(strLocale.trim().equalsIgnoreCase("EU1")|| strLocale.trim().equalsIgnoreCase("EU2")){
			WebDriverWait dr = new WebDriverWait(driver.getWebDriver(), 30);

			//dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[.='Results truncated. Please refine view.']")));
			dr.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='T_accountsTable']")));
			//}

			cf.clickLink(By.xpath("//table[@id='T_accountsTable']//tr/td[1]/input"), "Selecting account");
			report.updateTestLog("Verify able to select account in the New Survey Target.", "Able to select account in the new Survey Target", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaSurveys.addselectedaccount, "Add Selected");
			cf.clickLink(ObjVeevaSurveys.closesurveytarget, "Close");
			if (cf.isElementVisible(By.xpath("//table[@id='T_targetsTable']//td[.='" + account + "']"), "Account Selected"))
			{
				report.updateTestLog("Verify able to see added account", "Able to see added account", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to see added account", "Unable to see added account", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//Validating Warning after clicking on publish button by Ops admin
	public void publishandwarning() throws Exception
	{
		String warning = "Publishing will lock the survey and reassign owners based on assignment rules. You will receive an email when processing is complete.";
		try
		{
			cf.clickLink(objVeevaAdvanceCoachingReport.opspublish, "Publish");
			String warningmessage = cf.getElementValue(By.xpath("//div[@id='publishDialog']/div[1]"), "Warning Message");
			System.out.println(warningmessage);
			if ((warningmessage.trim()).equals(warning.trim()))
			{
				report.updateTestLog("Verify able to see warning message", "Able to see warning message", Status.PASS);
				cf.clickLink(By.id("publishDialog_ok"), "OK");
			}
			else
			{
				report.updateTestLog("Verify able to see warning message", "Unable to see warning message", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//validating publish status.
	public void publishstatus() throws Exception
	{
		try
		{
			driver.navigate().refresh();
			cf.waitForSeconds(10);
			driver.navigate().refresh();
			cf.waitForSeconds(10);
			driver.navigate().refresh();
			cf.switchToFrame(ObjVeevaEmail.mailFrame);
			String publishstatus = cf.getElementValue(By.id("value_Survey_vod__c:Status"), "One time survey Status");
			if ((publishstatus.trim()).equals("Published"))
			{
				report.updateTestLog("Verify able to see Survey status as Published", "Able to see survey status as Published", Status.PASS);
				cf.switchToParentFrame();
			}
			else
			{
				report.updateTestLog("Verify able to see Survey status as Published", "Unable to see survey status as Published", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void selectSurveyTarget() throws Exception
	{
		cf.waitForSeconds(15);
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		cf.switchToFrame(ObjVeevaSurveys.frameAddSurveyTarget);
		List<WebElement> listSurveyTargetNames = driver.findElements(ObjVeevaSurveys.txtSurveyNames);
		List<WebElement> listSurveyRadio = driver.findElements(ObjVeevaSurveys.radioSurveyOption);

		for (int i = 0; i < listSurveyTargetNames.size(); i++)
		{
			if (listSurveyTargetNames.get(i).getText().equalsIgnoreCase(surveyName))
			{
				listSurveyRadio.get(i).click();
				cf.clickButton(ObjVeevaSurveys.btnAddSurveyTarget, "Add Survey Target");
				report.updateTestLog("Click on Survey targets", "Survey target : '" + surveyName + "' is clicked", Status.DONE);
				break;
			}
			else if (i == listSurveyTargetNames.size() - 1)
				report.updateTestLog("Click on Survey Targets", "Survey target : '" + surveyName + "' is not listed", Status.FAIL);
		}
		cf.switchToParentFrame();
	}

	//Login as Representative and click on newly created survey by ops admin and select the answers
	public void surveyTargetsrep() throws Exception
	{
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		try
		{
			clickVeevaTab("Survey Targets");
			cf.selectData(ObjVeevaSurveys.surveytargetsdropdown, "Survey Target", "My Survey");
			cf.clickButton(ObjVeevaEmail.lookupReasonGo, "Go");
			//String des=cf.getElementValue(By.xpath("//div[@title='Created Date']/parent::td"), "class");
			//System.out.println(des);
			cf.clickButton(By.xpath("//div[@class='listButtons']//input[@class='btn refreshListButton']"), "Refresh Button");
			cf.waitForSeconds(8);
			if (cf.isElementVisible(By.xpath("//div[@title='Created Date']/parent::td[@class='x-grid3-hd x-grid3-cell x-grid3-td-CREATED_DATE ASC']"), "Sorting order is in Ascending Order"))
			{
				cf.clickLink(By.xpath("//div[@title='Created Date']/img"), "Sorting icon");
			}
			if (cf.isElementVisible(By.xpath("//div[@class='listBody']//span[.='" + surveyName + "']"), "Created Survey"))
			{
				report.updateTestLog("Verify able to see created survey " + surveyName + " in Survey Targets", "Able to see created survey " + surveyName + " in Survey Targets", Status.PASS);
				cf.clickLink(By.xpath("//div[@class='listBody']//span[.='" + surveyName + "']"), "Created OneTime Survey");
				cf.waitForSeconds(15);
				cf.switchToFrame(ObjVeevaEmail.mailFrame);
				validatesurveydetailsandselectanswers();
			}
			else
			{
				report.updateTestLog("Verify able to see created survey " + surveyName + " in Survey Targets", "Unable to see created survey " + surveyName + " in Survey Targets", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//public String answerpicklist;
	//Select the answers for the questions created by ops admin and submit the answers and validate segment
	public void validatesurveydetailsandselectanswers() throws Exception
	{
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		String answerpicklist;
		try
		{
			cf.waitForSeconds(30);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			String locale = dataTable.getData("Login", "Locale");
			
			if (cf.isElementVisible(By.xpath("//h2[.='" + surveyName + "']"), "Created Survey"))
			{
				cf.waitForSeconds(30);
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				report.updateTestLog("Verify able to see navigate to survey " + surveyName + " in Survey Targets", "Able to navigate to survey " + surveyName + " in Survey Targets", Status.PASS);
				//Select se=new Select(driver.findElement(By.xpath("//div[@id='Q_questionslist']//select")));
				//answerpicklist=driver.findElement(By.xpath("//div[@id='Q_questionslist']//select/option[2]")).getText();
				
				if(locale.equals("ANZ"))
				{
					cf.setData(By.xpath("//span[@class='veeva-pl-field ng-scope']/input[@id='answer']"), "AnswerNumber", "1");
				}
				else
				{
					driver.findElement(By.xpath("//select[@id='answer']/option[2]")).click();
					answerpicklist = driver.findElement(By.xpath("//select[@id='answer']/option[2]")).getText();
					dataTable.putData("Surveys", "AnswerPickList", answerpicklist);
				}
				cf.clickLink(By.xpath("//div[@class='qresponselistdiv']/form/div[2]//veev-edit-field//label[contains(text(),'Yes')]/preceding-sibling::input"), "Answer for New Survey Question");
				report.updateTestLog("Verify responses are selected for Questions", "Responses for Questions are selected", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaSurveys.submitanssurveytarget, "Submit");
				//Add Code for validation
				cf.waitForSeconds(20);
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]")));
				String strSubmissionStatus = cf.getData(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]"), "Submission status", "text");
				if (strSubmissionStatus.trim().equalsIgnoreCase("Submitted"))
					report.updateTestLog("Verify Survey Target submission status", "Survey target " + surveyName + " is submitted as expected", Status.PASS);
				else
					report.updateTestLog("Verify Survey Target submission status ", "Survey target " + surveyName + " status is not in submitted status", Status.FAIL);
			}
			else
			{
				report.updateTestLog("Verify able to see navigate to survey " + surveyName + " in Survey Targets", "Unable to navigate to survey " + surveyName + " in Survey Targets", Status.FAIL);
			}
			cf.switchToParentFrame();
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//Login as Ops admin and click the submitted survey and verify the answers given by representative.
	public void opssurveytarget() throws Exception
	{
		try
		{

			validatesurveydetailsandverifyanswers();

		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	//Login as Ops admin and verify the answers given by representative in the submitted survey.
	public void validatesurveydetailsandverifyanswers() throws Exception
	{
		//Score and Segment will be in visible mode as sales rep doesnot have that part it wont be visible here.-Step 46
		String answerpicklist = dataTable.getData("Surveys", "AnswerPickList");
		try
		{
			String locale = dataTable.getData("Login", "Locale");
			String selectedoption = null;
			//cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if(locale.equals("ANZ"))
			{
				selectedoption = driver.findElement(By.xpath("//input[@id='answer']")).getText().trim();
			}
			else
			{
				Select se = new Select(driver.findElement(By.id("answer")));
				selectedoption = se.getFirstSelectedOption().getText().trim();
			}
				
			WebElement radio = driver.findElement(By.xpath("//div[@class='qresponselistdiv']/form/div[2]//veev-edit-field//label[contains(text(),'Yes')]/preceding-sibling::input"));
			if ((selectedoption.trim()).equals(answerpicklist.trim()) && (radio.isSelected()))
				report.updateTestLog("Verify able to see answers correctly provided by the Representative", "Able to see answers correctly provided by the Representative", Status.PASS);
			else
				report.updateTestLog("Verify able to see answers correctly provided by the Representative", "Unable to see answers correctly provided by the Representative", Status.FAIL);
			cf.switchToParentFrame();
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void searchAndGo() throws Exception
	{
		/**
		String strSearchType = dataTable.getData("Search", "Search Type");
		String strSearchText = dataTable.getData("Search", "Search Text");
		 */
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		cf.clickElement(By.xpath("//a[text()='Home']"), "Go to home page to search");

		if (cf.isElementVisible(By.xpath("//span[text()='Click to Open Sidebar']"), "Click to Open Sidebar"))
		{
			cf.clickElement(By.xpath("//span[@id='pinIndicator']"), "pinIndicator");
		}

		for (int count = 1; count <= 2; count++)
		{
			WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 60);
			if (cf.isElementPresent(objHeaderPage.selectSearch))
			{

				cf.selectData(objHeaderPage.selectSearch, "Search Type", "Survey Targets");
				cf.setData(objHeaderPage.textSearch, "Search Text", surveyName);

				cf.clickButton(objHeaderPage.btnGo, "Go button in Search");
			}
			else
			{
				cf.clickButton(By.xpath("//a[.='Advanced Search...']"), "Advanced Search");
				wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".searchBoxInput>input[value='Search']")));
				cf.setData(By.cssSelector(".searchBoxInput>input"), "Search in Advanced Search", surveyName);
				cf.clickButton(By.xpath("//label[.='Survey Targets']/preceding-sibling::input[@type='checkbox']"), "Choose Survey Target");
				cf.clickButton(By.cssSelector(".searchBoxInput>input[value='Search']"), "Search Button");
				cf.waitForSeconds(10);
			}
			cf.waitForSeconds(5);
			if (cf.isElementPresent(By.xpath("//h2[contains(text(),'Survey Target Detail')]")))
			{
				cf.switchToParentFrame();
				break;
			}
			else
				cf.pageRefresh();

		}

	}

	public void newProfilingsurvey() throws Exception
	{
		String surveyName;
		Date exte = new Date();
		String ex = new SimpleDateFormat("mmss").format(exte).toString();
		String strDataSheet = "Surveys";
		String strDataSheet1 = "Login";
		String SelectTerritory_SalesRep = dataTable.getData(strDataSheet, "SelectTerritory_SalesRep");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		String countrycode = dataTable.getData(strDataSheet, "Country_Code");
		String locale = dataTable.getData(strDataSheet1, "Locale");
		String sSurveyType = dataTable.getData(strDataSheet, "SurveyType");
		String sProfilingType = dataTable.getData(strDataSheet, "ProfilingType");
		String affairs;
		try
		{
			clickVeevaTab("Surveys");
			cf.clickifElementPresent(objVeevaAdvanceCoachingReport.newButton, "New Button");
			cf.waitForSeconds(5);
			cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", sSurveyType);
			report.updateTestLog("Verify able to select Coaching Report", "Able to select Coaching Report", Status.SCREENSHOT);
			cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
			cf.waitForSeconds(10);
			cf.switchToFrame(ObjVeevaEmail.mailFrame);

			if (cf.isElementVisible(By.xpath("//h2[.='New Survey']"), "NSurvey"))
			{
				surveyName = "OneTimeSurvey" + locale + ex;
				report.updateTestLog("Verify able to see New Survey Target Page", "Able to see survey Target Page", Status.PASS);
				cf.setData(objVeevaAdvanceCoachingReport.surveyname, "Survey Name", surveyName);
				dataTable.putData("Surveys", "SurveyName", surveyName);
				cf.clickLink(ObjVeevaSurveys.channelclm, "Channel CLM");
				cf.clickLink(ObjVeevaSurveys.channelcrm, "Channel CRM");
				cf.clickLink(By.xpath("//td[@id='value_Survey_vod__c:Channels']//img[@title='Add']"), "Adding Channel");

				//For JP_Commercial in Ops Admin need to select AZ Sharing BU as All for populating the created survey in Sales Rep
				if (locale.equals("JP") && profile.equals("Commercial"))
				{
					cf.selectData(By.id("i-value:picklist:Survey_vod__c:AZ_Sharing_BU_AZ_JP__c"), "AZ Sharing BU", "All");

				}

				String sFirstLovName = dataTable.getData("Surveys", "Lov One");
				if (cf.isElementVisible(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Select the product metric"))
				{
					cf.clickElement(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Input the product metric");
					cf.setData(By.xpath("(//span/input[contains(@id,'Product_Metric_AZ')])[1]"), "Product Metric", sFirstLovName);

				}
				cf.clickElement(By.xpath("//span/input[contains(@id,'Profiling_Survey_AZ') and @type='checkbox']"), "Check the profiling Survey");
				cf.selectData(By.xpath("//select[contains(@id,'Profiling_Type_AZ')]"), "Profiling Type", sProfilingType);
				cf.selectData(By.xpath("//select[contains(@id,'Choose_the_Operation_AZ')]"), "Choose the Operation", "SUM");

				cf.selectData(ObjVeevaSurveys.assignmentType, "Assignment Type", "Product and Territory");
				cf.clickElement(By.xpath("(//span/input[contains(@id,'Product_vod')])[1]"), "Select Product");
				cf.setData(By.xpath("(//span/input[contains(@id,'Product_vod')])[1]"), "Product", "Imfinzi_BR");
				cf.clickLink(ObjVeevaSurveys.allowuserschoosetargets, "Allow Users to choose targets");
				cf.clickElement(ObjVeevaSurveys.allowuserschoosetargets, "Allow Users to choose targets");
				cf.clickByJSE(ObjVeevaSurveys.allowuserschoosetargets, "Allow Users to choose targets");
				cf.waitForSeconds(2);
				selectterittory(SelectTerritory_SalesRep);
				if (cf.isElementVisible(By.xpath("//input[@title='Insert Selected']"), "Insert"))
				{
					cf.clickLink(By.xpath("//input[@title='Insert Selected']"), "Insert Selected");
					if (profile.equals("Commercial"))
					{
						affairs = "No";
					}
					else
					{
						affairs = "Yes";
					}
					if (!locale.equals("JP"))
					{
						cf.selectData(ObjVeevaSurveys.medicalaffairs, "Medical Affairs", affairs);
					}
					if (!locale.equals("JP"))
					{
						cf.setData(ObjVeevaSurveys.countrycode, "Countrycode", countrycode);
					}
					report.updateTestLog("Verify all details entered", "All details entered", Status.SCREENSHOT);
					cf.clickLink(objVeevaAdvanceCoachingReport.mansaveReport, "Save");
					cf.waitForSeconds(6);
					cf.switchToFrame(ObjVeevaEmail.mailFrame);
					if (cf.isElementVisible(By.xpath("//h3[.='Survey Detail']"), "Survey Detail Page"))
					{
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Able to navigate to survey detail page", Status.PASS);
					}
					else
					{
						report.updateTestLog("Verify able to navigate to Survey Detail Page", "Unable to navigate to survey detail page", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
				else
				{
					report.updateTestLog("Verify able to see New Survey Page", "Unable to see survey  Page", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
			}
			else
			{
				report.updateTestLog("Verify able to see Insert Selected Button", "Unable to see Insert Selected Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

	public void navigateToGlobalLOV() throws Exception
	{
		String strProfile = dataTable.getData("Login", "Profile");
		String strLocale = dataTable.getData("Login", "Locale");
		vf.clickVeevaTab("Global LOVs");
		cf.clickElement(By.xpath("//span/input[@class='btn' and @name='go']"), "Click GO");
		cf.waitForSeconds(10);
	}

	public void selectLOV() throws Exception
	{
		String sFirstLovName = dataTable.getData("Surveys", "Lov One");
		String sSecondLovName = dataTable.getData("Surveys", "Lov Two");

		if (cf.isElementPresent(By.xpath("//div[contains(@class,'grid')]/a/span[text()='" + sFirstLovName + "']")))
		{
			cf.clickElement(By.xpath("//td/div[contains(@class,'grid')]/a/span[text()='" + sFirstLovName + "']/../../../../td/div/input"), "Click the check box");
		}

		if (cf.isElementPresent(By.xpath("//div[contains(@class,'grid')]/a/span[text()='" + sSecondLovName + "']")))
		{
			cf.clickElement(By.xpath("//td/div[contains(@class,'grid')]/a/span[text()='" + sSecondLovName + "']/../../../../td/div/input"), "Click the check box");
		}

		vf.clickVeevaTab("Profiling Designer");
		cf.waitForSeconds(10);
	}

	public void createProfilingQues() throws Exception
	{
		String sFirstLovName = dataTable.getData("Surveys", "Lov One");
		String sSecondLovName = dataTable.getData("Surveys", "Lov Two");
		String sQues = dataTable.getData("Surveys", "Question_Text");

		if (cf.isElementPresent(By.xpath("//td/h2[text()='Creating Profiling Questions']")))
		{

			cf.clickElement(By.xpath("(//td/span[@class='lookupInput']/input[contains(@id,'mulques')])[1]"), "Input to look up icon");
			cf.setData(By.xpath("(//td/span[@class='lookupInput']/input[contains(@id,'mulques')])[1]"), "Product Metric Look Up", sFirstLovName);
			/*cf.clickElement(By.xpath("(//span[@class='lookupInput']/a/img[contains(@title,'Global LOV Lookup')])[1]"), "Click the look up icon");
			cf.waitForSeconds(8);
			cf.switchToFrame(By.id("searchFrame"));
			cf.switchToFrame(By.id("resultsFrame"));
			cf.clickElement(By.xpath("//th/a[text()='Admissions_AZ__c']"), "Click the result value");
			cf.switchToParentFrame();
			cf.switchToParentFrame();
			*/
			// Enter question text
			cf.clickElement(By.xpath("(//td/input[@type='text'])[1]"), "Input to the question field");
			cf.setData(By.xpath("(//td/input[@type='text'])[1]"), "Input to the question field", sQues);
			WebElement sSelect = driver.findElement(By.xpath("//select[contains(@name,'mulques')]"));
			Select dropDown = new Select(sSelect);
			dropDown.selectByValue("Number_vod");

			cf.clickElement(By.xpath("//td/input[@value='Create All Added Question']"), "Click create added questions");
			cf.waitForSeconds(10);

			if (cf.isElementVisible(By.xpath("//td/span[text()='" + sQues + "']"), "Saved Questions"))
			{
				report.updateTestLog("Verify able to see Created Question:", "Able to see the created Question " + sQues + " in the Table", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify able to see Created Question:", "Unable to see the created Question " + sQues + " in the Table", Status.FAIL);
			}

		}

	}

	public void surveysquestionCrearionQuestionBank() throws Exception
	{
		String questionbankquetext;
		String sQues = dataTable.getData("Surveys", "Question_Text");
		cf.clickLink(objVeevaAdvanceCoachingReport.opsquestionbankbutton, "Question Bank button");
		cf.waitForSeconds(10);
		if (cf.isElementVisible(By.xpath("//span[.='Question Bank']"), "Question Bank"))
		{
			cf.selectData(By.xpath("//div[@id='Q_bankDialog']/div/select"), "Type Picklist", "Number");
			int noofquestions = driver.findElements(By.xpath("//table[@id='Q_questionsTable']//tr/td[1]/div[1]/input")).size();
			try
			{
				for (int i = 1; i <= noofquestions; i++)
				{
					String type = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[3]")).getText().trim();
					if (type.equals("Number"))
					{
						cf.clickLink(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[1]//input[@type='checkbox']"), "Question Selected");
						questionbankquetext = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[2]")).getText();
						dataTable.putData("Login", "questionbankquetext", questionbankquetext);
						WebElement ele = driver.findElement(By.xpath("//table[@id='Q_questionsTable']//tr[" + i + "]/td[1]//input[@type='checkbox']"));
						if (ele.isSelected())
						{
							report.updateTestLog("Verify selected question from question bank", "Able to select question from question from question bank", Status.PASS);
							cf.clickLink(By.id("Q_addSelected"), "Add Selected");
							cf.clickLink(By.xpath("//span[.='Question Bank']/parent::div//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']"), "Close");
							cf.waitForSeconds(7);
							if (cf.isElementVisible(By.xpath("//div[@id='Q_questionslist']//span[.='" + questionbankquetext + "']"), "Question bank Question"))
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Able to see added Question Bank Question", Status.PASS);
								break;
							}
							else
							{
								report.updateTestLog("Verify able to see added New Survey Question", "Unable to see added Question Bank Question", Status.FAIL);
								// frameworkparameters.setStopExecution(true);
							}
						}
						else
						{
							report.updateTestLog("Verify selected question from question bank", "Unable to select question from question from question bank", Status.FAIL);
							// frameworkparameters.setStopExecution(true);
						}
					}
				}
			}
			catch (Exception e)
			{
				report.updateTestLog("Verify selected question from question bank", "No Questions were found", Status.FAIL);
				// frameworkparameters.setStopExecution(true);
			}
		}
	}

	public void surveydetailsanswersAndvalidation() throws Exception
	{
		String surveyName = dataTable.getData("Surveys", "SurveyName");
		String questionbankquetext = dataTable.getData("Login", "questionbankquetext");
		String sAnswers = dataTable.getData("Surveys", "AnswerPickList");
		String answerpicklist;
		try
		{
			cf.waitForSeconds(15);
			//cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if (cf.isElementVisible(By.xpath("//h2[.='" + surveyName + "']"), "Created Survey"))
			{

				report.updateTestLog("Verify able to see navigate to survey " + surveyName + " in Survey Targets", "Able to navigate to survey " + surveyName + " in Survey Targets", Status.PASS);
				//Select se=new Select(driver.findElement(By.xpath("//div[@id='Q_questionslist']//select")));
				//answerpicklist=driver.findElement(By.xpath("//div[@id='Q_questionslist']//select/option[2]")).getText();
				driver.findElement(By.xpath("//div[text()='" + questionbankquetext + "']/following-sibling::div//span/input")).click();
				cf.setData(By.xpath("//div[text()='" + questionbankquetext + "']/following-sibling::div//span/input"), "Question Response", sAnswers);
				report.updateTestLog("Verify responses are selected for Questions", "Responses for Questions are selected", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaSurveys.submitanssurveytarget, "Submit");
				//Add Code for validation
				cf.waitForSeconds(20);
				WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]")));
				String strSubmissionStatus = cf.getData(By.cssSelector(".veeva-app veev-field > span > span[name*=Status]"), "Submission status", "text");
				if (strSubmissionStatus.trim().equalsIgnoreCase("Submitted"))
					report.updateTestLog("Verify Survey Target submission status", "Survey target " + surveyName + " is submitted as expected", Status.PASS);
				else
					report.updateTestLog("Verify Survey Target submission status ", "Survey target " + surveyName + " status is not in submitted status", Status.FAIL);

				//Segment need to be validated-Step 41
			}
			else
			{
				report.updateTestLog("Verify able to see navigate to survey " + surveyName + " in Survey Targets", "Unable to navigate to survey " + surveyName + " in Survey Targets", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
			cf.switchToParentFrame();

			if (cf.isElementVisible(By.xpath("//div/span//span//span/a[@name='Account_vod__c']"), "Account Name displaying"))
			{
				cf.clickElement(By.xpath("//div/span//span//span/a[@name='Account_vod__c']"), "Click Account Name");
				cf.waitForSeconds(10);
			}

		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}

}
