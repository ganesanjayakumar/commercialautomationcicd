package businesscomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import ObjectRepository.ObjVeevaEmail;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_MCCP extends ReusableLibrary {

	public Veeva_MCCP(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	Veeva_CallsStandardization vcalls=new Veeva_CallsStandardization(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 60);
	public static String territory;
	public static int appcompleted;
	public static String cptid;
	public static String cpcid;
	public static String cppid;
	public static String target;
	public static String product;
	public static String channelattainment100;
	public static String productattainment100;

	/**
	 * Navigation to MC Cycle Plans
	 * @throws Exception
	 */

	public void navigateToMCcycleplans() throws Exception{
		try {
			vf.clickVeevaTab("MC Cycle Plans");
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Select Cycle plan from MC Cycle Plans.
	 * @throws Exception
	 */

	public void selectcycleplan() throws Exception{

		try {
			if(cf.isElementVisible(By.xpath("//table[@class='list']//tr[2]"), "Cycle Plan")){
				report.updateTestLog("Verify able to see Cycle plan", "Able to see Cycle Plan", Status.PASS);
				cf.clickLink(By.xpath("//table[@class='list']//tr[2]/th/a"), "Cycle plan");
			}
			else{
				report.updateTestLog("Verify able to see Cycle plan", "Unable to see Cycle Plan", Status.FAIL);
				throw new Exception("Unable to see Cycle Plan");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	/** captureTerritory from Advance User Details.
	 * @throws Exception
	 */

	public void captureTerritory() throws Exception{
		try{
			cf.clickLink(ObjVeevaEmail.logOutUser, "UserName");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.mySettings, "My Settings");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.personalsettings, "Personal Settings");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.advanceduserdetails, "Advanced User Details");
			cf.clickSFLinkLet("Territories");
			territory=cf.getDataFromLinkLetTable("Territories", "Territory", 2);
			report.updateTestLog("Verify able to capture the Territory "+territory+" from dvance user Details", "Able to capture the capture the Territorry  "+territory+" from Advance user details", Status.SCREENSHOT);

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}	
	}
	/** capture Details of MCCP Plan Detail and Validate status and Territorry.
	 * @throws Exception
	 */
	public void mCcycleplandetail() throws Exception{
		try {
			String appstatus=cf.getSFData("Status", "text", "text");
			String appterritorry=cf.getSFData("Territory", "text", "text");
		//	String appdaystotal=cf.getSFData("Days Total", "text", "text");
		//	String appdaysremaining=cf.getSFData("Days Remaining", "text", "text");
			String appcompleteds=cf.getSFData("% Cycle Completed", "text", "text");
			appcompleted=Integer.parseInt(appcompleteds.split("%")[0]);
			dataTable.putCommonData("appcompleted", Integer.toString(appcompleted));
			cf.waitForSeconds(1);

			//String appcoverage=cf.getSFData("Coverage", "text", "text");
			if((appstatus.trim().equals("In Progress")) && ((appterritorry.trim()).equals(territory))){
				report.updateTestLog("Verify able see status "+appstatus +" and territory "+appterritorry+"with proper values.", "Able see status "+appstatus +" and territory "+appterritorry+"with proper values.", Status.PASS);
			}
			else{
				report.updateTestLog("Verify able see status "+appstatus +" and territory "+appterritorry+"with proper values.", "Unable see status "+appstatus +" and territory "+appterritorry+"with proper values.", Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	/** 
	 * Capture Details of Calculations for Channels and Validate Attainment and Status.
	 * @throws Exception
	 */
	public void calculationschannels() throws Exception{
		String calcchnlstatus;
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try {

			String appchnintgoal=cf.getSFData("Channel Interactions Goal", "text", "text");
			double appchnintgoal1=Double.parseDouble(appchnintgoal);
			String appchnintearned=cf.getSFData("Channel Interactions Earned", "text", "text");
			double appchnintearned1=Double.parseDouble(appchnintearned);
			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppChnIntEarned", appchnintearned);
			}
			String appchnintattainment=cf.getSFData("Channel Interaction Attainment", "text", "text");
			int appchnintattainment1=Integer.parseInt(appchnintattainment.split("%")[0]);
			String appchnintstatus=cf.getSFData("Channel Interaction Status", "text", "text");
			int calcchannlattainment=(int)Math.round((appchnintearned1/appchnintgoal1)*100);
			int overattainment=appcompleted+10;
			int underattainment=appcompleted-20;


			if(appchnintattainment1 == calcchannlattainment){
				report.updateTestLog("Verify able to see channel attainment "+calcchannlattainment+" value propely based on goal and earned values.", "Able to see channel attainment value "+calcchannlattainment+" propely based on goal and earned values.", Status.PASS);
				if(appchnintattainment1>=overattainment)
					calcchnlstatus="Over Reached";
				else if(appchnintattainment1<=underattainment)
					calcchnlstatus="Under Reached";
				else{
					calcchnlstatus="On Schedule";
				}
				if(appchnintstatus.equals(calcchnlstatus)){
					report.updateTestLog("Verify able to see channel attainment status "+appchnintstatus+" properly ", "Able to see channel attainment status "+appchnintstatus+" properly", Status.PASS);
				}
				else{
					report.updateTestLog("Verify able to see channel attainment status "+appchnintstatus+" properly ", "Able to see channel attainment status "+appchnintstatus+" properly", Status.FAIL);
				}
			}
			else{
				report.updateTestLog("Verify able to see channel attainment value "+calcchannlattainment+" propely based on goal and earned values.", "Unable to see channel attainment value "+calcchannlattainment+" propely based on goal and earned values.", Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/** 
	 * Capture Details of Calculations for products and Validate Attainment and Status.
	 * @throws Exception
	 */
	public void calculationsproducts() throws Exception{
		String calcprodstatus;
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try {
			String appprodintgoal=cf.getSFData("Product Interactions Goal", "text", "text");
			double appprodgoal1=Double.parseDouble(appprodintgoal);
			String appprodearned=cf.getSFData("Product Interactions Earned", "text", "text");
			double appprodearned1=Double.parseDouble(appprodearned);
			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppProdEarned", appprodearned);
			}
			String appprodattainment=cf.getSFData("Product Interaction Attainment", "text", "text");
			int appprodattainment1=Integer.parseInt(appprodattainment.split("%")[0]);
			String appprodstatus=cf.getSFData("Product Interaction Status", "text", "text");
			int overattainment=appcompleted+10;
			int underattainment=appcompleted-20;

			int calcprodattainment=(int)Math.round((appprodearned1/appprodgoal1)*100);

			if(appprodattainment1 == calcprodattainment){
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainment+" properly based on goal and earned values.", "Able to see product attainment value "+calcprodattainment+" properly based on goal and earned values.", Status.PASS);
				if(appprodattainment1>overattainment)
					calcprodstatus="Over Reached";
				else if(appprodattainment1<underattainment)
					calcprodstatus="Under Reached";
				else{
					calcprodstatus="On Schedule";
				}
				if(appprodstatus.equals(calcprodstatus)){
					report.updateTestLog("Verify able to see product attainment status "+appprodstatus+" properly ", "Able to see product attainment status "+appprodstatus+" properly", Status.PASS);
				}
				else{
					report.updateTestLog("Verify able to see product attainment status "+appprodstatus+" properly ", "Able to see product attainment status "+appprodstatus+" properly", Status.FAIL);
				}
			}
			else{
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainment+" properly based on goal and earned values.", "Unable to see product attainment value "+calcprodattainment+" propely based on goal and earned values.", Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Select Cycle Plan Target and Click the CPT.
	 * @throws Exception 
	 */
	public void selectCPT() throws Exception{
		String putcommondata=dataTable.getData("Login", "PutCommonData");

		try {
			cf.clickSFLinkLet("MC Cycle Plan Targets");
			cptid=cf.getDataFromLinkLetTable("MC Cycle Plan Targets","Cycle Plan Target", 2);
			if(putcommondata.equals("Y")){
				channelattainment100=cf.getDataFromLinkLetTable("MC Cycle Plan Targets","Channel Attainment", 2);
				productattainment100=cf.getDataFromLinkLetTable("MC Cycle Plan Targets","Product Attainment", 2);
				dataTable.putCommonData("channelattainment100", channelattainment100);
				cf.waitForSeconds(2);
				dataTable.putCommonData("productattainment100", productattainment100);
				cf.waitForSeconds(2);
			}
			target=cf.getDataFromLinkLetTable("MC Cycle Plan Targets","Target", 2);
			cf.clickLink(By.xpath("//div[@id='ep']/parent::td//td[@class='pbTitle']//*[starts-with(text(),'MC Cycle Plan Targets')]//ancestor::table/parent::div/following-sibling::div/table/tbody//tr[2]/td[2]/a"),cptid );
			if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cptid+"')]"), cptid))
				report.updateTestLog("Verify able to navigate to "+cptid+ " MC Cycle Plan Target" , "Able to navigate to "+cptid+ " MC Cycle Plan Target", Status.PASS);
			else{
				report.updateTestLog("Verify able to navigate to "+cptid+ " MC Cycle Plan Target" , "Unable to navigate to "+cptid+ " MC Cycle Plan Target", Status.FAIL);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}
	/**
	 * Validate Cycle Plan Summary both channels and products.
	 * @throws Exception 
	 */
	public void validatecyclesummary() throws Exception{
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		cf.waitForSeconds(1);
		String product=dataTable.getData("MCCP", "Product");
		try {
			int rowcount=cf.getRowCountFromLinkLetTable("Cycle Plan Summary");
			for(int i=2;i<=rowcount+1;i++){
				String cps=cf.getDataFromLinkLetTable("Cycle Plan Summary", "Cycle Plan Summary", i);
				String summaryfor=cf.getDataFromLinkLetTable("Cycle Plan Summary", "Summary For", i);
				if(summaryfor.equals("f2f")||summaryfor.equals(product)){
					cf.clickLink(By.xpath("//div[@id='ep']/parent::td//td[@class='pbTitle']//*[starts-with(text(),'Cycle Plan Summary')]//ancestor::table/parent::div/following-sibling::div/table/tbody//tr["+i+"]/th/a"),  "CPS");
					if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cps+"')]"), cps)){
						report.updateTestLog("Verify able to navigate to "+cps+ " MC Cycle Plan Summary" , "Able to navigate to "+cps+ " MC Cycle Plan Summary", Status.PASS);
						String cpsintgoal=cf.getSFData("Interactions Goal", "text", "text");
						double cpsintgoal1=Double.parseDouble(cpsintgoal);
						String cpsearned=cf.getSFData("Interactions Earned", "text", "text");
						double cpsearned1=Double.parseDouble(cpsearned);
						if(putcommondata.equals("Y")){
							if(summaryfor.equals("f2f"))
								dataTable.putCommonData("CPSEarnedf2f", cpsearned);
							else
								dataTable.putCommonData("CPSEarnedprod", cpsearned);

						}
						String cpsattainment=cf.getSFData("Attainment", "text", "text");
						int appcpsattainment1=Integer.parseInt(cpsattainment.split("%")[0]);
						int calcattainment=(int)Math.round((cpsearned1/cpsintgoal1)*100);
						if(appcpsattainment1 == calcattainment){
							report.updateTestLog("Verify able to see  "+cps+ " attainment value properly "+calcattainment+" based on goal and earned values.", "Able to see  "+cps+ " attainment value properly "+calcattainment+" based on goal and earned values.", Status.PASS);
							cf.clickLink(By.xpath("//td[.='Cycle Plan']/parent::tr//td/a"), "Cycle Plan");

						}
						else{
							report.updateTestLog("Verify able to see  "+cps+ " attainment value "+calcattainment+" properly based on goal and earned values.", "Able to see  "+cps+ " attainment value "+calcattainment+" properly based on goal and earned values.", Status.FAIL);
						}
					}
					else{
						report.updateTestLog("Verify able to navigate to "+cps+ " MC Cycle Plan Summary" , "Unable to navigate to "+cps+ " MC Cycle Plan Summary", Status.FAIL);
					}
				}
			}
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Channel Calculations in CPT
	 * @throws Exception
	 */
	public void calculationschannelscpt() throws Exception{
		String calcchnlstatuscpt;
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try{
			String appchnintgoalcpt=cf.getSFData("Channel Interactions Goal", "text", "text");
			double appchnintgoalcpt1=Double.parseDouble(appchnintgoalcpt);

	//		String appchnintremainingcpt=cf.getSFData("Channel Interactions Remaining", "text", "text");
	//		double appchnintremainingcpt1=Double.parseDouble(appchnintremainingcpt);

			String appchnintearnedcpt=cf.getSFData("Channel Interactions Earned", "text", "text");
			double appchnintearnedcpt1=Double.parseDouble(appchnintearnedcpt);

			String appchnintactualcpt=cf.getSFData("Channel Interactions Actual", "text", "text");
	//		double appchnintactualcpt1=Double.parseDouble(appchnintactualcpt);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppChnIntEarnedCPT", appchnintearnedcpt);
				cf.waitForSeconds(2);
				dataTable.putCommonData("AppChnIntActualCPT", appchnintactualcpt);
				cf.waitForSeconds(2);

			}
			String appchnintattainmentcpt=cf.getSFData("Channel Attainment", "text", "text");
			int appchnintattainmentcpt1=Integer.parseInt(appchnintattainmentcpt.split("%")[0]);

			String appchnintstatuscpt=cf.getSFData("Channel Status", "text", "text");
			int calcchannlattainmentcpt=(int)Math.round((appchnintearnedcpt1/appchnintgoalcpt1)*100);

			int overattainment=appcompleted+10;
			int underattainment=appcompleted-20;

			if(appchnintattainmentcpt1 == calcchannlattainmentcpt){
				report.updateTestLog("Verify able to see channel attainment value "+calcchannlattainmentcpt+" propely based on goal and earned values for CPT.", "Able to see channel attainment value  "+calcchannlattainmentcpt+ "propely based on goal and earned values for CPT.", Status.PASS);
				if(appchnintattainmentcpt1>overattainment)
					calcchnlstatuscpt="Over Reached";
				else if(appchnintattainmentcpt1<underattainment)
					calcchnlstatuscpt="Under Reached";
				else{
					calcchnlstatuscpt="On Schedule";
				}
				if(appchnintstatuscpt.equals(calcchnlstatuscpt)){
					report.updateTestLog("Verify able to see channel attainment status "+appchnintstatuscpt+" properly for CPT", "Able to see channel attainment status "+appchnintstatuscpt+" properly for CPT", Status.PASS);
				}
				else{
					report.updateTestLog("Verify able to see channel attainment status "+appchnintstatuscpt+" properly  for CPT", "Able to see channel attainment status "+appchnintstatuscpt+" properly for CPT", Status.FAIL);
				}
			}
			else{
				report.updateTestLog("Verify able to see channel attainment value "+calcchannlattainmentcpt +" propely based on goal and earned values for CPT.", "Unable to see channel attainment value "+calcchannlattainmentcpt+" propely based on goal and earned values for CPT.", Status.FAIL);
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void calculationsproductsCPT() throws Exception{

		String calcprodstatuscpt;
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try {
			String appprodintgoalcpt=cf.getSFData("Product Interactions Goal", "text", "text");
			double appprodgoalcpt1=Double.parseDouble(appprodintgoalcpt);

			String appprodearnedcpt=cf.getSFData("Product Interactions Earned", "text", "text");
			double appprodearnedcpt1=Double.parseDouble(appprodearnedcpt);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppProdEarnedCPT", appprodearnedcpt);
			}

			String appprodattainmentcpt=cf.getSFData("Product Attainment", "text", "text");
			int appprodattainmentcpt1=Integer.parseInt(appprodattainmentcpt.split("%")[0]);

			String appprodstatuscpt=cf.getSFData("Product Status", "text", "text");
			int overattainment=appcompleted+10;
			int underattainment=appcompleted-20;

			int calcprodattainmentcpt=(int)Math.round((appprodearnedcpt1/appprodgoalcpt1)*100);

			if(appprodattainmentcpt1 == calcprodattainmentcpt){
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpt+" properly based on goal and earned values for "+cptid, "Able to see product attainment value "+calcprodattainmentcpt +"properly based on goal and earned values for"+cptid, Status.PASS);
				if(appprodattainmentcpt1>=overattainment)
					calcprodstatuscpt="Over Reached";
				else if(appprodattainmentcpt1<=underattainment)
					calcprodstatuscpt="Under Reached";
				else{
					calcprodstatuscpt="On Schedule";
				}
				if(appprodstatuscpt.equals(calcprodstatuscpt)){
					report.updateTestLog("Verify able to see product attainment status "+calcprodstatuscpt+" properly for CPT. ", "Able to see product attainment status "+calcprodstatuscpt+" properly for CPT.", Status.PASS);
				}
				else{
					report.updateTestLog("Verify able to see product attainment status "+calcprodstatuscpt+" properly for CPT. ", "Able to see product attainment status "+calcprodstatuscpt+" properly for CPT.", Status.FAIL);
				}
			}
			else{
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpt+" properly based on goal and earned values for "+cptid, "Unable to see product attainment value "+calcprodattainmentcpt+" propely based on goal and earned values for "+cptid, Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Select Cycle Plan Channel from My Cycle Plan Channel.
	 * @throws Exception
	 */
	public void selectCPC() throws Exception{

		try {
			cf.clickSFLinkLet("MC Cycle Plan Channels");
			cpcid=cf.getDataFromLinkLetTable("MC Cycle Plan Channels","Cycle Plan Channel", 2);
			cf.clickLink(By.xpath("//div[@id='ep']/parent::td//td[@class='pbTitle']//*[starts-with(text(),'MC Cycle Plan Channels')]//ancestor::table/parent::div/following-sibling::div/table/tbody//tr[2]/th/a"),"CPC" );
			if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cpcid+"')]"), cpcid))
				report.updateTestLog("Verify able to navigate to "+cpcid+ " MC Cycle Plan Channel" , "Able to navigate to "+cpcid+ " MC Cycle Plan Channel", Status.PASS);
			else{
				report.updateTestLog("Verify able to navigate to "+cpcid+ " MC Cycle Plan Channel" , "Unable to navigate to "+cpcid+ " MC Cycle Plan Channel", Status.FAIL);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}
	/**
	 * Calculating Channel Calculations for CPC
	 * @throws Exception
	 */
	public void calcchannelscpc() throws Exception{
		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try {

			String appchnintgoalcpc=cf.getSFData("Channel Interactions Goal", "text", "text");
			double appchnintgoalcpc1=Double.parseDouble(appchnintgoalcpc);

			String appchnintearnedcpc=cf.getSFData("Channel Interactions Earned", "text", "text");
			double appchnintearnedcpc1=Double.parseDouble(appchnintearnedcpc);

			String appchnintactualcpc=cf.getSFData("Channel Interactions Actual", "text", "text");
	//		double appchnintactualcpc1=Double.parseDouble(appchnintearnedcpc);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppChnIntEarnedCPC", appchnintearnedcpc);
				cf.waitForSeconds(2);
				dataTable.putCommonData("AppChnIntActualCPC", appchnintactualcpc);
				cf.waitForSeconds(2);

			}

			String appchnintattainmentcpc=cf.getSFData("Channel Attainment", "text", "text");
			int appchnintattainmentcpc1=Integer.parseInt(appchnintattainmentcpc.split("%")[0]);

			int calcchannlattainmentcpc=(int)Math.round((appchnintearnedcpc1/appchnintgoalcpc1)*100);
			if(appchnintattainmentcpc1 == calcchannlattainmentcpc){
				report.updateTestLog("Verify able to see channel attainment value "+calcchannlattainmentcpc+" propely based on goal and earned values for"+cpcid, "Able to see channel attainment value "+calcchannlattainmentcpc+" propely based on goal and earned values for"+cpcid, Status.PASS);
			}
			else{
				report.updateTestLog("Verify able to see channel attainment value "+calcchannlattainmentcpc+" propely based on goal and earned values for"+cpcid, "Unable to see channel attainment value "+calcchannlattainmentcpc+" propely based on goal and earned values for"+cpcid, Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Calculations of Products in CPC
	 * @throws Exception
	 */
	public void calcproductscpc() throws Exception{
		String putcommondata=dataTable.getData("Login", "PutCommonData");

		try {
			String appprodintgoalcpc=cf.getSFData("Product Interactions Goal", "text", "text");
			double appprodgoalcpc1=Double.parseDouble(appprodintgoalcpc);

			String appprodearnedcpc=cf.getSFData("Product Interactions Earned", "text", "text");
			double appprodearnedcpc1=Double.parseDouble(appprodearnedcpc);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppProdEarnedCPC", appprodearnedcpc);
			}

			String appprodattainmentcpc=cf.getSFData("Product Attainment", "text", "text");
			int appprodattainmentcpc1=Integer.parseInt(appprodattainmentcpc.split("%")[0]);

			int calcprodattainmentcpc=(int)Math.round((appprodearnedcpc1/appprodgoalcpc1)*100);
			if(appprodattainmentcpc1 == calcprodattainmentcpc){
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpc+" properly based on goal and earned values for"+cpcid, "Able to see product attainment value "+calcprodattainmentcpc+" properly based on goal and earned values for"+cpcid, Status.PASS);
			}
			else{
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpc+" properly based on goal and earned values for"+cpcid, "Unable to see product attainment value "+calcprodattainmentcpc+" propely based on goal and earned values for"+cpcid, Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Calculations of Actvities in CPC
	 * @throws Exception
	 */

	public void calcactivitiescpc() throws Exception{

		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try{
			String apppactgoalcpc=cf.getSFData("Channel Activity Goal", "text", "text");
			int apppactgoalcpc1=Integer.parseInt(apppactgoalcpc);

			String apppactiactualcpc=cf.getSFData("Channel Activity Actual", "text", "text");
			int apppactiactualcpc1=Integer.parseInt(apppactiactualcpc);

			String appactiremaincpc=cf.getSFData("Channel Activity Remaining", "text", "text");
			int appactiremaincpc1=Integer.parseInt(appactiremaincpc);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppActiRemainCPC", appactiremaincpc);
				cf.waitForSeconds(2);
				dataTable.putCommonData("AppActiActualCPC", apppactiactualcpc);
				cf.waitForSeconds(2);
			}
			int calactiremaincpc=(int)Math.round(apppactgoalcpc1-apppactiactualcpc1);
			if(calactiremaincpc<0){
				calactiremaincpc=0;
			}

			if(appactiremaincpc1 == calactiremaincpc)
				report.updateTestLog("Verify able to see the Remaining Activities count "+calactiremaincpc+" properly for"+cpcid, "Able to see the Remaining Activities "+calactiremaincpc+" count properly for"+cpcid, Status.PASS);
			else
				report.updateTestLog("Verify able to see the Remaining Activities count "+calactiremaincpc+" properly for"+cpcid, "Unable to see the Remaining Activities "+calactiremaincpc+ "count properly for"+cpcid, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * |Capturing the product to be selected  from CPC while recording a call.
	 * @throws Exception
	 */

	public void captureproduct() throws Exception{
		String datasheet="MCCP";
		product=dataTable.getData(datasheet, "Product");
		try {
			int rowproducts=cf.getRowCountFromLinkLetTable("MC Cycle Plan Products");
			for(int i=2;i<=rowproducts+1;i++){
				String appproduct=cf.getDataFromLinkLetTable("MC Cycle Plan Products", "Product", i);
				cppid=cf.getDataFromLinkLetTable("MC Cycle Plan Products", "Cycle Plan Product", i);
				if((appproduct.trim()).equals(product)){
					report.updateTestLog("Verify able to see product in My Cycle Plan Products", "Able to see product in Cycle plan in Products", Status.PASS);
					cf.clickLink(By.xpath("//div[@id='ep']/parent::td//td[@class='pbTitle']//*[starts-with(text(),'MC Cycle Plan Products')]//ancestor::table/parent::div/following-sibling::div/table/tbody//tr//th[.='"+cppid+"']/a"), cppid);
					if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cppid+"')]"), cppid)){
						report.updateTestLog("Verify able to navigate to "+cppid+ " MC Cycle Plan Product" , "Able to navigate to "+cpcid+ " MC Cycle Plan Product", Status.PASS);
						break;
					}
					else{
						report.updateTestLog("Verify able to navigate to "+cppid+ " MC Cycle Plan Product" , "Unable to navigate to "+cpcid+ " MC Cycle Plan Product", Status.FAIL);
					}
				}
				else{
					report.updateTestLog("Verify able to see product in My Cycle Plan Products", "Unable to see product in Cycle plan in Products", Status.FAIL);
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}


	public void calcactivitiesCPP() throws Exception{

		String putcommondata=dataTable.getData("Login", "PutCommonData");
		try{
			String apppactgoalcpp=cf.getSFData("Product Activity Goal", "text", "text");
			double apppactgoalcpp1=Double.parseDouble(apppactgoalcpp);

			String apppactiactualcpp=cf.getSFData("Product Activity Actual", "text", "text");
			double apppactiactualcpp1=Double.parseDouble(apppactiactualcpp);

			String appactiremaincpp=cf.getSFData("Product Activity Remaining", "text", "text");
			int appactiremaincpp1=Integer.parseInt(appactiremaincpp);



			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppActiRemainCPP", appactiremaincpp);
				cf.waitForSeconds(2);
				dataTable.putCommonData("AppActiActualCPP", apppactiactualcpp);
				cf.waitForSeconds(2);
			}
			int calactiremaincpp=(int)Math.round(apppactgoalcpp1-apppactiactualcpp1);
			if(calactiremaincpp<0){
				calactiremaincpp=0;
			}

			if(appactiremaincpp1 == calactiremaincpp)
				report.updateTestLog("Verify able to see the Remaining Activities count "+calactiremaincpp+" properly for"+cppid, "Able to see the Remaining Activities count "+calactiremaincpp+" properly for"+cppid, Status.PASS);
			else
				report.updateTestLog("Verify able to see the Remaining Activities count "+calactiremaincpp+" properly for"+cppid, "Unable to see the Remaining Activities count "+calactiremaincpp+" properly"+cppid, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public void calcproductsCPP() throws Exception{
		String putcommondata=dataTable.getData("Login", "PutCommonData");

		try {
			String appprodintgoalcpp=cf.getSFData("Product Interactions Goal", "text", "text");
			double appprodgoalcpp1=Double.parseDouble(appprodintgoalcpp);

			String appprodearnedcpp=cf.getSFData("Product Interactions Earned", "text", "text");
			double appprodearnedcpp1=Double.parseDouble(appprodearnedcpp);

			if(putcommondata.equals("Y")){
				dataTable.putCommonData("AppProdEarnedCPP", appprodearnedcpp);
			}

			String appprodattainmentcpp=cf.getSFData("Product Attainment", "text", "text");
			int appprodattainmentcpp1=Integer.parseInt(appprodattainmentcpp.split("%")[0]);

			int calcprodattainmentcpp=(int)Math.round((appprodearnedcpp1/appprodgoalcpp1)*100);
			if(appprodattainmentcpp1 == calcprodattainmentcpp){
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpp+" properly based on goal and earned values for"+cppid, "Able to see product attainment value "+calcprodattainmentcpp+" properly based on goal and earned values for"+cppid, Status.PASS);
			}
			else{
				report.updateTestLog("Verify able to see product attainment value "+calcprodattainmentcpp+" properly based on goal and earned values for"+cppid, "Unable to see product attainment value "+calcprodattainmentcpp+" propely based on goal and earned values for"+cppid, Status.FAIL);
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Click on CPC and CPT and then AccountName and navigate to Record  call Page.
	 * @throws Exception 
	 */
	public void navigaterecordcallpage() throws Exception{
		try{
			cf.clickLink(By.xpath("//a[.='"+cpcid+"']"), cpcid);
			if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cpcid+"')]"), cpcid)){
				report.updateTestLog("Verify able to navigate to "+cpcid+ " MC Cycle Plan Channel" , "Able to navigate to "+cpcid+ " MC Cycle Plan Channel", Status.PASS);
				cf.clickLink(By.xpath("//a[.='"+cptid+"']"), cptid);
				if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cptid+"')]"), cptid)){
					report.updateTestLog("Verify able to navigate to "+cptid+ " MC Cycle Plan Target" , "Able to navigate to "+cptid+ " MC Cycle Plan Target", Status.PASS);
					cf.clickLink(By.xpath("//a[.='"+target+"']"), target);
					if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+target+"')]"), "Account")){
						report.updateTestLog("Verify able to navigate to account properly", "Able to navigate to account properly", Status.PASS);
						cf.clickButton(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call Button");
						wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.newcallReportPage));
						if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.newcallReportPage, "New Call Report Page"))
							report.updateTestLog("Verify able to see New Call Report Page", "Able to see New Call Report Page", Status.PASS);
						else
							report.updateTestLog("Verify able to see New Call Report Page", "Unable to see New Call Report Page", Status.FAIL);
					}
					else
						report.updateTestLog("Verify able to navigate to account properly", "Unable to navigate to account properly", Status.PASS);
				}
				else{
					report.updateTestLog("Verify able to navigate to "+cptid+ " MC Cycle Plan Target" , "Unable to navigate to "+cptid+ " MC Cycle Plan Target", Status.FAIL);
				}
			}

			else{
				report.updateTestLog("Verify able to navigate to "+cpcid+ " MC Cycle Plan Channel" , "Unable to navigate to "+cpcid+ " MC Cycle Plan Channel", Status.FAIL);
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	} 
	/**
	 * Record a call for the selected product with F2F Channel.
	 * @throws Exception
	 */
	public void recordcall() throws Exception{
		try{
			cf.selectData(ObjVeevaCallsStandardization_Commercial.deliverychannel, "Delivery Channel", "F2F");
			addproductsMCCP();
			vcalls.submitrecordcall();
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Adding Products from the Excel Sheet.
	 * @throws Exception
	 */
	public void addproductsMCCP() throws Exception {
	//	String datasheet="Login";
	//	String locale=dataTable.getData(datasheet,"Locale");
		String datasheet1="MCCP";
		String product=dataTable.getData(datasheet1,"Product");

		try{
			int size=driver.findElements(By.xpath("//table[@id='vod_detailing']//td[2]")).size();
			if(size>0){
				report.updateTestLog("Verify able to see products", "Able to see products in Detail Reporting Module", Status.PASS);
				try{
					WebElement ele=driver.findElement(By.xpath("//table[@id='vod_detailing']//label[contains(text(),'"+product+"')]/preceding-sibling::input"));
					if(!ele.isSelected()){
						cf.scrollElementToMiddlePage(ele, "Product "+product);
						cf.clickLink(By.xpath("//table[@id='vod_detailing']//label[contains(text(),'"+product+"')]/preceding-sibling::input"), "Product");
						report.updateTestLog("Verify  product selection is successful", "Able to select the product", Status.PASS);
					}
				}
				catch (Exception e) {
					throw new Exception(e.getMessage());
				}
			}
			else{
				report.updateTestLog("Verify able to see products", "Unable to see products in Detail Reporting Module", Status.FAIL);
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Login as System Admin and Batch Run
	 * @throws Exception
	 */
	public void batchrun() throws Exception{
		try {
			vf.clickVeevaTab("Veeva Process Scheduler");
			cf.clickLink(By.xpath("//a[.='Multichannel Cycle Plans Process']/parent::td/parent::tr//span[.='Run']"), "Run");
			cf.waitForSeconds(2);
			cf.clickButton(By.xpath("//button[.='Run']"), "Confirmation Run");
			cf.clickLink(By.xpath("//h1[.='Process Scheduler']/following-sibling::button"), "Refresh");
			cf.waitForSeconds(5);


		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	public void validatebatchrun() throws Exception{
		boolean statusvalue=false;
		try {
			vf.clickVeevaTab("Veeva Process Scheduler");
			cf.clickLink(By.xpath("//a[.='Multichannel Cycle Plans Process']"), "Multichannel");
			cf.switchtoNewTab(2);
			int rows=cf.getRowcountFromTable("Cycle Plan Calculation History");
			for(int i=1;i<=rows;i++){
				String status=cf.getSFData("Status", "text", "text");
				if(!status.equals("In Progress")){
					statusvalue=true;
				}
				else{
					statusvalue=false;
				}
			}
			if(statusvalue==true)
				report.updateTestLog("Verify able to see the status as finished", "Able to see status as finished", Status.PASS);
			else{
				report.updateTestLog("Verify able to see the status as finished", "Unable to see status as finished", Status.PASS);
				throw new Exception("Batch Execution is not finished");
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void captureandnavigate() throws Exception{
		captureTerritory();
		navigateToMCcycleplans();
	}
	public void cycleplanvalidations() throws Exception{
		selectcycleplan();
		mCcycleplandetail();
		calculationschannels();
		calculationsproducts();
	}
	public void validationsCPT() throws Exception{
		selectCPT();
		calculationschannelscpt();
		calculationsproductsCPT();

	}
	public void afterbatchvalidationsCPT() throws Exception{
		selectCPT();
		validatecalculationschannelscpt();
		validatecalculationsproductsCPT();

	}
	public void validationsCPC() throws Exception{
		selectCPC();
		calcchannelscpc();
		calcproductscpc();
		calcactivitiescpc();

	}
	public void afterbatchvalidationsCPC() throws Exception{
		selectCPC();
		validatecalculationschannelscpc();
		validatecalculationsproductscpc();
		validatecalcactivitiescpc();

	}
	public void validationsCPP() throws Exception{
		captureproduct();
		calcproductsCPP();
		calcactivitiesCPP();

	}
	public void afterbatchvalidationsCPP() throws Exception{
		captureproduct();
		validatecalculationsproductscpp();
		validatecalcactivitiescpp();

	}
	public void afterbatchcycleplanvalidations() throws Exception{
		navigateToMCcycleplans();
		selectcycleplan();
		validatecalculationschannels();
		validatecalculationsproducts();
	}
	/**
	 * Validate Calculations for Channels after Batch Execution
	 * @throws Exception
	 */

	public void validatecalculationschannels() throws Exception{
		double incrementvalue;
		try{
			String beforeappchnintearned=dataTable.getData("MCCP", "AppChnIntEarned");
			double beforeappchnintearned1=Double.parseDouble(beforeappchnintearned);

			String validateappchnintearned=cf.getSFData("Channel Interactions Earned", "text", "text");
			double validateappchnintearned1=Double.parseDouble(validateappchnintearned);

			channelattainment100=dataTable.getData("MCCP", "channelattainment100");

			String appcompleteds=dataTable.getData("MCCP","appcompleted");
			appcompleted=Integer.parseInt(appcompleteds);

			if(Integer.parseInt(channelattainment100)==100){
				incrementvalue=beforeappchnintearned1;
			}
			else{
				double incrementedvalidateappchnintearnedcpt=validateappchnintearned1+1;
				incrementvalue=incrementedvalidateappchnintearnedcpt;
			}

			if(validateappchnintearned1 == incrementvalue){
				report.updateTestLog("Verify able to see the value "+incrementvalue+" incremented for Channel Interactions Earned with channelattainment "+channelattainment100, "Able to see the value "+incrementvalue+" incremented for Channel Interactions Earned with channelattainment "+channelattainment100, Status.PASS);
				calculationschannels();
			}else
				report.updateTestLog("Verify able to see the value "+incrementvalue+" incremented for Channel Interactions Earned with channelattainment "+channelattainment100, "Unable to see the value "+incrementvalue+" incremented for Channel Interactions Earned with channelattainment "+channelattainment100, Status.FAIL);

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Calculations for Products after Batch Execution
	 * @throws Exception
	 */
	public void validatecalculationsproducts() throws Exception{
		double incrementvalue;
		try{
			String beforeappprodearned=dataTable.getData("MCCP", "AppProdEarned");
			double beforeappprodearned1=Double.parseDouble(beforeappprodearned);

			String validateappprodearned=cf.getSFData("Product Interactions Earned", "text", "text");
			double validateappprodearned1=Double.parseDouble(validateappprodearned);

			productattainment100=dataTable.getData("MCCP", "productattainment100");

			if(Integer.parseInt(productattainment100)==100){
				incrementvalue=beforeappprodearned1;
			}
			else{
				double incrementedvalidateappchnintearnedcpt=validateappprodearned1+1;
				incrementvalue=incrementedvalidateappchnintearnedcpt;
			}

			if(validateappprodearned1 == incrementvalue){
				report.updateTestLog("Verify able to see the value "+incrementvalue+" incremented for Product Interactions Earned with productattainment "+productattainment100, "Able to see the value "+incrementvalue+" incremented for Product Interactions Earned with productattainment "+productattainment100, Status.PASS);
				calculationsproducts();
			}else
				report.updateTestLog("Verify able to see the value "+incrementvalue+" incremented for Product Interactions Earned with productattainment "+productattainment100, "Unable to see the value "+incrementvalue+" incremented for Product Interactions Earned with productattainment "+productattainment100, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public void vavalidatecyclesummary() throws Exception{
		cf.waitForSeconds(1);
		String product=dataTable.getData("MCCP", "Product");
		double incrementvalue;
		try {
			int rowcount=cf.getRowCountFromLinkLetTable("Cycle Plan Summary");
			for(int i=2;i<=rowcount+1;i++){
				String cps=cf.getDataFromLinkLetTable("Cycle Plan Summary", "Cycle Plan Summary", i);
				String summaryfor=cf.getDataFromLinkLetTable("Cycle Plan Summary", "Summary For", i);
				if(summaryfor.equals("f2f")||summaryfor.equals(product)){
					cf.clickLink(By.xpath("//div[@id='ep']/parent::td//td[@class='pbTitle']//*[starts-with(text(),'Cycle Plan Summary')]//ancestor::table/parent::div/following-sibling::div/table/tbody//tr["+i+"]/th/a"),  "CPS");
					if(cf.isElementVisible(By.xpath("//h2[contains(text(),'"+cps+"')]"), cps)){
						report.updateTestLog("Verify able to navigate to "+cps+ " MC Cycle Plan Summary" , "Able to navigate to "+cps+ " MC Cycle Plan Summary", Status.PASS);

						String validateappprodearned=cf.getSFData("Interactions Earned", "text", "text");
						double validateappprodearned1=Double.parseDouble(validateappprodearned);						

						if(summaryfor.equals("f2f")){
							String beforeftof=dataTable.getData("MCCP", "CPSEarnedf2f");
							double beforeftof1=Double.parseDouble(beforeftof);
							if(channelattainment100.equals("100")){
								incrementvalue=beforeftof1;
							}
							else{
								incrementvalue=beforeftof1+1;
							}
						}
						else{

							String beforeprod=dataTable.getData("MCCP", "CPSEarnedprod");
							double beforeprod1=Double.parseDouble(beforeprod);
							if(productattainment100.equals("100")){
								incrementvalue=beforeprod1;
							}
							else{
								incrementvalue=beforeprod1+1;
							}
						}

						if(validateappprodearned1 == incrementvalue){
							report.updateTestLog("Verify able to see "+incrementvalue+"  value properly based for earned based on attainment..", "Able to see "+incrementvalue+" value properly based for earned based on attainment..", Status.PASS);
							cf.clickLink(By.xpath("//td[.='Cycle Plan']/parent::tr//td/a"), "Cycle Plan");

						}
						else{
							report.updateTestLog("Verify able to see "+incrementvalue+" value properly based for earned based on attainment..", "Unable to "+incrementvalue+" value properly based for earned based on attainment..", Status.FAIL);
						}
					}
					else{
						report.updateTestLog("Verify able to navigate to "+cps+ " MC Cycle Plan Summary" , "Unable to navigate to "+cps+ " MC Cycle Plan Summary", Status.FAIL);
					}
				}
			}
			validatecyclesummary();
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Calculations for Channels for CPT after Batch Execution
	 * @throws Exception
	 */

	public void validatecalculationschannelscpt() throws Exception{
		double incrementvalue;
		double validate;
		try{
			String beforeappchnintearnedcpt=dataTable.getData("MCCP", "AppChnIntEarnedCPT");
			double beforeappchnintearnedcpt1=Double.parseDouble(beforeappchnintearnedcpt);

			String beforeappchnintactualcpt=dataTable.getData("MCCP", "AppChnIntActualCPT");
			double beforeappchnintactualcpt1=Double.parseDouble(beforeappchnintactualcpt);


			String validateappchnintearnedcpt=cf.getSFData("Channel Interactions Earned", "text", "text");
			double validateappchnintearnedcpt1=Double.parseDouble(validateappchnintearnedcpt);

			String validateappchnintactualcpt=cf.getSFData("Channel Interactions Actual", "text", "text");
			double validateappchnintactualcpt1=Double.parseDouble(validateappchnintactualcpt);



			if(Integer.parseInt(channelattainment100)==100){
				double incrementedvalidateappchnintactualcpt=beforeappchnintactualcpt1+1;
				incrementvalue=incrementedvalidateappchnintactualcpt;
				validate=validateappchnintactualcpt1;
			}
			else{
				double incrementedvalidateappchnintearnedcpt=beforeappchnintearnedcpt1+1;
				incrementvalue=incrementedvalidateappchnintearnedcpt;
				validate=validateappchnintearnedcpt1;
			}

			if(validate == incrementvalue){
				report.updateTestLog("Verify able to see the "+incrementvalue+"value  for Channel Interactions Earned for "+cptid +"based on Channel Attainment value."+channelattainment100, "Able to see the value "+incrementvalue+" for Channel Interactions Earned for "+cptid+" based on Channel Attainment value "+channelattainment100, Status.PASS);
				calculationschannelscpt();
			}else
				report.updateTestLog("Verify able to see the "+incrementvalue+"value  for Channel Interactions Earned for "+cptid +"based on Channel Attainment value."+channelattainment100, "Unable to see the value "+incrementvalue+" for Channel Interactions Earned for "+cptid+" based on Channel Attainment value "+channelattainment100, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Calculations for Products for CPT after Batch Execution
	 * @throws Exception
	 */
	public void validatecalculationsproductsCPT() throws Exception{
		double incrementvalue;
		try{
			String beforeappprodearnedcpt=dataTable.getData("MCCP", "AppProdEarnedCPT");
			double beforeappprodearnedcpt1=Double.parseDouble(beforeappprodearnedcpt);

			String validateappprodearnedcpt=cf.getSFData("Product Interactions Earned", "text", "text");
			double validateappprodearnedcpt1=Double.parseDouble(validateappprodearnedcpt);

			if(Integer.parseInt(productattainment100)==100){
				incrementvalue=beforeappprodearnedcpt1;
			}
			else{
				double incrementedvalidateappprodintearnedcpt=validateappprodearnedcpt1+1;
				incrementvalue=incrementedvalidateappprodintearnedcpt;
			}

			if(validateappprodearnedcpt1 == incrementvalue){
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cptid+" based on Product Attainment Value "+productattainment100, "Able to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cptid+" based on product Attainment value" +productattainment100, Status.PASS);
				calculationsproductsCPT();
			}else
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cptid+" based on Product Attainment Value "+productattainment100, "Unable to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cptid+" based on product Attainment value" +productattainment100, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Calculations for Channels for CPC after Batch Execution
	 * @throws Exception
	 */

	public void validatecalculationschannelscpc() throws Exception{
		double incrementvalue,validate;
		try{
			String beforeappchnintearnedcpc=dataTable.getData("MCCP", "AppChnIntEarnedCPC");
			double beforeappchnintearnedcpc1=Double.parseDouble(beforeappchnintearnedcpc);

			String beforeappchnintactualcpc=dataTable.getData("MCCP", "AppChnIntActualCPC");
			double beforeappchnintactualcpc1=Double.parseDouble(beforeappchnintactualcpc);


			String validateappchnintearnedcpc=cf.getSFData("Channel Interactions Earned", "text", "text");
			double validateappchnintearnedcpc1=Double.parseDouble(validateappchnintearnedcpc);

			String validateappchnintactualcpc=cf.getSFData("Channel Interactions Actual", "text", "text");
			double validateappchnintactualcpc1=Double.parseDouble(validateappchnintactualcpc);

			if(Integer.parseInt(channelattainment100)==100){
				double incrementedvalidateappchnintactualcpc=beforeappchnintactualcpc1+1;
				incrementvalue=incrementedvalidateappchnintactualcpc;
				validate=validateappchnintactualcpc1;
			}
			else{
				double incrementedvalidateappchnintearnedcpt=beforeappchnintearnedcpc1+1;
				incrementvalue=incrementedvalidateappchnintearnedcpt;
				validate=validateappchnintearnedcpc1;
			}

			if(validate == incrementvalue){
				report.updateTestLog("Verify able to see the "+incrementvalue+"value  for Channel Interactions Earned for "+cpcid +"based on Channel Attainment value."+channelattainment100, "Able to see the value "+incrementvalue+" for Channel Interactions Earned for "+cpcid+" based on Channel Attainment value "+channelattainment100, Status.PASS);
				calcchannelscpc();
			}else
				report.updateTestLog("Verify able to see the "+incrementvalue+"value  for Channel Interactions Earned for "+cpcid +"based on Channel Attainment value."+channelattainment100, "Unable to see the value "+incrementvalue+" for Channel Interactions Earned for "+cpcid+" based on Channel Attainment value "+channelattainment100, Status.FAIL);

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Validate Calculations for Products for CPC after Batch Execution
	 * @throws Exception
	 */

	public void validatecalculationsproductscpc() throws Exception{
		double incrementvalue;
		try{
			String beforeappprodintearnedcpc=dataTable.getData("MCCP", "AppProdEarnedCPC");
			double beforeappprodintearnedcpc1=Double.parseDouble(beforeappprodintearnedcpc);

			String validateappprodintearnedcpc=cf.getSFData("Product Interactions Earned", "text", "text");
			double validateappprodintearnedcpc1=Double.parseDouble(validateappprodintearnedcpc);

			if(Integer.parseInt(productattainment100)==100){
				incrementvalue=beforeappprodintearnedcpc1;
			}
			else{
				double incrementedvalidateappprodintearnedcpt=validateappprodintearnedcpc1+1;
				incrementvalue=incrementedvalidateappprodintearnedcpt;
			}

			if(validateappprodintearnedcpc1 == incrementvalue){
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cpcid+" based on Product Attainment Value "+productattainment100, "Able to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cpcid+" based on product Attainment value" +productattainment100, Status.PASS);
				calcproductscpc();
			}else
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cpcid+" based on Product Attainment Value "+productattainment100, "Unable to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cpcid+" based on product Attainment value" +productattainment100, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Calculating Remaining Activities for CPC.
	 * @throws Exception 
	 */
	public void validatecalcactivitiescpc() throws Exception{
		int actualact;

		try{
			String beforeappactiremaincpc=dataTable.getData("MCCP", "AppActiRemainCPC");
			int beforeappactiremaincpc1=Integer.parseInt(beforeappactiremaincpc);

			String beforeappactiactualcpc=dataTable.getData("MCCP", "AppActiActualCPC");
			int beforeappactiactualcpc1=Integer.parseInt(beforeappactiactualcpc);

			String validateappactiremaincpc=cf.getSFData("Channel Activity Remaining", "text", "text");
			int validateappactiremaincpc1=Integer.parseInt(validateappactiremaincpc);

			String validateappactiactualcpc=cf.getSFData("Channel Activity Actual", "text", "text");
			int validateappactiactualcpc1=Integer.parseInt(validateappactiactualcpc);

			int deccalactiremaincpc=beforeappactiremaincpc1-1;
			if(Integer.parseInt(channelattainment100) ==100){
				int asccalactiactualcpc=beforeappactiactualcpc1+1;
				actualact=asccalactiactualcpc;
			}
			else{
				actualact=beforeappactiactualcpc1;
			}
			if(deccalactiremaincpc<0){
				deccalactiremaincpc=0;
			}

			if(validateappactiremaincpc1 == deccalactiremaincpc){
				if(validateappactiactualcpc1 == actualact){
					report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpc+" and Channel Activities count "+actualact+" properly for "+cpcid+" ", "Able to see the Remaining Activities count "+deccalactiremaincpc+" and Channel Activities count "+actualact+" properly for "+cpcid+"", Status.PASS);
					calcactivitiescpc();
				}
				else{
					report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpc+" and Channel Activities count "+actualact+" properly for "+cpcid+" ", "Unable to see the Remaining Activities count "+deccalactiremaincpc+" and Channel Activities count "+actualact+" properly for "+cpcid+"", Status.FAIL);				}
			}else
				report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpc+" properly for "+cpcid, "Unable to see the Remaining Activities count "+deccalactiremaincpc+" properly for"+cpcid, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());

		}	

	}
	/**
	 * Validate Calculations for Products for CPP after Batch Execution
	 * @throws Exception
	 */

	public void validatecalculationsproductscpp() throws Exception{
		double incrementvalue;
		try{
			String beforeappprodintearnedcpp=dataTable.getData("MCCP", "AppProdEarnedCPP");
			double beforeappprodintearnedcpp1=Double.parseDouble(beforeappprodintearnedcpp);

			String validateappprodintearnedcpc=cf.getSFData("Product Interactions Earned", "text", "text");
			double validateappchnintearnedcpp1=Double.parseDouble(validateappprodintearnedcpc);

			if(Integer.parseInt(productattainment100)==100){
				incrementvalue=beforeappprodintearnedcpp1;
			}
			else{
				double incrementedvalidateappprodintearnedcpp=validateappchnintearnedcpp1+1;
				incrementvalue=incrementedvalidateappprodintearnedcpp;
			}

			if(validateappchnintearnedcpp1 == incrementvalue){
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cppid+" based on Product Attainment Value "+productattainment100, "Able to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cppid+" based on product Attainment value" +productattainment100, Status.PASS);
				calcproductsCPP();
			}else
				report.updateTestLog("Verify able to see the value "+incrementvalue+"  incremented for Product Interactions Earned for "+cppid+" based on Product Attainment Value "+productattainment100, "Unable to see the value"  +incrementvalue+" incremented for Product Interactions Earned for "+cppid+" based on product Attainment value" +productattainment100, Status.FAIL);

		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	/**
	 * Calculating Remaining Activities for CPP.
	 * @throws Exception 
	 */
	public void validatecalcactivitiescpp() throws Exception{
		int actualact;
		try{
			String beforeappactiremaincpp=dataTable.getData("MCCP", "AppActiRemainCPP");
			int beforeappactiremaincpp1=Integer.parseInt(beforeappactiremaincpp);

			String beforeappactiactualcpp=dataTable.getData("MCCP", "AppActiActualCPP");
			int beforeappactiactualcpp1=Integer.parseInt(beforeappactiactualcpp);


			String validateappactiremaincpp=cf.getSFData("Product Activity Remaining", "text", "text");
			int validateappactiremaincpp1=Integer.parseInt(validateappactiremaincpp);

			String validateappactiactualcpp=cf.getSFData("Product Activity Actual", "text", "text");
			int validateappactiactualcpp1=Integer.parseInt(validateappactiactualcpp);

			int deccalactiremaincpp=beforeappactiremaincpp1-1;

			if(Integer.parseInt(productattainment100) ==100){
				int asccalactiactualcpc=beforeappactiactualcpp1+1;
				actualact=asccalactiactualcpc;
			}
			else{
				actualact=beforeappactiactualcpp1;
			}


			if(deccalactiremaincpp<0){
				deccalactiremaincpp=0;
			}

			if(validateappactiremaincpp1 == deccalactiremaincpp){
				if(validateappactiactualcpp1 ==actualact){
					report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpp+" and Channel Activities count "+actualact+" properly for "+cppid+" ", "Able to see the Remaining Activities count "+deccalactiremaincpp+" and Channel Activities count "+actualact+" properly for "+cppid+"", Status.PASS);
					calcactivitiesCPP();
				}
				else{
					report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpp+" and Channel Activities count "+actualact+" properly for "+cppid+" ", "Unable to see the Remaining Activities count "+deccalactiremaincpp+" and Channel Activities count "+actualact+" properly for "+cppid+"", Status.FAIL);
				}
			}else
				report.updateTestLog("Verify able to see the Remaining Activities count "+deccalactiremaincpp+" properly for "+cppid, "Unable to see the Remaining Activities count "+deccalactiremaincpp+" properly for "+cppid, Status.FAIL);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());

		}	

	}



}

