package businesscomponents;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.Status;

import ObjectRepository.objVeevaOneKAM;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
//import supportlibraries.VeevaFunctions;

public class Veeva_US_OneKAM extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	//private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	static objVeevaOneKAM objOneKAM = new objVeevaOneKAM();
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);

	public Veeva_US_OneKAM(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public void enterPAndTProcess() throws Exception{

		cf.selectARandomOptionFromComboBox(cf.getSFElementXPath("P&T Process", "Sponsor Required for Formulary Review?", "select"), "Formulatory Review");
		
	}
	
	public void saveAndVerifySavedAccountPlanInUSKAM() throws Exception{
		cf.clickSFButton("Save", "submit");
		String strAccountPlanName=dataTable.getData("OneKAMAccountPlans", "AccountPlan");
		String strAccountName=dataTable.getData("OneKAMAccountPlans", "Account");
		cf.verifySFElementData("Account Plan Detail", "Account Plan Name", "header labels", "text",strAccountPlanName);
		cf.verifySFElementData("Account Plan Detail", "Account", "header labels", "text",strAccountName);
		cf.verifySFElementData("Account Plan Detail", "Start Date", "header help", "", "");
		cf.verifySFElementData("Account Plan Detail", "Record Type", "header labels", "text", "Account Plan AZ US Sales");
		//<<to be included for validating Therapeutic area and Owner
		cf.verifySFElementData("Account Plan Detail", "Status", "header labels", "text", "New");
	}
	
	public void exportToPowerPoint() throws Exception{
		cf.waitForSeconds(2);
		cf.switchToFrame(By.cssSelector("[title='Export_To_Powerpoint_Account_Plan_vod']"));
		try {
			File file = new File("C:\\workspace\\samplefiletoattach.pptx");
			boolean fvar = file.createNewFile();
			if (fvar)	System.out.println("PPT file has been created successfully");
			else				System.out.println("PPT File already present at the specified location");
			FileOutputStream out = new FileOutputStream(file);//"C:\\workspace\\samplefiletoattach.pptx");
			@SuppressWarnings("resource")
			XMLSlideShow slideShow = new XMLSlideShow();
			slideShow.createSlide();
			slideShow.createSlide();
			slideShow.write(out);
			out.close();
		} catch (IOException e) {
			report.updateTestLog("Error with PPT file creation", "Error occurred while creating PPT : " +e.getMessage(), Status.FAIL);
		}
		//cf.clickSFButton("Choose File", "pbButton");
		driver.findElement(By.id("fileinputobject")).sendKeys("C:\\workspace\\samplefiletoattach.pptx");
		cf.waitForSeconds(3);
		driver.findElement(By.cssSelector("[value='Export']")).click();
		report.updateTestLog("Attachments for account plan", "File attachment is done", Status.SCREENSHOT);
		cf.switchToParentFrame();
	}

	
}

