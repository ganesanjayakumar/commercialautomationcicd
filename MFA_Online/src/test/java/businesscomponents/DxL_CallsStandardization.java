package businesscomponents;

import ObjectRepository.ObjVeevaEmail;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class DxL_CallsStandardization extends VeevaFunctions {

	public DxL_CallsStandardization(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}
	MyAccountsPage ma=new MyAccountsPage(scriptHelper);
//	private DxL_Email de = new  DxL_Email(scriptHelper);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(),30);
	VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	MyAccountsPage ap = new MyAccountsPage(scriptHelper);
	/**
	 * Default Delivery channel of user will be stored in variable from My Settings Page
	 * and navigate to My Accounts Page.
	 * @throws Exception 
	 */
	public  String datecalls;
	public  String datetimecalls;

	public void clickAccountDxL() throws Exception {
		if(cf.isElementVisible(ObjVeevaEmail.menu,"Veeva - User Home Page")){
			cf.waitForSeconds(2); 
			vf.clickVeevaTab("My Accounts");
			cf.switchToFrame(ObjVeevaEmail.mailFrame);
			if(cf.isElementVisible(ObjVeevaEmail.viewdropdown, "View Dropdown"))
				report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is successful", Status.PASS);	
			else {
				report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is unsuccessful", Status.FAIL);	
				throw new FrameworkException("Clicking on Accounts link is unsuccessful");
			}
		}
	}
	
	public void accountTypeSelectDxL() throws Exception {
		
		String strDataSheet1 = "Call";
		String strTypeView = dataTable.getData(strDataSheet1, "TypeView");
		String strTableID = dataTable.getData(strDataSheet1, "Table Id");
		
		if(cf.isElementVisible(ObjVeevaEmail.viewdropdown, "View Dropdown"))
			report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is successful", Status.PASS);	
		else {
			report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is unsuccessful", Status.FAIL);	
		}
		
		cf.clickLink(By.xpath("//span[@class='fFooter']/a[text()='Create New View']"), "Create new View");
		cf.waitForSeconds(5);
		cf.clickLink(By.xpath("//tr/td/a[text()='Clear All']"), "Clear all View");
		cf.clickElement(By.xpath("//table[@id='"+strTableID+"']/tbody/tr/td/label[text()='"+strTypeView+"']"), "Click Check Box");
		cf.clickElement(By.xpath("//div[@class='requiredInput']/input[@id='vwname']"), "Click text box");
		cf.setData(By.xpath("//div[@class='requiredInput']/input[@id='vwname']"), "Input", strTypeView);
		cf.clickElement(By.xpath("//td[@class='pbButton']/input[@id='saveBtn']"), "Click Save");
		cf.waitForSeconds(10);
		
		
		
	}
	
	public void selectViewOptionandClickAccountdDxL() throws Exception {
		String strDataSheet = "Call";
		String strDataSheet1 = "Login";
		dataTable.getData(strDataSheet, "SF_ViewOption");
		String location = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		/*if(cf.isElementVisible(ObjVeevaEmail.viewdropdown,"View Dropdown")){
			cf.selectData(ObjVeevaEmail.viewdropdown, "selecting view option", strViewOption); 
			cf.waitForSeconds(7);
			if(location.equals("JP") && profile.equals("Medical")){
				cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
				cf.waitForSeconds(7);
			}
			Select s=new Select(driver.findElement(By.id("vwid")));
			String value=s.getFirstSelectedOption().getText();
			if(strViewOption.equals(value)){
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is successful", Status.PASS);	
				viewselect=true;
			}
			else {
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is unsuccessful", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		*/
		//if(viewselect==true){
			cf.waitForSeconds(5);
			Boolean accountselection=false;
			String accountname = null;
			String accountSelected =dataTable.getData(strDataSheet,"AccountSelected").trim();
			List<WebElement> accounts=driver.findElements(By.xpath("//table[@id='vodResultSet']//tr"));
			for(int i=1;i<=accounts.size();i++){
				if(location.equals("JP") && profile.equals("Commercial")){
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[3]/a")).getText();
				}
				else{
					cf.switchToFrame(ObjVeevaEmail.mailFrame);
					if(i==1)
						i++;
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']/tbody//tr["+i+"]//td[2]/a")).getText().trim();
					cf.waitForSeconds(5);
				}
				if(accountSelected.equalsIgnoreCase(accountname)){
					
					cf.scrollToElement(By.xpath("//table[@id='vodResultSet']/tbody//tr["+i+"]//td[2]/a"), accountname);
					driver.findElement(By.xpath("//table[@id='vodResultSet']/tbody//tr["+i+"]//td[2]/a")).click();
					cf.waitForSeconds(15);
					accountselection=true;
					break;
				}
				}
			if(accountselection==true)
				report.updateTestLog("Able to select the account", "Selecting account is successful", Status.PASS);
			else{
				report.updateTestLog("Able to select the account", "Selecting account is unsuccessful", Status.FAIL);	
				//frameworkparameters.setStopExecution(true);
			}
	}

	
		
	public void clickrecordcall_DxL() throws Exception{
		String datasheet="Login";
		dataTable.getData(datasheet, "Record Type");
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaCallsStandardization_Commercial.recordCallButton));
			if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call")){
				report.updateTestLog("Verify able to see Record Call Button", "Able to see Record Call Button", Status.PASS);
				cf.clickButton(ObjVeevaCallsStandardization_Commercial.recordCallButton, "Record Call");
				cf.waitForSeconds(13);
				report.updateTestLog("Verify able to see New Call Report Page", "Able to see New Call Report Page", Status.PASS);
				/*if(recordtype.equals("Pharmacy")){
					page=ObjVeevaCallsStandardization_Commercial.pharmacypage;
					wait.until(ExpectedConditions.elementToBeClickable(page));
				}
				else if (recordtype.equals("Back Office Samples")){
					page=ObjVeevaCallsStandardization_Commercial.backoffsamplespage;
					wait.until(ExpectedConditions.elementToBeClickable(page));
				}
				else if (recordtype.equals("Selling")||recordtype.equals("Non-Selling")){
					page=ObjVeevaCallsStandardization_Commercial.sellingpage;
					wait.until(ExpectedConditions.elementToBeClickable(page));
				}
				//				else if (recordtype.equals("Non-Selling")){
				//					page=ObjVeevaCallsStandardization_Commercial.nonsellingpage;
				//					wait.until(ExpectedConditions.elementToBeClickable(page));
				//				}
				else{
					page=ObjVeevaCallsStandardization_Commercial.newcallReportPage;
					wait.until(ExpectedConditions.elementToBeClickable(page));
				}
				if(cf.isElementVisible(page, "New Call Report Page")){
					//timeformat();

					report.updateTestLog("Verify able to see New Call Report Page", "Able to see New Call Report Page", Status.PASS);

				}
				else{
					report.updateTestLog("Verify able to see New Call Report Page", "Unable to see New Call Report Page", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}*/
			}
			else{
				report.updateTestLog("Verify able to see Record Call Button", "Unable to see Record Call Button", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			} 
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void saverecordDXL() throws Exception{
		try {
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.savechanges, "Save");
		cf.waitForSeconds(15);
			report.updateTestLog("Save Button is displayed", "Able to save successfully", Status.PASS);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void saverecordDxIn() throws Exception{
		try {
			cf.clickButton(By.xpath("//input[@id='pbButtonTableColSave']"), "Save");
		cf.waitForSeconds(12);
			report.updateTestLog("Save Button is displayed", "Able to save successfully", Status.PASS);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
public void saverecordUSDxl() throws Exception{
		
		try {
			cf.waitForSeconds(15);
			cf.clickButton(By.xpath("//td[@id='pbButtonTableCol']//input[@value='Save']"), "Save");		
			report.updateTestLog("Save Button is displayed", "Able to save successfully", Status.PASS);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void submitrecordDXL() throws Exception{
		try {
			cf.clickButton(By.xpath("//td[@id='topButtonRow']//input[@value='Submit']"), "Submit");
			cf.waitForSeconds(15);
			report.updateTestLog("Submit Button is displayed", "Able to submit successfully", Status.PASS);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void editRecordDXL() throws Exception{
		try {
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Edit'])[1]"), "Edit");
			cf.waitForSeconds(15);
			report.updateTestLog("Edit Button is displayed", "Able to edit successfully", Status.PASS);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void clickMoreActionDxL() throws Exception{
		String assignuser = dataTable.getData("Login", "Assign_To_User");
		String locale = dataTable.getData("Login", "Locale");
		try{
			cf.waitForSeconds(10);
			cf.clickElement(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "Click More Actions Button");
		if(!locale.equalsIgnoreCase("CA")){
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical/Dx Inquiry')])[1]"), "Click Medical Inquiry button");
			cf.waitForSeconds(5);
		}else{
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical Inquiry')])[1]"), "Click Medical Inquiry button");
			cf.waitForSeconds(5);
			}
			if(cf.isElementPresent(By.xpath("//h2[@class='pageDescription' and text()=' Select Medical/Dx Inquiry Record Type']"))){
				cf.clickElement(By.xpath("//td[@class='pbButtonb']/input[@value='Continue']"), "Click Continue button");	
			}else if(cf.isElementPresent(By.xpath("//h2[@class='pageDescription' and text()=' Select Medical Inquiry Record Type']"))){
				cf.clickElement(By.xpath("//td[@class='pbButtonb']/input[@value='Continue']"), "Click Continue button");				
			}
			cf.waitForSeconds(5);
			Set<String> priorHandles = driver.getWindowHandles();
            String parentWindow = driver.getWindowHandle();
            cf.waitForSeconds(7);
            cf.switchToFrame(ObjVeevaEmail.mailFrame);
            if(cf.isElementVisible(By.xpath("//span[contains(@id,'Assign_To_User')]/a"), "Look Up Icon"))
            		{
                  cf.clickButton(By.xpath("//span[contains(@id,'Assign_To_User')]/a"), "Search Lookup");
                  report.updateTestLog("Verify able to see Assign to User", "Assign to user is clicked", Status.SCREENSHOT);
                  cf.waitForSeconds(25);
                  Set<String> newHandles = driver.getWindowHandles();
                  if (newHandles.size() > priorHandles.size()) 
                         for (String newHandle : newHandles) 
                                if (!priorHandles.contains(newHandle)) {
                                       driver.switchTo().window(newHandle);
                                       cf.switchToFrame(By.id("frameSearch"));
                                       try{
                                              cf.clickElement(By.xpath("//input[@id='searchBox']"), "Click Search Text box");
                                              cf.setData(By.xpath("//input[@id='searchBox']"), "Search Box", assignuser);
                                              cf.clickButton(By.xpath("//input[@id='searchButton' and @type='submit']"), "Click GO");
                                              driver.switchTo().defaultContent();
                                              cf.waitForSeconds(20);
                                              cf.switchToFrame(By.id("frameResult"));
                                              cf.clickElement(By.xpath("//th[@id='search0Name']/a"), "Click the name");
                                              cf.waitForSeconds(2);
                                              driver.switchTo().defaultContent();
                                       }catch(Exception e){
                                              Set<String> currentHandles = driver.getWindowHandles();
                                              for (String currentHandle : currentHandles)
                                                     if(currentHandle.trim().equalsIgnoreCase(parentWindow.trim())){
                                                            driver.getWebDriver().switchTo().window(parentWindow);
                                                            report.updateTestLog("Account selection", "Switch to survey target done", Status.SCREENSHOT);
                                                     }
                                       }
                                }
            }
            cf.waitForSeconds(4);
            cf.switchToFrame(ObjVeevaEmail.mailFrame);
            Select sDeliveryMethod = new Select(driver.findElement(By.xpath("//select[contains(@id,'Delivery')]")));
            sDeliveryMethod.selectByVisibleText("Email");
            
            cf.clickElement(By.xpath("//input[@id='shipToNewEmail']"), "Click checkbox");
            cf.setData(By.xpath("//input[contains(@id,'Email') and @type='text']"), "Text Box", "mail91@mailinator.com");
    		
            cf.waitForSeconds(10);
    		if(cf.isElementVisible(By.xpath("(//input[contains(@id,'Brand_Name')]/following::a)[1]"), "Look Up Icon"))
    		{
          cf.clickButton(By.xpath("(//input[contains(@id,'Brand_Name')]/following::a)[1]"), "Search Lookup");
          report.updateTestLog("Verify able to see Assign to User", "Assign to user is clicked", Status.SCREENSHOT);
          cf.waitForSeconds(5);
          Set<String> newHandles = driver.getWindowHandles();
          if (newHandles.size() > priorHandles.size()) 
                 for (String newHandle : newHandles) 
                        if (!priorHandles.contains(newHandle)) {
                        	try{
                               driver.switchTo().window(newHandle);
                               cf.switchToFrame(By.id("frameResult"));
                               cf.clickElement(By.xpath("//th[@id='search0Name']/a"), "Click the name");
                               cf.waitForSeconds(2);
                              driver.switchTo().defaultContent();
                        	}
                        	catch(Exception e){
                                Set<String> currentHandles = driver.getWindowHandles();
                                for (String currentHandle : currentHandles)
                                       if(currentHandle.trim().equalsIgnoreCase(parentWindow.trim())){
                                              driver.getWebDriver().switchTo().window(parentWindow);
                                              report.updateTestLog("Account selection", "Switch to survey target done", Status.SCREENSHOT);
                                       
                        }
                                }
    		}
    		
    		//cf.switchToFrame(By.id("itarget"));
    		cf.switchToFrame(ObjVeevaEmail.mailFrame);
    		cf.clickElement(By.xpath("//div[@class='requiredInput']/following::textarea[contains(@name,'Inquiry')]"), "Inquiry Text details");
    		cf.setData(By.xpath("//div[@class='requiredInput']/following::textarea[contains(@name,'Inquiry')]"), "Text Box", "Test");
    		
     }
    		}
		

			
			
		 catch (Exception e){
			throw new Exception(e.getMessage());
		}
		
	}

public void clickMoreActionDxlUS() throws Exception{
		String locale=dataTable.getData("Login","Locale");
		String profile=dataTable.getData("Login","Profile");	
		try{
			cf.waitForSeconds(10);
			cf.clickButton(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "More Actions");
			if(locale.equals("US") && profile.equals("DXL")){
				cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical Information Request')])[1]"), "Click Medical Information Request");
			}
			else{
			report.updateTestLog("failed to click button", "Not Clicking Medical Information Request", Status.FAIL);
			}
			cf.waitForSeconds(5);
			if(cf.isElementPresent(By.xpath("//h2[@class='pageDescription' and text()=' Select Medical Inquiry Record Type']"))){
				cf.clickElement(By.xpath("//td[@class='pbButtonb']/input[@value='Continue']"), "Click Continue button");

			}
			else
			{
				cf.clickElement(By.xpath("//td[@class='pbButtonb']/input[@value='Continue']"), "Continue button is not clicked");
			}
			cf.waitForSeconds(5);
			Set<String> priorHandles = driver.getWindowHandles();
			String parentWindow = driver.getWindowHandle();
			cf.waitForSeconds(7);
			cf.switchToFrame(By.id("itarget"));
			if(cf.isElementVisible(By.xpath("//span[contains(@id,'Assign_To_User')]/a"), "Look Up Icon"))
			{
				cf.clickButton(By.xpath("//span[contains(@id,'Assign_To_User')]/a"), "Search Lookup");
				report.updateTestLog("Verify able to see Assign to User", "Assign to user is clicked", Status.SCREENSHOT);
				cf.waitForSeconds(5);
				Set<String> newHandles = driver.getWindowHandles();
				if (newHandles.size() > priorHandles.size()) 
					for (String newHandle : newHandles) 
						if (!priorHandles.contains(newHandle)) {
							driver.switchTo().window(newHandle);
							cf.switchToFrame(By.id("frameSearch"));
							try{
								cf.clickElement(By.xpath("//input[@id='searchBox']"), "Click Search Text box");
								//cf.setData(By.xpath("//input[@id='searchBox']"), "Search Box", "Dido");
//Added by subash on 19th June 2019
//necessary data is not available in textbox
								cf.setData(By.xpath("//input[@id='searchBox']"), "Search Box", "Peter Benton");
								cf.clickButton(By.xpath("//input[@id='searchButton' and @type='submit']"), "Click GO");
								driver.switchTo().defaultContent();
								cf.waitForSeconds(20);
								cf.switchToFrame(By.id("frameResult"), "Frame Result");
								cf.clickElement(By.xpath("//th[@id='search0Name']/a"), "Click the name");
								cf.waitForSeconds(2);
								driver.switchTo().defaultContent();
							}catch(Exception e){
								Set<String> currentHandles = driver.getWindowHandles();
								for (String currentHandle : currentHandles)
									if(currentHandle.trim().equalsIgnoreCase(parentWindow.trim())){
										driver.getWebDriver().switchTo().window(parentWindow);
										report.updateTestLog("Account selection", "Switch to survey target done", Status.SCREENSHOT);
									}
							}
						}
			}
			cf.waitForSeconds(4);
			cf.switchToFrame(By.id("itarget"));
			Select sDeliveryMethod = new Select(driver.findElement(By.xpath("//select[contains(@id,'Delivery')]")));
			sDeliveryMethod.selectByVisibleText("Email");

			cf.clickElement(By.xpath("//input[@id='shipToNewEmail']"), "Click checkbox");
			cf.setData(By.xpath("//input[contains(@id,'Email') and @type='text']"), "Text Box", "mail91@mailinator.com");

			cf.waitForSeconds(10);
			if(cf.isElementVisible(By.xpath("(//input[contains(@id,'Brand_Name')]/following::a)[1]"), "Look Up Icon"))
			{
				cf.clickButton(By.xpath("(//input[contains(@id,'Brand_Name')]/following::a)[1]"), "Search Lookup");
				report.updateTestLog("Verify able to see Assign to User", "Assign to user is clicked", Status.SCREENSHOT);
				cf.waitForSeconds(5);
				Set<String> newHandles = driver.getWindowHandles();
				if (newHandles.size() > priorHandles.size()) 
					for (String newHandle : newHandles) 
						if (!priorHandles.contains(newHandle)) {
							try{
								driver.switchTo().window(newHandle);
								cf.switchToFrame(By.id("frameResult"),"Frame Result done");
								cf.waitForSeconds(2);
								cf.clickElement(By.xpath("//th[@id='search0Name']/a"), "Click the name");
								cf.waitForSeconds(2);
								driver.switchTo().defaultContent();
							}
							catch(Exception e){
								Set<String> currentHandles = driver.getWindowHandles();
								for (String currentHandle : currentHandles)
									if(currentHandle.trim().equalsIgnoreCase(parentWindow.trim())){
										driver.getWebDriver().switchTo().window(parentWindow);
										report.updateTestLog("Account selection", "Switch to survey target done", Status.SCREENSHOT);

									}
							}
						}

				cf.switchToFrame(By.id("itarget"));
				cf.clickElement(By.xpath("//div[@class='requiredInput']/following::textarea[contains(@name,'Inquiry')]"), "Inquiry Text details");
				cf.setData(By.xpath("//div[@class='requiredInput']/following::textarea[contains(@name,'Inquiry')]"), "Text Box", "Test");

			}
		}
			catch (Exception e){
			throw new Exception(e.getMessage());
		}

	}
	
	public void addattendeDxL() throws Exception{
		String strDataSheet1="Call";
		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String typeview = dataTable.getData(strDataSheet1, "TypeView");
		String attendee = dataTable.getData(strDataSheet1, "Attendee");
		try {
			if(!typeview.equalsIgnoreCase("Professional")){	
			cf.waitForSeconds(10);	
			cf.clickButton(By.xpath("//div[@class='dataCol']/span//a[contains(@name,'Interaction')]"), "Click Interaction Link");
			}
			cf.waitForSeconds(10);			
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Edit'])[1]"), "Edit");
			cf.waitForSeconds(10);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendesSearch, "Search");
			cf.setData(ObjVeevaCallsStandardization_Commercial.attendeesearchinput, "Attendee Name", attendee);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendeesearchgo, "Go");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.attendeecheckbox, "Select Attendee");
			report.updateTestLog("Verify able to see Attendee is selected", "Able to see attendee is selected", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.addattendee, "Add Attendee");
			cf.waitForSeconds(20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
			if(!locale.equals("RU") && !locale.equals("JP")){
				cf.waitForSeconds(15);
				if(cf.isElementVisible(By.xpath("//b[.='"+attendee+"']"), "Selected Attendee")){
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}

				
				else{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see selected attendee after adding", Status.FAIL);
				
				}
			}
			else{
				if(attendee.contains(" ")) attendee=attendee.split(" ")[1];
				if(cf.isElementVisible(By.xpath("//b[contains(text(),'"+attendee+"')]"), "Selected Attendee")){
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					cf.waitForSeconds(10);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}
				else{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see  selected attendee after adding", Status.FAIL);
					// frameworkParameters.setStopExecution(true);

				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void addproductDxL() throws Exception {
		String datasheet="Login";
		String locale=dataTable.getData(datasheet,"Locale");
		try{
			int size=driver.findElements(By.xpath("//table[@id='vod_detailing']//td[2]")).size();
			cf.scrollElementToMiddlePage(By.xpath("//table[@id='vod_detailing']//td[2]"), "Product");
			if(size>0){

				report.updateTestLog("Verify able to see products", "Able to see products in Detail Reporting Module", Status.PASS);
				try{
					WebElement ele=driver.findElement(By.xpath("(//table[@id='vod_detailing']/tbody[1]/tr/following::tbody/tr/td[2]/input)[1]"));
					if(!ele.isSelected()){
					cf.waitForSeconds(10);	
						cf.clickLink(By.xpath("(//table[@id='vod_detailing']/tbody[1]/tr/following::tbody/tr/td[2]/input)[1]"), "Product");
						cf.getData(By.xpath("(//table[@id='vod_detailing']/tbody[1]/tr/following::tbody/tr/td[2]/input[@checked='checked'])[1]"), "Product", "text");
						new Veeva_CallsStandardizationMedical(scriptHelper);
						/*if(locale.equals("US")||locale.equals("JP")||locale.equals("EU1"))
							vcm.keyMessagesAndComments(strHeaderProduct, "");*/
						report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
					}
				}
				catch (Exception e){
					/*if(!(locale.equals("RU")||locale.equals("EU1"))){
						WebElement ele=driver.findElement(By.xpath("//table[@id='vod_detailing']/tbody[6]//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input"));
						if(!ele.isSelected()){
							cf.clickLink(By.xpath("//table[@id='vod_detailing']/tbody[6]//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input"), "Product");
							report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
						}

					}*/
					if(!(locale.equals("RU")||locale.equals("EU1"))){
						WebElement ele=driver.findElement(By.xpath("(//table[@id='vod_detailing']/tbody//tr/td[2]/input)[1]"));
						if(!ele.isSelected()){
							cf.clickLink(By.xpath("(//table[@id='vod_detailing']/tbody//tr/td[2]/input)[1]"), "Product");
							report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
						}

					}
					else{
						WebElement ele=driver.findElement(By.xpath("//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input"));
						if(!ele.isSelected()){
							cf.clickLink(By.xpath("//table[@id='vod_detailing']/tbody//tr[@ng-repeat='i in range(group.details) track by $index']/td[2]/input"), "Product");
							report.updateTestLog("Verify able to select products", "Able to select products in Detail Reporting Module", Status.PASS);
						}
					}
				}

			}
			else{
				report.updateTestLog("Verify able to see products", "Not able to see products in Detail Reporting Module", Status.FAIL);
				// frameworkParameters.setStopExecution(true);
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
//Added by subash on 18th June 2019
//selecting IOV Provided DropDown is not available	
	
	public void addDropDownIOVDxl() throws Exception {
	
	String locale=dataTable.getData("Login","Locale");
	String profile=dataTable.getData("Login","Profile");
	
	if(locale.equals("US") && profile.equals("DXL"))
	{
		cf.waitForSeconds(10);
		cf.selectData(By.id("Promo_Items_Provided_AZ_US__c"), "IOVs Provided?", "Yes - I have provided Items of Value to HCP and recorded below");
		report.updateTestLog("IOVs Provided is Cliked", "" +"Yes - I have provided Items of Value to HCP and recorded below"+"Dropdwon option is selected", Status.PASS);
	}
	else
	{
		report.updateTestLog("IOVs Provided is Not Cliked", "" +"Yes - I have provided Items of Value to HCP and recorded below" +"Dropdwon option is not selected", Status.FAIL) ;
	} 
}
	
	public void clickMoreActionsButtonDxL() throws Exception{
		String strDataSheet1="Call";
		String locale=dataTable.getData("Login","Locale");
		String sAccountSelected = dataTable.getData(strDataSheet1, "AccountSelected");
		cf.waitForSeconds(10);
		cf.clickElement(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "Click More Actions Button");
		cf.clickElement(By.xpath("// (//div/a[contains(text(),'Medical/Dx Insight')])[1]"), "Click Medical Insight button");   // (//div/a[contains(text(),'Medical/Dx Insight')])[1]
		cf.waitForSeconds(12);
		//div[@id='pbHeader']/following::div/div[contains(.,'Select One Attendee for a Medical/Dx Insight')]
		if(cf.isElementPresent(By.xpath("//div[@id='pbHeader']/following::div/div[contains(.,'Select One Attendee for a Medical/Dx Insight')]"))){
			cf.clickElement(By.xpath("//td[text()='"+sAccountSelected+"']/preceding::td/input[@type='radio' and @name='selAccount']"), "Click radio button");
			cf.clickElement(By.xpath("//td[@class='pbButton']/input[@type='button' and @value='Next']"), "Click next button");
		}
		
		if(!(locale.equalsIgnoreCase("EU1")||locale.equalsIgnoreCase("CA"))){
		Select sEquipment = new Select(driver.findElement(By.xpath("(//select[contains(@name,'Equipment')])[1]")));
		sEquipment.selectByVisibleText("Diatech Easy");
		cf.clickElement(By.xpath("(//span[@class='multiSelectPicklistCell']/a)[1]"), "Click the add button");
		
		Select sChallenge = new Select(driver.findElement(By.xpath("(//select[contains(@name,'Challenges')])[1]")));
		sChallenge.selectByVisibleText("Test Reimbursement");
		cf.clickElement(By.xpath("(//span[@class='multiSelectPicklistCell']/a)[3]"), "Click the add button");
		}
	}

public void clickMoreActionsButtonDxlUS() throws Exception{
		String strDataSheet1="Call";
		String sAccountSelected = dataTable.getData(strDataSheet1, "AccountSelected");

		String locale=dataTable.getData("Login","Locale");
		String profile=dataTable.getData("Login","Profile");
		cf.waitForSeconds(10);
//Added by subash on 19th June 2019
//selecting AZ call type DropDown is not available	
		
		if(locale.equals("US") && profile.equals("DXL"))
		{

			cf.selectData(By.id("Call_Type_AZ_US__c"), "AZ Call Type", "Email");
			report.updateTestLog("AZ Call Type is Cliked", "" +"Email"+"Dropdwon option is selected", Status.PASS);
		}
		else
		{
			report.updateTestLog("AZ Call Type is not Cliked", "" +"Email" +"Dropdwon option is not selected", Status.FAIL) ;
		} 

		cf.clickElement(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "Click More Actions Button");	
		if(locale.equals("US") && profile.equals("DXL")){
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical/Dx Insight')])[1]"), "Click Medical Insight button");
		}
		else{
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical Insight')])[1]"), "Not Clicking Medical Insight button");
		}

		cf.waitForSeconds(5);
		//if(cf.isElementPresent(By.xpath("//div[@id='pbHeader']/following::div/div[contains(.,'Select One Attendee for a Dx')]"))){
		
			cf.clickElement(By.xpath("//td[text()='"+sAccountSelected+"']/preceding::td/input[@type='radio' and @name='selAccount']"), "Click radio button");
			cf.clickElement(By.xpath("//td[@class='pbButton']/input[@type='button' and @value='Next']"), "Click next button");
			
//Changed by subash on 19th June 2019
//UI the DropDown option is not exist	
			
		if(!(locale.equals("US") && profile.equals("DXL")))
		{
			Select sEquipment = new Select(driver.findElement(By.xpath("(//select[contains(@name,'Equipment')])[1]")));
			sEquipment.selectByVisibleText("Diatech Easy");
			cf.clickElement(By.xpath("(//span[@class='multiSelectPicklistCell']/a)[1]"), "Click the add button");

			Select sChallenge = new Select(driver.findElement(By.xpath("(//select[contains(@name,'Challenges')])[1]")));
			sChallenge.selectByVisibleText("Test Reimbursement");
			cf.clickElement(By.xpath("(//span[@class='multiSelectPicklistCell']/a)[3]"), "Click the add button");
		}
		
	}
	
	public void sendmailDxL() throws Exception {
		cf.waitForSeconds(10);
		driver.switchTo().defaultContent();
		cf.clickElement(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "Click More Actions Button");
		cf.waitForSeconds(3);
		cf.clickElement(By.xpath("(//div/a[contains(text(),'Send Email')])[1]"), "Click Send Email button");
		cf.waitForSeconds(5);
		
		if(cf.isElementVisible(By.xpath("//div[contains(@class,'popover') and text()='Send Email']"), "Click Continue")){
			cf.clickElement(By.xpath("//input[@type='button' and @value='Continue' and contains(@ng-click,'Email')]"), "Click Continue Button");
			cf.waitForSeconds(15);
			/*de.verifyTemplate();
			de.verifyEmailAccount();	
			de.checkMail();
			de.checkMailBCC();	*/
			
		}
	}
//Added by subash on 20th June 2019
//This method need for Test Script
	public void selectMoreActionDxl() throws Throwable
	{
		String locale=dataTable.getData("Login","Locale");
		String profile=dataTable.getData("Login","Profile");
		cf.waitForSeconds(10);
//Changed by subash on 20th June 2019
//selecting AZ call type is not available	
		
		if(locale.equals("US") && profile.equals("DXL"))
		{

			cf.selectData(By.id("Call_Type_AZ_US__c"), "AZ Call Type", "Email");
			report.updateTestLog("AZ Call Type is Cliked", "" +"Email"+"Dropdwon option is selected", Status.PASS);
		}
		else
		{
			report.updateTestLog("AZ Call Type is not Cliked", "" +"Email" +"Dropdwon option is not selected", Status.FAIL) ;
		} 

		cf.clickElement(By.xpath("(//input[@type='button' and @name='MoreActions'])[1]"), "Click More Actions Button");	
		if(locale.equals("US") && profile.equals("DXL")){
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical/Dx Insight')])[1]"), "Click Medical Insight button");
		}
		else{
			cf.clickElement(By.xpath("(//div/a[contains(text(),'Medical Insight')])[1]"), "Not Clicking Medical Insight button");
			
		}
	}
	public void addAttenDxl() throws Throwable
	{
		String strDataSheet1="Call";
		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String profile = dataTable.getData(strDataSheet, "Profile");
		String attendee = dataTable.getData(strDataSheet1, "Attendee");
		try {
			cf.waitForSeconds(10);			
			cf.clickButton(By.xpath("(//input[@type='button' and @name='Edit'])[1]"), "Edit");
			cf.waitForSeconds(10);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendesSearch, "Search");
			cf.setData(ObjVeevaCallsStandardization_Commercial.attendeesearchinput, "Attendee Name", attendee);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.attendeesearchgo, "Go");
			cf.clickLink(ObjVeevaCallsStandardization_Commercial.attendeecheckbox, "Select Attendee");
			report.updateTestLog("Verify able to see Attendee is selected", "Able to see attendee is selected", Status.SCREENSHOT);
			cf.clickButton(ObjVeevaCallsStandardization_Commercial.addattendee, "Add Attendee");
			cf.waitForSeconds(20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
			if(!locale.equals("RU") && !locale.equals("JP")){
				cf.waitForSeconds(10);
				if(cf.isElementVisible(By.xpath("//b[.='"+attendee+"']"), "Selected Attendee")){
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}

				else if(locale.equals("ANZ") && profile.equals("Medical"))
				{
					String[] att = attendee.split(" ");
					String x= att[1].concat(", ");
					String attName = x.concat(att[0]);
					if(cf.isElementVisible(By.xpath("//b[.='"+attName+"']"), "Selected Attendee")){
						report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
						wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
					}		
				}
				else{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see  selected attendee after adding", Status.FAIL);
					// frameworkParameters.setStopExecution(true);
				}
			}
			else{
				if(attendee.contains(" ")) attendee=attendee.split(" ")[1];
				if(cf.isElementVisible(By.xpath("//b[contains(text(),'"+attendee+"')]"), "Selected Attendee")){
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Able to see  selected attendee after adding", Status.PASS);
					cf.waitForSeconds(10);
					wait.until(ExpectedConditions.visibilityOfElementLocated(ObjVeevaCallsStandardization_Commercial.savechanges));
				}
				else{
					report.updateTestLog("Verify able to see Selected Attendee after adding", "Unable to see  selected attendee after adding", Status.FAIL);
					// frameworkParameters.setStopExecution(true);

				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	
	}
	
}

