package businesscomponents;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import com.mfa.framework.selenium.SeleniumTestParameters;

import ObjectRepository.ObjVeevaEmail;

import ObjectRepository.objMyAccounts;
import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class MySamples extends ReusableLibrary {

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	public MySamples(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Global Account Search page
	 * 
	 * @throws Exception
	 */
	public void navigateToMySamples() throws Exception {
		vf.clickVeevaTab("My Samples");
	}

	public void selectTransaction() throws Exception {

		try {
			cf.switchToFrame(By.id("itarget"));
			if (cf.isElementVisible(By.xpath("//div[@id='pbSubheader']/h3[text()='Sample Transactions']"),
					"Header text")) {
				cf.clickElement(By.xpath("//a[@id='newTransferLink']"), "Transfer Link");
				cf.waitForSeconds(15);
				cf.clickElement(By.xpath("//input[contains(@id,'Transferred_Date')]"), "TransferredDate");
				cf.clickElement(By.xpath("//div[@class='buttonBar']/a"), "Select today date");
				/*
				 * DateTimeFormatter dtf =
				 * DateTimeFormatter.ofPattern("dd/mm/yyyy"); LocalDate
				 * localDate = LocalDate.now(); String sDate =
				 * dtf.format(localDate).toString();
				 * System.out.println(dtf.format(localDate));
				 * cf.setData(By.xpath(
				 * "//input[contains(@id,'Transferred_Date')]"), "Date",
				 * dtf.format(localDate));
				 */
				// Look up Image icon

				cf.waitForSeconds(15);
				if (cf.isElementVisible(By.xpath("//span[contains(@id,'Transfer_To')]/a"), "Look Up Icon")) {
					cf.clickButton(By.xpath("//span[contains(@id,'Transfer_To')]/a"), "Search Lookup");
					report.updateTestLog("Verify able to see Transfer to", "Transfer to is clicked", Status.SCREENSHOT);
					report.updateTestLog("Verify able to see Transfer to is successfully", "Able to see Transfer to is successfully", Status.PASS);
				}
			}
			Set<String> newHandles = driver.getWindowHandles();
			Set<String> priorHandles = driver.getWindowHandles();
			String parentWindow = driver.getWindowHandle();
			if (newHandles.size() > priorHandles.size())
				for (String newHandle : newHandles)
					if (!priorHandles.contains(newHandle)) {
						try {
							driver.switchTo().window(newHandle);
							cf.switchToFrame(By.id("frameSearch"));
							cf.waitForSeconds(15);
							cf.clickElement(By.xpath("//input[@id='searchBox']"), "Click Search Text box");
							cf.setData(By.xpath("//input[@id='searchBox']"), "Search Box", "Gustavo P�rez");
							
						
							report.updateTestLog("Verify able to see searchBox is successfully clicked", "able to see searchBox is successfully clicked", Status.PASS);
							cf.waitForSeconds(15);
							cf.clickButton(By.xpath("//input[@id='searchButton' and @type='submit']"), "Click GO");
							driver.switchTo().defaultContent();
							cf.waitForSeconds(20);
							cf.switchToFrame(By.id("frameResult"));
							report.updateTestLog("Switch to frame is successfull", "Switch to frame is successfull", Status.PASS);
							cf.clickElement(By.xpath("//th[@id='search0Name']/a"), "Click the name");
						} catch (Exception e) {
							Set<String> currentHandles = driver.getWindowHandles();
							for (String currentHandle : currentHandles)
								if (currentHandle.trim().equalsIgnoreCase(parentWindow.trim())) {
									driver.getWebDriver().switchTo().window(parentWindow);
									report.updateTestLog("Account selection", "Switch to survey target done",
											Status.SCREENSHOT);

								}

						}
					}

			// Sample INformation product
			report.updateTestLog("Verify able to see Transfer to", "Transfer to is clicked", Status.SCREENSHOT);
			cf.waitForSeconds(2);
			driver.switchTo().defaultContent();
			cf.waitForSeconds(4);
			cf.switchToFrame(By.id("itarget"));
			Select sSampleInformation = new Select(
					driver.findElement(By.xpath("//select[contains(@id,'Sample_vod')]")));
			sSampleInformation.selectByVisibleText("Forxiga 10 mg 7 tabs MM MX");

			cf.waitForSeconds(4);
			Select sLotNumber = new Select(driver.findElement(By.xpath("//select[contains(@id,'Lot_vod')]")));
			sLotNumber.selectByVisibleText("JM0277");

			cf.clickElement(By.xpath("//input[contains(@id,'Quantity_vod')]"), "Input Quantity");
			cf.setData(By.xpath("//input[contains(@id,'Quantity_vod')]"), "Quantity", "9");
			report.updateTestLog("All the inouts are entered is successfully entered", "All the inouts are entered is successfully entered", Status.PASS);
		} catch (Exception e) {

		}

	}

}// End of Class
