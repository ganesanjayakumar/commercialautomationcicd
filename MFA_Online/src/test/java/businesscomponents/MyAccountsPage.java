package businesscomponents;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import com.mfa.framework.selenium.SeleniumTestParameters;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objMyAccounts;
import ObjectRepository.objVeevaAdvanceCoachingReport;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class MyAccountsPage extends ReusableLibrary
{

	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);

	public MyAccountsPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	/**
	 * Method to navigate to Global Account Search page
	 * @throws Exception 
	 */
	public void navigateToMyAccounts() throws Exception
	{
		vf.clickVeevaTab("My Accounts");
	}

	public void chooseViewInMyAccounts() throws Exception
	{
		String strViewName = dataTable.getData("Login", "View");

		cf.switchToFrame(By.xpath("//iframe[@id='vod_iframe']"), "MyAccounts List");

		cf.selectData(By.xpath("//select[@id='vwid']"), "Select Accounts View", strViewName);
		report.updateTestLog("Verify able to select the view", "Able to select the view", Status.SCREENSHOT);
		cf.switchToParentFrame();
	}

	public void clickNewSurveyTarget() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		cf.clickButton(objMyAccounts.btnSurveyTarget, "New Survey Target");
		if (strLocale.trim().equalsIgnoreCase("CN"))
		{
			if (!profile.equals("Medical"))
			{
				cf.selectData(objVeevaAdvanceCoachingReport.recordType, "Record Type", "One Time");
				report.updateTestLog("Verify able to select Surveys", "Able to select One time survey", Status.SCREENSHOT);
				cf.clickButton(objVeevaAdvanceCoachingReport.continueReport, "Continue");
			}
			Set<String> priorHandles = driver.getWindowHandles();
			String parentWindow = driver.getWindowHandle();
			cf.switchToFrame(By.id("vod_iframe"));
			if (cf.isElementVisible(By.cssSelector("img[title='Lookup (New Window)']"), "Lookup Account", 5))
			{
				cf.clickButton(By.cssSelector("img[title='Lookup (New Window)']"), "Search Lookup");
				report.updateTestLog("Verify able to select Account", "Account selection is clicked", Status.SCREENSHOT);
				Set<String> newHandles = driver.getWindowHandles();
				if (newHandles.size() > priorHandles.size())
					for (String newHandle : newHandles)
						if (!priorHandles.contains(newHandle))
						{
							driver.switchTo().window(newHandle);
							cf.switchToFrame(By.id("frameResult"));
							try
							{
								cf.clickButton(By.cssSelector("#listTableId a"), "Account Selection");
								driver.switchTo().defaultContent();
							}
							catch (Exception e)
							{
								Set<String> currentHandles = driver.getWindowHandles();
								for (String currentHandle : currentHandles)
									if (currentHandle.trim().equalsIgnoreCase(parentWindow.trim()))
									{
										driver.getWebDriver().switchTo().window(parentWindow);
										report.updateTestLog("Account selection", "Switch to survey target done", Status.SCREENSHOT);
									}
							}
						}
			}
		}
	}

	public void chooseAccountFromMyAccounts() throws Exception
	{
		String strDataSheet1 = "Login";
		String locale = dataTable.getData(strDataSheet1, "Locale");
		
		cf.switchToFrame(objMyAccounts.sframe);

	/*	String accountType = dataTable.getData(strDataSheet1, "AccountType");
		if (locale.equals("JP") && accountType.equalsIgnoreCase("HCP"))
			cf.clickElement(By.xpath("//table[@id='vodResultSet']//tr/td[3]/a"), "Click any Account");
		else*/
			//cf.clickElement(By.xpath("//table[@id='vodResultSet']//tr/td[3]/a"), "Click any Account");
		cf.clickElement(By.xpath("//table[@id='vodResultSet']//tr[6]/td[2]/a"), "Click any Account");
		cf.waitForSeconds(3);
		cf.switchToParentFrame();
	}

	public void chooseRandomAccountFromMyAccounts() throws Exception
	{
		Boolean bOpenAccount = false;
		String strDataSheet1 = "Login";
		String strAppAccount_Name = null;
		String locale = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");

		cf.switchToFrame(objMyAccounts.sframe);

		/*if (locale.equals("IC") || locale.equals("CA") || locale.equals("RU") || locale.equals("IW") || locale.equals("JP") || locale.equals("ANZ") || locale.equals("CN"))
		{
			cf.waitForSeconds(5);
			cf.switchToFrame(By.id("itarget"));
		}
		else
		{
			cf.switchToFrame(objMyAccounts.sframe);
		}*/

		if (locale.equals("JP") && profile.equals("Medical"))
		{
			cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
			cf.waitForSeconds(5);
		}

		String strTableXPath = cf.getTableXPath("vodresultset", "Accounts");
		By byTable = By.xpath(strTableXPath);
		cf.isElementVisible(byTable, "vodresultset Table: Accounts");

		int iRowCount = cf.getRowCountFromTable("vodresultset", "Accounts") + 1; //As Header row is to be considered.
		if (iRowCount >= 2)
		{
			int iRow = cf.getRandomNumber(2, iRowCount);
			strAppAccount_Name = cf.getClickDataFromTable("vodresultset", "Accounts", "Name", iRow, "Click");
		}

		if (strAppAccount_Name != null)
		{
			cf.waitForSeconds(3);
			cf.switchToParentFrame();
			if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details"))
			{
				strAppAccount_Name = cf.getSFData("Name", "text", "text");
				report.updateTestLog("Verify Account Details Opened", "Account Details Opened for Account: " + strAppAccount_Name, Status.PASS);
				bOpenAccount = true;
			}
		}
		if (!bOpenAccount)
		{
			throw new FrameworkException("Account Details not Opened.");
		}
	}

	public void openRandomAccount() throws Exception
	{
		vf.clickVeevaTab("My Accounts");
		chooseViewInMyAccounts();
		chooseRandomAccountFromMyAccounts();
	}

	public void openAccount() throws Exception
	{
		String strAccount_Name = dataTable.getData("Login", "AccountName");

		vf.clickVeevaTab("Home");
		if (strAccount_Name == null || strAccount_Name.isEmpty())
		{
			openRandomAccount();
		}
		else
		{
			vf.selectaccount();
		}
	}

	public void openAccount(String strAccount_Name) throws Exception
	{
		Boolean bOpenAccount = false;
		String strAccount_Name_Returned = vf.searchAndGo("Accounts", strAccount_Name);
		By account_TopName = By.xpath("//h2[contains(text(),'" + strAccount_Name_Returned + "')]");
		if (!(strAccount_Name_Returned == null || strAccount_Name_Returned.isEmpty()))
		{
			if (cf.isElementVisible(account_TopName, "Account Details" + strAccount_Name_Returned))
			{
				String strAppAccount_Name = cf.getSFData("Name", "text", "text");
				if (strAppAccount_Name.contains(strAccount_Name_Returned))
				{
					report.updateTestLog("Verify Account Details Opened", "Account Details Opened for Account: " + strAccount_Name_Returned, Status.PASS);
					bOpenAccount = true;
				}
			}
			else
			{

				By byPageType = cf.getSFElementXPath("PageType", "PageType");
				if (cf.isElementVisible(byPageType, "Search Results Page"))
				{
					//Get Search Account
					String strTableXPath = cf.getTableXPath("Search", "Accounts");
					if (cf.isElementVisible(By.xpath(strTableXPath), "Accounts - Search Table"))
					{
						String strAppAccount_Name = cf.verifyGetClickSearchDataFromTable("Accounts", "Name", strAccount_Name, "", "", "Click", "Search");
						if (strAppAccount_Name != null)
						{
							if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name_Returned, 5))
							{
								strAppAccount_Name = cf.getSFData("Name", "text", "text");
								strAppAccount_Name = strAppAccount_Name.replace("[View Hierarchy]", "").trim();
								if (strAppAccount_Name.equals(strAccount_Name_Returned))
								{
									report.updateTestLog("Verify Account Details Opened", "Account Details Opened for Account: " + strAccount_Name_Returned, Status.PASS);
									bOpenAccount = true;
								}
							}
						}
					}
				}
			}
		}
		if (!bOpenAccount)
		{
			throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name_Returned);
		}
	}

	public void verifyAccountDetails() throws Exception
	{
		String locale = dataTable.getData("Login", "Locale");
		String profile = dataTable.getData("Login", "Profile");
		String accountType = dataTable.getData("Login", "AccountType");

		if (accountType.equalsIgnoreCase("Professional"))
		{
			if (cf.isElementVisible(cf.getSFElementXPath("Primary Specialty", "text"), "Primary Specialty", 3))
			{
				report.updateTestLog("Verify Primary Specialty field", "Primary Specialty field is present", Status.DONE);
			}
			if (cf.isElementVisible(cf.getSFElementXPath("Specialties", "text"), "Specialties", 3))
			{
				report.updateTestLog("Verify Specialties field", "Specialties field is present", Status.DONE);
			}

			if (cf.isElementVisible(cf.getSFElementXPath("AZ Specialty", "text"), "AZ Specialty", 3))
			{
				report.updateTestLog("Verify AZ Specialty field", "AZ Specialty field is present", Status.DONE);
			}
		}

		if (profile.equalsIgnoreCase("Medical"))
		{
			String strAccountRecordType = cf.getSFData("Account Record Type", "Text", "Text");
			if (strAccountRecordType.equalsIgnoreCase("Professional") || strAccountRecordType.equalsIgnoreCase("Hospital") || strAccountRecordType.equalsIgnoreCase("Pharmacy"))
			{
				By byAmazonMDMID = cf.getSFElementXPath("Amazon MDM ID", "text");
				if (cf.isElementVisible(byAmazonMDMID, "Amazon MDM ID", 3))
				{
					cf.scrollToElement(byAmazonMDMID, "Amazon MDM ID", 300);
					cf.hoverAction(byAmazonMDMID, "Amazon MDM ID");
					cf.verifySFElementDataPresent("Amazon MDM ID", "text", "text");
					String strClass = cf.getSFData("Amazon MDM ID", "text", "class");
					if (strClass.contains("inlineEditLock"))
					{
						report.updateTestLog("Verify Amazon MDM ID is not editable", "Amazon MDM ID is not editable", Status.PASS);
					}
					else
					{
						report.updateTestLog("Verify Amazon MDM ID is not editable", "Amazon MDM ID is editable which is not expected", Status.FAIL);
					}
				}
				else if (cf.isElementVisible(cf.getSFElementXPath("External ID", "text"), "External ID", 3))
				{
					By byExternalID = cf.getSFElementXPath("External ID", "text");
					cf.scrollToElement(byExternalID, "External ID", 300);
					cf.hoverAction(byExternalID, "External ID");
					cf.verifySFElementDataPresent("External ID", "text", "text");
					String strClass = cf.getSFData("External ID", "text", "class");
					if (strClass.contains("inlineEditLock"))
					{
						report.updateTestLog("Verify External ID is not editable", "External ID is not editable", Status.PASS);
					}
					else
					{
						report.updateTestLog("Verify External ID is not editable", "External ID is editable which is not expected", Status.FAIL);
					}
				}
				else
				{
					report.updateTestLog("Verify Amazon MDM ID or External ID is Present", "Amazon MDM ID or External ID is not Present", Status.FAIL);
				}
			}
		}

		if (cf.isElementVisible(cf.getSFElementXPath("Status", "text"), "Status", 3) && !(locale.equals("CN") && profile.equals("Commercial")))
		{
			String strStatus = cf.getSFData("Status", "Text", "Text");
			if (strStatus.isEmpty())
				strStatus = "blank";
			report.updateTestLog("Verify Account Status", "Current Status is " + strStatus, Status.DONE);
		}
		if (profile.equals("Medical") && locale.equals("EU2"))
		{
			if (accountType.equals("Professional"))
			{

				By mailSalute = By.xpath("//td[text() = 'Mail Salutation']/following-sibling::td");
				cf.actionDoubleClick(mailSalute, "Mail Salute Edit");
				By mailSaluteEdit = By.xpath("//td[text() = 'Mail Salutation']/following-sibling::td//input");
				String salutation = "Sir or Madam";
				if (cf.isElementPresent(mailSaluteEdit))
				{
					cf.setData(mailSaluteEdit, "Mail Salutation", salutation);
					cf.clickSFButton("Save", "pbButton");
					report.updateTestLog("Mail Salutation", "Mail Salutation field is editable or input cannot be given", Status.PASS);
				}
				else
					report.updateTestLog("Mail Salutation", "Mail Salutation field is not editable or input cannot be given", Status.FAIL);

				String saveSalute = driver.findElement(mailSalute).getText();
				if (saveSalute.equals(salutation))

					report.updateTestLog("Mail Salutation", "Edited salutation is saved", Status.DONE);
				else
					report.updateTestLog("Mail Salutation", "Edited salutation incorrectly saved", Status.FAIL);
			}
		}
		/*Core 28 Change end*/

		//Verify User Territory Details--------------------------------------------------------------------------------------------------------
		report.updateTestLog("Verify UserTerritory User", "User", Status.DONE);
		String strPageTitle = driver.getTitle();
		String strlogin_UserNavLabel = cf.getData(objMyAccounts.login_UserNavLabel, "login_UserNavLabel", "title");
		strlogin_UserNavLabel = strlogin_UserNavLabel.replace("User menu for ", "");
		By bycustomLinks = cf.getSFElementXPath("Custom Links", "Link");
		By byAZViewUserTerritory = cf.getSFElementXPath("AZ View User Territory", "pbButton");
		Boolean bUserTerritory_UserFound = false;
		By byViewUserTerritory = null;
		String strViewUserTerritoryTxt = null;

		if (cf.isElementVisible(bycustomLinks, "custom Links - View User Territory", 5) && !(locale.equals("CN") && profile.equals("Commercial")))
		{
			byViewUserTerritory = bycustomLinks;
			strViewUserTerritoryTxt = "custom Links - View User Territory";
		}
		else if (cf.isElementVisible(byAZViewUserTerritory, "Button - View User Territory", 5) && !(locale.equals("CN") && profile.equals("Commercial")))
		{
			byViewUserTerritory = byAZViewUserTerritory;
			strViewUserTerritoryTxt = "Button - View User Territory";
		}

		if (!(locale.equals("CN") && profile.equals("Commercial")))
		{ // View user territory link is not available to CN commercial user
			try
			{
				if (cf.isElementVisible(byViewUserTerritory, strViewUserTerritoryTxt, 5))
				{
					Boolean bNewPage = cf.waitForPageToOpenAfterLinkClick2(byViewUserTerritory, strViewUserTerritoryTxt);
					if (bNewPage)
					{
						System.out.println("bNewPage: " + bNewPage);
						cf.waitForSeconds(10);
						if (cf.isElementVisible(objMyAccounts.userTerritory_Page_Heading, "userTerritory_Page_Heading", 10))
						{
							System.out.println("userTerritory_Page_Heading visible");
							String struserTerritory_TableRowsXPath = objMyAccounts.userTerritory_TableRowsXPath;
							int iRowCount = driver.findElements(By.xpath(struserTerritory_TableRowsXPath)).size();
							for (int iRow = 1; iRow <= iRowCount; iRow++)
							{
								By byUserTerritory_User = By.xpath(struserTerritory_TableRowsXPath + "[" + iRow + "]/td[2]");
								String strUserTerritory_User = cf.getData(byUserTerritory_User, "UserTerritory_User", "text");
								if (strUserTerritory_User.contains(strlogin_UserNavLabel))
								{
									cf.highLightElement(byUserTerritory_User, "User Territory - User: " + strUserTerritory_User);
									bUserTerritory_UserFound = true;
									break;
								}
							} //for loop
							if (bUserTerritory_UserFound)
							{
								report.updateTestLog("Verify UserTerritory User", "User: " + strlogin_UserNavLabel + " present in the User Territory Table", Status.PASS);
							}
							else
							{
								report.updateTestLog("Verify UserTerritory User", "User: " + strlogin_UserNavLabel + " not present in the User Territory Table", Status.FAIL);
							}
							driver.close();
						}
						else
						{//userTerritory_Page_Heading
							report.updateTestLog("Verify User Territory User Heading", " custom Links - View User Territory User Heading not present", Status.FAIL);
							driver.close();
						}
					}
					else
					{
						report.updateTestLog("Verify User Territory User", "New Page not opened", Status.FAIL);
					}
				}
				else
				{
					report.updateTestLog("Verify User Territory User", " custom Links - View User Territory/Button - View User Territory not present", Status.FAIL);
					//bycustomLinks
				}
			}
			catch (Exception e)
			{
				report.updateTestLog("Verify User Territory User", "Error: " + e.getLocalizedMessage(), Status.FAIL, false);
			}
		}
		cf.switchToPage(strPageTitle);
	}//verifyAccountDetails

	public void verifyPreferredAddress() throws Exception
	{
		String locale = dataTable.getData("Login", "Locale");
		String stdAdd;
		////////////Verify Territory Specific Fields ===> Primary Address present in Territory Specific Fields and Address Table
		String strUserMarket = cf.getData(ObjVeevaEmail.userMarket, "userMarket", "text");
		strUserMarket = strUserMarket.toUpperCase().replace("FULL", "").trim();

		if (cf.isElementVisible(objMyAccounts.frame_AccountTerritoryInfo, "frame_AccountTerritoryInfo", 5))
		{
			cf.scrollToElement(objMyAccounts.frame_AccountTerritoryInfo, "frame_AccountTerritoryInfo", 300);
			//Show the details, if it is hidden
			By byShowTerritorySpecificFields = By.xpath("//h3[text()='Territory Specific Fields']/parent::div/img");
			if (cf.isElementVisible(byShowTerritorySpecificFields, "ShowTerritorySpecificFields", 5))
			{
				String strClass = cf.getData(byShowTerritorySpecificFields, "ShowTerritorySpecificFields", "class");
				if (strClass.contains("showListButton"))
				{
					cf.clickByJSE(byShowTerritorySpecificFields, "ShowTerritorySpecificFields");
					cf.waitForSeconds(3);
				}
			} //byShowTerritorySpecificFields
			cf.switchToFrame(objMyAccounts.frame_AccountTerritoryInfo, "frame_AccountTerritoryInfo");
			String strMyPreferredAddress = "";
			if (cf.isElementVisible(objMyAccounts.myPreferredAddress, "My Preferred Address", 5))
			{
				strMyPreferredAddress = cf.getData(objMyAccounts.myPreferredAddress, "My Preferred Address", "text");
				if (strMyPreferredAddress.isEmpty())
				{
					report.updateTestLog("Territory Specific Fields", "My Preferred Address is Empty", Status.SCREENSHOT);
					cf.switchToParentFrame();
				}
				else
				{
					report.updateTestLog("Territory Specific Fields", "My Preferred Address: " + strMyPreferredAddress, Status.SCREENSHOT);
					cf.switchToParentFrame();
					cf.scrollToElement(objMyAccounts.table_Addresses, "Table - Addresses", 400);

					String[] strAdd2 = strMyPreferredAddress.split(",");
					int s = strAdd2.length;
					System.out.println(s);
					if (locale.equals("IW"))
					{
						if (s == 3)
							stdAdd = strAdd2[0] + "," + strAdd2[1];
						else
							stdAdd = strAdd2[0];
					}
					else
					{
						stdAdd = strAdd2[0];
					}
					System.out.println(stdAdd);
					int iAddress1_RowNum = cf.getRowNumDataFromTable("LinkLet", "Addresses", "Address line 1", stdAdd);

					if (iAddress1_RowNum > 1)
					{
						String strAddress2 = null, strCity = null, strZip = null, strAppAddress2 = null, strAppCity = null, strAppZip = null, strZipColName = null;
						if (strUserMarket.equalsIgnoreCase("EU"))
						{
							strZipColName = "Zip/Postcode";
						}
						else if (strUserMarket.equalsIgnoreCase("CA"))
						{
							strZipColName = "Postal Code";
						}
						else if (strUserMarket.equalsIgnoreCase("ANZ"))
						{
							strZipColName = "Post Code";
						}
						else
						{
							strZipColName = "Zip";
						}

						if (strUserMarket.equalsIgnoreCase("RU"))
						{
							strAddress2 = strMyPreferredAddress;
							strCity = strMyPreferredAddress;
							strZip = strMyPreferredAddress;
							strAppAddress2 = cf.getDataFromLinkLetTable("Addresses", "Address line 2", iAddress1_RowNum);
							cf.verifyActualExpectedData("Primary - Address Line2", strAppAddress2, strAddress2, "contains", false);
							strAppCity = cf.getDataFromLinkLetTable("Addresses", "City", iAddress1_RowNum);
							cf.verifyActualExpectedData("Primary - City", strAppCity, strCity, "contains", false);
							strAppZip = cf.getDataFromLinkLetTable("Addresses", strZipColName, iAddress1_RowNum);
							cf.verifyActualExpectedData("Primary - Zip", strZip, strAppZip, "contains", false);
						}
						else if (strUserMarket.equalsIgnoreCase("JP"))
						{
							report.updateTestLog("Addresses Related List Table", "My Preferred Address: " + strMyPreferredAddress + ", Address line 1: " + stdAdd, Status.SCREENSHOT);
							//Skip Address2, Zip, city
						}
						else
						{
							if (strMyPreferredAddress.contains(","))
							{
								String[] aryMyPreferredAddress = strMyPreferredAddress.split(",");
								int iCount = aryMyPreferredAddress.length;
								switch (iCount)
								{
									case 2 :
										//                               strAddress1 = aryMyPreferredAddress[0].trim();
										strCity = aryMyPreferredAddress[1];
										String[] strCity1 = strCity.split(" ");
										strCity = strCity1[0];
										strAppCity = cf.getDataFromLinkLetTable("Addresses", "City", iAddress1_RowNum);
										cf.verifyActualExpectedData("Primary - City", strAppCity, strCity, "contains", false);
										break;
									case 3 :
										//                                  strAddress1 = aryMyPreferredAddress[0].trim();
										strCity = aryMyPreferredAddress[1].trim();
										strZip = aryMyPreferredAddress[2].trim();
										strAppCity = cf.getDataFromLinkLetTable("Addresses", "City", iAddress1_RowNum);
										cf.verifyActualExpectedData("Primary - City", strAppCity, strCity, "contains", false);
										strAppZip = cf.getDataFromLinkLetTable("Addresses", strZipColName, iAddress1_RowNum);
										if (cf.compareActualExpectedData(strAppZip, strZip, "Exact"))
										{
											cf.verifyActualExpectedData("Primary - Zip", strAppZip, strZip, "Exact", false);
										}
										else
										{
											cf.verifyActualExpectedData("Primary - Zip", strZip, strAppZip, "Partial", false);
										}
										break;
									case 4 :
										//                                    strAddress1 = aryMyPreferredAddress[0].trim();
										strAddress2 = aryMyPreferredAddress[1].trim();
										strCity = aryMyPreferredAddress[2].trim();
										strZip = aryMyPreferredAddress[3].trim();
										strAppAddress2 = cf.getDataFromLinkLetTable("Addresses", "Address line 2", iAddress1_RowNum);
										cf.verifyActualExpectedData("Primary - Address Line2", strAppAddress2, strAddress2, "Equal", false);
										strAppCity = cf.getDataFromLinkLetTable("Addresses", "City", iAddress1_RowNum);
										cf.verifyActualExpectedData("Primary - City", strAppCity, strCity, "contains", false);
										strAppZip = cf.getDataFromLinkLetTable("Addresses", strZipColName, iAddress1_RowNum);
										if (cf.compareActualExpectedData(strAppZip, strZip, "Exact"))
										{
											cf.verifyActualExpectedData("Primary - Zip", strAppZip, strZip, "Exact", false);
										}
										else
										{
											cf.verifyActualExpectedData("Primary - Zip", strZip, strAppZip, "Partial", false);
										}
										break;
									default :
										report.updateTestLog("Verify Preferred Address", "Preferred Addresses split count is greater than 4. Develope code to handle it.", Status.FAIL);
								}
							}
						} //                  
					}
					else
					{
						report.updateTestLog("Verify Preferred addresses", "Preferred address: " + strMyPreferredAddress + " not found in the Address table.", Status.DONE);
					}
				} //strMyPreferredAddress
			}
			else
			{//My Preferred Address not found
				report.updateTestLog("Verify Preferred addresses", "Preferred address:field not found.", Status.DONE);
				cf.switchToParentFrame();
			}

		}
		else
		{
			report.updateTestLog("Territory Specific Fields", "Territory Specific Fields not found", Status.DONE);
			cf.switchToParentFrame();
		}
	}

	public void verifyAccountTactics() throws Exception
	{
		cf.scrollToTop();
		if (cf.isSFLinkLetPresent("Account Tactics") != null)
		{
			By byAccount_Name = cf.getSFElementXPath("Name", "text");
			String strAccount_Name = cf.getData(byAccount_Name, "Account_Name", "Text");
			cf.clickSFLinkLet("Account Tactics");
			report.updateTestLog("verifyAccountTactics", "Account Tactics - Records", Status.SCREENSHOT);
			cf.clickSFButton("New Account Objective/Tactic", "pbButton");
			if (vf.verifyVeevaPageOpened("New Account Objective/Tactic", "Account Objective/Tactic Edit"))
			{
				//				String strAccountPlan = dataTable.getData(strDataSheet, "Account_Plan");
				//				cf.setSFData("Account Plan", "lookupInput", strAccountPlan);
				By byAccountPlan = cf.getSFElementXPath("Account Plan", "lookupIcon");
				String strAccountPlan = vf.searchInLookupWindow(byAccountPlan, "Account Plan Lookup", "");
				String strTacticName = "Tactic_" + cf.createUnique();
				String strTacticDesc = strTacticName + " Desc";
				cf.setSFData("Tactic Name", "Input", strTacticName);
				cf.setSFData("Tactic Description", "TextArea", strTacticDesc);
				By byTactic_EndDate = By.xpath("//label[text()='Tactic End Date']/following::td[1]//span[@class='dateFormat']/a");
				String strTactic_EndDate = cf.getData(byTactic_EndDate, "Tactic_EndDate", "Text");
				cf.clickLink(byTactic_EndDate, "Tactic End Date: " + strTactic_EndDate);
				report.updateTestLog("Create Account Tactics", "Create Account Tactics - Screenshot", Status.SCREENSHOT);
				///////////////////////////////////////////////////////////////////////////////////////////
				cf.clickSFButton("Save", "pbButton");
				if (vf.verifyVeevaPageOpened(strTacticName, "Account Objective/Tactic"))
				{
					//Verify
					//					dataTable.putData(strDataSheet, "Account_Tactic_Name", strTacticName);
					By byAccountPlan2 = cf.getSFElementXPath("Account Plan", "text");
					cf.verifyPageElement(byAccountPlan2, "Account Plan", "text", strAccountPlan, "Exact", true);
					By byTacticName = cf.getSFElementXPath("Tactic Name", "text");
					cf.verifyPageElement(byTacticName, "Tactic Name", "text", strTacticName, "Exact", false);
					By byTacticDesc = cf.getSFElementXPath("Tactic Description", "text");
					cf.verifyPageElement(byTacticDesc, "Tactic Description", "text", strTacticDesc, "Exact", false);
					By byTacticEndDate = cf.getSFElementXPath("Tactic End Date", "text");
					cf.verifyPageElement(byTacticEndDate, "Tactic End Date", "text", strTactic_EndDate, "Exact", false);
					cf.clickSFLink("Account", "Link");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						//report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.PASS);
						cf.clickSFLinkLet("Account Tactics");
						String strTacticName_Returned = cf.verifyGetClickSearchDataFromTable("Account Tactics", "Tactic Name", strTacticName, "", "", "Search", "LinkLet");
						if (strTacticName_Returned != null)
						{
							String strAppAccountPlan = cf.getDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Account Plan");
							cf.verifyActualExpectedData("Account Plan", strAppAccountPlan, strAccountPlan, "Exact", false);
							//							String strAppTactic_Desc = cf.getDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Description");
							//							cf.verifyActualExpectedData("Tactic Description", strAppTactic_Desc, strTacticDesc,"Exact",false);
						}
					}
					else
					{
						//report.updateTestLog("Verify app is back to Account Details Page", "Account Details not Opened for Account: " + strAccount_Name, Status.FAIL);
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
				else
				{//Back to MyAccounts Page
					cf.clickSFLink("Account", "Link");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.DONE);
					}
					else
					{
						//report.updateTestLog("Verify Account Details Opened", "Account Details not Opened for Account: " + strAccount_Name, Status.FAIL);
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
			} //Create
		}
		else
		{
			report.updateTestLog("verifyAccountTactics", "LinkLet: Account Tactics not present", Status.DONE);
		}
	}

	public void verifyDecisionMakingUnits() throws Exception
	{
		cf.scrollToTop();
		if (cf.isSFLinkLetPresent("Decision Making Units") != null)
		{
			By byAccount_Name = cf.getSFElementXPath("Account Information", "Name", "text");
			String strAccount_Name = cf.getData(byAccount_Name, "Account_Name", "Text");
			cf.clickSFLinkLet("Decision Making Units");
			report.updateTestLog("verifyDecisionMakingUnits", "Decision Making Units - Records", Status.SCREENSHOT);
			cf.clickSFButton("New Decision Making Unit", "pbButton");
			String strDataSheet = "MyAccounts";
			if (vf.verifyVeevaPageOpened("New Decision Making Unit", "Decision Making Unit Edit"))
			{
				By byAccountPlan = cf.getSFElementXPath("Account Plan", "lookupIcon");
				String strAccountPlan = vf.searchInLookupWindow(byAccountPlan, "Account Plan Lookup", "TestABC");
				report.updateTestLog("Create Account Tactics", "Create Account Tactics - Screenshot", Status.SCREENSHOT);
				///////////////////////////////////////////////////////////////////////////////////////////
				cf.clickSFButton("Save", "pbButton");
				String strDecisionMakingUnit_Id = cf.getSFData("PageDescription", "PageDescription", "text");
				if (vf.verifyVeevaPageOpened(strDecisionMakingUnit_Id, "Decision Making Unit"))
				{
					//Verify
					By byAccountPlan2 = cf.getSFElementXPath("Account Plan", "text");
					cf.verifyPageElement(byAccountPlan2, "Account Plan", "text", strAccountPlan, "Exact", true);

					//Save Last Modified By
					String strLastModifiedBy = cf.getSFData("Last Modified By", "text", "text");
					dataTable.putData(strDataSheet, "DecisionMakingUnit_LastModifiedBy", strLastModifiedBy);
					if (cf.isElementVisible(cf.getSFElementXPath("Decision Making Unit", "Link"), "Decision Making Unit", 5))
					{
						cf.clickSFLink("Decision Making Unit", "Link");
					}
					else
					{
						cf.clickSFLink("Key Stakeholder", "Link");
					}
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						cf.clickSFLinkLet("Decision Making Units");
						String strDecisionMakingUnit_Id_Returned = cf.verifyGetClickSearchDataFromTable("Decision Making Units", "ID", strDecisionMakingUnit_Id, "", "", "Search", "LinkLet");
						if (strDecisionMakingUnit_Id_Returned != null)
						{
							String strAppAccountPlan = cf.getDataFromLinkLetTable("Decision Making Units", "ID", strDecisionMakingUnit_Id, "Account Plan");
							cf.verifyActualExpectedData("Account Plan", strAppAccountPlan, strAccountPlan, "Exact", false);
						}
					}
					else
					{
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
				else
				{//Go back to MyAccounts page
					cf.clickSFButton("Cancel", "pbButton");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.DONE);
					}
					else
					{
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
			}
		}
		else
		{
			report.updateTestLog("verifyDecisionMakingUnits", "LinkLet: Decision Making Units not present", Status.DONE);
		}
	}

	public void verifyTasks() throws Exception
	{
		cf.scrollToTop();
		if (cf.isSFLinkLetPresent("Tasks") != null)
		{
			By byAccount_Name = cf.getSFElementXPath("Name", "text");
			String strAccount_Name = cf.getData(byAccount_Name, "Account_Name", "Text");
			String strUserName = cf.getData(objMyAccounts.login_UserNavLabel, "User Name", "Text");
			cf.clickSFLinkLet("Tasks");
			report.updateTestLog("verifyTasks", "Task - Records", Status.SCREENSHOT);
			By byNewTask = By.xpath("//td[@class='pbTitle']//*[text()='Tasks']/following::td[1]//input[@*='New Task']");
			cf.clickButton(byNewTask, "New Task Button");
			if (vf.verifyVeevaPageOpened("New Task", "Task Edit"))
			{
				String strTaskName = "Task_" + cf.createUnique();
				cf.setSFData("Task Name", "Input", strTaskName);
				//				String strAccount_Tactic_Name = dataTable.getData(strDataSheet, "Account_Tactic_Name");
				//				cf.setSFData("Account Tactic", "lookupInput", strAccount_Tactic_Name);				
				By byAccount_Tactic_Name = cf.getSFElementXPath("Account Tactic", "lookupIcon");
				String strAccount_Tactic_Name = vf.searchInLookupWindow(byAccount_Tactic_Name, "Account Tactic Lookup", "TestABC");

				cf.setSFData("Assigned To", "lookupInput", strUserName);
				report.updateTestLog("Create Task", "Create Task - Screenshot", Status.SCREENSHOT);
				///////////////////////////////////////////////////////////////////////////////////////////
				cf.clickSFButton("Save", "pbButton");
				if (vf.verifyVeevaPageOpened(strTaskName, "Task"))
				{
					//Verify
					By byTaskName = cf.getSFElementXPath("Task Name", "text");
					cf.verifyPageElement(byTaskName, "Task Name", "text", strTaskName, "Exact", false);
					By byAccount_Tactic_Name2 = cf.getSFElementXPath("Account Tactic", "text");
					cf.verifyPageElement(byAccount_Tactic_Name2, "Account Tactic", "text", strAccount_Tactic_Name, "Exact", false);
					By byAssignedTo = cf.getSFElementXPath("Assigned To", "text");
					cf.verifyPageElement(byAssignedTo, "Assigned To", "text", strUserName, "Exact", false);
					cf.clickSFLink("Account", "Link");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						cf.clickSFLinkLet("Tasks");
						String strTask_Name_Returned = cf.verifyGetClickSearchDataFromTable("Tasks", "Task Name", strTaskName, "", "", "Search", "LinkLet");
						if (strTask_Name_Returned != null)
						{
							String strAppAccount_Tactic = cf.getDataFromLinkLetTable("Tasks", "Task Name", strTaskName, "Account Tactic");
							cf.verifyActualExpectedData("Account Tactic", strAppAccount_Tactic, strAccount_Tactic_Name, "Exact", false);

							String strAppAssigned_To = cf.getDataFromLinkLetTable("Tasks", "Task Name", strTaskName, "Assigned To");
							cf.verifyActualExpectedData("Assigned To", strAppAssigned_To, strUserName, "Exact", false);
							//							String strAppTactic_Desc = cf.getDataFromLinkLetTable("Account Tactics", "Tactic Name", strTacticName, "Description");
							//							cf.verifyActualExpectedData("Tactic Description", strAppTactic_Desc, strTacticDesc,"Exact",false);
						}
					}
					else
					{
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
				else
				{//Back to MyAccounts Page
					cf.clickSFLink("Cancel", "pbButton");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.DONE);
					}
					else
					{
						//report.updateTestLog("Verify Account Details Opened", "Account Details not Opened for Account: " + strAccount_Name, Status.FAIL);
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
			}
		}
		else
		{
			report.updateTestLog("verifyTasks", "LinkLet: Tasks not present", Status.DONE);
		}
	}

	public void verifyStudySites() throws Exception
	{
		cf.scrollToTop();
		if (cf.isSFLinkLetPresent("Study Sites") != null)
		{
			By byAccount_Name = cf.getSFElementXPath("Name", "text");
			String strAccount_Name = cf.getData(byAccount_Name, "Account_Name", "Text");
			cf.clickSFLinkLet("Study Sites");
			report.updateTestLog("verify Study Sites", "Study Sites - Records", Status.SCREENSHOT);
			By byNewStudySite = By.xpath("//td[@class='pbTitle']//*[text()='Study Sites']/following::td[1]//input[@*='New Study Site']");
			if (cf.isElementVisible(byNewStudySite, ""))
			{
				cf.clickButton(byNewStudySite, "New Study Site Button");
				//String strDataSheet = "MyAccounts";
				if (vf.verifyVeevaPageOpened("New Study Site", "Study Site Edit"))
				{
					//	By byStudy_Lookup_Icon = cf.getSFElementXPath("Study", "lookupIconOn");
					By byStudy_Lookup_Icon = By.xpath("//img[contains(@title,'Study Lookup (New Window)')]");
					//					String strStudy_Name = vf.selectRandomLookupData(byStudy_Lookup_Icon, "Study_Lookup_Icon");
					String strStudy_Name = vf.getLookupDataInWindow(byStudy_Lookup_Icon, "Study_Lookup_Icon", "");
					//					String strStudy_Name =vf.searchInLookupWindow(byStudy_Lookup_Icon, "Study_Lookup","Study");
					cf.waitForSeconds(4);
					if (strStudy_Name != null)
					{
						String strStudySiteName = "Study_Site_" + cf.createUnique();
						cf.setSFData("Study Site Name", "textbox", strStudySiteName);
						cf.clickSFCheckBox("Active", "Following", true);
						cf.clickSFButton("Save", "pbButton");
						if (vf.verifyVeevaPageOpened(strStudySiteName, "Study Site"))
						{
							By byStudySiteName = cf.getSFElementXPath("Study Site Name", "text");
							cf.verifyPageElement(byStudySiteName, "Study Site Name", "text", strStudySiteName, "Exact", false);
							By byStudyName = cf.getSFElementXPath("Study", "text");
							cf.verifyPageElement(byStudyName, "Study Name", "text", strStudy_Name, "Exact", false);
							cf.verifyCheckBoxStatus("Active", "Following", true, false);
							cf.clickSFLink("Account", "Link");
							if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
							{
								cf.clickSFLinkLet("Study Sites");
								String strStudySites_Name_Returned = cf.verifyGetClickSearchDataFromTable("Study Sites", "Study Site Name", strStudySiteName, "", "", "Search", "LinkLet");
								if (strStudySites_Name_Returned != null)
								{
									String strAppStudy_Name = cf.getDataFromLinkLetTable("Study Sites", "Study Site Name", strStudySiteName, "Study");
									cf.verifyActualExpectedData("Study", strAppStudy_Name, strStudy_Name, "Exact", false);
									String strStudySite_Active = cf.getDataFromLinkLetTableCheckbox("Study Sites", "Study Site Name", strStudySiteName, "Active");
									cf.verifyActualExpectedData("Addresses - Primary", strStudySite_Active, "Checked", "exact", false);
								}
							}
							else
							{
								throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
							}
						}
					}
					else
					{
						cf.clickSFButton("Cancel", "pbButton");
						if (!cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
						{
							throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
						}
					}
				}
			}
			else
			{
				report.updateTestLog("verify Study Sites", "New Study Sites Button not present", Status.DONE);
			}
		}
		else
		{
			report.updateTestLog("verify Study Sites", "LinkLet: Study Sites not present", Status.DONE);
		}
	}

	public void verifyViewHierarchy() throws Exception
	{
		cf.scrollToTop();
		String strPrimary_Parent = "";

		String profile = dataTable.getData("Login", "Profile");
		String locale = dataTable.getData("Login", "Locale");
		By byViewHierarchy = null;

		if (locale.equals("JP") && profile.equals("Commercial"))
				byViewHierarchy = cf.getSFElementXPath("AZ View User Territory", "pbButton");
			else
				byViewHierarchy = cf.getSFElementXPath("View Hierarchy", "pbButton");
		
		if (cf.isElementVisible(byViewHierarchy, "View Hierarchy", 5))
		{
			String accountType = dataTable.getData("Login", "AccountType");
			String strAccount_Name = cf.getData(objMyAccounts.accountDetails_TopName, "Account_Name", "Text");
			if (cf.isElementVisible(cf.getSFElementXPath("Primary Parent", "text"), "Primary Parent"))
			{
				strPrimary_Parent = cf.getSFData("Primary Parent", "text", "Text");
			}
			if(locale.equals("JP") && profile.equals("Commercial"))
				cf.clickSFButton("AZ View User Territory", "pbButton");
			else
				cf.clickSFButton("View Hierarchy", "pbButton");
			Boolean bFrameSwitch = cf.switchToFrame(objMyAccounts.frame_Hieratchy, "frame_Hieratchy");
			if (bFrameSwitch)
			{
				if (cf.isElementVisible(objMyAccounts.byHierarchy_Table, "Hierarchy_Table"))
				{
					report.updateTestLog("Verify Hierarchy_Table", "Hierarchy_Table - Screenshot", Status.SCREENSHOT);
					if (accountType.equalsIgnoreCase("Professional"))
					{ /////Health Care Provider/////////////////////////////////Doctors
						String strAccountName = cf.getSFData("pageDescription", "pageDescription", "text");
						cf.highLightElement(objMyAccounts.byHierarchy_Table_HCP_Self, "Hierarchy_Table - Account Name");
						String strAppAccountName = cf.getData(objMyAccounts.byHierarchy_Table_HCP_Self, "Hierarchy_Table - Account Name", "text");
						cf.verifyActualExpectedData("Hierarchy_Table - Account Name", strAppAccountName, strAccountName, "Exact", false);
						//Verify Parent
						if (!strPrimary_Parent.isEmpty())
						{
							//Verify Primary Parent
							cf.highLightElement(objMyAccounts.byHierarchy_Table_HCP_PrimaryParent, "Hierarchy_Table - Primary Parent");
							//String strAppPrimary_Parent = cf.getData(objMyAccounts.byHierarchy_Table_HCP_PrimaryParent, "Hierarchy_Table - Primary Parent", "text");
							cf.verifyPageElement(objMyAccounts.byHierarchy_Table_HCP_PrimaryParent, "Hierarchy_Table - Primary Parent", "text", strPrimary_Parent, "Exact", false);
							//Verify Connect
							int iConnectCount = driver.findElements(objMyAccounts.byHierarchy_Table_Connect).size();
							if (iConnectCount > 0)
							{
								report.updateTestLog("Verify Hierarchy_Table Connect", "Hierarchy_Table Connect is Present", Status.DONE);
							}
						}
					}
					else
					{////////////HOSPITAL //////////////////////////////////////
						String strAccountName = cf.getSFData("pageDescription", "pageDescription", "text");
						cf.highLightElement(objMyAccounts.byHierarchy_Table_HCA_Self, "Hierarchy_Table - Account Name");
						String strAppAccountName = cf.getData(objMyAccounts.byHierarchy_Table_HCA_Self, "Hierarchy_Table - Account Name", "text");
						cf.verifyActualExpectedData("Hierarchy_Table - Account Name", strAppAccountName, strAccountName, "Exact", false);
						//Verify Child present					
						int iChild_Count = driver.findElements(objMyAccounts.byHierarchy_Table_HCA_Child).size();
						if (iChild_Count > 0)
						{
							List<WebElement> e = driver.findElements(objMyAccounts.byHierarchy_Table_HCA_Child);
							cf.highLightElement(e.get(0), "Hierarchy_Table - Hierarchy_Table Child");
							report.updateTestLog("Verify Hierarchy_Table Child", "Hierarchy_Table Child Count: " + iChild_Count, Status.DONE);
							//Verify Connect
							int iConnectCount = driver.findElements(objMyAccounts.byHierarchy_Table_Connect).size();
							if (iConnectCount > 0)
							{
								report.updateTestLog("Verify Hierarchy_Table Connect", "Hierarchy_Table Connect is Present", Status.DONE);
							}
							else
							{
								report.updateTestLog("Verify Hierarchy_Table Connect", "Hierarchy_Table Connect is not Present", Status.FAIL);
							}
						}
						else
						{
							report.updateTestLog("Verify Hierarchy_Table Child", "No Accounts mapped as Child", Status.DONE);
						}
						if (!strPrimary_Parent.isEmpty())
						{
							//Verify Parent
							cf.verifyPageElement(objMyAccounts.byHierarchy_Table_HCP_PrimaryParent, "HCA - Parent", "text", strPrimary_Parent, "Exact", false);
						}
					}
					cf.clickLink(objMyAccounts.byHierarchy_BackToAccount, "byHierarchy_BackToAccount");
					if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
					{
						report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.DONE);
					}
					else
					{
						//report.updateTestLog("Verify app is back to Account Details Page", "Account Details not Opened for Account: " + strAccount_Name, Status.FAIL);
						throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
					}
				}
				else
				{
					throw new FrameworkException("Hierarchy Table not Opened for Account: " + strAccount_Name);
				}
			} //bFrameSwitch	
		}
		else
		{
			report.updateTestLog("Verify View Hierarchy", "View Hierarchy Button not Present", Status.FAIL);
		}
	}

	public void verifyViewOverview() throws Exception
	{
		cf.scrollToTop();
		By byViewOverview = cf.getSFElementXPath("View Overview", "pbButton");
		if (cf.isElementVisible(byViewOverview, "View Overview", 5))
		{
			//			String strAccount_Category = dataTable.getData("MyAccounts", "Account_Category");
			//			By byAccount_Name = cf.getSFElementXPath("Name", "text");
			String strAccount_Name = cf.getData(objMyAccounts.accountDetails_TopName, "Account_Name", "Text");
			String strUserName = cf.getData(objMyAccounts.login_UserNavLabel, "User Name", "Text");
			cf.clickSFButton("View Overview", "pbButton");
			Boolean bFrameSwitch = cf.switchToFrame(objMyAccounts.frame_VOD, "frame_VOD");
			if (bFrameSwitch)
			{
				By byPageLoad_Spinner = By.xpath("//div[@class='spinner']/parent:://span[@us-spinner][@class='ng-scope ng-hide']");
				cf.isElementPresent(byPageLoad_Spinner, "Page Load Spinner");
				By bylistViewportWrapper = By.xpath("//div[@class='listViewportWrapper']");
				By bytimelineLegend = By.xpath("//div[@class='timelineLegend']");
				if (cf.isElementVisible(bylistViewportWrapper, "Overview"))
				{
					report.updateTestLog("Verify Account Details Overview", "Account Details Overview Opened for Account: " + strAccount_Name, Status.PASS);
					cf.waitForSeconds(7);
					//Verify Current Month
					By byCurrentMonth = By.xpath("//div[@class='monthHeader currentMonth']/span[@class='ng-binding']");
					String strCurrentMonth = cf.getData(byCurrentMonth, "Current Month", "text");
					report.updateTestLog("Verify Account Details Overview", "Account Details - Current Month " + strCurrentMonth, Status.DONE);
					//					cf.verifyPageElement(byCurrentMonth, "Current Month", "text", strExpectedCurrentMonth, "Exact", false);

					//Uncheck all key events and verify No Activities present////////////////////////////////////////////////////////////////////////////
					if (cf.isElementVisible(bytimelineLegend, "Show Timeline section"))
					{
						//////Filter with Show only my activities and verify it
						cf.clickByJSE(objMyAccounts.filterByOwnerCheckbox, "filterByOwnerCheckbox");

						By byMonths = By.xpath("//div[@class='timeline']/div");
						int iMonthCount = driver.findElements(byMonths).size();
						for (int i = 2; i <= iMonthCount; i++)
						{
							String strXPath = "//div[@class='timeline']/div[" + i + "]";
							cf.scrollToElement(By.xpath(strXPath), (i - 1) + "Month");
							if (verifyMonthEmpty(strXPath))
							{
								continue;
							}

							By byMonth_Expand_Status = By.xpath(strXPath + "/div[contains(@class,'monthTimeline')]");
							By byMonth_Id = By.xpath(strXPath);
							String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
							report.updateTestLog("verifyViewOverview", strMonth_Id + " - Activity Items", Status.SCREENSHOT);
							String strmonthTimeline = cf.getData(byMonth_Expand_Status, "Month_Collapse_Status", "class");
							if (!strmonthTimeline.equalsIgnoreCase("monthTimeline"))
							{
								//Expand
								By byMonth_Header = By.xpath(strXPath + "/div[contains(@class,'monthHeader')]/span[@class='arrow']");
								cf.clickButton(byMonth_Header, "Expand Time line Activities for " + strMonth_Id);
							} ///Expand the Month View

							///////Each Day in Month
							By byDays = By.xpath(strXPath + "/div[contains(@class,'monthTimeline')]/div");
							int iDaysCount = driver.findElements(byDays).size();
							for (int iDay = 1; iDay <= iDaysCount; iDay++)
							{
								String strDayXPath = strXPath + "/div[contains(@class,'monthTimeline')]/div[" + iDay + "]";
								//By byDay = By.xpath(strDayXPath);

								By byDay_timelineItem = By.xpath(strDayXPath + "/div[contains(@class,'timelineItem')]");

								//By byDay_IsEmpty = By.xpath(strDayXPath + "/div[@class='emptyDay']");

								//int iEmptyDay = driver.findElements(byDay_IsEmpty).size();
								driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
								int iItem_Count = driver.findElements(byDay_timelineItem).size();
								//cf.isElementPresent(byDay_IsEmpty,"Day is Empty");
								if (iItem_Count > 0)
								{/////////Day is not Empty
									By byItem_Divs = By.xpath(strDayXPath + "/div");
									int iItem_DivsCount = driver.findElements(byItem_Divs).size();
									for (int iItem_Div = 1; iItem_Div <= iItem_DivsCount; iItem_Div++)
									{//Each Div in Day
										By byItem_Div = By.xpath(strDayXPath + "/div[" + iItem_Div + "]");
										String strItem_Div = cf.getData(byItem_Div, iItem_Div + "Item_Div", "class");
										if (strItem_Div.contains("timelineItemRight") || strItem_Div.contains("timelineItemLeft"))
										{
											By byOwner = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//span[@ng-show='timelineItem.owner.name' or @ng-show='timelineItem.createdBy.name']");
											String strOwner = cf.getData(byOwner, "Item - Owner", "text");
											strOwner = strOwner.replace("- ", "");
											By byItem_Date = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//div[@class='timelineSubtext ng-binding']");
											String strItem_Date = cf.getData(byItem_Date, "Item - Date", "text");
											By byItem_Name = By.xpath(strDayXPath + "/div[" + iItem_Div + "]" + "//div[@class='timelineActivityName ng-binding']");
											String strItem_Name = cf.getData(byItem_Name, "Item - Name", "text");
											strItem_Name = strItem_Name.replace(" - " + strUserName, "");
											if (strOwner.equals(strUserName))
											{
												//												cf.verifyActualExpectedData("Item Owner", strOwner, strUserName,"Exact",false);
												report.updateTestLog("Verify Owner for Activity Name: " + strItem_Name + " on Date: " + strItem_Date, "Actual Value: (" + strOwner + ") matches with Expected Value: (" + strUserName + ").", Status.DONE);
											}
											else
											{//////Error
												report.updateTestLog("Verify Owner for Activity Name: " + strItem_Name + " on Date: " + strItem_Date, "Actual Value: (" + strOwner + ") not matches with Expected Value: (" + strUserName + ").", Status.FAIL);
											}
										}
									} //For iItem_DivsCount loop								
								} //Day is not Empty							
							} //for iDay loop
						} //for Month loop

						//////UnFilter with Show only my activities and verify it
						cf.clickByJSE(objMyAccounts.filterByOwnerCheckbox, "filterByOwnerCheckbox");
						//Uncheck all Key Events
						//						cf.scrollToTop();
						//						cf.scrollToTop();
						cf.scrollElementToMiddlePage(objMyAccounts.timeLine_Calls, "Calls");
						//cf.scrollToElement(objMyAccounts.timeLine_Calls, "timeLine_Calls", 400);

						//UnCheck all the Items
						By byTimeLineObjects = By.xpath("//div[@class='timelineLegend']/div");
						int iTimeLineObjectsCount = driver.findElements(byTimeLineObjects).size();
						for (int i = 2; i <= iTimeLineObjectsCount; i++)
						{
							By byTimeLineObj = By.xpath("//div[@class='timelineLegend']/div[" + i + "]//input");
							String strTimeLineObj_Label = cf.getData(By.xpath("//div[@class='timelineLegend']/div[" + i + "]//label//span[@class='labelText ng-binding']"), (i - 1) + "TimeLineObj", "text");
							cf.clickByJSE(byTimeLineObj, strTimeLineObj_Label);
						}
						cf.waitForSeconds(3);
						report.updateTestLog("View Overview", "View Overview - Screenshot", Status.SCREENSHOT);
						for (int i = 2; i <= iMonthCount; i++)
						{
							String strXPath = "//div[@class='timeline']/div[" + i + "]";
							if (!verifyMonthEmpty(strXPath))
							{
								By byMonth_Id = By.xpath(strXPath);
								String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
								report.updateTestLog("Verify Activities displayed for the Month ", " Activities displayed the Month: " + strMonth_Id + " is not Expected.", Status.FAIL);
							}
						}
					}
					else
					{
						throw new FrameworkException("Overview Details - Show Timeline section not found.");
					}
				}
				else
				{
					throw new FrameworkException("Overview Details not Opened for Account: " + strAccount_Name);
				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				cf.clickLink(objMyAccounts.overView_AccountLink, "overView_AccountLink");
				if (cf.isElementVisible(objMyAccounts.accountDetails_TopName, "Account Details" + strAccount_Name))
				{
					report.updateTestLog("Verify app is back to Account Details Page", "Account Details Opened for Account: " + strAccount_Name, Status.PASS);
				}
				else
				{
					//report.updateTestLog("Verify app is back to Account Details Page", "Account Details not Opened for Account: " + strAccount_Name, Status.FAIL);
					throw new FrameworkException("Account Details not Opened for Account: " + strAccount_Name);
				}
			} //bFrameSwitch
		}
		else
		{
			report.updateTestLog("Verify View Overview", "View Overview Button not Present", Status.FAIL);
		}
		//		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public Boolean verifyMonthEmpty(String strMonthXPath) throws Exception
	{
		Boolean bMonthEmpty = false;
		By byMonthSummary_Class = By.xpath(strMonthXPath + "/div[contains(@class,'monthSummary')]");
		By byMonth_Id = By.xpath(strMonthXPath);
		String strMonth_Id = cf.getData(byMonth_Id, "Month_Id", "id");
		String strMonth_Summary = cf.getData(byMonthSummary_Class, "byMonthSummary_Class", "class");
		if (!strMonth_Summary.equalsIgnoreCase("monthSummary"))
		{
			//Collapse
			By byMonth_Collapse = By.xpath(strMonthXPath + "/div[contains(@class,'monthHeader')]/span[@class='arrow']");
			cf.clickButton(byMonth_Collapse, "Collapse Activities for " + strMonth_Id);
		}
		By byMonth_emptyMonth = By.xpath(strMonthXPath + "/div[contains(@class,'monthSummary')]/div[contains(@class,'emptyMonth')][normalize-space(text())='No Activities']");
		String strMonth_emptyMonth = cf.getData(byMonth_emptyMonth, "Month_Empty Activities Status", "class");
		if (strMonth_emptyMonth.equalsIgnoreCase("emptyMonth"))
		{//No Activities			
			bMonthEmpty = true;
			report.updateTestLog("Verify Activities displayed for the Month: " + strMonth_Id, "No Activities displayed the Month: " + strMonth_Id, Status.DONE);
		}
		return bMonthEmpty;
	}

	public String getAccountRecordType() throws Exception
	{
		String strAccount_Record_Type2 = cf.getSFData("Account Record Type", "text", "text");
		String strAccount_Record_Type = null;
		strAccount_Record_Type2 = strAccount_Record_Type2.replace(" [Change]", "");
		switch (strAccount_Record_Type2)
		{
			case "Hospital" :
			case "Hospital Department" :
			case "Laboratory" :
			case "MCO" :
			case "Organization" :
			case "Pharmacy" :
			case "Virtual" :
				strAccount_Record_Type = "Hospital";
				break;
			case "Consumer/Other" :
			case "GP" :
			case "Health Care Provider" :
			case "Internal" :
			case "Non Prescriber" :
			case "Non-Prescriber" :
			case "Professional" :
				strAccount_Record_Type = "Professional";
				break;
			default :
				report.updateTestLog("getAccountRecordType", "Account Record Type: " + strAccount_Record_Type2 + " which is Unknown", Status.FAIL);
		}
		return strAccount_Record_Type;
	}

	public void testMethod12345()
	{
		SeleniumTestParameters t = driver.getTestParameters();
		String strBrowserName = t.getBrowser().toString();
		String strTCName = t.getCurrentTestcase();

		tv.addVariable("browserName", strBrowserName);
		tv.addVariable("tcName", strTCName);
		tv.addVariable("test2", cf.getRandomNumber(4));
		tv.addVariable("test3", false);
		tv.addVariable("test4", "Test test");

	}

	public void testMethod123()
	{
		String dTest = (String) tv.getVariable("test2");
		System.out.println("Test2 value: " + dTest);

		Boolean bTest = (Boolean) tv.getVariable("test3");
		System.out.println("Test3 value: " + bTest);
		String strTest = (String) tv.getVariable("test4");
		System.out.println("Test4 value: " + strTest);

		String strBrowserName = (String) tv.getVariable("browserName");
		System.out.println("Browser Name: " + strBrowserName);

		String strtcName = (String) tv.getVariable("tcName");
		System.out.println("TC Name: " + strtcName);

	}

}//End of Class
