package businesscomponents;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.ObjVeevaEmail;
import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class HeaderPage extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);

	public HeaderPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	/**
	 * This method will help with launching and logging into Veeva application
	 * 
	 * @throws Exception
	 */
	public void launchAndLoginToVeeva() throws Exception
	{
		openVeevaApp();
		loginToVeevaApp();
	}

	/**
	 * This method will launch the Veeva application
	 * 
	 * @throws Exception
	 */
	public void openVeevaApp() throws Exception
	{
		String strSalesForceUrl = properties.getProperty("VeevaAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		driver.get(strSalesForceUrl);
		if (cf.isElementVisible(ObjVeevaEmail.loginUserName, "login - User Name"))
		{
			report.updateTestLog("Verify Veeva Login page is opened", "Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is opened", Status.PASS);
		}
		else
		{
			throw new FrameworkException("Veeva Url: <b>" + strSalesForceUrl + "<b/>, Login page is not opened");
		}
	}

	/**
	 * This method will enter login credentials and login based on username and
	 * password from CommonTestData
	 * 
	 * @throws Exception
	 */
	public void loginToVeevaApp() throws Exception
	{
		String strDataSheet = "Login";
		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strPassword = dataTable.getData(strDataSheet, "SF_Password");
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strPassword);
		report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
		cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
		if (cf.isElementVisible(By.cssSelector(".continue"), "Continue"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.className("continue"), "Continue Maintenance Alert");
		}
		if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		else if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}
		else if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
		{
			cf.waitForSeconds(2); // screenshot is taken without page load
			report.updateTestLog("Verify login is successful", "Login is successful", Status.PASS);
		}
		else
		{
			if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
			{
				report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
				cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
				if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
				{
					cf.waitForSeconds(2); // screenshot is taken without page
											// load
					report.updateTestLog("Verify login is successful", "Login is successful", Status.PASS);
				}
			}

		}

		//cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0] + "com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			driver.get(currentUrl);
		}
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "User is not logged in", Status.FAIL);

		//languageSetupVerification();
	}

	public void loginAsSystemAdmin() throws Exception
	{
		String strLocale = dataTable.getData("Login", "Locale");
		String strCommonDataHeader = "#VeevaLogin_" + (strLocale.trim().toUpperCase()) + "_SysAdmin";// VeevaLogin_IE_SysAdmin
		String strSysAdminUserName = dataTable.getCommonData("SF_UserName", strCommonDataHeader);
		String strSysAdminPassword = dataTable.getCommonData("SF_Password", strCommonDataHeader);
		cf.setData(ObjVeevaEmail.loginUserName, "User Name", strSysAdminUserName);
		cf.setData(ObjVeevaEmail.loginPassword, "Password", strSysAdminPassword);
		cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
		report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
		if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
		}
		if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
		{
			report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
			cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
		}

		cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println("Current URL: "+currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			System.out.println("Changed URL: "+currentUrl);
			driver.get(currentUrl);
		}
		
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
		languageSetupVerification();
	}

	/**
	 * This method is used to search for any user and login (this method is used
	 * for login without entering password)
	 * 
	 * @throws Exception
	 */
	public void searchUserAndLoginFromSysAdmin() throws Exception
	{
		cf.clickButton(By.cssSelector("a[title*='Home Tab']"), "Home");
		String strSideBarCollapseStatus = cf.getData(By.cssSelector("#sidebarCell"), "Side Bar link", "class");
		if (strSideBarCollapseStatus.toLowerCase().contains("collapsed"))
			cf.clickButton(By.cssSelector("#sidebarCell"), "Sidebar");

		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strLocale = dataTable.getData("Login", "Locale");

		if (!strLocale.equals("CN"))
			cf.selectData(objHeaderPage.selectSearch, "Search Users", "Users");

		cf.setData(objHeaderPage.textSearch, "Username", strUserName);
		cf.clickButton(objHeaderPage.btnGo, "Go");
		if (cf.isElementVisible(By.xpath("//h1[contains(text(),'User')]"), "User Page"))
		{
			cf.clickSFButton("Login", "button");
			cf.waitForSeconds(5);
			String currentUrl = driver.getCurrentUrl();
			System.out.println("Current URL: "+currentUrl);
			if (currentUrl.contains("lightning"))
			{
				String[] splitUrl = currentUrl.split("com");
				currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
				System.out.println("Changed URL: "+currentUrl);
				driver.get(currentUrl);
			}
			
			cf.waitForSeconds(5);
			if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
				report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
			else
				report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
			languageSetupVerification();
			
			if (cf.isElementVisible(By.xpath("//span[contains(text(),'Logged in as')]"), "Successful login"))
			{
				report.updateTestLog("Verify Successful Login", "User '" + strUserName + "' is successfully logged in without password", Status.PASS);
			}
			else
				report.updateTestLog("Verify Successful Login", "User '" + strUserName + "' is not logged in from System Admin page", Status.FAIL);
		}
		else
			report.updateTestLog("Verify User Page", "Search user landing page is not displayed", Status.FAIL);
		
		cf.waitForSeconds(5);
		String currentUrl = driver.getCurrentUrl();
		System.out.println("Current URL: "+currentUrl);
		if (currentUrl.contains("lightning"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0]+"com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fpage%2Fhome";
			System.out.println("Changed URL: "+currentUrl);
			driver.get(currentUrl);
		}
		
		cf.waitForSeconds(5);
		if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
			report.updateTestLog("Verify Successful Login", "User is successfully logged in as System Admin", Status.PASS);
		else
			report.updateTestLog("Verify Successful Login", "System Admin User is not logged in", Status.FAIL);
	}

	/**
	 * This method is used to logout and land in system admin page
	 * 
	 * @throws Exception
	 */
	public void logOutToSysAdmin() throws Exception
	{
		cf.switchToParentFrame();
		if (cf.isElementVisible(ObjVeevaEmail.logOutUser, "Logout"))
		{
			driver.findElement(ObjVeevaEmail.logOutUser).click();
			cf.waitForSeconds(3);
			cf.clickByJSE(ObjVeevaEmail.logOutLink, "Logout");
			cf.waitForSeconds(3);
		}
	}

	/**
	 * This method will be used for changing password from common test data
	 * excel sheet using System Admin
	 * 
	 * @throws Exception
	 */
	public void verifyAndChangeInvalidPassword() throws Exception
	{
		try
		{
			cf.waitForSeconds(15);
			// */
			if (cf.isElementVisible(By.cssSelector("[name='login'] div.loginError"), "Invalid login error"))
			{
				String strValueError = cf.getData(By.cssSelector("[name='login'] div.loginError"), "InVALID login", "text");
				System.out.println(strValueError);
				if (!strValueError.contains("has been disabled by your System Administrator"))
				{
					// */
					boolean successfulPwdChange = false;
					String strDataSheet = "Login";
					String strUserName = dataTable.getData(strDataSheet, "SF_UserName");
					// String strPassword = dataTable.getData(strDataSheet,
					// "SF_Password");
					String strLocale = dataTable.getData(strDataSheet, "Locale");
					String strCommonDataHeader = "#VeevaLogin_" + (strLocale.trim().toUpperCase()) + "_SysAdmin";// VeevaLogin_IE_SysAdmin
					String strSysAdminUserName = dataTable.getCommonData("SF_UserName", strCommonDataHeader);
					String strSysAdminPassword = dataTable.getCommonData("SF_Password", strCommonDataHeader);
					cf.setData(ObjVeevaEmail.loginUserName, "User Name", strSysAdminUserName);
					cf.setData(ObjVeevaEmail.loginPassword, "Password", strSysAdminPassword);
					cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");
					report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
					if (cf.isElementVisible(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel", 5))
					{
						report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
						cf.clickButton(ObjVeevaEmail.passwordResetCancel, "PasswordResetCancel");
					}
					if (cf.isElementVisible(By.xpath("//*[@id='box']"), "Maintenance Alert Page"))
					{
						report.updateTestLog("Scheduled Maintenace", "Maintenance Alert is displayed", Status.WARNING);
						cf.clickButton(By.xpath("//*[@id='box']//a"), "Continue in maintenance alert");
					}
					if (cf.isElementVisible(ObjVeevaEmail.menu, "Veeva - User Home Page"))
					{
						String parentHandle = driver.getWindowHandle();
						cf.clickButton(objHeaderPage.linkUserName, "Username");
						cf.clickButton(objHeaderPage.linkDeveloperConsole, "Developer Console");
						cf.waitForSeconds(3);
						cf.switchToPage("Developer Console");
						driver.manage().window().maximize();
						report.updateTestLog("Developer Console", "Opened developer Console successfully", Status.SCREENSHOT);
						Robot robot = new Robot(); // Robot class throws AWT
													// Exception
													// Actions builder = new Actions(driver.getWebDriver());
						synchronized (robot)
						{
							try
							{
								for (int i = 3; i >= 0; i--)
								{
									cf.clickButton(By.cssSelector("#debugMenuEntry-btnEl"), "Debug Menu");
									cf.clickButton(By.cssSelector("#openExecuteAnonymousWindow-textEl > div"), "Execute Anonymous window");
									// WebElement
									// editorElement=driver.findElement(By.cssSelector("[class=CodeMirror-sizer]"));
									String strQuery = "User usr1 = [select Id from User where username='" + strUserName + "']; System.setPassword(usr1.Id,'January2" + i + "19');";
									StringSelection stringSelection = new StringSelection(strQuery);
									Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
									clipboard.setContents(stringSelection, null);
									// int
									// x_StartPos=editorElement.getLocation().getX();
									// int
									// y_StartPos=editorElement.getLocation().getY();
									// int x_offset=(int)
									// (editorElement.getRect().width*0.5);
									// int
									// y_offset=(int)(editorElement.getRect().height*0.5);
									// int x_movePosition=x_StartPos+x_offset;
									// int y_movePosition=y_StartPos+y_offset;
									driver.manage().window().fullscreen();
									Thread.sleep(2000); // Thread.sleep throws
														// InterruptedException
														/*
														 * / Action mouseMoveAction =
														 * builder.moveToElement(editorElement).
														 * pause(1500).moveByOffset(x_offset,
														 * y_offset).build();
														 * mouseMoveAction.perform(); Action
														 * keyControlA =
														 * builder.keyDown(Keys.CONTROL).sendKeys(
														 * "A").keyUp(Keys.CONTROL).build();
														 * keyControlA.perform(); Action
														 * keyBackSpace =
														 * builder.keyDown(Keys.BACK_SPACE).keyUp(
														 * Keys.BACK_SPACE).build();
														 * keyBackSpace.perform();
														 * cf.waitForSeconds(2); Action keyControlV
														 * = builder.keyDown(Keys.CONTROL).sendKeys(
														 * "V").keyUp(Keys.CONTROL).build();
														 * keyControlV.perform(); //
														 */
														/*
														 * / robot.mouseMove(x_movePosition,
														 * y_movePosition); // move mouse point to
														 * specific location robot.delay(1500);
														 * robot.keyPress(KeyEvent.VK_CONTROL); //
														 * press arrow down key of keyboard to
														 * navigate and select Save radio button
														 * robot.keyPress(KeyEvent.VK_A); // press
														 * arrow down key of keyboard to navigate
														 * and select Save radio button // CTRL+Z is
														 * now pressed (receiving application should
														 * see a "key down" event.)
														 * robot.keyRelease(KeyEvent.VK_A);
														 * robot.keyRelease(KeyEvent.VK_CONTROL); //
														 * CTRL+Z is now released (receiving
														 * application should now see a "key up"
														 * event - as well as a "key pressed" event)
														 * robot.keyPress(KeyEvent.VK_BACK_SPACE);
														 * robot.keyRelease(KeyEvent.VK_BACK_SPACE);
														 * robot.keyPress(KeyEvent.VK_CONTROL);
														 * robot.keyPress(KeyEvent.VK_V);
														 * robot.keyRelease(KeyEvent.VK_V);
														 * robot.keyRelease(KeyEvent.VK_CONTROL);
														 * cf.waitForSeconds(3); //
														 */
									report.updateTestLog("Developer Console", "Query entered to Developer Console: " + strQuery, Status.SCREENSHOT);
									driver.findElement(By.xpath("//span[.='Execute']/parent::button")).click();
									cf.waitForSeconds(3);
									report.updateTestLog("Developer Console", "Query executed in Developer Console", Status.SCREENSHOT);
									successfulPwdChange = true;
									cf.waitForSeconds(2);
								} // for loop
								driver.close();
								driver.switchTo().window(parentHandle);
							}
							catch (Exception e)
							{
								cf.switchToParentFrame();
								report.updateTestLog("Developer Console Error", "Error occurred in Developer Console entry", Status.FAIL);
								successfulPwdChange = false;
							}
						} // synchronized block
						veevaEmail vemail = new veevaEmail(scriptHelper);
						vemail.logOut();
						if (successfulPwdChange)
						{
							// String
							// strPutDataField="#VeevaLogin_"+strLocale+"_Rep";//VeevaLogin_IE_Rep
							dataTable.putCommonFieldData("Login", "SF_Password", "January2019");
							loginToVeevaApp();
							System.out.println("Password changed as January2019 for user : " + strUserName);
						}
					}
					else
						throw new FrameworkException("Login is unsuccesful in invalid password");
					// */
				}
				else
					throw new FrameworkException("Login is unsuccesful in invalid password");
			} // if password invalid page exists
			else
			{
				report.updateTestLog("Invalid password page verification", "Invalid password page is not displayed. Refer to screenshot", Status.DONE);
			}
			// */
		}
		catch (Exception e)
		{
			throw new FrameworkException(e.getMessage());
		}
	}

	public void navigateToGASLink()
	{
		try
		{
			cf.clickLink(objHeaderPage.plussymbol, "Plus Symbol");
			cf.clickLink(objHeaderPage.lnkGlobalAccountSearch, "Global Account Search Link");
			if (cf.isElementVisible(objHeaderPage.lblSearchHeader, "Search for Account Header"))
				report.updateTestLog("Verify able to navigate to Search for Account page", "Navigating to Global account search page is successful", Status.PASS);
			else
			{
				report.updateTestLog("Verify able to navigate to Search for Account page", "Unable to navigate to Surveys Home page", Status.FAIL);
			}
		}
		catch (Exception e)
		{
			throw new FrameworkException(e.getMessage());
		}
	}

	public void languageSetupVerification() throws Exception
	{
		String strHelpAndTraining = cf.getData(objHeaderPage.linkHelpAndTraining, "Help and Training", "text");
		String strLocale = dataTable.getData("Login", "Locale");
		if (!strHelpAndTraining.trim().equalsIgnoreCase("Help & Training"))
		{
			switch (strLocale.trim().toUpperCase())
			{
				case "RU" :
				case "BR" :
				case "JP" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.linkPersonalSetup, "Personal Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfo, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.linkPersonalInformation, "Personal Information Side Bar");
					cf.clickButton(objHeaderPage.linkLanguageAndTimeZone, "Language and Time Zone");
					cf.selectData(objHeaderPage.selectLocaleSetup, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.btnSaveInPersonalLanguageTimeZone, "Language and Time Zone");
					break;
				case "CN" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.setupCN, "Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfoExpand, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.linkPersonalInformation, "Personal Information");
					cf.clickButton(objHeaderPage.btnEditPersonalInformation, "Edit Personal Information");
					cf.selectData(objHeaderPage.selectLanguageInChina, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.btnSaveInPersonalLanguageTimeZone, "Language and Time Zone");
					break;
				// Case EU2 added by Murali on 07/02/2019
				case "EU2" :
					cf.clickButton(objHeaderPage.linkUserName, "Username");
					cf.clickButton(objHeaderPage.linkPersonalSetup, "Personal Setup");
					cf.clickButton(objHeaderPage.sideNavPersonalInfo, "Personal Information Side Bar Header");
					cf.clickButton(objHeaderPage.advanceUserDetails, "AdvanceUserDetails Link");
					cf.clickButton(objHeaderPage.editbutton, "Editbutton Link");
					cf.selectData(objHeaderPage.selectLanguageInChina, "Language and Locale Setup", "English");
					cf.clickButton(objHeaderPage.saveButton, "saveButton Link");
					break;
			}
			report.updateTestLog("Verify Language Setup", "Language setup is changed to English", Status.PASS);
		}
		else
			report.updateTestLog("Verify Language Setup", "Language setup is English", Status.SCREENSHOT);
	}

	public void passwordReset() throws Exception
	{
		cf.clickElement(By.xpath("//span[@id='userNavLabel']"), "");
		cf.clickElement(By.xpath("//a[@title='Developer Console (New Window)']"), "");
		cf.SwitchWindow("child");
		cf.clickElement(By.xpath("//span[@id='debugMenuEntry-btnInnerEl']"), "");
		cf.clickElement(By.xpath("//div[text()='Open Execute Anonymous Window']"), "");
		if (cf.isElementVisible(By.xpath("//span[text()='Enter Apex Code']"), ""))
		{
			cf.clickElement(By.xpath("//img[@data-qtip='Open in new window']"), "Open in new window");
			cf.switchtoNewTab(2);
			driver.manage().window().maximize();
			String text = "User usr5 = [select Id from User where username='kwrq926@astrazeneca.net.rufull']; System.setPassword(usr5.Id,'Veeva2019');";
			cf.clickElement(By.xpath("//div[@class='CodeMirror-code']//div/pre"), "");
			driver.findElement(By.xpath("//div[@class='CodeMirror-code']//div/pre")).clear();
			driver.findElement(By.xpath("//div[@class='CodeMirror-code']//div/pre")).sendKeys(Keys.chord(Keys.CONTROL, "a"), text);
			cf.clickElement(By.xpath("//span[text()='Execute']"), "");
			Robot robot = new Robot();
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			// Set the String to Enter
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		}
	}

	public void searchUserID() throws Exception
	{
		cf.clickButton(By.cssSelector("a[title*='Home Tab']"), "Home");
		String strSideBarCollapseStatus = cf.getData(By.cssSelector("#sidebarCell"), "Side Bar link", "class");
		if (strSideBarCollapseStatus.toLowerCase().contains("collapsed"))
			cf.clickButton(By.cssSelector("#sidebarCell"), "Sidebar");
		if (cf.isElementPresent(By.id("sen")))
		{
			Select dropdown = new Select(driver.findElement(By.id("sen")));
			dropdown.selectByVisibleText("Users");
			System.out.println("Users selected from Search");
		}
		String UserName = dataTable.getData("Login", "UserName");
		System.out.println(UserName);
		int user = UserName.split(";").length;
		for (int i = 0; i <= user - 1; i++)
		{
			UserName = dataTable.getData("Login", "UserName");
			if (UserName.contains(";"))
			{
				String[] acc = UserName.split(";");
				UserName = acc[i];
				System.out.println("Spilt using (;) User Name: " + UserName);
			}

			if (i >= 1)
			{
				openVeevaApp();
				loginAsSystemAdmin();
			}

			cf.setData(objHeaderPage.textSearch, "Username", UserName);
			cf.clickButton(objHeaderPage.btnGo, "Go");
			String title = driver.getCurrentUrl();
			String[] title1 = title.split("/");
			String userid = title1[3];
			userid = userid.substring(0, 15);
			System.out.println(userid);
			dataTable.putData("Login", "UserID", userid);
			report.updateTestLog("Verify userid", "Userid is" + userid, Status.SCREENSHOT);
			passwordResetWorkbench();

		}

	}

	public void unDelete() throws Exception
	{
		String strWorkbenchAppUrl = properties.getProperty("WorkbenchAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strWorkbenchAppUrl);
		if (cf.isElementPresent(By.xpath("//label[text()='Environment:']"), "Workbench - Home Page"))
		{
			Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='oauth_env']")));
			dropdown.selectByVisibleText("Sandbox");
			System.out.println("Sandbox selected from Environment");
			cf.clickElement(By.xpath("(//input[@type='checkbox'])[2]"), "Agree Button");
			cf.clickElement(By.xpath("//input[@type='submit']"), "Login with salesforce");
		}
		if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
		{
			cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
		}
	}

	public void passwordResetWorkbench() throws Exception
	{
		String strWorkbenchAppUrl = properties.getProperty("WorkbenchAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strWorkbenchAppUrl);
		report.updateTestLog("Verify " + strWorkbenchAppUrl + "is launch", strWorkbenchAppUrl + " is launched", Status.SCREENSHOT);
		if (cf.isElementPresent(By.xpath("//label[text()='Environment:']"), "Workbench - Home Page"))
		{
			Select dropdown = new Select(driver.findElement(By.xpath("//select[@id='oauth_env']")));
			dropdown.selectByVisibleText("Sandbox");
			System.out.println("Sandbox selected from Environment");
			cf.clickElement(By.xpath("(//input[@type='checkbox'])[2]"), "Agree Button");
			cf.clickElement(By.xpath("//input[@type='submit']"), "Login with salesforce");
		}

		if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
		{
			cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
		}
		else
		{
			String url = driver.getTitle();
			if (url.equals("Login | Salesforce"))
			{
				String strDataSheet = "Login";
				String strUserName = dataTable.getData(strDataSheet, "SF_UserName");
				String strPassword = dataTable.getData(strDataSheet, "SF_Password");
				cf.setData(ObjVeevaEmail.loginUserName, "User Name", strUserName);
				cf.setData(ObjVeevaEmail.loginPassword, "Password", strPassword);
				report.updateTestLog("login To Veeva App", "Login Page", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaEmail.loginSubmit, "Login Submit");

				if (cf.isElementPresent(By.xpath("//h2[text()='Allow Access?']")))
				{
					cf.clickElement(By.xpath("//input[@title='Allow']"), "Allow");
				}
			}
		}
		if (cf.isElementPresent(By.xpath("//span[text()='utilities']")))
		{
			cf.clickElement(By.xpath("//span[text()='utilities']"), "utilities");
			cf.clickElement(By.xpath("//a[text()='Password Management']"), "Password Management");
		}
		else
		{
			report.updateTestLog("Verify utilities is Visible", "Utilities is NOT Visible", Status.FAIL);
		}
		if (cf.isElementPresent(By.xpath("//td[text()='Password Management']")))
		{
			String userid = dataTable.getData("Login", "UserID");
			// Set Password
			cf.clickElement(By.xpath("//input[@type='radio' and @value='set']"), "Radio - Set button");
			cf.setData(By.xpath("//input[@id='userId']"), "", userid);
			cf.setData(By.xpath("//input[@id='passwordOne']"), "Password", "Veeva2020");
			cf.setData(By.xpath("//input[@id='passwordConfirm']"), "Confirm Password", "Veeva2020");
			cf.clickElement(By.xpath("//input[@id='changePasswordAction']"), "Change Password - Button");

			report.updateTestLog("Verify password is change", "Password is changed", Status.SCREENSHOT);

			if (cf.isElementPresent(By.xpath("//p[text()='UNKNOWN_EXCEPTION: invalid repeated password']")))
			{
				report.updateTestLog("Verify Invalid repeated password", "Invalid repeated password", Status.FAIL);
			}
			if (cf.isElementPresent(By.xpath("//p[contains(text(),'Successfully set password for " + userid + "')]")))
			{
				report.updateTestLog("Verify Password Changed", "Password Changed", Status.PASS);
			}
			else
			{
				report.updateTestLog("Verify Password Changed", "Password is NOT Changed", Status.FAIL);
			}
		}
		else
		{
			report.updateTestLog("Verify Password Management is Visible", "Password Management is NOT Visible", Status.FAIL);
		}
	}

	public void openTeliumApp() throws Exception
	{
		String strTeliumUrl = properties.getProperty("TeliumUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		driver.get(strTeliumUrl);
	}

	public void loginToTeliumApp() throws Exception
	{
		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strPassword = dataTable.getData("Login", "SF_Password");
		cf.setData(By.xpath("//input[@id='email']"), "User Name", strUserName);
		cf.setData(By.xpath("//input[@id='password']"), "Password", strPassword);
		report.updateTestLog("Login To Telium App", "Login Page", Status.SCREENSHOT);
		cf.clickButton(By.xpath("(//a[@id='loginBtn'])/span"), "Login Submit");

		cf.waitForSeconds(5);

		WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 60);
		for (int i = 0; i < 30; i++)
		{
			try
			{
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//h2[text()='Client-Side Customer Data Hub']")));
				break;
			}
			catch (Exception e)
			{
				System.out.println("waiting continues");
			}
		}

		cf.clickElement(By.xpath("//div[text()='Sources']"), "Sources");

		cf.SwitchWindow("child");

		cf.waitForSeconds(10);

		cf.clickElement(By.xpath("(//ul[@class='submenu-list-menu'])[6]/preceding-sibling::div/div[2]"), "Server Side Tools");

		cf.clickElement(By.xpath("//li[text()='Visitor Lookup']"), "Visitor Lookup");

		cf.clickElement(By.xpath("//div[text()='Select a Value']"), "Select a Value");

		//cf.setData(By.xpath("//div[text()='Select a Value']"), "Select a Value", "Ultmarc ID");

		//cf.clickElement(By.xpath("//div[@id='react-select-3-option-0-1']/div[1]"), "ULTMARC ID");
		cf.clickElement(By.xpath("//span[text()='ULTMARC ID']"), "ULTMARC ID");

		cf.setData(By.xpath("//input[@placeholder='Search for a Visitor ID']"), "Search for a Visitor ID", "01465563");

		cf.clickElement(By.xpath("//button[text()='Search']"), "Search");

		String text = driver.findElement(By.xpath("//div[@class='main-container visitorLookupMainPanel']/div[2]/div[2]/pre")).getText();

		System.out.println(text);

		if (text.contains("TAG Next Offer"))
			report.updateTestLog("Verify able to see TAG Next Offer field", "Able to see TAG Next Offer field", Status.PASS);
		else
			report.updateTestLog("Verify able to see TAG Next Offer field", "Unable to see TAG Next Offer field", Status.FAIL);

	}

	/*public static void apacheHttpClientGet()
	{
		try
		{
			@SuppressWarnings("deprecation")
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("https://www.google.com/gmail/");
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			int res = response.getStatusLine().getStatusCode();

			System.out.println("Response Code: " + res);
			if (response.getStatusLine().getStatusCode() != 200)
			{
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			InputStream content = response.getEntity().getContent();
			System.out.println(content);

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null)
			{
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (ClientProtocolException e)
		{

			e.printStackTrace();

		}
		catch (IOException e)
		{

			e.printStackTrace();
		}
	}

	public static void apacheHttpClientPost()
	{
		try
		{
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://www.google.com/gmail/");

			StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
			input.setContentType("application/json");

			postRequest.setEntity(input);

			HttpResponse response = httpClient.execute(postRequest);

			int res = response.getStatusLine().getStatusCode();
			System.out.println("Response Code: " + res);

			if (response.getStatusLine().getStatusCode() != 201)
			{
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null)
			{
				System.out.println(output);
			}

			httpClient.getConnectionManager().shutdown();

		}
		catch (MalformedURLException e)
		{

			e.printStackTrace();

		}
		catch (IOException e)
		{

			e.printStackTrace();

		}

	}
*/
	
}
