package businesscomponents;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import supportlibraries.*;

//import com.itextpdf.text.log.SysoCounter;

import ObjectRepository.*;

//import org.openqa.jetty.html.Select;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;

public class DxL_Email  extends VeevaFunctions{

	//private ConvenienceFunctions cf = new ConvenienceFunctions(driver, report, dataTable);
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	private veevaEmail ve = new veevaEmail(scriptHelper);
	private VeevaFunctions vf = new VeevaFunctions(scriptHelper);
	private Maill mf = new Maill(scriptHelper);
	WebDriverWait wait= new WebDriverWait(driver.getWebDriver(), 30);
	public DxL_Email(ScriptHelper scriptHelper) {
		super(scriptHelper);
		

	}
	static String productleft;
	static String productright;
	static String productinEmail;
	static String product;
	static String strPageTitle=null;
	static String mailPage=null;
	static String productsubject;
	static String SentEmailIDexpanded;
	static String SentEmailID;
	Date dNow=new Date();
	static String date;
	public static String templatetype;
	static boolean optInFlag_EU1 = false;

	public void dXLEmailFlow() throws Exception{
		clickAccountsDxL();
		selectViewOptionandAccountDxL();
		selectSecondViewOptionandAccountDxL();
		chooseMoreActionsOptDxL();	
		pageverifyTemplateDxL();	
		verifyEmailAccountDxL();	
		checkMailDxL();
		checkMailBCCDxL();	
		ve.logOut();

		
	}
	
	public void selectSecondViewOptionandAccountDxL() throws Exception {
		Boolean viewselect=false;
		String strDataSheet = "ApprovedEmail";
		String strDataSheet1 = "Login";
		String strViewOption = dataTable.getData(strDataSheet, "SF_ViewOption");
		String location = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		if(cf.isElementVisible(ObjVeevaEmail.viewdropdown,"View Dropdown")){
			cf.selectData(ObjVeevaEmail.viewdropdown, "selecting view option", strViewOption); 
			cf.waitForSeconds(7);
			if(location.equals("JP") && profile.equals("Medical")){
				cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
				cf.waitForSeconds(7);
			}
			Select s=new Select(driver.findElement(By.id("vwid")));
			String value=s.getFirstSelectedOption().getText();
			if(strViewOption.equals(value)){
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is successful", Status.PASS);	
				viewselect=true;
			}
			else {
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is unsuccessful", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		if(viewselect==true){
			Boolean accountselection=true;
			String accountname = null;
			String accountSelected =dataTable.getData(strDataSheet,"SecondAccountSelected");
			List<WebElement> accounts=driver.findElements(By.xpath("//table[@id='vodResultSet']//tr"));
			for(int i=2;i<=accounts.size();i++){
				if(location.equals("JP") && profile.equals("Commercial")){
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[3]/a")).getText();
				}
				else{
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[2]/a")).getText();
					cf.waitForSeconds(1);
				}
				if(accountSelected.equals(accountname)){
					cf.scrollToElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[2]/a"), accountname);
					driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[1]/input[1]")).click();
					accountselection=true;
					break;
				}
				else
					accountselection=false;
			}
			if(accountselection==true)
				report.updateTestLog("Able to select the account", "Selecting account is successful", Status.PASS);
			else{
				report.updateTestLog("Able to select the account", "Selecting account is unsuccessful", Status.FAIL);	
				//frameworkparameters.setStopExecution(true);
			}
		}
	}
	
	
	public void pageverifyTemplateDxL() throws Exception{
		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String profile=dataTable.getData(strDataSheet, "Profile");
		String product = null,stylevalue = null;
		List<WebElement> productDesc;
		cf.waitForSeconds(8);
		if(cf.isElementVisible(By.id("vod_iframe"), "vod_iframe")){
			cf.switchToFrame(By.id("vod_iframe"));
		}

		if(cf.isElementVisibleforToggle(ObjVeevaEmail.addTemplatesButton, "Add Templates")){
			int r=1,s=1;
			Boolean productSelection=false;
			Boolean productVerify=false;
			List<WebElement> products=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr"));
			if(products.size()>0){
				if(!((locale.equals("JP") && profile.equals("Commercial"))||locale.equals("US"))){
					for(r=1;r<=products.size()-1;r++){
					//	String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/span")).getText();
						cf.waitForSeconds(2);
						driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).click();
						if(driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).isSelected())
							productSelection=true;
						else{
							productSelection=false;
							//frameworkparameters.setStopExecution(true);
						}
						if(productSelection==true){
							report.updateTestLog("Able to select product", "Selecting Product option is successful", Status.PASS);
							productleft=driver.findElement(By.xpath("//tbody[@id='filterTableBody']/tr["+r+"]/td[1]")).getText();
							productright=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/span")).getText();
							if(productleft.equals("Common")||productleft.equals("Products")){
								product=productright;
							}
							else{
								product=productright+" "+"|"+" "+productleft;
							}
							System.out.println(product);
							if(locale.equals("US"))
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr//table//td/span"));
							else
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr/td/input"));
							for(int j=1;j<=productDesc.size();j++){
								stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
								if(stylevalue.equals("display: table-row;")){
									String productText=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]/td[4]")).getText();
									if(product.equals(productText))
										productVerify=true;
									else{
										productVerify=false;
										break;
									}
								}
							}
							if(productVerify==true){
								report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is successful", Status.PASS);
								if(locale.equals("US"))
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).click();
								else
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//td/input")).click();	
							}
							else{
								report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is unsuccessful", Status.FAIL);
								//frameworkparameters.setStopExecution(true);
							}
						}
					}
				}
			//Added by Mainak on 24th June 2019 
				else if(((locale.equals("US") && profile.equals("DXL")))){
					for(r=1;r<=products.size();r++){
						int columnproductssize=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr["+r+"]/td")).size();
						for(s=1;s<=columnproductssize;s++){
			//				String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
							cf.waitForSeconds(5);
							
							driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+s+"]/td["+r+"]/input")).click();
							System.out.println("Inside template");
							cf.waitForSeconds(5);
							if(driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+s+"]/td["+r+"]/input")).isSelected())
								productSelection=true;
							else{
								productSelection=false;
								//frameworkparameters.setStopExecution(true);
								
							}
							System.out.println("Selected First checkbox of the template");
							if(productSelection==true){
								report.updateTestLog("Able to select product", "Selecting Product option is successful", Status.PASS);
								productright=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+r+"]/td["+s+"]")).getText();
								
								product=productright;
								System.out.println(product);
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr"));
								System.out.println("Product Descriptions are:"+productDesc);
								for(int j=1;j<=productDesc.size();j++){
									stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
									if(stylevalue.equals("display: table-row;")){
										System.out.println("Style value validation");
										String productText=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]/td[1]")).getText();
										if(product.equals(productText))
											productVerify=true;
										else{
											productVerify=false;
											break;
										}
									}
								}
								//*[@id="addTemplatesButton"]
								if(productVerify==true){
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is successful", Status.PASS);
									//driver.findElement(By.xpath("//tbody[@id='addTemplatesButton']//tr["+r+"]/td["+s+"]/input")).click();
									cf.waitForSeconds(5);
									//By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input"
									
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).click();
								}
								else{
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is unsuccessful", Status.FAIL);
									//frameworkparameters.setStopExecution(true);
								}
								System.out.println("Out of Template");
								cf.waitForSeconds(5);
								//driver.findElement(By.xpath("//*[@id='addTemplatesButton']")).click();
							}
						}
					}
				}
				
				
				else{
					for(r=1;r<=products.size();r++){
						int columnproductssize=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr["+r+"]/td")).size();
						for(s=1;s<=columnproductssize;s++){
			//				String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
							driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).click();
							if(driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).isSelected())
								productSelection=true;
							else{
								productSelection=false;
								//frameworkparameters.setStopExecution(true);
							}
							if(productSelection==true){
								report.updateTestLog("Able to select product", "Selecting Product option is successful", Status.PASS);
								productright=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
								product=productright;
								System.out.println(product);
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr"));
								for(int j=1;j<=productDesc.size();j++){
									stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
									if(stylevalue.equals("display: table-row;")){
										String productText=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]/td[4]")).getText();
										if(product.equals(productText))
											productVerify=true;
										else{
											productVerify=false;
											break;
										}
									}
								}
								if(productVerify==true){
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is successful", Status.PASS);
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).click();
								}
								else{
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is unsuccessful", Status.FAIL);
									//frameworkparameters.setStopExecution(true);
								}
							}
						}
					}
				}


				if(productVerify==true){
					manageEmailAddressDxL();
					selectTemplateDxL();
					changeOptPreferencesDxL();
					sendAndCloseDxL();
				}
			}
			else{
				report.updateTestLog("Verification of Products availability", " No products are available", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}

//only a single template that is auto selected
		else{  
			cf.waitForSeconds(4);
			manageEmailAddressDxL();
			changeOptPreferencesDxL();
			if(cf.isElementVisible(By.xpath("//div[.='Select at least 1 document(s).']"), "Select atleast 1 documents ERRORRRRR")){
				addDocumentsDxL();
			}
			sendAndCloseDxL();
		}
	}
	
	public void clickAccountsDxL() throws Exception {
		if(cf.isElementVisible(ObjVeevaEmail.menu,"Veeva - User Home Page")){
			cf.waitForSeconds(2); 
			vf.clickVeevaTab("My Accounts");
			cf.switchToFrame(ObjVeevaEmail.sframe);
			if(cf.isElementVisible(ObjVeevaEmail.viewdropdown, "View Dropdown"))
				report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is successful", Status.PASS);	
			else {
				report.updateTestLog("Verify able to see view Dropdown", "Clicking on Accounts tab is unsuccessful", Status.FAIL);	
				throw new FrameworkException("Clicking on Accounts link is unsuccessful");
			}
		}
	}
	public void selectViewOptionandAccountDxL() throws Exception {
		Boolean viewselect=false;
		String strDataSheet = "ApprovedEmail";
		String strDataSheet1 = "Login";
		String strViewOption = dataTable.getData(strDataSheet, "SF_ViewOption");
		String location = dataTable.getData(strDataSheet1, "Locale");
		String profile = dataTable.getData(strDataSheet1, "Profile");
		if(cf.isElementVisible(ObjVeevaEmail.viewdropdown,"View Dropdown")){
			cf.selectData(ObjVeevaEmail.viewdropdown, "selecting view option", strViewOption); 
			cf.waitForSeconds(7);
		//added by Mainak on 14th June 2019 for execution of DXL US email script
			if(location.equals("US") && profile.equals("DXL")){
				cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
				cf.waitForSeconds(7);
			}
			/*if(location.equals("JP") && profile.equals("Medical")){
				cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
				cf.waitForSeconds(7);
			}*/
			Select s=new Select(driver.findElement(By.id("vwid")));
			String value=s.getFirstSelectedOption().getText();
			if(strViewOption.equals(value)){
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is successful", Status.PASS);	
				viewselect=true;
			}
			else {
				report.updateTestLog("Verify able to select the value from dropdown", "Selecting value from dropdown is unsuccessful", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		if(viewselect==true){
			Boolean accountselection=true;
			String accountname = null;
			String accountSelected =dataTable.getData(strDataSheet,"AccountSelected");
			List<WebElement> accounts=driver.findElements(By.xpath("//table[@id='vodResultSet']//tr"));
			for(int i=2;i<=accounts.size();i++){
				if(location.equals("JP") && profile.equals("Commercial")){
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[3]/a")).getText();
				}
				else{
					accountname=driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[2]/a")).getText();
					cf.waitForSeconds(1);
				}
				if(accountSelected.equals(accountname)){
					cf.scrollToElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[2]/a"), accountname);
					driver.findElement(By.xpath("//table[@id='vodResultSet']//tr["+i+"]//td[1]/input[1]")).click();
					accountselection=true;
					break;
				}
				else
					accountselection=false;
			}
			if(accountselection==true)
				report.updateTestLog("Able to select the account", "Selecting account is successful", Status.PASS);
			else{
				report.updateTestLog("Able to select the account", "Selecting account is unsuccessful", Status.FAIL);	
				//frameworkparameters.setStopExecution(true);
			}
		}
	}
	
	
	public void chooseMoreActionsOptDxL() throws Exception{

		Boolean option=true;
		String strDataSheet = "ApprovedEmail";
		String moreActionsOpt=dataTable.getData(strDataSheet, "MoreActonsOption");
		if(cf.isElementVisible(ObjVeevaEmail.moreactionsButton,"More options Button")){
			cf.clickButton(ObjVeevaEmail.moreactionsButton, "More Actions");
		}
		else{
			//frameworkparameters.setStopExecution(true);
		}
		cf.waitForSeconds(5);
		List<WebElement> options=driver.findElements(By.xpath("//ul[@role='menu']//li"));
		for(int i=1;i<=options.size();i++){
			String optionname=driver.findElement(By.xpath("//ul[@role='menu']//li["+i+"]//a")).getText();
			if(moreActionsOpt.equals(optionname)){
				option=true;
				driver.findElement(By.xpath("//ul[@role='menu']//li["+i+"]//a")).click();
				cf.waitForSeconds(10);
				break;
			}
			else
				option=false;
		}
		if(option==true)
			report.updateTestLog("Able to click on SendEmail Option", "Clicking SendEmail option is successful", Status.PASS);
		else{
			report.updateTestLog("Able to click on SendEmail Option", "Clicking SendEmail option is unsuccessful", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}	

	public void dateformatDxL(){

		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String profile=dataTable.getData(strDataSheet, "Profile");
		if(locale.equals("EU2")||locale.equals("RU")){
			date=new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
		}
		else if(locale.equals("EU1") && profile.equals("DXL")){
			date=new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
		}
	//added by Mainak on 14/06/2019
		else if(locale.equals("US") && profile.equals("DXL")){
			date=new SimpleDateFormat("dd.MM.YYYY").format(dNow).toString();
		}
		else if(locale.equals("EU1")||locale.equals("BR")||locale.equals("CA")){
			date=new SimpleDateFormat("dd/MM/YYYY").format(dNow).toString();
		}
		else if(locale.equals("ANZ"))
			date=new SimpleDateFormat("d/MM/YYYY").format(dNow).toString();
		else if(locale.equals("JP") && profile.equals("Commercial")){
			date=new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
		}
		else if(locale.equals("JP") && profile.equals("Medical") ){
			date=new SimpleDateFormat("YYYY/MM/dd").format(dNow).toString();
		}
		else if(locale.equals("IC")){
			date=new SimpleDateFormat("M/d/YYYY").format(dNow).toString();
		}
		else if(locale.equals("IE")){
			date=new SimpleDateFormat("d/M/YYYY").format(dNow).toString();

		}
	}
	public void verifyTemplateDxL() throws Exception{
		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String profile=dataTable.getData(strDataSheet, "Profile");
		String product = null,stylevalue = null;
		List<WebElement> productDesc;
		cf.waitForSeconds(8);
		if(cf.isElementVisible(By.id("vod_iframe"), "vod_iframe")){
			cf.switchToFrame(By.id("vod_iframe"));
		}

		if(cf.isElementVisibleforToggle(ObjVeevaEmail.addTemplatesButton, "Add Templates")){
			int r=1,s=1;
			Boolean productSelection=false;
			Boolean productVerify=false;
			List<WebElement> products=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr"));
			if(products.size()>0){
				if(!((locale.equals("JP") && profile.equals("Commercial"))||locale.equals("US"))){
					for(r=1;r<=products.size()-1;r++){
					//	String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/span")).getText();
						cf.waitForSeconds(2);
						driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).click();
						if(driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).isSelected())
							productSelection=true;
						else{
							productSelection=false;
							//frameworkparameters.setStopExecution(true);
						}
						if(productSelection==true){
							report.updateTestLog("Able to select product", "Selecting Product option is successful", Status.PASS);
							productleft=driver.findElement(By.xpath("//tbody[@id='filterTableBody']/tr["+r+"]/td[1]")).getText();
							productright=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/span")).getText();
							if(productleft.equals("Common")||productleft.equals("Products")){
								product=productright;
							}
							else{
								product=productright+" "+"|"+" "+productleft;
							}
							System.out.println(product);
							if(locale.equals("US"))
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr//table//td/span"));
							else
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr/td/input"));
							for(int j=1;j<=productDesc.size();j++){
								stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
								if(stylevalue.equals("display: table-row;")){
									String productText=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]/td[4]")).getText();
									if(product.equals(productText))
										productVerify=true;
									else{
										productVerify=false;
										break;
									}
								}
							}
							if(productVerify==true){
								report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is successful", Status.PASS);
								if(locale.equals("US"))
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//table//td/input")).click();
								else
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]//td/input")).click();	
							}
							else{
								report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is unsuccessful", Status.FAIL);
								//frameworkparameters.setStopExecution(true);
							}
						}
					}
				}
				else{
					for(r=1;r<=products.size();r++){
						int columnproductssize=driver.findElements(By.xpath("//tbody[@id='filterTableBody']//table//tr["+r+"]/td")).size();
						for(s=1;s<=columnproductssize;s++){
			//				String productoption=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
							driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).click();
							if(driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).isSelected())
								productSelection=true;
							else{
								productSelection=false;
								//frameworkparameters.setStopExecution(true);
							}
							if(productSelection==true){
								report.updateTestLog("Able to select product", "Selecting Product option is successful", Status.PASS);
								productright=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/span")).getText();
								product=productright;
								System.out.println(product);
								productDesc=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr"));
								for(int j=1;j<=productDesc.size();j++){
									stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
									if(stylevalue.equals("display: table-row;")){
										String productText=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]/td[4]")).getText();
										if(product.equals(productText))
											productVerify=true;
										else{
											productVerify=false;
											break;
										}
									}
								}
								if(productVerify==true){
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is successful", Status.PASS);
								cf.waitForSeconds(4);	
									driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr["+r+"]/td["+s+"]/input")).click();
								}
								else{
									report.updateTestLog("Verification of Product Name in Template Table", " Product Name "  +product+  " Verification in Template Table is unsuccessful", Status.FAIL);
									//frameworkparameters.setStopExecution(true);
								}
							}
						}
					}
				}


				if(productVerify==true){
					manageEmailAddressDxL();
					selectTemplateDxL();
					changeOptPreferencesDxL();
					sendAndCloseDxL();
				}
			}
			else{
				report.updateTestLog("Verification of Products availability", " No products are available", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}

//only a single template that is auto selected
		else{  
			cf.waitForSeconds(4);
			manageEmailAddressDxL();
			changeOptPreferencesDxL();
			if(cf.isElementVisible(By.xpath("//div[.='Select at least 1 document(s).']"), "Select atleast 1 documents ERRORRRRR")){
				addDocumentsDxL();
			}
			sendAndCloseDxL();
		}
	}

	public void manageEmailAddressDxL() throws Exception{

		String strDataSheet="Login";
		String locale=dataTable.getData(strDataSheet, "Locale");
		String profile=dataTable.getData(strDataSheet, "Profile");
		if(!(locale.equals("JP") && profile.equals("Commercial"))){

			if(cf.isElementVisible(ObjVeevaEmail.manageEmailAddress, "Manage Email Address Dropdown")){
				cf.selectData(ObjVeevaEmail.manageEmailAddress, "Manage Email Address", "<Manage Email Addresses>");
				driver.findElement(By.xpath("//td[.='Secondary Email']//following-sibling::td/input")).clear();
				cf.setData(ObjVeevaEmail.manageSecondaryEmail, "Manage Secondary Email", ObjVeevaEmail.mailaddress);
				cf.clickButton(ObjVeevaEmail.manageEmailSave, "Save");
				cf.selectData(ObjVeevaEmail.manageEmailAddress, "Manage Email Address",ObjVeevaEmail.mailaddress);
			} 
		}

		else /*if(locale.equals("JP"))
				cf.switchToFrame(By.xpath("//*[@id='vod_iframe']"));*/
					if(cf.isElementVisible(ObjVeevaEmail.manageEmailAddress, "Manage Email Address Dropdown")){
					cf.selectData(ObjVeevaEmail.manageEmailAddress, "Manage Email Address", "<Manage Email Addresses>");
					driver.findElement(By.xpath("//td[.='AZ Doctor Email Address2']//following-sibling::td/input")).clear();
					cf.setData(By.xpath("//td[.='AZ Doctor Email Address2']//following-sibling::td/input"), "Manage Secondary Email", ObjVeevaEmail.mailaddress);
					cf.clickButton(ObjVeevaEmail.manageEmailSave, "Save");
					cf.selectData(ObjVeevaEmail.manageEmailAddress, "Manage Email Address",ObjVeevaEmail.mailaddress);
				} 
		else{

			report.updateTestLog("Verification of Email Address Dropdown", "Unable to see Manage Email Address Dropdown", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	//add Boolean value for reporting
	public void selectTemplateDxL() throws Exception{
		if(cf.isElementVisible(By.xpath("//tbody[@id='filterTableBody']"),"Products table")){
			driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr[1]//table//td/input")).click();
			productleft=driver.findElement(By.xpath("//tbody[@id='filterTableBody']/tr[1]/td[1]")).getText();
			productright=driver.findElement(By.xpath("//tbody[@id='filterTableBody']//tr[1]//table//td/span")).getText();
			List<WebElement> producttemplates=driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr"));
			int j;
			j=1;
			for(j=1;j<=producttemplates.size();j++){
				String stylevalue=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]")).getAttribute("style");
				if(stylevalue.equals("display: table-row;")){ // without attchment
					if(!(driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]//td[2]/img")).size()>0)){
						WebElement ele=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]//td[1]/input"));
						templatetype="Without Attachment";
						cf.clickElement(ele, "selecting a template without attachment");
						cf.clickButton(ObjVeevaEmail.addTemplatesButton, "Add Templates Button");
						changeOptPreferencesDxL();
						break;
					}
					//template with attachment
					else if(driver.findElements(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]//td[2]/img")).size()>0){
						WebElement ele=driver.findElement(By.xpath("//tbody[@id='templateTableBody']//tr["+j+"]//td[1]/input"));
						cf.clickElement(ele, "selecting a template with attachment");
						templatetype="With Attachment";
						cf.clickButton(ObjVeevaEmail.addTemplatesButton, "Add Templates Button");
						changeOptPreferencesDxL();
						if(cf.isElementVisible(By.xpath("//div[.='Select at least 1 document(s).']"), "Select atleast 1 documents ERRORRRRR")){
							addDocumentsDxL();
						}
						break;
					}
					else{
						report.updateTestLog("Verify able to select template", "Selecting template  is unsuccessful", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
			}
		}
	}
	public void changeOptPreferencesDxL() throws Exception{

		String strDataSheet="Login";
		String location=dataTable.getData(strDataSheet, "Locale");
		String profile=dataTable.getData(strDataSheet, "Profile");
		Boolean finish=false;
		if(cf.isElementVisibleforToggle(ObjVeevaEmail.changeOptPreferences, "Change Opt Preferences")){
			Boolean preferences=false;
			cf.scrollToTop();
			report.updateTestLog("Verify able to see Change Opt Preferences ", "Able to see Change Opt preferences link", Status.SCREENSHOT);
			cf.clickLink(ObjVeevaEmail.changeOptPreferences, "Change Opt Preferences link ");
			cf.waitForSeconds(8);
			if(!location.equals("JP")){
				//if(cf.isElementVisibleforToggle(By.xpath("//td[.='No Product Diabetes']"), "EU1 product toggle")||cf.isElementVisibleforToggle(By.xpath("//span[.='CV_CA']"), "CA product toggle"))
				if(location.equals("EU1")||location.equals("CA"))
				{
					List<WebElement> preferenceProducts=driver.findElements(By.xpath("//div[@class='consent-body consent-edit']/table//table//tbody"));
					System.out.println(preferenceProducts.size());
					for(int i=1;i<=preferenceProducts.size();i++)
					{
						if(cf.isElementVisibleforToggle(By.xpath("//div[@class='consent-body consent-edit']/table//table//tr["+i+"]//span[.='"+productleft+"']"),"Checking "+productleft+" is present")){
							String productrghtsp[]=productright.split("_");
							if(cf.isElementVisibleforToggle(By.xpath("//div[@class='consent-body consent-edit']/table//table//tr["+i+"]//table//tr/td[.='"+productrghtsp[0]+"']"),"Checking "+productrghtsp[0]+" is present" )){

								WebElement ele=driver.findElement(By.xpath("//div[@class='consent-body consent-edit']/table//table//tr["+i+"]//table//tr//table//td//input"));
								WebElement ele1=driver.findElement(By.xpath("//div[@class='consent-body consent-edit']/table//table//tr["+i+"]//table//tr//table//td//label"));
								cf.scrollToElement(By.xpath("//div[@class='consent-body consent-edit']/table//table//tr["+i+"]//table//tr//table//td//label"), "Toggle Element");
								if((ele.getAttribute("checked"))==null){
									cf.clickElement(ele1, "Clicked on Toggle");
									preferences=true;
									break;
								}
							}
						}
					}

				}

				else if(cf.isElementVisibleforToggle(By.xpath("//td[.='"+ObjVeevaEmail.mailaddress+"']"), "Mail address Toogle")){
					WebElement ele=driver.findElement(By.xpath("//td[.='"+ObjVeevaEmail.mailaddress+"']/preceding-sibling::td//input"));
					WebElement ele1=driver.findElement(By.xpath("//td[.='"+ObjVeevaEmail.mailaddress+"']/preceding-sibling::td//label"));
					cf.scrollToElement(By.xpath("//td[.='"+ObjVeevaEmail.mailaddress+"']/preceding-sibling::td//label"), "Toogle Element");
					if((ele.getAttribute("checked"))==null){
						cf.clickElement(ele1, "Clicked on Toogle");
						preferences=true;
					}

				}

				else if(cf.isElementVisibleforToggle(By.xpath("//td[.='"+productright+"']"), "Third type Toogle")){
					WebElement thirdtype=driver.findElement(By.xpath("//td[.='"+productright+"']/preceding-sibling::td//input"));
					WebElement thirdtype1=driver.findElement(By.xpath("//td[.='"+productright+"']/preceding-sibling::td//label"));
					cf.scrollToElement(By.xpath("//td[.='"+ObjVeevaEmail.mailaddress+"']/preceding-sibling::td//label"), "Toogle Element Third type");
					if((thirdtype.getAttribute("checked"))==null){
						cf.clickElement(thirdtype1, "Clicked on Toogle");
						preferences=true;
					}
				}
				if(preferences==true)
					report.updateTestLog("Verification of Changing of preferences", " Changing of  mai preferences is successful", Status.PASS);
				else{
					report.updateTestLog("Verification of Changing of preferences", " Changing of   mail preferences is unsuccessful", Status.FAIL);
					//frameworkparameters.setStopExecution(true);
				}
				cf.clickButton(ObjVeevaEmail.changeOptContinue, "Continue");
			}
			cf.waitForSeconds(8);
			if(location.equals("JP")){
				if(profile.equals("Medical")){
					cf.setData(By.id("ConsentID"), "Consent ID", "12345");
				}
				cf.clickLink(By.id("consent"), "Selecting Consent Checkbox");
				cf.clickLink(By.id("saveButton"), "Save Button");
			}
			else if(location.equals("ANZ"))
			{
				WebElement ele=driver.findElement(By.xpath("//div[@class='consentRequired']//following-sibling::input"));
				cf.clickElement(ele, "Consent Required CheckBOx");
				report.updateTestLog("Verify paper consent checkbox is checked ", "Able to see checkbox checked", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaEmail.papercosentsave, "Clickig on continue Button in paper Cosent");
			}
			else
			{		
				cf.setData(ObjVeevaEmail.paperconsent, "Paper Consent id", "12345");
				WebElement ele=driver.findElement(By.xpath("//div[@class='consentRequired']//following-sibling::input"));
				cf.clickElement(ele, "Consent Required CheckBOx");
				report.updateTestLog("Verify paper consent checkbox is checked ", "Able to see checkbox checked", Status.SCREENSHOT);
				cf.clickButton(ObjVeevaEmail.papercosentsave, "Clickig on continue Button in paper Cosent");
			}
			if(!location.equals("JP")){
				WebDriverWait f=new WebDriverWait(driver.getWebDriver(),30);
				f.until(ExpectedConditions.elementToBeClickable(ObjVeevaEmail.papercosentfinish));
				if(cf.isElementVisible(ObjVeevaEmail.papercosentfinish, "Able to see finish Button")){
					cf.clickButton(ObjVeevaEmail.papercosentfinish, "Saving the preferences by clicking finsh");
					finish=true;
					cf.waitForSeconds(6);
					if(finish == true)
						report.updateTestLog("Verify Preferences have been successfully changed", "Saving of preferences is successful", Status.PASS);
					else{
						report.updateTestLog("Verify Preferences have been successfully changed", "Saving of preferences is unsuccessful", Status.FAIL);
						//frameworkparameters.setStopExecution(true);
					}
				}
			}
			
			if(location.equals("JP"))
				cf.switchToFrame(By.id("vod_iframe"));
			if(cf.isElementVisible(By.xpath("//tbody[@id='filterTableBody']"), "Products Table")){
				selectTemplateDxL();


			}
		}
		
		else if(optInFlag_EU1== false && location.equals("EU1") )
		{
			if(cf.isElementVisible(ObjVeevaEmail.optInConfirmMsg, "Account must click link in confirmation email to verify consent."))
			{
				String optinMsg = driver.findElement(ObjVeevaEmail.optInConfirmMsg).getText();
				System.out.println(optinMsg+" - message is displayed");
				String veevaPage = driver.getWindowHandle();
				//open mailinator to opt-in
				mf.optInConfimration(ObjVeevaEmail.mailaddress,veevaPage);//confirm opt-in
				optInFlag_EU1=true;
				report.updateTestLog("Double-opt- in", optinMsg+" - Unable to send email as the account has not opted in", Status.PASS);
				cf.pageRefresh();
				selectTemplateDxL();
			}
			else{
				System.out.println("Already opt-in");
				report.updateTestLog("Double-opt- in", "Account has opted in", Status.PASS);
			}
		}
	}
	public void addDocumentsDxL() throws Exception {

		Boolean addDocuments=false;

		if(cf.isElementVisible(By.xpath("//div[.='Select at least 1 document(s).']"), "Select atleast 1 documents ERRORRRRR")){

			cf.clickButton(ObjVeevaEmail.addDocuments, "Add Documents Button");
			WebDriverWait wait=new WebDriverWait(driver.getWebDriver(), 15);
			wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaEmail.addSelectedDoc));
			WebElement element=driver.findElement(By.xpath("//tbody[@class='docTableBody']/tr[1]/td[1]/input"));
			cf.clickElement(element, "Click any of the Documents in Add Documents Alert");
			cf.clickButton(ObjVeevaEmail.addSelectedDoc, "Add Selected");
			addDocuments=true;

		}
		else{

			report.updateTestLog("verify able to error Message Select at least 1 document(s)", "Unable to see Error Message Select at least 1 document(s)", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		if(addDocuments==true)
			report.updateTestLog("Verify documents have been added successfully","Adding of documents  is successful", Status.PASS);
		else{
			report.updateTestLog("Verify documents have been added successfully","Adding of documents  is unsuccessful", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}

	public void sendAndCloseDxL() throws Exception{
		String strDataSheet="Login";
		String profile=dataTable.getData(strDataSheet, "Profile");
		String locale=dataTable.getData(strDataSheet, "Locale");
		if(cf.isElementVisibleforToggle(ObjVeevaEmail.previewButton, "Preview Button is Visible")){
			cf.clickButton(ObjVeevaEmail.previewButton, "Preview Button");
			if(cf.isElementVisible(By.id("vod_iframe"), "Frame"))
				cf.switchToFrame(By.id("vod_iframe"), "vod_iframe");
//			if(cf.isElementVisibleforToggle(By.className("emailBody"), "Email Body Frame"))
//				cf.switchToFrame(By.className("emailBody"), "Email Body Frame");		
			if(cf.isElementVisibleforToggle(By.xpath("//td[@class='removeDoc']//img"), "Documents"))
			{
				cf.clickLink(By.xpath("//td[@class='removeDoc']//img"), "Delete Documents");
				report.updateTestLog("Verify able to delete documnets ", "Able to delete the documents", Status.SCREENSHOT);
				driver.switchTo().defaultContent();
				if(cf.isElementVisibleforToggle(ObjVeevaEmail.mailFrame, "Frame"))
					cf.switchToFrame(ObjVeevaEmail.mailFrame);
				addDocumentsDxL();
			}
			else{
				driver.switchTo().defaultContent();
				if(cf.isElementVisible(By.id("vod_iframe"), "Frame"))
					cf.switchToFrame(By.id("vod_iframe"));			
				cf.clickButton(By.xpath("//button[.='Edit']"), "Edit beside Send Now");
			}

		}
		cf.scrollToTop();
		productinEmail=driver.findElement(By.xpath("//table[@class='emailHeaderInfo']//td[.='Product']//following-sibling::td")).getText();
		if(!(locale.equals("JP"))){
			productsubject=driver.findElement(By.xpath("//table[@class='headerTable']//td[.='Subject']/following-sibling::td")).getText();
			report.updateTestLog("Verify able to see the Subject", "Able to see the Subject", Status.SCREENSHOT);
		}
		else{
			productsubject="Email Template "+profile;
			cf.setData(By.xpath("//span[@class='AE_Sub_customText']/input"), "Email Subject", productsubject);
			cf.clickLink(By.xpath("//table[@class='infoTable']"), "Table");
			report.updateTestLog("Verify able to see the Subject", "Able to see the Subject", Status.SCREENSHOT);
		}


		/*Core 28 - BCC field introduced*/
		//Enter BCC veeva.automationBCC@mailinator.com       

		if((cf.isElementPresent(ObjVeevaEmail.bcc, "Bcc field"))&& !(locale.equals("JP")&&(profile.equals("Commercial"))))	
		{       
			cf.setData(ObjVeevaEmail.bcc, "BCC field", ObjVeevaEmail.mailaddressBCC);
			System.out.println(ObjVeevaEmail.mailaddressBCC+" is entered in BCC field");
			report.updateTestLog("BCC field is present", "veeva.automationBCC@mailinator.com is entered in the BCC field", Status.DONE);
		}
		else
		{
			System.out.println("BCC field is not present");
			report.updateTestLog("BCC field", "BCC field is not present", Status.WARNING);
		}

		//Email is being sent - Send Email button is clicked
		cf.clickLink(ObjVeevaEmail.sendButton, "Send Button");
		wait.until(ExpectedConditions.elementToBeClickable(ObjVeevaEmail.successNote));
		if(cf.isElementVisibleforToggle(ObjVeevaEmail.successNote, "Success Note")){
			cf.verifyElementVisible(ObjVeevaEmail.successNote, "Able to see successnote");
			report.updateTestLog("Verify successnote", "Able to see sucessNote", Status.PASS);
		}

		else{
			report.updateTestLog("Verify successnote", "UnAble to see sucessNote", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		cf.clickButton(ObjVeevaEmail.close, "Close Button");
		cf.clickLink(ObjVeevaEmail.accountstab, "My Accounts");
		cf.switchToFrame(ObjVeevaEmail.sframe);
		if(locale.equals("JP") && profile.equals("Medical")){
			cf.selectData(By.id("terrId"), "Selecting all Accounts from Territory", "All Accounts");
		}

	}

	public void logOutDxL() throws Exception{

		//if(cf.isElementVisible(ObjVeevaEmail.mailFrame, "Frame")){
			cf.switchToParentFrame();
		//}
		if(cf.isElementVisible(ObjVeevaEmail.logOutUser, "Logout")){
			driver.findElement(ObjVeevaEmail.logOutUser).click();
			cf.clickLink(ObjVeevaEmail.logOutLink, "Logout");
			if(cf.isElementVisible(ObjVeevaEmail.loginUserName, "Login form"))
				report.updateTestLog("Verify succesfully looged out or not", "Successfully logged Out", Status.PASS);
			else{
				report.updateTestLog("Verify succesfully looged out or not", "Successfully not logged Out", Status.FAIL);
				//frameworkparameters.setStopExecution(true);
			}
		}
		
		/**
		 * Please uncomment these lines after the batch and execute the batch. - to check after core 29
		 */
//		else{
//			report.updateTestLog("Verify able to click on the user for Log out", "Unable to click on the user for logout", Status.FAIL);
//		}
	}

	public void verifyEmailAccountDxL() throws Exception{
		int rows;
		Boolean mail=false;
		dateformatDxL();
		System.out.println("Current Date: " +date);
		String strDataSheet = "ApprovedEmail";
		String accountSelected =dataTable.getData(strDataSheet,"AccountSelected");
		String strDataSheet1 = "Login";
		String locale =dataTable.getData(strDataSheet1,"Locale");
		String profile =dataTable.getData(strDataSheet1,"Profile");
		
		cf.scrollToElement(By.xpath("//a[.='"+accountSelected+"']"), accountSelected+" identified");
		cf.clickLink(By.xpath("//a[.='"+accountSelected+"']"),accountSelected+" Account" );
		cf.waitForSeconds(10);
		driver.navigate().refresh();
		cf.clickSFLinkLet("Sent Email");

		if(cf.isElementVisible(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Sent Email')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"), "Go to List")){
			WebElement element1=driver.findElement(By.xpath("//td[@class='pbTitle']//*[starts-with(text(),'Sent Email')]//ancestor::table/parent::div/following-sibling::div//div[@class='pShowMore']//a[2]"));
			cf.clickElement(element1, "Go to list");
			cf.waitForSeconds(6);
		}
		String productdisplay;
		if(cf.isElementVisible(By.xpath("//h3[.='Sent Email']"),null)){
			rows=cf.getRowCountFromLinkLetTable("Sent Email");
			for(int j=rows+1;j>1;j--){
				if(locale.equals("JP") && profile.equals("Commercial")){
					productdisplay=cf.getDataFromLinkLetTable("Sent Email", "Product", j);
				}
				else{
					productdisplay=cf.getDataFromLinkLetTable("Sent Email", "Product Display", j);
				}
				String datesent=cf.getDataFromLinkLetTable("Sent Email", "Sent Date", j);
				System.out.println(datesent);
				String status=cf.getDataFromLinkLetTable("Sent Email", "Status", j);
				SentEmailID=cf.getDataFromLinkLetTable("Sent Email", "Sent Email Name", j);
				String datesentt[]=datesent.split(" ");

				if((date.trim()).equals(datesentt[0].trim())){
					if(productinEmail.equals(productdisplay)){
						if(status.equals("Delivered")){
							mail=true;
							break;
						}
					}
				}

			}

		}
		else{
			rows=cf.rowscountexpanded("Sent Email");
			cf.clickLink(By.xpath("//a[contains(@title,'Sent Date')]"), "Sorted Ascending");
			if(cf.isElementVisible(By.xpath("//a[@title='Sent Date - Sorted ascending']"), "Sorted Desending"))
				cf.clickLink(By.xpath("//a[@title='Sent Date - Sorted ascending']"), "Sorted Ascending");
			for(int i=2;i<=rows+1;i++){
				//cf.scrollToBottom();
				System.out.println(i);
				if(locale.equals("JP") && profile.equals("Commercial")){
					productdisplay=cf.getDataFromLinkLetTableexpanded("Sent Email", "Product", i);
				}
				else{
					productdisplay=cf.getDataFromLinkLetTableexpanded("Sent Email", "Product Display", i);
				}
				String datesent1=cf.getDataFromLinkLetTableexpanded("Sent Email", "Sent Date", i);
				System.out.println(datesent1);
				String status=cf.getDataFromLinkLetTableexpanded("Sent Email", "Status", i);
				SentEmailIDexpanded=cf.getDataFromLinkLetTableexpanded("Sent Email", "Sent Email Name", i);
				String datesentt1[]=datesent1.split(" ");
				if((date.trim()).equals(datesentt1[0].trim())){
					//if(productinEmail.equals(productdisplay)){
						if(status.equals("Delivered")){
							mail=true;
							break;
						//}
					}

				}
			}

		}

		if(mail==true)
			report.updateTestLog("Verify Email in "+accountSelected+"  account", "Able to see the mail in account with status as Delivered on   "+date, Status.PASS);
		else{
			report.updateTestLog("Verify Email in "+accountSelected+"  account", "Unable to see the mail in account with status as Delivered on  "+date, Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
	}


	public void checkMailDxL() throws Exception{

		String id = null;
		//	mf.verifyEmailSentToGmail(ObjVeevaEmail.mailaddress,ObjVeevaEmail.mailpassword , productsubject);
		//	mf.deletegmailmails();
		mf.openMailinatorEmail(ObjVeevaEmail.mailaddress, productsubject);
	//	String pageswitch=mf.strPageTitle;
		String mailswitch=mf.mailpage;
		String clickspage = null;
		System.out.println(mailswitch);
		//String sTitle = driver.getTitle();
		//System.out.println(sTitle);
		//mf.mailinatorEmailVerification();
		//mf.deleteAllMailsFromMailinator();

		if(!(SentEmailIDexpanded==null)){
			id=SentEmailIDexpanded;
			System.out.println(id);
			
		}
		else if(!(SentEmailID==null)){
			id=SentEmailID;
		}
		else{
			report.updateTestLog("Verify able to see the sent Email Id", "Unable to see Sent Email ID", Status.FAIL);
		}
		cf.waitForSeconds(15);
		id=id.trim();	
		cf.clickLink(By.xpath("//a[text()='"+id+"']"), "Sent Email Id");
		driver.navigate().refresh();
		cf.waitForSeconds(3);
		cf.pageRefresh();		
		String emailopened=driver.findElement(By.xpath("//td[.='Total Opens']/following-sibling::td[1]")).getText();
		int emailcount=Integer.parseInt(emailopened);
		if(emailcount==0)
		{
			for(int i=0;i<7;i++){
			driver.navigate().refresh();
			cf.waitForSeconds(3);
			cf.pageRefresh();}
		}
		//boolean c=false;
		/*do{
			if(emailcount==0)
			{
				driver.navigate().refresh();
				cf.waitForSeconds(6);
			}
			else
			{
				c=true;
			}
		}while(c==false);
		*/
		if(emailcount==2 || emailcount==1){
			report.updateTestLog("Verify Email how many times email has been opened by To and BCC user", "Email has opened "+emailcount+"  time", Status.PASS);

		}
		else{
			report.updateTestLog("Verify Email how many times email has been opened", "Email has not opened for one time", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		if(templatetype.equals("With Attachment")){
			clickspage=driver.getTitle();	
			System.out.println(mailswitch);
			cf.switchToPage(mailswitch);
			driver.switchTo().frame("msg_body");
			List<WebElement> links=driver.findElements(By.tagName("a"));
			if((links.size()>0)){
				for(int i=0;i<links.size();i++){
					WebElement elem=links.get(0);
					if(elem.getAttribute("href") != null){
						String linkname=elem.getText();
						cf.clickElement(elem, linkname);
						cf.waitForSeconds(10);
						String attach = cf.getSFPageTitle();
						wait.until(ExpectedConditions.titleContains(attach));
						cf.switchToPage(clickspage);
						driver.navigate().refresh();
						cf.waitForSeconds(3);
						cf.pageRefresh();
						String totalclicks=driver.findElement(By.xpath("//td[.='Total Clicks']/following-sibling::td[1]")).getText();
						int clickscount=Integer.parseInt(totalclicks);
						if(clickscount==0)
						{
							for(int j=0;j<7;j++){
							driver.navigate().refresh();
							cf.waitForSeconds(3);
							cf.pageRefresh();}
						}
						/*boolean f=false;
						do{
							if(clickscount==0)
							{
								driver.navigate().refresh();
								cf.waitForSeconds(6);
							}
							else
							{
								f=true;
							}
						}while(f==false);
						*/
						if(clickscount==1||clickscount==2)
							report.updateTestLog("Verify Email how many times email has been clicked", "Email has clicked "+clickscount+"  time", Status.PASS);
						else{
							report.updateTestLog("Verify Email how many times email has been clicked", "Email has clicked "+clickscount+" time", Status.FAIL);
							//frameworkparameters.setStopExecution(true);
						}
						String lastClick=driver.findElement(By.xpath("//td[.='Last Click']/following-sibling::td[1]")).getText();
						if(lastClick.contains(date))
							report.updateTestLog("Verify when Email is last Clicked", "Email has last clicked on "+date+" ", Status.PASS);
						else{
							report.updateTestLog("Verify when Email is last Clicked", "Email has not last clicked on "+date+" ", Status.FAIL);
							//frameworkparameters.setStopExecution(true);
						}  
						break;

					}

				}
			}
			else{
				report.updateTestLog("Verify Emails has links", "Email has no links.", Status.SCREENSHOT);

			}
		}
		else{
			report.updateTestLog("Verify attachments available", "Email has no attachments.", Status.SCREENSHOT);
		}
	}

	/*Core 28 - BCC field introduced*/
	public void checkMailBCCDxL() throws Exception{

	//	String id = null;
		String locale =dataTable.getData("Login","Locale");	
		String profile =dataTable.getData("Login","Profile");
		if(!(locale.equals("JP") && profile.equals("Commercial")))
		{
			openMailinatorEmail(ObjVeevaEmail.mailaddressBCC, productsubject);
			if(!(SentEmailIDexpanded==null)){
	//			id=SentEmailIDexpanded;
			}
			else if(!(SentEmailID==null)){
	//			id=SentEmailID;
			}
			else{
				report.updateTestLog("Verify able to see the sent Email Id", "Unable to see Sent Email ID", Status.FAIL);
			}

		}
		else
			report.updateTestLog("BCC", "BCC is not available for JP Commercial", Status.DONE);
	}
	
	
	public Boolean openMailinatorEmail(String strEmail_Id, String strEmail_Subject) throws Exception{
		
		String strCurrentMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String strCurrentClassName = Thread.currentThread().getStackTrace()[1].getFileName();
		System.out.println("businesscomponents." + strCurrentClassName + "." + strCurrentMethodName + " method is called");
		Boolean bOpenMailinatorEmail = false;
		//String strEmailId = dataTable.getData(strDataSheet, "Login_EmailAddress");
		//String strEmail_Subject = dataTable.getData(strDataSheet, "Login_ForgotPwd_MailSubject");
		//cf.switchToParentFrame();
		strEmail_Subject = strEmail_Subject.replace("Email: ", "");
		//rf.openUrlInNewTab(strMailinatorUrl);	
		strPageTitle = driver.getTitle();
		cf.openUrlInNewTab("https://www.mailinator.com/");	
		Boolean bMailinatorLoginPage = false;
		if(cf.isElementVisible(ObjVeevaEmail.inputEmail,"Mailinator - Loginpage")){
			report.updateTestLog("Verify Mailinator login page is displayed", "Mailinator loginpage is displayed", Status.PASS);
			bMailinatorLoginPage = true; 			
		}else{
			report.updateTestLog(strCurrentMethodName, "Mailinator login page is not displayed", Status.FAIL);
			//frameworkparameters.setStopExecution(true);
		}
		Boolean bEmailFound = false;
		By byEmailSubject = null;
		String strAppSubject = null;
		if(bMailinatorLoginPage){
			cf.setData(ObjVeevaEmail.inputEmail, "Email ID textbox", strEmail_Id);
			WebElement go=driver.findElement(ObjVeevaEmail.searchmail);
			cf.clickElement(go, "Go!");
			String strXPath = "//div[@id='inboxpane']//table/tbody/tr";	
			int iEmailCount = driver.findElements(By.xpath(strXPath)).size();
			System.out.println("iEmailCount: " + iEmailCount); 			
			for(int iEmail = 1; iEmail <= iEmailCount; iEmail++){
				//			By byEmailFrom = By.xpath(strXPath + "[" + iEmail + "]//div[@title='FROM']");
				//			byEmailSubject = By.xpath(strXPath + "[" + iEmail + "]//div[@class='all_message-min_text all_message-min_text-3']");
				//			By byEmailTime = By.xpath(strXPath + "[" + iEmail + "]//div[@class='all_message-min_datte all_message-min_datte-3 ng-binding']");
				By byEmailFrom = By.xpath(strXPath + "[" + iEmail + "]/td[3]");
				byEmailSubject = By.xpath(strXPath + "[" + iEmail + "]/td[4]");
				By byEmailTime = By.xpath(strXPath + "[" + iEmail + "]/td[5]");                     
				strAppSubject = cf.getData(byEmailSubject, iEmail +"EmailSubject", "text").replace("Sandbox: ", "");
				String strAppEmailFrom = cf.getData(byEmailFrom, iEmail +"EmailFrom", "text");
				String strAppEmailTime = cf.getData(byEmailTime, iEmail +"EmailTime", "text");
				if(strAppSubject.equals(strEmail_Subject)){
					bEmailFound = true;
					cf.isElementVisible(byEmailSubject, "EmailSubject: " + strEmail_Subject);
					cf.waitForSeconds(3); // to take screenshot to visible in test results
					report.updateTestLog(strCurrentMethodName, "Email came from " + strAppEmailFrom + " with subject as " 
							+ strEmail_Subject + " in time: " + strAppEmailTime, Status.PASS);
					System.out.println("Email came from " + strAppEmailFrom + " with subject as " 
							+ strEmail_Subject + " in time: " + strAppEmailTime);
					break;
				}
			}//for loop
			if(!bEmailFound){
				report.updateTestLog(strCurrentMethodName, "Email is not came from " + strEmail_Id + " with subject as " + strEmail_Subject, Status.FAIL);
			}
		}//bMailinatorLoginPage
		Boolean bFrame_MsgBody = false;
		if(bEmailFound){	
			//Click on the Email Subject
			WebElement click=driver.findElement(byEmailSubject);
			cf.clickElement(click, "Email Subject: " + strAppSubject);
			String mailpage="";
			mailpage=driver.getTitle();
			System.out.println(mailpage);
			cf.waitForSeconds(3); // to take screenshot to visible in test results
			report.updateTestLog(strCurrentMethodName, "Email with subject as " + strEmail_Subject + " is Opened", Status.PASS);
			System.out.println("Email with subject as " + strEmail_Subject + " is Opened");			
			By byFrame_MsgBody = By.xpath("//iframe[@id='msg_body']");
			bFrame_MsgBody = cf.switchToFrame(byFrame_MsgBody, "byFrame_MsgBody");	
			System.out.println(bFrame_MsgBody);
			System.out.println(strPageTitle + "Page title");
			cf.switchToParentFrame();
			cf.waitForSeconds(3);
			driver.switchTo().defaultContent();
			switchToPageArgumentDxL(strPageTitle);
			cf.waitForSeconds(5);
			System.out.println(strPageTitle);
			
			//cf.switchBacktoOriginalTab();
			
		}//bEmailFound
		if(bFrame_MsgBody){
			bOpenMailinatorEmail = true;			
		}//bbyFrame_MsgBody
		return bOpenMailinatorEmail;
	}
	
	public Boolean switchToPageArgumentDxL(String strPageTitle) throws Exception{
		Boolean bPageTitleSwitch = false;
		System.out.println("switchToPage: " + strPageTitle);
		String strTmpPageTitle1 = "";
		try {
			strTmpPageTitle1 = getSFPageTitleNameDxL();
		} catch (Exception e) {
			//Nothing
		}
		if(strTmpPageTitle1.equalsIgnoreCase(strPageTitle)){
			bPageTitleSwitch = true;
		}else{
			ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
			int iWindowcount = availableWindows.size();
			System.out.println(iWindowcount);
			String strHandle = availableWindows.get(iWindowcount - 3);
			System.out.println(strHandle);
			cf.waitForSeconds(2);
			driver.switchTo().window(strHandle);
			//Second Method
			//driver.switchTo().window(strPageTitle);
			System.out.println("Switched to Page: " + strHandle);
			driver.manage().window().maximize();

			System.out.println("iWindowcount:" + iWindowcount);	
			String strTmpPageTitle = getSFPageTitleNameDxL();
			if((strTmpPageTitle.toLowerCase().trim()).startsWith(strPageTitle.toLowerCase().trim())){    		
				bPageTitleSwitch = true;
				//break;
			}
			/*/
			for(int i=0; i< iWindowcount; i++){
				String strHandle = availableWindows.get(i);
				driver.switchTo().window(strHandle);
				String strTmpPageTitle = getSFPageTitle();
				System.out.println(i + " PageTitle: " + strTmpPageTitle);
				if((strTmpPageTitle.toLowerCase().trim()).startsWith(strPageTitle.toLowerCase().trim())){    		
					bPageTitleSwitch = true;
					break;
				}
			} //for Loop
			//*/		    	

		}
		if(bPageTitleSwitch){
			System.out.println("Switched to Page: " + strPageTitle);
			report.updateTestLog("switchToPage", "Switched to Page: " + strPageTitle,Status.PASS);
		}else{
			report.updateTestLog("switchToPage", "Page: " + strPageTitle + " not Found",Status.FAIL);
		}	
		return bPageTitleSwitch;
	} //End of switchToPage method
	
	public String getSFPageTitleNameDxL(){
		String strCurrentPageTitle = null;
		Boolean bPageTitleNotFound = true;
		while(bPageTitleNotFound){
			strCurrentPageTitle = driver.getTitle();
			if(strCurrentPageTitle.toLowerCase().contains("loading...")){
				cf.waitForSeconds(3);
			}else{
				bPageTitleNotFound = false;
			}
		}
		return strCurrentPageTitle;
	}
	
}


