package businesscomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.mfa.framework.Status;

import ObjectRepository.ObjVeevaCallsStandardization_Commercial;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ScriptHelper;
import supportlibraries.VeevaFunctions;

public class Veeva_CallsStandardization_US extends VeevaFunctions {
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	public Veeva_CallsStandardization_US(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	/**
	 * Validate the Record Types Selling and Non Selling and select the Record Type.
	 * @throws Exception
	 */
	public void validateRecordTypes() throws Exception{
		Boolean validate=false;
		if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.recordType, "Record Type")){
			report.updateTestLog("Verify able to see Record Type dropdown", "Able to see Record Type dropdown.", Status.PASS);
			List<String> optionsCall=new ArrayList<String>(Arrays.asList("Selling","Non-Selling"));
			for(int i=0;i<optionsCall.size();i++){
				String option=optionsCall.get(i).toString();
				WebElement optionele=driver.findElement(By.xpath("//select[@id='RecordTypeId']//option[.='"+option+"']"));
				if(cf.isElementPresent(optionele, "Option")){
					System.out.println(option);
					validate=true;
				}
				else{
					break;
				}
			}

			if(validate == true)
				report.updateTestLog("Verify able to see Selling and Non Selling under Record Type Dropdown.", "Able to see Selling and Non Selling under Record Type Dropdown.", Status.PASS);
			else
				report.updateTestLog("Verify able to see Selling and Non Selling under Record Type Dropdown.", "Unable to see Selling and Non Selling under Record Type Dropdown.", Status.FAIL);
		}
	}
	/**
	 * Validate the IOV Provided values in the dropdown
	 * Select the value from IOV dropdown.
	 * @throws Exception
	 */
	public void validateandselectIOVProvided() throws Exception{
		Boolean validate=false;
		if(cf.isElementVisible(ObjVeevaCallsStandardization_Commercial.iovprovided, "IOV")){
			report.updateTestLog("Verify able to see IOV's dropdown", "Able to see IOV's dropdown.", Status.PASS);
			List<String> optionsCall=new ArrayList<String>(Arrays.asList("Yes - I have provided Items of Value to HCP and recorded below","No - I have not provided any promotional items to HCP on this call"));
			for(int i=0;i<optionsCall.size();i++){
				String option=optionsCall.get(i).toString();
				WebElement optionele=driver.findElement(By.xpath("//select[@id='Promo_Items_Provided_AZ_US__c']//option[.='"+option+"']"));
				if(cf.isElementPresent(optionele, "Option")){
					System.out.println(option);
					validate=true;
				}
				else{
					break;
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}
			}

			if(validate == true){
				report.updateTestLog("Verify able to validate options under IOV Dropdown.", "Able to validate options under IOV Dropdown.", Status.PASS);
				cf.selectData(ObjVeevaCallsStandardization_Commercial.iovprovided, "Select the value from IOV dropdown", optionsCall.get(0).toString());
				//statesampleverification();
			}else
				report.updateTestLog("Verify able to validate options under IOV Dropdown.", "Unable to validate options under IOV Dropdown.", Status.FAIL);
		}
	}

	/**
	 * Select State Sample Verification value.
	 * @throws Exception
	 */
	public void statesampleverification() throws Exception{
		cf.selectARandomOptionFromComboBox(ObjVeevaCallsStandardization_Commercial.statesamplingverification, "Select any value from State Sampling Verification");
	}
	
	/**
	 * Select Items value.
	 * @throws Exception 
	 */
	
	public void itemsvalue() throws Exception{
		cf.clickLink(By.xpath("//h3[.='Items of Value']"), "Items of Value");
		cf.clickLink(By.xpath("//table[@name='call2_sample_vod__c']//tbody[@ng-repeat='sample in meta.sampleTypes'][2]//tr[2]/td[1]/input[1]"), "Sample Selection");
		cf.setData(By.xpath("//input[@ng-model='row.data.Quantity_vod__c']"), "Quantity", "1");
	
	}
}

