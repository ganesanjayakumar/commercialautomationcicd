/**
 * Package containing reusable libraries which are common across MFA's CRAFT and CRAFTLite Frameworks, and applicable to any automation tool based on Java
 * @author MFA
 */
package com.mfa.framework;