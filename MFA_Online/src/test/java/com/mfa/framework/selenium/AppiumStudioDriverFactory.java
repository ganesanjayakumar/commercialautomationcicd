package com.mfa.framework.selenium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.mfa.framework.FrameworkException;
import com.mfa.framework.Settings;

import io.appium.java_client.ios.IOSElement;

public class AppiumStudioDriverFactory {

                private static Properties mobileProperties;

                private AppiumStudioDriverFactory() {
                                // To prevent external instantiation of this class
                }

                /**
                * Function to return the BrowserStack {@link RemoteWebDriver}
                * object based on the parameters passed
                * 
                 * @param platformName
                *            The platform to be used for the test execution (Windows, Mac, Android
                *            etc.)
                * @param version
                *            The browser version to be used for the test execution
                * @param browserName
                *            The {@link Browser} to be used for the test execution
                * @param sauceUrl
                *            The Saucelabs URL to be used for the test execution
                * @return The corresponding {@link RemoteWebDriver} object
                */

                @SuppressWarnings({ "rawtypes", "unchecked" })
				public static WebDriver getBrowserStackWebDriver(
                		MobileExecutionPlatform platformName,Browser browser, String browserVersion){                                
                       // WebDriver driver = null;
                        IOSDriver<IOSElement> driver = null;
                        System.out.println("platformName: " + platformName);
                        //Add desiredCapabilities depends on the Platform
                        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                        //desiredCapabilities.setCapability("browserstack.debug", "false");
                        desiredCapabilities.setCapability("acceptSslCerts", "true");
                        //desiredCapabilities.setCapability("browserstack.debug", "true");
                       // desiredCapabilities.setCapability("browserstack.video", "false");
                        switch (platformName) {
                        case ANDROID:
                        	//Add desiredCapabilities for ANDROID
                        	//desiredCapabilities.setCapability("realMobile", "true");
                            desiredCapabilities.setCapability("browserName", "android");
                            desiredCapabilities.setCapability("platform", "ANDROID");
                         //   desiredCapabilities.setCapability("device", mobileDeviceName);
                            desiredCapabilities.setCapability("browser", "Chrome");
                            break;
                        case IOS:
                        	//desiredCapabilities.setCapability("platformName", "ios");
            				//desiredCapabilities.setCapability("platformVersion", mobileOSVersion);
            				//desiredCapabilities.setCapability("deviceName", mobileDeviceName);
                      	desiredCapabilities.setCapability("browserName", "Safari");
            				desiredCapabilities.setCapability("udid","890391fbd6e804c748e1389c2cba5e826506b3b6");
            				// desiredCapabilities.setCapability("app",
            				// properties.getProperty("iPhoneApplicationPath"));
            				//desiredCapabilities.setCapability("bundleId",
            				//		mobileProperties.getProperty("iPhoneBundleID"));
            				desiredCapabilities.setCapability("newCommandTimeout", 320);
            				try {
            					driver = new IOSDriver(new URL("http://localhost:4723/wd/hub"),
            							desiredCapabilities);

            				} catch (MalformedURLException e) {
            					throw new FrameworkException(
            							"The IOS driver invokation has problem, please re-check the capabilities or Start Appium");
            				}
            				break;


                        default:
                        	throw new FrameworkException("Unhandled Mobile Execution Platform Mode!");
                        }
						return driver;
                        
                        //desiredCapabilities.setCapability("mobileOSVersion", mobileOSVersion);
                        // desiredCapabilities.setCapability("screen-resolution","800x600");
                        //desiredCapabilities.setCapability("name",testParameters.getCurrentTestcase());

                }

                @SuppressWarnings("rawtypes")
                public static AppiumDriver getSauceAppiumDriver(
                                                MobileExecutionPlatform executionPlatform, String deviceName,
                                                String sauceURL, SeleniumTestParameters testParameters) {

                                AppiumDriver driver = null;

                                mobileProperties = Settings.getMobilePropertiesInstance();

                                DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                                try {
                                                switch (executionPlatform) {

                                                case ANDROID:

                                                                desiredCapabilities.setCapability("platformName", "Android");
                                                                desiredCapabilities.setCapability("deviceName", deviceName);
                                                                desiredCapabilities.setCapability("app",
                                                                                                mobileProperties.getProperty("SauceAndroidIdentifier"));
                                                                desiredCapabilities.setCapability("name",
                                                                                                testParameters.getCurrentTestcase());
                                                                /*
                                                                * desiredCapabilities.setCapability("appPackage",
                                                                * mobileProperties.getProperty("Application_Package_Name"));
                                                                * desiredCapabilities .setCapability("appActivity",
                                                                * mobileProperties
                                                                * .getProperty("Application_MainActivity_Name"));
                                                                */
                                                                // desiredCapabilities.setCapability("app",
                                                                // "PUBLIC:appium/apiDemos.apk");
                                                                try {
                                                                                driver = new AndroidDriver(new URL(sauceURL),
                                                                                                                desiredCapabilities);
                                                                } catch (MalformedURLException e) {
                                                                                throw new FrameworkException(
                                                                                                                "The android driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
                                                                }

                                                                break;

                                                case IOS:

                                                                desiredCapabilities.setCapability("appiumVersion", "1.4.16");
                                                                desiredCapabilities.setCapability("platformName", "ios");
                                                                desiredCapabilities.setCapability("deviceName", deviceName);
                                                                desiredCapabilities.setCapability("browserName", "");
                                                                desiredCapabilities.setCapability("name",
                                                                                                testParameters.getCurrentTestcase());
                                                                desiredCapabilities.setCapability("platformVersion", "9.0");
                                                                desiredCapabilities.setCapability("app",
                                                                                                mobileProperties.getProperty("SauceIosBundleID"));

                                                                try {
                                                                                driver = new IOSDriver(new URL(sauceURL),
                                                                                                                desiredCapabilities);

                                                                } catch (MalformedURLException e) {
                                                                                throw new FrameworkException(
                                                                                                                "The IOS driver invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
                                                                }
                                                                break;

                                                case WEB_ANDROID:

                                                                desiredCapabilities.setCapability("platformName", "Android");
                                                                desiredCapabilities.setCapability("deviceName", deviceName);
                                                                desiredCapabilities.setCapability("browserName", "Browser");
                                                                desiredCapabilities.setCapability("name",
                                                                                                testParameters.getCurrentTestcase());

                                                                try {
                                                                                driver = new AndroidDriver(new URL(sauceURL),
                                                                                                                desiredCapabilities);
                                                                } catch (MalformedURLException e) {
                                                                                throw new FrameworkException(
                                                                                                                "The android driver/browser invokation has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
                                                                }
                                                                break;

                                                case WEB_IOS:

                                                                desiredCapabilities.setCapability("platformName", "ios");
                                                                desiredCapabilities.setCapability("deviceName", deviceName);
                                                                desiredCapabilities.setCapability("name",
                                                                                                testParameters.getCurrentTestcase());
                                                                desiredCapabilities.setCapability("browserName", "Safari");
                                                                desiredCapabilities.setCapability("platformVersion", "9.0");

                                                                try {
                                                                                driver = new IOSDriver(new URL(sauceURL),
                                                                                                                desiredCapabilities);

                                                                } catch (MalformedURLException e) {
                                                                                throw new FrameworkException(
                                                                                                                "The IOS driver invokation/browser has problem, please re-check the capabilities and check the SauceLabs details URL, Username and accessKey ");
                                                                }
                                                                break;

                                                default:
                                                                throw new FrameworkException("Unhandled ExecutionMode!");

                                                }
                                } catch (Exception ex) {
                                                ex.printStackTrace();
                                                throw new FrameworkException(
                                                                                "The Sauce appium driver invocation created a problem , please check the capabilities");
                                }
                                return driver;

                }

}
