package com.mfa.framework.selenium;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Settings;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.*;

/**
 * Factory class for creating the {@link WebDriver} object as required
 * @author MFA
 */
public class WebDriverFactory
{
	private static Properties properties;

	private WebDriverFactory()
	{
		// To prevent external instantiation of this class
	}

	/**
	 * Function to return the appropriate {@link WebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @return The corresponding {@link WebDriver} object
	 */
	@SuppressWarnings("deprecation")
	public static WebDriver getWebDriver(Browser browser)
	{
		WebDriver driver = null;
		properties = Settings.getInstance();
		boolean proxyRequired = Boolean.parseBoolean(properties.getProperty("ProxyRequired"));

		switch (browser)
		{
			case CHROME :
				// Takes the system proxy settings automatically			
				System.setProperty("webdriver.chrome.driver", properties.getProperty("ChromeDriverPath"));
				Map<String, Object> chromePreferences = new Hashtable<String, Object>();
				chromePreferences.put("profile.default_content_settings.popups", 0);
				chromePreferences.put("download.default_directory", "C:\\Users\\" + System.getProperty("user.name") + "\\Downloads");
				chromePreferences.put("download.prompt_for_download", "false");
				chromePreferences.put("safebrowsing.enabled", "false"); 
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--safebrowsing-disable-download-protection");
				options.addArguments("--safebrowsing-disable-extension-blacklist");
		        options.setExperimentalOption("prefs", chromePreferences);
		        DesiredCapabilities cap = DesiredCapabilities.chrome();
		        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		        cap.setCapability(ChromeOptions.CAPABILITY, options);        
				driver = new ChromeDriver(cap);
				break;
			case CHROME_HEADLESS :
				// Takes the system proxy settings automatically
				System.setProperty("webdriver.chrome.driver", properties.getProperty("ChromeDriverPath"));
				ChromeOptions option = new ChromeOptions();
				Map<String, Object> chromePreferences1 = new Hashtable<String, Object>();
				chromePreferences1.put("profile.default_content_settings.popups", 0);
				chromePreferences1.put("download.prompt_for_download", "false");
				option.addArguments("headless");
				option.addArguments("window-size=1600,900");
				driver = new ChromeDriver(option);
				break;
			//		case CHROME:
			//			ChromeOptions ops = new ChromeOptions();
			//			ops.addArguments("--disable-notifications");
			//			System.setProperty("webdriver.chrome.driver",properties.getProperty("ChromeDriverPath"));
			//			driver = new ChromeDriver(ops);
			//			break;
			//		case CHROME:
			//			// Takes the system proxy settings automatically//			
			//			DesiredCapabilities capability = DesiredCapabilities.chrome();
			//			// To Accept SSL certificate
			//			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			//			ChromeOptions options = new ChromeOptions();
			//			Map<String, Object> prefs = new HashMap<String, Object>();
			//			prefs.put("profile.default_content_settings.popups", 0);
			//			prefs.put("profile.default_content_setting_values.automatic_downloads", 1);
			//			prefs.put("download.prompt_for_download", false);
			//			options.addArguments("disable-extensions");
			//			prefs.put("credentials_enable_service", false);
			//			prefs.put("password_manager_enabled", false);
			//			options.setExperimentalOption("prefs", prefs);
			//			options.addArguments("chrome.switches","--disable-extensions");
			//			options.addArguments("--test-type");
			//			options.addArguments("disable-infobars");
			//			options.addArguments("--disable-notifications");
			//			///options.addArguments("incognito");/////////////////////////////////////
			//			capability.setCapability(ChromeOptions.CAPABILITY, options);
			//			capability.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
			//			// setting system property for Chrome browser
			//			System.setProperty("webdriver.chrome.driver",properties.getProperty("ChromeDriverPath"));
			//			// create Google Chrome instance and maximize it
			//			driver = new ChromeDriver(capability);
			//			break;

			case FIREFOX :
				// Takes the system proxy settings automatically
				System.setProperty("webdriver.gecko.driver", properties.getProperty("GeckoDriverPath"));
				driver = new FirefoxDriver();
				break;

			case GHOST_DRIVER :
				// Takes the system proxy settings automatically (I think!)

				System.setProperty("phantomjs.binary.path", properties.getProperty("PhantomJSPath"));
				driver = new PhantomJSDriver();
				break;

			case INTERNET_EXPLORER :
				// Takes the system proxy settings automatically

				System.setProperty("webdriver.ie.driver", properties.getProperty("InternetExplorerDriverPath"));

				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability("InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION", true);
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capabilities.setCapability("ignoreZoomSetting", true);
				capabilities.setCapability("EnableNativeEvents", false);
				//capabilities.setCapability("nativeEvents", false);
				capabilities.setCapability("ignoreProtectedModeSettings", true);
				capabilities.setCapability("initialBrowserUrl", "www.google.co.in");
				capabilities.setCapability("EnablePersistentHover", true);
				driver = new InternetExplorerDriver(capabilities);

				//			/////////code added by Kamatchirajan on Jul 20
				//			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				//		    capabilities.setCapability("InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION", true);
				//		    capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				//		    capabilities.setCapability("ignoreZoomSetting", true);////
				//		    capabilities.setCapability("EnableNativeEvents", false);
				//		    //capabilities.setCapability("nativeEvents", false);
				//		    capabilities.setCapability("ignoreProtectedModeSettings", true);///
				//		    capabilities.setCapability("initialBrowserUrl","www.google.co.in");///
				//		    capabilities.setCapability("EnablePersistentHover", true);///		    
				//		    capabilities.setCapability("requireWindowFocus", true);
				//		    capabilities.setCapability("disable-popup-blocking", true);///
				//		    driver = new InternetExplorerDriver(capabilities);		

				//		    InternetExplorerOptions options = new InternetExplorerOptions();
				//		    options.enablePersistentHovering();
				//		    options.ignoreZoomSettings();
				//		    options.introduceFlakinessByIgnoringSecurityDomains();
				//		    options.withInitialBrowserUrl("www.google.co.in");
				//		    //options.enableNativeEvents();		    
				//		    //options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
				//		    //options.RequireWindowFocus = true;
				//		    driver = new InternetExplorerDriver(options);

				//driver = new InternetExplorerDriver();
				break;

			case OPERA :
				// Does not take the system proxy settings automatically!
				// NTLM authentication for proxy NOT supported

				if (proxyRequired)
				{
					DesiredCapabilities desiredCapabilities = getProxyCapabilities();
					driver = new OperaDriver(desiredCapabilities);
				}
				else
				{
					driver = new OperaDriver();
				}

				break;

			case SAFARI :
				// Takes the system proxy settings automatically

				driver = new SafariDriver();
				break;

			default :
				throw new FrameworkException("Unhandled browser!");
		}

		return driver;
	}

	private static DesiredCapabilities getProxyCapabilities()
	{
		properties = Settings.getInstance();
		String proxyUrl = properties.getProperty("ProxyHost") + ":" + properties.getProperty("ProxyPort");

		Proxy proxy = new Proxy();
		proxy.setProxyType(ProxyType.MANUAL);
		proxy.setHttpProxy(proxyUrl);
		proxy.setFtpProxy(proxyUrl);
		proxy.setSslProxy(proxyUrl);

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.PROXY, proxy);

		return desiredCapabilities;
	}

	/**
	 * Function to return the {@link RemoteWebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param browserVersion The browser version to be used for the test execution
	 * @param platform The {@link Platform} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getRemoteWebDriver(Browser browser, String browserVersion, Platform platform, String remoteUrl)
	{
		// For running RemoteWebDriver tests in Chrome and IE:
		// The ChromeDriver and IEDriver executables needs to be in the PATH of the remote machine
		// To set the executable path manually, use:
		// java -Dwebdriver.chrome.driver=/path/to/driver -jar selenium-server-standalone.jar
		// java -Dwebdriver.ie.driver=/path/to/driver -jar selenium-server-standalone.jar

		properties = Settings.getInstance();
		boolean proxyRequired = Boolean.parseBoolean(properties.getProperty("ProxyRequired"));

		DesiredCapabilities desiredCapabilities = null;
		if (browser.equals(Browser.OPERA) && proxyRequired)
		{
			desiredCapabilities = getProxyCapabilities();
		}
		else
		{
			desiredCapabilities = new DesiredCapabilities();
		}

		desiredCapabilities.setBrowserName(browser.getValue());

		if (browserVersion != null)
		{
			desiredCapabilities.setVersion(browserVersion);
		}
		if (platform != null)
		{
			desiredCapabilities.setPlatform(platform);
		}

		desiredCapabilities.setJavascriptEnabled(true); // Pre-requisite for remote execution

		URL url = getUrl(remoteUrl);

		return new RemoteWebDriver(url, desiredCapabilities);
	}

	private static URL getUrl(String remoteUrl)
	{
		URL url;
		try
		{
			url = new URL(remoteUrl);
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			throw new FrameworkException("The specified remote URL is malformed");
		}
		return url;
	}

	/**
	 * Function to return the {@link RemoteWebDriver} object based on the parameters passed
	 * @param browser The {@link Browser} to be used for the test execution
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getRemoteWebDriver(Browser browser, String remoteUrl)
	{
		return getRemoteWebDriver(browser, null, null, remoteUrl);
	}

	/**
	 * Function to return the {@link ChromeDriver} object emulating the device specified by the user
	 * @param deviceName The name of the device to be emulated (check Chrome Dev Tools for a list of available devices)
	 * @return The corresponding {@link ChromeDriver} object
	 */
	@SuppressWarnings("deprecation")
	public static WebDriver getEmulatedWebDriver(String deviceName)
	{
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceName);

		properties = Settings.getInstance();
		System.setProperty("webdriver.chrome.driver", properties.getProperty("ChromeDriverPath"));

		return new ChromeDriver(desiredCapabilities);
	}

	private static DesiredCapabilities getEmulatedChromeDriverCapabilities(String deviceName)
	{
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", deviceName);

		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);

		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		return desiredCapabilities;
	}

	/**
	 * Function to return the {@link RemoteWebDriver} object emulating the device specified by the user
	 * @param deviceName The name of the device to be emulated (check Chrome Dev Tools for a list of available devices)
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getEmulatedRemoteWebDriver(String deviceName, String remoteUrl)
	{
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceName);
		desiredCapabilities.setJavascriptEnabled(true); // Pre-requisite for remote execution

		URL url = getUrl(remoteUrl);

		return new RemoteWebDriver(url, desiredCapabilities);
	}

	/**
	 * Function to return the {@link ChromeDriver} object emulating the device attributes specified by the user
	 * @param deviceWidth The width of the device to be emulated (in pixels)
	 * @param deviceHeight The height of the device to be emulated (in pixels)
	 * @param devicePixelRatio The device's pixel ratio
	 * @param userAgent The user agent string
	 * @return The corresponding {@link ChromeDriver} object
	 */
	@SuppressWarnings("deprecation")
	public static WebDriver getEmulatedWebDriver(int deviceWidth, int deviceHeight, float devicePixelRatio, String userAgent)
	{
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceWidth, deviceHeight, devicePixelRatio, userAgent);

		properties = Settings.getInstance();
		System.setProperty("webdriver.chrome.driver", properties.getProperty("ChromeDriverPath"));

		return new ChromeDriver(desiredCapabilities);
	}

	private static DesiredCapabilities getEmulatedChromeDriverCapabilities(int deviceWidth, int deviceHeight, float devicePixelRatio, String userAgent)
	{
		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
		deviceMetrics.put("width", deviceWidth);
		deviceMetrics.put("height", deviceHeight);
		deviceMetrics.put("pixelRatio", devicePixelRatio);

		Map<String, Object> mobileEmulation = new HashMap<String, Object>();
		mobileEmulation.put("deviceMetrics", deviceMetrics);
		//mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
		mobileEmulation.put("userAgent", userAgent);

		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);

		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		return desiredCapabilities;
	}

	/**
	 * Function to return the {@link RemoteWebDriver} object emulating the device attributes specified by the user
	 * @param deviceWidth The width of the device to be emulated (in pixels)
	 * @param deviceHeight The height of the device to be emulated (in pixels)
	 * @param devicePixelRatio The device's pixel ratio
	 * @param userAgent The user agent string
	 * @param remoteUrl The URL of the remote machine to be used for the test execution
	 * @return The corresponding {@link RemoteWebDriver} object
	 */
	public static WebDriver getEmulatedRemoteWebDriver(int deviceWidth, int deviceHeight, float devicePixelRatio, String userAgent, String remoteUrl)
	{
		DesiredCapabilities desiredCapabilities = getEmulatedChromeDriverCapabilities(deviceWidth, deviceHeight, devicePixelRatio, userAgent);
		desiredCapabilities.setJavascriptEnabled(true); // Pre-requisite for remote execution

		URL url = getUrl(remoteUrl);

		return new RemoteWebDriver(url, desiredCapabilities);
	}

}