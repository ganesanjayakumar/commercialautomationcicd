/**
 * Package containing reusable libraries which are common across MFA's CRAFT and CRAFTLite Frameworks, but specific to Selenium
 * @author MFA
 */
package com.mfa.framework.selenium;